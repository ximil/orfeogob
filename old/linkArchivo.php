<?php  /*
  * Invocado por una funcion javascript (funlinkArchivo(numrad,rutaRaiz))
  * Consulta el path del radicado 
  * @author Liliana Gomez Velasquez
  * @since 20 de AGOSTO de 2009
  * @category imagenes
 */

session_start(); date_default_timezone_set('America/Bogota'); date_default_timezone_set('America/Bogota');
ini_set("memory_limit","600M");
$krd = $_SESSION["krd"];
$numrad=$_GET['numrad'];
if (!$ruta_raiz) $ruta_raiz = ".";
include $ruta_raiz.'/conf/VariablesGlobales.php';
if (isset($db)) unset($db);
include_once("$ruta_raiz/include/db/ConnectionHandler.php");
include_once $ruta_raiz . '/validadte.php';
$db = new ConnectionHandler("$ruta_raiz");
$db->conn->SetFetchMode( ADODB_FETCH_ASSOC );
//$db->conn->debug=true;
include_once "$ruta_raiz/tx/verLinkArchivo.php";

$verLinkArchivo = new verLinkArchivo($db);

if (strlen( $numrad) <= "14"){
//Se trata de un Radicado

  $resulVali = $verLinkArchivo->valPermisoRadi($numrad);
  $verImg = $resulVali['verImg'];
  $pathImagen = $resulVali['pathImagen'];
  if(substr($pathImagen,0,9) == "../bodega") {
  	//$pathImagen=str_replace('../bodega','./bodega',$pathImagen);
  	$pathImagen=str_replace('../bodega',RUTA_BODEGA,$pathImagen);
  	$file = $pathImagen;
  }elseif(substr($pathImagen,0,12) == "../../bodega") {
    //$pathImagen=str_replace('../../bodega','./bodega',$pathImagen);
    $pathImagen=str_replace('../../bodega',RUTA_BODEGA,$pathImagen);
  	$file = $pathImagen;
  }
  	else {
  		//$file = $ruta_raiz. "/bodega/".$pathImagen;
  		$file = RUTA_BODEGA.$pathImagen;
  }	
}else {
//Se trata de un anexo	
  $resulValiA = $verLinkArchivo->valPermisoAnex($numrad);
  $verImg = $resulValiA['verImg'];
  $pathImagen = $resulValiA['pathImagen'];
  //$file="$ruta_raiz/bodega/".substr(trim($numrad),0,4)."/".substr(trim($numrad),4,3)."/docs/".trim($pathImagen);
  $file=RUTA_BODEGA.substr(trim($numrad),0,4)."/".substr(trim($numrad),4,3)."/docs/".trim($pathImagen);	
/*}
if(substr($pathImagen,0,3) == 201){
$file = RUTA_BODEGA.$pathImagen;
*/
}
$fileArchi = $file;
$tmpExt = explode('.',$pathImagen);
$filedatatype = $pathImagen;
/*echo $pathImagen;
die($pathImagen);*/
// Si se tiene una extension 
if(count($tmpExt)>1){
   $filedatatype =  $tmpExt[count($tmpExt)-1];
}
if($verImg=="SI"){
  if (file_exists($fileArchi)) {
    header('Content-Description: File Transfer');
    switch($filedatatype)
      {
         case 'odt':
			   header('Content-Type: application/vnd.oasis.opendocument.text');
			   break;
         case 'doc':
               header('Content-Type: application/msword');
               break;
         case 'tif':
               header('Content-Type: image/TIFF');
               break;
         case 'pdf':
               header('Content-Type: application/pdf');
               break;  
         case 'xls':
               header('Content-Type: application/vnd.ms-excel');
               break;
         case 'csv':
               header('Content-Type: application/vnd.ms-excel');
               break;
         case 'ods':
               header('Content-Type: application/vnd.ms-excel');
               break;   
         default :
		      header('Content-Type: application/octet-stream');
			  break;  
        }
        header('Content-Disposition: inline; filename='.basename($file));
        header("Expires: Mon, 26 Nov 1962 00:00:00 GMT");
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
        ob_clean();
        flush();
        readfile($file);
        exit;
    }else {
 	   die ("<B><CENTER>  NO se encontro el Archivo </a><br>".$pathImagen);
    }
  }elseif($verImg == "NO"){ 
  	die ("<B><CENTER>  NO tiene permiso para acceder al Archivo </a><br>");
  }
else{
    die ("<B><CENTER>  NO se ha podido encontrar informacion del Documento</a><br>"); 
}
?>
