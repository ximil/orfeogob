<?php putenv("NLS_LANG=American_America.UTF8");
/* * ***************************************************************** */
/* 	Busqueda Piloto ==== Busqueda clasica  						   	 */
/*  Rediseno Hardy Deimont Nino Velasquez 							 */
/*  Archivo  relacionados                							 */
/*  	/busqueda/CiudadanoShow.php				 					 */
/*  	/busqueda/ResultadoBusqueda.php								 */
/*  	/busqueda/common.php										 */
/*  	/js/Archivo.js 												 */
/*  	/js/form-field-tooltip.js									 */
/*  	/js/rounded-corners.js 										 */
/*  	/imagenes/green-arrow.gif									 */
/*  	/imagenes/loading.gif										 */
/*  	/imagenes/green-arrow-right.gif								 */
/*  	/include/db/ConnectionHandler.php							 */
/*  	/include/query/busqueda/busquedaPiloto1.php					 */
/* * ***************************************************************** */
session_start();
date_default_timezone_set('America/Bogota');
/**
 * Modificacion para aceptar Variables GLobales
 * @autor Infometrika 2009/05 
 * @fecha 2009/05
 */
$krd = $_SESSION["krd"];
$dependencia = $_SESSION["dependencia"];
$usua_doc = $_SESSION["usua_doc"];
$codusuario = $_SESSION["codusuario"];
$nivelus = $_SESSION["codi_nivel"];
$metadatos =$_SESSION['metadatos'];
if (isset($_REQUEST["flds_TDOC_CODI"]))
    $flds_TDOC_CODI = $_REQUEST["flds_TDOC_CODI"];
foreach ($_GET as $key => $valor)
    $$key = $valor;
foreach ($_POST as $key => $valor)
    $$key = $valor;

$ruta_raiz = "..";
require_once ("$ruta_raiz/include/db/ConnectionHandler.php");
if(count($metadatos)>0){
    $metabusq="<select id='metadato' name='metadato' class='select' onchange=\"cambiarMeta(this.value);\"><option value=0>Buscar Por Metadato</option>\n";
    for($i=0;$i<count($metadatos);$i++){
        $metabusq.="<option value={$metadatos[$i]['id']}>{$metadatos[$i]['nomb']}</option>";
    }
    $metabusq.="</select>";
    $dis='table-row';
}
else{
    $metabusq="";
    $dis='none';
}
$krdAnt = $krd;

$ruta_raiz = "..";
/* if (! $krd)
  $krd = $krdAnt; */
// print_r($_SESSION);
include($ruta_raiz . '/validadte.php');
include ("common.php");
$fechah = date("ymd") . "_" . time("hms");
// busqueda CustomIncludes end
//-------------------------------
//$db->conn->debug=true;
//===============================
// Save Page and File Name available into variables
//-------------------------------
$sFileName = "busquedaPiloto.php";
//===============================
//$usu = get_param("usu");
$usu = $krd;
//$niv = get_param("niv");
$niv = $nivelus;
if (strlen($niv)) {
    /* 	set_session ( "UserID", $usu );
      set_session ( "krd", $krd );
      set_session ( "Nivel", $niv ); */
}

//$krd = get_param ( "krd" );
//Variables select
$sFormTitle = "Busqueda Clasica";
$sActionFileName = "busquedaPiloto.php";
$ss_TDOC_CODIDisplayValue = "Todos los Tipos";
$ss_TRAD_CODIDisplayValue = "Todos los Tipos (-1,-2,-3,-5, . . .)";
$ss_RADI_DEPE_ACTUDisplayValue = "Todas las Dependencias";

$desdeTimestamp = mktime(0, 0, 0, Date('m') - 1, Date('d'), Date('Y'));
$flds_desde_dia = Date('d', $desdeTimestamp);
$flds_hasta_dia = Date('d');
$flds_desde_mes = Date('m', $desdeTimestamp);
$flds_hasta_mes = Date('m');
$flds_desde_ano = Date('Y', $desdeTimestamp);
$flds_hasta_ano = Date('Y');
//===============================
// Display page
//===============================
// HTML Page layout
//-------------------------------
//Función que calcula el tiempo transcurrido
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link rel="stylesheet" href="<?php echo  $ruta_raiz ?>/estilos/orfeo.css"
              type="text/css" />
        <script type="text/javascript" src="<?php echo  $ruta_raiz ?>/js/commonFuntion.js" ></script>
        <!--link rel="stylesheet" href="<?php echo  $ruta_raiz ?>/estilos/form-field-tooltip.css" media="screen" type="text/css"-->
        <!--script type="text/javascript" src="<?php echo  $ruta_raiz ?>/js/rounded-corners.js"></script-->
        <!--script type="text/javascript" src="<?php echo  $ruta_raiz ?>/js/form-field-tooltip.js"></script-->
        <?php include_once "$ruta_raiz/js/funtionImage.php"; ?>
    </head>
    <script>
        function Consultar(page) {
	    var metaTags;
            document.getElementById('contenido').innerHTML = '<center><img  alt="Procesando" src="../imagenes/loading.gif"></center>';
	    if(document.getElementById('metaTag')!==null){
		metaTags="&metadata="+document.getElementById('metadato').value+"&tag="+document.getElementById('tag').value+"&metaTag="+document.getElementById('metaTag').value;
	    }
	    else{
		metaTags="&metadata=0&tag=0&metaTag=";
	    }
            radicado = document.getElementById('s_RADI_NUME_RADI').value;
            documento = document.getElementById('s_DOCTO').value;
            EXT = document.getElementById('ext').value;
            expediente = document.getElementById('s_SGD_EXP_SUBEXPEDIENTE').value;
            tipo = document.getElementById('s_entrada').value;
            busqueda = document.getElementById('s_RADI_NOMB').value;
            CriterioBusq = document.getElementById('CriterioBusq').value;
            inidia = document.getElementById('s_desde_dia').value;
            inimes = document.getElementById('s_desde_mes').value;
            iniano = document.getElementById('s_desde_ano').value;
            findia = document.getElementById('s_hasta_dia').value;
            finmes = document.getElementById('s_hasta_mes').value;
            finano = document.getElementById('s_hasta_ano').value;
            trpd = document.getElementById('s_TDOC_CODI').value;
            depe = document.getElementById('s_RADI_DEPE_ACTU').value;
            CriterioExp = document.getElementById('CriterioExp').value;
            var poststr2 = "&EXT=" + EXT + "&FormCIUDADANO_Page=" + page + "&s_RADI_NUME_RADI=" + radicado + "&s_DOCTO=" + documento + "&s_SGD_EXP_SUBEXPEDIENTE=" + expediente + "&s_entrada=" + tipo + "&s_RADI_NOMB=" + busqueda + "&CriterioBusq=" + CriterioBusq + "&CriterioExp=" + CriterioExp + "&s_desde_dia=" + inidia + "&s_desde_mes=" + inimes + "&s_desde_ano=" + iniano + "&s_hasta_dia=" + findia + "&s_hasta_mes=" + finmes + "&s_hasta_ano=" + finano + "&s_TDOC_CODI=" + trpd + "&s_RADI_DEPE_ACTU=" + depe + "&indiVinculo=<?php echo  $_GET['indiVinculo']; ?>&etapa=<?php echo  $_GET['etapa']; ?>";
            var poststr = "<?php  echo session_name() . "=" . session_id() . '&dependencia=' . $dependencia . '&krd=' . $krd; ?>" + poststr2+metaTags;
            divajax('../busqueda/ResultadoBusqueda.php', poststr, '', 'contenido');
            botonBusqueda('Proces');
        }

	function cambiarMeta(id){
	    if(id!=0){
		var poststr="action=selMetadatos&id="+id;
		divajax('operBusq.php',poststr,'','tagm');
		document.getElementById('tagn').innerHTML ="<input id='metaTag' name='metaTag' type='text' class='tex_area'>"; 
	    }
	    else{
		document.getElementById('tagm').innerHTML ="";
		document.getElementById('tagn').innerHTML ="";
	    }
	}

        function limpiar()
        {
            document.Search.elements['s_RADI_NUME_RADI'].value = "";
            document.Search.elements['s_RADI_NOMB'].value = "";
            document.Search.elements['s_RADI_DEPE_ACTU'].value = "";
            document.Search.elements['s_DOCTO'].value = "";
            document.Search.elements['s_TDOC_CODI'].value = "9999";
            document.Search.elements['CriterioExp'].value = "1";
            document.Search.elements['CriterioBusq'].value = "1";
            document.Search.elements['s_entrada'].value = "9999"
            /**
             * Limpia el campo expediente
             * Fecha de modificaci�n: 30-Junio-2006
             * Modificador: Supersolidaria
             */
            document.Search.elements['s_SGD_EXP_SUBEXPEDIENTE'].value = "";
<?php 
$dia = intval(date("d"));
$mes = intval(date("m"));
$ano = intval(date("Y"));
?>
            document.Search.elements['s_desde_dia'].value = "<?php echo  $dia ?>";
            document.Search.elements['s_hasta_dia'].value = "<?php echo  $dia ?>";
            document.Search.elements['s_desde_mes'].value = "<?php echo  ($mes - 1) ?>";
            document.Search.elements['s_hasta_mes'].value = "<?php echo  $mes ?>";
            document.Search.elements['s_desde_ano'].value = "<?php echo  $ano ?>";
            document.Search.elements['s_hasta_ano'].value = "<?php echo  $ano ?>";
            for (i = 4; i < document.Search.elements.length; i++)
                document.Search.elements[i].checked = 1;
        }

        function noPermiso() {
            alert("No tiene permiso para acceder");
        }

        function pasar_datos(fecha, num)
        {
<?php 
echo "if(num==1){";
echo " opener.document.VincDocu.numRadi.value = fecha\n";
echo "opener.focus(); window.close();\n }";
echo "if(num==2){";
echo "opener.document.insExp.numeroExpediente.value = fecha\n";

echo "opener.focus(); window.close();}\n";
?>

        }
        function botonBusqueda(tipo, iPage) {
            radicado = document.getElementById('s_RADI_NUME_RADI').value;
            exp = document.getElementById('s_SGD_EXP_SUBEXPEDIENTE').value;
            docu = document.getElementById('s_DOCTO').value;
	    var metaTag;
	    if(document.getElementById('metaTag')!==null){
		metaTag=document.getElementById('metaTag').value;
	    }
	    else{
		metaTag='';
	    }
            tipCri = document.getElementById('CriterioBusq').value;
            Crit = document.getElementById('s_RADI_NOMB').value;
            inidia = parseInt(document.getElementById('s_desde_dia').value);
            inimes = parseInt(document.getElementById('s_desde_mes').value);
            iniano = parseInt(document.getElementById('s_desde_ano').value);
            findia = parseInt(document.getElementById('s_hasta_dia').value);
            finmes = parseInt(document.getElementById('s_hasta_mes').value);
            finano = parseInt(document.getElementById('s_hasta_ano').value);
            if (radicado.length < 4) {
                if (exp.length < 5) {
                    if (Crit.length < 4 && tipCri != 10) {
                        if (docu.length < 3) {
			    if(metaTag.length < 3){
                            alert('La B\xfasqueda no  inicia por los siguientes motivos:\n \n* El n\xfamero de radicado debe tener m\xednimo 4 caracteres\n* El n\xfamero de expediente debe tener m\xednimo 5 caracteres\n* El n\xfamero de documento debe tener m\xednimo 3 caracteres\n* El criterio de b\xfasqueda (Buscar por) debe tener m\xednimo 4 caracteres\n* El tag de b\xfasqueda de Metadatos tiene que ser de mas de 3 caracteres\n');
                            return false;
		            }
                        }

                    }
                }
            }
            if (iniano > finano) {
                alert('El a\xf1o desde no puede ser mayor');
                document.Search.s_desde_ano.focus();
                return false;
            }
            if (iniano == finano) {
                xmes = finmes - inimes;
                //			alert(xmes+"="+inimes+">"+finmes);
                if (xmes < 0) {
                    alert('El mes desde no puede ser mayor');
                    document.Search.s_desde_mes.focus();
                    return false;
                }
                if (xmes == 0) {
                    
                            
                    if (inidia > findia) {
                        alert('El dia desde no puede ser mayor'+inidia+' > '+findia);
                        document.Search.s_desde_dia.focus();
                        return false;
                    }
                }
            }
            anoDif = finano - iniano;
            if (anoDif > 4) {
                alert('El limite de la b\xfasqueda es de 4 a\xf1os');
                return false;
            }
            if (anoDif == 4)
            {
                mesDif = finmes - inimes;
                if (mesDif > 1) {
                    alert('El limite de la b\xfasqueda es de 1 a\xf1o');
                    return false;
                }
                if (mesDif == 0) {
                    diaDif = findia - inidia;
                    //alert(diaDif);
                    if (diaDif >= 1) {
                        alert('El limite de la b\xfasqueda es de 1 a\xf1o');
                        return false;
                    }
                }
            }

            if (tipo == "Procesando") {
                document.getElementById('botonBusqueda').innerHTML = '<input class="botones" type="button" onClick="proceso();" name=Busqueda value="B&uacute;squeda">';
                Consultar(iPage);
            }
            else {
                document.getElementById('botonBusqueda').innerHTML = '<input class="botones" type="button"  onClick="botonBusqueda(\'Procesando\',1);" name=Busqueda value="B&uacute;squeda">';
            }

        }
        function proceso() {
            alert("La b\xFAsqueda se encuentra en proceso");

        }

        function orderby(isort, type) {
            botonBusqueda('Procesando');
            EXT = document.getElementById('ext').value;
            document.getElementById('contenido').innerHTML = '<center><img  alt="Procesando" src="../imagenes/loading.gif"></center>';
            radicado = document.getElementById('s_RADI_NUME_RADI').value;
            documento = document.getElementById('s_DOCTO').value;
            expediente = document.getElementById('s_SGD_EXP_SUBEXPEDIENTE').value;
            tipo = document.getElementById('s_entrada').value;
            busqueda = document.getElementById('s_RADI_NOMB').value;
            CriterioBusq = document.getElementById('CriterioBusq').value;
            inidia = document.getElementById('s_desde_dia').value;
            inimes = document.getElementById('s_desde_mes').value;
            iniano = document.getElementById('s_desde_ano').value;
            findia = document.getElementById('s_hasta_dia').value;
            finmes = document.getElementById('s_hasta_mes').value;
            finano = document.getElementById('s_hasta_ano').value;
            trpd = document.getElementById('s_TDOC_CODI').value;
            depe = document.getElementById('s_RADI_DEPE_ACTU').value;
            var orden = type;
            var isort2 = isort;
            var poststr2 = "&EXT=" + EXT + "&s_RADI_NUME_RADI=" + radicado + "&s_DOCTO=" + documento + "&s_SGD_EXP_SUBEXPEDIENTE=" + expediente + "&s_entrada=" + tipo + "&s_RADI_NOMB=" + busqueda + "&CriterioBusq=" + CriterioBusq + "&s_desde_dia=" + inidia + "&s_desde_mes=" + inimes + "&s_desde_ano=" + iniano + "&s_hasta_dia=" + findia + "&s_hasta_mes=" + finmes + "&s_hasta_ano=" + finano + "&s_TDOC_CODI=" + trpd + "&s_RADI_DEPE_ACTU=" + depe + "&indiVinculo=<?php echo  $_GET['indiVinculo']; ?>&etapa=<?php echo  $_GET['etapa']; ?>&FormCIUDADANO_Sorting=" + isort2 + "&orden=" + orden;
            var poststr = "<?php  echo session_name() . "=" . session_id() . '&dependencia=' . $dependencia . '&krd=' . $krd; ?>" + poststr2;
            divajax('../busqueda/ResultadoBusqueda.php', poststr, '', 'contenido');
            botonBusqueda('Proces');

        }


        function entsub(event, ourform) {
            if (event && event.which == 13)
                botonBusqueda('Procesando', 1);
            else
                return true;
        }
        /*	function entsub(event,ourform) {
         if (event && event.which == 13)
         ourform.submit();
         else
         return true;}
         */
    </script>
    <body class="PageBODY" marginheight="1" marginwidth="2" >
        <form method="post" onSubmit="botonBusqueda('Procesando', 1);
            return false" action="#submit" name="Search">
            <table border=0 width="*" >
                <tr>
                    <td valign="top" width="90%">
                        <table border=0 width="100%" cellpadding=0 cellspacing=2 class='borde_tab'>
                            <tr><td class="titulos4" colspan="4">
                                    <table  cellpadding=0 cellspacing=0 border=0 width="100%" class="titulos4">
                                        <tr><td>		
<?php echo  $sFormTitle ?>      
                                            </td>
                                            <!--  <?php if ($indiVinculo != 2 && $indiVinculo != 1) { ?><td class="titulos4"   align="right"><a class="titulos4"href="busquedaHist.php?<?php echo  session_name() . "=" . session_id() . "&fechah=$fechah&krd=$krd" ?>">
                                                             Busqueda por historico</a>
                                                          <a  class="titulos4"
                                                                  href="busquedaUsuActu.php?<?php echo  session_name() . "=" . session_id() . "&fechah=$fechah&krd=$krd" ?>">Reporte
                                                          por Usuarios</a>
                                                          <a class="titulos4" href="../busqueda/busquedaExp.php?<?php echo  $phpsession ?>&krd=<?php echo  $krd ?>&<?php 
                                            ECHO "&fechah=$fechah&primera=1&ent=2";
                                            ?>">Busqueda
                                                          Expediente por </a></td><?php } ?>-->
                                        </tr>    </table>
                                </td>
                            </tr>
                            <tr>
                                <td width="20%" class="titulos5">Radicado</td>
                                <td width="19%" class="listado5">
                                    <input  class="tex_area" type="text" onkeypress="return entsub(event, this.form)" name="s_RADI_NUME_RADI" tooltipText="La Busqueda Es Mas Efectiva Con El Numero De Radicado Completo" id="s_RADI_NUME_RADI" maxlength=""	 size="">
                                </td>

                                <td width="24%" class="titulos5">Buscar en Radicados de</td>
                                <td width="37%" class="listado5">
                                    <select class="select" name="s_entrada" id="s_entrada">
                                        <?php 
                                        echo "<option value=\"9999\">" . $ss_TRAD_CODIDisplayValue . "</option>";
                                        $lookup_s_entrada = db_fill_array("select SGD_TRAD_CODIGO, SGD_TRAD_DESCR from SGD_TRAD_TIPORAD order by 2");
                                        if (is_array($lookup_s_entrada)) {
                                            reset($lookup_s_entrada);
                                            while (list ( $key, $value ) = each($lookup_s_entrada)) {
                                                if ($key == $flds_entrada)
                                                    $option = "<option SELECTED value=\"$key\">$value</option>";
                                                else
                                                    $option = "<option value=\"$key\">$value</option>";
                                                echo $option;
                                            }
                                        }
                                        ?>
                                    </select></td>
                            </tr>
                            <tr>
                                <td class="titulos5" > 
                                    <table width="100%" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td class="titulos5">Expediente por</td>
                                            <td align="right">
                                                <select  class="select" name="CriterioExp" id="CriterioExp">
                                                    <option value="1">N&uacute;mero</option>
                                                    <!-- <option value="2">Nombre</option>-->
                                                    <option value="3" >Titulo</option>
                                                </select>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="listado5">
                                    <input onkeypress="return entsub(event, this.form)" class="tex_area" type="text" tooltipText="Recuerde Seleccionar El Modo De Busqueda del Expediente"	name="s_SGD_EXP_SUBEXPEDIENTE" id="s_SGD_EXP_SUBEXPEDIENTE" maxlength="" size="">
                                </td>
                                <td class="titulos5">Desde Fecha (dd/mm/yyyy)</td>
                                <td class="listado5">
                                    <select class="select" name="s_desde_dia" id="s_desde_dia">
                                        <?php 
                                        for ($i = 1; $i <= 31; $i++) {
                                            if ($i == $flds_desde_dia)
                                                $option = "<option SELECTED value=\"" . $i . "\">" . $i . "</option>";
                                            else
                                                $option = "<option value=\"" . $i . "\">" . $i . "</option>";
                                            echo $option;
                                        }
                                        ?>
                                    </select> <select class="select" name="s_desde_mes" id="s_desde_mes">
                                        <?php 
                                        for ($i = 1; $i <= 12; $i++) {
                                            if ($i == $flds_desde_mes)
                                                $option = "<option SELECTED value=\"" . $i . "\">" . $i . "</option>";
                                            else
                                                $option = "<option value=\"" . $i . "\">" . $i . "</option>";
                                            echo $option;
                                        }
                                        ?>
                                    </select> <select class="select" name="s_desde_ano" id="s_desde_ano" >
                                        <?php 
                                        $agnoactual = Date('Y');
                                        for ($i = 1990; $i <= $agnoactual; $i++) {
                                            if ($i == $flds_desde_ano)
                                                $option = "<option SELECTED value=\"" . $i . "\">" . $i . "</option>";
                                            else
                                                $option = "<option value=\"" . $i . "\">" . $i . "</option>";
                                            echo $option;
                                        }
                                        ?>
                                    </select></td>
                            </tr>
                            <tr>
                                <td class="titulos5">Identificaci&oacute;n (T.I.,C.C.,Nit) </td>
                                <td class="listado5"><input onkeypress="return entsub(event, this.form)" class="tex_area" type="text"	name="s_DOCTO" id="s_DOCTO" maxlength="" size="">
                                </td>
                                <!--    </tr>
    <tr>-->
                                <td class="titulos5">Hasta Fecha (dd/mm/yyyy)</td>
                                <td class="listado5"><select class="select" name="s_hasta_dia"  id="s_hasta_dia">
                                        <?php 
                                        for ($i = 1; $i <= 31; $i++) {
                                            if ($i == $flds_hasta_dia)
                                                $option = "<option SELECTED value=\"" . $i . "\">" . $i . "</option>";
                                            else
                                                $option = "<option value=\"" . $i . "\">" . $i . "</option>";
                                            echo $option;
                                        }
                                        ?>
                                    </select> <select class="select" name="s_hasta_mes" id="s_hasta_mes">
                                        <?php 
                                        for ($i = 1; $i <= 12; $i++) {
                                            if ($i == $flds_hasta_mes)
                                                $option = "<option SELECTED value=\"" . $i . "\">" . $i . "</option>";
                                            else
                                                $option = "<option value=\"" . $i . "\">" . $i . "</option>";
                                            echo $option;
                                        }
                                        ?>
                                    </select> <select class="select" name="s_hasta_ano" id="s_hasta_ano">
                                        <?php 
                                        for ($i = 1990; $i <= $agnoactual; $i++) {
                                            if ($i == $flds_hasta_ano)
                                                $option = "<option SELECTED value=\"" . $i . "\">" . $i . "</option>";
                                            else
                                                $option = "<option value=\"" . $i . "\">" . $i . "</option>";
                                            echo $option;
                                        }
                                        ?>
                                    </select></td>
                            </tr>
                            <tr>
<!-- 				<td class="titulos5"> Buscar Por
           </td>-->
                                <td class="titulos5">Buscar Por </td>
                                <td class="listado5">
                                    <select  class="select" name="CriterioBusq" id="CriterioBusq">
                                        <!--<option value="10">Id Kogui</option>-->
                                        <option value="1">Asunto</option>
                                        <option value="2">Direcci&oacute;n</option>
                                        <option value="3">Telefono</option>
                                        <option value="4">Ciudadanos</option>
                                        <option value="5">Entidades</option>
                                        <option value="6">Empresas</option>
                                        <option value="7">Funcionarios</option>
                                        <option value="8">Cuenta Interna</option>
                                        <option value="9">Nombre</option>
                                        <option value="11">Guia</option>
                                    </select>				
                                </td>
                                <td class="listado5" colspan="2">

                                    <input class="tex_area" onkeypress="return entsub(event, this.form)" type="text" tooltipText="Recuerde Seleccionar El Criterio De Busqueda"	name="s_RADI_NOMB" id="s_RADI_NOMB" maxlength="70" size="70"> </td>

                            </tr>
			    <tr style='display:<?php echo $dis?>'>
				<td class='titulos5'>Metadatos</td>
				<td class='listado5'><?php echo $metabusq?></td>
				<td class='titulos5'><div id='tagm'></div></td>
				<td class='listado5'><div id='tagn'></div></td>
			    </tr>
                            <tr>
                                <td class="titulos5"><font class="FieldCaptionFONT">Tipo de	Documento</td>
                                <td class="listado5" colspan="3">
                                    <select class="select"	name="s_TDOC_CODI" id="s_TDOC_CODI">
                                        <?php 
                                        if ($flds_TDOC_CODI == 0)
                                            $flds_TDOC_CODI = "9999";
                                        echo "<option value=\"9999\">" . $ss_TDOC_CODIDisplayValue . "</option>";
                                        $lookup_s_TDOC_CODI = db_fill_array("select SGD_TPR_CODIGO, SGD_TPR_DESCRIP from SGD_TPR_TPDCUMENTO order by 2");

                                        if (is_array($lookup_s_TDOC_CODI)) {
                                            reset($lookup_s_TDOC_CODI);
                                            while (list ( $key, $value ) = each($lookup_s_TDOC_CODI)) {
                                                if ($key == $flds_TDOC_CODI)
                                                    $option = "<option SELECTED value=\"$key\">$value</option>";
                                                else
                                                    $option = "<option value=\"$key\">$value</option>";
                                                echo $option;
                                            }
                                        }
                                        ?>
                                    </select></td>
                            </tr>
                            <tr>
                                <td class="titulos5">Dependencia Actual</td>
                                <td class="listado5" colspan="3">
                                    <select class="select" name="s_RADI_DEPE_ACTU" id="s_RADI_DEPE_ACTU">
                                        <?php 
                                        $l = strlen($flds_RADI_DEPE_ACTU);

                                        if ($l == 0) {
                                            echo "<option value=\"\" SELECTED>" . $ss_RADI_DEPE_ACTUDisplayValue . "</option>";
                                        } else {
                                            echo "<option value=\"\">" . $ss_RADI_DEPE_ACTUDisplayValue . "</option>";
                                        }
                                        $lookup_s_RADI_DEPE_ACTU = db_fill_array("select DEPE_CODI, DEPE_NOMB from DEPENDENCIA order by 2");

                                        if (is_array($lookup_s_RADI_DEPE_ACTU)) {
                                            reset($lookup_s_RADI_DEPE_ACTU);
                                            while (list ( $key, $value ) = each($lookup_s_RADI_DEPE_ACTU)) {
                                                if ($l > 0 && $key == $flds_RADI_DEPE_ACTU)
                                                    $option = "<option SELECTED value=\"$key\">$value</option>";
                                                else
                                                    $option = "<option value=\"$key\">$value</option>";
                                                echo $option;
                                            }
                                        }
                                        ?>
                                    </select></td>
                            </tr>
                            <tr>
                                <td align="left" class="titulos5" >		
                                    Paginaci&oacute;n Multiples Pesta&ntilde;as 
                                </td><td class="titulos5">
                                    <select class="select" name="ext" id="ext">
                                        <option value="Y">Si</option> 
                                        <option selected value="N">No</option>
                                    </select>
                                </td>
                                <td align="right"  class="titulos5" colspan=4>
                                    <table><tr>
                                            <td align="right">
                                                <input class="botones" type="button" value="Limpiar" onClick="limpiar();"></td><td>
                                                <div id="botonBusqueda"><input class="botones" type="button" onClick="botonBusqueda('Procesando', 1);"
                                                                               name=Busqueda value="B&uacute;squeda"></div>
                                            </td></tr></table>

                                </td>
                            </tr>
                        </table>
                    </td><!--<td>
                        <a class="vinculos" href="../busqueda/busExpA.php">
                            Búsqueda de Expedientes
                        </a>

                    </td>-->

                </tr>
            </table>
        </form>
        <div id='contenido' ></div>
        <?php 
        //Contabilizamos tiempo final
        /* $time_end = microtime_float ();
          $time = $time_end - $time_start;
          echo "<span class='info'>";
          echo "<br><b>$time = $time_end - $time_start</b>";
          echo "<br><b>Se demor&oacute;: $time segundos la Operaci&oacute;n total.</b>";
          echo "</span>"; */
        ?>	
        <script type="text/javascript">
        var tooltipObj = new DHTMLgoodies_formTooltip('../imagenes/');
        tooltipObj.setTooltipPosition('right');
        tooltipObj.setPageBgColor('#EEEEEE');
        tooltipObj.setTooltipCornerSize(15);
        tooltipObj.initFormFieldTooltip();
        </script>
    </body>
</html>