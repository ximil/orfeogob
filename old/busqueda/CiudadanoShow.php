<?php //===============================
// Display Grid Form
//-------------------------------
function Ciudadano_show($nivelus, $tpRemDes, $whereFlds) {
	//-------------------------------
	// Initialize variables
	//-------------------------------


	global $db2;
	global $db3;
	global $sRADICADOErr;
	global $sFileName;
	global $styles;
	global $ruta_raiz;
	global $time_start;
	global $url_orfeo_power_tools;
	$sWhere = "";
	$sOrder = "";
	$sSQL = "";
	$db = new ConnectionHandler ( $ruta_raiz );
	if ($tpRemDes == 1) {
		$tpRemDesNombre = "Por Ciudadano";
	}
	if ($tpRemDes == 2) {
		$tpRemDesNombre = "Por Otras Empresas";
	}
	if ($tpRemDes == 3) {
		$tpRemDesNombre = "Por Esp";
	}
	if ($tpRemDes == 4) {
		$tpRemDesNombre = "Por Funcionario";
	}
	if ($tpRemDes == 9) {
		$tpRemDesNombre = "";
		$whereTrd = "   ";
	} else {
		$whereTrd = " and dir.sgd_trd_codigo = $whereFlds  ";
	}
	if ($indiVinculo == 2) {
		$sFormTitle = "Expedientes encontrados $tpRemDesNombre";
	} else {
		$sFormTitle = "Radicados encontrados $tpRemDesNombre";
	}
	$HasParam = false;
	/*
   * Modificacion Numero de paginas a reflejar en la consulta
   */
	$time_start = microtime_float ();
	$iRecordsPerPage = 25;
	$iCounter = 0;
	$iPage = 0;
	$bEof = false;
	$iSort = "";
	$iSorted = "";
	$sDirection = "";
	$sSortParams = "";
	$iTmpI = 0;
	$iTmpJ = 0;
	$sCountSQL = "";

	$transit_params = "";
	//Proceso de Vinculacion documentos
	$indiVinculo = get_param ( "indiVinculo" );
	$verrad = get_param ( "verrad" );
	$carpeAnt = get_param ( "carpeAnt" );
	$nomcarpeta = get_param ( "nomcarpeta" );
	//
	if($_SESSION['usua_debug']==5)
        $db->conn->debug=true;


	//-------------------------------
	// Build ORDER BY statement
	//-------------------------------
	//$sOrder = " order by r.RADI_NUME_RADI ";
	//print_r($_POST);
	$sOrder = " order by radi_fech_radi ";
	$iSort = get_param ( "FormCIUDADANO_Sorting" );
	$iSorted = get_param ( "FormCIUDADANO_Sorted" );
	$orden = get_param ( "orden" );
	//$iSort = get_param("isort");
	$krd = get_param ( "krd" );
	$form_params = trim ( session_name () ) . "=" . trim ( session_id () ) . "&krd=$krd&verrad=$verrad&indiVinculo=$indiVinculo&carpeAnt=$carpeAnt&nomcarpeta=$nomcarpeta&s_RADI_DEPE_ACTU=" . tourl ( get_param ( "s_RADI_DEPE_ACTU" ) ) . "&s_RADI_NOMB=" . tourl ( get_param ( "s_RADI_NOMB" ) ) . "&s_RADI_NUME_RADI=" . tourl ( get_param ( "s_RADI_NUME_RADI" ) ) . "&s_TDOC_CODI=" . tourl ( get_param ( "s_TDOC_CODI" ) ) . "&s_desde_dia=" . tourl ( get_param ( "s_desde_dia" ) ) . "&s_desde_mes=" . tourl ( get_param ( "s_desde_mes" ) ) . "&s_desde_ano=" . tourl ( get_param ( "s_desde_ano" ) ) . "&s_hasta_dia=" . tourl ( get_param ( "s_hasta_dia" ) ) . "&s_hasta_mes=" . tourl ( get_param ( "s_hasta_mes" ) ) . "&s_hasta_ano=" . tourl ( get_param ( "s_hasta_ano" ) ) . "&s_solo_nomb=" . tourl ( get_param ( "s_solo_nomb" ) ) . "&s_ciudadano=" . tourl ( get_param ( "s_ciudadano" ) ) . "&s_empresaESP=" . tourl ( get_param ( "s_empresaESP" ) ) . "&s_oEmpresa=" . tourl ( get_param ( "s_oEmpresa" ) ) . "&s_FUNCIONARIO=" . tourl ( get_param ( "s_FUNCIONARIO" ) ) . "&s_entrada=" . tourl ( get_param ( "s_entrada" ) ) . "&s_salida=" . tourl ( get_param ( "s_salida" ) ) . "&nivelus=$nivelus&s_Listado=" . get_param ( "s_Listado" ) . "&s_SGD_EXP_SUBEXPEDIENTE=" . get_param ( "s_SGD_EXP_SUBEXPEDIENTE" ) . "&";
	// s_Listado s_ciudadano s_empresaESP s_FUNCIONARIO
	if (! $iSort) {
		$form_sorting = "ASC";
	} else {
		if ($orden == "DESC") {
			$form_sorting = "ASC";
			$sDirection = " DESC ";
			$sSortParams = "FormCIUDADANO_Sorting=" . $iSort . "&FormCIUDADANO_Sorted=" . $iSort . "&";
		} else {
			$form_sorting = "DESC";
			$sDirection = " ASC ";
			$sSortParams = "FormCIUDADANO_Sorting=" . $iSort . "&FormCIUDADANO_Sorted=" . "&";
		}
		switch ($iSort) {
			case 1 :
				$sOrder = " order by r.radi_nume_radi" . $sDirection;
				break;
			case 2 :
				$sOrder = " order by radi_fech_radi" . $sDirection;
				break;
			case 3 :
				$sOrder = " order by r.ra_asun" . $sDirection;
				break;
			case 4 :
				$sOrder = " order by td.sgd_tpr_descrip" . $sDirection;
				break;
			case 5 :
				$sOrder = " order by r.radi_nume_folios" . $sDirection;
				break;
			case 6 :
				$sOrder = " order by dir.sgd_dir_direccion" . $sDirection;
				break;
			case 7 :
				$sOrder = " order by dir.sgd_dir_telefono" . $sDirection;
				break;
			case 8 :
				$sOrder = " order by dir.sgd_dir_mail" . $sDirection;
				break;
			case 9 :
				$sOrder = " order by dir.sgd_dir_nombre" . $sDirection;
				break;
			case 12 :
				$sOrder = " order by dir.sgd_dir_telefono" . $sDirection;
				break;
			case 13 :
				$sOrder = " order by dir.sgd_dir_direccion" . $sDirection;
				break;
			case 14 :
				$sOrder = " order by dir.sgd_dir_doc" . $sDirection;
				break;
			case 17 :
				$sOrder = " order by r.radi_usua_actu" . $sDirection;
				break;
			case 20 :
				$sOrder = " order by r.radi_pais" . $sDirection;
				break;
			case 21 :
				$sOrder = " order by diasr" . $sDirection;
				break;
			case 22 :
				$sOrder = " order by dir.sgd_dir_nombre" . $sDirection;
				break;
			case 23 :
				$sOrder = " order by dir.sgd_dir_nombre" . $sDirection;
				break;
			case 24 :
				$sOrder = " order by dir.sgd_dir_nombre" . $sDirection;
				break;
		}
	}

	//-------------------------------
	// Encabezados HTML de las Columnas
	//-------------------------------


	if ($indiVinculo != 2) {
		?>
<table width="2000" border=0 cellpadding=0 cellspacing=0 class='borde_tab'>
<!--<table  border=0 cellpadding=0 cellspacing=0 class='borde_tab'>-->
<?php
	} else {
		?>
<table width="200" border=0 cellpadding=0 cellspacing=0	class='borde_tab'>
<?php
	}
	?>
	<tr>
			<td class="titulos4" colspan="20"><script>
	     botonBusqueda('procesando');
        </script> <a name="RADICADO"><?php echo $sFormTitle?> - Pagina <?php echo $_GET['FormCIUDADANO_Page'];?></a></td>
		</tr>
		<tr>
	<?php
	if ($indiVinculo >= 1) {
		?>
		<td class="titulos5" align="center"><font class="ColumnFONT">
			Acci&oacute;n</td>
	<?php
	}

	if ($indiVinculo != 2) {
		?>
	    <td class="titulos5"><a class="vinculos" href="#"
				onclick="orderby(1,'<?php echo $form_sorting;?>')">Radicado</a></td>
			<td class="titulos5"><a class="vinculos" href="#"
				onclick="orderby(2,'<?php echo $form_sorting;?>')">Fecha Radicacion</a></td>
			<td class="titulos5"><font class="ColumnFONT">Expediente</td>
        <?php
	} else {
		?>
	    <td class="titulos5"><font class="ColumnFONT">Expediente</td>
			<td class="titulos5"><a class="vinculos" href="#"
				onclick="orderby(1,'<?php echo $form_sorting;?>')">Radicado vinculado al
			expediente</a></td>
			<td class="titulos5"><a class="vinculos" href="#"
				onclick="orderby(2,'<?php echo $form_sorting;?>')">Fecha Radicacion</a></td>
          <?php
	}
	?>
     		<td class="titulos5"><a class="vinculos" href="#"
				>Num. Interno</a></td>
		<td class="titulos5"><a class="vinculos" href="#"
				onclick="orderby(3,'<?php echo $form_sorting;?>')">Asunto</a></td>
			<td class="titulos5"><a class="vinculos" href="#"
				onclick="orderby(4,'<?php echo $form_sorting;?>')">Tipo de Documento</a></td>
			<td class="titulos5"><font class="ColumnFONT">Tipo</td>
			<td class="titulos5"><a class="vinculos" href="#"
				onclick="orderby(5,'<?php echo $form_sorting;?>')">Folios Digitalizados</a></td>
			<td class="titulos5"><a class="vinculos" href="#"
				onclick="orderby(6,'<?php echo $form_sorting;?>')">Direccion contacto</a></td>
			<td class="titulos5"><a class="vinculos" href="#"
				onclick="orderby(7,'<?php echo $form_sorting;?>')">Telefono contacto</a></td>
			<td class="titulos5"><a class="vinculos" href="#"
				onclick="orderby(8,'<?php echo $form_sorting;?>')">Mail Contacto</a></td>
			<td class="titulos5"><a class="vinculos" href="#"
				onclick="orderby(9,'<?php echo $form_sorting;?>')">Dignatario</a></td>
			<td class="titulos5"><a class="vinculos" href="#"
				onclick="orderby(9,'<?php echo $form_sorting;?>')">Nombre </a></td>
			<td class="titulos5"><a class="vinculos" href="#"
				onclick="orderby(14,'<?php echo $form_sorting;?>')">Documento</a></td>
			<td class="titulos5"><a class="vinculos" href="#"
				onclick="orderby(17,'<?php echo $form_sorting;?>')">Usuario Actual</a></td>
			<td class="titulos5"><font class="ColumnFONT">Dependencia Actual</td>
			<td class="titulos5"><font class="ColumnFONT">Usuario Anterior</td>
			<td class="titulos5"><a class="vinculos" href="#"
				onclick="orderby(20,'<?php echo $form_sorting;?>')">Pais</a></td>
			<td class="titulos5"><a class="vinculos" href="#"
				onclick="orderby(21,'<?php echo $form_sorting;?>')">Dias Restantes</a></td>

		</tr>
<?php

	//-------------------------------
	// Build WHERE statement
	//-------------------------------
	// Se crea la $ps_desde_RADI_FECH_RADI con los datos ingresados.
	//------------------------------------

        $ps_RADI_NUME_RADI = get_param ( "s_RADI_NUME_RADI" );
        $trimps_RADI_NUME_RADI = trim($ps_RADI_NUME_RADI,' ');
        $ps_SGD_EXP_SUBEXPEDIENTE = get_param ( "s_SGD_EXP_SUBEXPEDIENTE" );
        $trimps_SGD_EXP_SUBEXPEDIENTE = trim($ps_SGD_EXP_SUBEXPEDIENTE,' ');
	$cri=get_param ("CriterioExp");
	$metadata=get_param("metadata");
	if($metadata!=0){
	    $metaTag=get_param("metaTag");
	    $tag=get_param("tag");
	    if(strlen(trim($metaTag))>3){
		$meta_sql="select c.radi_cam_nombre,m.radi_data_nomb, case when c.radi_cam_tipo='N' then 0 else 1 end tipo
			from RADI_CAM_METACAMPOS c, radi_data_metainfo m where c.RADI_DATA_ID=$metadata and radi_cam_id=$tag and c.radi_data_id=m.RADI_DATA_ID";
		$rs=$db->conn->Execute($meta_sql);
		if(!$rs->EOF){
		    $meta_tipo=$rs->fields['TIPO'];
		    $meta_tabla=$rs->fields['RADI_DATA_NOMB'];
		    $meta_columna=$rs->fields['RADI_CAM_NOMBRE'];
		}
	    }
	}
        if ((strlen ($trimps_RADI_NUME_RADI )== 14 && is_numeric($ps_RADI_NUME_RADI)) ||
                (strlen($trimps_SGD_EXP_SUBEXPEDIENTE))== 17 && substr($trimps_SGD_EXP_SUBEXPEDIENTE,-1) =='E' &&
                is_numeric(substr($trimps_SGD_EXP_SUBEXPEDIENTE,0,-1)) || ($cri==3 && strlen($trimps_SGD_EXP_SUBEXPEDIENTE)>5)){

        }
        else{
	$ps_desde_RADI_FECH_RADI = mktime ( 0, 0, 0, get_param ( "s_desde_mes" ), get_param ( "s_desde_dia" ), get_param ( "s_desde_ano" ) );
	$ps_hasta_RADI_FECH_RADI = mktime ( 23, 59, 59, get_param ( "s_hasta_mes" ), get_param ( "s_hasta_dia" ), get_param ( "s_hasta_ano" ) );

	if (strlen ( $ps_desde_RADI_FECH_RADI ) && strlen ( $ps_hasta_RADI_FECH_RADI )) {
		$HasParam = true;
		$desdef= $db->conn->DBTimeStamp ( $ps_desde_RADI_FECH_RADI );

		$hastaf=$db->conn->DBTimeStamp ( $ps_hasta_RADI_FECH_RADI );
		$sWhere =$sWhere ."r.radi_fech_radi between $desdef and $hastaf ";
            }
        }

	/* Se recibe la dependencia actual para bsqueda */
	$ps_RADI_DEPE_ACTU = get_param ( "s_RADI_DEPE_ACTU" );
	if (is_number ( $ps_RADI_DEPE_ACTU ) && strlen ( $ps_RADI_DEPE_ACTU ))
		$ps_RADI_DEPE_ACTU = tosql ( $ps_RADI_DEPE_ACTU, "Number" );
	else
		$ps_RADI_DEPE_ACTU = "";

	if (strlen ( $ps_RADI_DEPE_ACTU )) {
		if ($sWhere != "")
			$sWhere .= " and ";
		$HasParam = true;
		$sWhere = $sWhere . "r.radi_depe_actu=" . $ps_RADI_DEPE_ACTU;
	}

	/* Se recibe el nmero del radicado para bsqueda */

 	//$ps_RADI_NUME_RADI = get_param ( "s_RADI_NUME_RADI" );
	$ps_DOCTO = get_param ( "s_DOCTO" );
	if (strlen ( $ps_RADI_NUME_RADI )) {
		if ($sWhere != "")
			$sWhere .= " and ";
		$HasParam = true;
		if (strlen ( $ps_RADI_NUME_RADI ) >= 14) {
			$sWhere = $sWhere . "r.radi_nume_radi=" . tosql ( trim ( $ps_RADI_NUME_RADI ), "Text" );
		} else {
			$sWhere = $sWhere . "cast(r.radi_nume_radi as varchar(15)) like " . tosql ( "%" . trim ( $ps_RADI_NUME_RADI ) . "%", "Text" );
		}
	}

	if (strlen ( $ps_DOCTO )) {
		if ($sWhere != "")
			$sWhere .= " and ";
		$HasParam = true;
		$sWhere = $sWhere . " dir.SGD_DIR_DOC = '$ps_DOCTO' ";
	}

	/**
	 * Se recibe el nmero del expediente para busqueda
	 * Fecha de modificacin: 30-Junio-2006
	 * Modificador: Supersolidaria
	 */
	//$ps_SGD_EXP_SUBEXPEDIENTE = get_param ( "s_SGD_EXP_SUBEXPEDIENTE" );
	if (strlen ( $ps_SGD_EXP_SUBEXPEDIENTE ) != 0) {
		if ($sWhere != "") {
			$sWhere .= " and ";
		}
		$HasParam = true;
		$sWhere = $sWhere . " R.RADI_NUME_RADI = EXP.RADI_NUME_RADI";
		$sWhere = $sWhere . " AND EXP.SGD_EXP_NUMERO = SEXP.SGD_EXP_NUMERO";
		/**
		 * No se tienen en cuenta los radicados que han sido excluidos de un expediente.
		 * Fecha de modificacion: 12-Septiembre-2006
		 * Modificador: Supersolidaria
		 */
		$sWhere = $sWhere . " AND EXP.SGD_EXP_ESTADO <> 2";
		switch (get_param ( "CriterioExp" )) {
			case 1 :
				//      		echo strlen($ps_SGD_EXP_SUBEXPEDIENTE);
				if (strlen ( $ps_SGD_EXP_SUBEXPEDIENTE ) != 17) {
					$sWhere = $sWhere . " AND ( EXP.SGD_EXP_NUMERO LIKE '%" . strtoupper ( str_replace ( '\'', '', tosql ( trim ( $ps_SGD_EXP_SUBEXPEDIENTE ), "Text" ) ) ) . "%'";
				} else {
					$sWhere = $sWhere . " AND ( EXP.SGD_EXP_NUMERO = '" . strtoupper ( str_replace ( '\'', '', tosql ( trim ( $ps_SGD_EXP_SUBEXPEDIENTE ), "Text" ) ) ) . "'";
				}
				break;
			case 2 :
				$sWhere = $sWhere . " AND ( SEXP.SGD_SEXP_PAREXP1 LIKE  '%" . strtoupper ( str_replace ( '\'', '', tosql ( trim ( $ps_SGD_EXP_SUBEXPEDIENTE ), "Text" ) ) ) . "%' ";
				$sWhere = $sWhere . " OR SEXP.SGD_SEXP_PAREXP2 LIKE  '%" . strtoupper ( str_replace ( '\'', '', tosql ( trim ( $ps_SGD_EXP_SUBEXPEDIENTE ), "Text" ) ) ) . "%' ";
				$sWhere = $sWhere . " OR SEXP.SGD_SEXP_PAREXP3 LIKE  '%" . strtoupper ( str_replace ( '\'', '', tosql ( trim ( $ps_SGD_EXP_SUBEXPEDIENTE ), "Text" ) ) ) . "%' ";
				$sWhere = $sWhere . " OR SEXP.SGD_SEXP_PAREXP4 LIKE '%" . strtoupper ( str_replace ( '\'', '', tosql ( trim ( $ps_SGD_EXP_SUBEXPEDIENTE ), "Text" ) ) ) . "%' ";
				$sWhere = $sWhere . " OR SEXP.SGD_SEXP_PAREXP5 LIKE '%" . strtoupper ( str_replace ( '\'', '', tosql ( trim ( $ps_SGD_EXP_SUBEXPEDIENTE ), "Text" ) ) ) . "%' ";
				break;
			case 3 :
				$sWhere = $sWhere . " AND ( upper(EXP.SGD_EXP_TITULO) LIKE upper('%" . strtoupper ( str_replace ( '\'', '', tosql ( trim ( $ps_SGD_EXP_SUBEXPEDIENTE ), "Text" ) ) ) . "%')";
				break;
		}
		$sWhere = $sWhere . " )";
	}

	/* Se decide si busca en radicado de entrada o de salida o ambos */
	$ps_entrada = strip ( get_param ( "s_entrada" ) );
	$eLen = strlen ( $ps_entrada );
	$ps_salida = strip ( get_param ( "s_salida" ) );
	$sLen = strlen ( $ps_salida );

	if ($ps_entrada != "9999") {
		if ($sWhere != "")
			$sWhere .= " and ";
		$HasParam = true;
		$sWhere = $sWhere . "(substring(cast(r.radi_nume_radi as varchar(15)) from 14 for 1)  like " . tosql ( "%" . trim ( $ps_entrada ), "Text" ) . ")";
	}

	/* Se recibe el tipo de documento para la busqueda */

	$ps_TDOC_CODI = get_param ( "s_TDOC_CODI" );
	if (is_number ( $ps_TDOC_CODI ) && strlen ( $ps_TDOC_CODI ) && $ps_TDOC_CODI != "9999")
		$ps_TDOC_CODI = tosql ( $ps_TDOC_CODI, "Number" );
	else
		$ps_TDOC_CODI = "";
	if (strlen ( $ps_TDOC_CODI )) {
		if ($sWhere != "")
			$sWhere .= " and ";

		$HasParam = true;
		$sWhere = $sWhere . "r.tdoc_codi=" . $ps_TDOC_CODI;
	}

	/* Se recibe la cadena a buscar y el tipo de busqueda (All) (Any) */
	$Criterio_busq =utf8_decode( get_param ( "CriterioBusq" ));
	$ps_RADI_NOMB = strip ( get_param ( "s_RADI_NOMB" ) );
	$ps_solo_nomb = get_param ( "s_solo_nomb" );
	$yaentro = false;
	if (strlen ( $ps_RADI_NOMB )) //&& $ps_solo_nomb == "Any")
{
		if ($sWhere != "")
			$sWhere .= " and (";
		$HasParam = true;
		$sWhere .= " ";

		if ($Criterio_busq == 4 || $Criterio_busq == 5 || $Criterio_busq == 6 || $Criterio_busq == 7 || $Criterio_busq == 9) {
			$ps_RADI_NOMB = strtoupper ( $ps_RADI_NOMB );
			$tok = strtok ( $ps_RADI_NOMB, " " );
			$sWhere .= "(";
			while ( $tok ) {
				$sWhere .= "";
				if ($yaentro == true) {
					$sWhere .= " and ";
				}
				$sWhere .= "UPPER(dir.sgd_dir_nomremdes) LIKE upper ('%" .  $tok  . "%') ";
				$tok = strtok ( " " );
				$yaentro = true;
			}
			$sWhere .= ") or (";
			$tok = strtok ( $ps_RADI_NOMB, " " );
			$yaentro = false;
			while ( $tok ) {
				$sWhere .= "";
				if ($yaentro == true) {
					$sWhere .= " and ";
				}
				$sWhere .= "UPPER(dir.sgd_dir_nombre) LIKE upper('%" .   $tok  . "%') ";
				$tok = strtok ( " " );
				$yaentro = true;
			}
			$sWhere .= ")) ";
		}
		//,"r.radi_cuentai","dir.sgd_dir_telefono","dir.sgd_dir_direccion
		if ($Criterio_busq == 1) {
			$yaentro = false;
			$tok = strtok ( $ps_RADI_NOMB, " " );
			if ($yaentro == true)
				$sWhere .= " and (";
			$sWhere .= "UPPER(" . $db->conn->Concat ( "r.ra_asun" ) . ") LIKE upper('%" .$ps_RADI_NOMB. "%') ";
			$tok = strtok ( " " );
			if ($yaentro == true)
				$sWhere .= ")";
			$yaentro = true;
			$sWhere .= ")";
		}
		if ($Criterio_busq == 2) {
			$yaentro = false;
			$tok = strtok ( $ps_RADI_NOMB, " " );
			if ($yaentro == true)
				$sWhere .= " and (";
			$sWhere .= "UPPER(" . $db->conn->Concat ( "dir.sgd_dir_direccion" ) . ") LIKE '%" . strtoupper ( $ps_RADI_NOMB ) . "%' ";
			$tok = strtok ( " " );
			if ($yaentro == true)
				$sWhere .= ")";
			$yaentro = true;
			$sWhere .= ")";
		}
		if ($Criterio_busq == 3) {
			$yaentro = false;
			$tok = strtok ( $ps_RADI_NOMB, " " );
			if ($yaentro == true)
				$sWhere .= " and (";
			$sWhere .= "UPPER(" . $db->conn->Concat ( "dir.sgd_dir_telefono" ) . ") LIKE '%" . strtoupper ( $ps_RADI_NOMB ) . "%' ";
			$tok = strtok ( " " );
			if ($yaentro == true)
				$sWhere .= ")";
			$yaentro = true;
			$sWhere .= ")";
		}
		if ($Criterio_busq == 8) {
			$yaentro = false;
			$tok = strtok ( $ps_RADI_NOMB, " " );
			if ($yaentro == true)
				$sWhere .= " and (";
			$sWhere .= "UPPER(" . $db->conn->Concat ( "r.radi_cuentai" ) . ") LIKE  '%" . strtoupper ( $ps_RADI_NOMB ) . "%' ";
			$tok = strtok ( " " );
			if ($yaentro == true)
				$sWhere .= ")";
			$yaentro = true;
			$sWhere .= ")";
		}
                if ($Criterio_busq == 10) {
			$yaentro = false;
			$tok = strtok ( $ps_RADI_NOMB, " " );
			if ($yaentro == true)
				$sWhere .= " and (";
			$sWhere .= "r.radi_num_numinterno =  '$ps_RADI_NOMB' ";
			$tok = strtok ( " " );
			if ($yaentro == true)
				$sWhere .= ")";
			$yaentro = true;
			$sWhere .= ")";
		}
		if ($Criterio_busq == 11) {
			$yaentro = false;
			$tok = strtok ( $ps_RADI_NOMB, " " );
			if ($yaentro == true)
				$sWhere .= " and (";
			$sWhere .= "r.radi_nume_guia =  '$ps_RADI_NOMB' ";
			$tok = strtok ( " " );
			if ($yaentro == true)
				$sWhere .= ")";
			$yaentro = true;
			$sWhere .= ")";
	}
	}
	if ($HasParam)
		$sWhere = " AND (" . $sWhere . ") ";

	//-------------------------------
	// Build base SQL statement
	//-------------------------------


	require_once ("../include/query/busqueda/busquedaPiloto1.php");
	$sSQL = "SELECT " . $radi_nume_radi . " AS RADI_NUME_RADI, r.radi_num_numinterno as NUMERO_INTERNO,
		to_char(r.RADI_FECH_RADI,'YYYY-MM-DD hh:MI:ss') as RADI_FECH_RADI, r.RA_ASUN, td.sgd_tpr_descrip, " . $redondeo . " as diasr,
		r.RADI_NUME_FOLIOS, r.RADI_PATH, dir.SGD_DIR_DIRECCION, dir.SGD_DIR_MAIL,
		dir.SGD_DIR_NOMREMDES, dir.SGD_DIR_TELEFONO, dir.SGD_DIR_DIRECCION,
		dir.SGD_DIR_DOC, r.RADI_USU_ANTE, r.RADI_PAIS, dir.SGD_DIR_NOMBRE,
		dir.SGD_TRD_CODIGO, r.RADI_DEPE_ACTU, r.RADI_USUA_ACTU, r.CODI_NIVEL,r.SGD_SPUB_CODIGO,
		 r.radi_leido";//R.RADI_RESERVADO,

	/**
	 * Busqueda por parametro del expediente
	 * Fecha de modificacion: 11-Agosto-2006
	 * Modificador: Supersolidaria
	 */
	if (strlen ( $ps_SGD_EXP_SUBEXPEDIENTE ) != 0) {
		$sSQL .= " ,EXP.SGD_EXP_NUMERO";
	}

	$sSQL .= " FROM sgd_dir_drecciones dir, radicado r, sgd_tpr_tpdcumento td";
	/**
	 * Bsqueda por expediente
	 * Fecha de modificacion: 30-Junio-2006
	 * Modificador: Supersolidaria
	 */
	if (strlen ( $ps_SGD_EXP_SUBEXPEDIENTE ) != 0) {
		$sSQL .= ", SGD_EXP_EXPEDIENTE EXP, SGD_SEXP_SECEXPEDIENTES SEXP";
	}
	if(isset($meta_tipo)){
	    $sSQL.=", radi_$meta_tabla meta ";
	    if($meta_tipo==0){
		$sWhere.=" and cast(meta.$meta_columna as varchar(15)) like '%$metaTag%' and r.radi_nume_radi=meta.radi_nume_radi ";
	    }
	    else{
		$sWhere.=" and meta.$meta_columna like '%$metaTag%' ";
	    }
	}
	$sSQL .= " WHERE dir.sgd_dir_tipo = 1 AND dir.RADI_NUME_RADI=r.RADI_NUME_RADI AND r.TDOC_CODI=td.SGD_TPR_CODIGO";
	//-------------------------------
	//SE QUITA " AND r.CODI_NIVEL <=$nivelus "
	//-------------------------------
	// Assemble full SQL statement
	//-------------------------------


	$depevali = $_SESSION ['dependencia'];
	if ($depevali == 900 || $depevali == 905) {
		$nodepi = "";
	} else {
		$nodepi = " and radi_depe_radi<>900 and radi_depe_radi<>905 ";
		$nodepi = "";
	}
	 $sSQL .= $sWhere . $nodepi . $whereTrd . $sOrder; //
        //$sSQL .= $sWhere . $whereTrd . $sOrder;
    if ($_SESSION ['usua_debug'] == 5)
	$db->conn->debug=true;
	//-------------------------------
	// Execute SQL statement
	//-------------------------------
	$db->conn->SetFetchMode ( ADODB_FETCH_ASSOC );
	//print $sSQL;

	$rs = $db->query ( $sSQL );
	$db->conn->SetFetchMode ( ADODB_FETCH_NUM );
	//echo "<hr>$sSQL<hr>";


	//-------------------------------
	// Process empty recordset
	//-------------------------------
	if ($rs->EOF || ! $rs) {
		?>
     <tr>
			<td colspan="20" class="alarmas">No hay resultados</td>
		</tr>
<?php

		//-------------------------------
		//  The insert link.
		//-------------------------------
		?>
    <tr>
			<td colspan="20" class="ColumnTD"><font class="ColumnFONT">
<?php

		?>




	</table>

<?php

		return;
	}

	//-------------------------------

	//-------------------------------
	// Initialize page counter and records per page
	//-------------------------------
	$iCounter = 0;
	//-------------------------------


	//-------------------------------
	// Process page scroller
	//-------------------------------
	$iPage = get_param ( "FormCIUDADANO_Page" );
	//print ("<BR>($iPage)($iRecordsPerPage)");
	if (strlen ( trim ( $iPage ) ) == 0)
		$iPage = 1;
	else {
		if ($iPage == "last") {
			$db_count = get_db_value ( $sCountSQL );
			$dResult = intval ( $db_count ) / $iRecordsPerPage;
			$iPage = intval ( $dResult );
			if ($iPage < $dResult)
				$iPage ++;
		} else
			$iPage = intval ( $iPage );
	}

	if (($iPage - 1) * $iRecordsPerPage != 0) {
		//print ("<BR>($iPage)($iRecordsPerPage)");
		do {
			$iCounter ++;
			$rs->MoveNext ();
			//print("Entra......");
		} while ( $iCounter < ($iPage - 1) * $iRecordsPerPage && (! $rs->EOF && $rs) );

	}

	$iCounter = 0;
	//-------------------------------


	//$ruta_raiz ="..";
	//include "../config.php";
	//include "../jh_class/funciones_sgd.php";
	//-------------------------------
	// Display grid based on recordset
	//-------------------------------.
	$i = 1;
	while ( (! $rs->EOF && $rs) && $iCounter < $iRecordsPerPage ) {
		//-------------------------------
		// Create field variables based on database fields
		//-------------------------------
		$fldRADI_NUME_RADI = $rs->fields ['RADI_NUME_RADI'];
		$fldRADI_FECH_RADI = $rs->fields ['RADI_FECH_RADI'];
		/**
		 *Bsqueda por expediente
		 *Fecha de modificacin: 11-Agosto-2006
		 *Modificador: Supersolidaria
		 */
		$fldsSGD_EXP_SUBEXPEDIENTE = $rs->fields ['SGD_EXP_NUMERO'];
                $fldsRADI_NUM_NUMINTERNO = $rs->fields ['NUMERO_INTERNO'];
		$radiLeido = $rs->fields ['RADI_LEIDO'];

		$fldASUNTO = $rs->fields ['RA_ASUN'];
		$fldASUNTO = str_replace('\\','',$fldASUNTO);

		$fldTIPO_DOC = $rs->fields ['SGD_TPR_DESCRIP'];
		$fldNUME_HOJAS = $rs->fields ['RADI_NUME_FOLIOS'];
		$fldRADI_PATH = $rs->fields ['RADI_PATH'];
		$fldDIRECCION_C = $rs->fields ['SGD_DIR_DIRECCION'];
		$fldDIGNATARIO = $rs->fields ['SGD_DIR_NOMBRE'];
		$fldTELEFONO_C = $rs->fields ['SGD_DIR_TELEFONO'];
		$fldMAIL_C = $rs->fields ['SGD_DIR_MAIL'];
		$fldNOMBRE = $rs->fields ['SGD_DIR_NOMREMDES'];
			$fldNOMBRE = str_replace('\\','',$fldNOMBRE);
		$fldCEDULA = $rs->fields ['SGD_DIR_DOC'];
		//$fldUSUA_ACTU = $rs->fields['NOMB_ACTU") . " - (" . $rs->fields['LOGIN_ACTU").")";
		$aRADI_DEPE_ACTU = $rs->fields ['RADI_DEPE_ACTU'];
		$aRADI_USUA_ACTU = $rs->fields ['RADI_USUA_ACTU'];
		$fldUSUA_ANTE = $rs->fields ['RADI_USU_ANTE'];
		$fldPAIS = $rs->fields ['RADI_PAIS'];
		$fldDIASR = $rs->fields ['DIASR'];
		$tipoReg = $rs->fields ['SGD_TRD_CODIGO'];
		$nivelRadicado = $rs->fields ['CODI_NIVEL'];
		$seguridadRadicado = $rs->fields ['SGD_SPUB_CODIGO'];
		$reservaRadicado = $rs->fields["RADI_RESERVADO"];  //23-SEP-2010 EJMUNOZ INCLUIR RESERVADO POR ELABORACION

		if ($tipoReg == 1)
			$tipoRegDesc = "Ciudadano";
		if ($tipoReg == 2)
			$tipoRegDesc = "Empresa";
		if ($tipoReg == 3)
			$tipoRegDesc = "ESP";
		if ($tipoReg == 4)
			$tipoRegDesc = "Funcionario";

		if ($radiLeido !=3)
		{
			$color= 'class="leidos"';
			$vinculo= "class='vinculos'";
		}
		if ($radiLeido ==3)
		{
			$color= 'class="devueltos"';
			$vinculo= "class='devueltos_link'";
			}


		$fldNOMBRE = str_replace ( $ps_RADI_NOMB, "<font color=green><b>$ps_RADI_NOMB</b>", tohtml ( $fldNOMBRE ) );
		$fldASUNTO = str_replace ( $ps_RADI_NOMB, "<font color=green><b>$ps_RADI_NOMB</b>", tohtml ( $fldASUNTO ) );
		//-------------------------------
		// Busquedas Anidadas
		//-------------------------------
		$queryDep = "select DEPE_NOMB from dependencia where DEPE_CODI=$aRADI_DEPE_ACTU";

		$db->conn->SetFetchMode ( ADODB_FETCH_ASSOC );
		$rs2 = $db->query ( $queryDep );
		$fldDEPE_ACTU = $rs2->fields ['DEPE_NOMB'];

		//$queryUs = "select USUA_NOMB,CODI_NIVEL from USUARIO where DEPE_CODI=$aRADI_DEPE_ACTU and USUA_CODI=$aRADI_USUA_ACTU ";
                $queryUs ="SELECT   usuario.usua_nomb,   sgd_drm_dep_mod_rol.sgd_drm_valor codi_nivel,   sgd_urd_usuaroldep.rol_codi
                            FROM   usuario,   sgd_urd_usuaroldep,   sgd_mod_modules,   sgd_drm_dep_mod_rol
                            WHERE    usuario.usua_codi = sgd_urd_usuaroldep.usua_codi AND  sgd_mod_modules.sgd_mod_id = sgd_drm_dep_mod_rol.sgd_drm_modecodi AND
                            sgd_drm_dep_mod_rol.sgd_drm_rolcodi = sgd_urd_usuaroldep.rol_codi AND  sgd_drm_dep_mod_rol.sgd_drm_depecodi = sgd_urd_usuaroldep.depe_codi AND
                            sgd_mod_modules.sgd_mod_estado = 1 AND
                            usuario.usua_codi = $aRADI_USUA_ACTU AND
                            sgd_urd_usuaroldep.depe_codi=$aRADI_DEPE_ACTU and  sgd_mod_modules.sgd_mod_modulo = 'codi_nivel'";
		$rs3 = $db->query ( $queryUs );
		$fldUSUA_ACTU = $rs3->fields ['USUA_NOMB'];
		$fldUSUA_NIVEL = $rs3->fields ['CODI_NIVEL'];
		$db->conn->SetFetchMode ( ADODB_FETCH_NUM );
		$linkDocto = "<a $vinculo href='javascript:noPermiso()' > ";
		$linkInfGeneral = "<a $vinculo href='javascript:noPermiso()' > ";

		/*
 * Ajuste validacion permisos unificados
 * @author Liliana Gomez Velasquez
 * @fecha 11 septiembre 2009
 */

		include_once "$ruta_raiz/tx/verLinkArchivo.php";

		$verLinkArchivo = new verLinkArchivo ( $db );

		$resulVali = $verLinkArchivo->valPermisoRadi ( $fldRADI_NUME_RADI );
                //print_r($resulVali);
                $valImg = $resulVali ['verImg'];
		$pathImagen = $resulVali ['pathImagen'];
		$fldsSGD_EXP_SUBEXPEDIENTE = $resulVali ['numExpe'];
                if($pathImagen!='null'){
			if ($valImg == "SI" ) {
				$linkDocto = "<a $vinculo href='".$url_orfeo_power_tools."/document/".$fldRADI_NUME_RADI."/download'>";
				$linkInfGeneral = "<a $vinculo href='../verradicado.php?verrad=$fldRADI_NUME_RADI&" . session_name () . "=" . session_id () . "&krd=$krd&carpeta=8&nomcarpeta=Busquedas&tipo_carp=0'>";
			}
		} else{
			$linkDocto = "";
                        $linkInfGeneral = "";
		}
		//Fin Modificacion


		//$linkInfGeneral =
		//-------------------------------
		// Process the HTML controls
		//-------------------------------
		if ($i == 1) {
			$formato = "listado1";
			$i = 2;
		} else {
			$formato = "listado2";
			$i = 1;
		}
		?>
	<tr class="<?php echo $formato?>">
	<?php
		if ($indiVinculo == 1) {
			?>
	   <td <?php echo $color?> align="center" width="70"><A
			href="javascript:pasar_datos('<?php echo $fldRADI_NUME_RADI?>',1);"> Vincular
		</td>
	  <?php
		}
		if ($indiVinculo == 2) {
			?>
	   <td <?php echo $color?> align="center" width="70"><A
			href="javascript:pasar_datos('<?php echo $fldsSGD_EXP_SUBEXPEDIENTE?>',2);">
		Vincular </td>

	  <?php
		}
		if ($indiVinculo != 2) {
			?>

	<td <?php echo $color?>>
	<?php
			if (strlen ( $fldRADI_PATH )) {
				$iii = $iii + 1;
				echo ($linkDocto);
			}
			echo $fldRADI_NUME_RADI;
			if (strlen ( $fldRADI_PATH )) {
				?></a><?php 			}
			?>&nbsp;</td>
		<td <?php echo $color?>><?php echo $linkInfGeneral?>
	<?php echo tohtml ( $fldRADI_FECH_RADI )?>&nbsp;</a></td>
		<!--
    Bsqueda por expediente
    Fecha de modificacin: 11-Agosto-2006
    Modificador: Supersolidaria
    -->

		<td <?php echo $color?>>
    <?php echo $fldsSGD_EXP_SUBEXPEDIENTE?>&nbsp;</td>
    <?php
		} else {
			?>

    <td <?php echo $color?>>
    <?php echo $fldsSGD_EXP_SUBEXPEDIENTE?>&nbsp;</td>
		<td <?php echo $color?>>
	<?php
			if (strlen ( $fldRADI_PATH )) {
				$iii = $iii + 1;
				echo ($linkDocto);
			}
			echo $fldRADI_NUME_RADI;
			if (strlen ( $fldRADI_PATH )) {
				?></a><?php 			}
			?>&nbsp;</td>
		<td <?php echo $color?>><?php echo $linkInfGeneral?>
	<?php echo tohtml ( $fldRADI_FECH_RADI )?>&nbsp;</a></td>
		<!--
    Busqueda por expediente
    Fecha de modificacion: 11-Agosto-2006
    Modificador: Supersolidaria
    -->
    <?php
		}
		?>
    <TD  <?php echo $color?> align="center" width="70"><?php echo $fldsRADI_NUM_NUMINTERNO;?> </Td>
	<td  <?php echo $color?>>
	<?php

	echo $fldASUNTO;
	?>&nbsp;</td>
		<td <?php echo $color?>>
	<?php echo tohtml ( $fldTIPO_DOC )?>&nbsp;</td>
		<td <?php echo $color?>>
	<?php echo $tipoRegDesc;?>&nbsp;</td>
		<td <?php echo $color?>>
	<?php echo tohtml ( $fldNUME_HOJAS )?>&nbsp;</td>
		<td <?php echo $color?>>
	<?php echo tohtml ( $fldDIRECCION_C )?>&nbsp;</td>
		<td <?php echo $color?>>
	<?php echo tohtml ( $fldTELEFONO_C )?>&nbsp;</td>
		<td <?php echo $color?>>
	<?php echo tohtml ( $fldMAIL_C )?>&nbsp;</td>
		<td <?php echo $color?>>
	<?php echo tohtml ( $fldDIGNATARIO )?>&nbsp;</td>
		<td <?php echo $color?>>
	<?php

	echo   $fldNOMBRE;
	?>&nbsp;</td>
		<td <?php echo $color?>>
	<?php echo tohtml ( $fldCEDULA )?>&nbsp;</td>
		<td <?php echo $color?>>
	<?php echo tohtml ( $fldUSUA_ACTU )?>&nbsp;</td>
		<td <?php echo $color?>>
	<?php echo tohtml ( $fldDEPE_ACTU )?>&nbsp;</td>
		<td <?php echo $color?>>
	<?php echo tohtml ( $fldUSUA_ANTE )?>&nbsp;</td>
		<td <?php echo $color?>>
	<?php echo tohtml ( $fldPAIS );?>&nbsp;</td>
		<td <?php echo $color?>>
	<?php
		if ($fldRADI_DEPE_ACTU != 999) {
			echo tohtml ( $fldDIASR );
		} else {
			echo "Sal";
		}
		?>&nbsp;</td>
	</tr>
	  <?php
		$iCounter ++;
		$rs->MoveNext ();

	}

	//-------------------------------
	//  Record navigator.
	//-------------------------------
	?>
<tr>
		<td colspan="20" class="ColumnTD"><font class="ColumnFONT">
<?php

	// Navigation begin
	$bEof = $rs;
	$s_RADI_NUME_RADI=$_POST['s_RADI_NUME_RADI'];
	$s_DOCTO=$_POST['s_DOCTO'];
	$s_SGD_EXP_SUBEXPEDIENTE=$_POST['s_SGD_EXP_SUBEXPEDIENTE'];
	$s_entrada=$_POST['s_entrada'];
	$s_RADI_NOMB=$_POST['s_RADI_NOMB'];
	$CriterioBusq=$_POST['CriterioBusq'];
	$s_desde_dia=$_POST['s_desde_dia'];
	$s_desde_mes=$_POST['s_desde_mes'];
	$s_desde_ano=$_POST['s_desde_ano'];
	$s_hasta_dia=$_POST['s_hasta_dia'];
	$s_hasta_mes=$_POST['s_hasta_dia'];
	$s_hasta_ano=$_POST['s_hasta_ano'];
	$s_TDOC_CODI=$_POST['s_TDOC_CODI'];
	$s_RADI_DEPE_ACTU=$_POST['s_RADI_DEPE_ACTU'];
	$indiVinculo=$_POST['indiVinculo'];
	$etapa=$_POST['etapa'];
	$FormCIUDADANO_Sorting=$_POST['FormCIUDADANO_Sorting'];
	$orden=$_POST['orden'];
	$EXT=$_POST['EXT'];

	if (($bEof && ! $bEof->EOF) || $iPage != 1) {
		$iCounter = 1;
		$iHasPages = $iPage;
		$sPages = "";
		$sPages2 = "";
		$iDisplayPages = 0;
		$iNumberOfPages = 1000; /* El numero de paginas que aparecer en el navegador al pie de la pagina */

		while ( (! $rs->EOF && $rs) && $iHasPages < $iPage + $iNumberOfPages ) {
			if ($iCounter == $iRecordsPerPage) {
				$iCounter = 0;
				$iHasPages = $iHasPages + 1;
			}
			$iCounter ++;
			$rs->MoveNext ();
		}
		if (($rs->EOF || ! $rs) && $iCounter > 1)
			$iHasPages ++;
		if (($iHasPages - $iPage) < intval ( $iNumberOfPages / 2 ))
			$iStartPage = $iHasPages - $iNumberOfPages;
		else
			$iStartPage = $iPage - $iNumberOfPages + intval ( $iNumberOfPages / 2 );

		if ($iStartPage < 0)
			$iStartPage = 0;
		for($iPageCount = $iPageCount + 1; $iPageCount <= $iPage - 1; $iPageCount ++) {
			//$sPages .=  "<a href=" . $sFileName . "?" . $form_params . $sSortParams . "FormCIUDADANO_Page=" . $iPageCount . "#RADICADO\"><font " . "class=\"ColumnFONT\"" . ">" . $iPageCount . "</a>&nbsp;";
			//$sPages .=  "<a href='#' onclick=" . $sFileName . "?" . $form_params . $sSortParams . "FormCIUDADANO_Page=" . $iPageCount . "#RADICADO\"><font " . "class=\"ColumnFONT\"" . ">" . $iPageCount . "</a>&nbsp;";
			if(($iDisplayPages%50)==0){
			    $sPages .="</tr><tr>";
				$sPages2 .="</tr></tr>";
			}
			$sPages .= "<td><a href='#' onclick=\"botonBusqueda('Procesando'," . $iPageCount . ");\">" . $iPageCount . "</a>&nbsp;</td>";
			$sPages2 .= "<td><a target='_blank' href='../busqueda/ResultadoBusqueda.php?".session_name ()."=".session_id ()."&dependencia=$dependencia&krd=$krd&EXT=Y&FormCIUDADANO_Page=$iPageCount&s_RADI_NUME_RADI=$s_RADI_NUME_RADI&s_DOCTO=$s_DOCTO&s_SGD_EXP_SUBEXPEDIENTE=$s_SGD_EXP_SUBEXPEDIENTE&s_entrada=$s_entrada&s_RADI_NOMB=$s_RADI_NOMB&CriterioBusq=$CriterioBusq&s_desde_dia=$s_desde_dia&s_desde_mes=$s_desde_mes&s_desde_ano=$s_desde_ano&s_hasta_dia=$s_hasta_dia&s_hasta_mes=$s_hasta_dia&s_hasta_ano=$s_hasta_ano&s_TDOC_CODI=$s_TDOC_CODI&s_RADI_DEPE_ACTU=$s_RADI_DEPE_ACTU&indiVinculo=$indiVinculo&etapa=$etapa&FormCIUDADANO_Sorting=$FormCIUDADANO_Sorting&orden=$orden'> $iPageCount </a>&nbsp;</td>";

			$iDisplayPages ++;
		}

		$sPages .= "<b>" . $iPage . "</b>&nbsp;";
		$sPages2 .= "<b>" . $iPage . "</b>&nbsp;";
		$iDisplayPages ++;

		$iPageCount = $iPage + 1;

		while ( $iDisplayPages < $iNumberOfPages && $iStartPage + $iDisplayPages < $iHasPages ) {

			//sPages .= "<a href=\"" . $sFileName . "?" . $form_params . $sSortParams . "FormCIUDADANO_Page=" . $iPageCount . "#RADICADO\"><font " . "class=\"ColumnFONT\"" . ">" . $iPageCount . "</a>&nbsp;";
			if(($iDisplayPages%50)==0){
				$sPages .="</tr><tr>";
				$sPages2 .="</tr></tr>";
			}
			$sPages .= "<td><font class=\"paginacion\"><a href='#' onclick=\"botonBusqueda('Procesando'," . $iPageCount . ");\">" . $iPageCount . "</a>&nbsp;</td>";
			$sPages2 .= "<td><font class=\"paginacion\"><a target='_blank' href='../busqueda/ResultadoBusqueda.php?".session_name ()."=".session_id ()."&dependencia=$dependencia&krd=$krd&EXT=Y&FormCIUDADANO_Page=$iPageCount&s_RADI_NUME_RADI=$s_RADI_NUME_RADI&s_DOCTO=$s_DOCTO&s_SGD_EXP_SUBEXPEDIENTE=$s_SGD_EXP_SUBEXPEDIENTE&s_entrada=$s_entrada&s_RADI_NOMB=$s_RADI_NOMB&CriterioBusq=$CriterioBusq&s_desde_dia=$s_desde_dia&s_desde_mes=$s_desde_mes&s_desde_ano=$s_desde_ano&s_hasta_dia=$s_hasta_dia&s_hasta_mes=$s_hasta_dia&s_hasta_ano=$s_hasta_ano&s_TDOC_CODI=$s_TDOC_CODI&s_RADI_DEPE_ACTU=$s_RADI_DEPE_ACTU&indiVinculo=$indiVinculo&etapa=$etapa&FormCIUDADANO_Sorting=$FormCIUDADANO_Sorting&orden=$orden'> $iPageCount </a>&nbsp;</td>";
			$iDisplayPages ++;
			$iPageCount ++;

		}
		$xpages="<table><tr><td><font class='paginacion'>";
		$Spages="<table><tr><td><font class='paginacion'>";
		if ($iPage == 1) {
			$Spages.="Primero Anterior";
			$xpages="Primero Anterior";
		} else {
		  	$ipaget=$iPage - 1;
	 		$Spages.="<font class=\"paginacion\"><a href='#' onclick=\"botonBusqueda('Procesando',1);\">Primero</a><a href='#' onclick=\"botonBusqueda('Procesando',$ipaget);\">Anterior</a>";
   			$xpages.="<font class=\"paginacion\"><a target='_blank' href='../busqueda/ResultadoBusqueda.php?".session_name ()."=".session_id ()."&dependencia=$dependencia&krd=$krd&EXT=Y&FormCIUDADANO_Page=1&s_RADI_NUME_RADI=$s_RADI_NUME_RADI&s_DOCTO=$s_DOCTO&s_SGD_EXP_SUBEXPEDIENTE=$s_SGD_EXP_SUBEXPEDIENTE&s_entrada=$s_entrada&s_RADI_NOMB=$s_RADI_NOMB&CriterioBusq=$CriterioBusq&s_desde_dia=$s_desde_dia&s_desde_mes=$s_desde_mes&s_desde_ano=$s_desde_ano&s_hasta_dia=$s_hasta_dia&s_hasta_mes=$s_hasta_dia&s_hasta_ano=$s_hasta_ano&s_TDOC_CODI=$s_TDOC_CODI&s_RADI_DEPE_ACTU=$s_RADI_DEPE_ACTU&indiVinculo=$indiVinculo&etapa=$etapa&FormCIUDADANO_Sorting=$FormCIUDADANO_Sorting&orden=$orden'>Primero</a>&nbsp;&nbsp;
			<a href='../busqueda/ResultadoBusqueda.php?".session_name ()."=".session_id ()."&dependencia=$dependencia&krd=$krd&EXT=Y&FormCIUDADANO_Page=$ipaget&s_RADI_NUME_RADI=$s_RADI_NUME_RADI&s_DOCTO=$s_DOCTO&s_SGD_EXP_SUBEXPEDIENTE=$s_SGD_EXP_SUBEXPEDIENTE&s_entrada=$s_entrada&s_RADI_NOMB=$s_RADI_NOMB&CriterioBusq=$CriterioBusq&s_desde_dia=$s_desde_dia&s_desde_mes=$s_desde_mes&s_desde_ano=$s_desde_ano&s_hasta_dia=$s_hasta_dia&s_hasta_mes=$s_hasta_dia&s_hasta_ano=$s_hasta_ano&s_TDOC_CODI=$s_TDOC_CODI&s_RADI_DEPE_ACTU=$s_RADI_DEPE_ACTU&indiVinculo=$indiVinculo&etapa=$etapa&FormCIUDADANO_Sorting=$FormCIUDADANO_Sorting&orden=$orden'> Anterior </a>";
		}

		$Spages.="</td><td><table><tr>" . $sPages . "</tr></table></td><td><font class='paginacion'>";
		$xpages.="</td><td><table><tr>" . $sPages2 . "</tr></table></td><td><font class='paginacion'>";
		$ultima = $iPageCount - 1;
		if ($ultima == $iPage) {
			//class="ColumnFONT"
			$Spages.="Siguiente Ultimo";
			$xpages.='Siguiente Ultimo';
		} else {
			$ipaget=$iPage + 1;
			$Spages.="<a href='#' onclick=\"botonBusqueda('Procesando',$ipaget );\">Siguiente</a>&nbsp; <a href='#' onclick=\"botonBusqueda('Procesando',$ultima);\">Ultimo</a>";
			$xpages.="<a target='_blank' href='../busqueda/ResultadoBusqueda.php?".session_name ()."=".session_id ()."&dependencia=$dependencia&krd=$krd&EXT=Y&FormCIUDADANO_Page=$ipaget&s_RADI_NUME_RADI=$s_RADI_NUME_RADI&s_DOCTO=$s_DOCTO&s_SGD_EXP_SUBEXPEDIENTE=$s_SGD_EXP_SUBEXPEDIENTE&s_entrada=$s_entrada&s_RADI_NOMB=$s_RADI_NOMB&CriterioBusq=$CriterioBusq&s_desde_dia=$s_desde_dia&s_desde_mes=$s_desde_mes&s_desde_ano=$s_desde_ano&s_hasta_dia=$s_hasta_dia&s_hasta_mes=$s_hasta_dia&s_hasta_ano=$s_hasta_ano&s_TDOC_CODI=$s_TDOC_CODI&s_RADI_DEPE_ACTU=$s_RADI_DEPE_ACTU&indiVinculo=$indiVinculo&etapa=$etapa&FormCIUDADANO_Sorting=$FormCIUDADANO_Sorting&orden=$orden'>Siguiente</a>&nbsp;&nbsp;
			<a href='../busqueda/ResultadoBusqueda.php?".session_name ()."=".session_id ()."&dependencia=$dependencia&krd=$krd&EXT=Y&FormCIUDADANO_Page=$ultima&s_RADI_NUME_RADI=$s_RADI_NUME_RADI&s_DOCTO=$s_DOCTO&s_SGD_EXP_SUBEXPEDIENTE=$s_SGD_EXP_SUBEXPEDIENTE&s_entrada=$s_entrada&s_RADI_NOMB=$s_RADI_NOMB&CriterioBusq=$CriterioBusq&s_desde_dia=$s_desde_dia&s_desde_mes=$s_desde_mes&s_desde_ano=$s_desde_ano&s_hasta_dia=$s_hasta_dia&s_hasta_mes=$s_hasta_dia&s_hasta_ano=$s_hasta_ano&s_TDOC_CODI=$s_TDOC_CODI&s_RADI_DEPE_ACTU=$s_RADI_DEPE_ACTU&indiVinculo=$indiVinculo&etapa=$etapa&FormCIUDADANO_Sorting=$FormCIUDADANO_Sorting&orden=$orden'>Ultimo</a>";
		}
		$xpages.="</td></tr></table>";
		$Spages.="</td></tr></table>";
	}
	if($EXT=='Y'){
		echo "<font class=\"paginacion\">&nbsp;&nbsp;".$xpages;
	}
	else{
		echo "<font class=\"paginacion\">&nbsp;&nbsp;".$Spages;
	}
//-------------------------------
// Navigation end
//-------------------------------

?>
	</td>
	</tr>
</table>

<?php
}
?>
