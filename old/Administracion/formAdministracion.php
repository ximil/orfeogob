<?php session_start(); date_default_timezone_set('America/Bogota'); date_default_timezone_set('America/Bogota');
/**
  * Se añadio compatibilidad con variables globales en Off
  * @autor Jairo Losada 2009-05
  * @licencia GNU/GPL V 3
  */

define('ADODB_ASSOC_CASE', 2);

foreach($_GET as $k=>$v) $$k=$v;

$krd = $_SESSION["krd"];
$dependencia = $_SESSION["dependencia"];
$usua_doc = $_SESSION["usua_doc"];
$codusuario = $_SESSION["codusuario"];
$tip3Nombre=$_SESSION["tip3Nombre"];
$tip3desc = $_SESSION["tip3desc"];
$tip3img =$_SESSION["tip3img"];
$ruta_raiz = "..";

$nomcarpeta=$_GET["carpeta"];
$tipo_carpt=$_GET["tipo_carpt"];
$adodb_next_page=$_GET["adodb_next_page"];
if(!$_SESSION["krd"]){
	$mensajeCierre='La Session ha Terminado';
	die(include "$ruta_raiz/cerrar_session.php");
} 
if($_SESSION['usua_admin_sistema']!=1) die(include "$ruta_raiz/sinacceso.php");
?>
<html>
<head>
<title>Documento  sin t&iacute;tulo</title>
<link rel="stylesheet" href="../estilos/orfeo.css">
</head>
<body>
<table width="71%" align="center" border="0" cellpadding="0" cellspacing="5" class="borde_tab">
<tr bordercolor="#FFFFFF">
	<td colspan="2" class="titulos4"><div align="center"><strong>M&Oacute;DULO DE ADMINISTRACI&Oacute;N</strong></div></td>
</tr>
<tr bordercolor="#FFFFFF">
	<td align="center" class="listado2" width="48%">
		<a href='usuario/mnuUsuarios.php?krd=<?php echo $krd?>' target='mainFrame' class="vinculos">1. USUARIOS Y PERFILES</a>
	</td>
	<td align="center" class="listado2" width="48%"><a href="tbasicas/adm_dependencias.php" class="vinculos" target="mainFrame">2. DEPENDENCIAS</a></td>
</tr>
<tr bordercolor="#FFFFFF">
	<td align="center" class="listado2" width="48%"> <a  href="tbasicas/adm_nohabiles.php" class="vinculos" target='mainFrame'>3. DIAS NO HABILES</a></td>
	<td align="center" class="listado2" width="48%"><a href="tbasicas/adm_fenvios.php" class="vinculos" target='mainFrame'>4. ENV&Iacute;O DE CORRESPONDENCIA</a> </td>
</tr>
<tr bordercolor="#FFFFFF">
	<td align="center" class="listado2" width="48%"><a href="tbasicas/adm_tsencillas.php" class="vinculos" target='mainFrame'>5. TABLAS SENCILLAS</a></td>
	<td align="center" class="listado2" width="48%"><a href="tbasicas/adm_trad.php?krd=<?php echo $krd?>" class="vinculos" target='mainFrame'>6. TIPOS DE RADICACI&Oacute;N</a></td>
</tr>
<tr bordercolor="#FFFFFF">
	<td align="center" class="listado2" width="48%"><a href="tbasicas/adm_paises.php" class="vinculos" target='mainFrame'>7. PA&Iacute;SES</a></td>
	<td align="center" class="listado2" width="48%"><a href="tbasicas/adm_dptos.php" class="vinculos" target='mainFrame'>8. DEPARTAMENTOS</a></td>
</tr>
<tr bordercolor="#FFFFFF">
	<td align="center" class="listado2" width="48%"><a href="tbasicas/adm_mcpios.php" class="vinculos" target='mainFrame'>9. MUNICIPIOS</a></td>
	<td align="center" class="listado2" width="48%"><a href="tbasicas/adm_tarifas.php" class="vinculos" target='mainFrame'>10. TARIFAS</a></td>
</tr>
<tr bordercolor="#FFFFFF">
	<td align="center" class="listado2" width="48%"><a href="tbasicas/adm_contactos.php" class="vinculos" target='mainFrame'>11. CONTACTOS</a></td>
	<td align="center" class="listado2" width="48%"><a href="tbasicas/adm_esp.php?krd=<?php echo $krd?>" class="vinculos" target='mainFrame'>12. ENTIDADES</a></td>
</tr>
<tr bordercolor="#FFFFFF">
	<td align="center" class="listado2" width="48%"><a href="roles/roles.php" class="vinculos" target='mainFrame'>13. ROLES</a></td>
	<td align="center" class="listado2" width="48%"></a></td>
</tr>
</table>
<br>
<?php 
 // MODULO OPCIONAL DE ADMINISTRACION DE FUNCIONARIOS Y ENTIDADES
 /* Por SuperSolidaria
    Modifico y Adapto Jairo Losada 08/2009 */

?>
<table width="71%" align="center" border="0" cellpadding="0" cellspacing="5" class="borde_tab">
<tr bordercolor="#FFFFFF">
	<td colspan="2" class="titulos4" align="center">
          Opcional x SES
        </td>
</tr>
<tr bordercolor="#FFFFFF">
	<td align="center" class="listado2" width="48%">
		<a href='entidad/listaempresas.php?<?php echo $phpsession ?>&krd=<?php echo $krd?>' target='mainFrame'  class="vinculos">12. ENTIDADES  V.SES</a>
	</td>
	<td align="center" class="listado2" width="48%">
	<a href='usuario/listafuncionarios.php?<?php echo $phpsession ?>' target='mainFrame'  class="vinculos">12.1 FUNCIONARIO - ENTIDAD</a>
</td>
</tr>
</table>

</body>
</html>
