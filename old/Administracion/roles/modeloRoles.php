<?php 
/** 
 * @author deimont
 * 
 * 
 */
class modeloRoles {
	//TODO - Insert your code here
	var $link;
	
	//El contructor instancia la clase conexion, que se conecta con la base de datos en $link
	function __construct($db) {
		$this->link = $db;
	}   
	   
	 function consultar($activo=NULL)
	 {
	 	if(!$activo)
	 	   $query="SELECT sgd_rol_nombre as ROL , sgd_rol_id as IDROL, sgd_rol_estado as ESTADO FROM sgd_rol_roles";
	 	else
	 	   $query="SELECT sgd_rol_nombre as ROL , sgd_rol_id as IDROL, sgd_rol_estado as ESTADO FROM sgd_rol_roles where sgd_rol_estado=1";
	 	$rs=$this->link->conn->Execute($query);
		return $rs;
	 }
function consultarDepe()
	 {
	 	$query = "select DEPE_NOMB,DEPE_CODI from dependencia order by depe_nomb";
	 	$rs=$this->link->conn->Execute($query);
		return $rs;
	 }
     function consultarModroldep($depe,$rol)
	 {
	
 $query = "SELECT sgd_drm_depecodi AS DEPECODI, sgd_drm_rolcodi AS ROLCODI, sgd_drm_modecodi AS MODCODI, sgd_drm_valor AS VALOR   
			FROM   sgd_drm_dep_mod_rol WHERE   sgd_drm_dep_mod_rol.sgd_drm_depecodi = $depe AND sgd_drm_dep_mod_rol.sgd_drm_rolcodi = $rol 
            ORDER BY  sgd_drm_modecodi ASC;";
	 	$rs=$this->link->conn->Execute($query);
		return $rs;
	 }

function Insertdrm($iddep,$idrol,$idmod,$valor){
		
		 $query = "INSERT INTO sgd_drm_dep_mod_rol (
		sgd_drm_depecodi, sgd_drm_rolcodi, sgd_drm_modecodi, sgd_drm_valor  ) 
		VALUES ($iddep,$idrol,$idmod,$valor)";
	 	$rs=$this->link->conn->Execute($query);	
			if (!$rs){
		   return 'ERROR';
	    }	
		return 'OK';
	}
     function moddrm($iddep,$idrol,$idmod,$valor) {
	 	$query = "UPDATE sgd_drm_dep_mod_rol SET sgd_drm_valor=$valor WHERE 
	     sgd_drm_depecodi= $iddep and 
		sgd_drm_rolcodi = $idrol and sgd_drm_modecodi = $idmod";
	 	$rs=$this->link->conn->Execute($query);
       	if (!$rs){
		   return 'ERROR';
	    }	
		return 'OK';
	} 
	function modificar($id,$nombre,$tipo) {
		$query = "UPDATE sgd_rol_roles SET sgd_rol_nombre= '$nombre', sgd_rol_estado = '$tipo' WHERE sgd_rol_id = ".$id;
	 	$rs=$this->link->conn->Execute($query);
       	if (!$rs){
		   return 'ERROR';
	    }	
		return 'OK';
	}
	function crear($nombre,$tipo) {
		$query1 = "select max(sgd_rol_id) as id  from sgd_rol_roles";
		$rs1=$this->link->conn->Execute($query1);
		$idrol=$rs1->fields["ID"] +1;
		$query = "INSERT INTO sgd_rol_roles (sgd_rol_id,sgd_rol_nombre, sgd_rol_estado) VALUES ($idrol,'$nombre','$tipo')";
	 	$rs=$this->link->conn->Execute($query);	
			if (!$rs){
		   return 'ERROR';
	    }	
		return 'OK';
	}
	function consultarScript($idrol) {
		$query="select rs.id,s.nombre, s.descrp,rs.modificar,rs.crear,rs.hab,rs.listar from rolxscripts rs, rol r, scripts s 
where r.id=rs.idrol and s.id=rs.idscripts and r.id=$idrol order by 	idcategoria";// and rs.hab=1";
	 	$resultado=$this->link->query($query);  
		return $resultado;
		;
	}
	 function consultarModulos($e=0)
	 {
	 	$estadoSql="";
	 	if($e==1) $estadoSql=" where sgd_mod_estado=1 ";
	 	$query="SELECT * FROM sgd_mod_modules $estadoSql order by SGD_MOD_ID asc";
	 	$rs=$this->link->conn->Execute($query);
		return $rs;
	 }
	 //	sgd_mod_id	sgd_mod_modulo	
	 function  modificarModulo($id,$nombre,$code,$valmax,$path,$deta,$titulo,$estado){
	 			$query = "UPDATE sgd_mod_modules SET sgd_mod_modulo= '$nombre', 
	 			sgd_mod_valmax=$valmax,	sgd_mod_detalles='$deta',	sgd_mod_path='$path',	
	 			sgd_mod_titulo='$titulo',	sgd_mod_menu='$code',
	 			sgd_mod_estado = '$estado' WHERE sgd_mod_id = ".$id;
	 	$rs=$this->link->conn->Execute($query);
       	if (!$rs){
		   return 'ERROR';
	    }	
		return 'OK';	 	
	 }
	 
	function crearModulo($nombre,$code,$valmax,$path,$deta,$titulo,$estado){
		$query1 = "select max(sgd_mod_id) as id  from sgd_mod_modules";
		$rs1=$this->link->conn->Execute($query1);
		$idmod=$rs1->fields["ID"] +1;
		$query = "INSERT INTO sgd_mod_modules (sgd_mod_id,sgd_mod_modulo,sgd_mod_valmax,sgd_mod_detalles,sgd_mod_path,sgd_mod_titulo,sgd_mod_menu,sgd_mod_estado) 
		VALUES ($idmod,'$nombre',$valmax,'$deta','$path','$titulo','$code','$estado')";
	 	$rs=$this->link->conn->Execute($query);	
			if (!$rs){
		   return 'ERROR';
	    }	
		return 'OK';
	}
}	
 /*	$query = "SELECT 
sgd_drm_dep_mod_rol.sgd_drm_depecodi AS DEPECODI, sgd_drm_dep_mod_rol.sgd_drm_rolcodi AS ROLCODI, 
sgd_drm_dep_mod_rol.sgd_drm_modecodi AS MODCODI, sgd_mod_modules.sgd_mod_modulo AS MODULO,
 sgd_drm_dep_mod_rol.sgd_drm_valor AS VALOR   
FROM 
  public.dependencia, 
  public.sgd_mod_modules, 
  public.sgd_rol_roles, 
  public.sgd_drm_dep_mod_rol
WHERE 
  sgd_drm_dep_mod_rol.sgd_drm_depecodi = dependencia.depe_codi AND
  sgd_drm_dep_mod_rol.sgd_drm_modecodi = sgd_mod_modules.sgd_mod_id AND
  sgd_drm_dep_mod_rol.sgd_drm_rolcodi = sgd_rol_roles.sgd_rol_id AND
  sgd_drm_dep_mod_rol.sgd_drm_depecodi = $depe AND
  sgd_drm_dep_mod_rol.sgd_drm_rolcodi = $rol AND
  sgd_mod_modules.sgd_mod_estado = 1 AND 
  sgd_rol_roles.sgd_rol_estado = 1
ORDER BY
  sgd_mod_modules.sgd_mod_id ASC;";*/
?>