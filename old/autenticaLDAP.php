<?php error_reporting(0);

function checkldapuser($username,$password){
  //Nombre o IP del servidor de autenticacion LDAP
//$ldapServer = '10.200.1.123';
$ldapServer = 'entsvrred';
//Cadena de busqueda en el servidor.
$cadenaBusqLDAP = 'dc=caprecom,dc=rloc';
//Campo seleccionado (de las variables LDAP) para realizar la autenticacion.
$campoBusqLDAP = 'uid';
 //$ldapconn = ldap_connect("ldap://10.200.1.123", 389) or die("Ocurrieron errores al conectar a LDAP");
    
  if($connect=@ldap_connect('ldap://'.$ldapServer,3268)){ 

   // enlace a la conexión
   if(($bind=@ldap_bind($connect,$username.'@caprecom.rloc',$password)) == false){
     	$mensajeError =  "Falla la conexi&oacute;n con el servidor LDAP";
     return $mensajeError;
   }

 //print_r($bind);
   // busca el usuario
   if (($res_id = ldap_search( $connect,$cadenaBusqLDAP,
                               $campoBusqLDAP."=$username")) == false) {
    	$mensajeError = "No encontrado el usuario en el arbol LDAP";
     return $mensajeError;
   }
   
 //recolecta datos
   if (ldap_count_entries($connect, $res_id) != 1) {
     	$mensajeError =  "El usuario $username se encontr&oacute; mas de 1 vez";
     return $mensajeError;
   }

   if (( $entry_id = ldap_first_entry($connect, $res_id))== false) {
     	$mensajeError =  "No se obtuvieron resultados";
     return $mensajeError;
   }

   if (( $user_dn = ldap_get_dn($connect, $entry_id)) == false) {
     	$mensajeError = "No se puede obtener el dn del usuario";
     return $mensajeError;
   }
	error_reporting( 0 );
   /* Autentica el usuario */
   if (($link_id = ldap_bind($connect, $user_dn, $password)) == false) {
   	error_reporting( 0 );
   	$mensajeError = "USUARIO O CONTRASE&Ntilde;A INCORRECTOS";
     return $mensajeError;
   }

   return '';
   @ldap_close($connect);
  } else {                                 
   $mensajeError = "no hay conexión a '$ldap_server'";
   return $mensajeError;
  }

  @ldap_close($connect);
  return(false);

}

?>