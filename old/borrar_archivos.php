<?php
if (!$ruta_raiz)
$ruta_raiz= ".";
require_once("$ruta_raiz/include/db/ConnectionHandler.php");

if (!$db)
$db = new ConnectionHandler($ruta_raiz);

$db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
$db->conn->BeginTrans();

//Cargando variables del log
if(isset($_SERVER['HTTP_X_FORWARD_FOR'])){
    $proxy=$_SERVER['HTTP_X_FORWARD_FOR'];
}else
    $proxy=$_SERVER['REMOTE_ADDR'];
$REMOTE_ADDR=$_SERVER['REMOTE_ADDR'];
//$ruta_raiz='../..';

include_once "$ruta_raiz/../core/clases/log.php";
$log = new log($ruta_raiz);
$log->setAddrC($REMOTE_ADDR);
$log->setProxyAd($proxy);

//$db->conn->debug=true;
//$isql = "select usua_login,usua_pasw,codi_nivel, USUA_NOMB from usuario "."where usua_login ='$usua'";
$isql="SELECT
  usuario.usua_login,
  sgd_mod_modules.sgd_mod_modulo,
  usuario.USUA_PASW,
  usuario.usua_doc,
  usuario.usua_nomb,
  sgd_drm_dep_mod_rol.sgd_drm_valor as CODI_NIVEL,
  sgd_urd_usuaroldep.depe_codi,
  sgd_urd_usuaroldep.rol_codi
FROM
  usuario,
  sgd_urd_usuaroldep,
  sgd_mod_modules,
  sgd_drm_dep_mod_rol
WHERE
  usuario.usua_codi = sgd_urd_usuaroldep.usua_codi AND
  sgd_mod_modules.sgd_mod_id = sgd_drm_dep_mod_rol.sgd_drm_modecodi AND
  sgd_drm_dep_mod_rol.sgd_drm_rolcodi = sgd_urd_usuaroldep.rol_codi AND
  sgd_drm_dep_mod_rol.sgd_drm_depecodi = sgd_urd_usuaroldep.depe_codi AND
  sgd_mod_modules.sgd_mod_estado = 1 AND
  usuario.usua_codi = $codusuario AND
sgd_drm_dep_mod_rol.sgd_drm_rolcodi = $id_rol AND
  sgd_drm_dep_mod_rol.sgd_drm_depecodi = $dependencia AND
  sgd_mod_modules.sgd_mod_modulo = 'codi_nivel'";
$rs=$db->query($isql);
if  ($rs && !$rs->EOF){
	$secur=$rs->fields['CODI_NIVEL'];
	//Traigo el nombre del usuario para ponerlo en la descripción del histórico
	$nombreUsuario = $rs->fields['USUA_NOMB'];
}
if (!$secur){
	$mensaje="No tiene permisos para borrar el documento";
}
if ($secur) {
	$isql = "select codi_nivel ,anex_solo_lect ,anex_creador ,anex_desc,anex_tipo_ext , anex_numero ,anex_nomb_archivo ".
	"from anexos, anexos_tipo,radicado ".
	"where
		anex_codigo='$anexo' and anex_radi_nume=radi_nume_radi and anex_tipo=anex_tipo_codi";

	$rs=$db->query($isql);
	if  ($rs && !$rs->EOF){
		$docunivel=$rs->fields['CODI_NIVEL'];
		$sololect=($rs->fields['ANEX_SOLO_LECT']=="S");
		$extension=$rs->fields['ANEX_TIPO_EXT'];
		$usua_creador=($rs->fields['ANEX_CREADOR']==$usua);
		$nombrearchivo=strtoupper($rs->fields['ANEX_NOMB_ARCHIVO']);
	if ($docunivel>$nivel)
		$secur=0;
	}else{
		$mensaje="El archivo que desea borrar no existe: Por favor consulte al administrador del sistema";
		}
   }

   //$bien=unlink(trim($linkarchivo));
   $bien=true;
   if ($bien){
     $isql = "update anexos set anex_borrado='S' ".
              "where anex_codigo='$anexo'";
     $bien= $db->query($isql);
   }
   if ($bien){
   	include "$ruta_raiz/include/tx/Historico.php";
	$hist = new Historico($db);
	$anexBorrado = array();
	$anexBorrado[] = $numrad;

	$observa = "Se Elimina Anexo Digitalizado con Codigo: $anexo. Eliminado por: $nombreUsuario.";
	$codTx = 31; //Código correspondiente a la eliminación de anexos
	$hist->insertarHistorico($anexBorrado,  $dependencia,$codusuario,$id_rol,$dependencia,$codusuario,$id_rol,$observa, $codTx);
	$log->setUsuaCodi($codusuario);
    $log->setDepeCodi($dependencia);
    $log->setRolId($id_rol);
    $log->setDenomDoc($doc='Radicado');
    $log->setAction('linked_document_deleted');
    $log->setOpera("Borrado anexo No. $anexo");
    $log->setNumDocu($numrad);
    $log->registroEvento();

     $mensaje="<span class='info'>Archivo eliminado<span><br> ";
     $db->conn->CommitTrans();
   }
	else {
     $mensaje="<span class='alarmas'>No fue posible eliminar Archivo<span></br>";
     $db->conn->RollbackTrans();
	}
?>
<html>
   <head>
      <title>Informaci&oacute;n de Anexos</title>
      <link rel="stylesheet" href="estilos/orfeo.css">
   </head>
<script language="javascript">
function actualizar(){
   archivo=document.forma.userfile.value;
   if (archivo==""){
      if (document.forma.sololect.checked!=true)
         alert("Por favor escoja un archivo");
	  else
	     document.forma.submit();
   }
   else if (archivo.toUpperCase().substring(archivo.length-<?php echo strlen(trim($nombrearchivo))?>,archivo.length)!="<?php echo trim($nombrearchivo)?>"){
     if (confirm("Al parecer va a modificar un archivo diferente del original. Esta seguro?"))
         document.forma.submit();
   }else{
      document.forma.submit();
   }
}
</script>
<body bgcolor="#FFFFFF" topmargin="0">
<br>
<div align="center">
<p>
<?php echo $mensaje?>
</p>
<input type='button' class="botones" value='cerrar' onclick='opener.regresar();window.close();'>
</body>
</html>
