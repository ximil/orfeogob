<?php  
/**
 * Este programa despliega el men� principal de correspondencia masiva
 * @author      Sixto Angel Pinz�n
 * @version     1.0
 */
error_reporting(7);
session_start();
foreach ($_GET as $key => $valor)   ${$key} = $valor;
foreach ($_POST as $key => $valor)   ${$key} = $valor;

$krd = $_SESSION["krd"];
$dependencia = $_SESSION["dependencia"];
$usua_doc = $_SESSION["usua_doc"];
$codusuario = $_SESSION["codusuario"];
$tpNumRad = $_SESSION["tpNumRad"];
$tpPerRad = $_SESSION["tpPerRad"];
$tpDescRad = $_SESSION["tpDescRad"];
$tip3Nombre = $_SESSION["tip3Nombre"];
$tip3img = $_SESSION["tip3img"];
$tpDepeRad = $_SESSION["tpDepeRad"];
$tip3desc = $_SESSION["tip3desc"];
$tip3img =$_SESSION["tip3img"];
$id_rol =$_SESSION["id_rol"];
$ruta_raiz = "..";
include($ruta_raiz.'/validadte.php');
$ruta_raiz = "../../";

require_once("$ruta_raiz/include/db/ConnectionHandler.php"); 
//Si no llega la dependencia recupera la sesion
if($tpDepeRad ){
	echo $tpDepeRad[$ent];
	echo "La dependencia no  tiene consecutivos consultar a su  administrador";
	die();
}

if (!$db)
		$db = new ConnectionHandler($ruta_raiz);
$phpsession = session_name()."=".session_id(); ?>
<html>
<head>
<link rel="stylesheet" href="../../estilos/orfeo.css">
</head>
<body bgcolor="#FFFFFF" topmargin="0" onLoad="window_onload();">
<table width="47%" align="center" border="0" cellpadding="0" cellspacing="5" class="borde_tab">
     <tr> 
      <td height="25" class="titulos4"> 
        RADICACI&Oacute;N MASIVA DE DOCUMENTOS
      </td>
    </tr>
    <tr align="center"> 
      <td class="listado2" > 
        <a href='upload2.php?<?php echo $phpsession ?>&krd=<?php echo $krd?>&krd=<?php echo $krd?>&<?php  echo "fechah=$fechah"; ?>' class="vinculos"  target='mainFrame'>Generar 
          Radicaci&oacute;n Masiva</a>
      </td>
    </tr>
    <tr align="center"> 
      <td class="listado2" > 
        <a  href="../cuerpo_masiva_recuperar_listado.php?<?php echo $phpsession ?>&krd=<?php echo $krd?>" class="vinculos">Recuperar 
          Listado</a>
      </td>
    </tr>
    <tr align="center"> 
      <td class="listado2" > 
        <a  href='consulta_depmuni.php?<?php echo $phpsession ?>&krd=<?php echo $krd?>&krd=<?php echo $krd?>&<?php  echo "fechah=$fechah"; ?>' class="vinculos" target='mainFrame'>Consultar 
          Divisi&oacute;n Pol&iacute;tica Administrativa de Colombia (DIVIPOLA)</a>
      </td>
    </tr>
    <tr align="center"> 
      <td class="listado2" > 
        <a  href='consultaESP.php?<?php echo $phpsession ?>&krd=<?php echo $krd?>&krd=<?php echo $krd?>&<?php  echo "fechah=$fechah"; ?>' target='mainFrame' class="vinculos">Consultar 
          ESP </a>
      </td>
    </tr>
    
  </table>
</body>
</html>
