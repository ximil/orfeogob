<?php 
//session_start(); date_default_timezone_set('America/Bogota'); date_default_timezone_set('America/Bogota');
include_once "$ruta_raiz/../core/config/config-inc.php";
if (!$no_planilla or intval($no_planilla) == 0) die ("<table class=borde_tab width='100%'><tr><td class=titulosError><center>Debe colocar un Numero de Planilla v&aacute;lido</center></td></tr></table>");
$no_planillaC=$no_planilla;
if($generar_listado)
{
   	error_reporting(7);
	$ruta_raiz = "..";
	
   	if (!defined('ADODB_FETCH_NUM'))	define('ADODB_FETCH_NUM',1);
	$ADODB_FETCH_MODE = ADODB_FETCH_NUM; 
	
	$fecha_ini = $fecha_busq;
    $fecha_fin = $fecha_busq;
	$fecha_ini = mktime($hora_ini,$minutos_ini,00,substr($fecha_ini,5,2),substr($fecha_ini,8,2),substr($fecha_ini,0,4));
	$fecha_fin = mktime($hora_fin,$minutos_fin,59,substr($fecha_fin,5,2),substr($fecha_fin,8,2),substr($fecha_fin,0,4));
	//$db->conn->debug = 	true;
	$fecha_ini1 = "$fecha_busq $hora_ini:$minutos_ini:00";
	$fecha_mes = "'" . substr($fecha_ini1,0,7) . "'";
	$sqlChar = $db->conn->SQLDate("Y-m","SGD_RENV_FECH");	

// Si la variable $generar_listado_existente viene entonces este if genera la planilla existente
	$order_isql = " ORDER BY SGD_RENV_CODIGO,SGD_RENV_VALOR";
	include "./oracle_pdf.php";
	$pdf = new PDF('L','pt','A3');
	$pdf->lmargin = 0.2;
	$pdf->SetFont('Arial','',8);
	$pdf->AliasNbPages();

	$head_table = array ("CANTIDAD","CATEGORIA DE CORRESPONDENCIA","NUMERO DE REGISTRO","DESTINATARIO","DESTINO","PESO EN GRAMOS","VALOR PORTE","CERTIFICADO","VALOR ASEGURADO","TASA DE SEGURO","VALOR REEMBOLSABLE","AVISO DE LLEGADA","SERVICIOS ESPECIALES","VALOR TOTAL PORTES Y TASAS");
	$head_table_size = array (57   ,90                            ,60                  ,234           ,104      ,53               ,44           ,74           ,72               ,55              ,88                  ,65                ,75                    ,80);
	$attr=array('titleFontSize'=>10,'titleText'=>'');
	//$arpdf_tmp = "../bodega/pdfs/planillas/$dependencia_". date("Ymd_hms") . "_jhlc.pdf"; Comentariada Por HLP.
	$arpdf_tmp = "../bodega/pdfs/planillas/".$dependencia."_".date("Ymd_hms")."_jhlc.pdf";
	$pdf->SetFont('Arial','',8);
	$pdf->usuario = $usua_nomb;
	$pdf->dependencia = $dependencianomb;
	$pdf->depe_municipio = $depe_municipio;
	$pdf->entidad_largo = $entidad_largo;
	$total_registros = 0;
	$pdf->lmargin = 0.2;
	$i_total3 = 0;
	if(!$generar_listado_existente)
	$wfecha=" AND " . $sqlChar . " = " . $fecha_mes ;
	else
	 $wfecha=' ';
	do
	{  // Amplia
		include "$ruta_raiz/include/query/radsalida/queryListado_planillas.php";	

		$pdf->planilla = $no_planilla;
		if($generar_listado_existente)
		{
			$where_isql = $where_isql2;
		}else
		{  
			$where_isql = $where_isql1;
	}

	$query_t = $query . $where_isql . $order_isql;
	$pdf->oracle_report($db,$query_t,false,$attr,$head_table,$head_table_size,$arpdf_tmp,0,31);
	
	if ($i_total3 == 0)  {
		$i_total3 = $pdf->numrows;
		$total_registros += $i_total3;
	}
	if($generar_listado_existente)
	{
			$i_total3 = 0;
	}else
		{
		error_reporting(7);

		$db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
		$rsParaUp = $db->conn->Execute("select * from sgd_renv_regenvio  $where_isql $order_isql");
		$nregis = 0 ;
		$rvCodigo = $rsParaUp->fields["SGD_RENV_CODIGO"] ;
		if ($rvCodigo)  {
//			$rsParaUp->MoveFirst();
			while (!$rsParaUp->EOF) {
				$nregis = $nregis + 1;
				$rsParaUp->MoveNext();
			}
		}
		if ($nregis > 0)  {
			$rsParaUp = $db->conn->Execute("select * from sgd_renv_regenvio  $where_isql $order_isql");
//			$rsParaUp->MoveFirst();	
			if ($nregis <= 32) $maximo = $nregis; else $maximo = 32;
			for ($cont = 1; $cont <= $maximo; $cont++ )
			{
				$renv_codigo = $rsParaUp->fields["SGD_RENV_CODIGO"];  
				include "$ruta_raiz/include/query/radsalida/queryListado_planillas.php";	
//				$wrc=" WHERE SGD_RENV_CODIGO = $renv_codigo AND SGD_RENV_PLANILLA = '' ";
				echo $update_isql = "update sgd_renv_regenvio set sgd_renv_planilla='$no_planilla' $wrc";
				//$rs = $db->query($update_isql);	
				$rsParaUp->MoveNext();
			}
		}
	}
	$no_planilla++;
	$iii++;
	$i_total3 = $i_total3 - 32 ;
	}while ($i_total3>0);
	$pdf->Output($arpdf_tmp);
}
?>
		<TABLE BORDER=0 WIDTH=100% class="borde_tab">
		<TR><TD class="listado2"  align="center"><center>
Se han Generado <b><?php echo $total_registros?> </b>Registros para Imprimir en <?php echo $paginas?> Planillas. <br>
<a href='<?php echo $arpdf_tmp?>' target='<?php echo date("dmYh").time("his")?>'>Abrir Archivo PDF</a></center>
</td>
</TR>
</TABLE>
<?php 		$rs = $db->query($query_t);

$head="<table width='100%' border='1'   style='border-collapse: collapse;font-style:normal;font-size:8;letter-spacing: 0.1em;font-stretch: ultra-expanded;'>
<tr><td  width='30%' align='center'>$entidad_largo_Planilla</td><td  width='30%' align='center'>ADMINISTRACION  POSTAL NACIONAL</td><td width='30%'></td></tr>
<tr><td></td><td  align='center'>Planilla para Consignacion</td><td  align='center'>Planilla No. $no_planillaC</td></tr>
<tr><td>Contrato No.</td><td  align='center'>Envios con Contrato</td><td align='center'>fecha ".date('d/m/Y')."</td></tr></table>"; 
$footer="<br><br><table width='100%' border='1'  cellpadding='0' cellspacing='0' style='border-collapse: collapse'>
<tr><td  width='50%' align='center'><br><br><br></td><td  width='50%' align='center'></td></tr>
<tr><td  width='50%' align='center'><br><br></td><td  width='50%' align='center'></td></td></tr>
</table>";
$contenido="<br><br><table width='100%' border='1'  cellpadding='0' cellspacing='0' style='border-collapse: collapse;font-style:normal;font-size:8;text-align:center;letter-spacing: 0.1em;font-stretch: ultra-expanded;vertical-align:inherit;'>
<tr bgcolor='#999999'><td>CANTIDAD</td><td>CATEGORIA DE CORRESPONDENCIA</td><td>NUMERO DE REGISTRO</td><td>DESTINATARIO</td><td>DESTINO</td><td>PESO EN GRAMOS</td><td>VALOR PORTE</td><td>CERTIFICADO</td>
<td>VALOR ASEGURADO</td><td>TASA DE SEGURO</td><td>VALOR REEMBOLSABLE</td><td>AVISO DE LLEGADA</td><td>SERVICIOS ESPECIALES</td><td>VALOR TOTAL PORTES Y TASAS</td></tr></table>";
echo $head.$contenido.$footer;

//   llamado Libreria dompdf
require_once("../dompdf/dompdf_config.inc.php");
// 	Inicializando la  clase
  $dompdf = new DOMPDF();
  //Creando  el  html
  //echo  $html=$head.$inf2.$inf3.$espacio.$inf2.$inf3.$footer;
   $html=$head.$contenido.$footer;
  //tomando los datos del  html
  $dompdf->load_html($html);
	//	tipo de palpe
  $dompdf->set_paper('legal','landscape');
  //$dompdf->set_paper('portrait');//Paper orientation ('portrait' or 'landscape') 
  // renderizando
  $dompdf->render();
  // ruta de guardado
  $arpdf_tmp = "../bodega/pdfs/planillas/prueba.pdf";
  //salida 
  $pdf = $dompdf->output();
  //Copiar al  disco
  file_put_contents($arpdf_tmp, $pdf);


?>
<a href='<?php echo $arpdf_tmp?>' target='<?php echo date("dmYh").time("his")?>'>Abrir Archivo PDF</a>
</body>