<?php 
/**
 * Este programa despliega el men� principal de correspondencia masiva
 * @author      Sixto Angel Pinz�n
 * @version     1.0
 */
//error_reporting(7);
session_start(); date_default_timezone_set('America/Bogota'); date_default_timezone_set('America/Bogota');
$ruta_raiz = "../../";

//require_once("$ruta_raiz/include/db/ConnectionHandler.php");
include_once "$ruta_raiz/include/db/ConnectionHandler.php" ;
//Si no llega la dependencia recupera la sesi�n
$krd = $_SESSION["krd"];
$dependencia = $_SESSION["dependencia"];
$usua_doc = $_SESSION["usua_doc"];
$codusuario = $_SESSION["codusuario"];
$tpNumRad = $_SESSION["tpNumRad"];
$tpPerRad = $_SESSION["tpPerRad"];
$tpDescRad = $_SESSION["tpDescRad"];
$tip3Nombre = $_SESSION["tip3Nombre"];
$tip3img = $_SESSION["tip3img"];
$tpDepeRad = $_SESSION["tpDepeRad"];
$tip3desc = $_SESSION["tip3desc"];
$tip3img =$_SESSION["tip3img"];
$id_rol =$_SESSION["id_rol"];

include($ruta_raiz.'/validadte.php');
if (!$db)	$db = new ConnectionHandler($ruta_raiz);

$phpsession = session_name()."=".session_id();
$db->conn->debug = true;

?>
<html>
<head>
<link rel="stylesheet" href="../../estilos/orfeo.css">
<script language="JavaScript">
<!--

function Start(URL, WIDTH, HEIGHT)
{
 windowprops = "top=0,left=0,location=no,status=no, menubar=no,scrollbars=yes, resizable=yes,width=";
 windowprops += WIDTH + ",height=" + HEIGHT;

 preview = window.open(URL , "preview", windowprops);
}

//-->
</script>

</head>
<body bgcolor="#FFFFFF" topmargin="0" onLoad="window_onload();">
<form id="frmGeneraSecuenciasMasiva" action='upload2.php?<?php echo $phpsession ?>&krd=<?php echo $krd?>&<?php  echo "fechah=$fechah"; ?>' method="POST">
<table width="47%" align="center" border="0" cellpadding="0" cellspacing="5" class="borde_tab">
<tr>
	<td height="25" class="titulos4" colspan="2">ADMINISTRACI&Oacute;N DE SECUENCIAS</td>
</tr>
<tr align="center">
	<td class="listado2" >
		<a href='javascript:Start("generarSecuencias.php?<?php echo $phpsession ?>&krd=<?php echo $krd?>&<?php  echo "fechah=$fechah"; ?>",500,300)' class="vinculos" target='mainFrame'>
			Separar secuencia</a>
	</td>
</tr>
<tr align="center">
	<td class="listado2" colspan="2" >
<!--		<center>-->
			<a href='upload2PorExcel.php?<?php echo $phpsession ?>&krd=<?php echo $krd?>&<?php  echo "fechah=$fechah"; ?>' class="vinculos" target='mainFrame'>
			Cargar Lista de Documentos generados</a>
<!--			<input type="button" value="Generar" class="botones" onclick="return advertencia();">-->
<!--		</center>-->
	</td>
</tr>
</table>
</form>
</body>
</html>
