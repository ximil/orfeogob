<?php session_start(); date_default_timezone_set('America/Bogota'); date_default_timezone_set('America/Bogota');

foreach ($_GET  as $key => $val){$$key = $val;}
foreach ($_POST as $key => $val){$$key = $val;}

$ruta_raiz = "../..";

$krd = $_SESSION["krd"];
$dependencia = $_SESSION["dependencia"];
$usua_doc = $_SESSION["usua_doc"];
$codusuario = $_SESSION["codusuario"];
$tpNumRad = $_SESSION["tpNumRad"];
$tpPerRad = $_SESSION["tpPerRad"];
$tpDescRad = $_SESSION["tpDescRad"];
$tip3Nombre = $_SESSION["tip3Nombre"];
$tip3img = $_SESSION["tip3img"];
$tpDepeRad = $_SESSION["tpDepeRad"];
$tip3desc = $_SESSION["tip3desc"];
$tip3img =$_SESSION["tip3img"];
$id_rol =$_SESSION["id_rol"];

include($ruta_raiz.'/validadte.php');

include_once "$ruta_raiz/include/db/ConnectionHandler.php";
include_once("$ruta_raiz/include/combos.php");

if (!$db)	$db = new ConnectionHandler($ruta_raiz);
//if (!defined('ADODB_FETCH_ASSOC'))	define('ADODB_FETCH_ASSOC',2);
$ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
//$db->conn->debug=true;

/**
 * Retorna la cantidad de bytes de una expresion como 7M, 4G u 8K.
 *
 * @param char $var
 * @return numeric
 */
function return_bytes($val)
{	$val = trim($val);
	$ultimo = strtolower($val{strlen($val)-1});
	switch($ultimo)
	{	// El modificador 'G' se encuentra disponible desde PHP 5.1.0
		case 'g':	$val *= 1024;
		case 'm':	$val *= 1024;
		case 'k':	$val *= 1024;
	}
	return $val;
}
?>
<html>
<head>
<link rel="stylesheet" href="../../estilos/orfeo.css">
</head>
<body bgcolor="#FFFFFF" topmargin="0">
<script language="JavaScript" type="text/JavaScript">

/**
* Valida que el formulario desplegado se encuentre adecuadamente diligenciado
*/
function validar() {
	archDocto = document.formAdjuntarArchivos.archivoPlantilla.value;
	//Adición Johnny: debe aceptar archivos .odt.
	if ( (archDocto.substring(archDocto.length-1-3,archDocto.length)).indexOf(".csv") == -1){
		alert ("El archivo de datos debe ser .csv");
		return false;
	}

	if (document.formAdjuntarArchivos.archivoPlantilla.value.length<1){
		alert ("Debe ingresar el archivo CSV con los datos");
		return false;
	}

	return true;
}

function enviar() {

	if (!validar())
		return;

	document.formAdjuntarArchivos.accion.value="PRUEBA";
	document.formAdjuntarArchivos.submit();
}

</script>

<?php 
if(!$tipoRad) $tipoRad = 1;

//TRD
include "tipificar_masivaExcel.php";

$params="dependencia=$dependencia&codiTRD=$codiTRD&depe_codi_territorial=$depe_codi_territorial&usua_nomb=$usua_nomb&depe_nomb=$depe_nomb&usua_doc=$usua_doc&tipo=$tipo&codusuario=$codusuario";
?>
	<form action="adjuntar_masivaExcel.php?<?php echo $params?>" method="post" enctype="multipart/form-data" name="formAdjuntarArchivos">
	<input type=hidden name=<?php echo session_name()?>  value='<?php echo session_id()?>'>
	<input type=hidden name=pNodo value='<?php echo $pNodo?>'>
	<input type=hidden name=codProceso value='<?php echo $codProceso?>'>
	<input type=hidden name=tipoRad value='<?php echo $tipoRad?>'>
       <table width="31%" align="center" border="0" cellpadding="0" cellspacing="5" class="borde_tab">
	<tr align="center">
		<td height="25" colspan="2" class="titulos4">TIPO DE RADICACI&oacute;N</td>
	</tr>
	<tr align="center">
		<td width="16%" class="titulos2">Seleccione: </td>
		<td width="84%" height="30" class="listado2">
			<?php 				$cad = "USUA_PRAD_TP";
				// Creacion del combo de Tipos de radicado habilitados seg�n permisos
				 $sql = "SELECT SGD_TRAD_CODIGO,SGD_TRAD_DESCR FROM SGD_TRAD_TIPORAD 
				       WHERE SGD_TRAD_GENRADSAL > 0";	//Buscamos los TRAD En la entidad
				$Vec_Trad = $db->conn->GetAssoc($sql);
				$Vec_Perm = array();
				/*while (list($id, $val) = each($Vec_Trad))
				{	$sql = "SELECT ".$cad.$id." FROM USUARIO WHERE USUA_LOGIN='".$krd."'";
					$rs2 = $db->conn->Execute($sql);
					if  ($rs2->fields[$cad.$id] > 0)
					{	$Vec_Perm[$id] = $val;
					}
				}*/
				while (list($id, $val) = each($Vec_Trad))
				{	
					$ooo='usua_prad_tp'.$id;
					//echo $_SESSION[$ooo];
					if  ($_SESSION[$ooo] > 0)
					{	$Vec_Perm[$id] = $val;
					}
				}
				//print_r($Vec_Perm);
				reset($Vec_Perm);
			?>
			<select name="tipoRad" id="Slc_Trd" class="select" onchange="submit();">
				<?php 
				while (list($id, $val) = each($Vec_Perm))
				{
					if($tipoRad==$id) $datoss = " selected "; else $datoss="";
					echo "<option value=".$id." $datoss>$val</option>";
				}
				?>
			</select>
		</td>
	</tr>
</table><br/> 
    <!--<table width="70%" align="center" border="0" cellpadding="0" cellspacing="5" class="borde_tab">
    <tr align="center">
        <td height="25" colspan="2" class="titulos4">TIPO DE RADICACI&oacute;N</td>
        <td width="84%" height="30" class="listado2">
        <?php /*
        $cad      = "USUA_PRAD_TP";
        $sql      = "SELECT SGD_TRAD_CODIGO,SGD_TRAD_DESCR FROM SGD_TRAD_TIPORAD WHERE SGD_TRAD_GENRADSAL > 0";	//Buscamos los TRAD En la entidad
        $Vec_Trad = $db->conn->GetAssoc($sql);
        $Vec_Perm = array();

        while (list($id, $val) = each($Vec_Trad)){	
            $sql = "SELECT ".$cad.$id." FROM USUARIO WHERE USUA_LOGIN='".$krd."'";
            $rs2 = $db->conn->Execute($sql);
            if  ($rs2->fields[$cad.$id] > 0){	
                $Vec_Perm[$id] = $val;
            }
        }

                    reset($Vec_Perm);
                    ?>
                    <select name="tipoRad" id="Slc_Trd" class="select">
                    <option value="0">Seleccione una opci&oacute;n</option>
                    <?
                    while (list($id, $val) = each($Vec_Perm)){
                        if($tipoRad==$id) $datoss = " selected "; else $datoss="";
                        echo " <option value=".$id." $datoss>$val</option>";
                    }*/
                    ?>
                    </select>
                </td>
            </tr>
    </table>
    </br>-->
	<table width="70%" align="center" border="0" cellpadding="0" cellspacing="5" class="borde_tab">
			<tr align="center">
				<td height="25" colspan="2" class="titulos4">
					ADJUNTAR ARCHIVO CON COMBINACI&Oacute;N
					<input type="hidden" name="MAX_FILE_SIZE" value="<?php echo return_bytes(ini_get('upload_max_filesize')); ?>">
					<input name="accion" type="hidden" id="accion">
				</td>
			</tr>
			<tr align="center">
				<td width="16%" class="titulos2">LISTADO </td>
				<td width="84%" height="30" class="listado2">
					<input name="archivoPlantilla" type="file" value='<?php echo $archivoPlantilla?>' class="tex_area"  id=archivoPlantilla accept="text/csv">
				</td>
			</tr>
			<tr align="center">
				<td height="30" colspan="2" class="celdaGris">
					<span class="celdaGris"> <span class="e_texto1">
					<input name="enviaPrueba" type="button"  class="botones" id="envia22"  onClick="enviar();" value="Radicar">
					</span></span>
				</td>
			</tr>
			<tr align="center">
				<td height="30" colspan="2" class="celdaGris">
					Esta operaci&oacute;n generar&aacute; un radicado 
					por cada registro del archivo CSV de origen. Por 
					favor tenga cuidado con esta opci&oacute;n ya que 
					se realizar&aacute; cambios irreversibles en el 
					sistema.
				</td>
			</tr>
		</table>
	</form>
</body>
</html>
