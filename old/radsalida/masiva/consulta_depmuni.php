<?php 
/**
 * Este programa despliega el formulario con los par�metro de selecci�n que para consultar departamento y municipio
 * @author      Sixto Angel Pinz�n
 * @version     1.0
 */
session_start();
$ruta_raiz = "../..";

//Si no llega la dependencia recupera la sesi�n	
$krd = $_SESSION["krd"];
$dependencia = $_SESSION["dependencia"];
$usua_doc = $_SESSION["usua_doc"];
$codusuario = $_SESSION["codusuario"];
$tpNumRad = $_SESSION["tpNumRad"];
$tpPerRad = $_SESSION["tpPerRad"];
$tpDescRad = $_SESSION["tpDescRad"];
$tip3Nombre = $_SESSION["tip3Nombre"];
$tip3img = $_SESSION["tip3img"];
$tpDepeRad = $_SESSION["tpDepeRad"];
$tip3desc = $_SESSION["tip3desc"];
$tip3img =$_SESSION["tip3img"];
$id_rol =$_SESSION["id_rol"];

include($ruta_raiz.'/validadte.php');
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Untitled Document</title>
<link rel="stylesheet" href="../../estilos/orfeo.css">
<script language="JavaScript" type="text/JavaScript">
/**
* Valida que el formulario desplegado se encuentre adecuadamente diligenciado y si lo est� entonces env�a el formulario
*/
function enviar()
{	if (document.consultar.consulta.value.length >0)
	{	document.consultar.submit();	}
	else
	{	alert("Debe ingresar el dato a consultar");	}
}
</script>
</head>
<body>
<form action="resultado_consulta_depmuni.php?<?php echo $phpsession?>&krd=<?php echo $krd?>&dependencia=<?php echo $dependencia?>" method="post" enctype="multipart/form-data" name="consultar" id="consultar">
<table width="45%" border="0" cellspacing="5" cellpadding="0" align="center" class="borde_tab">
	<tr align="center"  class="titulos2"> 
		<td height="25" class="titulos2">
			RADICACION MASIVA <BR>CONSULTA DE LA DIVISION POLITICA ADMINISTRATIVA<BR>(DIVIPOLA)
		</td>
	</tr>
	<tr align="center" class='listado2'> 
		<td class='listado2' height="12"><span class="etextomenu">
			<BR>Efect&uacute;e la b&uacute;squeda por el nombre del departamento o municipio.<BR><BR>
			<center><input name="consulta" type="input" size="50" class="tex_area"  id="consulta"></center>
		</td>
	</tr>
	<tr align="center" class="listado2"> 
		<td height="30" class="listado2">
        	<center><input name="enviaPrueba" type="button"  class="botones" id="enviaPrueba"  onClick="enviar();" value="Consultar"></center>
        </td>
	</tr>
</table>
</form>
</body>
</html>
