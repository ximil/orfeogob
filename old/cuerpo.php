<?php putenv("NLS_LANG=American_America.UTF8");
session_start(); date_default_timezone_set('America/Bogota');
// Modificado SGD 20-Septiembre-2007
/**
  * Paggina Cuerpo.php que muestra el contenido de las Carpetas
	* Creado en la SSPD en el año 2003
  * 
	* Se añadio compatibilidad con variables globales en Off
  * @autor Jairo Losada 2009-05
  * @licencia GNU/GPL V 3
  */

foreach ($_GET as $key => $valor)  $$key = $valor;
foreach ($_POST as $key => $valor)  $$key = $valor;
$nomcarpeta=$_GET["nomcarpeta"];
if($_GET["tipo_carp"])  $tipo_carp = $_GET["tipo_carp"];
//$carpeta=$_GET['carpeta'];
//print_r($_SESSION);
//print_r($_GET);
define('ADODB_ASSOC_CASE', 2);

$krd = $_SESSION["krd"];
$dependencia = $_SESSION["dependencia"];
$usua_doc = $_SESSION["usua_doc"];
$codusuario = $_SESSION["codusuario"];
$tip3Nombre=$_SESSION["tip3Nombre"];
$tip3desc = $_SESSION["tip3desc"];
$tip3img =$_SESSION["tip3img"];
$id_rol =$_SESSION["id_rol"];
//echo $_SESSION["rows_per_page"]." ||". $_SESSION["rows_per_page"]."!=".$rows_per_page;
if(!$_SESSION["rows_per_page"] ||  ($_SESSION["rows_per_page"]!=$_GET['rows_per_page'] and $_GET['rows_per_page'])){
   $_SESSION["rows_per_page"]=$_GET['rows_per_page'];
   $rows_per_page=$_SESSION["rows_per_page"];
}
    else
   $rows_per_page=$_SESSION["rows_per_page"];
$ruta_raiz = ".";
include($ruta_raiz.'/validadte.php');
include($ruta_raiz.'/../core/clases/timecount.php');
$timerRespuesta= new timecount();

error_reporting(7);
$verrad = "";
$_SESSION['numExpedienteSelected'] = null;
$scriptname='cuerpo.php';
?>
<html >
<head>
<link rel="stylesheet" href="estilos/orfeo.css">
<script src="js/popcalendar.js"></script>
<script src="js/mensajeria.js"></script>
 
</head>

<body bgcolor="#FFFFFF" topmargin="0" onLoad="window_onload();">
<div id="spiffycalendar" class="text"></div>
<?php     include "./envios/paEncabeza.php";
	include_once "./include/db/ConnectionHandler.php";
	require_once("$ruta_raiz/class_control/Mensaje.php");
	if (!$db) $db = new ConnectionHandler($ruta_raiz);
	$db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
	//$db->conn->debug=true;
	$objMensaje= new Mensaje($db);
	$mesajes = $objMensaje->getMsgsUsr($_SESSION['usua_doc'],$_SESSION['dependencia']);

if ($swLog==1)
	echo ($mesajes);
	  if(trim($orderTipo)=="") $orderTipo="DESC";
  if($orden_cambio==1)
	{
	  if(trim($orderTipo)!="DESC")
		{
		   $orderTipo="DESC";
		}else
		{
			$orderTipo="ASC";
		}
	}

	if(!$carpeta) $carpeta=0;
	if($busqRadicados)
	{
    $busqRadicados = trim($busqRadicados);
    $textElements = split (",", $busqRadicados);
    $newText = "";
	$dep_sel = $dependencia;
    foreach ($textElements as $item)
    {
         $item = trim ( $item );
         if ( strlen ( $item ) != 0)
		 {
 	        $busqRadicadosTmp .= " b.radi_nume_radi like '%$item%' or";
		 }
    }
	if(substr($busqRadicadosTmp,-2)=="or")
	{
	 $busqRadicadosTmp = substr($busqRadicadosTmp,0,strlen($busqRadicadosTmp)-2);
	}
	if(trim($busqRadicadosTmp))
	{
	 $whereFiltro .= "and ( $busqRadicadosTmp ) ";
	}

	}
   $encabezado = "".session_name()."=".session_id()."&depeBuscada=$depeBuscada&filtroSelect=$filtroSelect&tpAnulacion=$tpAnulacion&carpeta=$carpeta&tipo_carp=$tipo_carp&chkCarpeta=$chkCarpeta&busqRadicados=$busqRadicados&nomcarpeta=$nomcarpeta&agendado=$agendado&";
   $linkPagina = "$PHP_SELF?$encabezado&orderTipo=$orderTipo&orderNo=$orderNo";
   $encabezado = "".session_name()."=".session_id()."&adodb_next_page=1&depeBuscada=$depeBuscada&filtroSelect=$filtroSelect&tpAnulacion=$tpAnulacion&carpeta=$carpeta&tipo_carp=$tipo_carp&nomcarpeta=$nomcarpeta&agendado=$agendado&orderTipo=$orderTipo&orderNo=";
?>
<table style='width:100%;border-spacing: 0;padding: 0; margin-bottom: 0;margin: 0;margin-top: 0' align="center"  class="borde_tab">
<tr class="tablas">
	<td>
	<span class="etextomenu">
	<form name="form_busq_rad" id="form_busq_rad" action="<?php echo $_SERVER['PHP_SELF']?>?<?php echo $encabezado?>" method="post" style='border-spacing: 0;padding: 0; margin-bottom: 0;margin: 0;margin-top: 0'>
			Buscar radicado(s) (Separados por coma)<span class="etextomenu">
	   	   <input name="busqRadicados" type="text" size="40" class="tex_area" value="<?php echo $busqRadicados?>">
	       <input type=submit value='Buscar ' name=Buscar valign='middle' class='botones'>
        </span>
        <?php 		/**
		  * Este if verifica si se debe buscar en los radicados de todas las carpetas.
		  * @$chkCarpeta char  Variable que indica si se busca en todas las carpetas.
		  *
		  */
		if($chkCarpeta)
		{
			$chkValue=" checked ";
			$whereCarpeta = " ";
		}
		else
		{
      $chkValue="";
			$whereCarpeta = " and b.carp_codi=$carpeta ";
		}
	
	if(!$tipo_carp) $tipo_carp=0;

  $whereCarpeta = $whereCarpeta ." and b.carp_per=$tipo_carp ";
	$fecha_hoy = Date("Y-m-d");
	$sqlFechaHoy=$db->conn->DBDate($fecha_hoy);

	//Filtra el query para documentos agendados
	 if ($agendado==1){
	 	$sqlAgendado=" and (radi_agend=1 and radi_fech_agend > $sqlFechaHoy) "; // No vencidos

	 }
	 else  if ($agendado==2){
	   	$sqlAgendado=" and (radi_agend=1 and radi_fech_agend <= $sqlFechaHoy)  "; // vencidos
	 }


	 if ($agendado){
	 	$colAgendado = "," .$db->conn->SQLDate("Y-m-d H:i A","b.RADI_FECH_AGEND").' as "Fecha Agendado"';
	 	$whereCarpeta="";
	 }

	//Filtra teniendo en cienta que se trate de la carpeta Vb.
	 if($carpeta==11 && $id_rol !=1 && $_GET['tipo_carp']!=1)
	 {	$whereUsuario = " and  b.radi_usu_ante ='$krd' ";
	 }
	 else
	 {	
		$whereUsuario = " and b.radi_usua_actu='$codusuario' ";
	 }
	?>
   <input type="checkbox" name="chkCarpeta" value=xxx <?php echo $chkValue?> > Todas las carpetas
	</form>
			 </span>
			</td>
		  </tr>
	 </table>
    <form name="form1" id="form1" action="./tx/formEnvio.php?<?php echo $encabezado?>" method="GET" style="border-spacing: 0;padding: 0; margin-bottom: 0;margin: 0;margin-top: 0">

  <?php   $controlAgenda=1;
	if($carpeta==11 and !$tipo_carp and $id_rol!=1)
	{
	}else
	{	
  include "./tx/txOrfeo.php"; 
	}
	/*  GENERACION LISTADO DE RADICADOS
	 *  Aqui utilizamos la clase adodb para generar el listado de los radicados
	 *  Esta clase cuenta con una adaptacion a las clases utiilzadas de orfeo.
	 *  el archivo original es adodb-pager.inc.php la modificada es adodb-paginacion.inc.php
	 */
	error_reporting(7);

	if(strlen($orderNo)==0)
	{
		$orderNo="2";
		$order = 3;
	}else
	{
		$order = $orderNo +1;
	}

	$sqlFecha = $db->conn->SQLDate("Y-m-d H:i A","b.RADI_FECH_RADI");
	//$sqlFecha = $db->conn->DBDate("b.RADI_FECH_RADI", "d-m-Y H:i A");
	//$sqlFecha = $db->conn->DBTimeStamp("b.RADI_FECH_RADI","" ,"Y-m-d H:i:s");
  //$db->SQLDate('Y-\QQ');
	//$db->conn->debug = true;

	include "$ruta_raiz/include/query/queryCuerpo.php";
        if(!$rows_per_page)
	   $rows_per_page=20;
        //echo $rows_per_page;
        $timerRespuesta->intertime('Antes de consultar');
	$rs=$db->conn->Execute($isql);
	if ($rs->EOF and $busqRadicados)  {
		echo "<hr><center><b><span class='alarmas'>No se encuentra ningun radicado con el criterio de busqueda</span></center></b></hr>";
	}
	else{
           // $db->conn->debug=true;
		//echo $adodb_next_page=$_GET['adodb_next_page'];
            $timerRespuesta->intertime('Antes de Render');
		$pager = new ADODB_Pager($db,$isql,'adodb', true,$orderNo,$orderTipo);
		$pager->checkAll = false;
		$pager->checkTitulo = true;
		$pager->toRefLinks = $linkPagina;
		$pager->toRefVars = $encabezado;
		//if(adodb_next_page)
		  //$pager->pagConsulta = $adodb_next_page;
		$pager->descCarpetasGen=$descCarpetasGen;
		$pager->descCarpetasPer=$descCarpetasPer;
		$pager->Render($rows_per_page,$linkPagina,$checkbox=chkAnulados);
                
	}
	?>
	</form>
                                                    <?php // if($scriptname=='cuerpo.php'){        ?>
       <form name="list" id="list" action="cuerpo.php?<?php echo $encabezado?>" method="post" style='border-spacing: 0;padding: 0; margin-bottom: 0;margin: 0;margin-top: 0'>
                                            <!--<select id="rows_per_page" name='rows_per_page'>
                                                        <?php                                                 /*       $index = 10; 
                                                        while ($index <= 100)  {
                                                           $selected='';
                                                           if($rows_per_page==$index)
                                                               $selected='Selected';
                                                                echo "<option $selected>$index</option>";
                                                                $index=$index+10;
                                                        } */
                                                        ?>
                                            </select>
                                                <input type="submit" value="Listar">-->
                                            </form>
                                                <?php //} ?>
</tr>
</td>
</table>
<div style='alignment-adjust: auto;text-align: right;font-size: 10'>
<?php $timerRespuesta->endtime(); 
      $timerRespuesta->printToIntervalo();?>
</div>
</body>
</html>
