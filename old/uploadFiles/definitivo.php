<?php putenv("NLS_LANG=American_America.UTF8");
session_start(); date_default_timezone_set('America/Bogota'); date_default_timezone_set('America/Bogota');

foreach ($_GET as $key => $valor)   $$key = $valor;
foreach ($_POST as $key => $valor)   $$key = $valor;

$krd = $_SESSION["krd"];
$dependencia = $_SESSION["dependencia"];
$usua_doc = $_SESSION["usua_doc"];
$codusuario = $_SESSION["codusuario"];
$depe_nomb  = $_SESSION["depe_nomb"];
$usua_nomb  = $_SESSION["usua_nomb"];
$id_rol     = $_SESSION["id_rol"];
$ruta_raiz = "..";


/**
 * Retorna la cantidad de bytes de una expresion como 7M, 4G u 8K.
 *
 * @param char $var
 * @return numeric
 */
function return_bytes($val)
{	$val = trim($val);
	$ultimo = strtolower($val{strlen($val)-1});
	switch($ultimo)
	{	// El modificador 'G' se encuentra disponible desde PHP 5.1.0
		case 'g':	$val *= 1024;
		case 'm':	$val *= 1024;
		case 'k':	$val *= 1024;
	}
	return $val;
}
include_once "$ruta_raiz/include/db/ConnectionHandler.php";
   $db = new ConnectionHandler("$ruta_raiz");
   $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
?>
<html>
    <head>
	<title>Imagen definitiva del Memorando</title>
        <link rel="stylesheet" href="<?php echo $ruta_raiz;?>/estilos/default/orfeo.css">
        <link rel="stylesheet" href="<?php echo $ruta_raiz; ?>/estilos/Style.css" type="text/css">
        <script language="javascript">
function notSupported(){ alert('Su browser no soporta las funciones Javascript de esta p�ina.'); }
function validar()
{       
    if (document.txImg.upload.value.length == 0)
        {
            alert('Seleccione la imagen a adjutar...');
            document.txImg.upload.focus();
            return false;
        }
        else return true;
}
function validar_forma_arch(field_name, allowed_ext){
    obj1=document.txImg;
    var temp_field= 'obj1.'+field_name+'.value';
    field_value=eval(temp_field);
    if(field_value!=""){
        var file_ext= (field_value.substring((field_value.lastIndexOf('.')+1)).toLowerCase());
        ext=allowed_ext.split(',');
        var allow=0;
        for ( var i=0; i < ext.length; i++) {
                if(ext[i]==file_ext){
                    allow=1;
                }
        }
        if(!allow){
            alert('Formato invalidado. Por favor verique sea formato '+allowed_ext);        
            return false;
            }
    	}
    return false;
    }
    function regresar(){
        document.DocDefi.submit();
    }
	</script>
    </head>
    <body>
	<header>
	    <table width="100%"  border="0" cellpadding="0" cellspacing="2" class="tabS">
                <tr>
                    <td class="titulos4" style="width:33%"><center><br><?php echo$depe_nomb ?></center></td>
                    <td class="titulos4" style="width:33%"><center><br><?php echo $usua_nomb;?></center></td>
                    <td class="titulos4" style="width:33%">Asociaci&oacute;n de imagen definitiva del Radicado</td>
                </tr>
             </table>
	</header>
        <?php 
        if(!isset($doform))
        {?>
        <form method="post" name="txImg" enctype="multipart/form-data">
	<table width="500"  border=0 align="center" bgcolor="White">
	    <tr bgcolor='white'>
		<td><center><input type="hidden" name="MAX_FILE_SIZE" value="<?php echo return_bytes(ini_get('upload_max_filesize')); ?>">
		<span class="leidos">Seleccione un Archivo (pdf  Tama&ntilde;o Max. <?php echo ini_get('upload_max_filesize')?>)<input type="file" name="upload" size="50" class=tex_area onchange="validar_forma_arch('upload','pdf')"></span></center></td>
	    </tr>
	</table>
	<?php 	$isql="select radi_nume_radi as numrad, ra_asun as asunto,radi_fech_radi as fecha from radicado where radi_nume_radi=$numeradi";
	$rs=$db->conn->Execute($isql);
	$numrad=$rs->fields['NUMRAD'];
	$asun=$rs->fields['ASUNTO'];
	$fecha=$rs->fields['FECHA'];
	?>
        <center>
	<table border=0 width="70%" class="border_tab">
		<tr class="titulo1"><td>N&uacute;mero de Radicado</td><td>Fecha de Radicado</td><td>Asunto</td></tr>
		<tr class="listado2"><td><?php echo $numrad?></td><td><?php echo $fecha?></td><td><?php echo $asun?></td></tr>
	</table>
        </center>
            <input type="hidden" name="numeradi" value="<?php echo $numeradi?>">
    <center>
        <input type="submit" name="doform" class="botones_largo" value="Actualizar" onclick="return validar();">
    </center>
    </form>
    <?php         }else{
            
            include ("$ruta_raiz/include/upload/upload_class.php");
            $max_size = return_bytes(ini_get('upload_max_filesize')); // the max. size for uploading
$my_upload = new file_upload;
$my_upload->language="es";
$my_upload->upload_dir = "$ruta_raiz/bodega/tmp/"; // "files" is the folder for the uploaded files (you have to create this folder)
$my_upload->extensions = array(".pdf"); // specify the allowed extensions here
//$my_upload->extensions = "de"; // use this to switch the messages into an other language (translate first!!!)
$my_upload->max_length_filename = 50; // change this value to fit your field length in your database (standard 100)
$my_upload->rename_file = true;
        $tmpFile = trim($_FILES['upload']['name']);
        $newFile = $numeradi;
        $uploadDir = "$ruta_raiz/bodega/".substr($numeradi,0,4)."/".substr($numeradi,4,3)."/";
        $fileGrb = substr($numeradi,0,4)."/".substr($numeradi,4,3)."/$numeradi".".".strtolower(substr($tmpFile,-3));
        $my_upload->upload_dir = $uploadDir;
        $my_upload->the_temp_file = $_FILES['upload']['tmp_name'];
        $my_upload->the_file = $_FILES['upload']['name'];
        $my_upload->http_error = $_FILES['upload']['error'];
        $my_upload->replace = (isset($_POST['replace'])) ? $_POST['replace'] : "n"; // because only a checked checkboxes is true
        $my_upload->do_filename_check = (isset($_POST['check'])) ? $_POST['check'] : "n"; // use this boolean to check for a valid filename
        //$new_name = (isset($_POST['name'])) ? $_POST['name'] : "";
        if ($my_upload->upload($newFile)) {
                // new name is an additional filename information, use this to rename the uploaded file
                $full_path = $my_upload->upload_dir.$my_upload->file_copy;
                $info = $my_upload->get_uploaded_file_info($full_path);
                // ... or do something like insert the filename to the database

        }else
        {

                        die("<table class=borde_tab><tr><td class=titulosError>Ocurrio un Error la Fila no fue cargada Correctamente <p>".$my_upload->show_error_string()."<br><blockquote>".nl2br($info)."</blockquote></td></tr></table>");
        }
  $query = "update radicado
                        set radi_path='$fileGrb'
                        where radi_nume_radi=$numeradi";
    if($db->conn->Execute($query))
        {
            $radicadosSel[] = $numeradi;
            $codTx = 42;    //C�digo de la transacci�n
            include "$ruta_raiz/include/tx/Historico.php";
            $hist = new Historico($db);
            $observa="Asociacion definitiva del Memo";
            $hist->insertarHistorico($radicadosSel,  $dependencia , $codusuario,$id_rol, $dependencia, $codusuario,$id_rol, $observa, $codTx);
            echo "<form method='post' name='DocDefi'>";
            echo "<table class='border_tab' width='100%'><tr class='titulos2'><td><b>Asociaci&oacute;n de imagen realizada con exito</b></td></tr></table>\n";
            echo "<center><input type='button' value='cerrar' class='botones_largo' onclick='opener.regresar();window.close();'></center>";
            echo "</form>";
        }else{
        echo "<hr>No actualizo la BD <hr>";
        }


    }
    ?>
    </body>
</html>
