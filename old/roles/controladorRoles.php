<?php include('modeloRoles.php');
/** 
 * @author deimont
 * 
 * 
 */
class controladorRoles {
	//TODO - Insert your code here
	var $modelo;        

	function __construct($db) {
		$this->modelo=  new modeloRoles($db);  
	}
	
	function consultar()
	{	
		$resultado=$this->modelo->consultar();	
		
		return $resultado;
	}
	
	function modificar($id,$nombre,$tipo) {
		$resultado=$this->modelo->modificar($id,$nombre,$tipo);	
		return $resultado;
	}
	function crear($nombre,$tipo) {
		$resultado=$this->modelo->crear($nombre,$tipo);	
		return $resultado;
	}
	function consultarScript($idrol) {
		$resultado=$this->modelo->consultarScript($idrol);	
		
		return $resultado;
	}
	
    function consultarScriptAnadir($idrol) {
		$resultado=$this->modelo->consultarScriptAnadir($idrol);	
		
		return $resultado;
	}
function consultarrScr($idrol, $idscripts, $modificar, $crear, $hab, $listar) {
		$resultado=$this->modelo->crearRScr($idrol, $idscripts, $modificar, $crear, $hab, $listar);	
		
		return $resultado;
	}
	function consultarMScr($id, $modificar, $crear, $hab, $listar) {
		$resultado=$this->modelo->consultarMScr($id, $modificar, $crear, $hab, $listar);	
		
		return $resultado;
	} 
}

?>