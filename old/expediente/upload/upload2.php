<?php session_start();
date_default_timezone_set('America/Bogota'); date_default_timezone_set('America/Bogota');
$ruta_raiz = "../..";
foreach ($_GET as $key => $valor)   ${$key} = $valor;
foreach ($_POST as $key => $valor)   ${$key} = $valor;
define('ADODB_ASSOC_CASE', 1);
$verrad         = "";
$krd            = $_SESSION["krd"];
$dependencia    = $_SESSION["dependencia"];
$usua_doc       = $_SESSION["usua_doc"];
$codusuario     = $_SESSION["codusuario"];
if(!$usua_doc) $usua_doc = "0000";
error_reporting(-1);
ini_set("display_errors", "On");

include_once    ("$ruta_raiz/include/db/ConnectionHandler.php");
require_once    ("$ruta_raiz/class_control/Mensaje.php");

$db = new ConnectionHandler($ruta_raiz);
$db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
$db->conn->debug = false;	
include $ruta_raiz . "/include/tx/Expediente.php";
$exp = new Expediente($db);
include $ruta_raiz . "/include/tx/Historico.php";
$hist= new Historico ($db);
?>
<html>
<head>
    <link rel="stylesheet" href="<?php echo $ruta_raiz."/estilos/orfeo.css"?>">
</head>
<body>

<form action="upload2.php" method="post" enctype="multipart/form-data">
<table class='borde_tab' align=center width='70%'>
<tr class=listado1>
 <td colspan=3 align=center class=titulos2>
  Cargue de Expedientes Masivos 
  </td>
</tr>
<tr class=listado1>
  <td class=titulos2><label for="file">Archivo:</label></td>
  <td><input type="file" name="file" id="file"></td>
</tr><tr class=listado1>
 <td colspan=3 align=center class=titulos2>
  <input type="submit" name="submit" value="Carga CSV">
  </td>
</tr>
<tr bordercolor="#FFFFFF">
        <td width="3%" align="justify" class="info" colspan="3" bgcolor="#FFFFFF"><b>Precauci&oacute;n:</b> Esta secci&oacute;n hace cargue directo a la base de datos, por tanto de ingresar un n&uacute;mero valido de Expediente ser&aacute; creado de forma directa y quedar&aacute; as&iacute; de forma permanente
</td>
</tr>
</table>
</form>  


<?php $allowedExts = array("gif", "jpeg", "jpg", "png", "csv", "txt");
$temp = explode(".", $_FILES["file"]["name"]);
$extension = end($temp);
echo $_FILES["file"]["type"] ." ." . $_FILES["file"]["size"];
if ((($_FILES["file"]["type"] == "text/csv")) || ($_FILES["file"]["type"] == "text/txt") || ($_FILES["file"]["type"] == "application/octet-stream") ||  ($_FILES["file"]["type"] == "text/comma-separated-values") || ($_FILES["file"]["type"] == "text/csv") || ($_FILES["file"]["type"] == "application/csv") || ($_FILES["file"]["type"] == "application/excel") || ($_FILES["file"]["type"] == "application/vnd.ms-excel") || ($_FILES["file"]["type"] == "application/vnd.msexcel"))
  {
  if ($_FILES["file"]["error"] > 0)
    {
    echo "Error al Cargar Archivo: " . $_FILES["file"]["error"] . "<br>";
    }else{
      $pathDestino = "$ruta_raiz/bodega/tmp/".$_FILES["file"]["name"];
      if(move_uploaded_file($_FILES["file"]["tmp_name"], $pathDestino )){
      echo "Archivo: ". $pathDestino;
      }else{ echo "No se Cargo el Archivo";
      }
    }
  }
else
  {
    die( "Archivo No valido.");
  }
$fp = fopen($pathDestino,"r");

?>

<table class=borde_tab>
<tr class='titulo1'>
    <th>Descripci&oacute;n</th>
    <th>Expediente</th>
    <th>Radicado</th>
    <th>Titulo o Nombre</th>
    <th>Asunto u Objeto</th>
</tr>
<?php $iFile = 0;
 while(!feof($fp)) {
   $linea =  fgets($fp);
   $linea = str_replace(";",",",$linea);
   $arrDatos = explode(",",$linea);
   echo "<tr class=listado2>";
   $numeroMF = array();
   $i=0;
   $datoMal = "No";
   if($iFile>=1){
   foreach($arrDatos as $rad){
     $rad = str_replace('"','',$rad);
     $errad=0;
     if($i==0) { $texp_num=trim($rad);
					if(strlen($texp_num)==17 &&  substr($texp_num,-1)=='E' && is_numeric(substr($texp_num,0,-1))){
						//echo "Se creara el Exp  <$rad> ";
						//echo $exp->existeExpediente($rad) . "";
						//$db->conn->debug = true;
					  if($exp->existeExpediente($rad)==0){
					    if($statexp=$exp->verifExped($texp_num)==1){
					      $serie=substr($rad,7,2);
				              $subserie=substr($rad,9,2);
					      $depexpe = substr($rad,4,3);
					      echo "<td>Exp: $rad <Dependencia:$depexpe> <Doc: $usua_doc> </td>";
					      $expediente = $rad;
					      $numExpediente = $exp->crearExpediente($expediente,'',$depexpe,1,$usua_doc,$usua_doc,$serie,$subserie, 'false',null,0,$numeroMF);
					      $insertarExp = "Si";
					      $res=1;
					      $radnum=end($arrDatos);
					      }
					    else{
					      $insertarExp="Error dependencia,serie o subserie no validas para crear expedientes";
					      $res=0;
					    }
					  }else{
					   echo "<td>Exp: $rad <Dependencia:$dependencia> <Doc: $usua_doc> </td>";
					   $insertarExp = "Ya existe";
					   $numExpediente=trim($rad);
					   $res=5;
					  }
					
					}else{
					  //echo "Expediente erroneo.";
					  $insertarExp = "Error Formato de expediente,<br>(Debe contener 17 Digitos,16 N&uacute;meros y al Final la letra E) $radnum";
					  $res=0;
					}
      $numeroMF[1] = $rad;
     }

     if($i==2 && substr($insertarExp,0,5)!="Error"){
	$titulo=$rad;
	$insertarExp="";
     }
     elseif($i==3 && substr($insertarExp,0,5)!="Error"){
	$asunto=$rad;
	$insertarExp="";
     }
     if($i==1  && substr($insertarExp,0,5)!="Error"){
      if(strlen(trim($rad))==14 && is_numeric(trim($rad))){
           if($exp->verifExpedAbierto($numExpediente)==2){
		$res=0;
		$insertarExp .= "(Cerrado)";
            	$errad=1;
	   }
	   else{
               $res = $exp->insertar_expediente($numExpediente,$rad,$dependencia,$codusuario,$usua_doc);
	       $radicados[0]=$rad;
	       if($res==1 && $i==1 && $insertarExp=="Si"){
		   $observa="Creacion de expediente por Carga Masiva";
		   $hist->insertarHistoricoExp($numExpediente,$radicados,$dependencia,$codusuario,$observa,51,0);
		   $insertarExp="Creado";
	       }elseif($res==1){
		   $observa="Incluido al expediente por Carga Masiva";
                   $hist->insertarHistoricoExp($numExpediente,$radicados,$dependencia,$codusuario,$observa,53,0);
		   $insertarExp="Incluido";
	       }else{
		   $insertarExp.="(X)";
	       }
	    }
        }else{ 
	    $res = 0; 
	    $insertarExp .= "(X)";
	    $errad=1;
	}
      }
      elseif($i+1==count($arrDatos) && substr($insertarExp,0,5)!="Error" && (trim($asunto)!='' || trim($titulo)!='')){
	$ac=array();
        if(trim($titulo)!=''){
	    $ac[]="sgd_exp_titulo='".mb_strtoupper($titulo)."'";
        }
	if(trim($asunto)!=''){
            $ac[]="sgd_exp_asunto='".mb_strtoupper($asunto)."'";
        }
	$set=implode(',',$ac);
	$usql="update sgd_exp_expediente set $set where sgd_exp_numero='$numExpediente'";
	$rs=$db->conn->Execute($usql);
      }
     if(substr($insertarExp,0,5)=="Error" || $errad==1){
					if($res==1) {$fColor = "green"; }elseif($res==0){$fColor="red"; }elseif($res==5){ $fColor="blue";};
          echo "<td><font color=$fColor>* $insertarExp ($rad) </font></td>";  
       }else{
           
          if($res==1) {$fColor = "green"; }elseif($res==0){$fColor="red"; }elseif($res==5){ $fColor="blue";};
          echo "<td><font color=$fColor>* $insertarExp ($rad) </font></td>"; 
       }
     $i++;
   }
   }
   echo "</tr>";
   $iFile++;
   
}
fclose($fp);
?> 
</table>
</body>
</html> 
