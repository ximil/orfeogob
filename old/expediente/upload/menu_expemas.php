<?php session_start(); 
date_default_timezone_set('America/Bogota');
date_default_timezone_set('America/Bogota');
/**
  * Desarrollo de Carga de carga Masiva para la DNE
  *
  * Autores:
  * Jairo Hernan Losada
  * Diego Enrique Rodriguez
  */
$ruta_raiz = "../../";
include($ruta_raiz.'/validadte.php');
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../../estilos/orfeo.css">



<script language="JavaScript">
<!--


function Start(URL, WIDTH, HEIGHT)
{
 windowprops = "top=0,left=0,location=no,status=no, menubar=no,scrollbars=yes, resizable=yes,width=";
 windowprops += WIDTH + ",height=" + HEIGHT;

 preview = window.open(URL , "preview", windowprops);
}

//-->
</script>
</head>
<body bgcolor="#FFFFFF" topmargin="0" onLoad="window_onload();">


<table width="47%" align="center" border="0" cellpadding="0" cellspacing="5" class="borde_tab">
<tr>
	<td height="25" class="titulos4">CREACI&Oacute;N E INCLUSI&Oacute;N MASIVA DE EXPEDIENTES</td>
</tr>

<tr align="center">
	<td class="listado2" >
		<a href='upload.php' class="vinculos" target='mainFrame'>
		Cargar Listado de Expedientes (Con Faro o Matrix) 
		</a>
	</td>
</tr>
<tr align="center">
	<td class="listado2" >
		<a href='upload2.php' class="vinculos" target='mainFrame'>
		Cargar Listado de Expedientes (Sin Faro y sin Matrix)
		</a>
	</td>
</tr>
</table>
</body>
</html>
