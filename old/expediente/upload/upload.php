<?php 
session_start();
$ruta_raiz = "../..";
foreach ($_GET as $key => $valor)   ${$key} = $valor;
foreach ($_POST as $key => $valor)   ${$key} = $valor;
date_default_timezone_set('America/Bogota');

define('ADODB_ASSOC_CASE', 1);
$verrad         = "";
$krd            = $_SESSION["krd"];
$dependencia    = $_SESSION["dependencia"];
$usua_doc       = $_SESSION["usua_doc"];
$codusuario     = $_SESSION["codusuario"];

ini_set("display_errors", 1);

include_once    ("$ruta_raiz/include/db/ConnectionHandler.php");
require_once    ("$ruta_raiz/class_control/Mensaje.php");

$db = new ConnectionHandler($ruta_raiz);
$db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
$db->conn->debug = false;	
include $ruta_raiz . "/include/tx/Expediente.php";
$exp = new Expediente($db);
include $ruta_raiz . "/include/tx/Historico.php";
$hist= new Historico ($db);
?>
<html>
<head>
    <link rel="stylesheet" href="<?php echo $ruta_raiz."/estilos"?>/orfeo.css">
</head>
<body>

<form action="upload.php" method="post" enctype="multipart/form-data">
<table class='borde_tab' align=center width='70%'>
<tr class=listado1>
 <td colspan=3 align=center class=titulos2>
  Cargue de Expedientes Masivos (Con Faro o Matrix)
  </td>
</tr>
<tr class=listado1>
  <td class=titulos2><label for="file">Archivo:</label></td>
  <td><input type="file" name="file" id="file"></td>
</tr><tr class=listado1>
 <td colspan=3 align=center class=titulos2>
  <input type="submit" name="submit" value="Submit">
  </td>
</tr>
</table>
</form>  


<?php $allowedExts = array("gif", "jpeg", "jpg", "png", "csv", "txt");
$temp = explode(".", $_FILES["file"]["name"]);
$extension = end($temp);
echo $_FILES["file"]["type"] ." ." . $_FILES["file"]["size"];
if ((($_FILES["file"]["type"] == "text/csv")) || ($_FILES["file"]["type"] == "text/txt") || ($_FILES["file"]["type"] == "application/octet-stream") ||  ($_FILES["file"]["type"] == "text/comma-separated-values") || ($_FILES["file"]["type"] == "text/csv") || ($_FILES["file"]["type"] == "application/csv") || ($_FILES["file"]["type"] == "application/excel") || ($_FILES["file"]["type"] == "application/vnd.ms-excel") || ($_FILES["file"]["type"] == "application/vnd.msexcel"))
  {
  if ($_FILES["file"]["error"] > 0)
    {
    echo "Error al Cargar Archivo: " . $_FILES["file"]["error"] . "<br>";
    }
  else
    {
      $pathDestino = "$ruta_raiz/bodega/tmp/".$_FILES["file"]["name"];
      if(move_uploaded_file($_FILES["file"]["tmp_name"], $pathDestino )){
      echo "Archivo: ". $pathDestino;
      }else{ echo "No se Cargo el Archivo";
      }
    }
  }
else
  {
    die( "Archivo No valido o no Cargado.");
  }
$fp = fopen($pathDestino,"r");

?>

<table class=borde_tab>
<?php 
$iFile = 0;
 while(!feof($fp)) {
   $linea =  fgets($fp);
   $linea = str_replace(";",",",$linea);
   $arrDatos = explode(",",$linea);
   
   echo "<tr class=listado2>";
   $numeroMF = array();
   $i=0;
   $datoMal = "No";
   foreach($arrDatos as $rad){
     $rad = str_replace('"','',$rad);
     if($i==0) {
					if(strlen(trim($rad))>=1 && strlen(trim($rad)) <=5 ){
						//$numeroMF[2]= "EXPEDIENTE FARO";
					    $faro=$rad;
					    $matrix=null;
					}elseif(strlen(trim($rad))==14){
						//$numeroMF[2]= "EXPEDIENTE MATRIZ";
					    $faro=null;
					    $matrix=$rad;
					}else{
						$datoMal = "Si";
					}
      //$numeroMF[1] = $rad;
     }
     
     if($i==1) $depexp = $rad;
     if($i==2) $doc = $rad;
     if($i==3){ $srd= substr('00'.$rad,-2);$serie=$rad; }
     if($i==4){ $sbrd=substr('00'.$rad,-2);$subserie=$rad; }
     if($i==5){
     $res=5;
      if($iFile>=1 && $datoMal!="Si" ){ 
       echo "<td>";
       $exp->expManual = 0;
       $ano=substr($rad,0,4);
       $secuenciaExp = $exp->secExpediente($depexp,$serie,$subserie,$ano);
       $expediente = $ano.$depexp.$srd.$sbrd. str_pad($secuenciaExp, 5, "0", STR_PAD_LEFT)  . "E";
       $numExpediente = $exp->crearExpediente($expediente,$rad,$depexp,1,$usua_doc,$doc,$serie,$subserie, 'false',null,0,null,$matrix,$faro);
       if(strlen($numExpediente)>=8) $res=1;
       echo " $expediente</td>";
      }
     }
     if($i>=5  && $datoMal!="Si"){
      if($iFile>=1 and trim($rad)){
           $res = $exp->insertar_expediente($numExpediente,$rad,$dependencia,$codusuario,$usua_doc);
	   $radicados[]=$rad;
           if($i==5 && $res!=0){
		$observa="*TRD*$serie/$subserie (Creacion de Expediente.)";
		$hist->insertarHistoricoExp($numExpediente,$radicados,$dependencia,$codusuario,$observa,51,0);
	   }elseif($i>5 && $res!=0){
		$observa="Incluido al expediente";
                $hist->insertarHistoricoExp($numExpediente,$radicados,$dependencia,$codusuario,$observa,53,0);
	   }
	   unset($radicados);
        }
      }
     if($iFile>=1 && $datoMal=="Si"){
						echo "<td>$rad<font color=red>Numero Matriz/Faro Incorrecto</font></td>";  
       }else{
           
          if($res==1) {$fColor = "green"; }elseif($res==0){$fColor="red"; }elseif($res==5){ $fColor="blue";};
          if($i<=4) $fColor = "black";
          echo "<td><font color=$fColor>$rad </font></td>"; 
       }
     $i++;
   }
   echo "</tr>";
   $iFile++;
   
}
fclose($fp);
?> 
</table>
</body>
</html> 
