<?php session_start(); date_default_timezone_set('America/Bogota'); date_default_timezone_set('America/Bogota');
//echo "-->". session_id();
/** Modulo de Expedientes o Carpetas Virtuales
  * Modificacion Variables
  *@autor Jairo Losada 2009/06
  *Licencia GNU/GPL 
  */
foreach ($_GET as $key => $valor)  $$key = $valor;
foreach ($_POST as $key => $valor)  $$key = $valor;

$krd = $_SESSION["krd"];
$dependencia = $_SESSION["dependencia"];
$usua_doc = $_SESSION["usua_doc"];
$codusuario = $_SESSION["codusuario"];
$tip3Nombre=$_SESSION["tip3Nombre"];
$tip3desc = $_SESSION["tip3desc"];
$tip3img =$_SESSION["tip3img"];
$tpNumRad = $_SESSION["tpNumRad"];
$tpPerRad = $_SESSION["tpPerRad"];
$tpDescRad = $_SESSION["tpDescRad"];
$tip3Nombre = $_SESSION["tip3Nombre"];
$tpDepeRad = $_SESSION["tpDepeRad"];
$usuaPermExpediente = $_SESSION["usuaPermExpediente"];
$ruta_raiz = ".."; 
//Cargando variables del log
if(isset($_SERVER['HTTP_X_FORWARD_FOR'])){
    $proxy=$_SERVER['HTTP_X_FORWARD_FOR'];
}else
    $proxy=$_SERVER['REMOTE_ADDR'];
$REMOTE_ADDR=$_SERVER['REMOTE_ADDR'];
$ruta_raiz2=$ruta_raiz;
$ruta_raiz='../..';
include_once "$ruta_raiz/core/clases/log.php";
$log = new log($ruta_raiz);
$log->setAddrC($REMOTE_ADDR);
$log->setProxyAd($proxy);
$ruta_raiz=$ruta_raiz2;
$log->setUsuaCodi($codusuario);
$log->setDepeCodi($dependencia);
$log->setRolId($_SESSION['id_rol']);

if ( !$nurad )
{
    $nurad = $rad;
}

include_once( "$ruta_raiz/include/db/ConnectionHandler.php" );
$db = new ConnectionHandler( "$ruta_raiz" );
//$db->conn->debug = true;
include_once( "$ruta_raiz/include/tx/Historico.php" );
include_once( "$ruta_raiz/include/tx/Expediente.php" );

$encabezado = "$PHP_SELF?".session_name()."=".session_id()."&opcionExp=$opcionExp&numeroExpediente=$numeroExpediente&nurad=$nurad&coddepe=$coddepe&codusua=$codusua&depende=$depende&ent=$ent&tdoc=$tdoc&codiTRDModi=$codiTRDModi&codiTRDEli=$codiTRDEli&codserie=$codserie&tsub=$tsub&ind_ProcAnex=$ind_ProcAnex";
$expediente = new Expediente( $db );

// Inserta el radicado en el expediente
if( $funExpediente == "INSERT_EXP" )
{   
    // Consulta si el radicado est� incluido en el expediente.
    $arrExpedientes = $expediente->expedientesRadicado( $nurad);
    /* Si el radicado esta incluido en el expediente digitado por el usuario.
     * != No identico no se puede poner !== por que la funcion array_search 
     * tambien arroja 0 o "" vacio al ver que un expediente no se encuentra
     */ 
	 $arrExpedientes[] = "1";
    foreach ( $arrExpedientes as $line_num => $line){
    	if ($line == $_POST['numeroExpediente']) {
    		  print '<center><hr><font color="red">El radicado ya est&aacute; incluido en el expediente.</font><hr></center>';
    	}else {
    		  $resultadoExp = $expediente->insertar_expediente( $_POST['numeroExpediente'], $_GET['nurad'], $dependencia, $codusuario, $usua_doc );
        if( $resultadoExp == 1 )
        {
            $observa = "Incluir radicado en Expediente";
            include_once "$ruta_raiz/include/tx/Historico.php";
            $radicados[] = $_GET['nurad'];
            $tipoTx = 53;
            $Historico = new Historico( $db );
            $Historico->insertarHistoricoExp( $_POST['numeroExpediente'], $radicados, $dependencia, $codusuario, $observa, $tipoTx, 0 );
            $log->setDenomDoc($doc='Radicado');
            $log->setNumDocu($radicados);
            $log->setAction('document_added_to_record');
            $log->setOpera("Incluido en expediente $numeroExpediente");
            $log->registroEvento();
            ?>
            <script language="JavaScript">
              opener.regresar();
              window.close();
            </script>  
            <?php         }
        else
        {
            print '<hr><font color=red>No se anexo este radicado al expediente. Verifique que el numero del expediente exista e intente de nuevo.</font><hr>';	    
        }
    	}
    }
   
      
    
}
?>
<html>
<head>
<title>Incluir en Expediente</title>
<link href="../estilos/orfeo.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
function validarNumExpediente()
{
    numExpediente = document.getElementById( 'numeroExpediente' ).value;
	
    // Valida que se haya digitado el nombre del expediente
    // a�o dependencia serie subserie consecutivo E
    if( numExpediente.length != 0 && numExpediente != "" )
    {
        insertarExpedienteVal = true;
    }
    else if( numExpediente.length == 0 || numExpediente == "" )
    {
        alert( "Error. Debe especificar el nombre de un expediente." );
        document.getElementById( 'numeroExpediente' ).focus();
        insertarExpedienteVal = false;
    }
    
    if( insertarExpedienteVal == true )
	{
        document.insExp.submit();
	}
}

function confirmaIncluir()
{
    document.getElementById( 'funExpediente' ).value = "INSERT_EXP";
    document.insExp.submit();
}

function cambia_Exp(expId, expNo){
	var exp_id = document.getElementById(expId);
	numExp = exp_id.value;
	var exp_no = document.getElementById(expNo);
	exp_no.value = numExp;
	document.insExp.numeroExpediente.focus();
	}

function procModificar()
{
	nombreventana="ventanaBusqAV";
	url='<?php echo  $url_orfeo_power_tools ?>/records';
	window.open(url,nombreventana,'height=600,width=770,scrollbars=yes');
return;
}

</script>
</head>
<body bgcolor="#FFFFFF" onLoad="document.insExp.numeroExpediente.focus();">



<?php 
//$dependencia = '230';
/************************************************/
/* Ejecucion de los QUERYS respectivos para		*/
/* filtrar los expedientes coincidentes			*/
/************************************************/

$btnBuscaExp = $_POST['btnBuscaExp'];
if($btnBuscaExp){
	$criterio = strtoupper($_POST['criterio']);
	if(!$criterio)
	{
		$criterio="_nada_";
	}
	
	$sql_Fin = "SELECT * FROM SGD_SEXP_SECEXPEDIENTES 
			WHERE SGD_SEXP_PAREXP1 LIKE '%$criterio%' OR SGD_SEXP_PAREXP2 LIKE '%$criterio%' OR SGD_SEXP_PAREXP3 LIKE '%$criterio%'";
	}
else{
	$sql_rad = "SELECT * FROM RADICADO r, BODEGA_EMPRESAS b 
					WHERE b.IDENTIFICADOR_EMPRESA = r.EESP_CODI 
						AND r.RADI_NUME_RADI = '$nurad'";
	$rs_rad = $db->query($sql_rad);

	if(!$rs_rad->EOF){
		$sgd_sexp_parexp1 = $rs_rad->fields['NIT_DE_LA_EMPRESA'];
//		$sgd_sexp_parexp2 = $rs_rad->fields['SIGLA_DE_LA_EMPRESA'];
		if(!$sgd_sexp_parexp1)
		{
			$sgd_sexp_parexp1="_nada_";
		}
		
		$sql_Fin = "SELECT * FROM SGD_SEXP_SECEXPEDIENTES 
			WHERE SGD_SEXP_PAREXP1 LIKE '%$sgd_sexp_parexp1%'"; // OR SGD_SEXP_PAREXP2 LIKE '%$sgd_sexp_parexp2%'";
		}
	}

/*
	$campos = array("NIT" => "SGD_SEXP_PAREXP1",
					"SIGLA" => "SGD_SEXP_PAREXP2",
					"CARPETA" => "SGD_SEXP_PAREXP3"
					);
		
	$sql_pex = "SELECT * FROM SGD_PAREXP_PARAMEXPEDIENTE 
			WHERE DEPE_CODI = '$dependencia' AND SGD_PAREXP_CODIGO = '$exp_cod_Bus'";
					
	$rs_pex = $db->conn->query($sql_pex);
	
	$param = $rs_pex->fields['SGD_PAREXP_ETIQUETA'];
	
	$fieldBus = $campos[$param];
*/
	
	//echo $sql_Fin;
	$rs_Fin = $db->conn->query($sql_Fin);
	?>
	
	<br>
	<?php 
//	}
/*	FIN	*/
	?>


<form method="post" action="<?php print $encabezado; ?>" name="insExp">
<input type="hidden" name='funExpediente' id='funExpediente' value="" >
<input type="hidden" name='confirmaIncluirExp' id='confirmaIncluirExp' value="" >
  <table border=0 width=90% align="center" class="borde_tab" cellspacing="1" cellpadding="0">        
    <tr align="center" class="titulos2">
      <td height="15" class="titulos2" colspan="2">INCLUIR EN  EL EXPEDIENTE</td>
    </tr>
  </table>
<table width="90%" border="0" cellspacing="1" cellpadding="0" align="center" class="borde_tab">

</table>
<table border=0 width=90% align="center" class="borde_tab">
      <tr align="center">
      <td class="titulos5" align="left" nowrap>
  N&uacute;mero del Expediente </td>
      <td class="titulos5" align="left">
        <input type="text" name="numeroExpediente" id="numeroExpediente" value="<?php print $_POST['numeroExpediente']; ?>" size="30">
      </td>
    </tr>
<tr>     <td width="13%" height="25" class="titulos5" align="center" colspan="2">
<center><input name="actualizar" type="button" class="botones_funcion" id="envia23" onClick="procModificar();"value=" Busqueda "></center>
      
    </td></tr>
</table>
<table border=0 width=90% align="center" class="borde_tab">
	<tr align="center">

	<td width="33%" height="25" class="listado2" align="center">
	<center>
	  <input name="btnIncluirExp" type="button" class="botones_funcion" id="btnIncluirExp" onClick="validarNumExpediente();" value="Incluir en Exp">
		</center></TD>
	<td width="33%" class="listado2" height="25">
	<center><input name="btnCerrar" type="button" class="botones_funcion" id="btnCerrar" onClick="opener.regresar(); window.close();" value=" Cerrar "></center></TD>
	</tr>
</table>
<?php 
// Consulta si existe o no el expediente.
if ( $expediente->existeExpediente( $_POST['numeroExpediente'] ) !== 0 )
{
    if($expediente->verifExpedAbierto($_POST['numeroExpediente'])==2){
?>
         <script language="JavaScript">
      alert( "El expediente <?php echo $_POST['numeroExpediente']?> ha sido cerrado y no se permite incluir nuevos radicados" );
      document.getElementById( 'numeroExpediente' ).focus();
    </script>
<?php     }else{
?>
<table border=0 width=70% align="center" class="borde_tab">
  <tr align="center">
    <td width="33%" height="25" class="listado2" align="center">
      <center class="titulosError2">
        ESTA SEGURO DE INCLUIR ESTE RADICADO EN EL EXPEDIENTE: 
      </center>
      <B>
        <center class="style1"><b><?php print $numeroExpediente; ?></b></center>
      </B>
      <div align="justify"><br>
        <strong><b>Recuerde:</b>No podr&aacute; modificar el numero de expediente si hay
        un error en el expediente, m&aacute;s adelante tendr&aacute; que excluir este radicado del
        expediente y si es el caso solicitar la anulaci&oacute;n del mismo. Adem&aacute;s debe
        tener en cuenta que tan pronto coloca un nombre de expediente, en Archivo crean
        una carpeta f&iacute;sica en el cual empezaran a incluir los documentos
        pertenecientes al mismo.
        </strong>
      </div>
    </td>
  </tr>
</table>
<table border=0 width=70% align="center" class="borde_tab">
  <tr align="center">
    <td width="33%" height="25" class="listado2" align="center">
	  <center>
	    <input name="btnConfirmar" type="button" onClick="confirmaIncluir();" class="botones_funcion" value="Confirmar">
	  </center>
    </td>
	<td width="33%" class="listado2" height="25">
	<center><input name="cerrar" type="button" class="botones_funcion" id="envia22" onClick="opener.regresar(); window.close();" value=" Cerrar "></center></TD>
	</tr>
</table>
<?php 	
    }
}
else if ( $_POST['numeroExpediente'] != "" && ( $expediente->existeExpediente( $_POST['numeroExpediente'] ) === 0 ) )
{
    ?>
    <script language="JavaScript">
      alert( "Error. El nombre del Expediente en el que desea incluir este radicado \n\r no existe en el sistema. Por favor verifique e intente de nuevo." );
      document.getElementById( 'numeroExpediente' ).focus();
    </script>
    <?php }
?>
</form>
</body>
</html>
