<?php 
/* fecha 2 oct 2008 
* *Desarollado por  Hardy Deimont Niño V. para SSPD
* *Funcionalidad tipificacion Masiva
* *La tipificacion se  hace solo radicados no  tipificados por los siguiete no se pueden 
* Si un radicado ya esta tipificado  no se puede modificar por esta opcion ya esta solo
* para los radicados sin tipificar.
* Modificacion hecha para sspd
*/
/* Se incluye historicos control de tipo documental y expediente*/
    include_once "$ruta_raiz/include/tx/Historico.php";
    include_once ("$ruta_raiz/class_control/TipoDocumental.php");
    include_once "$ruta_raiz/include/tx/Expediente.php";
/* variables de entorno */

	$coddepe = $dependencia;
	$codusua = $codusuario;
// variable  tipo  de radicado 15 dic 2008
	     $varRadi=$radxtip{13};//substr($radxtip[0], -2, 1);
//$db->conn->debug=true;
/* valida que los radicados sean del mismo usuario actual */ 

 	$isqlDepR = "SELECT RADI_DEPE_ACTU,RADI_USUA_ACTU from radicado WHERE RADI_NUME_RADI = '$nurad'";
	$rsDepR = $db->conn->Execute($isqlDepR);
	if ($rsDepR)
	{	// $coddepe = $rsDepR->fields['RADI_DEPE_ACTU'];
		// $codusua = $rsDepR->fields['RADI_USUA_ACTU'];
	}

/* generar select de escogencia */
?>
<html><body><br><br>
<form name="form1" method="post" action="proceso.php?<?php print $encabezado; ?>&tipificar=1">
<table border=0 width=600 align="center" class="borde_tab" cellspacing="0">
	  <tr align="center" class="titulos4">
	    <td height="30" >APLICACION DE LA TRD</td>
      </tr>
</table> 
<table width="600" border="0" cellspacing="1" cellpadding="0" align="center" class="borde_tab">
   <tr >
     <td class="listado1h" >SERIE</td>
	 <td class='listado1h' ><?php   
    if(!$tdoc) $tdoc = 0;
    if(!$codserie) $codserie = 0;
	if(!$tsub) $tsub = 0;
	$fechah=date("dmy") . " ". time("h_m_s");
	$fecha_hoy = Date("Y-m-d");
	$sqlFechaHoy=$db->conn->DBDate($fecha_hoy);
	$check=1;
	$fechaf=date("dmy") . "_" . time("hms");
	$num_car = 4;
	$nomb_varc = "s.sgd_srd_codigo";
	$nomb_varde = "s.sgd_srd_descrip";
   	include "$ruta_raiz/include/query/trd/queryCodiDetalle.php";
	$querySerie = "select distinct ($sqlConcat) as detalle, s.sgd_srd_codigo 
	         from sgd_mrd_matrird m, sgd_srd_seriesrd s
			 where m.depe_codi = '$dependencia'
			 	   and s.sgd_srd_codigo = m.sgd_srd_codigo
			       and ".$sqlFechaHoy." between s.sgd_srd_fechini and s.sgd_srd_fechfin and m.sgd_mrd_esta='1'
			 order by detalle			  ";
	$rsD=$db->conn->query($querySerie);
	$comentarioDev = "Muestra las Series Docuementales";
	include "$ruta_raiz/include/tx/ComentarioTx.php";
	//15 - dic- 2008 ase agrega variable
	print $rsD->GetMenu2("codserie", $codserie, "0:-- Seleccione --", false,"","onChange='subSerie(".$dependencia.",".$varRadi.");' class='select' id='codserie' " );


 ?>     </td>
	  
  </tr>
   <tr>
     <td class="listado1h" >SUBSERIE</td>
	 <td class="listado1h" ><div id="subseriediv">
<select class='select' name='subserie2' id='subserie2'>	<option value="0">-- Seleccione --</option>
 </select></div>
     </td>
  </tr>
   <tr>
     <td class="listado1h" >TIPO DE DOCUMENTO</td>
 	 <td class="listado1h" >	<div id="documento"><select class='select'>  <option value="0">-- Seleccione --</option>
</select> </div>
    </td>
  </tr>
</table>
   

<table border=0 width="600" align="center" class="borde_tab">
  <tr align="center">
    <td width="33%" class="listado2" height="25"><center>
      <input name="insertar_registro2" type=submit class="botones_funcion" value=" Insertar ">
    </center></TD>
  </tr>
  <tr>
    <td class='info'>Todo documento requiere la TRD para ser procesado y almacenado. <br>
	Seleccione la TRD asignada a su área para el radicado que está diligenciando o va archivar.</td>
  </tr>
</table>
<input type="hidden" name="radicados" value="<?php  echo $radxtip?>" >
</form>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><?php 
//print_r($recordid);
//echo "<br>". $radxtip;

?>
</p>
</body>

</html>
