<?php
$ruta_raiz="..";
    include_once ("$ruta_raiz/class_control/TipoDocumental.php");
#    include_once "$ruta_raiz/include/tx/Expediente.php";
     include_once "$ruta_raiz/include/db/ConnectionHandler.php";
 	include_once "$ruta_raiz/include/tx/Historico.php";

if (!isset($db))	$db = new ConnectionHandler("$ruta_raiz");
//$db->conn->debug=true;
$db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
	$fecha_hoy = Date("Y-m-d");
	$sqlFechaHoy=$db->conn->DBDate($fecha_hoy);
if(isset($_SERVER['HTTP_X_FORWARD_FOR'])){
    $proxy=$_SERVER['HTTP_X_FORWARD_FOR'];
}else
    $proxy=$_SERVER['REMOTE_ADDR'];
$REMOTE_ADDR=$_SERVER['REMOTE_ADDR'];
$ruta_raiz2=$ruta_raiz;
$ruta_raiz='../..';
include_once "$ruta_raiz/core/clases/log.php";
$log = new log($ruta_raiz);
$log->setAddrC($REMOTE_ADDR);
$log->setProxyAd($proxy);
$ruta_raiz=$ruta_raiz2;
$log->setUsuaCodi($_SESSION['codusuario']);
$log->setDepeCodi($_SESSION['dependencia']);
$log->setRolId($_SESSION['id_rol']);
$log->setDenomDoc('Radicado');
	// subserie
IF($_GET['tranx']=="subserie"){
 ?><span class="listado5">
<?php

	$nomb_varc = "su.sgd_sbrd_codigo";
	$nomb_varde = "su.sgd_sbrd_descrip";
	include "$ruta_raiz/include/query/trd/queryCodiDetalle.php";
   	$querySub = "select distinct ($sqlConcat) as detalle, su.sgd_sbrd_codigo
	         from sgd_mrd_matrird m, sgd_sbrd_subserierd su
			 where m.depe_codi = '".$_GET['depe']."'
			       and m.sgd_srd_codigo = '".$_GET['code']."'
				   and su.sgd_srd_codigo = '".$_GET['code']."'
			       and su.sgd_sbrd_codigo = m.sgd_sbrd_codigo
 			       and ".$sqlFechaHoy." between su.sgd_sbrd_fechini and su.sgd_sbrd_fechfin
                                   and m.sgd_mrd_esta='1'
			 order by detalle
			  ";
	$rsSub=$db->conn->query($querySub);
	include "$ruta_raiz/include/tx/ComentarioTx.php";
	//Se agrego  variable 15/12/2008
	print $rsSub->GetMenu2("subserie", $tsub, "0:-- Seleccione --", false,"","onChange='documento(".$_GET['depe'].",".$_GET['varRadi2'].")' class='select' id='subserie' " );
?>
</span>
<?php  }
	//Tipo Documental
IF($_GET['tranx']=="documento"){
?><span class="listado5"><?php
	$nomb_varc = "t.sgd_tpr_codigo";
	$nomb_varde = "t.sgd_tpr_descrip";
	include "$ruta_raiz/include/query/trd/queryCodiDetalle.php";
	//se agrega validacion a la consulta
 	$queryTip = "select distinct ($sqlConcat) as detalle, t.sgd_tpr_codigo
	         from sgd_mrd_matrird m, sgd_tpr_tpdcumento t
			 where m.depe_codi = '".$_GET['depe']."'
			       and m.sgd_mrd_esta = '1'
 			       and m.sgd_srd_codigo = '".$_GET['code']."'
			       and m.sgd_sbrd_codigo = '".$_GET['subserie']."'
 			       and t.sgd_tpr_codigo = m.sgd_tpr_codigo
			       and t.sgd_tpr_tp".$_GET['varRadi3']."='1'
			 order by detalle
			 ";

	$rsTip=$db->conn->query($queryTip);
	$ruta_raiz = "..";
	include "$ruta_raiz/include/tx/ComentarioTx.php";
	print $rsTip->GetMenu2("tdoc", $tdoc, "0:-- Seleccione --", false,""," class='select' id='tdoc'" );
	?> </span> <?php
}
//tipificar ================================================================
if($_GET['tipificar']==1){
   //	Inciar seession
	session_start(); date_default_timezone_set('America/Bogota'); date_default_timezone_set('America/Bogota');
//	include "$ruta_raiz/rec_session.php";
  /***validar  transaciones*/
include "$ruta_raiz/tx/validar.php";
	//variables
	$codserie=$_POST['codserie'];
	$tsub=$_POST['subserie'];
	$tdoc=$_POST['tdoc'];
	$dependencia=$_SESSION['dependencia'];
	$RadicadoPT=explode(",",$_POST['radicados']);
	$coddepe=$dependencia;
    $codusua =$_SESSION['codusuario'];
	$ia=0;
	for($ia=0;(count($RadicadoPT)-1)>$ia ;$ia++ ){
     	//variable para busqueda
	      $ent= substr($RadicadoPT[$ia], -1, 1);
		// validar si  el  tipo  documental  del  documento
		//	include "$ruta_raiz/include/query/trd/queryCodiDetalle.php";
	    $queryTip = "select  t.sgd_tpr_codigo from sgd_mrd_matrird m, sgd_tpr_tpdcumento t  where m.depe_codi = '$dependencia'  and m.sgd_mrd_esta = '1' and m.sgd_srd_codigo = '$codserie' and m.sgd_sbrd_codigo = '$tsub'
 			       and t.sgd_tpr_codigo = m.sgd_tpr_codigo and t.sgd_tpr_codigo =$tdoc  and t.sgd_tpr_tp$ent='1'	 ";
		$rsTip=$db->conn->query($queryTip);
		if(!$rsTip->EOF){
			$nurad=$RadicadoPT[$ia];
			//cuando  el  tipo  documental corresponde
			$isqlTRD = "select SGD_MRD_CODIGO from SGD_MRD_MATRIRD  where DEPE_CODI = '$dependencia'  and SGD_SRD_CODIGO = '$codserie' and SGD_SBRD_CODIGO = '$tsub' and SGD_TPR_CODIGO = '$tdoc'";
			$rsTRD = $db->conn->Execute($isqlTRD);
			$i=0;
			while(!$rsTRD->EOF)
			{
	    		$codiTRDS[$i] = $rsTRD->fields['SGD_MRD_CODIGO'];
				$codiTRD = $rsTRD->fields['SGD_MRD_CODIGO'];
	    		$i++;
				$rsTRD->MoveNext();
			}
			  	$trd = new TipoDocumental($db);
			$radicados = $trd->insertarTRD($codiTRDS,$codiTRD,$nurad,$coddepe, $codusua);
		    $TRD = $codiTRD;
			include "$ruta_raiz/radicacion/detalle_clasificacionTRD.php";
			$sqlH = "SELECT $radi_nume_radi RADI_NUME_RADI
					FROM SGD_RDF_RETDOCF r
					WHERE r.RADI_NUME_RADI = '$nurad'
				    AND r.SGD_MRD_CODIGO =  '$codiTRD'";
			$rsH=$db->conn->query($sqlH);
			$i=0;
			while(!$rsH->EOF)
			{
	    		$codiRegH[$i] = $rsH->fields['RADI_NUME_RADI'];
	    		$i++;
				$rsH->MoveNext();
			}
			$observa = "*Incluido TRD* ".$deta_serie."/".$deta_subserie."/".$deta_tipodocu;
			$log->setUsuaCodi($codusua);
			$log->setDepeCodi($dependencia);
			$log->setRolId($_SESSION['id_rol']);
			$log->setDenomDoc('Radicado');
			$log->setAction('document_classification');
			$log->setOpera("Tipificación de radicado");
  		  	$Historico = new Historico($db);
  		  	//$radiModi = $Historico->insertarHistorico($codiRegH, $coddepe, $codusua, $coddepe, $codusua, $observa, 32);
			$radiModi = $Historico->insertarHistorico($codiRegH, $dependencia, $codusua, $_SESSION['id_rol'],$dependencia, $codusua, $_SESSION['id_rol'],$observa, 32);
			$log->setNumDocu($codiRegH);
			$log->registroEvento();
		  	/*
		  	*Actualiza el campo tdoc_codi de la tabla Radicados
		  	*/
	 	    $radiUp = $trd->actualizarTRD($codiRegH,$tdoc);
			$TSi.='<tr> <td align="center" class="listado3h"> '.$nurad.'</td></tr>';
      }
	  else{
			 //cuando  el  tipo  documental no  corresponde
			     $TipoNo.='<tr> <td align="center" class="listado3h"> '.$RadicadoPT[$ia].'</td></tr>';
 		 }
	}
$m='<tr> <td align="center" class="listado3h"> - </td></tr>';
   ?><html><head> <link rel="stylesheet" href="../estilos/orfeo.css"></style>
      </head><body>
   <center><br><br>
   <table><tr>
   <td valign="top">
       <table class="borde_tab" width="250">
	   <tr align="center" class="titulos4"><th alig="center" class="titulos4">Radicados  que se tipificaron</th></tr>
        <?php   if($TSi==NULL){ echo $m; }else{ echo $TSi; }?>
      </table>
   </td>
   <td  width="10"valign="top">
      </td>
   <td valign="top">
       <table class="borde_tab" width="250">
	   <tr align="center" class="titulos4">	     <th align="center" class="titulos4">Radicados que el tipo documental no corresponde y no se tipificaron</th></tr>
		 <?php  if($TipoNo==NULL){ echo $m; }else{  echo $TipoNo;  }?>
		</table></td>
   </tr></table>


</center></body></html>

<?php
}
?>
