<?php session_start();
putenv("NLS_LANG=American_America.UTF8");
date_default_timezone_set('America/Bogota');
date_default_timezone_set('America/Bogota');
/**
 * Modificacion para aceptar Variables GLobales
 * @autor Jairo Losada 2009/05 
 * @fecha 2009/05
 */
foreach ($_GET as $key => $valor)
    $$key = $valor;
foreach ($_POST as $key => $valor)
    $$key = $valor;
$archivado_requiere_exp = $_SESSION['archRequExp'];
$krd = $_SESSION ["krd"];
$dependencia = $_SESSION ["dependencia"];
$usua_doc = $_SESSION ["usua_doc"];
$codusuario = $_SESSION ["codusuario"];
$usuario_reasignacion = $_SESSION ["usuario_reasignar"];
$id_rol = $_SESSION ["id_rol"];
/* echo "<hr>";
  print_r($_SESSION);
  echo "<hr>";
  print_r($_GET);
  echo "<hr>";
  print_r($_POST);
  echo "<hr>"; */
$ruta_raiz = "..";
$mensaje_error = false;
include ($ruta_raiz . '/validadte.php');
/**
 * Inclusion de archivos para utilizar la libreria ADODB
 *
 */
include_once "$ruta_raiz/include/db/ConnectionHandler.php";
if (!isset($db))
    $db = new ConnectionHandler("$ruta_raiz");
$db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
require_once ("$ruta_raiz/class_control/Dependencia.php");
$objDep = new Dependencia($db);
/*
 * Generamos el encabezado que envia las variable a la paginas siguientes.
 * Por problemas en las sesiones enviamos el usuario.
 * @$encabezado  Incluye las variables que deben enviarse a la singuiente pagina.
 * @$linkPagina  Link en caso de recarga de esta pagina.
 */
$encabezado = "" . session_name() . "=" . session_id() . "&depeBuscada=$depeBuscada&filtroSelect=$filtroSelect&tpAnulacion=$tpAnulacion";
$linkPagina = "$PHP_SELF?$encabezado&orderTipo=$orderTipo&orderNo=";

/*  FILTRO DE DATOS
 */

if ($codTx == 21) {
    goto ExpMasivos;
}

if ($checkValue) {
    echo $num = count($checkValue);
    reset($checkValue);
    $i = 0;
    $jglCounter = 0;
    $resultadoJGL = "";
    while (list ( $recordid, $tmp ) = each($checkValue)) {
        $record_id = $recordid;
        //echo $codTx;
        switch ($codTx) {
            case 7 :
            case 8 : {
                    if (strpos($record_id, '-')) { //Si trae el informador concatena el informador con el radicado sino solo concatena los radicados.
                        $tmp = explode('-', $record_id);
                        if ($tmp [0]) {
                            $whereFiltro .= ' (b.radi_nume_radi = ' . $tmp [1] . ' and i.info_codi=\'' . trim($tmp [0]) . '\') or';
                            $tmp_arr_id = 2;
                        } else {
                            $whereFiltro .= ' b.radi_nume_radi = ' . $tmp [1] . ' or';
                            $tmp_arr_id = 1;
                        }
                    } else {
                        $whereFiltro .= ' b.radi_nume_radi = ' . $record_id . ' or';
                        $tmp_arr_id = 0;
                    }
                    $record_id = $tmp [1];
                }
                break;
            case 9 :
            case 12 :
            case 13 : {
                    $condicionAnexBorrados = " and anex_borrado = 'N'";
                    /*
                     * Se crea condicion de obligatoriedad clasificacion TRD
                     */
                    /** Se obtienen el parametro de configuración desde papiro cloud que indica si se debe hacer la
                     * validación de obligatoriedad para la reasignación y la devolución, y de acuerdo a ese 
                     parametro se define la condición del if de la validación para incluir o no estas dos acciones */
                    include($ruta_raiz.'/../core/config/config-inc.php');
                    $additional_validation_for_assigned_documents = false;
                    if(($codTx == 9 or $codTx == 12) && $allow_assign_to_and_return_documents_without_trd == false){
                        $additional_validation_for_assigned_documents = true;
                    }

                    if ($codTx == 16 or $codTx == 13 or $additional_validation_for_assigned_documents) {
                    //if ((( $codTx == 12) and $_SESSION ['id_rol'] != 1) or $codTx == 16) {
                        include_once "$ruta_raiz/include/db/ConnectionHandler.php";
                        $db = new ConnectionHandler("$ruta_raiz");

                        include_once ("../include/query/busqueda/busquedaPiloto1.php");
                        /*
                         * Condicion Radicado Padre
                         */
                        $anoRad = substr($record_id, 0, 4);
                        $isqlTRDP = "select $radi_nume_radi as RADI_NUME_RADI from SGD_RDF_RETDOCF r where r.RADI_NUME_RADI = '$record_id'";
                        if ($anoRad == "2005" or $anoRad == "2004" or $anoRad == "2003")
                            $pasaFiltro = "Si";
                        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
                        $rsTRDP = $db->conn->Execute($isqlTRDP);
                        $radiNumero = $rsTRDP->fields ["RADI_NUME_RADI"];

                        if (!($anoRad == "2005" or $anoRad == "2004" or $anoRad == "2003") && strlen(trim($radiNumero) == 0)) {
                            $pasaFiltro = "No";
                            $setFiltroSinTRD .= $record_id;
                            if ($i <= ($num)) {
                                $setFiltroSinTRD .= ",";
                            }
                            break;
                        }
                        $whereFiltro .= ' b.radi_nume_radi = ' . $record_id . ' or';
                        if ($codTx == 12) {
                            $isqlw = "select b.RADI_USU_ANTE as RADI_USU_ANTE, u.usua_esta  from radicado  b, usuario u where b.radi_nume_radi = " . $record_id . " AND b.RADI_USU_ANTE=u.USUA_LOGIN ";
                            $UsuIn = $db->query($isqlw);
                            $usuInActEst = $UsuIn->fields ["USUA_ESTA"];
                            $usuInAct = $UsuIn->fields ["RADI_USU_ANTE"];
                            if ($usuInAct == null) {
                                $pasaFiltro2 = "No";
                                break;
                            } else {
                                $pasaFiltro2 = "Si";
                            }
                        }
                        /*
                         * Condicion Anexos Radicados
                         */
                        $isqlTRDA = "select $radi_nume_salida as RADI_NUME_SALIDA from anexos
						where ANEX_RADI_NUME = '$record_id' and RADI_NUME_SALIDA != 0
						and RADI_NUME_SALIDA not in(select RADI_NUME_RADI from SGD_RDF_RETDOCF)";

                        if ($codTx == 12 || $codTx == 13 || $codTx == 9) {
                            $isqlTRDA .= $condicionAnexBorrados;
                        }
                        $rsTRDA = $db->conn->Execute($isqlTRDA);

                        while ($rsTRDA && !$rsTRDA->EOF && $pasaFiltro != "No") {
                            $radiNumero = $rsTRDA->fields ["RADI_NUME_SALIDA"];
                            $anoRadsal = substr($radiNumero, 0, 4);

                            if ($radiNumero != '' && !($anoRadsal == "2005" or $anoRadsal == "2004" or $anoRadsal == "2003")) {
                                $pasaFiltro = "No";
                                $setFiltroSinTRD .= $record_id;
                                if ($i <= ($num)) {
                                    $setFiltroSinTRD .= ",";
                                }
                                break;
                            }
                            $rsTRDA->MoveNext();
                        }
                        $i ++;
                    }
                    $whereFiltro .= ' b.radi_nume_radi = ' . $record_id . ' or';
                    if ($codTx == 12) {
                        $isqlw = "select upper(b.RADI_USU_ANTE) as RADI_USU_ANTE, u.usua_esta  from radicado  b, usuario u where b.radi_nume_radi = " . $record_id . " AND upper(b.RADI_USU_ANTE)=upper(u.USUA_LOGIN) ";
                        $UsuIn = $db->query($isqlw);
                        $usuInActEst = $UsuIn->fields ["USUA_ESTA"];
                        $usuInAct = $UsuIn->fields ["RADI_USU_ANTE"];

                        if (!$usuInAct or $usuInActEst == 0) {
                            $pasaFiltro2 = "No";
                        } else {
                            $pasaFiltro2 = "Si";
                        }
                    }
                    
                    /**
                     * Modificaciones Febrero de 2007, por SSPD para el DNP
                     * Archivar:
                     * Se verifica si el radicado se encuentra o no en un expediente,
                     * si es negativa la verificacion, ese radicado no se puede archivar
                     */
                    if ($codTx == 13 && $archivado_requiere_exp==0) {

                        include_once "$ruta_raiz/include/db/ConnectionHandler.php";
                        $db = new ConnectionHandler("$ruta_raiz");

                        $isqlExp = "select SGD_EXP_NUMERO as NumExpediente from SGD_EXP_EXPEDIENTE
						where RADI_NUME_RADI = '$record_id'";
                        $rsExp = $db->conn->Execute($isqlExp);
                        $resultadoJGL .= "CONSULTA: $isqlExp ";
                        if ($rsExp && !$rsExp->EOF) {
                            $expNumero = $rsExp->fields [0];

                            if ($expNumero == '' || $expNumero == null) {
                                $setFiltroSinEXP .= $record_id;
                                if ($jglCounter <= ($num)) {
                                    $setFiltroSinEXP .= ",";
                                }
                                break;
                            }

                            $rsExp->MoveNext();
                        } else {
                            $setFiltroSinEXP .= $record_id;
                            if ($jglCounter <= ($num)) {
                                $setFiltroSinEXP .= ",";
                            }
                        }
                        $jglCounter ++;
                    }
                }
                break;
            case 16 : {
                    /*
                     * Se crea condicion de obligatoriedad clasificacion TRD
                     */
                    include_once "$ruta_raiz/include/db/ConnectionHandler.php";
                    $db = new ConnectionHandler("$ruta_raiz");
                    include_once ("../include/query/busqueda/busquedaPiloto1.php");
                    /*
                     * Condicion Radicado Padre
                     */
                    $anoRad = substr($record_id, 0, 4);
                    $isqlTRDP = "select $radi_nume_radi as RADI_NUME_RADI from SGD_RDF_RETDOCF r where r.RADI_NUME_RADI = '$record_id'";
                    if ($anoRad == "2005" or $anoRad == "2004" or $anoRad == "2003")
                        $pasaFiltro = "Si";
                    $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
                    $rsTRDP = $db->conn->Execute($isqlTRDP);
                    $radiNumero = $rsTRDP->fields ["RADI_NUME_RADI"];

                    if (!($anoRad == "2005" or $anoRad == "2004" or $anoRad == "2003") && strlen(trim($radiNumero) == 0)) {
                        $pasaFiltro = "No";
                        $setFiltroSinTRD .= $record_id;
                        if ($i <= ($num)) {
                            $setFiltroSinTRD .= ",";
                        }
                        break;
                    }

                    /*
                     * Condicion Anexos Radicados
                     */
                    $isqlTRDA = "select $radi_nume_salida as RADI_NUME_SALIDA from anexos
						where ANEX_RADI_NUME = '$record_id' and RADI_NUME_SALIDA != 0
						and RADI_NUME_SALIDA not in(select RADI_NUME_RADI from SGD_RDF_RETDOCF)";
                    $condicionAnexBorrados = " and anex_borrado = 'N'";

                    $isqlTRDA .= $condicionAnexBorrados;

                    $rsTRDA = $db->conn->Execute($isqlTRDA);

                    while ($rsTRDA && !$rsTRDA->EOF && $pasaFiltro != "No") {
                        $radiNumero = $rsTRDA->fields ["RADI_NUME_SALIDA"];
                        $anoRadsal = substr($radiNumero, 0, 4);

                        if ($radiNumero != '' && !($anoRadsal == "2005" or $anoRadsal == "2004" or $anoRadsal == "2003")) {
                            $pasaFiltro = "No";
                            $setFiltroSinTRD .= $record_id;
                            if ($i <= ($num)) {
                                $setFiltroSinTRD .= ",";
                            }
                            break;
                        }
                        $rsTRDA->MoveNext();
                    }
                    $i ++;

                    $whereFiltro .= ' b.radi_nume_radi = ' . $record_id . ' or';

                    /**
                     * Modificaciones Febrero de 2007, por SSPD para el DNP
                     * Archivar:
                     * Se verifica si el radicado se encuentra o no en un expediente,
                     * si es negativa la verificacion, ese radicado no se puede archivar
                     */
                    //echo $codTx;&& $archivado_requiere_exp


                    include_once "$ruta_raiz/include/db/ConnectionHandler.php";
                    $db = new ConnectionHandler("$ruta_raiz");

                    $isqlExp = "select SGD_EXP_NUMERO as NumExpediente from SGD_EXP_EXPEDIENTE
						where RADI_NUME_RADI = '$record_id'";
                    $rsExp = $db->conn->Execute($isqlExp);
                    $resultadoJGL .= "CONSULTA: $isqlExp ";
                    if ($rsExp && !$rsExp->EOF) {
                        $expNumero = $rsExp->fields [0];

                        if ($expNumero == '' || $expNumero == null) {
                            $setFiltroSinEXP .= $record_id;
                            if ($jglCounter <= ($num)) {
                                $setFiltroSinEXP .= ",";
                            }
                            break;
                        }

                        $rsExp->MoveNext();
                    } else {
                        $setFiltroSinEXP .= $record_id;
                        if ($jglCounter <= ($num)) {
                            $setFiltroSinEXP .= ",";
                        }
                    }
                    $jglCounter ++;
                }
                break;
            // Inicio cambio 2 oct 2008 desarolla por  Hardy Deimont Nino V. para SSPD
            //tipificacion Masiva validacion no esten tipificadas
            case 19 : {
                    //include_once "$ruta_raiz/include/db/ConnectionHandler.php";
                    //$db = new ConnectionHandler("$ruta_raiz");


                    include_once ("../include/query/busqueda/busquedaPiloto1.php");
                    /*
                     * Condicion Radicado Padre
                     */
                    $anoRad = substr($record_id, 0, 4);
                    $isqlTRDP = "select $radi_nume_radi as RADI_NUME_RADI from SGD_RDF_RETDOCF r where r.RADI_NUME_RADI = '$record_id'";
                    if ($anoRad == "2005" or $anoRad == "2004" or $anoRad == "2003")
                        $pasaFiltro = "Si";
                    $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
                    $rsTRDP = $db->conn->Execute($isqlTRDP);
                    $radiNumero = $rsTRDP->fields ["RADI_NUME_RADI"];

                    //if( !($anoRad == "2005" or $anoRad == "2004" or $anoRad == "2003")  && strlen (trim($radiNumero)==0))
                    if (strlen(trim($radiNumero) == 0)) {
                        $radxtip .= $record_id;
                        if ($i <= ($num)) {
                            $radxtip .= ",";
                        }
                    } else {
                        $pasaFiltro = "No";
                        $setFiltroConTRD .= $record_id;
                        if ($i <= ($num)) {
                            $setFiltroConTRD .= ",";
                        }
                        break;
                    }
                }
                break;
            //fin modificacion		
            default : {
                    $whereFiltro .= ' b.radi_nume_radi = ' . $record_id . ' or';
                }
                break;
        }

        $setFiltroSelect .= "$record_id,";
    }
    if ($setFiltroSinTRD and $pasaFiltro == "No") {
        //Modificado idrd para aplicar trd
        $mensaje_error = "NO SE PERMITE ESTA OPERACION PARA LOS RADICADOS <BR> < $setFiltroSinTRD > <BR> FALTA CLASIFICACION TRD PARA ESTOS O PARA SUS ANEXOS <BR> FAVOR APLICAR TRD";
    }
    // Inicio cambio 2 oct 2008 desarolla por  Hardy Deimont Nino V. para SSPD
    //tipificacion Masiva validacion no esten tipificadas mensaje de error
    if ($setFiltroConTRD and $pasaFiltro == "No") {
        $mensaje_error = " NO SE PERMITE ESTA OPERACION PARA LOS RADICADOS <BR> < $setFiltroConTRD > <BR> YA TIENEN CLASIFICACION TRD";
    }
    //para no de volver al usuario inanctivo o sin usuario anterior 
    if ($pasaFiltro2 == "No") {
        IF ($usuInActEst == 0 or $usuInActEst){
	    if($usuInAct!=''){
                $mgsob = " El USUARIO < $usuInAct > <BR> SE ENCUENTRA INACTIVO";
	    }
	    else{
		$mgsob = "El radicado seleccionado no permite la acción requerida, está asignado al usuario quien lo generó.";
	    }
	}
        else
            $mgsob = 'NO HAY  USUARIO  ANTERIOR';
        $mensaje_error = "NO SE PERMITE ESTA OPERACION <BR> $mgsob";
    }
    /**
     * Modificaciones Febrero de 2007, por SSPD para el DNP
     * Archivar:
     * si la variable $setFiltroSinEXP tiene algo, es porque algun radicado no esta en expediente
     */
    if ($setFiltroSinEXP) {
        $mensaje_errorEXP = "<br>NO SE PERMITE ESTA OPERACION PARA LOS RADICADOS <BR> < $setFiltroSinEXP > <BR> PORQUE NO SE ENCUENTRAN EN NING&Uacute;N EXPEDIENTE";
    }

    if (substr($whereFiltro, - 2) == "or") {
        $whereFiltro = substr($whereFiltro, 0, strlen($whereFiltro) - 2);
    }
    if (trim($whereFiltro)) {
        $whereFiltro = "and ( $whereFiltro ) ";
    }
} else {
    $mensaje_error = "NO HAY REGISTROS SELECCIONADOS";
}
?>
<html>
    <head>
        <title>Enviar Datos</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link rel="stylesheet" href="<?php echo  $ruta_raiz; ?>/estilos/orfeo.css">
<?php // Inicio cambio 2 oct 2008 desarolla por  Hardy Deimont Nino V. para SSPD
//tipificacion Masiva validacion Function JavaScript
//5 de  noviembre 2008  correcion if
// Cambia de if($codTx=19) a if($codTx==19)
if ($codTx == 19) {
    ?>        <script type="text/javascript"
                    src="<?php echo  $ruta_raiz; ?>/js/tipificarMasiva.js">
            </script><?php 
            } 
            else {
            //  fin Inicio cambio 2 oct 2008 
            ?>
	    <SCRIPT Language="JavaScript" SRC="<?php echo $ruta_raiz; ?>/js/common.js"></SCRIPT>
            <script>
                        function notSupported()
                        {
                        alert('Su browser no soporta las funciones Javascript de esta pagina.');
                        }

                function setSel(start, end)
                {	document.realizarTx.observa.focus();
                        var t = document.realizarTx.observa;
                        if (t.setSelectionRange)
                {	t.setSelectionRange(start, end);
                        t.focus();
                        //f.t.value = t.value.substr(t.selectionStart,t.selectionEnd-t.selectionStart);
                }
                else notSupported();
                }

                function valMaxChars(maxchars)
                {	document.realizarTx.observa.focus();
                        if (document.realizarTx.observa.value.length > maxchars)
                {	/*  alert('Demasiados caracteres en el texto ! Por favor borre '+
                 (document.realizarTx.observa.value.length - maxchars)+ ' caracteres pues solo se permiten '+ maxchars);*/
                alert('Demasiados caracteres en el texto, solo se permiten ' + maxchars);
                        setSel(maxchars, document.realizarTx.observa.value.length);
                        return false;
                }
                else	return true;
                }

                /*
                 * OPERACIONES EN JAVASCRIPT
                 * @marcados Esta variable almacena el numeo de chaeck seleccionados.
                 * @document.realizarTx  Este subNombre de variable me indica el formulario principal del listado generado.
                 * @tipoAnulacion Define si es una solicitud de anulacion  o la Anulacion Final del Radicado.
                 *
                 * Funciones o Metodos EN JAVA SCRIPT
                 * Anular()  Anula o solicita esta dependiendo del tipo de anulacin.  Previamente verifica que este seleccionado algun  radicado.
                 * markAll() Marca o desmarca los check de la pagina.
                 *
                 */

                function Anular(tipoAnulacion)
                {
                marcados = 0;
                        for (i = 0; i < document.realizarTx.elements.length; i++)
                {	if (document.realizarTx.elements[i].checked == 1)
                {
                marcados++;
                }
                }
    <?php     if ($codusuario == 1 || $usuario_reasignacion == 1) {
        ?>
                    if (document.realizarTx){
                    if (document.realizarTx.chkNivel.checked == 1)	 marcados = marcados - 1;
                    }
        <?php     }
    ?>
                if (marcados >= 1)
                {
                return 1;
                }
                else
                {
                alert("Debe marcar un elemento");
                        return 0;
                }
                }

                function markAll(noRad)
                {
                if (document.realizarTx.elements.check_titulo.checked || noRad >= 1)
                {
                for (i = 3; i < document.realizarTx.elements.length; i++)
                {
                document.realizarTx.elements[i].checked = 1;
                }
                }
                else
                {
                for (i = 3; i < document.realizarTx.elements.length; i++)
                {
                document.realizarTx.elements[i].checked = 0;
                }
                }
                }

                function okTx()
                {
                valCheck = Anular(0);
                        if (valCheck == 0) return 0;
                        numCaracteres = document.realizarTx.observa.value.length;
                        if (numCaracteres >= 6)
                {	//alert(document.realizarTx.usCodSelect.options.selected);
                if (valMaxChars(550))
                        document.realizarTx.submit();
                } else
                {
                alert("Atencion:  Falta la observacion, el numero de caracteres minimo es de 6 letras, (Digito :" + numCaracteres + ")");
                }
                }

                /**
                 * funcion para contar el numero de caracteres de un textarea
                 * @autor Luis Jaime Benavides P. 
                 * @fecha 2011/11
                 **/
                contenido_textarea = ""
                        num_caracteres_permitidos = 300
                        function valida_longitud(){
                        num_caracteres = document.forms[0].observa.value.length
                                if (num_caracteres > num_caracteres_permitidos){
                        document.forms[0].observa.value = contenido_textarea
                        }
                        else{
                        contenido_textarea = document.forms[0].observa.value
                        }
                        if (num_caracteres >= num_caracteres_permitidos){
                        document.forms[0].caracteres.style.color = "#ff0000";
                        }
                        else{
                        document.forms[0].caracteres.style.color = "#000000";
                        }
                        cuenta()
                        }
                function cuenta(){
                var difer = num_caracteres_permitidos - document.forms[0].observa.value.length
                        document.forms[0].caracteres.value = difer
                }
		function contar_extra(){
		    var objectDa=document.getElementById('depe2info');
		    var depinfsel;
		    var selist=document.getElementById('action_info');
		    var cont = 0;
		    for(i=0;i<objectDa.options.length;i++){
			if(objectDa.options[i].selected){
			    if(cont==0){
			    	depinfsel=objectDa.options[i].value;
			    }
			    else{
				depinfsel+=","+objectDa.options[i].value;
			    }
			    cont=cont+1;
			}
		    }
		    if(cont==0){
		   	alert("No ha seleccionado ninguna dependencia a Informar");
			return false;
		    }
		    var pstvar="depinfsel="+depinfsel;
		    partes('operFormenvio.php','response',pstvar,'');
		    objectDa.setAttribute('disabled','disabled');
		    selist.setAttribute('disabled','disabled');
		}
		function extra_informa(){
		    //var comboUsuario=document.getElementById('usua2info');
		    if( document.getElementById('usua2info') === null){
			alert('Debe desplegar el listado de usuarios a informar');
			return false;
		    }
		    var comboUsuario=document.getElementById('usua2info').options,conta=0;
		    for(var i=0;i<comboUsuario.length;i++){
			if(comboUsuario[i].selected) conta++;
		    }
		    if(conta==0){
			alert('Seleccione al menos un usuario a informar');
                        return false;
		    }
		    document.getElementById('statusinf').value='1';
		    vistaFormUnitid('tabinfo',2);
		    vistaFormUnitid('postinf',1);
		}
            </script>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        </head>
        <body bgcolor="#FFFFFF" topmargin="0" onLoad="markAll(1);">
    <?php     // Inicio cambio 2 oct 2008 desarolla por  Hardy Deimont Nino V. para SSPD
//tipificacion Masiva validacion no esten tipificadas
}
//fin cambio 2 de octubre
/**
 * Modificaciones Febrero de 2007, por SSPD para el DNP
 * Archivar:
 * Hay algun error, ya sea por tipificacion o por Expediente, luego se muestra mensaje
 * donde se indica que no se puede archivar el(los) radicado(s)
 */
if ($mensaje_errorEXP || $mensaje_error) {
    die("<center><table class='borde_tab' width=100% CELSPACING=5><tr class=titulosError><td align='center'>$mensaje_errorEXP <br> $mensaje_error</td></tr></table></CENTER>");
} //tipificacion Masiva validacion no esten tipificadas
//5 de  noviembre 2008 correcion if
// Cambia de elseif($codTx=19) a elseif($codTx==19)
elseif ($codTx == 19) {
    Include ('txTipificar.php');
} //fin cambio 2 oct 2008
else {
    ?>
            <table border=0 width=100% cellpadding="0" cellspacing="0"
                   align='center'>
                <tr>
                    <td width=100% align='center'><br>
                        <form action='realizarTx.php?<?php echo  $encabezado ?>' method='POST'
                              name="realizarTx"><input type='hidden' name=depsel8
                                                 value="<?php echo  implode($depsel8, ',') ?>"> <input type='hidden'
                                                 name=codTx value='<?php echo  $codTx ?>'> <input type='hidden' name=EnviaraV
                                                 value='<?php echo  $EnviaraV ?>'> <input type='hidden' name=fechaAgenda
                                                 value='<?php echo  $fechaAgenda ?>'>
                            <table width="98%" border="0" cellpadding="0" cellspacing="5"
                                   class="borde_tab">
                                <TR>
                                    <TD width='30%' class="titulos4">USUARIO:  <?php echo  $_SESSION ['usua_nomb'] ?> </TD>
                                    <TD width='30%' class="titulos4">DEPENDENCIA: <?php echo  $_SESSION ['depe_nomb'] ?><br>
                                    </TD>
                                    <td width='40%' class="titulos4">
    <?php     //$db->conn->debug = true;
    $rolsql = '';
    switch ($codTx) {
        case 7 : {
                print "Borrar Informados ";
                echo "<input type='hidden' name='info_doc' value='" . $tmp_arr_id . "'>";
            }
            break;
        case 8 :
            $usDefault = 1;
            //$cad = $db->conn->Concat("RTRIM(u.depe_codi)","'-'","RTRIM(u.usua_codi)");
            //$cad2 = $db->conn->Concat($db->conn->IfNull("d.DEP_SIGLA", "'N.N.'"),"'-'","RTRIM(u.usua_nomb)");
            $cad = $db->conn->Concat("CAST(ur.depe_codi as char(6))", "'-'", "cast(u.usua_codi as char(6))");
            $cad2 = $db->conn->Concat($db->conn->IfNull("d.DEP_SIGLA", "'N.N.'"), "'-'", "RTRIM(u.usua_nomb)");
            /* $sql = "select $cad2 as usua_nomb, $cad as usua_codi from usuario u,dependencia d 
              where u.depe_codi in(" . implode($depsel8, ',') . ")
              $whereReasignar and u.USUA_ESTA='1' and u.depe_codi = d.depe_codi ORDER BY usua_nomb"; */
            $sql = "SELECT 
                          $cad2 as usua_nomb, $cad as usua_codi 
                         FROM  	usuario U,   sgd_urd_usuaroldep UR,  DEPENDENCIA D 
						WHERE   u.usua_codi = UR.usua_codi
  						AND   UR.DEPE_CODI  in(" . implode($depsel8, ',') . ")
  						and ur.depe_codi=d.Depe_codi
  						AND U.USUA_ESTA='1' order by usua_nomb asc					";

            $rs = $db->conn->Execute($sql);
            $usuario = $codUsuario;
            print "Informados";
            $selectFuncionariosInfo = $rs->GetMenu2('usCodSelect[]', $usDefault, false, true, 10, " id='usCodSelect' class='select' ");
            break;
        case 9 :
            $whereDep = "and url.depe_codi=$depsel ";
            if ($dependencia == $depsel) {
                $usDefault = $codusuario;
            }
            $usuario_publico = "";

            if (
                (($_SESSION["usuario_reasignar"] == 1 || $_SESSION["assign_document_to_any_user"] == 1) && $dependencia != $depsel)
                ||
                ($dependencia == $depsel && ($id_rol != 1 || $usuario_reasignacion != 1) && $EnviaraV == "VoBo")) {

                /**
                 * @todo Reemplazar la asignación del permiso quemada por una consulta a la tabla de permisos
                 */
                $permission_assign_to_any_user = ($_SESSION["assign_document_to_any_user"]=='1')?true:false;

                if($permission_assign_to_any_user){
                    $whereReasignar = " AND r.sgd_rol_id = url.rol_codi";
                }
                else{
                    $whereReasignar = " AND r.sgd_rol_nombre = 'Jefe' AND r.sgd_rol_id = url.rol_codi";
                }

                $rolsql = ', sgd_rol_roles r';
                $usDefault = 1;

                /**
                 * Define la consulta adicional para traer los usuarios públicos
                 */
                $usuario_publico = "UNION
                    SELECT 
                      u.usua_codi, 
                      url.depe_codi, 
                      u.usua_nomb , 
                      cast(url.depe_codi as char(4))||'-'||cast( u.usua_codi as char(4))||'-'||cast( url.rol_codi as char(3)) as USUA_COD, 
                      url.rol_codi 
                    FROM 
                      usuario u, 
                      sgd_urd_usuaroldep url,
                      sgd_drm_dep_mod_rol drm,
                      sgd_mod_modules modules
                    WHERE 
                      u.usua_codi = url.usua_codi   
                      AND drm.sgd_drm_rolcodi = url.rol_codi
                      AND modules.sgd_mod_id = drm.sgd_drm_modecodi
                      AND drm.sgd_drm_depecodi = url.depe_codi
                      AND modules.sgd_mod_modulo = 'usuario_publico'
                      AND drm.sgd_drm_valor = '1'
                      AND u.usua_codi!= ".$codusuario." 
                      AND u.usua_esta = '1'
                      AND url.depe_codi=".$depsel;
            }

            if (($id_rol == 1 || $usuario_reasignacion == 1) && $dependencia == $depsel && $EnviaraV == "VoBo") {
                if ($objDep->Dependencia_codigo($dependencia)) {
                    $depPadre = $objDep->getDepe_codi_padre();
                }
                if (!$depPadre || $id_rol != 1 && $usuario_reasignacion == 1 && $EnviaraV == "VoBo") {
                    $depPadre = $dependencia;
                } else {
                    print ("La dependencia  padre ...($depPadre) ");
                }
                $rolsql = ',   sgd_rol_roles r';
                $whereDep = " and url.depe_codi=$depPadre AND   r.sgd_rol_nombre = 'Jefe' AND r.sgd_rol_id = url.rol_codi ";
                $depsel = $depPadre;
            }

            if ($EnviaraV == "VoBo") {
                $proccarp = "Visto Bueno";
                $usuario_publico = "";
            }
            $cad = $db->conn->Concat("cast(url.depe_codi as char(4))", "'-'", "cast( u.usua_codi as char(4))", "'-'", "cast( url.rol_codi as char(3))");
            /*   $sql = "select	u.USUA_NOMB, $cad as USUA_COD,u.DEPE_CODI,u.USUA_CODI from usuario u	where u.USUA_ESTA='1'
              $whereReasignar	$whereDep $usuario_publico ORDER BY USUA_NOMB"; */
	    $ADODB_COUNTRECS=true;
            $sql = "SELECT u.usua_codi,  url.depe_codi,   u.usua_nomb   , $cad as USUA_COD, url.rol_codi
                            FROM   usuario u,   sgd_urd_usuaroldep url $rolsql
                            WHERE u.usua_esta = '1' AND u.usua_codi!= $codusuario and 
                                   u.usua_codi = url.usua_codi  
                       $whereReasignar  $whereDep $usuario_publico";

            $rs = $db->conn->Execute($sql);
	    $recoun=$rs->RecordCount();
            $usuario = $codUsuario;
            //print $rs->GetMenu2('usCodSelect',$usDefault,false,false,0," id ='usCodSelect' class='select' ");
            $selectFuncionarios = "<select name=usCodSelect class=select>	<option value='-1'>-- Seleccione un funcionario --</option>";
            while (!$rs->EOF) {

                $depCodiP = $rs->fields ["DEPE_CODI"];
                $usuNombP = $rs->fields ["USUA_NOMB"];
                $usuCodiP = $rs->fields ["USUA_COD"];
                $usuCodi = $rs->fields ["USUA_CODI"];
		$usuRol = $rs->fields ["ROL_CODI"];
                $valOptionP = "";
                $valOptionP = $usuNombP;
                $class = "";
                if ($usuRol == 1) {
                    $defaultUs = "selected";
                } else {
                    $defaultUs = "";
                }
                if ($depCodiP != $dependencia) {
                    $sql = "select DEPE_NOMB from dependencia where depe_codi=$depCodiP";
                    $rs2 = $db->conn->Execute($sql);
                    $depNombP = $rs2->fields ["DEPE_NOMB"];
                    $valOptionP .= " [ " . $depNombP . "] ";

                    $class = " class='leidos'";
                }

                $selectFuncionarios .= "<option $class  value='$usuCodiP' $defaultUs>$valOptionP</option>";

                $rs->MoveNext();
            }
            print "Reasignar $proccarp";
            echo $selectFuncionarios .= "</select>";
            break;
        case 10 :
            $carpetaTipo = substr($carpSel, 1, 1);
            $carpetaCodigo = intval(substr($carpSel, - 3));
            if ($carpetaTipo == 1) {
                $sql = "select NOMB_CARP as carp_desc from CARPETA_PER
					   where
					     codi_carp=$carpetaCodigo
						 and usua_codi=$codusuario
						 and depe_codi=$dependencia";
            } else {
                $sql = "select carp_desc from carpeta where carp_codi=$carpetaCodigo";
            }
            $rs = $db->conn->Execute($sql); # Ejecuta la busqueda y obtiene el recordset vacio
            $carpetaNombre = $rs->fields ['carp_desc'];
            print "Movimiento a Carpeta <b>$carpetaNombre</b>
				<input type=hidden name='carpetaCodigo' value=$carpetaCodigo>
				<input type=hidden name='carpetaTipo' value=$carpetaTipo>
				<input type=hidden name='carpetaNombre' value=$carpetaNombre>
				";
            break;
        case 12 :
            print "Devolver documentos a Usuario Anterior ";
            break;
        case 13 :
            print "Archivo de Documentos";
            break;
        /* case 16:
          print "Archivo de NRR";
          break; */
    }
    ?>
                                        <BR>
                                    </td>
                                    <td width='1' class="grisCCCCCC"><input type=button value=REALIZAR
                                                                            onClick="<?php                                         if ($codTx == 8) {
                                            ?> if (document.getElementById('usCodSelect').value == ''){ alert('Debe seleccionar al menos un funcionario.'); return false; }<?php                                 }
                                ?> <?php                                 if ($codTx == 9) {
                                    ?>
                                    if (document.realizarTx.usCodSelect.value == '-1')
                                    {
                                    alert('Debe seleccionar un funcionario.');
                                            return false;
                                    }
                                                                                <?php                                                                             }
                                                                            ?> okTx();"
                                                                            name=enviardoc align=bottom class=botones id=REALIZAR></td>
                                </TR>
					<?php 					if($EnviaraV == "VoBo"){
                                        ?>
                                    <tr style="border:1px solid black">
                                    <td colspan=4 class='info'><b>RECUERDE:</b> La acción que va a realizar es envió de Visto Bueno al jefe del área de un 
				    radicado que puede ser una solicitud, respuesta o tramite que se requieren dentro y fuera de la entidad.
                                    Describa claramente el trámite o diligencia que el radicado requiera en función de la entidad y la
                                    gestión documental del mismo.</td></tr>
                                        <?php                                 }elseif($codTx == 9){
                                    ?>
                                    <tr style="border:1px solid black">
                                    <td colspan=4 class='info'><b>RECUERDE:</b> La acción que va a realizar es la reasignación de un radicado a alguno de los funcionarios de la entidad.
Describa claramente el trámite o diligencia que el radicado requiera en función de la entidad y la gestión documental del mismo.</td></tr>
                                    <?php 			}elseif($codTx == 12){
			    ?>
			    <tr style="border:1px solid black">
                                    <td colspan=4 class='info'>Esta acción permite el regreso del radicado a la bandeja de Devueltos al funcionario que realizo la
última transacción al documento.<br>
			    &nbsp;&nbsp;<b>RECUERDE:</b> Solo se devolverá al último funcionario que lo manipulo, en caso requerido sea otro funcionario usar la opción de Reasignar.</td></tr>
			    <?php 			}
			elseif($codTx==13){
			?>
			    <tr style="border:1px solid black">
                                    <td colspan=4 class='info'>Finalizado el proceso de gestión del radicado se recomienda el archivo del mismo, para su consulta
					mediata o inmediata; verifique el proceso que se ha realizado al documento y digite el asunto del 
					archivo. El documento reposará en el archivo central del ORFEO.</td></tr>
			<?php                         }
			elseif ($codTx==10){
			?>
			    <tr style="border:1px solid black">
                                    <td colspan=4 class='info'>Esta acción permite desplazar el radicado a cualquier carpeta de procesos de radicación dentro del
					usuario. Digite el motivo del movimiento del documento de carpeta.</td></tr>
			<?php 			}
			elseif ($codTx==8){
			   ?>
			   <tr style="border:1px solid black">
                                    <td colspan=4 class='info'>Seleccione del listado el y/o los funcionarios que le enviará copia del radicado.
				    Digite claramente el asunto y objetivo del envío del radicado como informado.</td></tr>
			    <?php 			}elseif ($codTx==14){
                           ?>
                           <tr style="border:1px solid black">
                                    <td colspan=4 class='info'>Digite el texto que describa el asunto, objetivo e importancia al agendar el radicado seleccionado.
				    </td></tr>
                            <?php 			}
			?>
			<tr align="center">
                                    <td colspan="4" class="celdaGris" align=center> <br>
                                        <?php                                         if ((($codusuario == 1) || ($usuario_reasignacion == 1)) && $codTx != 13) {
                                        ?>		
                                        <input type=checkbox
                                               name=chkNivel checked class=ebutton> <span class="info">El
                                            documento tomara el nivel del usuario destino.</span><br>
                                        <?php                                         } elseif ($codTx == 13) {
                                        ?>
                                        <input type="hidden"
                                               name="usCodSelect"> <input type="hidden" name=chkNivel> <span
                                               class="info">El documento conservar&aacute; el nivel del usuario
                                            que archiva.</span><br>
    <?php }
?>
                                    <!-- ------------Contar Caracteres-------------- -->
                            <center>
                                <table width="500" border=0 align="center" bgcolor="White">
                                    <tr bgcolor="White">
                                        <td width="100">
                                    <center>
                                        <img src="<?php echo  $ruta_raiz ?>/imagenes/iconos/tuxTx.gif" alt="Tux Transaccion" title="Tux Transaccion">
                                    </center>
                                    </td>
                                    <form action="#" method="post">			
                                        <td align="left"><span class="etextomenu"> </span> 
                                            <textarea name="observa" id="observa" cols="70" rows="3" class="ecajasfecha" onkeydown="valida_longitud()" onkeyup="valida_longitud()"></textarea>
                                            <span class="info">Caracteres escritos</span>
                                            <input type="text" name="caracteres" size="4" style="border: thin solid #e3e8ec">
                                        </td>
                                    </form>			
                                    <td>
                                        <?php  echo $selectFuncionariosInfo; ?>
                                    </td>
                                    </tr>
                                    <input type=hidden name=enviar value=enviarsi>
                                    <input type=hidden name=enviara value='9'>
                                    <input type=hidden name=carpeta value=12>
                                    <input type=hidden name=carpper value=10001>
                                    <!-- </td> 
                                    </tr> -->
                                </table>
                            </center>
                            <br>
                            <!-- ----------------------------------------- -->        

                            <?php                             /*  GENERACION LISTADO DE RADICADOS
                            *  Aqui utilizamos la clase adodb para generar el listado de los radicados
                            *  Esta clase cuenta con una adaptacion a las clases utiilzadas de orfeo.
                            *  el archivo original es adodb-pager.inc.php la modificada es adodb-paginacion.inc.php
                            */
                            error_reporting ( 0 );
                            if (! $orderNo)
                            $orderNo = 0;
                            $order = $orderNo + 1;

                            $sqlFecha = $db->conn->SQLDate ( "d-m-Y H:i A", "b.RADI_FECH_RADI" );
                            include_once "../include/query/tx/queryFormEnvio.php";
                            //$db->conn->debug = true;
                            switch ($codTx) {
                            case 12 :
                            {
                            $isql = str_replace ( "Enviado Por", "Devolver a", $isql );
                            }
                            break;
                            default :
                            break;
			    case 9:
				if($EnviaraV != "VoBo"){
				$txsql="select d.depe_nomb,d.depe_codi from dependencia d, sgd_urd_usuaroldep urd where urd.depe_codi=d.depe_codi and rol_codi=1 
					and depe_estado=1 and d.depe_codi<>999 order by d.depe_codi";
				$rsTxInf=$db->conn->Execute($txsql);
				echo "<input type='hidden' name='statusinf' id='statusinf' value='0'><div id='preinf'><center><input type='button' class='botones_mediano' value='Informar A' onclick=\"vistaFormUnitid('tabinfo',1);vistaFormUnitid('preinf',2);\"></center></div>";
				echo "<div id='tabinfo' style='display:none;'><table width='500' style='border:0;border-collapse:collapse;'>\n";
				echo "<tr><th width='245'>Deependencia a informar</th><th width='10'></th><th width='245'>Usuario a Informar</th></tr><td>\n";
				print $rsTxInf->GetMenu2('depe_selinfo[]',1,false,true,5,"class='select' id='depe2info'");
				echo "</td><td>\n";
				echo "<input class='botones_2' value='>>' onclick='return contar_extra()' id='action_info'></td>\n";
				echo "<td><div id='response'></div></td></tr>
				<tr><td colspan='3' align='center'><input type='button' class='botones_mediano' value='Informar'  onclick=\"extra_informa();\"></td></tr></table></div>\n";
				echo "<div id='postinf' style='display:none;'><center><span class='info'>Datos Cargados, una vez realizada la reasignaci&oacute;n se finalizara la transacci&oacute;n de informados</span></center></div>";
				}
			    break;
                            }
                            $pager = new ADODB_Pager ( $db, $isql, 'adodb', true, $orderNo, $orderTipo );
                            $pager->toRefLinks = $linkPagina;
                            $pager->toRefVars = $encabezado;
                            $pager->checkAll = true;
                            $pager->checkTitulo = true;
                            $pager->Render ( $rows_per_page = 20, $linkPagina, $checkbox = chkAnulados );
                            ?>
                            <input type='hidden' name=depsel
                                   value='<?php echo  $depsel ?>'>
                            </form>
                            <?php                             }

                            ExpMasivos:{
                            if($codTx == 21)
                            {
                            $h=0;
                            while ( list ( $recordid, $tmp ) = each ( $checkValue ) ) {
                            $record_id = $recordid;
                            $pruebamas[$h] = $record_id;
                            $h++;
                            }
                            $_SESSION['radsmas'] = $pruebamas;


                            include "../expediente/insertarExpedienteMas.php";
                            }
                            }

                            ?>


                            </body>
                            </html>
