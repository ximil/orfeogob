<?php 
/**
 * verLinkArchivo es la clase encargada de
 * validar los permisos de acceso a un documento (imagen informacion)
 * @author Liliana Gomez Velasquez
 * @version     1.0
 * @fecha  09 sep 2009
 */                  
class verLinkArchivo{

 /**
   * Variable que se corresponde con su par, uno de los campos de la tabla SGD_TDEC_TIPODECISION
   * @db Objeto conexion
   * @access public
   */
   var $db;

/**
   * Vector que almacena el resultado de la validacion
   * @var string
   * @access public
   */
	var $vecRads;
/**
   * Vector que almacena el resultado de la validacion
   * de un Anexo
   * @var string
   * @access public
   */
	var $vecRadsA;
	
/** 
* Constructor encargado de obtener la conexion
* @param	$db	ConnectionHandler es el objeto conexion
* @return   void
*/
	function verLinkArchivo($db) {
	  /**
     * Constructor de la clase 
	* @db variable en la cual se recibe el cursor sobre el cual se esta trabajando.
	*
	*/
	$this->db = $db;
 }


/** 
* Retorna el valor correspondiente al 
* resultado de la validacion
* @numrad  Numero del Radicado a validar
* @return   array  $vecRads resultado de la operacion de validacion
*/
function valPermisoRadi($numradi){

    /* Make available the audit trail (papirocloud app) configuration vars */
    global $papirocloud_api_key;
    global $papirocloud_api_key_user;
    global $papirocloud_api_url;

// Busca el Documento del usuario Origen
 $ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
 $this->db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
 $verImg = "NO";
 /*$isql = "select r.RADI_PATH, r.SGD_SPUB_CODIGO,r.radi_reservado, u.CODI_NIVEL, u.USUA_NOMB,u.USUA_DOC,
         r.RADI_USU_ANTE, r.RADI_DEPE_ACTU
         from RADICADO r, USUARIO u
         where r.RADI_NUME_RADI='$numradi'
         and r.RADI_USUA_ACTU= u.USUA_CODI
         and r.RADI_DEPE_ACTU= u.DEPE_CODI";*/
 $isql = "SELECT r.RADI_PATH, r.SGD_SPUB_CODIGO,drm.sgd_drm_valor as CODI_NIVEL,  u.usua_nomb,   u.usua_doc,  r.RADI_USU_ANTE, r.RADI_DEPE_ACTU
          FROM   RADICADO r,  usuario u,   sgd_urd_usuaroldep urd,   sgd_mod_modules mod,   sgd_drm_dep_mod_rol drm  
          WHERE r.RADI_NUME_RADI='$numradi' and
          u.usua_codi = urd.usua_codi   and r.RADI_USUA_ACTU= u.USUA_CODI AND
          mod.sgd_mod_id = drm.sgd_drm_modecodi AND  drm.sgd_drm_rolcodi = urd.rol_codi AND
          drm.sgd_drm_depecodi = urd.depe_codi   and r.RADI_DEPE_ACTU= urd.depe_codi AND  mod.sgd_mod_modulo = 'codi_nivel'";
   $rs=$this->db->conn->query($isql);
   $consultaExpediente="SELECT SGD_EXP_NUMERO  FROM SGD_EXP_EXPEDIENTE 
				     WHERE radi_nume_radi= $numradi AND sgd_exp_fech=(SELECT MIN(SGD_EXP_FECH) as minFech  
				     from sgd_exp_expediente where radi_nume_radi= $numradi  and sgd_exp_estado<>2)  
				     and sgd_exp_estado<>2";

   $rsE=$this->db->conn->query($consultaExpediente);
	
   if (!$rsE->EOF){
	   $fldsSGD_EXP_SUBEXPEDIENTE=$rsE->fields["SGD_EXP_NUMERO"];}
   else{ 
       $fldsSGD_EXP_SUBEXPEDIENTE= "";}
   $queryExp2="select 1 from sgd_exp_expediente e, sgd_sexp_secexpedientes sexp where e.sgd_exp_numero=sexp.sgd_exp_numero and e.radi_nume_radi=$numradi
 	and sexp.depe_codi in (".implode(',',$_SESSION['depeMatrizPer']).") and sgd_exp_estado<>2";
   $rsExp2=$this->db->conn->query($queryExp2);
   $expDepePer=0;
   if(!$rsExp2->EOF){
	$expDepePer=1;
   }
  //Consulta Informados
  $usuaInformado= "";
    $isqlI = "select USUA_DOC from INFORMADOS where RADI_NUME_RADI='$numradi'and USUA_DOC= '" .$_SESSION[ 'usua_doc' ]."'";
   $rsI=$this->db->conn->query($isqlI);
   if (!$rsI->EOF){
	 $usuaInformado=$rsI->fields["USUA_DOC"];
   } 
   
  if (!$rs->EOF){
	 $seguridadRadicado=$rs->fields["SGD_SPUB_CODIGO"];
	// $reservaRadicado = $rs->fields["RADI_RESERVADO"];  //23-SEP-2010 EJMUNOZ INCLUIR RESERVADO POR ELABORACION 
     $nivelRadicado=$rs->fields["CODI_NIVEL"];
     $USUA_ACTU_R = $rs->fields["USUA_DOC"];
     $USUA_ANTE = $rs->fields["RADI_USU_ANTE"];
     $DEPE_ACTU_R = $rs->fields["RADI_DEPE_ACTU"];
     $pathImagen = $rs->fields['RADI_PATH'];
     
   if($USUA_ACTU_R == $_SESSION["usua_doc"] || $usuaInformado == $_SESSION["usua_doc"]){
     	$verImg = "SI"; 
     } elseif(isset( $fldsSGD_EXP_SUBEXPEDIENTE ) ){
           //Consultamos el documento del usuario responsable del expediente
	       $consultaDuenoExp="SELECT USUA_DOC_RESPONSABLE	FROM SGD_SEXP_SECEXPEDIENTES 
				         WHERE SGD_EXP_NUMERO = '$fldsSGD_EXP_SUBEXPEDIENTE'";
           $rsExpDueno=$this->db->conn->query($consultaDuenoExp);
	       $duenoExpediente=$rsExpDueno->fields["USUA_DOC_RESPONSABLE"];
           /*
		    * Modificado el 29092009
		    * para el manejo de seguridad Radicado incluido en mas de un expediente
		   */
		     if (  $duenoExpediente != $_SESSION[ 'usua_doc' ]) {
	             $sqlExpR = "SELECT 
					  EXP.SGD_EXP_PRIVADO AS PRIVEXP, USUA_DOC_RESPONSABLE AS RESPONSABLE
				  FROM 
					  RADICADO R, SGD_SEXP_SECEXPEDIENTES SEXP, SGD_EXP_EXPEDIENTE EXP
				  WHERE 
					  R.RADI_NUME_RADI=$numradi 
					  AND R.RADI_NUME_RADI = EXP.RADI_NUME_RADI 
					  AND EXP.SGD_EXP_NUMERO = SEXP.SGD_EXP_NUMERO
					  AND EXP.sgd_exp_estado<>2
					  AND USUA_DOC_RESPONSABLE = "."'".$_SESSION[ 'usua_doc' ]."'" ; 
		          $rsER = $this->db->conn->query( $sqlExpR );
		          if (!$rsER->EOF){
		            
		             $responsableExp = $rsER->fields["RESPONSABLE"];
		          }
	         }
           //Si el usuario que consulta es: usuario actual o responsable del expediente puede ver el Radicado
	       if (  $duenoExpediente == $_SESSION[ 'usua_doc' ] || $responsableExp == $_SESSION[ 'usua_doc' ]) {
	           $verImg = "SI";
	
	       }elseif ($seguridadRadicado==1){// || $reservaRadicado == 'S'){  //23-SEP-2010 EJMUNOZ INCLUIR RESERVADO POR ELABORACION 
	           if ($DEPE_ACTU_R == '999' && $USUA_ANTE ==  $_SESSION[ 'krd' ] ){
	       	       $verImg = "SI";
	           }
	       	}elseif($_SESSION["codi_nivel"] >= $nivelRadicado && $seguridadRadicado==0 && (in_array($DEPE_ACTU_R,$_SESSION['depeMatrizPer']) || $expDepePer==1)){
	           $verImg = "SI";
	            }
	 }elseif($seguridadRadicado==1){ //|| $reservaRadicado == 'S'){ //23-SEP-2010 EJMUNOZ INCLUIR RESERVADO POR ELABORACION 
       if ($DEPE_ACTU_R == '999' && $USUA_ANTE ==  $_SESSION[ 'krd' ] ){
	       	   $verImg = "SI";
	       }
	    }/*elseif($_SESSION["codi_nivel"] >= $nivelRadicado){
	       $verImg = "SI";
     }*/
    // echo $_SESSION["codi_nivel"]." >= $nivelRadicado";
  }else{
  	$verImg = "NO SE ENCONTRO INFORMACION DEL RADICADO";
  }
        if($_SESSION["codi_nivel"] >= $nivelRadicado && $seguridadRadicado==0 && $DEPE_ACTU_R==999 && $fldsSGD_EXP_SUBEXPEDIENTE==""){
	    $verImg = "SI";
	}
	elseif($_SESSION["codi_nivel"] >= $nivelRadicado && $seguridadRadicado==0 && (in_array( $DEPE_ACTU_R,$_SESSION['depeMatrizPer']) || $expDepePer==1)){
	    $verImg = "SI";
	}

    /* Get the username (usuario.usua_login) using the id user, this is done because
        is needed to pass the username to papiro cloud audit module to maintain legacy
        with Orfeo systems where the usuario.usua_codi attribute is not unique. */
    $username = $_SESSION['krd'];

    /* Consulta el permiso de acceso desde el api de papiro cloud */
    /* Get ready the curl object to call the papiro cloud restful service */
    $papirocloud_api = curl_init();
    curl_setopt($papirocloud_api, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($papirocloud_api, CURLOPT_URL, $papirocloud_api_url.'/document/'.$numradi.'/access');
    curl_setopt($papirocloud_api, CURLOPT_POST, 1);

    /* Prepare the array to register the event on papirocloud audit trail module */
    $data = array(
        'key' => $papirocloud_api_key,
        'key_user' => $papirocloud_api_key_user,
        'user_id' => $username,
    );
    curl_setopt($papirocloud_api, CURLOPT_POSTFIELDS, $data);
    curl_exec($papirocloud_api);
    $document_access_http_response = curl_getinfo($papirocloud_api,CURLINFO_HTTP_CODE);
    curl_close($papirocloud_api);

    $vecRadsD['verImg'] = ($document_access_http_response == 200)?'SI':'NO';
    $vecRadsD['pathImagen']= $pathImagen;
    $vecRadsD['numExpe']= $fldsSGD_EXP_SUBEXPEDIENTE;

    return $vecRadsD;
}


/** 
* Retorna el valor correspondiente al 
* resultado de la validacion
* @numrad  Numero del Anexo a validar
* @return   array  $vecRadsA resultado de la operacion de validacion
*/
function valPermisoAnex($numAnex){

// Busca el Documento del usuario Origen
 $ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
 $this->db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
 $verImg = "SI";
 $pathImagen = ""; 
 $isqlAnex = "select ANEX_NOMB_ARCHIVO
         from ANEXOS
         where ANEX_CODIGO = '$numAnex'";
   $rsAnex=$this->db->conn->query($isqlAnex);
  if (!$rsAnex->EOF){
	 $pathImagen = trim($rsAnex->fields["ANEX_NOMB_ARCHIVO"]);
	 $numeradi = trim($rsAnex->fields["RADI_NUME_SALIDA"]);	 
   }else{
  	$verImg = "NO SE ENCONTRO INFORMACION DEL RADICADO";
  }
        $vecRadsA['verImg'] = $verImg;
        $vecRadsA['pathImagen']= $pathImagen;
        
        return $vecRadsA;
 }
}

?>
