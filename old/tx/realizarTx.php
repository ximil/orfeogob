<?php session_start();
date_default_timezone_set('America/Bogota');
putenv("NLS_LANG=American_America.UTF8");
foreach ($_GET as $key => $valor)
    $$key = $valor;
foreach ($_POST as $key => $valor)
    $$key = $valor;

//print_r($_POST);
if ($_POST["usCodSelect"])
    $usCodSelect = $_POST["usCodSelect"];
$krd = $_SESSION["krd"];
$dependencia = $_SESSION["dependencia"];
$depe_nomb = $_SESSION["depe_nomb"];
$usua_nomb = $_SESSION["usua_nomb"];
$usua_doc = $_SESSION["usua_doc"];
$codusuario = $_SESSION["codusuario"];
$id_rol = $_SESSION["id_rol"];
$ruta_raiz = "..";
include($ruta_raiz . '/validadte.php');
error_reporting(0);
/*  REALIZAR TRANSACCIONES
 *  Este archivo realiza las transacciones de radicados en Orfeo.
 */
//Cargando variables del log de usuario
if (isset($_SERVER['HTTP_X_FORWARD_FOR'])) {
    $proxy = $_SERVER['HTTP_X_FORWARD_FOR'];
} else
    $proxy = $_SERVER['REMOTE_ADDR'];
$REMOTE_ADDR = $_SERVER['REMOTE_ADDR'];
$ruta_raiz2 = $ruta_raiz;
$ruta_raiz = '../..';
include_once "$ruta_raiz/core/clases/log.php";
$log = new log($ruta_raiz);
$log->setAddrC($REMOTE_ADDR);
$log->setProxyAd($proxy);
$ruta_raiz = $ruta_raiz2;
$log->setUsuaCodi($codusuario);
$log->setDepeCodi($dependencia);
$log->setRolId($id_rol);
$log->setDenomDoc($doc = 'Radicado');
?>
<html>
    <head>
        <title>Realizar Transaccion - Orfeo </title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link rel="stylesheet" href="../estilos/orfeo.css">
    </head>
    <?php     /**
     * Inclusion de archivos para utiizar la libreria ADODB
     *
     */
    include_once "$ruta_raiz/include/db/ConnectionHandler.php";
    $db = new ConnectionHandler("$ruta_raiz");
//$db->conn->debug = true;
    /*
     * Genreamos el encabezado que envia las variable a la paginas siguientes.
     * Por problemas en las sesiones enviamos el usuario.
     * @$encabezado  Incluye las variables que deben enviarse a la singuiente pagina.
     * @$linkPagina  Link en caso de recarga de esta pagina.
     */
    $encabezado = "" . session_name() . "=" . session_id() . "&krd=$krd&depeBuscada=$depeBuscada&filtroSelect=$filtroSelect&tpAnulacion=$tpAnulacion";

    /*  FILTRO DE DATOS
     *  @$setFiltroSelect  Contiene los valores digitados por el usuario separados por coma.
     *  @$filtroSelect Si SetfiltoSelect contiene algunvalor la siguiente rutina realiza el arreglo de la condici� para la consulta a la base de datos y lo almacena en whereFiltro.
     *  @$whereFiltro  Si filtroSelect trae valor la rutina del where para este filtro es almacenado aqui.
     *
     */
    if ($checkValue) {
        $num = count($checkValue);
        $i = 0;
        while ($i < $num) {
            $record_id = key($checkValue);
            $setFiltroSelect .= $record_id;
            $radicadosSel[] = $record_id;
            if ($i <= ($num - 2)) {
                $setFiltroSelect .= ",";
            }
            next($checkValue);
            $i++;
        }
        if ($radicadosSel) {
            $whereFiltro = " and b.radi_nume_radi in($setFiltroSelect)";
        }
    }
    if ($setFiltroSelect) {
        $filtroSelect = $setFiltroSelect;
    }
    ?>
    <body>
    <?php     $txSql = "";
    if ($chkNivel and $codusuario == 1) {
        $tomarNivel = "si";
    } else {
        $tomarNivel = "no";
    }

    include "$ruta_raiz/include/tx/Tx.php";
//$db->conn->debug=true;
    $rs = new Tx($db);
//echo "Radicados Informados";
    foreach ($radicadosSel as $rad) {
        $radicadosSelText .= $rad . ", ";
        $rutaImagen = substr($rad, 0, 4) . "/" . substr($rad, 4, 3) . "/" . $rad . ".tif";
        $linkImagenes .= "<a href='*SERVIDOR_IMAGEN*" . $rutaImagen . "'>$rad</a> - ";
    }
    include("$ruta_raiz/class_control/Param_admin.php");
    $param = Param_admin::getObject($db, '%', 'ALERT_FUNCTION');

    switch ($codTx) {
        case 7:
            $nombTx = "Borrar Informados";
            $observa = "($krd) $observa";
            $radicadosSel = $rs->borrarInformado($radicadosSel, $krd, $depsel8, $_SESSION['dependencia'], $_SESSION['codusuario'], $codusuario, $observa, $id_rol);
            $log->setOpera("Borrado de informado");
            $log->setNumDocu($radicadosSel);
            $log->registroEvento();
            break;
        case 8: {
                if (is_array($_POST['usCodSelect']))
                    while (list(, $var) = each($_POST['usCodSelect'])) {
                        $depsel8 = split('-', $var);
                        $usCodSelect = $depsel8[1];
                        $depsel8 = $depsel8[0];
                        $nombTx = "Informar Documentos";
                        $usCodDestino .= $rs->informar($radicadosSel, $krd, $depsel8, $dependencia, $usCodSelect, $codusuario, $observa, $id_rol, $_SESSION['usua_doc']) . ", ";
                        $log->setOpera("Documento informado a usuario $usCodSelect Dep. $depsel8");
                        $log->setNumDocu($radicadosSel);
                        $log->registroEvento();
                    }
                $usCodDestino = substr($usCodDestino, 0, strlen(trim($usCodDestino)) - 1);
            }
            break;
        case 9:

            $depsel = split('-', $usCodSelect);
            $usCodSelect = $depsel[1];
            $depsel = $depsel[0];
            if ($EnviaraV == "VoBo") {
                $codTx = 16;
                $carp_codi = 11;
                // MODIFICADO PARA GENERAR ALERTAS
                // JUNIO DE 2009

                if ($param->PARAM_VALOR == "1") { // COMPRUEBO SI LA FUNCION DE ALERTAS ESTA ACTIVA
                    foreach ($radicadosSel as $rad) {
                        $rs->registrarNovedad('NOV_VOBO', $usCodSelect, $rad);
                    }
                }
                //////////////////////////////
            } else {
                $codTx = 9;
                $carp_codi = 0;
            }
            $nombTx = "Reasignar Documentos ";
            $usCodDestino = $rs->reasignar($radicadosSel, $krd, $depsel, $dependencia, $usCodSelect, $codusuario, $tomarNivel, $observa, $codTx, $carp_codi, $id_rol, $_SESSION['usua_doc']);
            $log->setAction('document_assign');
            $log->setOpera("Documento Reasignado a Dep. $depsel usuario $usCodSelect");
            $log->setNumDocu($radicadosSel);
            $log->registroEvento();
	    //Modificacion para Informar documento a la hora de una reasignacion
 	    if(isset($usua_selinfo)){
		$conta=count($usua_selinfo);
		$observa="INFORMADO DESDE REASIGNAR ".$observa;
		for($i=0;$i<$conta;$i++){
		    list($dep2inf,$usua2inf)=explode("-",$usua_selinfo[$i]);
		    $rs->informar($radicadosSel, $krd, $dep2inf, $dependencia, $usua2inf, $codusuario, $observa, $id_rol, $_SESSION['usua_doc']);
                    $log->setOpera("Documento informado a usuario $usua2inf Dep. $dep2inf en operacion de reasignacion");
                    $log->setNumDocu($radicadosSel);
                    $log->registroEvento();
		}
	    }
            /* $nombTx = "Reasignar Documentos ";
              $depsel = split('-',$_GET['usCodSelect']);
              $usCodSelect = $depsel[1];
              $depsel = $depsel[0];
              $usCodDestino = $rs->reasignar( $radicadosSel, $krd,$depsel,$dependencia,$usCodSelect, $codusuario,$tomarNivel, $observa,$codTx,$carp_codi); */
            break;
        case 10:
            $nombTx = "Movimiento a Carpeta $carpetaNombre";
            $okTx = $rs->cambioCarpeta($radicadosSel, $codusuario, $dependencia, $id_rol, $usua_nomb, $carpetaCodigo, $_SESSION["codi_nivel"], $carpetaTipo, $tomarNivel, $observa);

            $log->setOpera("Documento cambio a carpeta $carpetaTipo");
            $log->setNumDocu($radicadosSel);
            $log->registroEvento();
            $depSel = $dependencia;
            $usCodSelect = $codusuario;
            $usCodDestino = $usua_nomb;
            break;
        case 12:
            $nombTx = "Devolucion de Documentos";
            $usCodDestino = $rs->devolver($radicadosSel, $krd, $dependencia, $codusuario, $tomarNivel, $observa, $id_rol, $_SESSION['usua_doc']);
            $arr = explode('<br>', $usCodDestino);
            for ($i = 0; $i < count($radicadosSel); $i++) {
                $patt = $arr[$i];
                $log->setAction('document_return');
                $log->setOpera("Documento devuelto a usuario $patt");
                $log->setNumDocu($radicadosSel[$i]);
                $log->registroEvento();
            }
            $depSel = $dependencia;
            $usCodSelect = $codusuario;
            $usCodDestino = $usua_nomb;
            break;
        case 13:
            $nombTx = "Archivo de Documentos";
            $txSql = $rs->archivar($radicadosSel, $krd, $dependencia, $codusuario, $observa, $id_rol);
            $log->setAction('document_filed');
            $log->setOpera("Documento archivado");
            $log->setNumDocu($radicadosSel);
            $log->registroEvento();
            break;
        case 14:

            $nombTx = "Agendar Documentos";
            $txSql = $rs->agendar($radicadosSel, $krd, $dependencia, $codusuario, $observa, $fechaAgenda, $id_rol);
            $log->setOpera("Documento agendado");
            $log->setNumDocu($radicadosSel);
            $log->registroEvento();
            break;

        case 15:
            $nombTx = "Sacar de 'Agendar Documentos'";
            $txSql = $rs->noAgendar($radicadosSel, $krd, $dependencia, $codusuario, $observa, $id_rol);
            $log->setOpera("Documento agendado retirado");
            $log->setNumDocu($radicadosSel);
            $log->registroEvento();
            break;
        case 16:
            $nombTx = "Radicados NRR";
            $txSql = $rs->nrr($radicadosSel, $krd, $dependencia, $codusuario, $observa, $id_rol);
            break;
    }
    if ($okTx == -1)
        $okTxDesc = " No ";
    /**  if($txSql and codTx != 12 and  $txSql and codTx != 13)
      {
      include "$ruta_raiz/include/tx/Historico.php";
      $hist = new Historico($db);
      $hist->insertarHistorico($radicadosSel,  $dependencia , $codusuario, $depsel, $usCodSelect, $observa, $codTx);
      } * */
    /**  IMPRESION DE RESULTADOS DE LA Transaccion
     */
    ?>
        <form action='enviardatos.php?PHPSESSID=172o16o0o154oJH&krd=JH' method=post name=formulario>
            <br>
            <table border=0 align='center' cellspace=2 cellpad=2 WIDTH=50%  class="t_bordeGris" id=tb_general align="left">
                <tr>
                    <td colspan="2" class="titulos4">ACCION REQUERIDA <?php echo  $accionCompletada ?>  <?php echo  $okTxDesc ?> COMPLETADA <?php echo  $causaAccion ?> </td>
                </tr>
                <tr>
                    <td align="right" bgcolor="#CCCCCC" height="25" class="titulos2">ACCION REQUERIDA :
                    </td>
                    <td  width="65%" height="25" class="listado2_no_identa">
<?php echo  $nombTx ?>
                    </td>
                </tr>
                <tr>
                    <td align="right" bgcolor="#CCCCCC" height="25" class="titulos2">RADICADOS INVOLUCRADOS :
                    </td>
                    <td  width="65%" height="25" class="listado2_no_identa"><?php for ($index = 0; $index < count($radicadosSel); $index++) {
    echo str_replace('*#*', ' ', $radicadosSel[$index]);
    echo "<br>";
} //join("<BR> ",$radicadosSel)
?>
                    </td>
                </tr>
                <tr>
                    <td align="right" bgcolor="#CCCCCC" height="25" class="titulos2">USUARIO DESTINO :
                    </td>
                    <td  width="65%" height="25" class="listado2_no_identa">
                        <?php echo  $usCodDestino ?>
                    </td>
                </tr>
                <tr>
                    <td align="right" bgcolor="#CCCCCC" height="25" class="titulos2">FECHA Y HORA :
                    </td>
                    <td  width="65%" height="25" class="listado2_no_identa">
                        <?php echo  date("m-d-Y  H:i:s") ?>
                    </td>
                </tr>
                <tr>
                    <td align="right" bgcolor="#CCCCCC" height="25" class="titulos2">USUARIO ORIGEN:
                    </td>
                    <td  width="65%" height="25" class="listado2_no_identa">
                        <?php echo  $usua_nomb ?>
                    </td>
                </tr>
                <tr>
                    <td align="right" bgcolor="#CCCCCC" height="25" class="titulos2">DEPENDENCIA ORIGEN:
                    </td>
                    <td  width="65%" height="25" class="listado2_no_identa">
                        <?php echo  $depe_nomb ?>
                    </td>
                </tr>
                <tr>
                    <td class="listado2_no_identa" colspan=2>
<?php //echo $_SESSION["enviarMailMovimientos"];
if ($_SESSION["enviarMailMovimientos"] == 1) {
    if ($codTx == 9 || $codTx == 8) {
        if (is_array($_POST['usCodSelect'])) {
            foreach ($_POST['usCodSelect'] as $value) {
                $depsel8 = split('-', $value);
                $usuaCodiMail = $depsel8[1];
                $depeCodiMail = $depsel8[0];
                include "../include/mail/mailInformar.php";
            }
        } else {
            $depsel8 = split('-', $_POST['usCodSelect']);
            $usuaCodiMail = $depsel8[1];
            $depeCodiMail = $depsel8[0];
            include "../include/mail/mailInformar.php";
        }
    }
}
?>
                    </td>
                </tr>

            </table>
        </form>
    </body>
</html>
