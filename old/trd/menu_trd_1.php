<?php  
session_start(); date_default_timezone_set('America/Bogota'); date_default_timezone_set('America/Bogota');
/**
 * Este programa despliega el men� principal de administraci�n
 * tablas de Retenci�n documental
 * @version     1.0
 */
error_reporting(7);

/*
 * Lista Subseries documentales
 * @autor Jairo Losada
 * @fecha 2009/06 Modificacion Variables Globales.
 */
foreach ($_GET as $key => $valor)  $$key = $valor;
foreach ($_POST as $key => $valor)  $$key = $valor;
$krd = $_SESSION["krd"];
$dependencia = $_SESSION["dependencia"];
$usua_doc = $_SESSION["usua_doc"];
$codusuario = $_SESSION["codusuario"];
$ruta_raiz = "..";
include($ruta_raiz.'/validadte.php');
require_once("$ruta_raiz/include/db/ConnectionHandler.php"); 
//Si no llega la dependencia recupera la sesi�n
$krd = $_SESSION["krd"];
$dependencia = $_SESSION["dependencia"];
$usua_doc = $_SESSION["usua_doc"];
$codusuario = $_SESSION["codusuario"];
$tpNumRad = $_SESSION["tpNumRad"];
$tpPerRad = $_SESSION["tpPerRad"];
$tpDescRad = $_SESSION["tpDescRad"];
$tip3Nombre = $_SESSION["tip3Nombre"];
$tip3img = $_SESSION["tip3img"];
$tpDepeRad = $_SESSION["tpDepeRad"];
$tip3desc = $_SESSION["tip3desc"];
$tip3img =$_SESSION["tip3img"];
$id_rol =$_SESSION["id_rol"];

include($ruta_raiz.'/validadte.php');
if (!$db)
		$db = new ConnectionHandler($ruta_raiz);

$phpsession = session_name()."=".session_id(); ?>

<html>
<head>
<link rel="stylesheet" href="../estilos/orfeo.css">
</head>
<body bgcolor="#FFFFFF" topmargin="0" onLoad="window_onload();">
<table width="47%" align="center" border="0" cellpadding="0" cellspacing="5" class="borde_tab">
     <tr> 
      <td height="25" class="titulos4"> 
        ADMINISTRACION  -TABLAS RETENCION DOCUMENTAL- 
      </td>
    </tr>
    <tr align="center"> 
      <td class="listado2" > 
        <a href='../trd/admin_series.php?<?php echo $phpsession ?>&krd=<?php echo $krd?>&krd=<?php echo $krd?>&<?php  echo "fechah=$fechah"; ?>' class="vinculos" target='mainFrame'>Series </a>
      </td>
    </tr>
    <tr align="center"> 
      <td class="listado2" > 
        <a href='../trd/admin_subseries.php?<?php echo $phpsession ?>&krd=<?php echo $krd?>&krd=<?php echo $krd?>&<?php  echo "fechah=$fechah"; ?>' class="vinculos" target='mainFrame'>Subseries </a>
      </td>
    </tr>
    <tr align="center"> 
      <td class="listado2" > 
        <a href='../trd/cuerpoMatriTRD.php?<?php echo $phpsession ?>&krd=<?php echo $krd?>&krd=<?php echo $krd?>&<?php  echo "fechah=$fechah"; ?>' class="vinculos" target='mainFrame'>Matriz 
          Relaci&oacute;n </a>
      </td>
    </tr>
    <tr align="center"> 
      <td class="listado2" >
	  <a href='../trd/admin_tipodoc.php?<?php echo $phpsession ?>&krd=<?php echo $krd?>&krd=<?php echo $krd?>&<?php  echo "fechah=$fechah"; ?>' class="vinculos" target='mainFrame'>Tipos 
          Documentales </a>
	  </td>
    </tr>
	<tr align="center"> 
      <td class="listado2" >
	  <a href='../trd/procModTrdArea.php?<?php echo $phpsession ?>&krd=<?php echo $krd?>&krd=<?php echo $krd?>&<?php  echo "fechah=$fechah"; ?>' class="vinculos" target='mainFrame'>Modificacion 
          TRD Area </a>
	  </td>
    </tr>
	<tr align="center"> 
      <td class="listado2" >
	  <a href='../trd/informe_trd.php?<?php echo $phpsession ?>&krd=<?php echo $krd?>&krd=<?php echo $krd?>&<?php  echo "fechah=$fechah"; ?>' class="vinculos" target='mainFrame'>Listado 
          Tablas de Retencion Documental </a>
	  </td>
	   </tr>  
    </tr>
  </table>
</body>
</html>
