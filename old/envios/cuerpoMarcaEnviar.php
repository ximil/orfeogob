<?php 
/* * ********************************************************************************** */
/* ORFEO GPL:Sistema de Gestion Documental		http://www.orfeogpl.org	     */
/* 	Idea Original de la SUPERINTENDENCIA DE SERVICIOS PUBLICOS DOMICILIARIOS     */
/* 				COLOMBIA TEL. (57) (1) 6913005  orfeogpl@gmail.com   */
/* ===========================                                                       */
/*                                                                                   */
/* Este programa es software libre. usted puede redistribuirlo y/o modificarlo       */
/* bajo los terminos de la licencia GNU General Public publicada por                 */
/* la "Free Software Foundation"; Licencia version 2. 			             */
/*                                                                                   */
/* Copyright (c) 2005 por :	  	  	                                     */
/* SSPS "Superintendencia de Servicios Publicos Domiciliarios"                       */
/*   Jairo Hernan Losada  jlosada@gmail.com                Desarrollador             */
/*   Sixto Angel Pinzón López --- angel.pinzon@gmail.com   Desarrollador             */
/* C.R.A.  "COMISION DE REGULACION DE AGUAS Y SANEAMIENTO AMBIENTAL"                 */
/*   Liliana Gomez        lgomezv@gmail.com                Desarrolladora            */
/*   Lucia Ojeda          lojedaster@gmail.com             Desarrolladora            */
/* D.N.P. "Departamento Nacional de Planeación"                                      */
/*   Hollman Ladino       hollmanlp@gmail.com                Desarrollador           */
/*                                                                                   */
/* Colocar desde esta lInea las Modificaciones Realizadas Luego de la Version 3.5    */
/*  Nombre Desarrollador   Correo     Fecha   Modificacion                           */
/* * ********************************************************************************** */
//Programa que genera el listado de todos los grupos de masiva generados por la dependencia, que no han sido enviados y da la opci�n de 
//generar el envio respectivo

session_start(); date_default_timezone_set('America/Bogota'); date_default_timezone_set('America/Bogota');
foreach ($_GET as $key => $valor)  $$key = $valor;
foreach ($_POST as $key => $valor)  $$key = $valor;

$krd = $_SESSION["krd"];
$dependencia = $_SESSION["dependencia"];
$usua_doc = $_SESSION["usua_doc"];
$codusuario = $_SESSION["codusuario"];
$tip3Nombre = $_SESSION["tip3Nombre"];
$tip3desc = $_SESSION["tip3desc"];
$tip3img = $_SESSION["tip3img"];
$usua_perm_impresion = $_SESSION["perm_impresion"];

if ($_GET["carpeta"])
    $carpeta = $_GET["carpeta"];
$tipo_carpt = $_GET["tipo_carpt"];
$adodb_next_page = $_GET["adodb_next_page"];
if ($_GET["gen_lisDefi"])     $gen_lisDefi = $_GET["gen_lisDefi"];
if ($_GET["dep_sel"])     $dep_sel = $_GET["dep_sel"];
if ($_GET["generar_listado"])     $generar_listado = $_GET["generar_listado"];
if ($_GET["cancelarAnular"])     $cancelarAnular = $_GET["cancelarAnular"];
if ($_GET["orderNo"])     $orderNo = $_GET["orderNo"];
if ($_GET["orderTipo"])     $orderTipo = $_GET["orderTipo"];
//if ($_GET["busqRadicados"])
    $busqRadicados = $_POST["busqRadicados"];
if ($_GET["estado_sal_max"])    $estado_sal_max = $_GET["estado_sal_max"];
if ($_GET["estado_sal"])    $estado_sal = $_GET["estado_sal"];
if ($_GET["Buscar"])    $Buscar = $_GET["Buscar"];
if ($_GET["busqRadicados"]) $busqRadicados=$_GET["busqRadicados"];
if ($_POST["busqRadicados"]) $busqRadicados=$_POST["busqRadicados"];

$ruta_raiz = "..";
include($ruta_raiz . '/validadte.php');
/*print_r($_GET);
print_r($_POST);*/
include_once "$ruta_raiz/class_control/usuario.php";
require_once("$ruta_raiz/include/db/ConnectionHandler.php");
include_once "$ruta_raiz/class_control/TipoDocumento.php";
include_once "$ruta_raiz/class_control/firmaRadicado.php";

$db = new ConnectionHandler($ruta_raiz);
$db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
//Se crea el objeto de analisis de firmas
if ($_SESSION ['usua_debug'] == 1)
    $db->conn->debug = true;
$objFirma = new FirmaRadicado($db);
$nombusuario = $_SESSION['usua_nomb'];
if (!$dep_sel)
    $dep_sel = $dependencia;

$sqlFechaHoy = $db->conn->DBDate($fecha_hoy);
?>

<html>
    <head>
        <meta http-equiv="Cache-Control" content="cache">
        <meta http-equiv="Pragma" content="public">
        <link rel="stylesheet" href="../estilos/table.css" type="text/css">
        <?php 
//variable con la fecha formateada
        $fechah = date("dmy") . "_" . time("h_m_s");
//variable con elementos de sesi�n
        $encabezado = session_name() . "=" . session_id() . "&krd=$krd";
        include_once "$ruta_raiz/js/funtionImage.php";
        ?>

        <script>

            pedientesFirma="";
            function back() {
                history.go(-1);
            }

            function recargar(){
                window.location.reload();	
            }

            function editFirmantes(rad){
                nombreventana="EdiFirms";
                url="<?php echo  $ruta_raiz ?>/firma/editarFirmates.php?radicado=" + rad +"&<?php echo  "&usua_nomb=$usua_nomb&&depe_nomb=$depe_nomb&usua_doc=$usua_doc&krd=$krd&" . session_name() . "=" . trim(session_id()) ?>&usua=<?php echo  $krd ?>";
                window.open(url,nombreventana,'height=500,width=750,scrollbars=yes,resizable=yes');
                return; 
            }

            function solicitarFirma () {
                marcados = 0;
                radicados = "";

                for(i=0;i<document.formEnviar.elements.length;i++){
                    if(document.formEnviar.elements[i].checked==1)	{
                        marcados++;
                        if (radicados.length > 0)
                            radicados = radicados + ",";
                        radicados = radicados +  (document.formEnviar.elements[i].value) ;
                    }
                }

                if(marcados>=1)	{
		
                    nombreventana="SolFirma";
                    url="<?php echo  $ruta_raiz ?>/firma/seleccFirmantes.php?codigo=&<?php echo  "&usua_nomb=$usua_nomb&&depe_nomb=$depe_nomb&usua_doc=$usua_doc&krd=$krd&" . session_name() . "=" . trim(session_id()) ?>&usua=<?php echo  $krd ?>&radicados="+radicados;
                    window.open(url,nombreventana,'height=550,width=1000,scrollbars=yes,resizable=yes');
                    return; 
	  
                }else{
                    alert("Debe seleccionar un radicado");
                }	

            }

            function valPendsFirma (){
	
                for(i=0;i<document.formEnviar.elements.length;i++){
                    if(document.formEnviar.elements[i].checked==1)	{
                        if (pedientesFirma.indexOf(document.formEnviar.elements[i].value)!=-1){
                            alert ("No se puede enviar el radicado " + document.formEnviar.elements[i].value + " pues se encuentra pendiente de firma ");
                            return false;
                        }
				
                    }
                }
	
                return true;
            }

            function continuar(){
                accion = '<?php echo  $pagina_sig ?>?<?php echo  session_name() . "=" . session_id() . "&krd=$krd&fechah=$fechah&dep_sel=$dep_sel&estado_sal=$estado_sal&estado_sal_max=$estado_sal_max" ?>';
                alert (accion);
            }

        </script>
        <?php         error_reporting(7);
        ?> 
        <link rel="stylesheet" href="../estilos/orfeo.css">
        <?php 
        $calendario = 'no';
        if (!$carpeta)
            $carpeta = 0;

        if (!$estado_sal) {
            $estado_sal = 2;
        }

        if (!$estado_sal_max)
            $estado_sal_max = 3;

        if ($estado_sal == 2) {
            $accion_sal = "Marcar Documentos Como Impresos";
            $nomcarpeta = "Documentos Para Impresion";

            $pagina_sig = "cuerpoMarcaEnviar.php";
            /*if ($usua_perm_impresion == 2) {
                $swBusqDep = "S";
            }*/
            $dependencia_busq1 = " and c.radi_depe_radi = $dep_sel ";
            $dependencia_busq2 = " and c.radi_depe_radi = $dep_sel";
        }

        //variable que indica la acci�n a ejecutar en el formulario
        $accion_sal = "Marcar Documentos como Impresos";
        //variable que indica la acci�n a ejecutar en el formulario
        $nomcarpeta = "Marcar Documentos como Impresos";
        $carpeta = "nada";
        $pagina_sig = "../envios/marcaEnviar.php";
        $pagina_actual = "../envios/cuerpoMarcaEnviar.php";
        $varBuscada = "radi_nume_salida";
        $swListar = "si";


        if ($orden_cambio == 1) {
            if (!$orderTipo) {
                $orderTipo = " DESC";
            } else {
                $orderTipo = "";
            }
        }

//var de formato para la tabla
        $tbbordes = "#CEDFC6";
//var de formato para la tabla
        $tbfondo = "#FFFFCC";

//le pone valor a la variable que maneja el criterio de ordenamiento inicial
        if (!$orno) {
            $orno = 1;
            $ascdesc = $orderTipo;
        }

        $imagen = "flechadesc.gif";
        ?> 
        <script>
            <!-- Esta funcion esconde el combo de las dependencia e inforados Se activan cuando el menu envie una se�al de cambio.-->
 
            function window_onload()
		
            {
   
                form1.depsel.style.display = '';
                form1.enviara.style.display = '';
                form1.depsel8.style.display = 'none';
                form1.carpper.style.display = 'none';
                setVariables();
                setupDescriptions();
            }



            function markAll()
            {
                if(form1.marcartodos.checked==1)
                    for(i=4;i<form1.elements.length;i++)
                        form1.elements[i].checked=1;
                else
                    for(i=4;i<form1.elements.length;i++)
                        form1.elements[i].checked=0;
            }
<?php 
//include "libjs.php";
function tohtml($strValue) {
    return htmlspecialchars($strValue);
}
?>

        </script>
        <script>
            function cambioDependecia (dep){
                document.formDep.action="cuerpo_masiva.php?krd=<?php echo  $krd ?>&dep_sel="+dep;
                //alert(document.formDep.action);
                document.formDep.submit();
            }

            function marcar()
            {
                marcados = 0;

                for(i=0;i<document.formEnviar.elements.length;i++)
                {
                    if(document.formEnviar.elements[i].checked==1)
                    {
                        marcados++;
                    }
                }
                if(marcados>=1)
                {
		
                    if (valPendsFirma())
                        document.formEnviar.submit();
                }
                else
                {
                    alert("Debe seleccionar un radicado");
                }
            }

        </script>
        <style type="text/css">
            <!--
            .textoOpcion {  font-family: Arial, Helvetica, sans-serif; font-size: 8pt; color: #000000; text-decoration: underline}
            FORM {
clear: none; 
float: none; 
border-style: none; 
margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px
}
            -->
        </style>
    </head>

    <body  topmargin="0" >
        <!--<div id="object1" style="position:absolute; visibility:show; left:10px; top:-50px; width=80%; z-index:2" > 
            <p>Cuadro de Historico</p>
        </div>-->
        <?php         $sqlFecha = $db->conn->SQLDate("Y/m/d", "r.SGD_RENV_FECH");
        $img1 = "";
        $img2 = "";
        $img3 = "";
        $img4 = "";
        $img5 = "";
        $img6 = "";
        $img7 = "";
        $img8 = "";
        $img9 = "";
        IF ($ordcambio) {
            IF ($ascdesc == "") {
                $ascdesc = "DESC";
                $imagen = "flechadesc.gif";
            } else {
                $ascdesc = "";
                $imagen = "flechaasc.gif";
            }
        } else
        if ($ascdesc == "DESC")
            $imagen = "flechadesc.gif";
        else
            $imagen="flechaasc.gif";

        //		$estado_salX=$estado_sal;
        //print_r($_POST);
        //if(strlen( $_POST['busqRadicados'])==14){
        if ($busqRadicados) {
            $estado_salX = "3 or a.ANEX_ESTADO=2";
        } else {
            if ($_GET['imp'] == 3) {
                $estado_salX = 3;
                $imgImp1 = "<img src='../imagenes/iconos/flechanoleidos.gif' border=0 alt='$data'>";
                $impX = '&imp=3';
            } else {
                $estado_salX = 2;
                $imgImp2 = "<img src='../imagenes/iconos/flechanoleidos.gif' border=0 alt='$data'>";
                $impX = '&imp=2';
            }
        }

        if ($orno == 1) {
            $order = " a.radi_nume_salida  $ascdesc";
            $img1 = "<img src='../imagenes/iconos/$imagen' border=0 alt='$data'>";
        }
        if ($orno == 2) {
            $order = " 6  $ascdesc";
            $img2 = "<img src='../imagenes/iconos/$imagen' border=0 alt='$data'>";
        }
        if ($orno == 3) {
            $order = " a.anex_radi_nume $ascdesc";
            $img3 = "<img src='../imagenes/iconos/$imagen' border=0 alt='$data'>";
        }
        If ($orno == 4) {
            $order = " c.radi_fech_radi  $ascdesc";
            $img4 = "<img src='../imagenes/iconos/$imagen' border=0 alt='$data'>";
        }
        If ($orno == 5) {
            $order = " a.anex_desc  $ascdesc";
            $img5 = "<img src='../imagenes/iconos/$imagen' border=0 alt='$data'>";
        }
        If ($orno == 6) {
            $order = " a.sgd_fech_impres  $ascdesc";
            $img6 = "<img src='../imagenes/iconos/$imagen' border=0 alt='$data'>";
        }
        If ($orno == 7) {
            $order = " a.anex_creador $ascdesc";
            $img7 = "<img src='../imagenes/iconos/$imagen' border=0 alt='$data'>";
        }
        If ($orno == 8) {
            $order = " a.anex_creador $ascdesc";
            $img7 = "<img src='../imagenes/iconos/$imagen' border=0 alt='$data'>";
        }


        $encabezado = session_name() . "=" . session_id() . "&dep_sel=$dep_sel&krd=$krd&estado_sal=$estado_sal&fechah=$fechah&estado_sal_max=$estado_sal_max&ascdesc=$ascdesc$impX&orno=";
        $fechah = date("dmy") . "_" . time("h_m_s");
        $check = 1;
        $fechaf = date("dmy") . "_" . time("hms");
        $row = array();
        ?>        
        <table border=0 width='100%' class='t_bordeGris' align='center'>
            <tr >
                <td height="20" > 
                    <TABLE width="100%" align="center" cellspacing="0" cellpadding="0" >
                        <tr> 
                            <td height="73"> 
<?php 
if ($usua_perm_impresion == 2) {
                $swBusqDep = "S";
            }
include "../envios/paEncabeza.php";
include "../envios/paBuscar.php";
// include "../envios/paOpciones.php";   
/*
 * GENERAR LISTADO ENTREGA FISICOSclass="borde_tab"
 */
$listarP="<b>Listar Por </b>$imgImp1 <a href='$pagina_actual?$encabezado1&ordcambio=1&imp=3&fechaI=$fechaIniHD&fechaF=$fechaFinal' alt='Ordenar Por Leidos' >
            <span class='leidos'>Impresos</span></a> $imgImp2 <a href='$pagina_actual?$encabezado1&ordcambio=1&imp=2&fechaI=$fechaIniHD&fechaF=$fechaFinal'  alt='Ordenar Por Leidos'><span class='no_leidos'>
            Por Imprimir</span></a>";
$botonM="<a href='$pagina_sig?$encabezado'></a><input type=submit value='$accion_sal' name=Enviar id=Enviar valign='middle' class='botones_largo' onclick='marcar();'>";
?>
                               <!-- <table BORDER=0  cellpad=2 cellspacing='0'   width="100%" >
                                    <tr>
                                        <td idth='50%' align='left' height="40" class="titulos2" >
                                            <b>Listar Por </b> <?php echo  $imgImp1 ?>
                                            <a href='<?php echo  $pagina_actual ?>?<?php echo  $encabezado ?>1&ordcambio=1&imp=3&fechaI=<?php echo  $fechaIniHD ?>&fechaF=<?php echo  $fechaFinal ?>' alt='Ordenar Por Leidos' >
                                                <span class='leidos'>Impresos</span></a>
                    <?php echo  $imgImp2 ?> <a href='<?php echo  $pagina_actual ?>?<?php echo  $encabezado ?>1&ordcambio=1&imp=2&fechaI=<?php echo  $fechaIniHD ?>&fechaF=<?php echo  $fechaFinal ?>'  alt='Ordenar Por Leidos'><span class='no_leidos'>
	Por Imprimir</span></a>

                                        </td>
                                        <td class="titulos2" align="center">

                                            <a href='<?php echo  $pagina_sig ?>?<?php echo  $encabezado ?> '></a>
                                            <input type=submit value="<?php echo  $accion_sal ?>" name=Enviar id=Enviar valign='middle' class='botones_largo' onclick="marcar();">

                                        </td>

                                    </tr>
                                </table>-->
 <?php 
$accion_sal2 = "Generar Listado de Entrega";
include "../envios/paListado.php";
?>                           </td>

                        </tr>
                    </table>
<?php 
/*  GENERACION LISTADO DE RADICADOS
 *  Aqui utilizamos la clase adodb para generar el listado de los radicados
 *  Esta clase cuenta con una adaptacion a las clases utiilzadas de orfeo.
 *  el archivo original es adodb-pager.inc.php la modificada es adodb-paginacion.inc.php
 */
/*if (strlen($busqRadicados)) {
    $dateSQL = '';
} else {
 /*   $dateSQL = "and c.radi_fech_radi between to_date('$fechaIniHD, 12:00:00 AM','DD/MM/YYYY, HH:MI:SS AM') 
and to_date('$fechaFinal, 11:59:59 PM','DD/MM/YYYY, HH:MI:SS AM')";*/
  /* $dateSQL = "and c.radi_fech_radi between to_date('$fechaIniHD','DD/MM/YYYY') 
and to_date('$fechaFinal','DD/MM/YYYY')";
}*/
include "$ruta_raiz/include/query/envios/queryCuerpoMarcaEnviar.php";
//echo $isql;
$db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
if ($_SESSION ['usua_debug'] == 2)
    $db->conn->debug = true;
//echo  $isql;
$rs = $db->query($isql);

if ($usua_perm_firma == 2 || $usua_perm_firma == 3) {
    ?>
                        <table cellpad=2 cellspacing='0' WIDTH=100% class='borde_tab' valign='top' align='center' >
                            <tr  class="titulos2" > 
                                <td align='left' height="17"  > <span class='etextomenu'> 
                                    </span> </td>
                                <td width='10%' align="left" height="17"> 
                                    <input type=button value='Solicitar Firma' name=solicfirma valign='middle' class='botones' onclick="solicitarFirma();" >
                                </td>
                            </tr>
                        </table>

                                            <?php 
                                        } //
                                        ?>

                </td>
            </tr>
        </table>			
        <form name='formEnviar'  method=post onsubmit=" return alert ('12345')" action=<?php echo  $pagina_sig ?>?<?php echo  session_name() . "=" . session_id() . "&krd=$krd&fechah=$fechah&dep_sel=$dep_sel&estado_sal=$estado_sal&estado_sal_max=$estado_sal_max" ?> >
            
            <TABLE width="98%" align="center" cellspacing="0" cellpadding="0" class='borde_tab'>
                <tr> 
                    <td class="grisCCCCCC"> 
                        <div style=" padding : 4px; width :100%; height : 400px; overflow : auto;" >
                        <table width="100%"  border="0"  cellpadding="0" cellspacing="1">
                               <tr class='titulos3'  > 
                                <th  width='130px'  > <img src='<?php echo  $ruta_raiz ?>/imagenes/estadoDoc.gif' width="130px"  border=0 > 
                                </th>
                                <th  align="center" >
                                    <a href='<?php echo  $PHP_SELF . "?" . $encabezado ?>1&ordcambio=1&orno=1' class='textoOpcion' alt='Ordenamiento'>
                                        <?php echo  $img1 ?>
                  		Radicado Salida 
                                    </a>
                                </th>
                                <th  width='25px' align="center">
                                    <a href='<?php echo  $PHP_SELF . "?" . $encabezado ?>1&ordcambio=1&orno=2' class='textoOpcion' alt='Ordenamiento'>
                            <?php echo  $img2 ?>
                  		Copia 
                                    </a>
                                </th>
                                <th  align="center"> 
                                    <a href='<?php echo  $PHP_SELF . "?" . $encabezado ?>1&ordcambio=1&orno=3' class='textoOpcion' alt='Ordenamiento'>
                            <?php echo  $img3 ?>
                      	Radicado Padre
                                    </a>
                                </th>
                                <td  width='149px' align="center"> 
                                    <a href='<?php echo  $PHP_SELF . "?" . $encabezado ?>1&ordcambio=1&orno=4' class='textoOpcion' alt='Ordenamiento'>
                            <?php echo  $img4 ?>
                  		Fecha Radicado
                                    </a>
                                </td>
                                            <td  width='300px' align="center"> <a href='<?php echo  $PHP_SELF . "?" . $encabezado ?>1&ordcambio=1&orno=5' class='textoOpcion' alt='Ordenamiento'> 
                            <?php echo  $img5 ?>
                                        Descripcion </a> </td>
                               <td align='center'   > 
                                
                            <?php echo  $img6 ?>
                                    </a> Fecha Impresi&oacute;n </td>
                                <td   align="center" width='90px'> <a href='<?php echo  $PHP_SELF . "?" . $encabezado ?>1&ordcambio=1&orno=7' class='textoOpcion' alt='Ordenamiento'>	
                            <?php echo  $img7 ?>
                                        Generado Por </a> </td>
                            <td  align="center"> <a href='<?php echo  $PHP_SELF . "?" . $encabezado ?>1&ordcambio=1&orno=6' class='textoOpcion' alt='Ordenamiento'>	
                            </tr>
                            <?php 
                            $i = 1;
                            $ki = 0;
                            $pagina=$_GET["pagina"];
                            if (!$pagina)    $pagina = 0;
                            $registro = $pagina * 20;
                            while ($rs && !$rs->EOF) {

                                if ($ki >= $registro and $ki < ($registro + 20)) {

                                    $swEsperaFirma = false;
                                    $estado = $rs->fields['CHU_ESTADO'];
                                    $copia = $rs->fields['COPIA'];
                                    $tipo = $rs->fields['HID_SGD_DIR_TIPO'];
                                    $documentos = $rs->fields['DOCUMENTOS'];
                                    $rad_salida = $rs->fields['IMG_RADICADO_SALIDA'];
                                    $rad_padre = $rs->fields['RADICADO_PADRE'];
                                    $cod_dev = $rs->fields['HID_DEVE_CODIGO'];
                                    $fech_radicado = $rs->fields['FECHA_RADICADO'];
                                    $descripcion = $rs->fields['DESCRIPCION'];
                                    $fecha_impre = $rs->fields['FECHA_IMPRESION'];
                                    $fecha_dev = $rs->fields['HID_SGD_DEVE_FECH'];
                                    $generadoPor = $rs->fields['GENERADO_POR'];
                                    $path_imagen = $rs->fields['HID_RADI_PATH'];

                                    //***********************************************
                                    $edoDev = 0;

                                    if ($cod_dev == 0 OR $cod_dev == NULL) {
                                        $edoDev = 97;
                                    } else {
                                        if ($cod_dev > 0)
                                            $edoDev = 98;
                                    }
                                    if ($cod_dev == 99)
                                        $edoDev = 99;

                                    switch ($edoDev) {
                                        case 99:
                                            $imgEstado = "<img src='$ruta_raiz/imagenes/docDevuelto_tiempo.gif'  border=0 alt='Fecha Devolucionyy :$fecha_dev' title='Fecha Devolucion :$fecha_dev'>";
                                            break;
                                        case 98:
                                            $imgEstado = "<img src='$ruta_raiz/imagenes/docDevuelto.gif'  border=0 alt='Fecha Devolucion :$fecha_dev' title='Fecha Devolucion :$fecha_dev'>";
                                            break;
                                        case 97:
                                            $fecha_dev = $rs->fields["HID_SGD_DEVE_FECH"];
                                            if ($rs->fields["HID_DEVE_CODIGO1"] == 99) {
                                                $imgEstado = "<img src='$ruta_raiz/imagenes/docDevuelto_tiempo.gif'  border=0 alt='Fecha Devolucionx :$fecha_dev' title='Devolucion por Tiempo de Espera'>";
                                                $noCheckjDevolucion = "enable";
                                                break;
                                            }
                                            if ($rs->fields["HID_DEVE_CODIGO"] >= 1 and $rs->fields["HID_DEVE_CODIGO"] <= 98) {
                                                $imgEstado = "<img src='$ruta_raiz/imagenes/docDevuelto.gif'  border=0 alt='Fecha Devolucionn :$fecha_dev' title='Fecha Devolucion :$fecha_dev'>";
                                                $noCheckjDevolucion = "disable";
                                                break;
                                            }
                                            switch ($estado) {
                                                case 2:
                                                    $estadoFirma = $objFirma->firmaCompleta($rad_salida);
                                                    if ($estadoFirma == "NO_SOLICITADA")
                                                        $imgEstado = "<img src=$ruta_raiz/imagenes/docRadicado.gif  border=0>";
                                                    else if ($estadoFirma == "COMPLETA") {
                                                        $imgEstado = "<a  href='javascript:editFirmantes($rad_salida)' > <img src=$ruta_raiz/imagenes/docFirmado.gif  border=0></a>";
                                                    } else if ($estadoFirma == "INCOMPLETA") {
                                                        $imgEstado = "<a  href='javascript:editFirmantes($rad_salida)' >
								  	<img src=$ruta_raiz/imagenes/docEsperaFirma.gif border=0>
								  </a>";
                                                        $swEsperaFirma = true;
                                                    }
                                                    break;
                                                case 3:
                                                    $imgEstado = "<img src=$ruta_raiz/imagenes/docImpreso.gif  border=0>";
                                                    break;
                                                case 4:
                                                    $imgEstado = "<img src=$ruta_raiz/imagenes/docEnviado.gif  border=0>";
                                                    break;
                                            }
                                            break;
                                    }

                                    //************************************************


                                    if ($data == "")
                                        $data = "NULL";
                                    error_reporting(7);

                                    if ($i == 1) {
                                        $formato = "listado2";

                                        $i = 2;
                                    } else {
                                        $formato = "listado1";

                                        $i = 1;
                                    }
                                    ?>
                                    <tr class='<?php echo  $formato ?>'> 
                                        <td class='<?php echo  $leido ?>' align="center" width='130px' > 
                                            <?php echo  $imgEstado ?>
                                        </td>
                                        <td class='<?php echo  $leido ?>'  > 
                                            <?php 
                                            /*
                                             * @author Liliana Gomez Velasquez
                                             * @fecha 14-09-2009
                                             * Se incluye modificacion manejo de validacion permisos 
                                             * visibilidad documento
                                             */
                                            include_once "$ruta_raiz/tx/verLinkArchivo.php";
                                            $verLinkArch = new verLinkArchivo($db);
                                            $resulVal = $verLinkArch->valPermisoRadi($rad_salida);
                                            $verImg = $resulVal['verImg'];
                                            $radicado_path = $resulVal['pathImagen'];
                                            if ($verImg == "SI") {
                                                echo "<a class=\"vinculos\" href=\"#2\" onclick=\"funlinkArchivo('$rad_salida','$ruta_raiz');\">$rad_salida</a>";
                                            } elseif ($verImg == "NO") {
                                                echo "<a href='#' onclick=\"alert('El documento posee seguridad y no posee los suficientes permisos'); return false;\"><span class=leidos>$rad_salida</span></a>";
                                            }
                                            ?>
                                        </td>
                                        <td class='<?php echo  $leido ?>' width='25px'> <span class='<?php echo $leido?>'> 
                                    <?php echo  $copia ?>
                                            </span> </td>
                                        <td class='<?php echo  $leido ?>' > 
                                    <?php echo  $rad_padre ?>
                                        </td>
                                        <td  class='<?php echo  $leido ?>' width='130px'> 
                                    <?php echo  $fech_radicado ?>
                                        </td>
                                        <td class='<?php echo  $leido ?>' width='300px' > 
        <?php echo  $descripcion ?>
                                        </td>
                                        <td class='<?php echo  $leido ?>' width='130px'> &nbsp; 
        <?php echo  $fecha_impre; ?>
                                        </td>
                                        <td class='<?php echo  $leido ?>' width='100px' > 
                            <?php echo  $generadoPor ?>
                                        </td>
                                        
                                        <td align='center'  class='<?php echo  $leido ?>' > 
                            <?php  if ($swEsperaFirma) { ?>
                                                <script>
                                                    pedientesFirma = pedientesFirma + <?php echo  $rad_salida ?> + "," ;
                                                </script>
                            <?php  } ?>
                                            <input type=checkbox name='checkValue[<?php  echo $rad_salida . '-' . $tipo; ?>]' value='<?php  echo $rad_salida . '-' . $tipo; ?>'  />
                                        </td>
                                    </tr>
                            <?php 
                        }
                        $ki = $ki + 1;
                        $rs->MoveNext();
                    }
                    ?>            

                        </table>
                        </div>
                    </TD>
                </tr>
            </TABLE>
        
        <table border=0 cellspace=2 cellpad=2 WIDTH=98% class='t_bordeGris' align='center'>
            <tr align="center"> 
                <td> <?php 
                    $numerot = $ki;

                    // Se calcula el numero de | a mostrar
                     $paginas = ($numerot / 20);
                    
                    ?><span class='leidos'> Paginas</span> <?php 
                    if (intval($paginas) < $paginas) {
                    	
                        $paginas = intval($paginas)+1;
                    } else {
                        $paginas = $paginas ;
                    }
                    // Se imprime el numero de Paginas.
                    
                    for ($ii2 = 0; $ii2 < $paginas; $ii2++) {
                    	
                        if ($pagina == $ii2) {
                            $letrapg = "<font color=green size=3>";
                        } else {
                            $letrapg = "<font color=blue size=2>";
                        }
                        echo " <a  class=paginacion  href='$PHP_SELF?dep_sel=$dep_sel&pagina=$ii2&$encabezado&orno=$orno&fechaI=$fechaIniHD&fechaF=$fechaFinal&busqRadicados=$busqRadicados&imp=".$_GET['imp']."'>$letrapg" . ($ii2 + 1) . "</font></a>\n";
                        
                    }
                    ?> </td>
            </tr></table>
            </form>
    </td></tr></table>
<?php /*$time_end = microtime_float();
$time = $time_end - $time_start;
echo "<font size='1'><b> Tiempo : " . $time . "</b></font>";*/
?>
</body>
</html>
