<?php //funtValidar.php
function ValidateCambios($db, $vectorX, $sgd_dir_tipo, $radi, $codi = 0) {
	$Cambios = '';
	$Consulta = "select SGD_TRD_CODIGO, SGD_DIR_NOMREMDES, SGD_DIR_DOC, MUNI_CODI, DPTO_CODI,
			id_pais, id_cont, SGD_DOC_FUN, SGD_OEM_CODIGO, SGD_CIU_CODIGO, SGD_ESP_CODI, RADI_NUME_RADI, SGD_SEC_CODIGO,
			SGD_DIR_DIRECCION, SGD_DIR_TELEFONO, SGD_DIR_MAIL, SGD_DIR_TIPO, SGD_DIR_CODIGO, SGD_ANEX_CODIGO, SGD_DIR_NOMBRE from SGD_DIR_DRECCIONES where RADI_NUME_RADI= $radi and SGD_DIR_TIPO=$sgd_dir_tipo";
	$go='si';
	$rs = $db->query ( $Consulta );
	if (! $rs->EOF) {
		$tipotrd = array (0 => 'Usuario', 1 => 'ESP', 2 => 'OTRAS EMPRESAS', 6 => 'FUNCIONARIOS' );
		//echo $vectorX['SGD_TRD_CODIGO'] .'!='. $rs->fields['SGD_TRD_CODIGO'];
		if ($vectorX ['SGD_TRD_CODIGO'] != $rs->fields ['SGD_TRD_CODIGO'] && $vectorX ['SGD_TRD_CODIGO'] != '' && $rs->fields ['SGD_TRD_CODIGO'] != '') {
			
			$Cambios .= "- tipo " . $tipotrd [$rs->fields ['SGD_TRD_CODIGO']] . " ";
		}
		if ($vectorX ['SGD_DIR_NOMREMDES'] != $rs->fields ['SGD_DIR_NOMREMDES'] && $vectorX ['SGD_DIR_NOMREMDES'] != '' && $rs->fields ['SGD_DIR_NOMREMDES'] != '') {
			$Cambios .= " - Nombre destinatario :" . $rs->fields ['SGD_DIR_NOMREMDES'] . " ;";
		}
		if ($vectorX ['MUNI_CODI'] != $rs->fields ['MUNI_CODI'] || $vectorX ['DPTO_CODI'] != $rs->fields ['DPTO_CODI'] || $vectorX ['ID_PAIS'] != $rs->fields ['ID_PAIS'] || $vectorX ['ID_CONT'] != $rs->fields ['ID_CONT']) {
			
			$sqlubicacion = "select m.muni_nomb, d.dpto_nomb, p.nombre_pais, c.nombre_cont 
   							, m.muni_codi, d.dpto_codi, p.id_pais, c.id_cont
				from municipio  m, departamento d, SGD_DEF_PAISES p, sgd_def_continentes c 
				where m.dpto_codi=d.dpto_codi and m.id_pais= p.id_pais and m.id_cont= c.id_cont 
				and (
				(m.muni_codi=1 and m.dpto_codi=11 and m.id_pais=170  and m.id_cont=1)
				or
				(m.muni_codi=1 and m.dpto_codi=8 and m.id_pais=170  and m.id_cont=1)
				)";
			$rsmubi = $db->query ( $sqlubicacion );
			
			while ( ! $rsmubi->EOF ) {
				
				if ($vectorX ['MUNI_CODI'] == $rsmubi->fields ['MUNI_CODI'] || $vectorX ['DPTO_CODI'] == $rsmubi->fields ['DPTO_CODI'] || $vectorX ['ID_PAIS'] == $rsmubi->fields ['ID_PAIS'] || $vectorX ['ID_CONT'] == $rsmubi->fields ['ID_CONT']) {
					$mubiM2 = $rsmubi->fields ['MUNI_NOMB'];
					$mubiD2 = $rsmubi->fields ['DPTO_NOMB'];
					$mubiP2 = $rsmubi->fields ['NOMBRE_PAIS'];
					$mubiC2 = $rsmubi->fields ['NOMBRE_CONT'];
				}
				if ($rs->fields ['MUNI_CODI'] == $rsmubi->fields ['MUNI_CODI'] || $rs->fields ['DPTO_CODI'] == $rsmubi->fields ['DPTO_CODI'] || $rs->fields ['ID_PAIS'] == $rsmubi->fields ['ID_PAIS'] || $rs->fields ['ID_CONT'] == $rsmubi->fields ['ID_CONT']) {
					$mubiM1 = $rsmubi->fields ['MUNI_NOMB'];
					$mubiD1 = $rsmubi->fields ['DPTO_NOMB'];
					$mubiP1 = $rsmubi->fields ['NOMBRE_PAIS'];
					$mubiC1 = $rsmubi->fields ['NOMBRE_CONT'];
				}
				$rsmubi->MoveNext ();
			}
			
			$Cambios .= " - Municipio : " . $mubiM1 . " ";
			if ($vectorX ['DPTO_CODI'] != $rs->fields ['DPTO_CODI'])
				$Cambios .= " - Departamento : " . $mubiD1 . "  ;";
			if ($vectorX ['ID_PAIS'] != $rs->fields ['ID_PAIS'])
				$Cambios .= " - Pais : " . $mubiP1 . "  ;";
			if ($vectorX ['ID_CONT'] != $rs->fields ['ID_CONT'])
				$Cambios .= " - Continente : " . $mubiC1 . "  ;";
		}
		
		if ($vectorX ['SGD_DOC_FUN'] != $rs->fields ['SGD_DOC_FUN'] && $rs->fields ['SGD_DOC_FUN'] != '' && $vectorX ['SGD_DOC_FUN'] != 0) {
			$Cambios .= " - Codigo doc Fun :" . $rs->fields ['SGD_DOC_FUN'] . " ;";
		}
		if ($vectorX ['SGD_OEM_CODIGO'] != $rs->fields ['SGD_OEM_CODIGO'] && $rs->fields ['SGD_OEM_CODIGO'] != '' && $vectorX ['SGD_OEM_CODIGO'] != 0) {
			$Cambios .= " - codigo OEM :" . $rs->fields ['SGD_OEM_CODIGO'] . " ;";
		}
		if ($vectorX ['SGD_SEC_CODIGO'] != $rs->fields ['SGD_SEC_CODIGO']) {
			$Cambios .= " - Codigo Sec :" . $rs->fields ['SGD_SEC_CODIGO'] . " ;";
		}
		
		$dire = trim ( $rs->fields ['SGD_DIR_DIRECCION'] );
		$dire2 = trim ( $vectorX ['SGD_DIR_DIRECCION'] );
		if ($dire2 != $dire && $dire2) {
			$Cambios .= " - Direccion :" . $rs->fields ['SGD_DIR_DIRECCION'] . " ;";
		}
		
		$tel = trim ( $rs->fields ['SGD_DIR_TELEFONO'] );
		$tel2 = trim ( $vectorX ['SGD_DIR_TELEFONO'] );
		if ($tel2 != $tel && $sgd_dir_tipo != 3) {
			$Cambios .= " - Telefono :" . $rs->fields ['SGD_DIR_TELEFONO'] . " ;";
		}
		$mail = $rs->fields ['SGD_DIR_MAIL'];
		if (trim ( $vectorX ['SGD_DIR_MAIL'] ) != trim ( $mail )) {
			
			$Cambios .= " - Mail :" . $rs->fields ['SGD_DIR_MAIL'] . " ;";
		}
		
		if (trim ( $vectorX ['SGD_DIR_NOMBRE'] ) != trim ( $rs->fields ['SGD_DIR_NOMBRE'] ) && strlen ( $vectorX ['SGD_DIR_NOMBRE'] ) != 0) {
			$Cambios .= " - Nombre :" . $rs->fields ['SGD_DIR_NOMBRE'] . " ;";
		}
		
		if ($Cambios != '') {
			if ($sgd_dir_tipo == 1)
				$Cambios = "  Remitente\Destinatario - " . $Cambios;
			if ($sgd_dir_tipo == 2)
				$Cambios = "  Predio - " . $Cambios;
		
		}
		$go='no';
	}
			if ($sgd_dir_tipo == 2 &&  strlen (trim($vectorX ['SGD_DIR_NOMREMDES']))!= 0 && $go=='si' )
				$Cambios = "Predio - Se agrega datos del predio";
	
	return $Cambios;
}

function ActualizarAnexo($db, $record, $recordOtros) {
	if ($_SESSION ['usua_debug'] == 1) {
		echo "<span style='color:GREEN;'>";
		echo "<hr>Ingreso Function ActualizarAnexo <hr>";
		$db->conn->debug = true;
	}
	$nurad = $record ['RADI_NUME_RADI'];
	//$isql = "select distinct sgd_anex_codigo from sgd_dir_drecciones  where radi_nume_radi=$nurad and sgd_anex_codigo is not null order by sgd_anex_codigo asc";
	$isql = " select anex_radi_nume,anex_codigo,radi_nume_salida from anexos where radi_nume_salida=$nurad and substr(anex_codigo,0,14) !='$nurad' ";
	//$isql = "select distinct anex_radi_nume from anexos where radi_nume_salida=$nurad";
	$resultado = $db->query ( $isql );
	$Anex_Nume = $resultado->fields ['ANEX_RADI_NUME'];
	$Anex_Codi = $resultado->fields ['ANEX_CODIGO'];
	//echo $Anex_RADI = $resultado->fields ['RADI_NUME_RADI'];
	if (! $Anex_Nume) {
	    $sql2 = "select anex_radi_nume, anex_codigo, radi_nume_salida  from anexos where radi_nume_salida=$nurad and  anex_radi_nume=$nurad";
		$resultado2 = $db->query ( $sql2 );
		$Anex_Nume2 = $resultado2->fields ['ANEX_RADI_NUME'];
		$Anex_Codi2 = $resultado2->fields ['ANEX_CODIGO'];
		if (! $Anex_Nume2) {
			if ($_SESSION ['usua_debug'] == 1) {
				echo "<hr>Salio Function 1 <hr>";
				echo "</span>";
			}
			return 0;
		}
		
		$isqlA = "update anexos set ANEX_DESC=q'[" . $recordOtros ['ASUNTO'] . "]', radi_reservado = '".$recordOtros['RADI_RESERVADO']."' where anex_radi_nume=$Anex_Nume2 and anex_codigo=$Anex_Codi2 and radi_nume_salida=$nurad";
		$resultadoAnexo = $db->query ( $isqlA );
		if ($_SESSION ['usua_debug'] == 1) {
			echo "<hr>Salio Function 3 <hr>";
			echo "</span>";
		}
		return 0;
	}
	$isqlA = "update anexos set ANEX_DESC=q'[" . $recordOtros ['ASUNTO'] . "]', radi_reservado = '".$recordOtros['RADI_RESERVADO']."' where anex_radi_nume=$Anex_Nume and anex_codigo=$Anex_Codi and radi_nume_salida=$nurad";
	$resultadoAnexo = $db->query ( $isqlA );
	
	$record2 = array ();
	$record2 ['SGD_TRD_CODIGO'] = $record ['SGD_TRD_CODIGO'];
	$record2 ['SGD_DIR_NOMREMDES'] = $record ['SGD_DIR_NOMREMDES'];
	$record2 ['SGD_DIR_DOC'] = $record ['SGD_DIR_DOC'];
	$record2 ['MUNI_CODI'] = $record ['MUNI_CODI'];
	$record2 ['DPTO_CODI'] = $record ['DPTO_CODI'];
	$record2 ['ID_PAIS'] = $record ['ID_PAIS'];
	$record2 ['ID_CONT'] = $record ['ID_CONT'];
	
	$record2 ['SGD_DOC_FUN'] = $record ['SGD_DOC_FUN'];
	$record2 ['SGD_OEM_CODIGO'] = $record ['SGD_OEM_CODIGO'];
	$record2 ['SGD_CIU_CODIGO'] = $record ['SGD_CIU_CODIGO'];
	$record2 ['SGD_OEM_CODIGO'] = $record ['SGD_OEM_CODIGO'];
	$record2 ['SGD_ESP_CODI'] = $record ['SGD_ESP_CODI'];
	//$record2['RADI_NUME_RADI'] = $nurad;
	

	$record2 ['SGD_SEC_CODIGO'] = $record ['SGD_SEC_CODIGO'];
	$record2 ['SGD_DIR_DIRECCION'] = $record ['SGD_DIR_DIRECCION'];
	//	$record2['SGD_DIR_TELEFONO'] = $record['SGD_DIR_TELEFONO']
	

	$record2 ['SGD_DIR_MAIL'] = $record ['SGD_DIR_MAIL'];
	$record2 ['SGD_DIR_TIPO'] = 7;
	
	$record2 ['SGD_DIR_NOMBRE'] = $record ['SGD_DIR_NOMBRE'];
	
	$sql = "select  SGD_DIR_CODIGO from sgd_dir_drecciones  where sgd_anex_codigo ='$Anex_Codi' and sgd_dir_tipo=7";
	$resultado1 = $db->query ( $sql );
	
	if ($resultado1->fields ['SGD_DIR_CODIGO']) {
		if ($_SESSION ['usua_debug'] == 1)
			echo '<hr>update Anx SGD_DIR_DIRECCIONES<hr>';
		$recordWhere2 ['SGD_DIR_CODIGO'] = $resultado1->fields ['SGD_DIR_CODIGO'];
		//$record2 ['SGD_ANEX_CODIGO'] = $Anex_Codi;
		$record2 ['RADI_NUME_RADI'] = $Anex_Nume;
		$rsUpdateSQL = $db->update ( 'SGD_DIR_DRECCIONES', $record2, $recordWhere2 );
	
	} else {
		if ($_SESSION ['usua_debug'] == 1)
			echo '<hr>Insert Anx SGD_DIR_DIRECCIONES<hr>';
		$isql = "select  max(sgd_dir_cpcodi)  NUM,max(sgd_dir_tipo) NUM2 
					from sgd_dir_drecciones	where
					(sgd_dir_tipo > '700' or sgd_dir_tipo = '0') and sgd_anex_codigo='$Anex_Codi'";
		$rs = $db->query ( $isql );
		if (! $rs->EOF)
			if ($rs->fields ["NUM"] > $rs->fields ["NUM2"]) {
				$NUM = $rs->fields ["NUM"];
			} else {
				$NUM = $rs->fields ["NUM2"];
			}
		$num_anexos = substr ( $NUM, 1, 2 );
		$str_num_anexos = substr ( "00$num_anexos", - 2 );
		$cpcodi2 = 7;
		$sgd_dir_tipo = "7$str_num_anexos";
		//		$insertSQL = $conexion->conn->Replace("SGD_DIR_DRECCIONES", $record, array('RADI_NUME_RADI','SGD_DIR_TIPO'), $autoquote = true);
		$record2 ['SGD_DIR_TIPO'] = 7;
		$record2 ['SGD_DIR_CPCODI'] = $sgd_dir_tipo;
		$record2 ['SGD_ANEX_CODIGO'] = $Anex_Codi;
		//$record2 ['RADI_NUME_RADI']  = $nurad;
		$record2 ['RADI_NUME_RADI'] = $Anex_Nume;
		$record2 ['SGD_DIR_CODIGO'] = $db->nextId ( "sec_dir_direcciones" );
		$rsInsertSQL = $db->insert ( 'SGD_DIR_DRECCIONES', $record2, 'true' );
	
	}
	if ($_SESSION ['usua_debug'] == 1) {
		echo "<hr>Salio Function <hr>";
		echo "</span>";
	}
	return 0;
}

function MasivaActualizar($db, $record, $recordOtros) {
	if ($_SESSION ['usua_debug'] == 1) {
		echo "<span style='color:cyan;'>";
		echo "<hr>Ingreso Function MasivaActualizar <hr>";
		$db->conn->debug = true;
	}
	$nurad = $record ['RADI_NUME_RADI'];
	//$isql = "select distinct sgd_anex_codigo from sgd_dir_drecciones  where radi_nume_radi=$nurad and sgd_anex_codigo is not null order by sgd_anex_codigo asc";
	$isql = "SELECT DISTINCT E.SGD_DEVE_CODIGO DEVUELTO, r.radi_nume_radi radicado ,e.radi_nume_grupo
    FROM hist_eventos h,   radicado r,  sgd_renv_regenvio e  WHERE h.radi_nume_radi = r.radi_nume_radi  and e.radi_nume_sal    = r.radi_nume_radi
    and h.sgd_ttr_codigo   =30  and sgd_fenv_codigo  =101 and sgd_renv_planilla ='00'  and r.radi_nume_radi=$nurad and E.SGD_DEVE_CODIGO is null ";
	//$isql = "select distinct anex_radi_nume from anexos where radi_nume_salida=$nurad";
	$resultado = $db->query ( $isql );
	$Nume_grupo = $resultado->fields ['RADI_NUME_GRUPO'];
	$Devuelto = $resultado->fields ['DEVUELTO'];
	//echo $Anex_RADI = $resultado->fields ['RADI_NUME_RADI'];
	if (! $Nume_grupo) {
		if ($_SESSION ['usua_debug'] == 1) {
			echo "<hr>Salio Function MasivaActualizar <hr>";
			echo "</span>";
		}
		return 0;
	}
		if ($_SESSION ['usua_debug'] == 1) 
			echo "<hr>Si pertenece a Masiva se debe Actualizar sgd_renv_envio <hr>";
		
	$record2 = array ();
	
	$record2 ['SGD_RENV_NOMBRE'] = $record ['SGD_DIR_NOMREMDES'];
	$record2 ['SGD_RENV_DIR'] = $record ['SGD_DIR_DIRECCION'];
	$sql = "select m.muni_nomb MUNI,d.dpto_nomb DEPTO,p.nombre_pais PAIS from municipio m,departamento d , sgd_def_paises p
  			where 
  			m.muni_codi=".$record ['MUNI_CODI']." and  m.id_pais=".$record ['ID_PAIS']." and m.dpto_codi=".$record ['DPTO_CODI']."
  			and m.dpto_codi=d.dpto_codi and m.id_pais=d.id_pais
  			and p.id_pais=d.id_pais and m.id_pais=p.id_pais";
	$resultadoP = $db->query ( $sql );
	$record2 ['SGD_RENV_MPIO'] = "'".$resultadoP->fields ['MUNI']."'";
	$record2 ['SGD_RENV_DEPTO'] = "'".$resultadoP->fields ['DEPTO']."'";
	$record2 ['SGD_RENV_PAIS'] = "'".$resultadoP->fields ['PAIS']."'";
	$record2 ['SGD_RENV_TEL'] = $record['SGD_DIR_TELEFONO'];	
	
		if ($_SESSION ['usua_debug'] == 1)
			echo '<hr>update Anx SGD_RENV_ENVIO<hr>';
		$recordWhere2 ['RADI_NUME_GRUPO'] = $Nume_grupo;
		$recordWhere2 ['RADI_NUME_SAL'] = $nurad;
		//$record2 ['SGD_ANEX_CODIGO'] = $Anex_Codi;
		//$record2 ['RADI_NUME_RADI'] = $Anex_Nume;
		$rsUpdateSQL = $db->update ( 'SGD_RENV_REGENVIO', $record2, $recordWhere2 );
	
	
	if ($_SESSION ['usua_debug'] == 1) {
		echo "<hr>Salio Function MAsiva <hr>";
		echo "</span>";
	}
	return 0;
}

function ActualizarAnexosPadre($db, $record) {
	if ($_SESSION ['usua_debug'] == 1) {
		echo "<span style='color:RED;'>";
		echo "<hr>Ingreso Function Actualizar Anexos del radicado modificado <hr>";
		$db->conn->debug = true;
	}
	$nurad = $record ['RADI_NUME_RADI'];
	//$isql = "select distinct sgd_anex_codigo from sgd_dir_drecciones  where radi_nume_radi=$nurad and sgd_anex_codigo is not null order by sgd_anex_codigo asc";
	$isql = " select   DISTINCT anex_radi_nume,radi_nume_salida from anexos where anex_radi_nume =$nurad 
              and  substr(anex_codigo,0,14) ='$nurad'
              order by anex_radi_nume asc ";
	$resultado = $db->query ( $isql );
	$Anex_Nume = $resultado->fields ['ANEX_RADI_NUME'];
	//echo $Anex_RADI = $resultado->fields ['RADI_NUME_RADI'];
	if (! $Anex_Nume) {
			if ($_SESSION ['usua_debug'] == 1) {
				echo "<hr>Salio Function 1 <hr>";
				echo "</span>";
			}
			return 0;
	}
	$isqlF="select SGD_DIR_CODIGO,sgd_dir_tipo,sgd_dir_cpcodi from sgd_dir_drecciones where  substr(sgd_anex_codigo,0,14) =$nurad
            and sgd_dir_cpcodi>0 and sgd_dir_cpcodi<4 and sgd_dir_tipo!=7";
	
	$resultado1=$db->query ( $isqlF );
	while($resultado1 && !$resultado1->EOF)
	 {	
		if ($_SESSION ['usua_debug'] == 1)
		   echo '<hr>update Anx SGD_DIR_DIRECCIONES<hr>';
		   
			$recordWhere2 ['SGD_DIR_CODIGO'] = $resultado1->fields ['SGD_DIR_CODIGO'];
			//$record2 ['SGD_ANEX_CODIGO'] = $Anex_Codi;
			$record2 ['SGD_DIR_CPCODI'] = $resultado1->fields ['SGD_DIR_TIPO'];
			$rsUpdateSQL = $db->update ( 'SGD_DIR_DRECCIONES', $record2, $recordWhere2 );
			$resultado1->MoveNext();
	
	}
	if ($_SESSION ['usua_debug'] == 1) {
		echo "<hr>Salio Function <hr>";
		echo "</span>";
	}
	return 0;
}

?>