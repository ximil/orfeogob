<?php /**
 * @name :   
 * @desc :   Genera checks de copias y destinatario.
 * @author  :  Hardy  Deimont Niño Velasquez.
 * @version 0.1
 */
putenv("NLS_LANG=American_America.UTF8");
$ruta_raiz = "..";
require_once ("$ruta_raiz/include/db/ConnectionHandler.php");
include_once $ruta_raiz . "/../core/config/config-inc.php";
$ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
$db = new ConnectionHandler($ruta_raiz);
$db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
session_start(); date_default_timezone_set('America/Bogota'); date_default_timezone_set('America/Bogota');
if ($_SESSION ['usua_debug'] == 1) {
    echo "<span style='color:RED;'>";
    echo "<hr> Varibles Get <br>";
    print_r($_GET);
    echo "<hr>";
    print_r($_SESSION);
    echo "<hr>";
    $db->conn->debug = true;
}
$action = $_POST ['action'];
//print_r($_POST);
//$db->conn->debug = true;
$no_documento = $_POST ['no_documento'];
$ent = $_POST ['ent'];
$tbusqueda = $_POST ['tbusqueda'];
$nombre_essp = $_POST ['nombre_essp'];
$label_us = $_POST ['label_us'];
$label_pred = $_POST ['label_pred'];
$label_emp = $_POST ['label_emp'];
if ($action == 'ModBusqueda') {
    //print_r($_POST);
    //echo 'Modificado';
    $codigo = $_POST ['codigo'];
    $no_documento1 = $_POST ['no_documento1'];
    $nombre_nus1 = $_POST ['nombre_nus1'];
    $prim_apell_nus1 = $_POST ['prim_apell_nus1'];
    $seg_apell_nus1 = $_POST ['seg_apell_nus1'];
    $direccion_nus1 = $_POST ['direccion_nus1'];
    $telefono_nus1 = $_POST ['telefono_nus1'];
    $tagregar = $_POST ['tagregar'];
    $mail_nus1 = $_POST ['mail_nus1'];
    $idcont4 = $_POST ['idcont4'];
    $idpais4 = $_POST ['idpais4'];
    $codep_us4 = $_POST ['codep_us4'];
    $muni_us4 = $_POST ['muni_us4'];
    $muni_tmp = explode("-", $muni_us4);
    $muni_tmp = $muni_tmp[2];
    $dpto_tmp = explode("-", $codep_us4);
    $dpto_tmp = $dpto_tmp[1];
    if ($tagregar == 0) {

        $isql = "update SGD_CIU_CIUDADANO set SGD_CIU_CEDULA='$no_documento1', SGD_CIU_NOMBRE='$nombre_nus1', 
      			SGD_CIU_DIRECCION='$direccion_nus1', SGD_CIU_APELL1='$prim_apell_nus1', SGD_CIU_APELL2='$seg_apell_nus1',
      			SGD_CIU_TELEFONO='$telefono_nus1', SGD_CIU_EMAIL='$mail_nus1', ID_CONT=$idcont4, ID_PAIS=$idpais4, 
      			DPTO_CODI=$dpto_tmp, MUNI_CODI=$muni_tmp where SGD_CIU_CODIGO=$codigo ";
        $rs = $db->query($isql);
        if (!$rs) {
            die("<span class='etextomenu'>No se pudo actualizar SGD_CIU_CIUDADANO ($isql) ");
        }
        $isql = "select * from SGD_CIU_CIUDADANO where SGD_CIU_CEDULA='$no_documento1'";
        $rs = $db->query($isql);
    }

    if ($tagregar == 2) {
        $isql = "UPDATE SGD_OEM_OEMPRESAS 
	SET SGD_OEM_NIT='$no_documento1', SGD_OEM_OEMPRESA='$nombre_nus1', 
	SGD_OEM_DIRECCION='$direccion_nus1', SGD_OEM_REP_LEGAL='$seg_apell_nus1', SGD_OEM_SIGLA='$prim_apell_nus1',
	SGD_OEM_TELEFONO='$telefono_nus1', ID_CONT=$idcont4, ID_PAIS= $idpais4, DPTO_CODI=$dpto_tmp, 
	MUNI_CODI=$muni_tmp where SGD_OEM_CODIGO='$codigo'";
        $rs = $db->query($isql);

        if (!$rs) {
            $db->conn->RollbackTrans();
        }
    }
    echo " <span   align='center' style='size: 30;color: red;font-style: oblique;' >-- Se Modifico el Registro -- $no_documento $nombre_essp -- </span>";
    $action = 'consultarBusqueda';
}
if ($action == 'AgregarBusqueda') {
    echo 'Agregar';
    $codigo = $_POST ['codigo'];
    $no_documento1 = $_POST ['no_documento1'];
    $nombre_nus1 = $_POST ['nombre_nus1'];
    $prim_apell_nus1 = $_POST ['prim_apell_nus1'];
    $seg_apell_nus1 = $_POST ['seg_apell_nus1'];
    $direccion_nus1 = $_POST ['direccion_nus1'];
    $telefono_nus1 = $_POST ['telefono_nus1'];
    $tagregar = $_POST ['tagregar'];
    $mail_nus1 = $_POST ['mail_nus1'];
    $idcont4 = $_POST ['idcont4'];
    $idpais4 = $_POST ['idpais4'];
    $codep_us4 = $_POST ['codep_us4'];
    $muni_us4 = $_POST ['muni_us4'];
    $muni_tmp = explode("-", $muni_us4);
    $muni_tmp = $muni_tmp [2];
    $dpto_tmp = explode("-", $codep_us4);
    $dpto_tmp = $dpto_tmp [1];

    $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
    if ($tagregar == 0) {
        $cedula = 999999;
        if ($no_documento) {
            $isql = "select SGD_CIU_CEDULA  from SGD_CIU_CIUDADANO WHERE  SGD_CIU_CEDULA='$no_documento'";
            $rs = $db->query($isql);

            if (!$rs->EOF)
                $cedula = $rs->fields ["SGD_CIU_CEDULA"];
            $flag == 0;
        }
        //echo "--->Doc >$no_documento<";
        if ($cedula == $no_documento and $no_documento != "" and $no_documento != 0) {
            echo "<center><b><font color=red><center><< No se ha podido agregar el usuario, El usuario ya se encuentra >> </center></font>";
        } else {

            $nextval = $db->nextId("sec_ciu_ciudadano");
            if ($nextval == - 1) {
                die("<span class='etextomenu'>No se encontr� la secuencia sec_ciu_ciudadano ");
            }
            error_reporting(7);
            $isql = "INSERT INTO SGD_CIU_CIUDADANO(SGD_CIU_CEDULA, TDID_CODI, SGD_CIU_CODIGO, SGD_CIU_NOMBRE,
					SGD_CIU_DIRECCION, SGD_CIU_APELL1, SGD_CIU_APELL2, SGD_CIU_TELEFONO, SGD_CIU_EMAIL, ID_CONT, ID_PAIS, 
					DPTO_CODI, MUNI_CODI) values ('$no_documento', 2, $nextval, '$nombre_nus1', '$direccion_nus1', 
					'$prim_apell_nus1', '$seg_apell_nus1','$telefono_nus1', '$mail_nus1', 
					$idcont4, $idpais4, $dpto_tmp, $muni_tmp)";
            if (!trim($no_documento))
                $nombre_essp = "$nombre_nus1 $prim_apell_nus1 $seg_apell_nus1";
            $rs = $db->query($isql);
            if (!$rs) {
                $db->conn->RollbackTrans();
                die("<span class='etextomenu'>No se pudo actualizar SGD_CIU_CIUDADANO ($isql) ");
            }
        }
        if ($flag == 1) {
            echo "<center><b><font color=red><center>No se ha podido agregar el usuario, verifique que los datos sean correctos</center></font>";
        }
        $isql = "select * from SGD_CIU_CIUDADANO where SGD_CIU_CEDULA='$no_documento1'";
        $rs = $db->query($isql);
    }
    if ($tagregar == 2) {
        $nextval = $db->nextId("sec_oem_oempresas");

        if ($nextval == - 1) {
            die("<span class='etextomenu'>No se encontr&oacute; la secuencia sec_oem_oempresas ");
        }

        $isql = "INSERT INTO SGD_OEM_OEMPRESAS( tdid_codi, SGD_OEM_CODIGO, SGD_OEM_NIT, SGD_OEM_OEMPRESA, SGD_OEM_DIRECCION, 
				SGD_OEM_REP_LEGAL, SGD_OEM_SIGLA, SGD_OEM_TELEFONO, ID_CONT, ID_PAIS, DPTO_CODI, MUNI_CODI) 
				values (4, $nextval, '$no_documento', '$nombre_nus1', '$direccion_nus1', '$seg_apell_nus1', 
						'$prim_apell_nus1', '$telefono_nus1',$idcont4, $idpais4, $dpto_tmp, $muni_tmp)";
        $rs = $db->query($isql);

        if (!$rs)
            die("<span class='titulosError'>No se pudo insertar en SGD_OEM_OEMPRESAS ($isql) ");
    }
    echo " <span   align='center' style='size: 30;color: red;font-style: oblique;' >-- Se Agrego el Registro -- $no_documento $nombre_essp -- </span>";
    $action = 'consultarBusqueda';
}

if ($action == 'consultarBusqueda') {
    //print_r($_POST);
    $nombre_essp = $_POST ['nombre_essp'];
    ?><table class=borde_tab width="99%" cellpadding="0" cellspacing="1"
           bgcolor="#FFFFFF"><?php     if ($tbusqueda == 0) {
        //$array_nombre =  split ( " ", $nombre_essp . "    " );
        $array_nombre = explode(" ", $nombre_essp . "    ");
        $isql = "select * from SGD_CIU_CIUDADANO where (";
        if ($nombre_essp) {
            if ($array_nombre [0]) {
                $where_split = $db->conn->Concat("UPPER(sgd_ciu_nombre)", "UPPER(sgd_ciu_apell1)", "UPPER(sgd_ciu_apell2)") . " LIKE UPPER('%" . $array_nombre [0] . "%') ";
            }
            if ($array_nombre [1]) {
                $where_split .= " and " . $db->conn->Concat("UPPER(sgd_ciu_nombre)", "UPPER(sgd_ciu_apell1)", "UPPER(sgd_ciu_apell2)") . " LIKE UPPER('%" . $array_nombre [1] . "%') ";
            }
            if ($array_nombre [2]) {
                $where_split .= " and " . $db->conn->Concat("UPPER(sgd_ciu_nombre)", "UPPER(sgd_ciu_apell1)", "UPPER(sgd_ciu_apell2)") . " LIKE UPPER('%" . $array_nombre [2] . "%') ";
            }
            if ($array_nombre [3]) {
                $where_split .= " and " . $db->conn->Concat("UPPER(sgd_ciu_nombre)", "UPPER(sgd_ciu_apell1)", "UPPER(sgd_ciu_apell2)") . " LIKE UPPER('%" . $array_nombre [3] . "%') ";
            }
            $isql .= "  $where_split ";
        }
        if ($no_documento) {
            if ($where_split)
                $isql .= " and ";

            $isql .= " SGD_CIU_CEDULA='$no_documento' ";
        }
        $isql .= " ) and (sgd_ciu_estado=0 or sgd_ciu_estado is null) order by sgd_ciu_nombre,sgd_ciu_apell1,sgd_ciu_apell2 ";
    }
    if ($tbusqueda == 2) {
        $isql = "select SGD_OEM_NIT AS SGD_CIU_CEDULA,SGD_OEM_OEMPRESA as 	SGD_CIU_NOMBRE,SGD_OEM_REP_LEGAL as SGD_CIU_APELL2
	 ,SGD_OEM_CODIGO AS SGD_CIU_CODIGO,SGD_OEM_DIRECCION as SGD_CIU_DIRECCION,SGD_OEM_TELEFONO AS SGD_CIU_TELEFONO,SGD_OEM_SIGLA AS SGD_CIU_APELL1
	 ,MUNI_CODI,DPTO_CODI,ID_PAIS,ID_CONT 	from SGD_OEM_OEMPRESAS where (";
        if ($nombre_essp) {
            $isql .= " UPPER(SGD_OEM_OEMPRESA) LIKE UPPER('%$nombre_essp%') 
	 OR UPPER(SGD_OEM_SIGLA) LIKE UPPER('%$nombre_essp%')";
        }

        if ($_POST ["no_documento"]) {
            if ($nombre_essp)
                $isql .= " and ";
            $isql .= "  SGD_OEM_NIT = '$no_documento'   ";
        }
        $isql .= ") and (sgd_oem_estado=0 or sgd_oem_estado is NULL) order by sgd_oem_oempresa";
    }
    if ($tbusqueda == 1) {
        $isql = "select NIT_DE_LA_EMPRESA AS SGD_CIU_CEDULA,NOMBRE_DE_LA_EMPRESA as SGD_CIU_NOMBRE
	,SIGLA_DE_LA_EMPRESA as SGD_CIU_APELL1, " . "IDENTIFICADOR_EMPRESA AS SGD_CIU_CODIGO
	,DIRECCION as SGD_CIU_DIRECCION
	,TELEFONO_1 AS SGD_CIU_TELEFONO
	,NOMBRE_REP_LEGAL as SGD_CIU_APELL2
	,SIGLA_DE_LA_EMPRESA as SGD_CIU_APELL1
	,CODIGO_DEL_DEPARTAMENTO as DPTO_CODI
	,CODIGO_DEL_MUNICIPIO as MUNI_CODI
	,ID_PAIS, ID_CONT 
	from BODEGA_EMPRESAS 
	WHERE 
	(UPPER(SIGLA_DE_LA_EMPRESA) LIKE UPPER('%$nombre_essp%') 
	OR UPPER(NOMBRE_DE_LA_EMPRESA) LIKE UPPER('%$nombre_essp%')) ";
        if ($db->driver == "mssql") {
            $isql = str_replace("UPPER(SIGLA_DE_LA_EMPRESA)", "SIGLA_DE_LA_EMPRESA", $isql);
            $isql = str_replace("UPPER(NOMBRE_DE_LA_EMPRESA)", "NOMBRE_DE_LA_EMPRESA", $isql);
        }
        //Si incluye ESP desactivas
        if (!isset($_POST ['chk_desact']))
            $isql .= " and ACTIVA = 1 ";
        if (strlen(trim($no_documento)) > 0) {
            $isql .= " and NIT_DE_LA_EMPRESA like '%$no_documento%'";
            $isql .= " order by NOMBRE_DE_LA_EMPRESA ";
        }
    }
    if ($tbusqueda == 6) {
        $array_nombre = split(" ", $nombre_essp . "    ");
        //Query que busca funcionario
        $isql = "select usua_doc AS SGD_CIU_CEDULA
	,usua_nomb as SGD_CIU_NOMBRE
	,'' as SGD_CIU_APELL1
	,USUA_DOC AS SGD_CIU_CODIGO
	,dependencia.depe_nomb as SGD_CIU_DIRECCION
	,USUARIO.USUA_EXT  AS SGD_CIU_TELEFONO
	,USUARIO.USUA_LOGIN as SGD_CIU_APELL2
	,'' as SGD_CIU_APELL1
	,dependencia.ID_CONT
	, dependencia.ID_PAIS
	, dependencia.DPTO_CODI as DPTO_CODI
	,dependencia.MUNI_CODI as MUNI_CODI
	,USUARIO.usua_email as SGD_CIU_EMAIL 
        ,dependencia.depe_nomb as DEPENOMB
        ,rol.sgd_rol_nombre as ROLNOMB
	from USUARIO,dependencia , sgd_urd_usuaroldep urd,sgd_rol_roles rol
    where USUA_ESTA='1' AND  urd.depe_codi= dependencia.depe_codi  and
 urd.usua_codi=USUARIO.usua_codi and rol.sgd_rol_id=urd.rol_codi  ";
        if ($nombre_essp) {
            if ($array_nombre [0]) {
                $where_split = "  UPPER(USUA_NOMB) LIKE UPPER('%" . $array_nombre [0] . "%') ";
            }
            if ($array_nombre [1]) {
                $where_split .= " AND UPPER(USUA_NOMB) LIKE UPPER('%" . $array_nombre [1] . "%') ";
            }
            if ($array_nombre [2]) {
                $where_split .= " AND UPPER(USUA_NOMB) LIKE UPPER('%" . $array_nombre [2] . "%') ";
            }
            if ($array_nombre [3]) {
                $where_split .= " AND UPPER(USUA_NOMB) LIKE UPPER('%" . $array_nombre [3] . "%') ";
            }
            $isql .= " and $where_split ";
        }
        if ($no_documento) {
            if ($nombre_eesp)
                $isql .= " and ";
            else
                $isql .= " and ";
            $isql .= " usua_doc='$no_documento' ";
        }
        $isql .= " order by usua_nomb ";
    }

    //$db->conn->debug = true;
    $rs = $db->query($isql);

    $tipo_emp = $tbusqueda;
    if ($rs && !$rs->EOF) { //echo $isql;
        $esty = 1;
        ?>
            <tr class="grisCCCCCC" align="center">
                <td width="10%" CLASS="titulos31">DOCUMENTO</td>
                <td width="10%" CLASS="titulos31">NOMBRE</td>
                <td width="10%" CLASS="titulos31">PRIM. APELLIDO o SIGLA</td>
                <td width="10%" CLASS="titulos31">SEG. APELLIDO o R Legal</td>
                <td width="10%" CLASS="titulos31">DIRECCION</td>
                <td width="5%" CLASS="titulos31">CIUDAD</td>
                <td width="5%" CLASS="titulos31">TELEFONO</td>
                <td width="5%" CLASS="titulos31">EMAIL</td>
        <?php if ($tbusqueda == 6) { ?>
                    <td width="5%" CLASS="titulos31" >DEPENDENCIA</td>
                    <td width="5%" CLASS="titulos31" >ROL</td>
        <?php } ?>
                <td colspan="3%" CLASS="titulos31">COLOCAR COMO</td>
            </tr> 

                <?php                 while (!$rs->EOF) {
                    ($grilla == "timparr") ? $grilla = "timparr" : $grilla = "tparr";
                    if ($esty == 1) {
                        $estilo = 'listado51';
                        $esty = 0;
                    } else {
                        $estilo = 'listado50';
                        $esty = 1;
                    }
                    ?>
                <tr class='<?php echo  $grilla ?>'>
                    <TD class="<?php echo  $estilo; ?>"><font size="-3"><?php echo  $rs->fields ["SGD_CIU_CEDULA"] ?></font></TD>
                    <TD class="<?php echo  $estilo; ?>"><font size="-3"> <?php echo  substr($rs->fields ["SGD_CIU_NOMBRE"], 0, 120) ?></font></TD>
                    <TD class="<?php echo  $estilo; ?>"><font size="-3">
                <?php echo  substr($rs->fields ["SGD_CIU_APELL1"], 0, 70) ?></font></TD>
                    <TD class="<?php echo  $estilo; ?>"><font size="-3"> <?php echo  $rs->fields ["SGD_CIU_APELL2"] ?> </font></TD>
                    <TD class="<?php echo  $estilo; ?>"><font size="-3"> <?php echo  $rs->fields ["SGD_CIU_DIRECCION"] ?></font></TD>
                    <TD class="<?php echo  $estilo; ?>"><font size="-3">
            <?php 
            $isql2 = "select MUNI_NOMB from municipio where
	muni_codi='" . $rs->fields ["MUNI_CODI"] . "' and
	dpto_codi='" . $rs->fields ["DPTO_CODI"] . "'";
            $rs2 = $db->query($isql2);
            if (!$rs2->EOF) {
                echo $rs2->fields ["MUNI_NOMB"];
            }
            $cc_documento = trim($rs->fields ["SGD_CIU_CODIGO"]);
            $email = trim(str_replace('"', ' ', $rs->fields ["SGD_CIU_EMAIL"]));
            $telefono = trim(str_replace('"', ' ', $rs->fields ["SGD_CIU_TELEFONO"]));
            $direccion = trim(str_replace('"', ' ', $rs->fields ["SGD_CIU_DIRECCION"]));
            $apell2 = trim(str_replace('"', ' ', $rs->fields ["SGD_CIU_APELL2"]));
            $apell1 = trim(str_replace('"', ' ', $rs->fields ["SGD_CIU_APELL1"]));
            $nombre = trim(str_replace('"', ' ', $rs->fields ["SGD_CIU_NOMBRE"]));
            $codigo = trim($rs->fields ["SGD_CIU_CODIGO"]);

            $codigo_cont = $rs->fields ["ID_CONT"];
            $codigo_pais = $rs->fields ["ID_PAIS"];
            $codigo_dpto = $codigo_pais . "-" . $rs->fields ["DPTO_CODI"];
            $codigo_muni = $codigo_dpto . "-" . $rs->fields ["MUNI_CODI"];
            $cc_documento = trim($rs->fields ["SGD_CIU_CEDULA"]);
            ?>
                        </font></TD>
                    <TD class="<?php echo  $estilo; ?>"><font size="-3"> <?php echo  trim($rs->fields ["SGD_CIU_TELEFONO"]) ?> </font></TD>
                    <TD class="<?php echo  $estilo; ?>"><font size="-3"> <?php echo  substr($rs->fields ["SGD_CIU_EMAIL"], 0, 30) ?></font></TD>
                     <?php if($tbusqueda==6){?> 
        <TD class="<?php echo $estilo;?>"> <font size="-3"> <?php echo substr($rs->fields["DEPENOMB"],0,30) ?></font></TD>            
        <TD class="<?php echo $estilo;?>"> <font size="-3"> <?php echo substr($rs->fields["ROLNOMB"],0,30) ?></font></TD>
                    <?php }?>
                    <TD width="6%" align="center" valign="top" class="<?php echo  $estilo; ?>"><font
                            size="-3"><a href="#btnpasar"
                            onClick="pasar('<?php echo  $codigo ?>', '<?php echo  $cc_documento ?>', '<?php echo  $nombre ?>', '<?php echo  $apell1 ?>', '<?php echo  $apell2 ?>', '<?php echo  $direccion ?>'
                                            , '<?php echo  $telefono ?>', '<?php echo  $email ?>', '<?php echo  $tbusqueda ?>', '<?php echo  $codigo_muni ?>', '<?php echo  $codigo_dpto ?>', '<?php echo  $codigo_pais ?>', '<?php echo  $codigo_cont ?>', 1);"
                            class="<?php echo  $estilo; ?>"><?php echo  $label_us ?></a></font></TD>
            <?php 
            if (!$envio_salida or $ent == 5) {
                if ($vista_predio == 1) {
                    ?>
                            <td width="6%" align="center" valign="top" class="<?php echo  $estilo; ?>"><font
                                    size="-3"><a href="#btnpasar"
                                    onClick="pasar('<?php echo  $codigo ?>', '<?php echo  $cc_documento ?>', '<?php echo  $nombre ?>', '<?php echo  $apell1 ?>', '<?php echo  $apell2 ?>', '<?php echo  $direccion ?>'
                                                    , '<?php echo  $telefono ?>', '<?php echo  $email ?>', '<?php echo  $tbusqueda ?>', '<?php echo  $codigo_muni ?>', '<?php echo  $codigo_dpto ?>', '<?php echo  $codigo_pais ?>', '<?php echo  $codigo_cont ?>', 2);"
                                    class="<?php echo  $estilo; ?>"><?php echo  $label_pred ?></a></font></td>
                            <?php 
                            } if ($vista_entidad == 1) {
                                if ($tbusqueda == 1) {
                                    if ($nuevo_archivo == 1) {
                                        $label_emp = '';
                                    }
                                    ?>
                                <td width="7%" align="center" valign="top" class="<?php echo  $estilo; ?>"><font
                                        size="-3"> <a href="#btnpasar"
                                        onClick="pasar('<?php echo  $codigo ?>', '<?php echo  $cc_documento ?>', '<?php echo  $nombre ?>', '<?php echo  $apell1 ?>', '<?php echo  $apell2 ?>', '<?php echo  $direccion ?>'
                                                        , '<?php echo  $telefono ?>', '<?php echo  $email ?>', '<?php echo  $tbusqueda ?>', '<?php echo  $codigo_muni ?>', '<?php echo  $codigo_dpto ?>', '<?php echo  $codigo_pais ?>', '<?php echo  $codigo_cont ?>', 3);"
                                        class="titulos5"><?php echo  $label_emp ?></a></font></td>
                                    <?php 
                                }
                            }
                        }
                        ?>
                </tr>

                        <?php 
                        $i++;
                        $rs->MoveNext();
                    }

                    //		echo "<span class='listado2'>Registros Encontrados</span>";
                } else {
                    ?><tr class='titulosError' ><td   align='center' style='size: 30;color: red;font-style: oblique;' >-- No se encontraron Registros -- <?php echo $no_documento . " " . $nombre_essp; ?> --</td></tr><?php 
        }
        ?>
    </table> 	
        <?php 
    }
 
 
 
