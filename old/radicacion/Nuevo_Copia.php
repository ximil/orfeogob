<?php /**
 *  @name :   
 *  @desc :   Genera checks de copias y destinatario.
 *  @author  :  Hardy  Deimont Niño Velasquez.
 *  @version 0.1
 */
putenv("NLS_LANG=American_America.UTF8");
$ruta_raiz = "..";
require_once ("$ruta_raiz/include/db/ConnectionHandler.php");
$ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
$db = new ConnectionHandler ( $ruta_raiz );
$db->conn->SetFetchMode ( ADODB_FETCH_ASSOC );
session_start ();
if ($_SESSION ['usua_debug'] == 1) {
	echo "<span style='color:RED;'>";
	echo "<hr> Varibles Get <br>";
	print_r ( $_GET );
	echo "<hr>";
	print_r ( $_SESSION );
	echo "<hr>";
	$db->conn->debug = true;
}
//titulo


$tipo = $_GET ['tipo'];
$codigo = $_GET ['codigo'];
$nameInput = $_GET ['nameInput'];
$name = $_GET ['name'];
$code = $_GET ['code'];
$divcp = $_GET ['divcp'];

if ($_GET ['action'] == 'Agregar') {
	
	$isql="select case when max(sgd_dir_tipo)>max(sgd_dir_cpcodi) "."then  max(sgd_dir_tipo) when max(sgd_dir_tipo) "." < max(sgd_dir_cpcodi)  then  max(sgd_dir_cpcodi) "." when max(sgd_dir_tipo)=max(sgd_dir_cpcodi)  then  max(sgd_dir_cpcodi) end num "." from sgd_dir_drecciones "
                    ."where  sgd_anex_codigo= '$codigo'";
	$rs = $db->query ( $isql );
	$radi = substr ( $codigo, 0, - 5 );
	if ($code == 3) {
		$isql = "select b.nit_de_la_empresa DOC, b.nombre_de_la_empresa NOMREMDES,b.direccion SGD_DIR_DIRECCION,b.id_cont,b.id_pais, b.codigo_del_departamento DPTO_CODI,b.codigo_del_municipio MUNI_CODI 
               ,b.identificador_empresa SGD_ESP_CODI,b.telefono_1 SGD_DIR_TELEFONO,b.email SGD_DIR_MAIL 
                from radicado r, bodega_empresas b
                 where r.radi_nume_radi='$radi'	and r.eesp_codi=b.identificador_empresa";
	
	} else {
		$isql = " select SGD_TRD_CODIGO , SGD_DIR_NOMREMDES NOMREMDES, SGD_DIR_DOC DOC, MUNI_CODI, DPTO_CODI,
			id_pais, id_cont, SGD_DOC_FUN, SGD_OEM_CODIGO, SGD_CIU_CODIGO, SGD_ESP_CODI, RADI_NUME_RADI,
			SGD_SEC_CODIGO,	SGD_DIR_DIRECCION, SGD_DIR_TELEFONO, SGD_DIR_MAIL, SGD_DIR_TIPO, SGD_DIR_CODIGO,
			 SGD_ANEX_CODIGO, SGD_DIR_NOMBRE
					from sgd_dir_drecciones
					where
					sgd_dir_tipo = '$code' and radi_nume_radi='$radi' and sgd_anex_codigo is null
			 ";
	}
	$rs2 = $db->query ( $isql );       
	$sgd_oem_codigo = $rs2->fields ["SGD_OEM_CODIGO"];
        if(!$sgd_oem_codigo) $sgd_oem_codigo=0;
	$sgd_ciu_codigo = $rs2->fields ["SGD_CIU_CODIGO"];
        if(!$sgd_ciu_codigo) $sgd_ciu_codigo=0;
	$sgd_esp_codigo = $rs2->fields ["SGD_ESP_CODI"];
        if(!$sgd_esp_codigo) $sgd_esp_codigo=0;
	$sgd_fun_codigo = $rs2->fields ["SGD_DOC_FUN"];
	$sgdTrd = $rs2->fields ["SGD_TRD_CODIGO"];
	$telefono_us1 = $rs2->fields ["SGD_DIR_TELEFONO"];
	$documento_us1 = $rs2->fields ["SGD_CIU_CODIGO"];
	$idcont1 = $rs2->fields ["ID_CONT"];
	$idpais1 = $rs2->fields ["ID_PAIS"];
	$codep_us1 = $rs2->fields ["DPTO_CODI"];
	$muni_us1 = $rs2->fields ["MUNI_CODI"];
	$cc_documento_us1 = $rs2->fields ["DOC"];
	$nombre_us1 = $rs2->fields ["NOMREMDES"];
	if (strlen ( $sgd_oem_codigo ) > 0 && $sgd_oem_codigo !=0) {
		$isql3 = " select sgd_oem_oempresa NOMREMDES from sgd_oem_oempresas where sgd_oem_codigo=$sgd_oem_codigo ";
		
		$rs3 = $db->query ( $isql3 );
		$nombre_us1 = $rs3->fields ["NOMREMDES"];
	}
	$direccion_us1 = $rs2->fields ["SGD_DIR_DIRECCION"];
	$mail_us1 = $rs2->fields ["SGD_DIR_MAIL"];
	$otro_us7 = $rs2->fields ["SGD_DIR_NOMBRE"];
	//         echo "($nombre_us1 or $prim_apel_us1 or $seg_apel_us2) and $direccion_us1 and $muni_us1 and $codep_us1)<br>";
	if (($nombre_us1 or $prim_apel_us1 or $seg_apel_us2) and $direccion_us1 and $muni_us1 and $codep_us1) {
		$nextval = $db->nextId ( "sec_dir_direcciones" );
		///$rs = $db->query ( $isql );
		if (! $rs->EOF) {			
		     $NUM=$rs->fields["NUM"]; 
		      	if($NUM<700)
			   	    $NUM=0;
		     $num_anexos = substr ( $NUM, 1, 2 );
		
		//$cpcodi = $code;
		$nurad = $radi;
		$num_anexos = $num_anexos + 1;
		$str_num_anexos = substr ( "00$num_anexos", - 2 );
		$sgd_dir_tipo = "7$str_num_anexos";
		$isql = "insert into SGD_DIR_DRECCIONES (SGD_TRD_CODIGO, SGD_DIR_NOMREMDES, SGD_DIR_DOC, MUNI_CODI, DPTO_CODI,
			id_pais, id_cont, SGD_DOC_FUN, SGD_OEM_CODIGO, SGD_CIU_CODIGO, SGD_ESP_CODI, RADI_NUME_RADI, SGD_SEC_CODIGO,
			SGD_DIR_DIRECCION, SGD_DIR_TELEFONO, SGD_DIR_MAIL, SGD_DIR_TIPO, SGD_DIR_CODIGO, SGD_ANEX_CODIGO, SGD_DIR_NOMBRE,sgd_dir_cpcodi) ";
		$isql .= "values ($sgdTrd, '$nombre_us1', '$cc_documento_us1', $muni_us1, '$codep_us1','$idpais1', '$idcont1',
						'$sgd_fun_codigo', '$sgd_oem_codigo', '$sgd_ciu_codigo', '$sgd_esp_codigo', $radi, 0, '$direccion_us1',
						'" . trim ( $telefono_us1 ) . "', '$mail_us1', $sgd_dir_tipo, $nextval, '$codigo', '$otro_us7',$code )";
		
		$dir_codigo_new = $nextval;
		$nextval ++;
		$rsg = $db->query ( $isql );
		
		//include "$ruta_raiz/radicacion/grb_direcciones.php";
		//	echo "<font size=1>Ha sido agregado el destinatario.</font>";
		echo "<input type='checkbox' checked name='$nameInput' value=''  id='$nameInput' onclick=\"HaboDES('$codigo','$code','$name','$nameInput','desX')\" >";
		}else{
			echo "Error base de datos";
		}
	} else {
		?><input type='checkbox' name='<?php  	echo $name;	?>' id='<?php 	echo $name;	?>' value=onclick='CopiaPAgregar("<?php  echo $codigo;
		?>","<?php 	echo $code;	?>","div <?php 	echo $name;		?>","div<?php 		echo $nameInput;		?>")' ><?php 		//echo "<font size=1>No se pudo guardar el documento, ya que faltan datos.(Los datos m&iacute;nimos de envio so Nombre, direccion, departamento, municipio)";
	}
	//	echo "<input type='checkbox'   name='$name' id='$name' value= onclick(CopiaPBorrar($codigo,$num_anexos,$name);) > ";


}

/** deshabilitar copia destinatario */

if ($_GET ['action'] == 'Modificar') {
	/// update 
	$cpcodi = $_POST ['tipo'];
	/*$isql = "delete from sgd_dir_drecciones
	         where
			   sgd_anex_codigo='$codigo' and sgd_dir_tipo = $tipo ";
			$rs=$db->query($isql);
		if(!$rs->EOF){
			//echo "pailas game over";
			echo " $mensaje <br><b><span  class='alarmas' >No puede Eliminar En algun momento Fue Enviado </span></b>";
			//var_dump($isql);	
		}*/
	
	echo "<input type='checkbox'   name='$name' id=$name value=0  > ";
}

if ($_GET ['action'] == 'hab') {
	hab_des_copia ( $db, $codigo, $tipo, $tipo );
	echo "<input type='checkbox'  checked name='$nameInput' value=''  id='$nameInput' checked onclick=\"CopiaPdeshab('$codigo','$tipo','$name','0','$nameInput')\" >";
}

if ($_GET ['action'] == 'des') {
	hab_des_copia ( $db, $codigo, $tipo, 0 );
	echo "<input type='checkbox'   name='$nameInput' value=''  id='$nameInput' onclick=\"CopiaPdeshab('$codigo','$tipo','$name','1','$nameInput')\" >";
}

if ($_GET ['action'] == 'Destin') {
	//echo "$tipo>700 && $sgdtipo!=7";
	if ($tipo > 700) {
		hab_des_copia ( $db, $codigo, $tipo, 7, 1 );
	}
	echo "<input type='checkbox'   name='$nameInput' value=''  id='$nameInput' onclick=\"CopiaPdeshab('$codigo','$tipo','$name','1','$nameInput')\" >";

}

if ($_GET ['action'] == 'DestinO') {
	$radpa=substr($codigo,0,14);
	$radicado_rem = $code;
	$isql2 = "update anexos set sgd_rem_destino=$radicado_rem,sgd_dir_tipo=$radicado_rem where anex_codigo='$codigo'";
	$rs = $db->query ( $isql2 );
	$isql3 = "select sgd_dir_tipo, sgd_dir_cpcodi from sgd_dir_drecciones where  sgd_anex_codigo='$codigo'";
	$rs2 = $db->query ( $isql3 );
	if ($rs2->fields ["SGD_DIR_CPCODI"]) {
		while ( ! $rs2->EOF ) {
			
			if ($rs2->fields ["SGD_DIR_TIPO"] == 7 or $rs2->fields ["SGD_DIR_CPCODI"] == $code) {
				$isql = "update  sgd_dir_drecciones set  SGD_DIR_TIPO=0,radi_nume_radi=$radpa
			    where  sgd_anex_codigo='$codigo' and sgd_dir_cpcodi=" . $rs2->fields ["SGD_DIR_CPCODI"];
				$rs = $db->query ( $isql );
			}
			
			$rs2->MoveNext ();
		}
		echo "<input type='checkbox'   name='cp$code' value=''  id='cp$code' onclick=\"HaboDES('$codigo','$code','$divcp','id$code','habX')\" >";
	} else {
		?><input type='checkbox' name='cp<?php 
		echo $code;
		?>'
	id='cp<?php 
		echo $code;
		?>' value=''
	onclick='CopiaPAgregar("<?php 
		echo $codigo;
		?>","<?php 
		echo $code;
		?>","<?php 
		echo $divcp;
		?>","id<?php 
		echo $code;
		?>")'><?php 	}

}

if ($_GET ['action'] == 'habX') {
	 $isql="select case when max(sgd_dir_tipo)>max(sgd_dir_cpcodi)  then  max(sgd_dir_tipo)
            when max(sgd_dir_tipo)<max(sgd_dir_cpcodi)  then  max(sgd_dir_cpcodi)
            when max(sgd_dir_tipo)=max(sgd_dir_cpcodi)  then  max(sgd_dir_cpcodi)
            end num from sgd_dir_drecciones	where  sgd_anex_codigo= '$codigo'";
	
	$rs = $db->query ( $isql );
	if (! $rs->EOF) {
			$NUM = $rs->fields ["NUM"];
			if($NUM<700)
			   	    $NUM=0;
		    $num_anexos = substr ( $NUM, 1, 2 );
	}
	//$cpcodi = $code;
	$nurad = $radi;
	$num_anexos = $num_anexos + 1;
	$str_num_anexos = substr ( "00$num_anexos", - 2 );
	$sgd_dir_tipo = "7$str_num_anexos";
	$radpa=substr($codigo,0,14);
	$isql = "update  sgd_dir_drecciones set  SGD_DIR_TIPO=$sgd_dir_tipo,radi_nume_radi=$radpa
           	where  sgd_anex_codigo='$codigo' and sgd_dir_cpcodi=$tipo ";
	$rs = $db->query ( $isql );
	echo "<input type='checkbox' checked  name='$nameInput' value=''  id='$nameInput' checked onclick=\"HaboDES('$codigo','$tipo','$name','$nameInput','desX')\" >";
}

if ($_GET ['action'] == 'desX') {
	hab_des_copia ( $db, $codigo, $tipo, 0 );
	$radpa=substr($codigo,0,14);
	$isql = "update  sgd_dir_drecciones set  SGD_DIR_TIPO=0,radi_nume_radi=$radpa
           	where  sgd_anex_codigo='$codigo' and sgd_dir_cpcodi=$tipo ";
	$rs = $db->query ( $isql );
	echo "<input type='checkbox'   name='$nameInput' value=''  id='$nameInput' onclick=\"HaboDES('$codigo','$tipo','$name','$nameInput','habX')\" >";
}

if ($_POST ['action'] == 'crearInfo') {
	 $nurad=$_POST['numrad'];
	$depes=$_POST['depes'];
	//ptint_r($_POST);
	$radicados[0]=$nurad;
	//$db->conn->debug=true;
	include "$ruta_raiz/include/tx/Tx.php";
	$Tx = new Tx($db);
		 //$sqldepe="select * from sgd_urd_usuaroldep  where  rol_codi =1 and depe_codi in ($depes)";
		 $sqldepe="select urd.usua_codi,u.usua_login,urd.DEPE_CODI from sgd_urd_usuaroldep urd,usuario u where  urd.rol_codi =1 and urd.depe_codi in ($depes) and urd.usua_codi=u.usua_codi";
		 $rs=$db->conn->query($sqldepe);
	 	$informar_rad = "Informar";
			
			//print_r($radicados);
    if (!$rs->EOF)
	{
		while (!$rs->EOF){
			$codUsDestino = $rs->fields['USUA_CODI'];
			$loginDestino = $rs->fields['USUA_LOGIN'];
			$depDestino = $rs->fields['DEPE_CODI'];
			//$observa = "(Se ha generado un anexo pero ha sido enviado a la dependencia $depDestino)";
			$observa = " Se informa de la radicación";
			$txSql = $Tx->informar( $radicados, $_SESSION['krd'],$depDestino,$_SESSION['dependencia'],$codUsDestino, $_SESSION['codusuario'],$observa,$_SESSION['id_rol'],$_SESSION['usua_doc'], $ruta_raiz);
			$rs->MoveNext();
		}
		mostraInfo($db,$nurad);
	}
	
	
}

if($_POST ['action'] =='vistaTipi'){
    
    //print_r($_POST);
    $depe=$_POST['depes'];
    $sqldepe="select m.sgd_mrd_codigo codemat,m.sgd_tpr_codigo codetp,t.sgd_tpr_descrip namea,sbrd.sgd_sbrd_descrip nsbrd,srd.sgd_srd_descrip nsrd
from sgd_mrd_matrird  m,sgd_tpr_tpdcumento t,sgd_sbrd_subserierd sbrd,sgd_srd_seriesrd srd
 where  m.depe_codi=$depe and m.sgd_mrd_esta_radi=1 and m.sgd_tpr_codigo=t.sgd_tpr_codigo and sbrd.sgd_sbrd_codigo=m.sgd_sbrd_codigo and srd.sgd_srd_codigo=m.sgd_srd_codigo and srd.sgd_srd_codigo=sbrd.sgd_srd_codigo
        order by t.sgd_tpr_descrip asc";
  //  $db->conn->debug=true;
    $option='';
      $rs=$db->conn->query($sqldepe);
     if (!$rs->EOF)
	{
                     while (!$rs->EOF){
			$codeMat = $rs->fields['CODEMAT'];
		         $codeTp = $rs->fields['CODETP'];
			$nametp = $rs->fields['NAMEA'];
                        $namesbrd = $rs->fields['NSBRD'];
                        //$namesrd = $rs->fields['NSRD'];
                        $option.="<option value=\"['$codeMat','$codeTp']\" >$nametp ($namesbrd)</option>";
                        $rs->MoveNext();
                     }
	}
        
    echo "<table style='width: 100%'><tr><td style='width: 26%' align='right' class='titulos5'>
        <font  face='Arial, Helvetica, sans-serif' class='etextomenu'>Tipo de Documento</font>
          </td><td style='width: 74%'><select class='select' id='tipiSel' name='tipiSel' onchange='changeTp()'>
          <option value='0'>-- Selecione tipo de documento --</option>
<option value=0>No Definido</option>$option</select></td></tr></table>";
    echo "<input type='hidden' id='codeMatriz' name='codeMatriz' >";
    echo "<input type='hidden' id='codeTp' name='codeTp' >";
}
if ($_POST ['action'] == 'Infoborrar') {
    $nurad=$_POST['numrad'];
	$depbrr=$_POST['depes'];
	$sqldepe="select urd.usua_codi,u.usua_login,urd.DEPE_CODI from sgd_urd_usuaroldep urd,usuario u where  urd.rol_codi =1 and urd.depe_codi =$depbrr and urd.usua_codi=u.usua_codi";
		 $rs=$db->conn->query($sqldepe);
		 if (!$rs->EOF)
	{
			$codUsDestino = $rs->fields['USUA_CODI'];
		    $loginDestino = $rs->fields['USUA_LOGIN'];
			$depDestino = $rs->fields['DEPE_CODI'];
	}
    //$db->conn->debug=true;
    include_once($ruta_raiz."/include/tx/Historico.php");
    $hist = new Historico($db);
	$flag=0;
	$observa = "Se borro de Inf. ( ".$loginDestino.")";
	$radicadosSel [0] = $nurad;
	$nombTx = "Borrar Informados";
	$codTx = 7;
	//$txSql = $rs->borrarInformado( $radicadosSel, $krd,$depbrr,$dependencia,$usCodSelect, $codusuario);
	$isql_inf= "delete from informados where depe_codi=$depbrr and radi_nume_radi=$nurad and id_rol  =1";
	$rs=$db->conn->query($isql_inf);
	//$hist->insertarHistorico($radicadosSel,$dependencia,$codusuario,$id_rol,$coddepe,$radi_usua_actu,$id_rol,$observa, $codTx);
	  $hist->insertarHistorico($radicadosSel,$_SESSION['dependencia'],$_SESSION['codusuario'],$_SESSION['id_rol'],$depbrr,$codUsDestino,1, $observa,$codTx);
	         
		if($flag==1)
		{
			echo "No se ha borrado la inf. de la dependencia $depbrr<br>";
		}else
		{
			echo "<font color=red>Se ha borrado un Inf. y registrado en eventos<br></font><br>";
			
		}
	mostraInfo($db,$nurad);
	
}
if ($_POST ['action'] == 'viewInfo') {
	    $nurad=$_POST['numrad'];
	
		mostraInfo($db,$nurad);
}


function hab_des_copia($db, $codigo, $tpcodi, $tipo, $des = 0) {
	//$db->conn->debug=true;
	$radpa=substr($codigo,0,14);
	if ($des == 1) {
		$radicado_rem = $tipo;
		$isql2 = "update anexos set sgd_rem_destino='$radicado_rem',sgd_dir_tipo='$radicado_rem' where anex_codigo='$codigo'";
		$rs = $db->query ( $isql2 );
		$isql3 = "select sgd_dir_tipo, sgd_dir_cpcodi
					from sgd_dir_drecciones
					where
					(sgd_dir_tipo = '7' or sgd_dir_tipo = '3' or sgd_dir_tipo = '2' or sgd_dir_tipo = '1')  and sgd_anex_codigo='$codigo'";
		$rs2 = $db->query ( $isql3 );
		$rs2->fields ["SGD_DIR_CPCODI"];
		if ($rs2->fields ["SGD_DIR_CPCODI"]) {
			$isql = "update  sgd_dir_drecciones set  SGD_DIR_TIPO=0,radi_nume_radi=$radpa
           	where  sgd_anex_codigo='$codigo' and sgd_dir_cpcodi=" . $rs2->fields ['SGD_DIR_CPCODI'];
			$rs = $db->query ( $isql );
		
		}
	
	}
	$isql = "update  sgd_dir_drecciones set  SGD_DIR_TIPO=$tipo,radi_nume_radi=$radpa
           	where  sgd_anex_codigo='$codigo' and sgd_dir_cpcodi=$tpcodi ";
	$rs = $db->query ( $isql );

}



if ($_SESSION ['usua_debug'] == 1) {
	echo "</span >";
}
/*?><script> alert('hola');</script> */



function mostraInfo($db,$nurad) {
	
	 $query2 = "select b.depe_nomb
					,a.INFO_DESC
					,b.DEPE_NOMB
					,a.DEPE_CODI
					,a.info_fech as INFO_FECH
					,INFO_DESC
					from informados a,dependencia b
					where a.depe_codi=b.depe_codi and cast(a.radi_nume_radi as varchar)='$nurad'
					order by info_fech desc ";
	$k = 1;
	$rs=$db->conn->query($query2);
	
	if (!$rs->EOF)
	{
		echo "<table>";
		while (!$rs->EOF){
			$data = $rs->fields['INFO_DESC'];
			$data2 = $rs->fields['DEPE_NOMB'];
			$data3 = $rs->fields['DEPE_CODI'];
			$data4 = date("dMy",$rs->fields['INFO_FECH']);
			// Modificado SGD 11-Jul-2007
			//$data5 = date("d-m-Y",$rs->fields['INFO_FECH']);
			$data5 = date( "d-m-Y", $db->conn->UnixTimeStamp( $rs->fields['INFO_FECH'] ) );
			$data6 = $db->conn->UnixTimeStamp( $rs->fields['INFO_DESC'] );
			?>
			<tr  class='listado50'>
			<td><input type='radio' name='borrarradicado' id='borrarradicado' value='<?php echo $data3?>' onclick='borrarInf()'></td>
			<td class="listado50"><b><?php echo $data?></td><td class="listado50"> <center><?php echo $data2?></td><td class="listado50"><?php echo $data5?></td></tr>
			<?php 
			$k = $k +1;
			$rs->MoveNext();
		}
		echo "</table>";
	}
	else {
	echo "No hay informados";
	}
	
}
