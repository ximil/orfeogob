<?php
/*************************************************************************************/
/* ORFEO GPL:Sistema de Gestion Documental		http://www.orfeogpl.org	     */
/*	Idea Original de la SUPERINTENDENCIA DE SERVICIOS PUBLICOS DOMICILIARIOS     */
/*				COLOMBIA TEL. (57) (1) 6913005  orfeogpl@gmail.com   */
/* ===========================                                                       */
/*                                                                                   */
/* Este programa es software libre. usted puede redistribuirlo y/o modificarlo       */
/* bajo los terminos de la licencia GNU General Public publicada por                 */
/* la "Free Software Foundation"; Licencia version 2. 			             */
/*                                                                                   */
/* Copyright (c) 2005 por :	  	  	                                     */
/* SSPS "Superintendencia de Servicios Publicos Domiciliarios"                       */
/*   Jairo Hernan Losada  jlosada@gmail.com                Desarrollador             */
/*   Sixto Angel Pinzón López --- angel.pinzon@gmail.com   Desarrollador             */
/* C.R.A.  "COMISION DE REGULACION DE AGUAS Y SANEAMIENTO AMBIENTAL"                 */
/*   Liliana Gomez        lgomezv@gmail.com                Desarrolladora            */
/*   Lucia Ojeda          lojedaster@gmail.com             Desarrolladora            */
/* D.N.P. "Departamento Nacional de Planeación"                                      */
/*   Hollman Ladino       hollmanlp@gmail.com                Desarrollador          */
/*                                                                                   */
/* Colocar desde esta lInea las Modificaciones Realizadas Luego de la Version 3.5    */
/*  Nombre Desarrollador   Correo     Fecha   Modificacion                           */
/*************************************************************************************/
$ruta_raiz2=$ruta_raiz;
$ruta_raiz="..";
//Modificacion de carga de log
if(isset($_SERVER['HTTP_X_FORWARD_FOR'])){
    $proxy=$_SERVER['HTTP_X_FORWARD_FOR'];
}else
    $proxy=$_SERVER['REMOTE_ADDR'];
$REMOTE_ADDR=$_SERVER['REMOTE_ADDR'];
include_once "$ruta_raiz/core/clases/log.php";
$log = new log($ruta_raiz);
$log->setAddrC($REMOTE_ADDR);
$log->setProxyAd($proxy);
//Guarda en el log la consulta de información general del radicado
$log->setUsuaCodi($_SESSION['codusuario']);
$log->setDepeCodi($_SESSION['dependencia']);
$log->setRolId($_SESSION['id_rol']);
$log->setDenomDoc($doc='Radicado');
$log->setAction('document_view');
$log->setOpera('Consulta Información general del radicado');
$log->setNumDocu($_GET['verrad']);
$log->registroEvento();
$ruta_raiz=$ruta_raiz2;
//Carga de info
include_once "class_control/AplIntegrada.php";
include_once "../core/config/config-inc.php";
$objApl = new AplIntegrada($db);
$lkGenerico = "&usuario=$krd&nsesion=".trim(session_id())."&nro=$verradicado"."$datos_envio";
?>
<script src="js/popcalendar.js"></script>
<script>
function regresar()
{	//window.history.go(0);
	window.location.reload();
}
</script>
<table  style='width: 100%; border-spacing:  0' class="borde_tab" bgcolor="#006699" >
<tr bgcolor="#006699">
	<td class="titulos4" colspan="6" >INFORMACION GENERAL </td>
</tr>
</table>
<table  style='width: 100%; border-spacing:  1;border-collapse: separate' align="left" border='0' class="borde_tab" id=tb_general>
<tr>
	<td align="right" bgcolor="#CCCCCC" style='width: 20' height="25" class="titulos2" >FECHA DE RADICADO</td>
    <td   height="25" class="listado2"><?php echo $radi_fech_radi ?></td>
    <td bgcolor="#CCCCCC" width="20" align="right" height="25" class="titulos2" >ASUNTO</td>
    <td class='listado2' colspan="3" width="25%"><?php echo $ra_asun ?></td>
</tr>
<tr>
	<td align="right" bgcolor="#CCCCCC" height="25" class="titulos2"><?php echo $tip3Nombre[1][$ent]?></td>
	<td class='listado2' width="25%" height="25"><?php echo $nombret_us1 ?>-- <?php echo $cc_documento_us1?></td>
	<td bgcolor="#CCCCCC" width="20" align="right" height="25" class="titulos2" >DIRECCI&Oacute;N CORRESPONDENCIA</td>
	<td class='listado2' width="25%"><?php echo $direccion_us1 ?></td>
	<td bgcolor="#CCCCCC" width="20" align="right" height="25" class="titulos2" >UBICACIÓN</td>
	<td class='listado2' width="25%"><?php echo $dpto_nombre_us1."/".$muni_nombre_us1 ?></td>
</tr>
<?php if($vista_predio==1){ ?>
<tr>
	<td align="right" bgcolor="#CCCCCC" height="25" class="titulos2"><?php echo $tip3Nombre[2][$ent]?></td>
	<td class='listado2' width="25%" height="25"> <?php echo $nombret_us2 ?></td>
    <td bgcolor="#CCCCCC" width="20" align="right" height="25" class="titulos2">DIRECCI&Oacute;N CORRESPONDENCIA </td>
    <td class='listado2' width="25%"> <?php echo $direccion_us2 ?></td>
    <td bgcolor="#CCCCCC" width="20" align="right" height="25" class="titulos2">MUN/DPTO</td>
    <td class='listado2' width="25%"> <?php echo $dpto_nombre_us2."/".$muni_nombre_us2 ?></td>
</tr>
<?php } if($vista_entidad==1){?>
<tr>
	<td align="right" bgcolor="#CCCCCC" height="25" class="titulos2"><?php echo $tip3Nombre[3][$ent]?></td>
	<td class='listado2' width="25%" height="25"> <?php echo $nombret_us3 ?> -- <?php echo $cc_documento_us3?></td>
    <td bgcolor="#CCCCCC" width="20" align="right" height="25" class="titulos2">DIRECCI&Oacute;N CORRESPONDENCIA </td>
    <td class='listado2' width="25%"> <?php echo $direccion_us3 ?></td>
    <td bgcolor="#CCCCCC" width="20" align="right" height="25" class="titulos2">MUN/DPTO</td>
    <td class='listado2' width="25%"> <?php echo $dpto_nombre_us3."/".$muni_nombre_us3 ?></td>
</tr>
<?php }?>
<tr>
	<td height="20" bgcolor="#CCCCCC" align="right" class="titulos2"> <p>FOLIOS F&Iacute;SICO</p></td>
    <td class='listado2' width="25%" height="25"> <?php echo $radi_nume_hoja ?></td>
    <td bgcolor="#CCCCCC" width="20" height="25" align="right" class="titulos2"> DESCRIPCI&Oacute;N ANEXOS </td>
    <td class='listado2'  width="25%" height="11"> <?php echo $radi_desc_anex ?></td>
    <td bgcolor="#CCCCCC" width="20" align="right" height="25" class="titulos2">FOLIOS DIGITALIZADOS</td>
    <td class='listado2' width="25%"><?php echo $radi_nume_folios ?></td>
</tr>
<?php //$factura=$objApl->datosFactura($_GET['verrad']);
if($id_metadato!=0){
$meta=$objApl->infoMetadata($id_metadato,$_GET['verrad']);
if($meta['error']==""){
    $cont=count($meta)-1;
    for($i=0;$i<$cont;$i++){
	if($i%3==0){
	    echo "<tr>";
	}
	?>
	<td height="20" bgcolor="#CCCCCC" align="right" class="titulos2"><p><?php echo $meta[$i]['etiqueta']?></p></td>
        <td class='listado2'> <?php echo $meta[$i]['valor']?></td>
	<?php 	$m=($i+1)%3;
	if($m==0){
	    echo "</tr>";
	}
    }
    if($m==2){
	?>
	<td height="20" bgcolor="#CCCCCC" align="right" class="titulos2"><p></p></td>
        <td class='listado2'></td></tr>
	<?php     }
    if($m==1){
	?>
	<td height="20" bgcolor="#CCCCCC" align="right" class="titulos2"><p></p></td>
        <td class='listado2'></td>
        <td height="20" bgcolor="#CCCCCC" align="right" class="titulos2"><p></p></td>
        <td class='listado2'></td></tr>
        <?php     }
}
}
?>
<tr>
	<td align="right" bgcolor="#CCCCCC" height="20" class="titulos2">DOCUMENTO<br>Anexo/Asociado</td>
	<td class='listado2' width="25%" height="25">
	<?php 	if($radi_tipo_deri!=1 and $radi_nume_deri)
		{	echo $radi_nume_deri;
		    /*
		     * Modificacion acceso a documentos
		     * @author Liliana Gomez Velasquez
		     * @since octubre 5 2009
		     */
		    $resulVali=$verLinkArchivo->valPermisoRadi($radi_nume_deri);

            $verImg = $resulVali['verImg'];
			/* @todo check permissions */
		    echo "<br>(<a class='vinculos' href='".$url_orfeo_power_tools."/document/".$radi_nume_deri."/download' target=\"_blank\">Ver Datos</a>)";


		}
		if($verradPermisos == "Full" or $datoVer=="985")
		{
	?>
		<input type=button name=mostrar_anexo value='...' class=botones_2 onClick="verVinculoDocto();">
	<?php
		}
	?>
	</td>
    <td bgcolor="#CCCCCC" width="20" align="right" height="25" class="titulos2">REF/OFICIO/CUENTA INTERNA </td>
    <td class='listado2' width="25%"> <?php echo $cuentai ?>&#160;&#160;&#160;&#160;&#160;
    <?php
		$muniCodiFac = "";
		$dptoCodiFac = "";
		if($sector_grb==6 and $cuentai and $espcodi)
		{	if($muni_us2 and $codep_us2)
			{	$muniCodiFac = $muni_us2;
				$dptoCodiFac = $codep_us2;
			}
			else
			{	if($muni_us1 and $codep_us1)
				{	$muniCodiFac = $muni_us1;
					$dptoCodiFac = $codep_us1;
				}
			}
	?>
		<a href="./consultaSUI/facturacionSUI.php?cuentai=<?php echo $cuentai?>&muniCodi=<?php echo $muniCodiFac?>&deptoCodi=<?php echo $dptoCodiFac?>&espCodi=<?php echo $espcodi?>" target="FacSUI<?php echo $cuentai?>"><span class="vinculos">Ver Facturacion</span></a>
	<?php
		}
	?>
    </td>
	<td bgcolor="#CCCCCC" width="20" align="right" height="25" class="titulos2">CANTIDAD</td>
	<td class='listado2' colspan="3" width="25%">FOLIOS: <?php echo $radi_nume_folio;?> &nbsp;&nbsp;&nbsp; ANEXOS: <?php echo $radi_nume_anexo;?></td>
  </tr>
  <tr>
	<td align="right" height="20" class="titulos2">IMAGEN</td>
	<td class='listado2' colspan="1"><span class='vinculos'><?php echo $imagenv ?></span></td>
	<!--<td align="right" height="20"  class="titulos2">ID KOGUI</td>
	<td class='listado2' >&nbsp;&nbsp;&nbsp;-->
            <?php //echo $numInterno;
            //if(!$numInterno) {
            //$varEnvio = "krd=$krd&numRad=$verrad&numInterno=$numInterno"; ?> &nbsp;&nbsp;&nbsp;
         <!--<input type='button' name='numInternoB' value='...' class='botones_2' onClick="window.open('<?php //=$ruta_raiz?>/radicacion/numInterno.php?<?php //=$varEnvio?>','Cambio Nivel de Seguridad Radicado', 'height=220, width=300,left=350,top=300')">-->
            <?php //} ?>
	<!--</td>
	<td align="right" height="20"  class="titulos2">Nivel de Seguridad</td>-->
	<td bgcolor="#CCCCCC" width="20" align="right" height="25" class="titulos2">GUIA</td>
	<td class='listado2'  width="25%"><?php echo $radi_nume_guia;?></td>
	<td bgcolor="#CCCCCC" width="20" align="right" height="25" class="titulos2">Nivel de Seguridad</td>
	<td class='listado2' colspan="2">
	<?php
		if($nivelRad==1)
		{	echo "Privado";	}
		else
		{	echo "P&uacute;blico";	}
		if($verradPermisos == "Full" or $datoVer=="985")
	  	{	$varEnvio = "krd=$krd&numRad=$verrad&nivelRad=$nivelRad";
	?>
		<input type='button' name='mostrar_causal' value='...' class='botones_2' onClick="window.open('<?php echo $ruta_raiz?>/seguridad/radicado.php?<?php echo $varEnvio?>','Cambio Nivel de Seguridad Radicado', 'height=220, width=300,left=350,top=300')">
	<?php
		}
	?>
	</td>
</tr>
<tr>
	<td align="right" height="25" class="titulos2">TRD</td>
	<td class='listado2' colspan="6">
	<?php
		if(!$codserie) $codserie = "0";
		if(!$tsub) $tsub = "0";
		if(trim($val_tpdoc_grbTRD)=="///") $val_tpdoc_grbTRD = "";
	?>
		<?php echo $serie_nombre ?><font color=black>/</font><?php echo $subserie_nombre ?><font color=black>/</font><?php echo $tpdoc_nombreTRD ?>
	<?php
		if($verradPermisos == "Full" or $datoVer=="985" or $krd=='WQUIJANO' ) {
                    if( $krd=='WQUIJANO'  ) {
	?><input type=button name="mosrtar_tipo_doc2" value='...' class="botones_2" onClick="ver_tipodocuTRD2(<?php echo $codserie?>,<?php echo $tsub?>);">
        <?php }else{
        ?><input type=button name="mosrtar_tipo_doc2" value='...' class="botones_2" onClick="ver_tipodocuTRD(<?php echo $codserie?>,<?php echo $tsub?>);">
	<?php }?>
        </td>
</tr>
<?php if($vista_sector==1){?>
  <tr>

    <td align="right" height="25" class="titulos2">SECTOR</td>
    <td class='listado2' colspan="6">
      <?php echo $sector_nombre?>
      <?php
		$nombreSession = session_name();
		$idSession = session_id();
		if ($verradPermisos == "Full"  or $datoVer=="985") {
	  		$sector_grb = (isset($sector_grb)) ? $sector_grb : 0;
	  		$causal_grb = (isset($causal_grb) ||$causal_grb !='') ? $causal_grb : 0;
	  		$deta_causal_grb = (isset($deta_causal_grb) || $deta_causal_grb!='') ? $deta_causal_grb : 0;

			$datosEnviar = "'$ruta_raiz/causales/mod_causal.php?" .
											$nombreSession . "=" . $idSession .
											"&krd=" . $krd .
											"&verrad=" . $verrad .
											"&sector=" . $sector_grb .
											"&sectorCodigoAnt=" . $sector_grb .
											"&sectorNombreAnt=" . $sector_nombre .
											"&causal_grb=" . $causal_grb .
											"&causal_nombre=" . $causal_nombre .
											"&deta_causal_grb=" . $deta_causal_grb .
											"&dcausal_nombre=". $dcausal_nombre . "'";
	  ?>
      <input type=button name="mostrar_causal" value="..." class="botones_2" onClick="window.open(<?php echo $datosEnviar?>,'Tipificacion_Documento','height=300,width=750,scrollbars=no')">
      <input type="hidden" name="mostrarCausal" value="N">
      <?php
	   }
	   ?>
    </td>
  </tr>
  <?php } if($vista_causal==1){?>
  <tr>
    <td align="right" height="25" class="titulos2">CAUSAL</td>
    <?php
	$causal_nombre_grb = $causal_nombre;
	$dcausal_nombre_grb = $dcausal_nombre;
	?>
    <td class='listado2' colspan="6">
      <?php echo $causal_nombre ?>
      /
      <?php echo $dcausal_nombre ?>
      /
      <?php echo $ddcausal_nombre ?>
      /
      <?php
	  if ($verradPermisos == "Full"  or $datoVer=="985" ) {
	  ?>
      	<input type=button name="mostrar_causal" value="..." class='botones_2' onClick="window.open(<?php echo $datosEnviar?>,'Tipificacion_Documento','height=300,width=750,scrollbars=no')">
      <?php
	  }
	  ?>
    </td>
  </tr>
  <?php }
  if($vista_tema==1){
  ?>
  <tr>
    <td align="right" height="25" class="titulos2">TEMA</td>
    <td class='listado2' colspan="6">
      <?php echo $tema_nombre ?>
      <?php
	  if ($verradPermisos == "Full"  or $datoVer=="985") {
	  ?>
      <input type=button name="mostrar_temas" value='...' class=botones_2 onClick="ver_temas();">
      <?php
	  }
}
	  ?>
    </td>
  </tr>
  <?php } ?>
</table>
</form>
<table align="center" border=0 id=ver_datos witdth=80%>
<tr><td>
<?php
 $ruta_raiz = ".";
 error_reporting(7);
 if($verradPermisos=="Full" or $datoVer=="985") {
 	//include ("tipo_documento.php");
 }
?>
</td></tr>
<tr><td align='center'>
<?php
 // <input type=button name=mod_tipo_doc3 value='Ver datos' class=botones_2 onClick="ver_datos();">
?>
</td></tr>
</table>
