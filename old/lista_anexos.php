<?php session_start();
date_default_timezone_set('America/Bogota');
date_default_timezone_set('America/Bogota');
//empieza Anexos   por  Julian Rolon
//lista los documentos del radicado y proporciona links para ver historicos de cada documento
//este archivo se incluye en la pagina verradicado.php
//print ("uno");
if (!$ruta_raiz)
    $ruta_raiz = ".";
include_once("$ruta_raiz/conf/VariablesGlobales.php");
include_once("$ruta_raiz/class_control/anexo.php");
include_once "$ruta_raiz/include/db/ConnectionHandler.php";
require_once("$ruta_raiz/class_control/TipoDocumento.php");
include_once "$ruta_raiz/class_control/firmaRadicado.php";
include "$ruta_raiz/config.php";
require_once("$ruta_raiz/class_control/ControlAplIntegrada.php");
require_once("$ruta_raiz/class_control/AplExternaError.php");

$db = new ConnectionHandler (".");

$objTipoDocto = new TipoDocumento ($db);
$objTipoDocto->TipoDocumento_codigo($tdoc);
$objFirma = new FirmaRadicado ($db);
$objCtrlAplInt = new ControlAplIntegrada ($db);
$serv_gendoc = 'servprodoc.imprenta.gov.co';
//if (!$db)
//$db2 = new ConnectionHandler(".");


//$db2->conn->debug = true;


//$db->conn->SetFetchMode(ADODB_FETCH_NUM);
$db->conn->SetFetchMode(ADODB_FETCH_ASSOC);

//$db2->conn->SetFetchMode(ADODB_FETCH_ASSOC);
$num_archivos = 0;

$anex = &new Anexo ($db);
$sqlFechaDocto = $db->conn->SQLDate("Y-m-D H:i:s A", "sgd_fech_doc");
$sqlFechaAnexo = $db->conn->SQLDate("Y-m-D H:i:s A", "anex_fech_anex");
//$sqlFechaAnexo = "to_char(anex_fech_anex, 'YYYY/DD/MM HH:MI:SS')";
$sqlSubstDesc = $db->conn->substr . "(anex_desc, 0, 50)";
include_once("include/query/busqueda/busquedaPiloto1.php");
$ssql2 = "select sgd_trad_codigo as id ,lower(sgd_trad_descr) as descr from sgd_trad_tiporad";
$rs23 = $db->conn->Execute($ssql2);

$i = 0;
while (!$rs23->EOF) {
    $arrtrad[$i]['id'] = $rs23->fields['ID'];
    $arrtrad[$i]['desc'] = $rs23->fields['DESCR'];
    $i++;
    $rs23->MoveNext();
}
// Modificado SGD 06-Septiembre-2007
$isql = "select anex_codigo AS DOCU
            ,anex_tipo_ext AS EXT
			,anex_tamano AS TAMA
			,anex_solo_lect AS RO
            ,usua_nomb AS CREA
			,$sqlSubstDesc AS DESCR
			,anex_nomb_archivo AS NOMBRE
			,ANEX_CREADOR
			,ANEX_ORIGEN
			,ANEX_SALIDA
			,$radi_nume_salida as RADI_NUME_SALIDA
			,ANEX_ESTADO
			,SGD_PNUFE_CODI
			,SGD_DOC_SECUENCIA
			,SGD_DIR_TIPO
			,SGD_DOC_PADRE
			,anexos.SGD_TPR_CODIGO
			,SGD_APLI_CODI
			,SGD_TRAD_CODIGO
			,anexos.SGD_TPR_CODIGO
			,SGD_TPR_DESCRIP
			,ANEX_TIPO
			,ANEX_FECH_ANEX
                        ,anex_folios_dig AS PAG
			,$sqlFechaDocto as FECDOC
			,$sqlFechaAnexo as FEANEX
			,ANEX_TIPO as NUMEXTDOC
		   from anexos left outer join sgd_tpr_tpdcumento on (anexos.SGD_TPR_CODIGO=sgd_tpr_tpdcumento.SGD_TPR_CODIGO), anexos_tipo,usuario
           where anex_radi_nume=$verrad and anex_tipo=anex_tipo_codi
		   and anex_creador=usua_login and anex_borrado='N'
	   order by anex_codigo,radi_nume_salida, sgd_dir_tipo, anex_numero ";
error_reporting(7);
?>
<script src="<?php echo $ruta_raiz ?>/js/jquery-1.10.2.js"></script>
<script>
    swradics = 0;
    radicando = 0;
    function verDetalles(anexo, tpradic, aplinteg, num) {
        optAsigna = "";
        if (swradics == 0) {
            optAsigna = "&verunico=1";
        }
        contadorVentanas = contadorVentanas + 1;
        nombreventana = "ventanaDetalles" + contadorVentanas;
        url = "detalle_archivos.php?usua=<?php echo $krd?>&radi=<?php echo $verrad?>&anexo=" + anexo;
        url = "<?php echo $ruta_raiz?>/nuevo_archivo.php?codigo=" + anexo + "&<?php echo session_name() . "=" . trim(session_id())?>&usua=<?php echo $krd?>&numrad=<?php echo $verrad?>&contra=<?php echo $drde?>&radi=<?php echo $verrad?>&tipo=<?php echo $tipo?>&ent=<?php echo $ent?><?php echo $datos_envio?>&ruta_raiz=<?php echo $ruta_raiz?>" + "&tpradic=" + tpradic + "&aplinteg=" + aplinteg + optAsigna;
        window.open(url, nombreventana, 'top=0,height=580,width=640,scrollbars=yes,resizable=yes');
        return;
    }
    function borrarArchivo(anexo, linkarch, radicar_a, procesoNumeracionFechado) {
        if (confirm('Estas seguro de borrar este archivo anexo ?')) {
            contadorVentanas = contadorVentanas + 1;
            nombreventana = "ventanaBorrar" + contadorVentanas;
            //url="borrar_archivos.php?usua=<?php echo $krd?>&contra=<?php echo $drde?>&radi=<?php echo $verrad?>&anexo="+anexo+"&linkarchivo="+linkarch;

            url = "lista_anexos_seleccionar_transaccion.php?borrar=1&usua=<?php echo $krd?>&numrad=<?php echo $verrad?>&&contra=<?php echo $drde?>&radi=<?php echo $verrad?>&anexo=" + anexo + "&linkarchivo=" + linkarch + "&numfe=" + procesoNumeracionFechado + "&dependencia=<?php echo $dependencia?>&codusuario=<?php echo $codusuario?>";
            window.open(url, nombreventana, 'height=100,width=180');
        }
        return;
    }
    function radicarArchivo(anexo, linkarch, radicar_a, procesoNumeracionFechado, tpradic, aplinteg, numextdoc) {
        if (radicando > 0) {
            alert("Ya se esta procesando una radicacion, para re-intentarlo hagla click sobre la pestaña de documentos");
            return;
        }

        radicando++;

        if (confirm('Se asignar\xe1 un n\xfamero de radicado a \xe9ste documento. Est\xe1 seguro  ?')) {
            contadorVentanas = contadorVentanas + 1;
            nombreventana = "mainFrame";

            url = "<?php echo $ruta_raiz?>/lista_anexos_seleccionar_transaccion.php?radicar=1&radicar_a=" + radicar_a + "&vp=n&<?php echo "&" . session_name() . "=" . trim(session_id())?>&radicar_documento=<?php echo $verrad?>&numrad=<?php echo $verrad?>&anexo=" + anexo + "&linkarchivo=" + linkarch + "<?php echo $datos_envio?>" + "&ruta_raiz=<?php echo $ruta_raiz?>&numfe=" + procesoNumeracionFechado + "&tpradic=" + tpradic + "&aplinteg=" + aplinteg + "&numextdoc=" + numextdoc;
            window.open(url, nombreventana, 'height=450,width=600');
        }
        return;
    }


    function numerarArchivo(anexo, linkarch, radicar_a, procesoNumeracionFechado) {
        if (confirm('Se asignar\xe1 un n\xfamero a \xe9ste documento. Est\xe1 seguro ?')) {
            contadorVentanas = contadorVentanas + 1;
            nombreventana = "mainFrame";
            url = "<?php echo $ruta_raiz?>/lista_anexos_seleccionar_transaccion.php?numerar=1" + "&vp=n&<?php echo "krd=$krd&" . session_name() . "=" . trim(session_id())?>&radicar_documento=<?php echo $verrad?>&numrad=<?php echo $verrad?>&anexo=" + anexo + "&linkarchivo=" + linkarch + "<?php echo $datos_envio?>" + "&ruta_raiz=<?php echo $ruta_raiz?>&numfe=" + procesoNumeracionFechado;
            window.open(url, nombreventana, 'height=450,width=600');
        }
        return;
    }


    function asignarRadicado(anexo, linkarch, radicar_a, numextdoc) {

        if (radicando > 0) {
            alert("Ya se esta procesando una radicacion, para re-intentarlo hagla click sobre la pesta�a de documentos");
            return;
        }

        radicando++;

        if (confirm('Esta seguro de asignarle el numero de Radicado a este archivo ?')) {
            contadorVentanas = contadorVentanas + 1;
            nombreventana = "mainFrame";
            url = "<?php echo $ruta_raiz?>/genarchivo.php?generar_numero=no&radicar_a=" + radicar_a + "&vp=n&<?php echo "&" . session_name() . "=" . trim(session_id())?>&radicar_documento=<?php echo $verrad?>&numrad=<?php echo $verrad?>&anexo=" + anexo + "&linkarchivo=" + linkarch + "<?php echo $datos_envio?>" + "&ruta_raiz=<?php echo $ruta_raiz?>" + "&numextdoc=" + numextdoc;
            window.open(url, nombreventana, 'height=450,width=600');
        }
        return;
    }
    function ver_tipodocuATRD(anexo, codserie, tsub) {
        <?php        $isqlDepR = "SELECT RADI_DEPE_ACTU,RADI_USUA_ACTU from radicado
		            WHERE RADI_NUME_RADI = '$numrad'";
        $rsDepR = $db->conn->Execute($isqlDepR);
        $coddepe = $rsDepR->fields ['RADI_DEPE_ACTU'];
        $codusua = $rsDepR->fields ['RADI_USUA_ACTU'];
        $ind_ProcAnex = "S";
        ?>
        window.open("./radicacion/tipificar_documento.php?krd=<?php echo $krd?>&nurad=" + anexo + "&ind_ProcAnex=<?php echo $ind_ProcAnex?>&codusua=<?php echo $codusua?>&coddepe=<?php echo $coddepe?>&tsub=" + tsub + "&codserie=" + codserie + "&texp=<?php echo $texp?>", "Tipificacion_Documento_Anexos", "height=500,width=750,scrollbars=yes");
    }


    function ver_tipodocuAnex(cod_radi, codserie, tsub) {

        window.open("./radicacion/tipificar_anexo.php?krd=<?php echo $krd?>&nurad=" + cod_radi + "&ind_ProcAnex=<?php echo $ind_ProcAnex?>&codusua=<?php echo $codusua?>&coddepe=<?php echo $coddepe?>&tsub=" + tsub + "&codserie=" + codserie, "Tipificacion_Documento_Anexos", "height=300,width=750,scrollbars=yes");
    }

    /*function docDefinitivo(cod_radi){
     window.open("<?php echo $ruta_raiz?>/uploadFiles/definitivo.php?krd=<?php echo $krd?>&<?php echo session_name()?>=<?php echo session_id()?>&numeradi="+cod_radi,"DocuDefinitivo","height=300,width=750,scrollbars=yes");
     }*/


    function vistaPreliminar(anexo, linkarch, linkarchtmp) {
        contadorVentanas = contadorVentanas + 1;
        nombreventana = "mainFrame";
        url = "<?php echo $ruta_raiz?>/genarchivo.php?vp=s&<?php echo "krd=$krd&" . session_name() . "=" . trim(session_id())?>&radicar_documento=<?php echo $verrad?>&numrad=<?php echo $verrad?>&anexo=" + anexo + "&linkarchivo=" + linkarch + "&linkarchivotmp=" + linkarchtmp + "<?php echo $datos_envio?>" + "&ruta_raiz=<?php echo $ruta_raiz?>";
        window.open(url, nombreventana, 'height=450,width=600');
        return;
    }
    function nuevoArchivo(asigna) {
        contadorVentanas = contadorVentanas + 1;
        optAsigna = "";
        if (asigna == 1) {
            optAsigna = "&verunico=1";
        }
//alert (asigna);

        nombreventana = "ventanaNuevo" + contadorVentanas;
        url = "<?php echo $ruta_raiz?>/nuevo_archivo.php?codigo=&<?php echo "krd=$krd&" . session_name() . "=" . trim(session_id())?>&usua=<?php echo $krd?>&numrad=<?php echo $verrad?>&contra=<?php echo $drde?>&radi=<?php echo $verrad?>&tipo=<?php echo $tipo?>&ent=<?php echo $ent?>" + "<?php echo $datos_envio?>" + "&ruta_raiz=<?php echo $ruta_raiz?>&tdoc=<?php echo $tdoc?>" + optAsigna;
        window.open(url, nombreventana, 'height=580,width=640,scrollbars=yes,resizable=yes');
        return;
    }

    function nuevoEditWeb(asigna) {
        contadorVentanas = contadorVentanas + 1;
        optAsigna = "";
        if (asigna == 1) {
            optAsigna = "&verunico=1";
        }
//alert (asigna);

        nombreventana = "ventanaNuevo" + contadorVentanas;
        url = "<?php echo $ruta_raiz?>/edicionWeb/editorWeb.php?codigo=&<?php echo "krd=$krd&" . session_name() . "=" . trim(session_id())?>&usua=<?php echo $krd?>&numrad=<?php echo $verrad?>&contra=<?php echo $drde?>&radi=<?php echo $verrad?>&tipo=<?php echo $tipo?>&ent=<?php echo $ent?>" + "<?php echo $datos_envio?>" + "&ruta_raiz=<?php echo $ruta_raiz?>&tdoc=<?php echo $tdoc?>" + optAsigna;
        window.open(url, nombreventana, 'height=800,width=700,scrollbars=yes,resizable=yes');
        return;
    }
    function Plantillas(plantillaper1) {
        if (plantillaper1 == 0) {
            plantillaper1 = "";
        }
        contadorVentanas = contadorVentanas + 1;
        nombreventana = "ventanaNuevo" + contadorVentanas;
        urlp = "plantilla.php?<?php echo "krd=$krd&" . session_name() . "=" . trim(session_id());?>&verrad=<?php echo $verrad?>&numrad=<?php echo $numrad?>&plantillaper1=" + plantillaper1;
        window.open(urlp, nombreventana, 'top=0,left=0,height=800,width=850');
        return;
    }
    function Plantillas_pb(plantillaper1) {
        if (plantillaper1 == 0) {
            plantillaper1 = "";
        }
        contadorVentanas = contadorVentanas + 1;
        nombreventana = "ventanaNuevo" + contadorVentanas;
        urlp = "crea_plantillas/plantilla.php?<?php echo "krd=$krd&" . session_name() . "=" . trim(session_id());?>&verrad=<?php echo $verrad?>&numrad=<?php echo $numrad?>&plantillaper1=" + plantillaper1;
        window.open(urlp, nombreventana, 'top=0,left=0,height=800,width=850');
        return;
    }

    function regresar() {
        //window.history.go(0);
        window.location.reload();
        //window.close();

    }

    function overWindow(script) {
        if ($("#lightbox").size() == 0) {
            var theLightbox = $('<div id="lightbox"/>');
            var theShadow = $('<div id="lightbox-shadow"/>');
            $('body').append(theShadow);
            $('body').append(theLightbox);
        }
        $("#lightbox").empty();
        $.get(script, function (data) {
            $("#lightbox").append(data);
        });
        $('#lightbox').css('top', 100 + 'px');
        // display the lightbox
        $('#lightbox').show();
        $('#lightbox-shadow').show();
    }

    function overWindowMini(script) {
        if ($("#lightbox").size() == 0) {
            var theLightbox = $('<div id="lightbox"/>');
            var theShadow = $('<div id="lightbox-shadow"/>');
            $('body').append(theShadow);
            $('body').append(theLightbox);
        }
        $("#lightbox").empty();
        $.get(script, function (data) {
            $("#lightbox").append(data);
        });
        //$(window).scrollTop();
        $('#lightbox').css('top', 70 + 'px');
        $('#lightbox').css('width', 30 + '%');
        $('#lightbox').css('height', 100 + 'px');
        $('#lightbox').css('left', 55 + '%');
        // display the lightbox
        $('#lightbox').show();
        $('#lightbox-shadow').show();
    }

    function overWindowFirma(script) {
        if ($("#lightbox").size() == 0) {
            var theLightbox = $('<div id="lightbox"/>');
            var theShadow = $('<div id="lightbox-shadow"/>');
            $('body').append(theShadow);
            $('body').append(theLightbox);
        }
        $("#lightbox").empty();
        $.get(script, function (data) {
            $("#lightbox").append(data);
        });
        $('#lightbox').css('top', 100 + 'px');
        $('#lightbox').css('width', 39 + '%');
        $('#lightbox').css('height', 445 + 'px');
        $('#lightbox').css('left', 50 + '%');
        // display the lightbox
        $('#lightbox').show();
        $('#lightbox-shadow').show();
    }

    function cerrar2() {
        $('#lightbox').hide();
        $('#lightbox-shadow').hide();
        $('#lightbox').empty();
        regresar();
    }

    function gendoc2(radi) {
        var tiporad =<?php echo json_encode($arrtrad)?>;
        var trad = radi.substr(-1);
        var ok = 0;
        var desctrad;
        for (var i = 0; i < tiporad.length; i++) {
            if (tiporad[i]['id'] == trad) {
                ok = 1;
                desctrad = tiporad[i]['desc'];
            }
        }
        if (ok == 0) {
            alert('ERROR: tipo de radicacion no valido ' + trad);
            return false;
        }

        var formData = "numrad=" + radi;
        $.ajax({
            type: "POST",
            url: "../core/Modulos/radicacion/vista/crearJson.php",
            data: formData,
            success: function (data) {
                var retorno = $(data).filter("#response").text();
                if (retorno.substr(0, 5) == 'ERROR') {
                    alert(retorno);
                }
                else {
                    window.location = 'http://<?php echo $serv_gendoc?>/app.php/plantilla/new/' + desctrad + '?documento=' + radi;
                }
            },
            error: function (e) {
                console.log(e.message);
                alert(e.message);
            }
        });
    }
    /*function borrarAnexo2(anexNume){
     var conf=confirm("Desea eliminar este anexo?");
     var formData="action=borrarAnex&radi=<?php echo $verrad?>&anexNume="+anexNume;
     if(conf){
     $.ajax({
     url: "../core/Modulos/radicacion/vista/operNuevoArchivo.php",
     type: "POST",
     data: formData,
     })
     .done(function(data){
     alert($(data).filter().text());
     })
     .fail(function(){
     alert("Fallo el ajax");
     });
     }
     }*/
</script>
<link rel="stylesheet" href="estilos/orfeo.css">

<body bgcolor="#FFFFFF">
<table style='width: 100%; border-spacing:  0' class="borde_tab">
    <tr>
        <td height="25" class="titulos4" colspan="10">GENERACI&Oacute;N DE DOCUMENTOS
        </td>
        <td height="25" class="titulos4" colspan="10" width="60%">
            <?php if (VLISTA_ANEXOS != 'VLISTA_ANEXOS') {
                ?>
                <a href="<?php echo VLISTA_ANEXOS;
                ?>" target='VideosOrfeoGPL'
                   alt='Descarga de Video Prueba de orfeogpl.org. Aprox 11 Megas'
                   title='Descarga de Video Prueba de orfeogpl.org. Aprox 11 Megas'> <img
                        src='imagenes/videoOrfeo.png' width=40 border=0></a>
            <?php }
            ?>
        </td>
    </tr>
    <tr>
        <td colspan='2'>
            <img src="<?php echo $ruta_raiz ?>/imagenes/estadoDocInfo.gif" width="320" height="35">
        </td>
    </tr>
</table>
<table align="center" cellspacing="2" style='WIDTH :100%;' class="borde_tab">

    <tr bgcolor='#6699cc' class='etextomenu' align='middle'>
        <th width='10%' class="titulos2" align="left"><img
                src="<?php echo $ruta_raiz ?>/imagenes/estadoDoc.gif" width="130" height="32">
        </th>
        <th width='15%' class="titulos2">RADICADO</th>
        <th width='5%' class="titulos2">TIPO<br>(Plantilla)</th>
        <th width='5%' class="titulos2">TRD</font></th>
        <!--<th width='1%' class="titulos2"></th>-->
        <th width='5%' class="titulos2">TAMA&Ntilde;O (Kb)</th>
        <th width='5%' class="titulos2">NUM PAG</th>
        <th width='5%' class="titulos2">SOLO LECTURA</th>
        <th width='20%' class="titulos2">CREADOR</th>
        <th width='20%' class="titulos2">DESCRIPCI&Oacute;N</th>
        <th width='12%' class="titulos2">ANEXADO</th>
        <!--<th width='13%' class="titulos2">NUMERADO</th>-->
        <th width='35%' colspan="5" class="titulos2">ACCI&Oacute;N</th>
    </tr>
    <?php $rowan = array();
    $rs = $db->query($isql);

    if (!$ruta_raiz_archivo)
        $ruta_raiz_archivo = $ruta_raiz;
    $directoriobase = "$ruta_raiz_archivo/bodega/";
    //Flag que indica si el radicado padre fue generado desde esta Ã¡rea de anexos
    $swRadDesdeAnex = $anex->radGeneradoDesdeAnexo($verrad);
    if ($rs) {
        while (!$rs->EOF) {
            $aplinteg = $rs->fields ["SGD_APLI_CODI"];
            $numextdoc = $rs->fields ["NUMEXTDOC"];
            $tpradic = $rs->fields ["SGD_TRAD_CODIGO"];
            $coddocu = $rs->fields ["DOCU"];
            $origen = $rs->fields ["ANEX_ORIGEN"];
            if ($rs->fields ["ANEX_SALIDA"] == 1)
                $num_archivos++;
            $puedeRadicarAnexo = $objCtrlAplInt->contiInstancia($coddocu, $MODULO_RADICACION_DOCS_ANEXOS, 2);
            $linkarchivo = $directoriobase . substr(trim($coddocu), 0, 4) . "/" . substr(trim($coddocu), 4, TAM_DEPE) . "/docs/" . trim($rs->fields ["NOMBRE"]);
            $linkarchivo_vista = $url_orfeo_power_tools.'/document/linked_document/'.$coddocu.'/download';
            $linkarchivotmp = $directoriobase . substr(trim($coddocu), 0, 4) . "/" . substr(trim($coddocu), 4, TAM_DEPE) . "/docs/tmp" . trim($rs->fields ["NOMBRE"]);
            if (!trim($rs->fields ["NOMBRE"]))
                $linkarchivo = "";
            ?>
            <tr class="listado2">
                <?php if ($origen == 1) {
                    //echo " class='timpar' ";
                    if ($rs->fields ["NOMBRE"] == "No") {
                        $linkarchivo = "";
                    }
                    //echo "";
                }
                if ($rs->fields ["RADI_NUME_SALIDA"] != 0) {
                    $cod_radi = $rs->fields ["RADI_NUME_SALIDA"];
                } else {
                    $cod_radi = $coddocu;
                }

                $anex_estado = $rs->fields ["ANEX_ESTADO"];
                if ($anex_estado == 0) {
                    $img_estado = "<img src=$ruta_raiz/imagenes/docVacio.gif ";
                }
                if ($anex_estado == 1) {
                    $img_estado = "<img src=$ruta_raiz/imagenes/docRecibido.gif ";
                }
                if ($anex_estado == 2) {
                    $estadoFirma = $objFirma->firmaCompleta($cod_radi);
                    if ($estadoFirma == "NO_SOLICITADA")
                        $img_estado = "<img src=$ruta_raiz/imagenes/docRadicado.gif  border=0>";
                    else if ($estadoFirma == "COMPLETA") {
                        $img_estado = "<img src=$ruta_raiz/imagenes/docFirmado.gif  border=0>";
                    } else if ($estadoFirma == "INCOMPLETA") {
                        $img_estado = "<img src=$ruta_raiz/imagenes/docEsperaFirma.gif border=0>";
                    }
                }
                if ($anex_estado == 3) {
                    $img_estado = "<img src=$ruta_raiz/imagenes/docImpreso.gif>";
                }
                if ($anex_estado == 4) {
                    $img_estado = "<img src=$ruta_raiz/imagenes/docEnviado.gif>";
                }
                ?>
                <TD height="21" class="listado2"><font size=1> <?php echo $img_estado ?> </font>
                </TD>
                <TD><font size=1>
                        <?php if (trim($linkarchivo)) {
                            $sqlradipath = "select radi_path from radicado where radi_nume_radi=$cod_radi";
                            $rs2 = $db->conn->Execute($sqlradipath);
                            $radipath = $rs2->fields['RADI_PATH'];
                            $nomarchivo = $rs->fields['NOMBRE'];
                            /*if ($anex_estado >= 2){
                                    $linkradipath=$ruta_raiz.'/bodega/'.$radipath;
                            }
                            else{*/
                            $linkradipath = $linkarchivo_vista;
                            //}
                            if ($anex_estado > -1) {

                                /* Added the signature icon */
                                /* Get ready the curl object to call the papiro cloud restful service, to query if
                                the current document is marked in the db as digitally signed */
                                $get_signed_url = $papirocloud_api_url.'/document/linked_document/'.$rs->fields['DOCU'].'/signed?key='.$papirocloud_api_key;
                                $papirocloud_api = curl_init();
                                curl_setopt($papirocloud_api, CURLOPT_RETURNTRANSFER, true);
                                curl_setopt($papirocloud_api, CURLOPT_URL, $get_signed_url);
                                $result = curl_exec($papirocloud_api);

                                if($result == 1){
                                    echo "<img title=\"Documento firmado digitalmente\" width=\"16\" src=\"".$url_orfeo_power_tools."/assets/images/icons/signature-icon.png\">&nbsp;";
                                }
                                /* Finished the adition to signature icon */

                                echo "<b><a target=\"_blank\"class=vinculos href='" . trim($linkradipath) . "'>" . trim(strtolower($cod_radi)) . "</a>";
                                echo "\n";
                            } else {
                                echo "<b><a class=vinculos href='#' onclick=\"alert('Este anexo no se ha generado el documento');\">" . trim(strtolower($cod_radi)) . "</a>\n";
                            }
                            /*                        if($anex_estado >= 2)
                                                    echo "  <b><a class=vinculos href='" . trim ( $linkarchivo_vista ) . "'>Borrador</a>";*/

                        } else echo strtolower($cod_radi);
                        $arch1 = preg_replace('/\/\//', '/', $linkradipath);
                        $pregext = substr(strtolower($arch1), -3);
                        $arch2 = substr($linkarchivo_vista, 0, -16);
                        ?>
                    </font></td>
                <TD align='center'><font size=1> <?php
                        if (trim($linkarchivo)) {
                            echo ($anex_estado > -1) ? $rs->fields ["EXT"] : "N/A";
                        } else {
                            echo $msg;
                        }
                        if ($rs->fields ["SGD_DIR_TIPO"] == 7)
                            $msg = "N/A";
                        else
                            $msg = "N/A";

                        /* Get ready the curl object to call the papiro cloud restful service, to get the
                        template name used to generate current document */
                        $get_signed_url = $papirocloud_api_url.'/documents/linked_document/'.$rs->fields['DOCU'].'/template/name?key='.$papirocloud_api_key;
                        $papirocloud_api = curl_init();
                        curl_setopt($papirocloud_api, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($papirocloud_api, CURLOPT_URL, $get_signed_url);
                        $result_template_name = curl_exec($papirocloud_api);

                        if($result_template_name != '0'){
                            echo "<br>(".$result_template_name.")";
                        }

                        ?>
                    </font></td>
                <td width="1%" valign="middle"><font
                        face="Arial, Helvetica, sans-serif" class="etextomenu">
                        <?php /*

	* Indica si el Radicado Ya tiene asociado algun TRD
	*/
                        $isql_TRDA = "SELECT *
				FROM SGD_RDF_RETDOCF
				WHERE RADI_NUME_RADI = '$cod_radi'
				";
                        $rs_TRA = $db->conn->Execute($isql_TRDA);
                        $radiNumero = $rs_TRA->fields ["RADI_NUME_RADI"];
                        if ($radiNumero != '') {
                            $msg_TRD = "S";
                        } else {
                            $msg_TRD = "N/A";
                        }
                        ?>
                        <center>
                            <?php echo $msg_TRD;
                            ?>
                        </center>
                    </font></td>

                <!--<td width="1%" valign="middle"><font
			face="Arial, Helvetica, sans-serif" class="etextomenu">
	<?php /**
                 * $perm_radi_sal  Viene del campo PER_RADI_SAL y Establece permiso en la rad. de salida
                 * 1 Radicar documentos,  2 Impresion de Doc's, 3 Radicacion e Impresion.
                 * (Por. Jh)
                 * Ademas verifica que el documento no este radicado con $rowwan[9] y [10]
                 * El jefe con $codusuario=1 siempre podra radicar $rs->fields ["EXT"] == "doc"  or
                 */
                if (($rs->fields ["EXT"] == "rtf" or $rs->fields ["EXT"] == "docx" or $rs->fields ["EXT"] == "odt" or $rs->fields ["EXT"] == "xml") and $rs->fields ["ANEX_ESTADO"] < 3) {
                    echo "<a class=vinculos href=javascript:vistaPreliminar('$coddocu','$linkarchivo','$linkarchivotmp')>";
                    ?>
	<img src="<?php echo $ruta_raiz ?>/imagenes/iconos/vista_preliminar.gif"
			alt="Vista Preliminar" border="0"> <font
			face="Arial, Helvetica, sans-serif" class="etextomenu">
	<?php echo "</a>";
                    $radicado = "false";
                    $anexo = $cod_radi;
                }
                ?>
	</font></TD>-->
                <td align="center"><font size=1> <?php echo $rs->fields ["TAMA"] ?> </font></td>
                <td align="center"><font size=1> <?php if ($rs->fields ["PAG"] > 0) {
                            echo $rs->fields ["PAG"];
                        } else {
                            echo "N/A";
                        } ?> </font></td>
                <td align="center"><font size=1> <?php echo ($rs->fields ["RO"] == 'S') ? "S" : "N/A" ?> </font></td>
                <td><font size=1> <?php echo $rs->fields ["CREA"] ?> </font></td>
                <td><font
                        size=1> <?php echo ($rs->fields ["SGD_TPR_DESCRIP"] != "") ? mb_strtoupper($rs->fields ["SGD_TPR_DESCRIP"], 'UTF-8') . '-' : "" ?><?php echo mb_strtoupper($rs->fields ["DESCR"], 'UTF-8') ?> </font>
                </td>
                <td align="center"><font size=1> <?php echo $rs->fields ["FEANEX"] ?> </font></td>
                <!--<td><font size=1>
 <?php /*if ($rs->fields ["SGD_PNUFE_CODI"] && strcmp ( $cod_radi, $rs->fields ["SGD_DOC_PADRE"] ) == 0 && strlen ( $rs->fields ["SGD_DOC_SECUENCIA"] ) > 0) {
			$anex->anexoRadicado ( $verrad, $rs->fields ["DOCU"] );
			echo ($anex->get_doc_secuencia_formato ( $dependencia ) . "<BR>" . $rs->fields ["FECDOC"]);
		}*/
                ?>
</font></td>-->
                <td style="padding: 0px 0px 0px 0px;">
                    <table width='100%' cellspacing='2' cellpadding='0'>
                        <tr class='titulos2' align='center'>
                            <?php if ($origen != 1 and $linkarchivo and $verradPermisos == "Full") {
                                if ($anex_estado < 4 && $rs->fields['SGD_TRAD_CODIGO'] == null) {
                                    echo "<td><font size=1><a class=vinculos href=\"#\" onclick=\"overWindow('../core/Modulos/radicacion/vista/nuevoArchivo.php?action=2&radi=$verrad&anexCodigo=$coddocu');\">Modificar</a></td></td>";
                                }
                            }
                            //Estas variables se utilizan para verificar si se debe mostrar la opci�n de tipificaci�n de anexo .TIF
                            $anexTipo = $rs->fields ["ANEX_TIPO"];
                            $anexTPRActual = $rs->fields ["SGD_TPR_CODIGO"];
                            if ($verradPermisos == "Full") {
                                $radiNumeAnexo = $rs->fields ["RADI_NUME_SALIDA"];
                                if ($radiNumeAnexo > 0 and trim($linkarchivo)) {
                                    if (!$codserie)
                                        $codserie = "0";
                                    if (!$tsub)
                                        $tsub = "0";
                                    echo "<td><font size=1><a class=vinculos href=javascript:ver_tipodocuATRD($radiNumeAnexo,$codserie,$tsub);>Tipificar</a></font></td>";
                                } elseif ($perm_tipif_anexo == 1 && $anexTipo == 4 && $anexTPRActual == '') { //Es un anexo de tipo tif (4) y el usuario tiene permiso para Tipificar, adem�s el anexo no ha sido tipificado
                                    if (!$codserie)
                                        $codserie = "0";
                                    if (!$tsub)
                                        $tsub = "0";
                                    echo "<td><font size=1><a class=vinculoTipifAnex href=javascript:ver_tipodocuAnex('$cod_radi','$anexo',$codserie,$tsub);> Tipificar </a></font></td>";
                                } elseif ($perm_tipif_anexo == 1 && $anexTipo == 4 && $anexTPRActual != '') { //Es un anexo de tipo tif (4) y el usuario tiene permiso para Tipificar, adem�s el anexo YA ha sido tipificado antes
                                    if (!$codserie)
                                        $codserie = "0";
                                    if (!$tsub)
                                        $tsub = "0";
                                    echo "<td><font size=1><a class=vinculoTipifAnex href=javascript:ver_tipodocuAnex('$cod_radi','$anexo',$codserie,$tsub);> Re-Tipificar </a></font></td>";
                                }
                                ?>
                                <?php if ($rs->fields ["RADI_NUME_SALIDA"] == 0 and $ruta_raiz != ".." and (trim($rs->fields ["ANEX_CREADOR"]) == trim($krd) or $codusuario == 1)) {
                                    if ($origen != 1 and $linkarchivo) {
                                        echo "<td><font size=1><a class=vinculos href='#' onclick=\"overWindowMini('../core/Modulos/radicacion/vista/nuevoArchivo.php?action=3&anexCodigo=$coddocu')\">Borrar</a></font></td>";
                                    }
                                }

                                /**
                                 * $perm_radi_sal  Viene del campo PER_RADI_SAL y Establece permiso en la rad. de salida
                                 * 1 Radicar documentos,  2 Impresion de Doc's, 3 Radicacion e Impresion.
                                 * (Por. Jh)
                                 * Ademas verifica que el documento no este radicado con $rowwan[9] y [10]
                                 * El jefe con $codusuario=1 siempre podra radicar
                                 */

                                /** Obtiene la información sobre el modo de radicación para conocer si es de salida o entrada */
                                $document_mode_id = substr($verrad, -1);
                                $sql_get_document_mode = 'select sgd_trad_genradsal from sgd_trad_tiporad where sgd_trad_codigo = '.$document_mode_id;
                                $result_get_document_mode = $db->query($sql_get_document_mode);
                                while(!$result_get_document_mode->EOF){
                                    $document_mode_outbound = $result_get_document_mode->fields['SGD_TRAD_GENRADSAL'];
                                    $result_get_document_mode->MoveNext();
                                }
                                /** */

                                if ($rs->fields ["EXT"] == "pdf" or $rs->fields ["EXT"] == "rtf" or $rs->fields ["EXT"] == "docx" or $rs->fields ["EXT"] == "odt" or $rs->fields ["EXT"] == "xml") {
                                    if ($tpPerRad [$tpradic] == 2 or $tpPerRad [$tpradic] == 3) {

                                        /* Si el documento puede ser radicado entonces se presenta la opción de elaborar el documento */
                                        echo "<td><font size=1><a class=vinculos href=\"".$url_orfeo_power_tools."/documents/linked_document/".$rs->fields['DOCU']."/values\">Elaborar documento</a></font></td>";
                                        /* Fin de la presentación de la opción de elaborar documento */

                                        if (!$rs->fields ["RADI_NUME_SALIDA"]) {
                                            if ($document_mode_outbound != 1 && $puedeRadicarAnexo == 1) {
                                                $rs->fields ["SGD_PNUFE_CODI"] = 0;
                                                echo "
                                                <td><a class=vinculos href=\"".$url_orfeo_power_tools."/documents/linked_document/".$rs->fields['DOCU']."/commit\">Radicar(-$tpradic)</a></td>";
                                                $radicado = "false";
                                                $anexo = $cod_radi;
                                            } else if ($puedeRadicarAnexo != 1) {
                                                $objError = new AplExternaError ();
                                                $objError->setMessage($puedeRadicarAnexo);
                                                echo($objError->getMessage());
                                            } else {

                                                if ((substr($verrad, -1) != 2) and $num_archivos == 1 and !$rs->fields ["SGD_PNUFE_CODI"] and $swRadDesdeAnex == false) {
                                                    echo "<td><font size=1><a class=vinculos href=javascript:asignarRadicado('$coddocu','$linkarchivo','$cod_radi','$numextdoc')>Asignar Rad</a></font></td>";
                                                    $radicado = "false";
                                                    $anexo = $cod_radi;
                                                } else if ($rs->fields ["SGD_PNUFE_CODI"] && strcmp($cod_radi, $rs->fields ["SGD_DOC_PADRE"]) == 0 && !$anex->seHaRadicadoUnPaquete($rs->fields ["SGD_DOC_PADRE"])) {
                                                    echo "<td><a class=vinculos href=\"".$url_orfeo_power_tools."/documents/linked_document/".$rs->fields['DOCU']."/commit\">Radicar(-$tpradic)</a></td>";
                                                    $radicado = "false";
                                                    $anexo = $cod_radi;
                                                } else if ($puedeRadicarAnexo == 1) {
                                                    $rs->fields ["SGD_PNUFE_CODI"] = 0;
                                                    echo "<td><a class=vinculos href=\"".$url_orfeo_power_tools."/documents/linked_document/".$rs->fields['DOCU']."/commit\">Radicar(-$tpradic)</a></td>";
                                                    $radicado = "false";
                                                    $anexo = $cod_radi;
                                                }
                                            }
                                        } else {
                                            if (!$rs->fields ["SGD_PNUFE_CODI"])
                                                $rs->fields ["SGD_PNUFE_CODI"] = 0;
                                            if ($anex_estado < 4) {
                                                $radicado = "true";

                                                /* Get ready the curl object to call the papiro cloud restful service, to query if
                                                the current document has permission to download docx template and to upload pdf custom file */
                                                $get_signed_url = $papirocloud_api_url.'/documents/linked_document/'.$rs->fields['DOCU'].'/docx_template_permission?key='.$papirocloud_api_key;
                                                $papirocloud_api = curl_init();
                                                curl_setopt($papirocloud_api, CURLOPT_RETURNTRANSFER, true);
                                                curl_setopt($papirocloud_api, CURLOPT_URL, $get_signed_url);
                                                $result_docx_template_permission = curl_exec($papirocloud_api);

                                                if($result_docx_template_permission=='1'){
                                                    /* Si el documento ya está radicado pero aún no ha sido enviado se presenta el vínculo para cargar un archivo con firma */
                                                    echo "<td><font size=1><a class=vinculos href=\"".$url_orfeo_power_tools."/documents/linked_document/".$rs->fields['DOCU']."/upload\">Cargar documento</a></font></td>";
                                                    /* Fin del vínculo para cargar un archivo con firma */
                                                }
                                                else{
                                                    echo "<td><span style='font-size:0.8em;color:gray;'>Cargar documento (desactivado)</span></td>";
                                                }

                                            }
                                        }
                                    } else if ($rs->fields ["SGD_PNUFE_CODI"] && ($usua_perm_numera_res == 1) && $ruta_raiz != ".." && !$rs->fields ["SGD_DOC_SECUENCIA"] && strcmp($cod_radi, $rs->fields ["SGD_DOC_PADRE"]) == 0) // SI ES PAQUETE DE DOCUMENTOS Y EL USUARIO TIENE PERMISOS
                                    {
                                        echo "<td><font size=1><a class=vinculos href=javascript:numerarArchivo('$coddocu','$linkarchivo','si'," . $rs->fields ["SGD_PNUFE_CODI"] . ")>Numerar</a></font></td>";
                                    }

                                }
                                if ($rs->fields ["RADI_NUME_SALIDA"]) {
                                    $radicado = "true";
                                }
                                ?>
                                <!--td><font size="1"><?php php//echo "if($anex_estado>=2 && $carpetaNo==3 && ($arch1==$arch2 || $pregext!='pdf') && ".substr($cod_radi,-1)."==3)";?>
                    <?php /*
                    if($anex_estado>=2 && ($arch1==$arch2 || $pregext!='pdf') && substr($cod_radi,-1)==3){
                        echo "<a class=vinculos href=javascript:docDefinitivo('$cod_radi')>Definitivo</a>";
                    }*/
                                ?>
                </font></td-->

                            <?php } else {
                                if ($origen != 1 and $linkarchivo and $perm_borrar_anexo == 1 && $anexTipo == 4 && $anex_estado != 3) {
                                    echo "<td><font size=1><a class=vinculoTipifAnex href='#' onclick=\"overWindowMini('../core/Modulos/radicacion/vista/nuevoArchivo.php?action=3&anexCodigo=$coddocu')\">Borrar</a></font></td>";
                                }
                                if ($perm_tipif_anexo == 1 && $anexTipo == 4 && $anexTPRActual == '') { //Es un anexo de tipo tif (4) y el usuario tiene permiso para Tipificar, adem�s el anexo no ha sido tipificado
                                    if (!$codserie)
                                        $codserie = "0";
                                    if (!$tsub)
                                        $tsub = "0";
                                    echo "<td><font size=1><a class=vinculoTipifAnex href=javascript:ver_tipodocuAnex('$cod_radi','$anexo',$codserie,$tsub);> Tipificar </a></font></td>";
                                } elseif ($perm_tipif_anexo == 1 && $anexTipo == 4 && $anexTPRActual != '') { //Es un anexo de tipo tif (4) y el usuario tiene permiso para Tipificar, adem�s el anexo YA ha sido tipificado antes
                                    if (!$codserie)
                                        $codserie = "0";
                                    if (!$tsub)
                                        $tsub = "0";
                                    echo "<td><font size=1><a class=vinculoTipifAnex href=javascript:ver_tipodocuAnex('$cod_radi','$anexo',$codserie,$tsub);> Re-Tipificar </a></font></td>";
                                }
                            }
                            ?>
                        </tr>
                    </table>
            </tr>
            <?php $rs->MoveNext();
        }
    }
    /*
    $mostrar_lista = 0;
    if($mostrar_lista==1)
    {
    ?>
    </TABLE>
    <?
    }*/
    ?>

</table>
<?php if ($verradPermisos == "Full") {
    ?>
    <br>
    <table width="100%" align="center" border="0" cellpadding="0"
           cellspacing="5" class="borde_tab">
        <tr align="center">
            <td><!--<a class="vinculos"
			href='javascript:nuevoArchivo(<?php if ($num_archivos == 0 && $swRadDesdeAnex == false)
                    echo "1";
                else
                    echo "0";
                ?>)'
			class="timpar"> Anexar Archivo ... </a>--> <!-- - - - -
<a class="vinculos" href='javascript:nuevoEditWeb(<?php if ($num_archivos == 0 && $swRadDesdeAnex == false)
                    echo "1";
                else
                    echo "0";
                ?>)' class="timpar">
Editar Nuevo Documento ... </a>--><input type='button' value='Anexar documento (no genera número de radicado)' class='botones_largo'
                                         onclick="return overWindow('../core/Modulos/radicacion/vista/nuevoArchivo.php?action=0&radi=<?php echo $verrad ?>');">&nbsp;&nbsp;
                <?php /*	if ($num_archivos == 0 && $swRadDesdeAnex == false){
	    echo "<a href=\"#\" onclick=\"return overWindow('../core/Modulos/radicacion/vista/nuevoArchivo.php?action=2&radi=$verrad');\" class='vinculos'>Anexo Documento Principal</a>&nbsp;&nbsp;\n";
	}*/
                ?>
                <input type='button' value='Elaborar nuevo documento' class='botones_largo'
                       onclick="location.href='<?= $url_orfeo_power_tools ?>/documents/linked_document/<?= $verrad ?>/new';">
            </td>
            <script>
                swradics =<?php echo $num_archivos?>;
            </script>
            <?php /* Anexar plantillas, keda por ahora aplazado el proyecto
	<td class="celdaGris"> <a href='javascript:Plantillas(0)' class="timparr">Anexar
      Plantilla ...</a>
      <!-- <a href='plantilla.php?<?=SID ?>'>Anexar Plantilla ... </a> </td>-->
    </TD>

    <td class="celdaGris"> <a href='javascript:Plantillas_pb(0)' class="timparr">A</a>
      <!-- <a href='plantilla.php?<?=SID ?>'>Anexar Plantilla ... </a> </td>-->
    </TD>
	*/

            ?>
        </tr>
    </table>
<?php }

?>
<br>
</body>