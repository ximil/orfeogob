<?php session_start(); date_default_timezone_set('America/Bogota'); date_default_timezone_set('America/Bogota');
$verrad = "";
$ruta_raiz = "..";
$krd = $_SESSION["krd"];
$dependencia = $_SESSION["dependencia"];
$usua_doc = $_SESSION["usua_doc"];
$codusuario = $_SESSION["codusuario"];
$tpNumRad = $_SESSION["tpNumRad"];
$tpPerRad = $_SESSION["tpPerRad"];
$tpDescRad = $_SESSION["tpDescRad"];
$tip3Nombre = $_SESSION["tip3Nombre"];
$tip3img = $_SESSION["tip3img"];
$tpDepeRad = $_SESSION["tpDepeRad"];
$tip3desc = $_SESSION["tip3desc"];
$tip3img =$_SESSION["tip3img"];
$id_rol =$_SESSION["id_rol"];
include_once $ruta_raiz.'/../core/config/config-inc.php';
include($ruta_raiz.'/validadte.php');
if (!$dep_sel) $dep_sel = $_SESSION['dependencia'];
$depeBuscada =$dep_sel;
foreach ($_GET as $key => $valor)  $$key = $valor;
foreach ($_POST as $key => $valor)  $$key = $valor;

?>
<html>
<head>
<title>Anulación de Radicados</title>
<link rel="stylesheet" href="../estilos/orfeo.css">
<script type="text/javascript" language="javascript">
    <!-- Funcion que activa el sistema de marcar o desmarcar todos los check  -->
</script>
</head>
<body bgcolor="#FFFFFF" onLoad="window_onload();">
<div id="spiffycalendar" class="text"></div>
<link rel="stylesheet" type="text/css" href="../js/spiffyCal/spiffyCal_v2_1.css">
<?php 
include_once "$ruta_raiz/include/db/ConnectionHandler.php";
$db = new ConnectionHandler("$ruta_raiz");
/**
 * Generamos el encabezado que envia las variable a la paginas siguientes.
 * Por problemas en las sesiones enviamos el usuario.
 * @$encabezado  Incluye las variables que deben enviarse a la singuiente pagina.
 * @$linkPagina  Link en caso de recarga de esta pagina.
*/
switch ($tpAnulacion)
{
    case 1:
        $whereTpAnulacion = " and (
			b.SGD_EANU_CODIGO = 9
			or b.SGD_EANU_CODIGO = 2
			or b.SGD_EANU_CODIGO IS NULL
			)";
	$nomcarpeta    = "Solicitud de Anulacion de Radicados";
	$nombreCarpeta = "Solicitud de Anulacion de Radicados";
	$accion_sal    = "Solicitar Anulacion";
	$textSubmit = "Solicitar Anulacion";
	break;
    case 2:
	$whereTpAnulacion = " AND b.SGD_EANU_CODIGO = 2 ";
	$nomcarpeta    =  "Radicados para Anular";
	$nombreCarpeta = "Radicados para Anular";
	$accion_sal    = "";
	$textSubmit = "";
	break;
    case 3:
        $whereTpAnulacion = " and b.SGD_EANU_CODIGO = 9 ";
	$nomcarpeta    = "Radicados Anulados";
	$nombreCarpeta = "Radicados Anulados";
	$accion_sal    = "Ver Reporte";
	$textSubmit = "Ver Reporte";
	break;
}

$encabezado = "".session_name()."=".session_id()."&krd=$krd&filtroSelect=$filtroSelect&accion_sal=$accion_sal&dep_sel=$dep_sel&tpAnulacion=$tpAnulacion&orderNo=";
$linkPagina = "$PHP_SELF?$encabezado&accion_sal=$accion_sal&orderTipo=$orderTipo&orderNo=$orderNo";
$carpeta = "xx";

include "../envios/paEncabeza.php";

$pagina_actual = "../anulacion/cuerpo_anulacion.php";
$varBuscada = "radi_nume_radi";
include "../envios/paBuscar.php";
$pagina_sig = "../anulacion/solAnulacion.php";
//$swListar = "no";
$accion_sal="Solicitar Anulacion";
include "../envios/paOpciones.php";

$whereFiltro=$dependencia_busq2;
/**  GENERACION LISTADO DE RADICADOS
 *  Aqui utilizamos la clase adodb para generar el listado de los radicados
 *  Esta clase cuenta con una adaptacion a las clases utiilzadas de orfeo.
 *  el archivo original es adodb-pager.inc.php la modificada es adodb-paginacion.inc.php
 */

//error_reporting(7);
if ($orderNo==98 or $orderNo==99)
{
    $order=1;
    if ($orderNo==98)   $orderTipo="desc";
    if ($orderNo==99)   $orderTipo="";
}
else
{
    if (!$orderNo)  $orderNo=3;
    $order = $orderNo + 1;

    if($orden_cambio==1)
    {
        (!$orderTipo) ? $orderTipo="desc" : $orderTipo="";
    }
}
$sqlFecha = $db->conn->SQLDate("d-m-Y H:i A","b.RADI_FECH_RADI");
include "$ruta_raiz/include/query/anulacion/querycuerpo_anulacion.php";
?>
  <form name=formEnviar action='../anulacion/solAnulacion.php?<?php echo session_name()."=".session_id()."&krd=$krd" ?>&tpAnulacion=<?php echo $tpAnulacion?>&depeBuscada=<?php echo $depeBuscada?>&estado_sal_max=<?php echo $estado_sal_max?>&pagina_sig=<?php echo $pagina_sig?>&dep_sel=<?php echo $dep_sel?>&nomcarpeta=<?php echo $nomcarpeta?>&orderTipo=<?php echo $orderTipo?>&orderNo=<?php echo $orderNo?>' method=post>
<?php //$db->conn->debug=true;
$encabezado = "".session_name()."=".session_id()."&krd=$krd&depeBuscada=$depeBuscada&accion_sal=$accion_sal&filtroSelect=$filtroSelect&dep_sel=$dep_sel&tpAnulacion=$tpAnulacion&nomcarpeta=$nomcarpeta&orderTipo=$orderTipo&orderNo=";
$linkPagina = $_SERVER['PHP_SELF']."?$encabezado&orderTipo=$orderTipo&orderNo=$orderNo";
if(isset($_POST['Buscar']) || isset($_GET['filtroSelect'])){
$pager = new ADODB_Pager($db,$isql,'adodb', true,$orderNo,$orderTipo);
$pager->checkAll = false;
$pager->checkTitulo = true;
$pager->toRefLinks = $linkPagina;
$pager->toRefVars = $encabezado;
$pager->Render($rows_per_page=20,$linkPagina,$checkbox=chkEnviar);
}
?>
</form>
</body>
</html>
