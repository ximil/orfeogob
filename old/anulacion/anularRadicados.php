<?php putenv("NLS_LANG=American_America.UTF8");
session_start(); date_default_timezone_set('America/Bogota'); date_default_timezone_set('America/Bogota');
error_reporting(0);

// Compatibilidad con register_globals = Off.
foreach ($_GET as $key => $valor)  $$key = $valor;
foreach ($_POST as $key => $valor)  $$key = $valor;

$ruta_raiz = "..";
include($ruta_raiz.'/validadte.php');
//if (!$_SESSION['dependencia'] and !$_SESSION['depe_codi_territorial'])	include "../rec_session.php";
$dependencia=$_SESSION['dependencia'];
$codusuario=$_SESSION['codusuario'];
$usua_doc=$_SESSION['usua_doc'];
$rolOrigen = $_SESSION["id_rol"];
if(!$fecha_busq) $fecha_busq=date("Y-m-d");
if(!$fecha_busq2) $fecha_busq2=date("Y-m-d");
include('../config.php');
include_once "Anulacion.php";
include_once "$ruta_raiz/include/tx/Historico.php";
include_once "$ruta_raiz/include/db/ConnectionHandler.php";
include_once $ruta_raiz.'/../core/clases/file-class.php';
$db = new ConnectionHandler("$ruta_raiz");
//$db->conn->debug=true;
//Cargando variables del log
if(isset($_SERVER['HTTP_X_FORWARD_FOR'])){
    $proxy=$_SERVER['HTTP_X_FORWARD_FOR'];
}else
    $proxy=$_SERVER['REMOTE_ADDR'];
$REMOTE_ADDR=$_SERVER['REMOTE_ADDR'];
$ruta_raiz2=$ruta_raiz;
$ruta_raiz='../..';
include_once "$ruta_raiz/core/clases/log.php";
$log = new log($ruta_raiz);
$log->setAddrC($REMOTE_ADDR);
$log->setProxyAd($proxy);
$ruta_raiz=$ruta_raiz2;

if ($cancelarAnular)
{	$aceptarAnular = "";
	$actaNo = "";
}

// Compatibilidad con register_globals = Off.
// Se asigna a la variable $depe_codi_territorial el valor de $_SESSION['depe_codi_territorial']
$depe_codi_territorial = $_SESSION['depe_codi_territorial'];
?>
<head>
<link rel="stylesheet" href="../estilos/orfeo.css">
<script>
function soloNumeros()
{
	jh =  document.new_product.actaNo.value;
 	if(jh)
 	{
    	var1 =  parseInt(jh);
		if(var1 != jh)
		{
			alert('Solo introduzca numeros.' );
			//document.forma.submit();
			return false;
		}
		else
		{
		    document.new_product.submit();
        }
 	}
 	else
 	{	document.new_product.submit();	}
 }
 </script>
 <link rel="stylesheet" type="text/css" href="../js/spiffyCal/spiffyCal_v2_1.css">
<script  src="../js/spiffyCal/spiffyCal_v2_1.js"></script>
<script >
<!--
	var dateAvailable = new ctlSpiffyCalendarBox("dateAvailable", "new_product", "fecha_busq","btnDate1","<?php echo $fecha_busq?>",scBTNMODE_CUSTOMBLUE);
	var dateAvailable2 = new ctlSpiffyCalendarBox("dateAvailable2", "new_product", "fecha_busq2","btnDate1","<?php echo $fecha_busq2?>",scBTNMODE_CUSTOMBLUE);
//-->
</script>
</head>
<BODY>
<div id="spiffycalendar" class="text"></div>
<TABLE style="width:100%;border-spacing: 2" class='borde_tab'>
  <TR>
    <TD height="30" valign="middle"   class='titulo1' align="center">Anulacion de Radicados por Dependencia
	</td>
	</tr>
</table>

  <form name="new_product"  action='anularRadicados.php' method='post'>
<diV align="center">
<table style="width:100%;" ><tr style='border-spacing: 2;vertical-align: top;'><td>
<TABLE  class='borde_tab' style="width:550px;border-spacing: 2; position:inherit; ;" >
  <!--DWLayoutTable-->
  <TR>
    <TD width="125" height="21"  class='titulos2'> Fecha desde<br>
	<?php
	  echo "($fecha_busq)";
	?>
	</TD>
    <TD width="500" align="right" valign="top" class='listado2'>
    <script >
		dateAvailable.date = "2003-08-05";
		dateAvailable.writeControl();
		dateAvailable.dateFormat="yyyy-MM-dd";
    </script>
</TD>
  </TR>
  <TR>
    <TD width="125" height="21"  class='titulos2'> Fecha Hasta<br>
	<?php
	  echo "($fecha_busq2)";
	?>
	</TD>
    <TD width="500" align="right" valign="top"  class='listado2'>
    <script >
		 dateAvailable2.date = "2003-08-05";
		 dateAvailable2.writeControl();
		 dateAvailable2.dateFormat="yyyy-MM-dd";
    </script>
</TD>
  </TR>
  <tr>
    <TD height="26" class='titulos2'>Tipo Radicacion</TD>
    <TD valign="top" align="left"  class='listado2'>
		<?php
		$sqlTR ="select upper(sgd_trad_descr),sgd_trad_codigo from sgd_trad_tiporad where sgd_trad_codigo!=2
				order by sgd_trad_codigo";
		$rsTR = $db->conn->Execute($sqlTR);
		print $rsTR->GetMenu2("tipoRadicado","$tipoRadicado",false, false, 0," class='select'>");
		//if(!$depeBuscada) $depeBuscada=$dependencia;
		?>

	 	</TD>
  </tr>
  <tr>
    <TD height="26" class='titulos2'>Dependencia</TD>
    <TD valign="top" align="left"  class='listado2'>
		<?php
			$sqlD = "select depe_nomb,depe_codi from dependencia
			       where depe_codi_territorial = $depe_codi_territorial
							order by depe_codi";
			$rsD = $db->conn->Execute($sqlD);
			print $rsD->GetMenu2("depeBuscada","$depeBuscada",false, false, 0," class='select'> <option value=0>--- TODAS LAS DEPENDENCIAS --- </OPTION ");
			//if(!$depeBuscada) $depeBuscada=$dependencia;
		?>

	 	</TD>
  </tr>
  <tr>
    <td  colspan="2" class='titulos2' style='text-align: center;vertical-align: top; height: 26'>
		<INPUT TYPE="submit" name="generar_informe" Value='Ver Documentos En Solicitud' class='botones_funcion' >
		</td>
	</tr>
  </TABLE>
</diV>

<?php if(!$fecha_busq) $fecha_busq = date("Y-m-d");
if($aceptar1 and !$actaNo and !$cancelarAnular) die ("<font color=red><span class=etextomenu>Debe colocal el Numero de acta para poder anular los radicados</span></font>");

if(($generar_informe or $aceptarAnular) and !$cancelarAnular)
{
if($depeBuscada and $depeBuscada != 0)
{
  $whereDependencia = " b.DEPE_CODI=$depeBuscada AND";
}
    include_once("../include/query/busqueda/busquedaPiloto1.php");
	include "$ruta_raiz/include/query/anulacion/queryanularRadicados.php";
	error_reporting(7);
	$fecha_ini = $fecha_busq;
    $fecha_fin = $fecha_busq2;
	$fecha_ini = mktime(00,00,00,substr($fecha_ini,5,2),substr($fecha_ini,8,2),substr($fecha_ini,0,4));
	$fecha_fin = mktime(23,59,59,substr($fecha_fin,5,2),substr($fecha_fin,8,2),substr($fecha_fin,0,4));

	//Modificacion skina
	$query = "select $radi_nume_radi as radi_nume_radi, r.radi_fech_radi, r.ra_asun, r.radi_usua_actu,
				r.radi_depe_actu, r.radi_usu_ante, c.depe_nomb, b.sgd_anu_sol_fech, ".$db->conn->substr."(b.sgd_anu_desc, 21,62) as sgd_anu_desc
			 from radicado r, sgd_anu_anulados b, dependencia c";
	$fecha_mes = substr($fecha_ini,0,7);
	// Si la variable $generar_listado_existente viene entonces este if genera la planilla existente
	$where_isql = " WHERE $whereDependencia	b.sgd_anu_sol_fecH BETWEEN ".
						$db->conn->DBTimeStamp($fecha_ini)." and ".$db->conn->DBTimeStamp($fecha_fin).
						" and SGD_EANU_CODI = 1 $whereTipoRadi and r.radi_nume_radi=b.radi_nume_radi and b.depe_codi = c.depe_codi";
	$order_isql = " ORDER BY   b.SGD_ANU_SOL_FECH,b.depe_codi";
	$query_t = $query . $where_isql . $order_isql ;

	// Verifica el ultimo numero de acta del tipo de radicado

        $yearact = date("Y");

	$queryk ="Select max (usua_anu_acta)
			from sgd_anu_anulados
			where sgd_eanu_codi=2 and sgd_trad_codigo = $tipoRadicado and sgd_anu_sol_fech >= to_date('".$yearact."-01-01','yyyy-mm-dd')	";

	$c = $db->conn->Execute($queryk);
	$rsk=$db->query($queryk);

	//require "$ruta_raiz/class_control/class_control.php";
	require "../anulacion/class_control_anu.php";
	$db->conn->SetFetchMode(ADODB_FETCH_NUM);
	$btt = new CONTROL_ORFEO($db);
	$campos_align = array("C","L","L","L","L","L","L","L","L","L","L","L");
	$campos_tabla = array("depe_nomb","radi_nume_radi","sgd_anu_sol_fech", "sgd_anu_desc");
	$campos_vista = array ("Dependencia","Radicado","Fecha de Solicitud", "Observacion Solicitante");
	$campos_width = array (200          ,100        ,280           ,300       );
	$btt->campos_align = $campos_align;
	$btt->campos_tabla = $campos_tabla;
	$btt->campos_vista = $campos_vista;
	$btt->campos_width = $campos_width;
	?>
	</td><td >
<TABLE  class='borde_tab'  style='border-spacing: 2;witext-align: center;vertical-align: top; height: 26'>
  <TR>
    <TD height="30" valign="middle"   class='titulos5' align="center" colspan="2" >Documentos con solicitud de Anulacion</td></tr>
  <tr><td width="16%" class='titulos5'>Fecha Inicial </td><td width="84%" class='listado5'><?php echo $fecha_busq ?> </td></tr>
  <tr><td class='titulos5'>Fecha Final   </td><td class='listado5'><?php echo $fecha_busq2 ?> </tr>
  <tr><td class='titulos5'>Fecha Generado </td><td class='listado5'><?php  echo date("Y-m-d - H:i:s"); ?></td></tr>
</table>
<?php 	if($generar_informe)
	{

	?>
	<div align="center">
	<span class="listado2">
	<br>Si esta seguro de Anular estos documentos por favor escriba el numero de acta y presione aceptar.<br>
	<br>Ultima acta generada de este tipo de radicado es la No. <?php  echo $rsk->fields["0"]; ?> <br>

	<br>Acta No.    <input type="text" name="actaNo" class="tex_area" value="<?php  echo $rsk->fields["0"]+1; ?>" readonly="readonly"/><br>
	<table class='borde_tab' align="center">
	<tr><td>
	<input type="submit" name="AcepAnular" value="Aceptar" class="botones"/>
	<input type="hidden" name="aceptarAnular" value="Aceptar" class="ebutton"/>
	</td><td>
	<input type="submit" name="cancelarAnular" value="Cancelar" class="botones">
	</td></tr>
	</table>
	</span>
	</div>
	<?php
	}?>

</td></tr><tr><td colspan="2">
		<?php

	$btt->tabla_sql($query_t);

	error_reporting(7);
	$html= $btt->tabla_html;

	$radAnular = $btt->radicadosEnv;

	$radObserva = $btt->radicadosObserva;

	//Se asigna el No. de la ultima acta + 1
	//$k = $rsk->fields["0"] + 1;

	}


	//Se le asigna a actaNo el No. de acta que debe seguir
	//	$actaNo=$k;

	if($aceptarAnular and $actaNo and $AcepAnular)
	{

	error_reporting(7);
	include_once "$ruta_raiz/include/db/ConnectionHandler.php";
	$db = new ConnectionHandler("$ruta_raiz");
	//*Inclusion territorial

	if ($depeBuscada == 0 )
	{
		$sqlD = "select depe_nomb,depe_codi from dependencia
		       where depe_codi_territorial = $depe_codi_territorial
				order by depe_codi";
		$db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
		$rsD = $db->conn->Execute($sqlD);
		while(!$rsD->EOF)
		{
		    $depcod = $rsD->fields["DEPE_CODI"];
		    $lista_depcod .= " $depcod,";
		    $rsD->MoveNext();
		}
		$lista_depcod .= "0";
	}
	else
	{
	    $lista_depcod = $depeBuscada;
	    }
            $where_depe = " and (depe_codi) in ($lista_depcod )";
			//*fin inclusion
		   /*
		   * Variables que manejan el tipo de Radicacion
		   */
		   $isqlTR = 'select sgd_trad_descr,sgd_trad_codigo from sgd_trad_tiporad
        	     	where sgd_trad_codigo = '. $tipoRadicado.'
		  			';
    		$rsTR = $db->conn->Execute($isqlTR);
			if ($rsTR)
	    		{
				$TituloActam = $rsTR->fields["SGD_TRAD_DESCR"];
				} else
				{
				$TituloActam = "sin titulo ";
		}

    	$dbSel = new ConnectionHandler("$ruta_raiz");
	//$dbSel->conn->debug=true;
	$dbSel->conn->SetFetchMode(ADODB_FETCH_ASSOC);
	$rsSel = $dbSel->conn->Execute($query_t);
	$i=0;
	while(!$rsSel->EOF)
	{
	   $radAnularE[$i] = $rsSel->fields['RADI_NUME_RADI'];
	   $radObservaE[$i]= $rsSel->fields['SGD_ANU_DESC'];
	   $depSolicitaE[$i]=$rsSel->fields['DEPE_NOMB'];
	    $i++;
		$rsSel->MoveNext();
	}
//
	if(!$radAnularE) die("<P><span class=etextomenu><CENTER><FONT COLOR=RED>NO HAY RADICADOS PARA ANULAR</FONT></CENTER><span>");
	else {
           /*
		  *$where_TipoRadicado incluido 03082005 para filtrar por tipo radicacion del anulado
		  */

 	    $where_TipoRadicado = " and sgd_trad_codigo = " . $tipoRadicado;
    	$Anulacion = new Anulacion($db);
		$observa = "Radicado Anulado. (Acta No $actaNo)";
		$noArchivo = "/pdfs/planillas/ActaAnul_$dependencia"."_"."$tipoRadicado"."_"."$actaNo"."_"."$yearact.pdf";
				$radicados = $Anulacion->genAnulacion($radAnularE,$dependencia,$usua_doc,$observaE,$codusuario,$actaNo,$noArchivo,$where_depe,$where_TipoRadicado,$tipoRadicado, $rsk->fields["0"]);
	//include_once "$ruta_raiz/include/tx/Historico.php";
    	$Historico = new Historico($db);
	/** Funcion Insertar Historico
         * para la ruta old/include/tx/Historico.php
	 *$radicados,  $depeOrigen , $usCodOrigen,$rolOrigen, $depeDestino,$usCodDestino,$rolDestino, $observacion, $tipoTx
	 *
         *
         */
		$radicados=$Historico->insertarHistorico($radAnularE,$dependencia,$codusuario,$rolOrigen,$dependencia,$codusuario,$rolOrigen,$observa,26);
		//Modificacion para log
		$log->setUsuaCodi($codusuario);
        	$log->setDepeCodi($dependencia);
        	$log->setRolId($rolOrigen);
        	$log->setDenomDoc($doc='Radicado');
        	$log->setAction('document_canceled');
        	$log->setOpera("Anulacion del radicado");
        	$log->setNumDocu($radAnularE);
        	$log->registroEvento();


                define('FPDF_FONTPATH','../fpdf/font/');
		$radAnulados = join(",", $radAnularE);
		error_reporting(7);
		$radicadosPdf = "<table WIDTH='685px'>
		<tr><td align='left'><b>Radicado</b>
		</td><td align='left'><b>Observacion Solicitante</b></td>
		<td align='left'><b>Dependencia Solicitante</b></td></tr>";
		foreach($radAnularE as $id=>$noRadicado)
		{
	  		$radicadosPdf .= "<tr><td align='left'>".$radAnularE[$id] ."&nbsp;&nbsp; &nbsp;</td><td align='left'>". $radObservaE[$id] ."</td>
			<td>{$depSolicitaE[$id]}</td></tr>";
		}
		$anoActual = date("Y");
		$radicadosPdf .= "</table>";
		error_reporting(7);
		$ruta_raiz = "..";
		//include ("$ruta_raiz/fpdf/html2pdf.php");

	$fecha = date("d-m-Y");
	$fecha_hoy_corto = date("d-m-Y");
	include "$ruta_raiz/class_control/class_gen.php";
	$b = new CLASS_GEN();
	$date =  date("m/d/Y");
        include_once "$ruta_raiz/../core/config/config-inc.php";
	$fecha_hoy =  $b->traducefecha($date);
  	$html = "
		<!DOCTYPE html >
                 <html lang='es'><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
                 <STYLE>
		     body {
                   	font-family: Arial, Helvetica, sans-serif;
                   	font-size: 13px;
                     }
		     .titulos{
			text-align:center;
		     }
		     .cuerpo{
			text-align:justify;
		     }
                </STYLE>

                    </head>
                 <body>
                <div class='titulos'>
		<br><br>
		<b>ACTA DE ANULACI&Oacute;N No.  $actaNo<br>
		NUMEROS DE RADICACI&Oacute;N DE CORRESPONDENCIA ANULADA<br>
		GRUPO DE GESTI&Oacute;N DOCUMENTAL Y ACTIVOS FIJOS</b></div><br><br>

                <table class='cuerpo'><tr><td width='10px'></td><td>
            En cumplimiento a lo establecido en el Acuerdo No. 060 del 30 de octubre de 2001 expedido
	   por el Archivo General de la Naci&oacute;n, en el cual se establecen pautas para la administraci&oacute;n
	   de comunicaciones oficiales en las entidades públicas y privadas que cumplen funciones
	   p&uacute;blicas, y con base especialmente en el par&aacute;grafo del Artículo Quinto, el cual establece
	   que:<br><br>

	    Cuando existan errores en la radicación y se anulen los números, se debe dejar constancia
	    por escrito, con la respectiva justificación y firma. Para tal efecto el Coordinador(a) de
	    Gestión Documental procede a
	    anular los siguientes números de radicaci&oacute;n de $TituloActam que no fueron tramitados por las
	    Dependencias Radicadoras:

                    </td><td width='5px'></td></tr></table>
                     <table  WIDTH='585px' class='cuerpo'><tr><td width='10px'></td><td><p align='justify'>
		1.- N&uacute;meros de radicaci&oacute;n de $TituloActam a anular:
		</p>
		</td><td width='5px'></td></tr></table><br><br>
		$radicadosPdf
		<br>
		<table  WIDTH='585px' class='cuerpo'><tr><td width='10px'></td><td>
		2.- Se deja  copia  de la presente acta en el archivo central de la  Entidad  para  el tr&aacute;mite
		     respectivo de la organizaci&oacute;n f&iacute;sica de los archivos.
			 <br><br>
		Se firma la presente el $fecha_hoy

		<br><br><br>
		______________________________________________________ <br>
		$usua_nomb<BR>
		Coordinador(a) de Gestión Documental<br>
		$entidad_largo_Planilla
                 </td><tdwidth='5px'></td></tr></table></body></html>";

  	$ruta_raiz = "..";
	$noArchivo = RUTA_BODEGA.$noArchivo;
	include_once("../../extra/MPDF57/mpdf.php");
        $mpdf=new mPDF('es-ES','Letter',0, '', 18.7, 18.7, 45, 30.7, 8, 8);
	$mpdf->PDFA=true;
        $mpdf->PDFAauto = true;
	$real_header="<table style='border-collapse:collapse;border-spacing:0;padding:0;width:100%;'>
                <tr><td valign='top' align='left'></td>
                <td valign='top' align='right'></td></tr></table>";
        $real_footer="<table style='border-collapse:collapse;border-spacing:0;padding:0;width:100%;font-family: Arial, Helvetica, sans-serif;font-size:8'>
                <tr><td align='left' valign='middle'> $entidad_largo <br>
                $entidad_dir <br>
                Tel: $entidad_tel <br>
                </td>
                <td valign='top' align='right'></td></tr>
                </table>";
        $mpdf->SetHTMLHeader($real_header);
        $mpdf->SetHTMLFooter($real_footer);
        $mpdf->WriteHTML($html);
  	$arpdf_tmp = RUTA_BODEGA."pdfs/planillas/envios/$krd"."_lis_IMP.pdf";
  	$arpdf_tmp2 = "pdfs/planillas/envios/$krd"."_lis_IMP.pdf";

  	/* If destination directory doesn't exist is created in this point */
  	if(!file_exists(RUTA_BODEGA.'/pdfs/planillas/')){
  		mkdir(RUTA_BODEGA.'/pdfs/planillas/',0777,true);
  	}

   	$mpdf->Output($noArchivo,'F');
	$encrypt=new file();
	$flpdf=$encrypt->encriptar($noArchivo);
}
}
?></div>

<?php if($radAnularE) { ?>
		<center>Ver Acta <a href='<?php echo $ruta_raiz;?>/../core/vista/image.php?nombArchivo=<?php echo $flpdf;?>' target='image' >Acta No <?php echo $actaNo?> </a></center>
<?php exit;

}
?>

</td></tr>
</table>
</form>
