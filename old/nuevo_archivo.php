<?php $ruta_raiz = ".";
if ($session_status != 'ok') {
    session_start();
    date_default_timezone_set('America/Bogota');
    date_default_timezone_set('America/Bogota');
    include_once "$ruta_raiz/include/db/ConnectionHandler.php";
    //   include "$ruta_raiz/tx/validar.php";
}
include_once $ruta_raiz . "/../core/config/config-inc.php";
/**
 * Modificacion Variables Globales Infometrika 2009-05
 * Licencia GNU/GPL 
 */
foreach ($_GET as $key => $valor)
    $$key = $valor;
foreach ($_POST as $key => $valor)
    $$key = $valor;
$krd = $_SESSION["krd"];
$dependencia = $_SESSION["dependencia"];
$usua_doc = $_SESSION["usua_doc"];
$codusuario = $_SESSION["codusuario"];
$tpNumRad = $_SESSION["tpNumRad"];
$tpPerRad = $_SESSION["tpPerRad"];
$tpDescRad = $_SESSION["tpDescRad"];
$tip3Nombre = $_SESSION["tip3Nombre"];
$id_rol = $_SESSION["id_rol"];

//Cargando variables del log
if (isset($_SERVER['HTTP_X_FORWARD_FOR'])) {
    $proxy = $_SERVER['HTTP_X_FORWARD_FOR'];
}
else
    $proxy = $_SERVER['REMOTE_ADDR'];
$REMOTE_ADDR = $_SERVER['REMOTE_ADDR'];
/* $ruta_raiz2=$ruta_raiz;
  $ruta_raiz='..';
  include_once "$ruta_raiz/core/clases/log.php";
  $log = new log($ruta_raiz);
  $log->setAddrC($REMOTE_ADDR);
  $log->setProxyAd($proxy);
  $ruta_raiz=$ruta_raiz2;
  $log->setUsuaCodi($codusuario);
  $log->setDepeCodi($dependencia);
  $log->setRolId($id_rol);
 */
include_once "$ruta_raiz/class_control/AplIntegrada.php";
if (!$ent)
    $ent = substr(trim($numrad), strlen($numrad) - 1, 1);
$nombreTp3 = $tip3Nombre[3][$ent];
/* print_r($_GET);
  echo "<hr> post";
  print_r($_POST);
  echo "<hr>"; */
if (!$db)
    $db = new ConnectionHandler($ruta_raiz);
$db->conn->SetFetchMode(ADODB_FETCH_NUM);
$dbAux = new ConnectionHandler($ruta_raiz);
$dbAux->conn->SetFetchMode(ADODB_FETCH_NUM);
$dbAux->conn->SetFetchMode(ADODB_FETCH_ASSOC);
$db->conn->SetFetchMode(ADODB_FETCH_NUM);
$db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
if ($_SESSION['usua_debug'] == 1)
    $db->conn->debug = true;

if (!$ent)
    $ent = substr(trim($numrad), strlen($numrad) - 1, 1);
$nombreTp3 = $tip3Nombre[3][$ent];

$objApl = new AplIntegrada($dbAux);
$conexion = & $db;
$rowar = array();
$mensaje = "";
$tipoDocumento = explode("-", $tipoLista);
$tipoDocumentoSeleccionado = $tipoDocumento[1];
//$isql = "select USUA_LOGIN,USUA_PASW,CODI_NIVEL from usuario where (usua_login ='$usua') ";
$isql = "SELECT 
  usuario.usua_login, 
  sgd_mod_modules.sgd_mod_modulo, 
  usuario.USUA_PASW,
  usuario.usua_doc, 
  usuario.usua_nomb, 
  sgd_drm_dep_mod_rol.sgd_drm_valor as CODI_NIVEL, 
  sgd_urd_usuaroldep.depe_codi, 
  sgd_urd_usuaroldep.rol_codi
FROM 
  usuario, 
  sgd_urd_usuaroldep, 
  sgd_mod_modules, 
  sgd_drm_dep_mod_rol
WHERE 
  usuario.usua_codi = sgd_urd_usuaroldep.usua_codi AND
  sgd_mod_modules.sgd_mod_id = sgd_drm_dep_mod_rol.sgd_drm_modecodi AND
  sgd_drm_dep_mod_rol.sgd_drm_rolcodi = sgd_urd_usuaroldep.rol_codi AND
  sgd_drm_dep_mod_rol.sgd_drm_depecodi = sgd_urd_usuaroldep.depe_codi AND
  sgd_mod_modules.sgd_mod_estado = 1 AND 
  usuario.usua_codi = $codusuario AND 
sgd_drm_dep_mod_rol.sgd_drm_rolcodi = $id_rol AND
  sgd_drm_dep_mod_rol.sgd_drm_depecodi = $dependencia AND
  sgd_mod_modules.sgd_mod_modulo = 'codi_nivel'";
$rs = $db->query($isql);

if ($rs->EOF) {
    $mensaje = "No tiene permisos para ver el documento";
} else {
    $nivel = $rs->fields["CODI_NIVEL"];
    ($tipo == 0) ? $psql = " where  anex_tipo_codi<20 or  anex_tipo_codi>23 " : $psql = " ";
    $isql = "select ANEX_TIPO_CODI,
				ANEX_TIPO_DESC, 
				ANEX_TIPO_EXT " .
            "from anexos_tipo  $psql order by ANEX_TIPO_CODI asc";
    $rs = $db->query($isql);
}
if ($resp1 == "OK") {
    if ($subir_archivo) {
        $mensaje = "<span class=info>Archivo anexado correctamente</span></br>";
        if ($idjbpm) {
            /**
             * @author cgprada
             * @fecha 20/05/2010
             * 
             * Modificado para realizar la ejecucion utilizando esquema Dinamico
             * 
             */
            //echo "<hr>1";
            include_once "$ruta_raiz/webServices/Cliente/FunWsImpl/AnexarArchivo.php";
            //echo "2<hr>";
        }
    }
    else
        $mensaje = "Anexo Modificado Correctamente<br>No se anex&oacute; ning&uacute;n archivo</br>";
}
else if ($resp1 == "ERROR") {
    $mensaje = "<span class=alarmas>Error al anexar archivos</span></br>";
}

include "radicacion/crea_combos_universales.php";
if (!function_exists(return_bytes)) {

    /**
     * Retorna la cantidad de bytes de una expresion como 7M, 4G u 8K.
     *
     * @param char $var
     * @return numeric
     */
    function return_bytes($val) {
        $val = trim($val);
        $ultimo = strtolower($val{strlen($val) - 1});
        switch ($ultimo) {
            // El modificador 'G' se encuentra disponible desde PHP 5.1.0
            case 'g': $val *= 1024;
            case 'm': $val *= 1024;
            case 'k': $val *= 1024;
        }
        return $val;
    }

}
/**  Modificado 13-nov-2009
 * @author hardy deimont niño
 * @Desc  se actulizo la consulta para que traiga el asunto del padre. y se inicializa variables
 */
$consultaESP = "select r.EESP_CODI,r.RA_ASUN from radicado r where r.radi_nume_radi = $numrad";
$rsESP = $db->query($consultaESP);
$Asunto = $rsESP->fields['RA_ASUN'];
$codigoESP = $rsESP->fields["EESP_CODI"];
$consultaRUPS = "select FLAG_RUPS from bodega_empresas where ARE_ESP_SECUE = $codigoESP";
$rsESPRUPS = $db->query($consultaRUPS);
$espEnRUPS = $rsESPRUPS->fields["FLAG_RUPS"];
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title>Informaci&oacute;n de Anexos</title>
        <link rel="stylesheet" href="estilos/orfeo.css">
        <SCRIPT Language="JavaScript" SRC="js/crea_combos_2.js"></SCRIPT>
        <SCRIPT Language="JavaScript" SRC="js/jsAjax.js"></SCRIPT>
        <SCRIPT Language="JavaScript" SRC="<?php echo  $ruta_raiz; ?>/js/trim.js"></SCRIPT>
        <script language="javascript">
<?php // Convertimos los vectores de los paises, dptos y municipios creados en crea_combos_universales.php a vectores en JavaScript.
echo arrayToJsArray($vpaisesv, 'vp');
echo arrayToJsArray($vdptosv, 'vd');
echo arrayToJsArray($vmcposv, 'vm');
$swIntegraAps = $objApl->comboRadiAplintegra($usua_doc);
$objApl->comboRadiAplisel();
?>
            window.onbeforeunload = function exitAlert()
            {
                //   var textillo = "Los datos que no se han guardado se perderan.";
                //alert('Utilice el boton cerrar');
                //return false;
                window.opener.location.reload();
            }
            //var textillo = "Los datos que no se han guardado se perderan.";
            //return textillo;


            function borrarArchivo(variables, copiaX, tpradic, aplinteg) {
                radRem = document.getElementById('radicado_rem' + copiaX).checked;
                radicado_salida = document.getElementById('radicado_salida').checked;
                alert(radRem);
                if (radRem == false) {
                    document.formulario.action = document.formulario.action + variables + "&borrar=" + copiaX + "&tpradic=" + tpradic + "&aplinteg=" + aplinteg + "&radicado_salida=" + radicado_salida;
                    document.formulario.submit();
                } else {
                    alert('Accion NO PERMITIDA, Si desea borrar Debe selecionar otro Destinatario');
                }

            }
            function continuar_grabar()
            {
<?php 
if ($swIntegraAps != "0") {
    ?>
                    document.formulario.aplinteg.disabled = false;
<?php  } ?>

                document.formulario.tpradic.disabled = true;
                document.formulario.action = document.formulario.action + "&cc=GrabarDestinatario";
                document.formulario.submit();
            }

            function doc_radicado()
            {

                mostrarForm();
                swSelRadic = 0;
                for (n = 1; n < document.formulario.tpradic.length; n++)
                    if (document.formulario.tpradic.options[n].selected) {
                        swSelRadic = 1;
                    }
                if (!document.formulario.radicado_salida.checked) {
                   // alert('entro');
                    //document.getElementById("reservar").style.display="none";
                    document.formulario.tpradic.disabled = false;
                    eval(document.formulario.elements['tpradic'].options[0] = new Option('- Tipos de Radicacion -', 'null'));
                    document.formulario.elements['tpradic'].options[0].selected = true;
                    document.formulario.elements['tpradic'].disabled = true;
                    comboRadiAplintegra(document.formulario, 'null', 'aplinteg');

                    //document.formulario.tpradic.disabled=true;
<?php  if ($swIntegraAps != "0") { ?>
                        document.formulario.aplinteg.disabled = true;
<?php  } ?>
                } else {
                    //document.getElementById("reservar").style.display="block";
                   // alert('entro1');
                    document.formulario.tpradic.disabled = false;
<?php 
//SI Puede integrar aplicativos
if ($radi) {
    $ent = substr(trim($radi), -1);
}
//echo ("*** $swIntegraAps ***");
if ($swIntegraAps != "0") {
    ?>
                        document.formulario.aplinteg.disabled = false;
    <?php 
    //si maneja prioridades y no es el primer documento es decir el soporte
    if ($swIntegraAps != "OK") {
        ?>
                            if (swSelRadic == 0) {
                                for (n = 0; n < document.formulario.tpradic.length; n++) {
                                    if (document.formulario.tpradic.options[n].value == '<?php echo  $swIntegraAps ?>') {
                                        document.formulario.tpradic.options[n].selected = true;
                                        comboRadiAplintegra(document.formulario,<?php echo  $swIntegraAps ?>, 'aplinteg');
                                        //document.formulario.elements['tpradic'].disabled=true;
                                        //document.formulario.elements['aplinteg'].disabled=true;
                                    }
                                }
                            }
    <?php  } else {
        ?>
                            if (swSelRadic == 0) {
                                for (n = 0; n < document.formulario.tpradic.length; n++) {
                                    if (document.formulario.tpradic.options[n].value == 1) {
                                        document.formulario.tpradic.options[n].selected = true;
                                    }
                                }
                            }
        <?php 
    }
} else {
    ?>

                        if (swSelRadic == 0) {
                            for (n = 0; n < document.formulario.tpradic.length; n++) {
                                if (document.formulario.tpradic.options[n].value == 1) {
                                    document.formulario.tpradic.options[n].selected = true;
                                }
                            }
                        }

<?php  } ?>
<?php  if (($verunico == 1) && ($ent != 2)) { ?>
    //alert(4);
                        //eval(document.formulario.elements['tpradic'].options[0] = new Option('Salida', '1'));
                        //eval(document.formulario.elements['tpradic'].options[0] = new Option('Salida', '1'));
                        document.formulario.elements['tpradic'].options[<?php echo $ent-1;?>].selected = true;
                        document.formulario.elements['tpradic'].disabled = true;
    <?php  if ($swIntegraAps != "0") { ?>
                            eval(document.formulario.elements['aplinteg'].options[0] = new Option('No integra', 'null'));
                            document.formulario.elements['aplinteg'].options[0].selected = true;

    <?php  } ?>
<?php  } ?>

                }


            }

            function cerrar(forma) {
                if (forma == 1) {
                    opener.regresar();
                    window.close();
                } else {
                    opener.regresar();
                    window.close();
                }
            }

            function escogio_archivo()
            {
                var largo;
                var valor;
                archivo_up = document.getElementById('userfile').value;
                valor = 0;
                var mySplitResult = archivo_up.split(".");

                for (i = 0; i < (mySplitResult.length); i++) {
                    extension = mySplitResult[i];
                }
                extension = extension.toLowerCase();
<?php 
while (!$rs->EOF) {
    echo "
                       if (extension=='" . $rs->fields["ANEX_TIPO_EXT"] . "')
                               {valor=" . $rs->fields["ANEX_TIPO_CODI"] . ";
                       }\n";

    $rs->MoveNext();
}
$anexos_isql = $isql;
?>
                if (valor == 0) {
                    alert('El archivo no corresponde a un tipo de anexo valido \npor favor mirar el listado');
                    document.getElementById('userfile').value = '';
                    return false;
                }
                document.getElementById('tipo_clase').value = valor;
                if (document.getElementById('radicado_salida').checked == true && valor != 14 && valor != 16 && valor != 24)
                {
                    alert("Atenci\363n. Si el archivo no es DOCX, ODT o XML no podr\341 realizar combinaci\363n de correspondencia. \n\n otros archivos no facilitan su acceso");
                }
            }

            function validarGenerico()
            {
                if (document.formulario.radicado_salida.checked && document.formulario.tpradic.value == 'null') {
                    alert("Debe seleccionar el tipo de radicacion");
                    return false;
                }

                archivo = document.getElementById('userfile').value;
                if (archivo == "")
                {
<?php if ($tipo == 0 and !$codigo) {
    echo "alert('Por favor escoja un archivo'); return false;";
} else {
    echo "return true;";
}
?>
                }
                // alert('GENERICO SALE');
                copias = document.getElementById('i_copias').value;
                /*if  (copias==0 && document.getElementById('radicado_salida').checked==true   )
                 {//alert ('sIN DESTINATARIO');
                 document.getElementById('radicado_salida').checked=false;
                 }
                 */
                return true;
            }

            function validar_rgroup(rgroup) {
                var algo_pinchado;
                algo_pinchado = 'no';
                for (var i = 0; i < rgroup.length; i++) {
                    if (rgroup[i].checked) {
                        algo_pinchado = 'si';
                        break;
                    }
                }
                return algo_pinchado;
            }

            function actualizar() {
                if (!validarGenerico())
                    return;

                radRem = validar_rgroup(formulario.radicado_rem);
                if (radRem == 'no') {
                    alert('Selecione un destinatario');
                    return false;
                }


                var integracion = document.formulario.tpradic.value;
                if (integracion == '5') {
                    //Selecciona Resolucion, luego va para sancionados
<?php 
if ($swIntegraAps != "0") {
    if ($espEnRUPS != 'S') {
        //Tiene integración pero no está en RUPS
        ?>
                            if (document.getElementById('rempre').checked == true) {
                                //y Efectivamente selecciono ESP como destinatario, no puede ingresar a sancionado por no cumplir con RUPS
                                alert("No puede ingresar sancionado, porque la ESP no se encuentra en RUPS. \nCambie el tipo de radicaci\363n o el destinatario.");
                                return false;
                            } else {
                                //No selecciona ESP, luego no puede sancionar
                                alert("Los sancionados solo pueden ir dirigidos a las ESPs.");
                                return false;
                            }
        <?php 
    }
    ?>

                        document.formulario.aplinteg.disabled = false;

<?php  } ?>
                }

                document.formulario.radicado_salida.disabled = false;
                document.formulario.tpradic.disabled = false;
                /*	if (document.formulario.reservado.checked == true)
                 document.formulario.reserva.value = 'S'
                 if (document.formulario.reservado.checked == false)
                 document.formulario.reserva.value = 'N'
                 document.getElementById("reservar").style.display="block";*/
                document.formulario.submit();


            }
            /*
             function mostrando(nombreCapa)
             {
             //alert (nombreCapa);
             if(nombreCapa != 2)
             {
             document.getElementById("reservar").style.display="block";
             }
             if(nombreCapa == 2)
             {
             document.getElementById("reservar").style.display="none";
             //}
     
             }*/


        </script>
    </head>
    <body bgcolor="#FFFFFF" topmargin="0" >
        <div id="padre">

            <?php 
            $i_copias = 0;
            if ($codigo) {
                $isql = "select CODI_NIVEL
			,ANEX_SOLO_LECT
			,ANEX_CREADOR
			,ANEX_DESC
			,ANEX_TIPO_EXT
			,ANEX_NUMERO
			,ANEX_RADI_NUME 
			,ANEX_NOMB_ARCHIVO nombre
			,ANEX_SALIDA,ANEX_ESTADO,SGD_DIR_TIPO,RADI_NUME_SALIDA,SGD_DIR_DIRECCION from anexos, anexos_tipo,radicado " .
                        "where anex_codigo='$codigo' and anex_radi_nume=radi_nume_radi and anex_tipo=anex_tipo_codi";
                //$db->conn->SetFetchMode(ADODB_FETCH_NUM);
                $rs = $db->query($isql);
                if (!$rs->EOF) {
                    $docunivel = ($rs->fields["CODI_NIVEL"]);
                    $sololect = ($rs->fields["ANEX_SOLO_LECT"] == "S");
                    $remitente = $rs->fields["SGD_DIR_TIPO"];
                    $extension = $rs->fields["ANEX_TIPO_EXT"];
                    $radicado_salida = $rs->fields["ANEX_SALIDA"];
                    $anex_estado = $rs->fields["ANEX_ESTADO"];
                    $descr = $rs->fields["ANEX_DESC"];
                    $radsalida = $rs->fields["RADI_NUME_SALIDA"];
                    $direccionAlterna = $rs->fields["SGD_DIR_DIRECCION"];
                }
            }
            $datos_envio = "&otro_us11=$otro_us11&codigo=$codigo&dpto_nombre_us11=$dpto_nombre_us11&direccion_us11=" . urlencode($direccion_us11) . "&muni_nombre_us11=$muni_nombre_us11&nombret_us11=$nombret_us11";
            $datos_envio.="&otro_us2=$otro_us2&dpto_nombre_us2=$dpto_nombre_us2&muni_nombre_us2=$muni_nombre_us2&direccion_us2=" . urlencode($direccion_us2) . "&nombret_us2=$nombret_us2";
            $datos_envio.="&dpto_nombre_us3=$dpto_nombre_us3&muni_nombre_us3=$muni_nombre_us3&direccion_us3=" . urlencode($direccion_us3) . "&nombret_us3=$nombret_us3";
            if ($idjbpm != Null) {
                $variableJbpm = "nurad=$nurad&idjbpm=$idjbpm&expediente=$expediente&dependencia=$dependencia&coddepe=$coddepe&nombreAnexo=$nombreAnexo";
            }
            $variables = "nuevo_archivo=$nuevo_archivo&ent=$ent&radi=$radi&krd=$krd&" . session_name() . "=" . trim(session_id()) . "&usua=$krd&contra=$drde&tipo=$tipo&ent=$ent&codigo=$codigo$datos_envio&numrad=$numrad&$variableJbpm";

            if ($_SESSION['usua_debug'] == 1) {
                echo '<hr>Variables Post';
                print_r($_POST);
                echo '<hr>Variables Get';
                print_r($_GET);
                echo '<hr>';
            }
            ?>
            <div id="vista1">
                <form enctype="multipart/form-data" method="post" name="formulario"
                      id="formulario" action='upload2.php?<?php echo  $variables ?>'>

                    <table width="100%" border="0" cellspacing="0" cellpadding="0" >
                        <tr>
                            <td><input type="hidden" name="usua" value="<?php echo  $usua ?>"> <input
                                    type="hidden" name="contra" value="<?php echo  $contra ?>"> <input
                                    type="hidden" name="anex_origen" value="<?php echo  $tipo ?>"> <input
                                    type="hidden" name="tipo" value="<?php echo  $tipo ?>"> <input type="hidden"
                                    name="tipoLista" value="<?php echo  $tipoLista ?>"> <input type="hidden"
                                    name="krd" value="<?php echo  $krd ?>"> <input type="hidden"
                                    name="tipoDocumentoSeleccionado"
                                    value="<?php echo $tipoDocumentoSeleccionado; ?>">
                                    <!---<input 	type="hidden" name="reserva" value="">-->
                                <div align="center">
                                    <table width="100%" align="center" border="0" cellpadding="0" cellspacing="1" class="borde_tab">
                                        <tr>
                                            <td height="25" align="center" class="titulos4" colspan="2">DESCRIPCION
                                                DEL DOCUMENTO</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table border=0 width=100% class="borde_tab" >
                                                    <!--DWLayoutTable-->
                                                    <tr>
                                                        <td class="titulos2"  height="25" align="left" colspan="2">
                                                            ATRIBUTOS</td>
                                                    </tr>
                                                    <tr>
                                                        <td  width=50% class="listado2"><input type="checkbox"
                                                                                               class="select" name="sololect"
                                                                <?php if ($tipo == 1) {
                                                                    echo " checked ";
                                                                } ?> id="sololect"> Solo
                                                            lectura</td>
                                                        <td  align="left" class="listado2">Tipo de Anexo: <select name="tipo" class="select" id="tipo_clase">
                                                                <?php 
                                                                $db->conn->SetFetchMode(ADODB_FETCH_NUM);
                                                                $rs = $db->query($anexos_isql);

                                                                while (!$rs->EOF) {

                                                                    //if ( ( $extension == 'docx' && $rs->fields[0]==18) || ( $extension == 'odt' && $rs->fields[0]==14) || ($extension == 'doc' && $rs->fields[0]==1) || ($extension == 'xml' && $rs->fields[0]==16)) {
                                                                    if ($extension == $rs->fields[2]) {
                                                                        $datoss = " selected ";
                                                                    } else {
                                                                        $datoss = "";
                                                                    }
                                                                    ?>
                                                                    <option value="<?php echo  $rs->fields[0] ?>" <?php echo  $datoss ?>>
                                                                <?php echo  $rs->fields[1] ?>
                                                                    </option>
                                                                <?php 
                                                                $rs->MoveNext();
                                                            }
                                                            ?>
                                                            </select>

<?php 
//Grabando el documento en formato <b>(Rich Text Format (rtf), txt, html ,pdf, . . )</b>,
//podremos realizar combinacion de correspondencia, ya que estos formatos permiten acceso al codigo.
//<br><i>El formato .doc no permite realizar este proceso. <a href='http://www.gnu.org/philosophy/no-word-attachments.es.html' target='softwarepropietario'>Mas Informaciï¿½n</a></i>
?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td  colspan="2" class="listado2">
                                                            <table border=0 width=100%>
                                                                <tr>
                                                                    <td width=50% class="listado2">
                                                                        <?php 
                                                                        $us_1 = "";
                                                                        $us_2 = "";
                                                                        $us_3 = "";
                                                                        $datoss = "";

                                                                        if ($nombret_us11 and $direccion_us11 and $dpto_nombre_us11 and $muni_nombre_us11) {
                                                                            $us_1 = "si";
                                                                            $usuar = 1;
                                                                            if ($remitente == 1) {
                                                                                $datoss1 = " checked ";
                                                                            }
                                                                        } else {
                                                                            $datoss1 = " disabled ";
                                                                        }
                                                                        ?>

                                                                        <?php 
                                                                        $datoss = "";
                                                                        if ($nombret_us2 and $direccion_us2 and $dpto_nombre_us2 and $muni_nombre_us2) {
                                                                            $us_2 = "si";
                                                                            $predi = 1;
                                                                            if ($remitente == 2) {
                                                                                $datoss2 = " checked  ";
                                                                            }
                                                                        } else {
                                                                            $datoss2 = " disabled ";
                                                                        }
                                                                        ?>

                                                                        <?php 
                                                                        $datoss = "";
                                                                        if ($nombret_us3 and $direccion_us3 and $dpto_nombre_us3 and $muni_nombre_us3) {
                                                                            $us_3 = "si";
                                                                            $empre = 1;
                                                                            if ($remitente == 3) {
                                                                                $datoss3 = " checked  ";
                                                                            }
                                                                        } else {
                                                                            $datoss3 = " disabled ";
                                                                        }
                                                                        ?>
<?php  if ($remitente == 7) $datoss4 = " checked  ";
else $datoss4 = ""; ?>

                                                                        <?php 
                                                                        if ($us_1 or $us_2 or $us_3) {
                                                                            ?>
    <?php 
    if ($radicado_salida)
        $datoss = " checked ";
    else
        $datoss = " ";
    $swDischekRad = "";
    if (strlen(trim($radsalida)) > 0)
        $swDischekRad = "disabled=true";
    $datoss = $datoss . $swDischekRad;
    ?>
                                                                            <input type="checkbox" class="select" name="radicado_salida" '<?php echo  $datoss ?>' value="radsalida" 	'
                                                                            <?php                                                                             if (!$radicado_salida and $ent == 1)
                                                                                $radicado_salida = 1;
                                                                            if ($radicado_salida == 1) {
                                                                                echo " checked ";
                                                                            }
                                                                            ?>' onChange="doc_radicado()" 	id="radicado_salida"> Este documento ser&aacute; radicado
                                                                            <?php 
                                                                        } else {
                                                                            ?>
                                                                            Este documento no puede ser radicado ya que faltan datos.<br>
                                                                            (Para envio son obligatorios Nombre, Direccion, Departamento,
                                                                            Municipio)
                                                                            <?php 
                                                                        }
                                                                        ?>
                                                                    </td>
                                                                    <td class="listado2">
                                                                        <?php 
                                                                        $comboRadOps = "";
                                                                        $eventoIntegra = "";
                                                                        if ($swIntegraAps != "0") {
                                                                            //	$eventoIntegra = "comboRadiAplintegra(document.formulario,this.value,'aplinteg');";
                                                                            $eventoIntegra = "onchange=comboRadiAplintegra(document.formulario,this.value,'aplinteg');";
                                                                        }
                                                                        // if ($ent!=1 && !$radicado_salida ){

                                                                        if ($ent != 1 and $datoss != " checked ") {
                                                                            $deshab = " disabled=true ";
                                                                        }
                                                                        //if (strlen(trim($swDischekRad)) > 0)
                                                                        //	$deshab = $swDischekRad;
                                                                        /* if ($eventoIntegra)
                                                                          $mostrarReserva = "mostrando(this.value)";
                                                                          if (!$eventoIntegra)
                                                                          $mostrarReserva = "onchange=mostrando(this.value)";
                                                                         */
                                                                        $comboRad = "<select name='tpradic' class='select' $deshab  $eventoIntegra$mostrarReserva >";
                                                                        $comboRadSelecc = "<option selected value='null'>- Tipos de Radicacion -</option>";

                                                                        $sel = "";
                                                                        foreach ($tpNumRad as $key => $valueTp) {

                                                                            if (strcmp(trim($tpradic), trim($valueTp)) == 0) {
                                                                                $sel = "selected";
                                                                                $comboIntSwSel = 1;
                                                                            }
                                                                            // verunico
                                                                            //Si se definio prioridad en algun tipo de radicacion
                                                                            $valueDesc = $tpDescRad[$key];
                                                                            //echo "==$valueTp"         ;
                                                                            if ($radicado_salida == $valueTp) {
                                                                                $sel = "selected";
                                                                            }
                                                                            if ($tpPerRad[$valueTp] == 2 or $tpPerRad[$valueTp] == 3) {
                                                                                $comboRadOps .="<option value='" . $valueTp . "' $sel>" . $valueDesc . "</option>";
                                                                            }
                                                                            $sel = "";
                                                                        }
                                                                        $comboRad = $comboRad . $comboRadSelecc . $comboRadOps . "</select>";
                                                                        ?>
                                                                        Radicacion  <?php echo  $comboRad ?> <BR>
<?php  if ($swIntegraAps != "0") { ?>
                                                                            Integra
                                                                            <select name='aplinteg' class='select' disabled='true'>
                                                                                <option selected value='null'>--- Aplicacion ---</option>
                                                                            </select>
<?php 
}
if ($aplinteg)
    echo ("<script>
	   	  		  comboRadiAplisel(document.formulario,$aplinteg,'aplinteg');
	             </script>

	   	  ")
    ;
if ($ent == 1) {
    echo ("<script>");
    //echo ("document.formulario.radicado_salida.checked=true;");
    echo ("doc_radicado();");
    echo ("</script>");
}

if (strlen(trim($swDischekRad)) > 0) {
    echo ("<script>");
    //echo ("document.formulario.radicado_salida.checked=true;");
    echo ("document.formulario.tpradic.disabled=true;");
    echo ("</script>");
}
?>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr >
                                                        <td >
                                                            <table width="100%" align="center" border="0" cellpadding="0"
                                                                   cellspacing="2" class="borde_tab" id="anexaExp"
                                                                   style="display: none">
                                                                <tr>
                                                                    <td class="titulos2" width="50%">Guardar en Expediente:</td>
                                                                    <td valign="top" class="listado2">
                                                                        <table border="0" class="borde_tab" align="center"
                                                                               class="titulos2">
                                                                            <tr>		  
<?php $q_exp = "SELECT  SGD_EXP_NUMERO as valor, SGD_EXP_NUMERO as etiqueta, SGD_EXP_FECH as fecha";
$q_exp .= " FROM SGD_EXP_EXPEDIENTE ";
$q_exp .= " WHERE RADI_NUME_RADI = " . $numrad;
$q_exp .= " AND SGD_EXP_ESTADO <> 2";
$q_exp .= " ORDER BY fecha desc";
$rs_exp = $db->conn->query($q_exp);

if ($rs_exp->RecordCount() == 0) {
    $mostrarAlerta = "<td align=\"center\" class=\"titulos2\">";
    $mostrarAlerta .= "<span class=\"leidos2\" class=\"titulos2\" align=\"center\">
										<b>EL RADICADO PADRE NO ESTA INCLUIDO  EN UN EXPEDIENTE.</b>
									</span>
									</td>";
    $sqlt = "select RADI_USUA_ACTU,RADI_DEPE_ACTU from RADICADO where RADI_NUME_RADI = '$numrad'";
    $rsE = $db->conn->query($sqlt);
    $depe = $rsE->fields['RADI_DEPE_ACTU'];
    $usua = $rsE->fields['RADI_USUA_ACTU'];


    echo $mostrarAlerta;
} else {
    ?>

                                                                                    <td align="center"  width="50%">
                                                                                        <span class="leidos2"  align="center">
                                                    <?php print $rs_exp->GetMenu('expIncluidoAnexo', $expIncluidoAnexo, false, false, 0, "class='select'", false); ?>
                                                                                        </span></td>

                                                    <?php                                                 }
                                                ?>

                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <td>
                                                <?php                                                 $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);

                                                /* Si viene la variable cc(Boton de destino copia) envia al modulo de grabaciï¿½n de datos  */
                                                if ($cc) {
                                                    //$db->conn->debug = true;
                                                    $nombre_us1 = $_POST['nombre_us1'];
                                                    $prim_apel_us1 = $_POST['prim_apel_us1'];
                                                    $seg_apel_us2 = $_POST['seg_apel_us2'];
                                                    $direccion_us1 = $_POST['direccion_us1'];
                                                    $muni_us1 = $_POST['muni_us1'];
                                                    $codep_us1 = $_POST['codep_us1'];
                                                    $telefono_us1 = $_POST['telefono_us1'];
                                                    $tipo_emp_us1 = $_POST['tipo_emp_us1'];
                                                    $documento_us1 = $_POST['documento_us1'];
                                                    $cc_documento_us1 = $_POST['cc_documento_us1'];
                                                    $mail_us1 = $_POST['mail_us1'];
                                                    //$cc=$_POST['cc'];

                                                    $muni_usX = explode('-', $muni_us1);
                                                    $codep_usX = explode('-', $codep_us1);
                                                    if ($_SESSION['usua_debug'] == 1)
                                                        echo "estos datos donde???($nombre_us1 or $prim_apel_us1 or $seg_apel_us2) and  $direccion_us1 and $muni_us1 and $codep_us1 and $muni_usX[2] and $codep_usX[1]";
                                                    if (($nombre_us1 or $prim_apel_us1 or $seg_apel_us2) and $direccion_us1 and $muni_usX[2] and $codep_usX[1]) {

                                                        $isql = "select case when max(sgd_dir_tipo)>max(sgd_dir_cpcodi)  then  max(sgd_dir_tipo)
            when max(sgd_dir_tipo)<max(sgd_dir_cpcodi)  then  max(sgd_dir_cpcodi)
            when max(sgd_dir_tipo)=max(sgd_dir_cpcodi)  then  max(sgd_dir_cpcodi)
            end num
					from sgd_dir_drecciones	where
					  sgd_anex_codigo= '$codigo'";
                                                        $rs = $db->query($isql);
                                                        if (!$rs->EOF) {
                                                            $NUM = $rs->fields["NUM"];
                                                            if ($NUM < 700)
                                                                $NUM = 0;
                                                            $num_anexos = substr($NUM, 1, 2);
                                                            $nurad = $radi;
                                                            $cpcodi = "0";
                                                            $grbNombresUs1 = " $nombre_us1 $prim_apel_us1  " . $_POST['seg_apel_us1'];
                                                            try {
                                                                include "$ruta_raiz/radicacion/grb_direcciones.php";
                                                                echo "<font size=1 class='alarmas'>Ha sido agregado el destinatario.</font>";
                                                                /* $log->setDenomDoc($doc='Radicado');
                                                                  $log->setOpera("Agregado destinatario a Anexo No. $codigo");
                                                                  $log->setNumDocu($radi);
                                                                  $log->registroEvento(); */
                                                            } catch (Exception $e) {
                                                                echo $e;
                                                            }
                                                        } else {
                                                            echo "<font size=1 class='alarmas'>No se pudo guardar el documento, erro database.";
                                                        }
                                                    } else {
                                                        echo "<font size=1 class='alarmas'>No se pudo guardar el documento, ya que faltan datos.(Los datos Obligatorios de envio, Verifique: Nombre, direcci&oacute;n, departamento, municipio)";
                                                    }
                                                }
                                                /** Borrar destinatario */
                                                if ($borrar) {
                                                    $isql = "delete from sgd_dir_drecciones
	         where
			   sgd_anex_codigo='$codigo' and sgd_dir_cpcodi = $borrar ";
                                                    $rs = $db->query($isql);
                                                    if (!$rs->EOF) {
                                                        //echo "pailas game over";
                                                        echo " $mensaje <br><b><span  class='alarmas' >No puede Eliminar En algun momento Fue Enviado </span></b>";
                                                        //var_dump($isql);		
                                                    }
                                                }
                                                $valida = array(1 => NULL, 2 => NULL, 3 => Null);
                                                /** Nuevo destinatario */
                                                include_once "$ruta_raiz/include/query/queryNuevo_archivo.php";
                                                $isql = $query1;
                                                if ($_SESSION['usua_debug'] == 1)
                                                    $db->conn->debug = true;
                                                //echo $isql;
                                                $rs = $db->query($isql);
                                                // $i_copias  Indica cuantas copias se han aï¿½adido
                                                $i_copias = 0;
                                                $varCount = 4;
                                                $style = array(0 => 'listado1n', 1 => 'listado2');
                                                $color = 0;
                                                $actionx = 1;
                                                while ($rs && !$rs->EOF) {
                                                    $checkX = '';
                                                    $checkX2 = '';
                                                    $i_copias++;
                                                    $sgd_ciu_codigo = "";
                                                    $sgd_esp_codi = "";
                                                    $sgd_oem_codi = "";
                                                    $sgd_ciu_codi = $rs->fields["SGD_CIU_CODIGO"];
                                                    $sgd_dir_direccion = $rs->fields["SGD_DIR_DIRECCION"];
                                                    $sgd_esp_codi = $rs->fields["SGD_ESP_CODI"];
                                                    $sgd_oem_codi = $rs->fields["SGD_OEM_CODIGO"];
                                                    $sgd_dir_tipo = $rs->fields["SGD_DIR_TIPO"];
                                                    $sgd_doc_fun = $rs->fields["SGD_DOC_FUN"];
                                                    $copiaX = $rs->fields["SGD_DIR_CPCODI"];
                                                    $sgdtipo = $rs->fields["SGD_DIR_TIPO"];
                                                    $sgd_dir_codigo = $rs->fields["SGD_DIR_CODIGO"];
                                                    $status = 'on';
                                                    if ($sgdtipo >= 700) {
                                                        $sqlEnv = "select sgd_dir_tipo from sgd_renv_regenvio where sgd_dir_codigo=$sgd_dir_codigo";
                                                        $rsEnvCopi = $db->query($sqlEnv);
                                                        $SgdTipoEnvCopi = $rsEnvCopi->fields["SGD_DIR_TIPO"];
                                                        if ($SgdTipoEnvCopi == $sgdtipo)
                                                            $status = 'off';
                                                    }
                                                    if ($sgd_ciu_codi > 0) {
                                                        $isql = "select SGD_DIR_NOMREMDES NOMBRE,SGD_CIU_APELL1 APELL1,SGD_CIU_APELL2 APELL2,SGD_CIU_CEDULA IDENTIFICADOR,SGD_CIU_EMAIL MAIL,SGD_DIR_DIRECCION  DIRECCION from sgd_ciu_ciudadano c, sgd_dir_drecciones d 
	     		  where c.sgd_ciu_codigo=d.sgd_ciu_codigo and c.sgd_ciu_codigo=$sgd_ciu_codi and sgd_anex_codigo='$codigo' and SGD_DIR_CPCODI = $copiaX";
                                                    }
                                                    if ($sgd_esp_codi > 0) {
                                                        $isql = "select nombre_de_la_empresa NOMBRE, identificador_empresa IDENTIFICADOR,EMAIL MAIL,DIRECCION DIRECCION from bodega_empresas where identificador_empresa=$sgd_esp_codi";
                                                    }
                                                    if ($sgd_oem_codi > 0) {
                                                        $isql = "select sgd_oem_oempresa NOMBRE, SGD_OEM_DIRECCION DIRECCION, sgd_oem_codigo IDENTIFICADOR from sgd_oem_oempresas  where sgd_oem_codigo=$sgd_oem_codi";
                                                    }
                                                    if ($sgd_doc_fun > 0) {
                                                        /* if(isset($sgd_dir_direccion))
                                                          $whdepe="and d.DEPE_NOMB = '$sgd_dir_direccion'";
                                                          else{
                                                          $whdepe="";
                                                          }
                                                          $isql = "select usua_nomb NOMBRE, d.depe_nomb DIRECCION, usua_doc IDENTIFICADOR, usua_email MAIL from usuario u ,dependencia d  where usua_doc='$sgd_doc_fun'
                                                          $whdepe "; */
                                                        $isql = "select u.usua_nomb NOMBRE, d.depe_nomb DIRECCION, u.usua_doc IDENTIFICADOR, u.usua_email MAIL from usuario u,dependencia d,sgd_urd_usuaroldep urd 
                    where usua_doc='$sgd_doc_fun' and u.usua_codi=urd.usua_codi and urd.depe_codi=d.depe_codi";
                                                    }
                                                    $rs2 = $db->query($isql);
                                                    $nombre_otros = "";
                                                    if ($rs2 && !$rs2->EOF)
                                                        if ($copiaX != 1 && $copiaX != 2 && $copiaX != 3) {
                                                            $nombre_otros = $rs2->fields["NOMBRE"]; //." ".$rs2->fields["APELL1"]." ".$rs2->fields["APELL2"];
                                                            $otroDest = "<tr><TD align='center' class='" . $style[$color] . "'> Documento <font size=1>" . $rs2->fields["IDENTIFICADOR"] . " | 
			Nombre 	: $nombre_otros <br />Dirigido a : " . $rs->fields["SGD_DIR_NOMBRE"] . "<br />Direccion : " . $rs2->fields["DIRECCION"] . " | 
			Mail :	" . $rs2->fields['MAIL'] . "</font></TD>";
                                                            if ($sgd_dir_tipo == 7) {
                                                                $checkX = "checked ";
                                                            } elseif ($copiaX == $sgdtipo) {
                                                                $checkX2 = "checked ";
                                                                $actionx = 0;
                                                            }
                                                            if ($status == 'on') {
                                                                $otroDestCheck = "<td class='" . $style[$color] . "'><center><input type='radio' onclick=\"Remitdeshabi('$codigo','$copiaX','div$varCount','div2$varCount','cop$varCount','radicado_rem',$sgdtipo)\" name='radicado_rem' id='radicado_rem$copiaX' value='7' $checkX ></center><div ud='div2$varCount'></div></td>";
                                                                $otroDestCheck.="<td class='" . $style[$color] . "'><center><div id='div$varCount'><input type='checkbox'   name='cop$varCount' value='$varCount'  id='cop$varCount' $checkX2 onclick=\"CopiaPdeshab('$codigo','$copiaX','div$varCount','$actionx','cop$varCount')\" ></div></center></td>";
                                                                $otroDestCheck.="<TD width='68' align='center' class='" . $style[$color] . "'>&nbsp; <font size=1>
				<a href='#$copiaX' onclick=\"borrarArchivo('$variables','$copiaX','$tpradic','$aplinteg');\">Borrar</a></TD>
				</tr>";
                                                            }
                                                            if ($status == 'off') {
                                                                $otroDestCheck = "<td class='" . $style[$color] . "'><center><input disabled type='radio' name='radicado_rem' id='radicado_rem$copiaX' value='7' $checkX ></center><div ud='div2$varCount'></div></td>";
                                                                $otroDestCheck.="<td class='" . $style[$color] . "'><center><div id='div$varCount'><input type='checkbox'   name='cop$varCount' value='$varCount'  id='cop$varCount' $checkX2 disabled ></div></center></td>";
                                                                $otroDestCheck.="<TD width='68' align='center' class='" . $style[$color] . "'>&nbsp; <font size=1></TD></tr>";
                                                            }
                                                            //nuevo_archivo.php?$variables&borrar=$copiaX&tpradic=$tpradic&aplinteg=$aplinteg
                                                            if ($color == 1)
                                                                $color = 0;
                                                            else
                                                                $color = 1;
                                                            $otoroDto.=$otroDest . $otroDestCheck;

                                                            $actionx = 1;
                                                            $varCount++;
                                                        }
                                                        else {
                                                            $valida[$copiaX] = $sgd_dir_tipo;
                                                            $valida2[$copiaX] = $status;
                                                        }
                                                    $rs->MoveNext();
                                                }

//print_r($valida);
//print_r($valida2);
                                                function funjav($codigo, $code, $div, $validar, $habilitar) {
                                                    //return "$codigo,$code,$div,$validar,$habilitar";
                                                    if (strlen($codigo) == 0) {
                                                        return 'disabled';
                                                    }
                                                    if (strtoupper(trim($habilitar)) == strtoupper('disabled')) {
                                                        return $habilitar;
                                                    }
                                                    if ($validar == Null) {
                                                        return "onclick=\"CopiaPAgregar('$codigo',$code,'$div','id$code');\"";
                                                    } else
                                                    if ($validar == 0) {
                                                        return "onclick=\"HaboDES('$codigo',$code,'$div','id$code','habX');\"";
                                                    } else {
                                                        return "onclick=\"HaboDES('$codigo',$code,'$div','id$code','desX');\"";
                                                    }
                                                }
                                                ?>		

                                                <table width="100%" class="borde_tab">
                                                    <tr valign="top">
                                                        <td valign="top" class="titulos2" colspan=2>DESTINATARIO</td>
                                                        <td valign="top" class="titulos3">Copia</td>
                                                        <td valign="top" class="titulos3">Acci&oacute;n</td>


                                                    <tr valign="top">
                                                        <td valign="top" class="listado2">
                                                        <?php echo  $tip3Nombre[1][$ent] ?> | 		
                                                        <?php  echo " - " . substr($nombret_us11, 0, 35) ?>
                                                            <br>
                                                        <?php echo  $direccion_us11 ?>	|	<?php echo  "$dpto_nombre_us11/$muni_nombre_us11" ?>
                                                        </td>
                                                        <td class="listado2">
                                                    <center>
<?php 
iF ($valida2[1] == 'off') {
    $onVar1 = 'disabled';
    $inVar1.='disabled';
} else {
    $onVar1 = funjav($codigo, 1, 'divcp1', $valida[1], $datoss1);
    if (strtoupper(trim($datoss1)) != strtoupper('disabled') and strlen($codigo) != 0) {
        $inVar1 = "onclick='destinRem(\"$codigo\",1,\"divcp1\",";
        if ($valida[1]) {
            $inVar1.=$valida[1];
        } else {
            $inVar1.='"no"';
        }
        $inVar1.=");'";
    }
}
?>
                                                        <input type="radio" name="radicado_rem" id="radicado_rem1" value=0 style="display: none">
                                                        <input type="radio" name="radicado_rem" id="radicado_rem1" value=1
                                            <?php echo  $inVar1 ?> <?php echo  $datoss1 ?>
                                            <?php                                             if (!$radicado_rem and $ent == 1 and $radicado_rem != 7)
                                                $radicado_rem = 1;
                                            if ($radicado_rem == 1 and $remitente != 7) {
                                                echo " checked ";
                                            }
                                            ?>></center>
                                            </td>
                                            <td class="listado2">
                                        <center>
                                            <div id='divcp1'><input type='checkbox' name='cp1' id='cp1'
                                            <?php echo $onVar1; ?>
                                            <?php if ($valida[1] != Null && $valida[1] != 0) {
                                                echo " checked ";
                                            } ?>></div>
                                        </center>
                                        </td>
                                            <?php $rows = $vista_predio + $vista_entidad + 1; ?>
                                        <td class="listado2" rowspan=<?php echo $rows; ?>></td>
                                        </tr>
                                                       <?php if ($vista_predio == 1) { ?>
                                            <tr valign="top">
                                                <td valign="top" class="listado1n">	
    <?php echo  $tip3Nombre[2][$ent] ?> | 
    <?php  echo " - " . substr($nombret_us2, 0, 35) ?>
                                                    <br>
                                                    <?php echo  $direccion_us2 ?>
                                                    | 
    <?php echo  "$dpto_nombre_us2/$muni_nombre_us2" ?>
                                                </td>
                                            <?php                                             iF ($valida2[2] == 'off') {
                                                $onVar2 = 'disabled';
                                                $inVar2.='disabled';
                                            } else {
                                                $onVar2 = funjav($codigo, 2, 'divcp2', $valida[2], $datoss2);
                                                if (strtoupper(trim($datoss2)) != strtoupper('disabled') and strlen($codigo) != 0) {
                                                    $inVar2 = "onclick='destinRem(\"$codigo\",2,\"divcp2\",";
                                                    if ($valida[2]) {
                                                        $inVar2.=$valida[2];
                                                    } else {
                                                        $inVar2.='"no"';
                                                    }
                                                    $inVar2.=");'";
                                                }
                                            }
                                            ?>
                                                <td class="listado1n">
                                            <center><input type="radio" name="radicado_rem" id="radicado_rem2"
    <?php echo  $inVar2; ?> id="rempre" value=2 <?php echo  $datoss2 ?>
                                                        <?php if ($radicado_rem == 2) {
                                                            echo " checked ";
                                                        } ?>></center>
                                            </td>
                                            <td class="listado1n">
                                            <center>
                                                <div id='divcp2'><input type="checkbox" <?php echo  $onVar2; ?>
    <?php if ($valida[2] != Null && $valida[2] != 0) {
        echo " checked ";
    } ?>
                                                                        name="cop2" value=2 id="cop2"></div>
                                            </center>
                                            </td>
                                            </tr>
                                                       <?php } if ($vista_entidad == 1) { ?>
                                            <tr>
                                                <td valign="top" class="listado2">
                                                           <?php 
                                                           if ($tip3Nombre[3][$ent])
                                                               echo $tip3Nombre[3][$ent] . " | ";
                                                           if (substr($nombret_us3, 0, 35))
                                                               echo substr($nombret_us3, 0, 35) . "<br>";
                                                           if ($direccion_us3)
                                                               echo $direccion_us3 . " | ";
                                                           if ($dpto_nombre_us3 && $muni_nombre_us3)
                                                               echo "$dpto_nombre_us3/$muni_nombre_us3" . "<br>";
                                                           if ($direccion_us3) {
                                                               if ($espEnRUPS == null && $swIntegraAps != "0") {
                                                                   echo "<font color=red> La ESP no se encuentra en RUPS, <br>por lo tanto no puede ser ingresada a Sancionados</font>";
                                                               } else {
                                                                   echo "<font color=blue>ESP en RUPS, se puede ingresar a Sancionados</font>";
                                                               }
                                                           }
                                                           ?>
                                                    <br>
                                                    Notificacion a: ( <span class="titulosError"> <input type="text"
                                                                                                         name="direccionAlterna" value="<?php echo  $direccionAlterna ?>" size="18"
                                                                                                         readonly="readonly"></span> )
                                                                        <?php                                                                         if (!empty($codigo)) {
                                                                            $anexo = $codigo;
                                                                            ?>
                                                        <input name="modificarDireccion" value="Modificar Datos"
                                                               class="botones"
                                                               onclick="window.open('./mostrarDireccion.php?<?php echo  session_name() ?>=<?php echo  session_id() ?>&krd=<?php echo  $krd ?>&anexo=<?php echo  $anexo ?>&dptoCodi=<?php echo  $codep_us1 ?>', 'Tipificacion_Documento', 'height=200,width=450,scrollbars=no')"
                                                               type="button">
        <?php     }
    iF ($valida2[3] == 'off') {
        $onVar3 = 'disabled';
        $inVar3.='disabled';
    } else {
        $onVar3 = funjav($codigo, 3, 'divcp3', $valida[3], $datoss3);
        if (strtoupper(trim($datoss3)) != strtoupper('disabled') and strlen($codigo) != 0) {
            $inVar3 = "onclick='destinRem(\"$codigo\",3,\"divcp3\",";
            if ($valida[3]) {
                $inVar3.=$valida[3];
            } else {
                $inVar3.='"no"';
            }
            $inVar3.=");'";
        }
    }
    ?>
                                                </td>
                                                <td class="listado2">
                                            <center><input type="radio" name="radicado_rem" id="radicado_rem3"
    <?php echo  $inVar3 ?> value=3<?php echo  $datoss3 ?> '<?php if ($radicado_rem == 3) {
        echo " checked ";
    } ?> '></center>
                                            </td>
                                            <td class="listado2">
                                            <center>
                                                <div id='divcp3'><input type="checkbox" name="cop3" value=3
                                                                        id="cop3" <?php echo $onVar3 ?>
                                                          <?php if ($valida[3] != Null && $valida[3] != 0) {
                                                              echo " checked ";
                                                          } ?>></div>
                                                </div>
                                            </center>
                                            </td>
                                            </tr>	

                                                      <?php } echo $otoroDto; ?>
                                        <tr>

                                            <td valign="top" align="right" colspan=4>
                                                <div id='bar'></div>
                                                <span
                                                    style="text-align: right; font-family: Arial, Helvetica, sans-serif; font-size: 10px; font-style: normal; font-weight: bolder;">
                                    <?php if (strlen($codigo) != 0) { ?><a href='#2' onclick="vistafunt(2);">
                                                            + A&ntilde;adir Destinatario o copia</a><?php } ?></span></td>
                                        </tr>
                                    </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table class="borde_tab" width="100%">
                                    <tr valign="top">
                                        <td class='titulos2' valign="top" colspan="2">Descripcion</td>
                                    </tr>
                                    <tr valign="top">
                                        <td valign="top" colspan="2" height="66" align='center' class="listado2">
                                    <center>(Es el asunto en el caso de que sea un anexo documento a Radicar. Max 595 Caracteres)<br>
                                        <SCRIPT>
    function ismaxlength(obj) {
        var mlength = obj.getAttribute ? parseInt(obj.getAttribute("maxlength")) : ""
        if (obj.getAttribute && obj.value.length > mlength)
            obj.value = obj.value.substring(0, mlength)
    }
                                        </script>
                                        <textarea name="descr" cols="70" rows="3" class="tex_area"
                                                  id="descr" maxlength="595" onkeyup="return ismaxlength(this)" onkeypress="return noEnter(this, event)"><?php 
                                    if (strlen($descr) == 0) {
                                        $Asunto = str_replace('\\', '', $Asunto);
                                        echo $Asunto;
                                    } else {
                                        $descr = str_replace('\\', '', $descr);
                                        echo $descr;
                                    }
                                    ?></textarea> <input name="usuar" type="hidden" id="usuar"
                                                           value="<?php echo $usuar ?>"><br>
                                        <input name="predi" type="hidden" id="predi"
                                               value="<?php echo $predi ?>"> <input name="empre" type="hidden"
                                               id="empre" value="<?php echo $empre ?>"> * El Asunto que se
                                        muestra se optine del Radicado padre.</center> 
                                               <?php                                                if ($tipo == 999999) {
                                                   echo "
			<div align='left'>
			<font size='1' color='#000000'><b>Ubicaci&oacute;n F&iacute;sica:</b></font>
			<input type='text' name='anex_ubic' value='$anex_ubic'>
		";
                                               }
                                               ?>
                            </td>
                        </tr>
                    </table>
                    </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" border="0" cellpadding="0" cellspacing="2"	class="borde_tab">
                                <tr align="center">
                                        <!--  <td width="18%"  class="titulos2">&nbsp;</td>-->
                                    <td width="100%" colspan=2 height="25" class="titulos2"><input
                                            type="hidden" name="MAX_FILE_SIZE"
                                            value="<?php echo return_bytes(ini_get('upload_max_filesize')); ?>">
                                        ADJUNTAR ARCHIVO</td>
                                </tr>
                                <tr align="center" class="listado2">
                                    <td align="center"><font size="-3" class="etextomenu">
                                        <div id="LabelPlantillaPliego"></div>
                                        </font>
                                        <input name="userfile1" type="file"
                                               class="tex_area" onChange="escogio_archivo();" id="userfile"> <label>
                                    </td></tr><tr class="listado2"><td align="center">
                                        <input name="button" type="button" class="botones_largo"
                                               onClick="actualizar()" value="ACTUALIZAR <?php echo  $codigo ?>"> </label>
                    <?php 
                    if ($radicado_rem == 7 and $i_copias == 0) {
                        echo " $mensaje <br><b><span  class='alarmas' >No puede generar envio, No ha anexado destinatario </span></b>";
                    } else {
                        $cerrar = "onclick='cerrar({$forma});'";
                        echo "  $mensaje <input type='button' class ='botones' value='cerrar' $cerrar > ";
                    }
                    ?>


                                    </td>
                                </tr>
                            </table>
                            <input type=hidden name=i_copias value='<?php echo  $i_copias ?>'
                                   id="i_copias"></td>
                    </tr>
                    </table>
                    </td>
                    </tr>
                    </table>  
<?php 
if ($respUpdate == "OK") {
    //echo ("Integrando ($aplinteg)");
    $objApl->AplIntegrada_codigo($aplinteg);
    if (trim($objApl->get_sgd_apli_codi()) > 0) {
        //include "$ruta_raiz/ver_datosrad.php";
        $lkGenerico = "&usuario=$krd&nsesion=" . trim(session_id()) . "&nro=$codigo&ruta_raiz=.&time=" . time();
        $lksancionados = $objApl->get_sgd_apli_lk1();
        $lkgen = str_replace("&", "|", $lkGenerico);
        $lksancionados = str_replace("/", "|", $lksancionados);

        echo ("<script>window.open('abre_en_frame.php?lkparam=$lksancionados&datoenvio=$lkgen','Agotamiento','top=0,height=580,width=850,scrollbars=yes');</script>");
        //echo "<script>window.open('".$objApl->get_sgd_apli_lk1() .$lkGenerico."', 'SANCIONADOS', 'top=0,height=580,width=850,scrollbars=yes');</script>";
    }
}
?>
                </form>

            </div>
            <div id="vista2"><br>
                <br>
                <div align="right">
                    <form enctype="multipart/form-data" method="post" name="formulario1"
                          id="formulario1" action='nuevo_archivo.php?<?php echo  $variables ?>'><span
                            style="text-align: right; font-family: Arial, Helvetica, sans-serif; font-size: 10px; font-style: normal; font-weight: bolder;">
                            <a href='#2' onclick="vistafunt(1);"> - cancelar</a></span>

                </div>
                <table width="100%" align="center" border="0" cellpadding="0"
                       cellspacing="5" class="borde_tab">
<?php 
if ($codigo) {
    ?>
                        <tr>
                            <td width="100%" class='titulos2'><font size="1" class="etextomenu">
                                Nuevo Destinatario o Copia </font>
    <?php  $busq_salida = "true"; ?>
                            </td>
                            <td width="25%"><input type="button" name="Button" value="BUSCAR"
                                                   class="botones"
                                                   onClick="Start('<?php echo  $ruta_raiz ?>/radicacion/buscar_usuario.php?nuevo_archivo=1&busq_salida=<?php echo  $busq_salida ?>&nombreTp3=<?php echo  $nombreTp3 ?>&krd=<?php echo  $krd ?>', 1024, 500);">
                            </td>
                        </tr>
                        <tr>
                            <td class='celdaGris' colspan="2"><font size="1">
                                <table width="100%" border="0" cellpadding="0" cellspacing="5"
                                       class="borde_tab">
                                    <tr>
                                        <td width="203" CLASS=titulos2>DOCUMENTO</td>
                                        <TD align="center" class="listado2"><input type=hidden
                                                                                   name=telefono_us1 value='' class=tex_area size=10> <input
                                                                                   type=hidden name=tipo_emp_us1 class=tex_area size=3
                                                                                   value='<?php echo  $tipo_emp_us1 ?>'> <input type=hidden name=documento_us1
                                                                                   class=tex_area size=3 value='<?php echo  $documento_us1 ?>'> <input
                                                                                   type=hidden name="idcont1" id="idcont1" value='<?php echo  $idcont1 ?>'
                                                                                   class=e_cajas size=4> <input type=hidden name="idpais1"
                                                                                   id="idpais1" value='<?php echo  $idpais1 ?>' class=e_cajas size=4> <input
                                                                                   type=hidden name="codep_us1" id="codep_us1"
                                                                                   value='<?php echo  $codep_us1 ?>' class=e_cajas size=4> <input type=hidden
                                                                                   name="muni_us1" id="muni_us1" value='<?php echo  $muni_us1 ?>' class=e_cajas
                                                                                   size=4> <input type=text name=cc_documento_us1
                                                                                   value='<?php  /* =$cc_documento_us1 */ ?>' READONLY class=e_cajas size=40>
                                        </TD>
                                    </tr>
                                    <tr>
                                        <td CLASS=titulos2>NOMBRE</td>
                                        <TD width="329" align="center" class="listado2"><input type=text
                                                                                               name=nombre_us1 value='' READONLY size=13 class=tex_area> <input
                                                                                               type=text name=prim_apel_us1 value='' READONLY size=13
                                                                                               class=tex_area> <input type=text name=seg_apel_us1 value=''
                                                                                               READONLY size=13 class=tex_area></TD>
                                    </tr>
                                    <tr>
                                        <td CLASS=titulos2>Dirigido a</td>
                                        <TD width="140" align="center" class="listado2"><input type=text
                                                                                               name=otro_us7 value='' class=tex_area size=40 maxlength="45"></TD>
                                    </tr>
                                    <tr>
                                        <td CLASS=titulos2 width="103">DIRECCION</td>
                                        <TD align="center" class="listado2"><input type=text
                                                                                   name=direccion_us1 value='' READONLY class=tex_area size=40></TD>
                                    </tr>
                                    <tr>
                                        <td CLASS=titulos2 width="68">EMAIL</td>
                                        <TD width="68" align="center" class="listado2" colspan="2"><input
                                                type=text name=mail_us1 value='' READONLY class=tex_area size=40></TD>
                                    </tr>
                                    <tr>
                                        <td colspan="3" align="center">
                                            <div id='GRB'><input type='hidden' id='tpradic' name='tpradic' value='<?php  echo $tpradic; ?>'></div><input type="submit" name="cc"
                                                                                                                                                     value="Grabar Destinatario" class="botones_mediano"></td>
                                    </tr>
                                </table>

<?php 
} else {
    ?><tr><TD width="68" align="center" class="listado2"> <?php     echo "falta Anexar un documento";
    ?></td>
                        </tr>
                    </table><?php // onClick="continuar_grabar()"
}
?>
                </form>
            </div>

        </div>

        <script language="javascript">
            vistafunt(1);
        </script>

    </body>
</html>

<?php  //print_r($_SESSION); ?>
