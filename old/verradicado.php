<?php putenv("NLS_LANG=American_America.UTF8");
/* * ********************************************************************************** */
/* ORFEO GPL:Sistema de Gestion Documental		http://www.orfeogpl.org	             */
/* 	Idea Original de la SUPERINTENDENCIA DE SERVICIOS PUBLICOS DOMICILIARIOS         */
/* 				COLOMBIA TEL. (57) (1) 6913005  orfeogpl@gmail.com                   */
/* ===========================                                                       */
/*                                                                                   */
/* Este programa es software libre. usted puede redistribuirlo y/o modificarlo       */
/* bajo los terminos de la licencia GNU General Public publicada por                 */
/* la "Free Software Foundation"; Licencia version 2. 			                     */
/*                                                                                   */
/* Copyright (c) 2005 por :	  	  	                                                 */
/* SSPS "Superintendencia de Servicios Publicos Domiciliarios"                       */
/*   Jairo Hernan Losada  jlosada@gmail.com                Desarrollador             */
/*   Sixto Angel Pinzón López --- angel.pinzon@gmail.com   Desarrollador           */
/* C.R.A.  "COMISION DE REGULACION DE AGUAS Y SANEAMIENTO AMBIENTAL"                 */
/*   Liliana Gomez        lgomezv@gmail.com                Desarrolladora            */
/*   Lucia Ojeda          lojedaster@gmail.com             Desarrolladora            */
/* D.N.P. "Departamento Nacional de Planeación"                                     */
/*   Hollman Ladino       hollmanlp@gmail.com                Desarrollador           */
/*                                                                                   */
/* Colocar desde esta lInea las Modificaciones Realizadas Luego de la Version 3.5    */
/*  Nombre Desarrollador   Correo     Fecha   Modificacion                           */
/*  Infometrika            info@infometrika.com  05/2009  Arreglo Variables Globales */
/*  Jairo Losada           jlosada@gmail.com     05/2009  Eliminacion Funciones-Procesos */
/* * ********************************************************************************** */

session_start(); date_default_timezone_set('America/Bogota'); date_default_timezone_set('America/Bogota');
foreach ($_GET as $key => $valor)
   $$key = $valor;
foreach ($_POST as $key => $valor)
   $$key = $valor;

$krd = $_SESSION["krd"];
$dependencia = $_SESSION["dependencia"];
$usua_doc = $_SESSION["usua_doc"];
$codusuario = $_SESSION["codusuario"];
$tip3Nombre = $_SESSION["tip3Nombre"];
$tip3desc = $_SESSION["tip3desc"];
$tip3img = $_SESSION["tip3img"];
$tpNumRad = $_SESSION["tpNumRad"];
$tpPerRad = $_SESSION["tpPerRad"];
$tpDescRad = $_SESSION["tpDescRad"];
$tip3Nombre = $_SESSION["tip3Nombre"];
$tpDepeRad = $_SESSION["tpDepeRad"];
$usuaPermExpediente = $_SESSION["usuaPermExpediente"];

$nomcarpeta = (isset($_GET["nomcarpeta"]))?$_GET["nomcarpeta"]:'';
$ruta_raiz = ".";

include "$ruta_raiz/config.php";

// Modificado Infom�trika 22-Julio-2009
// Compatibilidad con register_globals = Off
$verradicado = $_GET['verrad'];

if (!isset($ent)){
    $ent = substr($verradicado, -1);
}

if (!isset($carpeta)){
    $carpeta = (isset($carpetaOld))?$carpetaOld:'';
}

if (!isset($menu_ver_tmp)){
    $menu_ver_tmp = (isset($menu_ver_tmpOld))?$menu_ver_tmpOld:'';
}

if (!isset($menu_ver)){
    $menu_ver = (isset($menu_ver_Old))?$menu_ver_Old:'';
}

if (empty($menu_ver)){
    $menu_ver = 3;
}

if (!empty($menu_ver_tmp)){
    $menu_ver = $menu_ver_tmp;
}

if (!defined('ADODB_ASSOC_CASE'))
    define('ADODB_ASSOC_CASE', 1);
include_once $ruta_raiz . '/validadte.php';
include_once "./include/db/ConnectionHandler.php";
if ($verradicado)
    $verrad = $verradicado;
if (!$ruta_raiz)
    $ruta_raiz = ".";
$numrad = $verrad;
error_reporting(7);
$db = new ConnectionHandler(".");
$db->conn->SetFetchMode(3);

if ($carpeta == 8) {
    $info = 8;
    $nombcarpeta = "Informados";
}
/*
 * @author Modificacion Liliana Gomez Velasquez
 * @since 30 de SEPTIEMBRE de 2009
 * @category imagenes
 */

include_once "$ruta_raiz/tx/verLinkArchivo.php";

$verLinkArchivo = new verLinkArchivo($db);
include_once "$ruta_raiz/class_control/Radicado.php";
$objRadicado = new Radicado($db);
$objRadicado->radicado_codigo($verradicado);
$path = $objRadicado->getRadi_path();
/** verificacion si el radicado se encuentra en el usuario Actual
 *
 */
include "$ruta_raiz/tx/verifSession.php";
?>
<html><head><title>.: Modulo total :.</title>
        <link rel="stylesheet" href="estilos/orfeo.css">
	<link rel="stylesheet" type="text/css" href="../estilos/desvanecido.css">
        <meta name="tipo_contenido"  content="text/html;" http-equiv="content-type" charset="utf-8">
        <!-- seleccionar todos los checkboxes-->
<?php include_once "$ruta_raiz/js/funtionImage.php"; ?>

        <SCRIPT LANGUAGE="JavaScript">
            function datosBasicos()
            {
                window.location = 'radicacion/NEW.PHP?<?php echo  session_name() . "=" . session_id() ?>&<?php echo  "nurad=$verrad&fechah=$fechah&ent=$ent&Buscar=Buscar Radicado&carpeta=$carpeta&nomcarpeta=$nomcarpeta"; ?>';
            }
            function mostrar(nombreCapa)
            {
                document.getElementById(nombreCapa).style.display = "";
            }
            function ocultar(nombreCapa)
            {
                document.getElementById(nombreCapa).style.display = "none";
            }
            var contadorVentanas = 0
<?php if ($verradPermisos == "Full" or $datoVer == "985") {
    if ($datoVer == "985") {
        ?>
                    function  window_onload()
                    {
        <?php         if ($verradPermisos == "Full" or $datoVer == "985") {
            ?>
                            window_onload2();
                    <?php                 }
                ?>
                    }
        <?php     }
    ?>
            </script>

    <?php     include "pestanas.js";
    ?>

            <script >
    <?php } else {
    ?>
                function changedepesel(xx)
                {
                }
    <?php }
?>

            function window_onload2()
            {
<?php if ($menu_ver == 3) {
    echo "ocultar_mod(); ";
    if ($ver_causal) {
        echo "ver_causales();";
    }
    if ($ver_tema) {
        echo "ver_temas();";
    }
    if ($ver_sectores) {
        echo "ver_sector();";
    }
    if ($ver_flujo) {
        echo "ver_flujo();";
    }
    //if ($ver_subtipo) {echo "verSubtipoDocto();"; }
    if ($ver_VinAnexo) {
        echo "verVinculoDocto();";
    }
}
?>
            }
            function verNotificacion() {
                mostrar("mod_notificacion");
                ocultar("tb_general");
                ocultar("mod_causales");
                ocultar("mod_temas");
                ocultar("mod_sector");
                ocultar("mod_flujo");
            }
            function ver_datos()
            {
                mostrar("tb_general");
                ocultar("mod_causales");
                ocultar("mod_temas");
                ocultar("mod_sector");
                ocultar("mod_flujo");
            }
            function ocultar_mod()
            {
                ocultar("mod_causales");
                ocultar("mod_temas");
                ocultar("mod_sector");
                ocultar("mod_flujo");
            }

            function ver_tipodocumental()
            {
<?php if ($menu_ver_tmp != 2) {
    ?>
                    ocultar("tb_general");
                    ocultar("mod_causales");
                    ocultar("mod_temas");
                    ocultar("mod_flujo");
<?php } ?>

            }
            function ver_tipodocumento()
            {
                ocultar("tb_general");
                ocultar("mod_causales");
                ocultar("mod_temas");
                ocultar("mod_flujo");
            }

            function verDecision()
            {
                ocultar("tb_general");
                ocultar("mod_causales");
                ocultar("mod_temas");
                ocultar("mod_flujo");
            }

            function ver_tipodocuTRD(codserie, tsub)
            {
<?php //echo "ver_tipodocumental(); ";
$isqlDepR = "SELECT RADI_DEPE_ACTU,RADI_USUA_ACTU from radicado
                                    WHERE RADI_NUME_RADI = '$numrad'";
$rsDepR = $db->conn->Execute($isqlDepR);
$coddepe = $rsDepR->fields['RADI_DEPE_ACTU'];
$codusua = $rsDepR->fields['RADI_USUA_ACTU'];
$ind_ProcAnex = "N";
?>
                window.open("./radicacion/tipificar_documento.php?nurad=<?php echo  $verrad ?>&ind_ProcAnex=<?php echo  $ind_ProcAnex ?>&codusua=<?php echo  $codusua ?>&coddepe=<?php echo  $coddepe ?>&codusuario=<?php echo  $codusuario ?>&dependencia=<?php echo  $dependencia ?>&tsub=" + tsub + "&codserie=" + codserie, "Tipificacion_Documento", "height=500,width=750,scrollbars=yes");
            }
             function ver_tipodocuTRD2(codserie, tsub)
            {
<?php //echo "ver_tipodocumental(); ";
/*$isqlDepR = "SELECT RADI_DEPE_ACTU,RADI_USUA_ACTU from radicado
                                    WHERE RADI_NUME_RADI = '$numrad'";
$rsDepR = $db->conn->Execute($isqlDepR);
$coddepe = $rsDepR->fields['RADI_DEPE_ACTU'];
$codusua = $rsDepR->fields['RADI_USUA_ACTU'];
$ind_ProcAnex = "N";*/
?>
                window.open("./radicacion/tipificar_documento2.php?nurad=<?php echo  $verrad ?>&ind_ProcAnex=<?php echo  $ind_ProcAnex ?>&codusua=<?php echo  $codusua ?>&coddepe=<?php echo  $coddepe ?>&codusuario=<?php echo  $codusuario ?>&dependencia=<?php echo  $dependencia ?>&tsub=" + tsub + "&codserie=" + codserie, "Tipificacion_Documento", "height=500,width=750,scrollbars=yes");
            }

            function verVinculoDocto()
            {

                window.open("./vinculacion/mod_vinculacion.php?verrad=<?php echo  $verrad ?>&codusuario=<?php echo  $codusuario ?>&dependencia=<?php echo  $dependencia ?>", "Vinculacion_Documento", "height=500,width=750,scrollbars=yes");
<?php echo "ver_tipodocumental(); ";
?>

            }


            function verResolucion()
            {
                ocultar("tb_general");
                ocultar("mod_causales");
                ocultar("mod_temas");
                ocultar("mod_flujo");
                ocultar("mod_tipodocumento");
                mostrar("mod_resolucion");
                ocultar("mod_notificacion");
            }

            function ver_temas()
            {
                ocultar("tb_general");
                ocultar("mod_tipodocumento");
                ocultar("mod_causales");
                ocultar("mod_sector");
                ocultar("mod_flujo");
                ocultar("mod_tipodocumento");
                mostrar("mod_temas");
                ocultar("mod_resolucion");
                ocultar("mod_notificacion");
            }

            function ver_flujo()
            {
                mostrar("mod_flujo");
                ocultar("tb_general");
                ocultar("mod_tipodocumento");
                ocultar("mod_causales");
                ocultar("mod_temas");
                ocultar("mod_sector");
                mostrar("mod_flujo");
                ocultar("mod_resolucion");
                ocultar("mod_notificacion");
            }

            function hidden_tipodocumento()
            {
<?php if (!$ver_tipodoc) {
    ?>
                    //ocultar_mod();
    <?php }
?>

            }
            /** FUNCION DE JAVA SCRIPT DE LAS PESTA�S
             * Esta funcion es la que produce el efecto de pertanas de mover a,
             * Reasignar, Informar, Devolver, Vobo y Archivar
             */
        </script>
    <div id="spiffycalendar" class="text"></div>
    <script language="JavaScript" src="<?php echo $ruta_raiz; ?>/js/spiffyCal/spiffyCal_v2_1.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo $ruta_raiz; ?>/js/spiffyCal/spiffyCal_v2_1.css">
</head>
    <?php // Modificado Supersolidaria
    if (isset($_GET['ordenarPor']) && $_GET['ordenarPor'] != "") {
        $body = "document.location.href='#t1';";
    }
    ?>
<body bgcolor="#FFFFFF" topmargin="0" onLoad="window_onload();<?php print $body; ?>">

    <?php     $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
    $fechah = date("dmy_h_m_s") . " " . time("h_m_s");
    $check = 1;
    $numeroa = 0;
    $numero = 0;
    $numeros = 0;
    $numerot = 0;
    $numerop = 0;
    $numeroh = 0;

    include "ver_datosrad.php";
    if ($verradPermisos == "Full" or $datoVer == "985") {

    } else {
        $numRad = $verrad;
        if ($nivelRad == 1 && $permus != 0)
            include "$ruta_raiz/seguridad/sinPermisoRadicado.php";
        if ($nivelRad == 1 && $permus != 0)
            die("-");
    }
    ?>
    <div id='dvPadre' style='width: 100%;height:  100% '>
    <div id='dvCabezera' style='width: 100%'>
    <table  style='width: 100%; border-spacing: 0;z-index: auto'  class="borde_tab">
        <tr>
          <!--<td class="titulos2" width='10%' ><A class=vinculos HREF='javascript:history.back();'>PAGINA ANTERIOR</A></td>-->
            <td class="titulos4" style='font-size: 14;'>
                <?php                 if ($krd) {
                    $isql = "select * From usuario where USUA_LOGIN ='$krd' and USUA_SESION='" . substr(session_id(), 0, 29) . "' ";
                    $rs = $db->query($isql);
                    // Validacion de Usuario y COntrase� MD5
                    //echo "** $krd *** $drde";
                    //Modificado por idrd
                    if (($krd)) {
                        //  $iusuario = " and us_usuario='$krd'";
                        //  $isql = "select a.* from radicado a where radi_depe_actu=$dependencia  and radi_nume_radi=$verrad";
                        ?>
                        DATOS DEL RADICADO No
                        <?php                         if ($mostrar_opc_envio == 0 and $carpeta != 8 and !$agendado) {
                            $ent = substr($verrad, -1);
                            echo "<a style='color: #ffffff;' title='Click para modificar el Documento' href='".$url_orfeo_power_tools."/document/".$verrad."/edit' notborder >$verrad</a>";
                        }
                        else
                            echo $verrad;

                        /*
                         *  Modificado: 15-Agosto-2006 Supersolidaria
                         *  Muestra el numero del expediente al que pertenece el radicado.
                         */
                        if ($numExpediente && $_GET['expIncluido'][0] == "") {
                            echo "<span class=noleidos>&nbsp;&nbsp;&nbsp;PERTENECIENTE AL EXPEDIENTE No. " . ( $_SESSION['numExpedienteSelected'] != "" ? $_SESSION['numExpedienteSelected'] : $numExpediente ) . "</span>";
                        } else if ($_GET['expIncluido'][0] != "") {
                            echo "<span class=noleidos>&nbsp;&nbsp;&nbsp;PERTENECIENTE AL EXPEDIENTE No. " . $_GET['expIncluido'][0] . "</span>";
                            $_SESSION['numExpedienteSelected'] = $_GET['expIncluido'][0];
                        }
                        ?>
                    </td>
                    <td class="titulos4" style='width:10px'>
                        <a class="vinculos" style='color: #ffffff;' href='./solicitar/Reservas.php?radicado=<?php echo  "$verrad" ?>'>Solicitados</a>
                    </td>
                    <td class="titulos4" style='width: 10%'>
                        <a class="vinculos" style='color: #ffffff;' href='./solicitar/Reservar.php?radicado=<?php echo  "$verrad&sAction=insert" ?>'>Solicitar Fisico</a>
                    </td>
                </tr>
            </table>
                    </div>
        <div id='dvBody'> <?php         // style='padding : 4px; width : 99%; height: 470; overflow : auto;'>
                           ?>
            <table style='width:100%; border-spacing: 1'  class='borde_tab'>
                <tr >
                    <td width='33%'  >
                        <table style='width:100%; border-spacing: 0'>
                            <tr >
        <?php         $datosaenviar = "fechaf=$fechaf&mostrar_opc_envio=$mostrar_opc_envio&tipo_carp=$tipo_carp&carpeta=$carpeta&nomcarpeta=$nomcarpeta&datoVer=$datoVer&ascdesc=$ascdesc&orno=$orno";
        ?>
                                <td style="height: 20; text-align: center"   class="titulo1"><?php echo  $nomcarpeta ?> </td>
                            </tr>
                        </table>
                    </td>
                    <td width='33%'>
                        <table style='width:100%; border-spacing: 0'>
                            <tr>
                                <td class="titulo1" style="height: 20; text-align: center"><?php echo  $_SESSION['usua_nomb'] ?></td>
                            </tr>
                        </table>
                    </td>
                    <td  width="33%">
                        <table style='width:100%; border-spacing: 0' >
                            <tr>
                                <td  class="titulo1" style="height: 20; text-align: center"> <?php echo  $_SESSION['depe_nomb'] ?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <form name="form1" id="form1" action="<?php echo  $ruta_raiz ?>/tx/formEnvio.php?<?php echo  session_name() ?>=<?php echo  session_id() ?>" style="margin: 0px;" method="GET">
                <?php                 if ($verradPermisos == "Full") {
                    include "$ruta_raiz/tx/txOrfeo.php";
                } else {

                    }
                    ?>
                <input type=hidden name='checkValue[<?php echo  $verrad ?>]' value='CHKANULAR'>
                <input type=hidden name=enviara value='9'>
            </form>


             <form action='verradicado.php?<?php echo  session_name() ?>=<?php echo  trim(session_id()) ?>&krd=<?php echo  $krd ?>&verrad=<?php echo  $verrad ?>&datoVer=<?php echo  $datoVer ?>&chk1=<?php echo  $verrad . "&carpeta=$carpeta&nomcarpeta=$nomcarpeta" ?>' method='post' style="margin: 0px;" name='form2'>
            <table border="0" style='border-collapse: collapse;border-color: transparent' align='center'  width="100%" >
                    <?php
                    echo "<input type='hidden' name='fechah' value='$fechah'>";
                    // Modificado Infom�trika 22-Julio-2009
                    // Compatibilidad con register_globals = Off.
                    print "<input type='hidden' name='verrad' value='" . $verrad . "'>";
                    if ($flag == 2) {
                        echo "<CENTER>NO SE HA PODIDO REALIZAR LA CONSULTA<CENTER>";
                    } else {
                        if ($info) {
                            $row = array();
                            $row1 = array();

                            $row["INFO_LEIDO"] = 1;
                            $row1["DEPE_CODI"] = $dependencia;
                            $row1["USUA_CODI"] = $codusuario;
                            $row1["RADI_NUME_RADI"] = $verrad;
                            $rs = $db->update("informados", $row, $row1);
                        }

                        if (($leido != "no" or !$leido) and $datoVer != 985) {
                            $row = array();
                            $row1 = array();

                            $row["RADI_LEIDO"] = 1;
                            $row1["radi_depe_actu"] = $dependencia;
                            $row1["radi_usua_actu"] = $codusuario;
                            $row1["radi_nume_radi"] = $verrad;
                            $rs = $db->update("radicado", $row, $row1);
                        }
                    }
                    include "ver_datosrad.php";
                    include "ver_datosgeo.php";
                    $tipo_documento .= "<input type=hidden name=menu_ver value='$menu_ver'>";
                    $hdatos = session_name() . "=" . session_id() . "&leido=$leido&nomcarpeta=$nomcarpeta&tipo_carp=$tipo_carp&carpeta=$carpeta&verrad=$verrad&datoVer=$datoVer&fechah=fechah&menu_ver_tmp=";
                    ?>
                    <tr>
                        <td  rowspan="4" width="3%"  class="listado2">&nbsp;</td>
                        <td  width="94%" valign="bottom" style='margin-bottom: 0' class="listado2">
                            <?php
                            $datos1 = "";
                            $datos2 = "";
                            $datos3 = "";
                            $datos4 = "";
                            $datos5 = "";
                            $datos6 = "";
                            if ($menu_ver == 6) {
                                $datos6 = "_R";
                            }
                            if ($menu_ver == 5) {
                                $datos5 = "_R";
                            }
                            if ($menu_ver == 1) {
                                $datos1 = "_R";
                            }
                            if ($menu_ver == 2) {
                                $datos2 = "_R";
                            }
                            if ($menu_ver == 3) {
                                $datos3 = "_R";
                            }
                            if ($menu_ver == 4) {
                                $datos4 = "_R";
                            }
                            ?>
                            <table  style='margin-bottom: 0;border-collapse: collapse;border-color: transparent;bottom: -4;position:relative' width=69%  >
                                <tr>
                                    <td width="13%"  style='margin-bottom: 0;' class="" ><a href='verradicado.php?<?php echo  $hdatos ?>3' ><img src='imagenes/infoGeneral<?php echo  $datos3 ?>.gif' alt='' border="0" width="110" height="25"  ></a></td>
                                    <td width="13%" style='margin-bottom: 0;' class="" ><a href='verradicado.php?<?php echo  $hdatos ?>1' ><img src='imagenes/historico<?php echo  $datos1 ?>.gif' alt='' border="0" width="110" height="25" ></a></td>
                                    <td width="13%" style='margin-bottom: 0;' class="" ><a href='verradicado.php?<?php echo  $hdatos ?>2' ><img src='imagenes/documentos<?php echo  $datos2 ?>.gif' alt='' border="0" width="110" height="25" ></a></td>
                                    <td width="13%" style='margin-bottom: 0;' class="" ><a href='verradicado.php?<?php echo  $hdatos ?>4' ><img src='imagenes/expediente<?php echo  $datos4 ?>.gif' alt='' border="0" width="110" height="25"  ></a></td>
                                    <td width="61%" style='margin-bottom: 0;' class="" >&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                        <td  rowspan="4" width="3%">&nbsp;</td>
                    </tr>
                    <tr >
                        <td  bgcolor="" width="100%" height="100">
                            <?php                            if ($_SESSION['usua_debug'] == 1)
                                error_reporting(7);
                            switch ($menu_ver) {
                                case 1:
                                    include "ver_historico.php";
                                    break;
                                case 2:
                                    include "./lista_anexos.php";
                                    break;
                                case 3:
                                    include "lista_general.php";
                                    break;
                                case 4:
                                    include "./expediente/lista_expediente.php";
                                    break;
                                case 5:
                                    include "plantilla.php";
                                    break;
                                default:break;
                            }

                            ?>
                        </td>
                    </tr>
                    <input type=hidden name=menu_ver value='<?php echo $menu_ver; ?>'>
                    <tr>
                        <td height="17" width="94%" class="celdaGris"> <?php    } else {
        ?>  </td>
                    </tr>
            </table>
            <form name='form1' action='enviar.php' method='GET' style="margin: 0px;">
                <input type=hidden name=depsel>
                <input type=hidden name=depsel8>
                <input type=hidden name=carpper>
                <CENTER>
                    <span class='titulosError'>SU SESION HA TERMINADO O HA SIDO INICIADA EN OTRO EQUIPO</span><BR>
                    <span class='eerrores'>
                </CENTER></form>
        <?php     }
} else {
            echo "<center><b><span class='eerrores'>NO TIENE AUTORIZACION PARA INGRESAR</span><BR><span class='eerrores'><a href='login.php' target=_parent>Por Favor intente validarse de nuevo. Presione aca!</span></a>";
}
?> </td>
</tr>
<tr>
    <td height="15" width="94%" class="listado2">&nbsp;</TD>
</tr>
</form>
</table>
    </div>
                </div>
</body></html>
