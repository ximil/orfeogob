<?php

include_once($ruta_raiz . "/include/tx/Historico.php");
include_once($ruta_raiz . "/../core/config/config-inc.php");
//include_once $ruta_raiz . "/include/mail/infomail.php";

class Tx extends Historico {
    /** Aggregations: */
    /** Compositions: */
    /*     * * Attributes: ** */

    /**
     * Clase que maneja los Historicos de los documentos
     *
     * @param int     Dependencia Dependencia de Territorial que Anula
     * @param number  usuaDocB    Documento de Usuario
     * @param number  depeCodiB   Dependencia de Usuario Buscado
     * @param varchar usuaNombB   Nombre de Usuario Buscado
     * @param varcahr usuaLogin   Login de Usuario Buscado
     * @param number	usNivelB	Nivel de un Ususairo Buscado..
     * @db 	Objeto  conexion
     * @access public
     */
    var $db;

    function Tx($db) {
        /**
         * Constructor de la clase Historico
         * @db variable en la cual se recibe el cursor sobre el cual se esta trabajando.
         *
         */
        $this->db = $db;
    }

    /**
     * Metodo que trae los datos principales de un usuario a partir del codigo y la dependencia
     *
     * @param number $codUsuario
     * @param number $depeCodi
     *
     */
    function datosUs($codUsuario, $depeCodi, $id_rol) {
        $sql = "SELECT
  usuario.usua_login,
  sgd_mod_modules.sgd_mod_modulo,
  usuario.usua_doc,
  usuario.usua_nomb,
  sgd_drm_dep_mod_rol.sgd_drm_valor as codi_nivel,
  sgd_urd_usuaroldep.depe_codi,
  sgd_urd_usuaroldep.rol_codi
FROM
  usuario,
  sgd_urd_usuaroldep,
  sgd_mod_modules,
  sgd_drm_dep_mod_rol
WHERE
  usuario.usua_codi = sgd_urd_usuaroldep.usua_codi AND
  sgd_mod_modules.sgd_mod_id = sgd_drm_dep_mod_rol.sgd_drm_modecodi AND
  sgd_drm_dep_mod_rol.sgd_drm_rolcodi = sgd_urd_usuaroldep.rol_codi AND
  sgd_drm_dep_mod_rol.sgd_drm_depecodi = sgd_urd_usuaroldep.depe_codi AND
  sgd_mod_modules.sgd_mod_estado = 1 AND
  usuario.usua_codi = $codUsuario AND
   sgd_urd_usuaroldep.depe_codi=  $depeCodi and
     sgd_urd_usuaroldep.rol_codi=  $id_rol and
  sgd_mod_modules.sgd_mod_modulo = 'codi_nivel'";
        # Busca el usuairo Origen para luego traer sus datos.
        //$this->db->conn->debug=true;


        $this->db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        $rs = $this->db->query($sql);
        //$usNivel = $rs->fields["CODI_NIVEL"];
        //$nombreUsuario = $rs->fields["USUA_NOMB"];
        $this->usNivelB = $rs->fields['CODI_NIVEL'];
        $this->usuaNombB = $rs->fields['USUA_NOMB'];
        $this->usuaDocB = $rs->fields['USUA_DOC'];
    }

// MODIFICADO PARA GENERAR ALERTAS
// JUNIO DE 2009
    function getRadicados($tipo, $usua_cod) {
        $con = $this->db->driver;
        switch ($con) {
            case'oci8':
                $query = "SELECT $tipo FROM SGD_NOVEDAD_USUARIO WHERE USUA_DOC=$usua_cod";
                break;
            case 'postgres':

                $campo1 .='"';
                $campo1 .= $tipo;
                $campo1 .='"';
                $campo2 = '"USUA_DOC"';
                $query = "SELECT $campo1 FROM SGD_NOVEDAD_USUARIO WHERE $campo2='$usua_cod'";
                break;
        }
        $this->db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        $rs = $this->db->query($query);
        if ($rs) {
            return $rs->fields["$tipo"];
        }
    }

// MODIFICADO PARA GENERAR ALERTAS
// JUNIO  DE 2009
    function registrarNovedad($tipo, $docUsuarioDest, $numRad, $ruta_raiz = "") {
        // busco la información de radicados informados pendientes de alerta
        // Busco info del campo NOV_INFOR de la tabla SGD_NOVEDAD_USUARIO
        include_once("$ruta_raiz/class_control/Param_admin.php");
        $param = Param_admin::getObject($this->db, '%', 'ALERT_FUNCTION');
        $this->db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        if ($param->PARAM_VALOR == "1") {

            $rads = $this->getRadicados($tipo, $docUsuarioDest);

            if ($rads != "") {
                $rads.=",";
            }
            $rads.=$numRad;

            $con = $this->db->driver;

            switch ($con) {
                case'oci8':
                    $xarray['USUA_DOC'] = $docUsuarioDest;
                    $xarray["$tipo"] = $rads;

                    $tipo1 = $tipo;
                    $valor = $xarray["$tipo"];

                    $qs = "Select count(*) as contador from SGD_NOVEDAD_USUARIO where USUA_DOC=$docUsuarioDest";
                    $rs = $this->db->conn->query($qs);

                    if ($rs->fields['CONTADOR'] == 0) {
                        $qu = "INSERT INTO SGD_NOVEDAD_USUARIO (USUA_DOC,$tipo1) values ($docUsuarioDest,$valor)";
                        $this->db->conn->query($qu);
                    } else {
                        $this->db->conn->query("UPDATE SGD_NOVEDAD_USUARIO SET $tipo1 = $valor where USUA_DOC'$docUsuarioDest'");
                    }

                    break;

                case 'postgres':

                    $xarray['USUA_DOC'].='"';
                    $xarray['USUA_DOC'].=$docUsuarioDest;
                    $xarray['USUA_DOC'].='"';

                    $tipo1 = '"';
                    $tipo1.=$tipo;
                    $tipo1.='"';

                    $xarray["$tipo"].="'";
                    $xarray["$tipo"].=$rads;
                    $xarray["$tipo"].="'";

                    $valor = $xarray["$tipo"];

                    $campo = '"USUA_DOC"';
                    $qs = "Select count(*) as contador from SGD_NOVEDAD_USUARIO where $campo='$docUsuarioDest'";
                    $rs = $this->db->conn->query($qs);

                    if ($rs->fields['CONTADOR'] == 0) {
                        $qu = "INSERT INTO SGD_NOVEDAD_USUARIO ($campo,$tipo1) values ('$docUsuarioDest',$valor)";
                        $this->db->conn->query($qu);
                    } else {
                        $this->db->conn->query("UPDATE SGD_NOVEDAD_USUARIO SET $tipo1 = $valor where $campo='$docUsuarioDest'");
                    }

                    break;
            }
        }
    }

    function informar($radicados, $loginOrigen, $depDestino, $depOrigen, $codUsDestino, $codUsOrigen, $observa, $id_rol, $idenviador = null, $ruta_raiz = "") {
        $whereNivel = "";

        $this->db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        $sql = "SELECT
  usuario.usua_login,
  sgd_mod_modules.sgd_mod_modulo,
  usuario.usua_doc,
  usuario.usua_nomb,
  usuario.usua_login,
  sgd_drm_dep_mod_rol.sgd_drm_valor,
  sgd_urd_usuaroldep.depe_codi,
  sgd_urd_usuaroldep.rol_codi
FROM
  usuario,
  sgd_urd_usuaroldep,
  sgd_mod_modules,
  sgd_drm_dep_mod_rol
WHERE
  usuario.usua_codi = sgd_urd_usuaroldep.usua_codi AND
  sgd_mod_modules.sgd_mod_id = sgd_drm_dep_mod_rol.sgd_drm_modecodi AND
  sgd_drm_dep_mod_rol.sgd_drm_rolcodi = sgd_urd_usuaroldep.rol_codi AND
  sgd_drm_dep_mod_rol.sgd_drm_depecodi = sgd_urd_usuaroldep.depe_codi AND
  sgd_mod_modules.sgd_mod_estado = 1 AND
  usuario.usua_codi = $codUsDestino AND
  sgd_mod_modules.sgd_mod_modulo = 'codi_nivel'";
        # Busca el usuairo Origen para luego traer sus datos.
//$this->db->conn->debug=true;
# Busca el usuairo destino para luego traer sus datos.
        $rs = $this->db->query($sql); # Ejecuta la busqueda
        $usNivel = $rs->fields['SGD_DRM_VALOR'];
        $nombreUsuario = $rs->fields['USUA_NOMB'];
        $docUsuaDest = $rs->fields['USUA_DOC'];
        $loginUsuarioDesc = $rs->fields['USUA_LOGIN'];
        $depeUsuaDest = $rs->fields['DEPE_CODI'];
        $rollUsuaDest = $rs->fields['ROL_CODI'];
        $observa = $loginUsuarioDesc . ' - Comentario: ' . $observa;

        $codTx = 8;
        if ($tomarNivel == "si") {
            $whereNivel = ",CODI_NIVEL=$usNivel";
        }
        $codTx = 8;
        $observa = "A: $usLoginDestino - $observa";
        if (!$observacion)
            $observacion = $observa;

        $tmp_rad = array();
        $informaSql = true;
        //$this->db->conn->debug=true;
        $resSeparate = $this->radiseparate($radicados);
        $radicados = $resSeparate['rad'];
        while ((list(, $noRadicado) = each($radicados)) and $informaSql) {
            if (strstr($noRadicado, '-'))
                $tmp = explode('-', $noRadicado);
            else
                $tmp = $noRadicado;
            if (is_array($tmp)) {
                $record["RADI_NUME_RADI"] = $tmp[1];
            } else {
                $record["RADI_NUME_RADI"] = $noRadicado;
            }
            # Asignar el valor de los campos en el registro
            # Observa que el nombre de los campos pueden ser mayusculas o minusculas
            //insert into INFORMADOS(DEPE_CODI,INFO_FECH,USUA_CODI,RADI_NUME_RADI,INFO_DESC) values ($depsel, to_date ($formatfecha) ,$codus,$chk3,'$observa')
            //$record["RADI_NUME_RADI"] = $noRadicado;
            $record["DEPE_CODI"] = $depeUsuaDest;
            $record["USUA_CODI"] = $codUsDestino;
//		$record["INFO_CODI"] = $idenviador;
            $record["INFO_CODI"] = "'$idenviador'";
            $record["INFO_DESC"] = "'$observacion '";
            $record["USUA_DOC"] = "'$docUsuaDest'";
            $record["ID_ROL"] = $rollUsuaDest;
            $record["INFO_FECH"] = $this->db->conn->OffsetDate(0, $this->db->conn->sysTimeStamp);

            # Mandar como parametro el recordset vacio y el arreglo conteniendo los datos a insertar
            # a la funcion GetInsertSQL. Esta procesara los datos y regresara un enunciado SQL
            # para procesar el INSERT.
            $informaSql = $this->db->conn->Replace("INFORMADOS", $record, array('RADI_NUME_RADI', 'INFO_CODI', 'USUA_DOC'), false);
            //$informaSql = $this->db->conn->insert("INFORMADOS",$record,array('RADI_NUME_RADI','INFO_CODI','USUA_DOC'),false);
            // MODIFICADO PARA GENERAR ALERTAS
            // JUNIO DE 2009
            $this->registrarNovedad('NOV_INFOR', $docUsuarioDest, $record["RADI_NUME_RADI"], $ruta_raiz);
            //Modificado idrd
            if ($informaSql)
                $tmp_rad[] = $record["RADI_NUME_RADI"];
        }
        //echo "$tmp_rad,$depOrigen,$codUsOrigen,$id_rol,$depDestino,$codUsDestino,$id_rol, $observa,$codTx";
        $this->insertarHistorico($tmp_rad, $depOrigen, $codUsOrigen, $id_rol, $depDestino, $codUsDestino, $rollUsuaDest, $observa, $codTx);
        return $nombreUsuario;
    }

    function borrarInformado($radicados, $loginOrigen, $depDestino, $depOrigen, $codUsDestino, $codUsOrigen, $observa, $id_rol) {
        $tmp_rad = array();
        $deleteSQL = true;
        //$this->db->conn->debug=true;
        $resSeparate = $this->radiseparate($radicados);
        $radicados = $resSeparate['rad'];
        while ((list(, $noRadicado) = each($radicados)) and $deleteSQL) { //foreach($radicados as $noRadicado)
            # Borrar el informado seleccionado
            $tmp = explode('-', $noRadicado);
            ($tmp[0]) ? $wtmp = " and INFO_CODI = '" . $tmp[0] . "'" : $wtmp = ' and INFO_CODI IS NULL ';
            $record["RADI_NUME_RADI"] = $tmp[1];
            $record["USUA_CODI"] = $codUsOrigen;
            $record["DEPE_CODI"] = $depOrigen;
            $deleteSQL = $this->db->conn->Execute("DELETE FROM INFORMADOS WHERE RADI_NUME_RADI=" . $tmp[1] . " and USUA_CODI=" . $codUsOrigen . " and DEPE_CODI=" . $depOrigen . $wtmp);
            if ($deleteSQL)
                $tmp_rad[] = $record["RADI_NUME_RADI"];
        }
        $codTx = 7;
        if ($deleteSQL) {
            $this->insertarHistorico($tmp_rad, $depOrigen, $codUsOrigen, $id_rol, $depOrigen, $codUsOrigen, $id_rol, $observa, $codTx, $observa);
            return $tmp_rad;
        }
        else
            return $deleteSQL;
    }

    function cambioCarpeta($radicados, $codUsOrigen, $depOrigen, $id_rol, $nombOringen, $carpetaDestino, $usNivel, $carpetaTipo, $tomarNivel, $observa) {
        $whereNivel = "";
        /* echo 	$sql = "SELECT
          b.USUA_DOC
          ,b.USUA_LOGIN
          ,b.CODI_NIVEL
          ,b.DEPE_CODI
          ,b.USUA_CODI
          ,b.USUA_NOMB
          FROM
          USUARIO b
          WHERE
          b.USUA_LOGIN = '$usuaLogin'";
          # Busca el usuairo Origen para luego traer sus datos.
          $this->db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
          $rs = $this->db->query($sql); # Ejecuta la busqueda

          $usNivel = $rs->fields["CODI_NIVEL"];
          $depOrigen = $rs->fields["DEPE_CODI"];
          $codUsOrigen = $rs->fields["USUA_CODI"];
          $nombOringen = $rs->fields["USUA_NOMB"]; */
        if ($tomarNivel == "si") {
            $whereNivel = ",CODI_NIVEL=$usNivel";
        }
        $codTx = "10";

        //$radicadosIn = join(",",$radicados);
        $resSeparate = $this->radiseparate($radicados);
        $radicadosIn = join(",", $resSeparate['rad']);
        $sql = "update radicado
					set
					  CARP_CODI=$carpetaDestino
					  ,CARP_PER=$carpetaTipo
					  ,radi_fech_agend=null
					  ,radi_agend=null
					  $whereNivel
				 where RADI_NUME_RADI in($radicadosIn)";

        //$this->conn->Execute($isql);
        $rs = $this->db->query($sql); # Ejecuta la busqueda
        /* echo 2;
          bandeja($this->db,$carpante, $id_rol, $depOrigen,$codUsOrigen, 'mcarp',$carpetaDestino);
          echo 3; */
        $retorna = 1;
        if (!$rs) {
            echo "<center><font color=red>Error en el Movimiento ... A ocurrido un error y no se ha podido realizar la Transaccion</font> <!-- $sql -->";
            $retorna = -1;
        }
        if ($retorna != -1) {

            $this->insertarHistorico($radicados, $depOrigen, $codUsOrigen, $id_rol, $depOrigen, $codUsOrigen, $id_rol, $observa, $codTx);
        }
        return $retorna;
    }

    function reasignar($radicados, $loginOrigen, $depDestino, $depOrigen, $codUsDestino, $codUsOrigen, $tomarNivel, $observa, $codTx, $carp_codi, $id_rol, $usuaDocOrigen) {
        $whereNivel = "";

        $this->db->conn->SetFetchMode(ADODB_FETCH_ASSOC);

        /* 	 $sql= "SELECT
          USUA_DOC
          ,USUA_LOGIN
          ,CODI_NIVEL
          ,USUA_NOMB
          FROM
          USUARIO
          WHERE
          AND USUA_CODI=$codUsDestino"; */
        $sql = "SELECT
  usuario.usua_login,
  sgd_mod_modules.sgd_mod_modulo,
  usuario.usua_doc,
  usuario.usua_nomb,
  usuario.usua_email as email,
  sgd_drm_dep_mod_rol.sgd_drm_valor,
  sgd_urd_usuaroldep.depe_codi,
  sgd_urd_usuaroldep.rol_codi
FROM
  usuario,
  sgd_urd_usuaroldep,
  sgd_mod_modules,
  sgd_drm_dep_mod_rol
WHERE
  usuario.usua_codi = sgd_urd_usuaroldep.usua_codi AND
  sgd_mod_modules.sgd_mod_id = sgd_drm_dep_mod_rol.sgd_drm_modecodi AND
  sgd_drm_dep_mod_rol.sgd_drm_rolcodi = sgd_urd_usuaroldep.rol_codi AND
  sgd_drm_dep_mod_rol.sgd_drm_depecodi = sgd_urd_usuaroldep.depe_codi AND
  sgd_mod_modules.sgd_mod_estado = 1 AND
  usuario.usua_codi = $codUsDestino AND
  sgd_mod_modules.sgd_mod_modulo = 'codi_nivel'";
        # Busca el usuairo Origen para luego traer sus datos.
//$this->db->conn->debug=true;

        $rs = $this->db->query($sql);
        //$usNivel = $rs->fields["CODI_NIVEL"];
        //$nombreUsuario = $rs->fields["USUA_NOMB"];
        $usNivel = $rs->fields['SGD_DRM_VALOR'];
        $nombreUsuario = $rs->fields['USUA_NOMB'];
        $docUsuaDest = $rs->fields['USUA_DOC'];
        $depeUsuaDest = $rs->fields['DEPE_CODI'];
        $rollUsuaDest = $rs->fields['ROL_CODI'];
        $correoUsuaDest = $rs->fields['EMAIL'];
	$loginDest=$rs->fields['USUA_LOGIN'];
        if ($tomarNivel == "si") {
            $whereNivel = ",CODI_NIVEL=$usNivel";
        }

        $resSeparate = $this->radiseparate($radicados);
        $radicadosIn = join(",", $resSeparate['rad']);
        $proccarp = "Reasignar";
        $carp_per = 0;
        $isql = "update radicado
				set
				  RADI_USU_ANTE='$loginOrigen'
                                  ,USUA_DOC_ANTE=$usuaDocOrigen
                                  ,USUA_DEPE_ANTE='$depOrigen'
                                  ,USUA_ROL_ANTE='$id_rol'
				  ,RADI_DEPE_ACTU=$depDestino
				  ,RADI_USUA_ACTU=$codUsDestino
                                   ,RADI_USUA_DOC_ACTU=$docUsuaDest
                                   ,ID_ROL=$rollUsuaDest
				  ,CARP_CODI=$carp_codi
				  ,CARP_PER=$carp_per
				  ,RADI_LEIDO=0
				  , radi_fech_agend=null
				  ,radi_agend=null
				  ,pc_search_indexed=0
				  $whereNivel
			 where radi_depe_actu=$depOrigen
			 	   AND radi_usua_actu=$codUsOrigen
				   AND RADI_NUME_RADI in($radicadosIn)";
        //$this->conn->Execute($isql);
        // MODIFICADO PARA GENERAR ALERTAS
        // JUNIO DE 2009
        foreach ($radicados as $rad) {
            $this->registrarNovedad('NOV_REASIG', $docUsuaDest, $rad);
        }
        //////////////////////////////////
        $this->db->conn->Execute($isql); # Ejecuta la busqueda
        $this->insertarHistorico($radicados, $depOrigen, $codUsOrigen, $id_rol, $depDestino, $codUsDestino, $rollUsuaDest, $observa, $codTx);
        $this->alertaEmail($correoUsuaDest, $resSeparate['rad'], $resSeparate['asu'],$loginOrigen,$loginDest,$resSeparate['tdoc']);
        return $nombreUsuario;
    }

    function radiseparate($radicados) {
        for ($index = 0; $index < count($radicados); $index++) {
            $a = explode('*#*', $radicados[$index]);
	    $ssql="select tdoc_codi,ra_asun from radicado where radi_nume_radi={$a[0]}";
	    $rs=$this->db->conn->Execute($ssql);
	    $tdoc[$index] = $rs->fields['TDOC_CODI'];
	    $asunto[$index] = $rs->fields['RA_ASUN'];
            $rad[$index] = $a[0];
            //$asunto[$index] = $a[1];
        }
        $g['rad'] = $rad;
        $g['asu'] = $asunto;
	$g['tdoc']=$tdoc;
        return $g;
    }

//Modificado por Fabian Mauricio Losada
    function archivar($radicados, $loginOrigen, $depOrigen, $codUsOrigen, $observa, $id_rol) {
        /* Hace disponibles las variables de configuración del usuario y la dependencia
        de salida, los cuales están definidos en el archivo de configuracion
        core/config/config-inc.php */
        global $entidad_depsal;
        global $codigo_usuario_salida;

        $whereNivel = "";
        //$radicadosIn = join(",",$radicados);
        $resSeparate = $this->radiseparate($radicados);
        $radicadosIn = join(",", $resSeparate['rad']);
        $carp_codi = substr($depOrigen, 0, 2);
        $carp_per = 0;
        $carp_codi = 0;
        $isql = "update radicado
					set
					  RADI_USU_ANTE='$loginOrigen'
					  ,RADI_DEPE_ACTU='$entidad_depsal'
					  ,RADI_USUA_ACTU='$codigo_usuario_salida'
                      ,ID_ROL=1
					  ,CARP_CODI=$carp_codi
					  ,CARP_PER=$carp_per
                                          ,USUA_ROL_ANTE=$id_rol
                                          ,USUA_DEPE_ANTE=$depOrigen
					  ,RADI_LEIDO=0
					  ,radi_fech_agend=null
					  ,radi_agend=null
					  ,CODI_NIVEL=1
					  ,pc_search_indexed=0
				 where radi_depe_actu=$depOrigen
				 	   AND radi_usua_actu=$codUsOrigen
					   AND RADI_NUME_RADI in($radicadosIn)";
        //$this->conn->Execute($isql);
        $this->db->conn->Execute($isql); # Ejecuta la busqueda
        $this->insertarHistorico($radicados, $depOrigen, $codUsOrigen, $id_rol, 999, 2, 1, $observa, 13);
        return $isql;
    }

    // Hecho por Fabian Mauricio Losada
    function nrr($radicados, $loginOrigen, $depOrigen, $codUsOrigen, $observa, $id_rol) {
        $whereNivel = "";
        //$radicadosIn = join(",",$radicados);
        $resSeparate = $this->radiseparate($radicados);
        $radicadosIn = join(",", $resSeparate['rad']);
        $carp_codi = substr($depOrigen, 0, 2);
        $carp_per = 0;
        $carp_codi = 0;
        //$this->db->conn->debug=true;
        $isql = "update radicado
					set
					  RADI_USU_ANTE='$loginOrigen'
					  ,RADI_DEPE_ACTU=999
					  ,RADI_USUA_ACTU=1
					  ,CARP_CODI=$carp_codi
					  ,CARP_PER=$carp_per
					  ,RADI_LEIDO=0
					  ,radi_fech_agend=null
					  ,radi_agend=null
					  ,CODI_NIVEL=1
					  ,SGD_SPUB_CODIGO=0
					  ,RADI_NRR=1
					  ,pc_search_indexed=0
				 where radi_depe_actu=$depOrigen
				 	   AND radi_usua_actu=$codUsOrigen
					   AND RADI_NUME_RADI in($radicadosIn)";
        //$this->conn->Execute($isql);
        $this->db->conn->Execute($isql); # Ejecuta la busqueda

        $this->insertarHistorico($radicados, $depOrigen, $codUsOrigen, $id_rol, 999, 1, $observa, 65);
        return $isql;
    }

    /**
     * Nueva Funcion para agendar.
     * Este metodo permite programar un radicado para una fecha especifica, el arreglo con la version anterior
     * , es que no se borra el agendado cuando el radicado sale del usuario actual.
     *
     * @author JAIRO LOSADA JUNIO 2006
     * @version 3.5.1
     *
     * @param array int $radicados
     * @param varchar $loginOrigen
     * @param numeric $depOrigen
     * @param numeric $codUsOrigen
     * @param varchar $observa
     * @param date $fechaAgend
     * @return boolean
     */
    function agendar($radicados, $loginOrigen, $depOrigen, $codUsOrigen, $observa, $fechaAgend, $id_rol) {
        $whereNivel = "";
        $radicadosIn = join(",", $radicados);
        $carp_codi = substr($depOrigen, 0, 2);
        $carp_per = 1;
        $sqlFechaAgenda = $this->db->conn->DBDate($fechaAgend);
        $this->datosUs($codUsOrigen, $depOrigen, $id_rol);
        $usuaDocAgen = $this->usuaDocB;
        //return $usuaDocAgen;

        foreach ($radicados as $noRadicado) {
            # Busca el usuairo Origen para luego traer sus datos.
            //$this->db->conn->debug=true;
            $rad = array();
            //$ob = "Agendado para el $fechaAgend - " . $observa;
            if ($usuaDocAgen) {
                $record["RADI_NUME_RADI"] = $noRadicado;
                $record["DEPE_CODI"] = $depOrigen;
                $record["SGD_AGEN_OBSERVACION"] = "'Agendado para el $fechaAgend - $observa '";
                $record["USUA_DOC"] = "'$usuaDocAgen'";
                $record["SGD_AGEN_FECH"] = $this->db->conn->OffsetDate(0, $this->db->conn->sysTimeStamp);
                $record["SGD_AGEN_FECHPLAZO"] = $sqlFechaAgenda;
                $record["SGD_AGEN_ACTIVO"] = 1;
                $insertSQL = $this->db->insert("SGD_AGEN_AGENDADOS", $record, "true");
                $rad[0] = $noRadicado;
                //$this->insertarHistorico($radicados,$depOrigen,$codUsOrigen,$id_rol,$depOrigen,$codUsOrigen,$id_rol, $observa, 14);
                $this->insertarHistorico($rad, $depOrigen, $codUsOrigen, $id_rol, $depOrigen, $codUsOrigen, $id_rol, "Agendado para el $fechaAgend - " . $observa, 14);
            }
        }



        //$this->conn->Execute($isql);
        return $isql;
    }

    /**
     * Metodo que sirve para sacar uno o varios radicados de agendado
     *
     * @param array $radicados
     * @param unknown_type $loginOrigen
     * @param unknown_type $depOrigen
     * @param unknown_type $codUsOrigen
     * @param unknown_type $observa
     * @return unknown
     */
    function noAgendar($radicados, $loginOrigen, $depOrigen, $codUsOrigen, $observa, $id_rol) {
        $this->datosUs($codUsOrigen, $depOrigen, $id_rol);
        $usuaDocAgen = $this->usuaDocB;
        $whereNivel = "";
        $radicadosIn = join(",", $radicados);
        $carp_codi = substr($depOrigen, 0, 2);
        $isql = "update sgd_agen_agendados
					set
					  SGD_AGEN_ACTIVO=0
				 where
				   RADI_NUME_RADI in($radicadosIn)
				   AND USUA_DOC='" . $usuaDocAgen . "'";
        //$this->conn->Execute($isql);
//        $this->db->conn->debug=true;
        $this->db->conn->Execute($isql); # Ejecuta la busqueda
        $this->insertarHistorico($radicados, $depOrigen, $codUsOrigen, $id_rol, $depOrigen, $codUsOrigen, $id_rol, $observa, 15);
        return $isql;
    }

    function devolver($radicados, $loginOrigen, $depOrigen, $codUsOrigen, $tomarNivel, $observa, $id_rol, $usuaDocOrigen) {
        $whereNivel = "";
        $retorno = "";
        //$this->db->conn->debug=true;
        $this->db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        foreach ($radicados as $noRadicado) {
            $sql = "SELECT usuario.usua_login, usuario.usua_codi,  usuario.usua_doc, usuario.usua_nomb, sgd_drm_dep_mod_rol.sgd_drm_valor as codi_nivel,
usuario.usua_email as email,  sgd_urd_usuaroldep.depe_codi, sgd_urd_usuaroldep.rol_codi,a.ra_asun,a.tdoc_codi
  FROM usuario, sgd_urd_usuaroldep, sgd_mod_modules, sgd_drm_dep_mod_rol,radicado a
   WHERE usuario.usua_codi = sgd_urd_usuaroldep.usua_codi AND sgd_mod_modules.sgd_mod_id = sgd_drm_dep_mod_rol.sgd_drm_modecodi
    AND sgd_drm_dep_mod_rol.sgd_drm_rolcodi = sgd_urd_usuaroldep.rol_codi AND sgd_drm_dep_mod_rol.sgd_drm_depecodi = sgd_urd_usuaroldep.depe_codi
     AND sgd_mod_modules.sgd_mod_estado = 1  AND sgd_mod_modules.sgd_mod_modulo = 'codi_nivel'
     and a.RADI_USU_ANTE=usuario.USUA_LOGIN AND a.RADI_NUME_RADI =  $noRadicado";
            # Busca el usuairo Origen para luego traer sus datos.
            $rs = $this->db->conn->Execute($sql); # Ejecuta la busqueda
            $usNivel = $rs->fields['CODI_NIVEL'];
            $depDestino = $rs->fields['DEPE_CODI'];
            $codUsDestino = $rs->fields['USUA_CODI'];
            $nombDestino = $rs->fields['USUA_NOMB'];
            $docUsuaDest = $rs->fields['USUA_DOC'];
            $rollUsuaDest = $rs->fields['ROL_CODI'];
            $correoUsuaDest = $rs->fields['EMAIL'];
	    $loginDest=$rs->fields['USUA_LOGIN'];
	    $raAsun=$rs->fields['RA_ASUN'];
	    $tdoc=$rs->fields['TDOC_CODI'];
            $rad = array();
            if ($codUsDestino) {
                if ($tomarNivel == "si") {
                    $whereNivel = ",CODI_NIVEL=$usNivel";
                }
                $radicadosIn = join(",", $radicados);
                $proccarp = "Dev. ";
                $carp_codi = 12;
                $carp_per = 0;
                /* $isql = "update radicado
                  set
                  RADI_USU_ANTE='$loginOrigen'
                  ,RADI_DEPE_ACTU=$depDestino
                  ,RADI_USUA_ACTU=$codUsDestino
                  ,CARP_CODI=$carp_codi
                  ,CARP_PER=$carp_per
                  ,RADI_LEIDO=0
                  , radi_fech_agend=null
                  ,radi_agend=null
                  $whereNivel
                  where radi_depe_actu=$depOrigen
                  AND radi_usua_actu=$codUsOrigen
                  AND RADI_NUME_RADI = $noRadicado"; */
                $isql = "update radicado
				set
				  RADI_USU_ANTE='$loginOrigen'
                                  ,USUA_DOC_ANTE=$usuaDocOrigen
                                  ,USUA_DEPE_ANTE='$depOrigen'
                                  ,USUA_ROL_ANTE='$id_rol'
				  ,RADI_DEPE_ACTU=$depDestino
				  ,RADI_USUA_ACTU=$codUsDestino
                                   ,RADI_USUA_DOC_ACTU=$docUsuaDest
                                   ,ID_ROL=$rollUsuaDest
				  ,CARP_CODI=$carp_codi
				  ,CARP_PER=$carp_per
				  ,RADI_LEIDO=0
				  , radi_fech_agend=null
				  ,radi_agend=null
				  ,pc_search_indexed=0
				  $whereNivel
			 where radi_depe_actu=$depOrigen
			 	   AND radi_usua_actu=$codUsOrigen
				   AND RADI_NUME_RADI = $noRadicado";
                //$this->conn->Execute($isql);
                $this->db->conn->Execute($isql); # Ejecuta la busqueda
                $rad[] = $noRadicado;
		$asu[] = $raAsun;
		$tipi[] = $tdoc;
                $this->insertarHistorico($rad, $depOrigen, $codUsOrigen, $id_rol, $depDestino, $codUsDestino, $rollUsuaDest, $observa, 12);
                $this->alertaEmail($correoUsuaDest, $rad, $asu,$loginOrigen, $loginDest,$tipi);
                array_splice($rad, 0);
                $retorno = $retorno . "$noRadicado ------> $nombDestino <br>";
                // MODIFICADO PARA GENERAR ALERTAS
                //JUNIO DE 2009
                $this->registrarNovedad('NOV_DEV', $docUsuaDest, $noRadicado);
                //////////////////////////////////
            } else {
                $retorno = $retorno . "<font color=red>$noRadicado ------> Usuario Anterior no se encuentra o esta inactivo</font><br>";
            }
        }
        return $retorno;
    }

    function alertaEmail($correo_usuario, $radicado, $asunto,$loginOrigen,$loginDest,$tipi) {
        /*  $correo_Sistema='no-responder@dne.gov.co';
          $subject='Alerta de Radicado(s) '.$radicado;
          $mensaje="Usted ha recibido en su bandeja de Orfeo los siguientes radicado(s) : ";
          for ($index = 0; $index < count($radicado); $index++) {
          if($correo_usuario=='orfeo@dne.gov.co')
          $mensaje.= "\n <a href='$amnienteURL/core/Modulos/radicacion/vista/image.php?radicado={$radicado [$index]}'> {$radicado [$index]}</a> -  {$asunto[$index]}";
          else
          $mensaje.= "\n {$radicado [$index]} -  {$asunto[$index]}";
          }
          echo "$correo_usuario,$subject,$mensaje,From: $correo_Sistema";
          mail($correo_usuario,$subject,$mensaje,"From: $correo_Sistema");
         */
        ini_set('display_errors', 'on');
        //include_once($ruta_raiz."/include/mail/infomail.php");
        $correo_Sistema = 'orfeo@imprenta.gov.co';
        //  include($ruta_raiz."/../core/config/config-inc.php");
        $subject = "Documento(s) asignado(s) a su cuenta de ORFEO -- $loginDest";
        $mensaje = "Cordial Saludo: <br>\nUsted ha recibido en su cuenta de Orfeo lo siguiente:<br>\n<table border=1><tr><th>No.</th><th>Radicado</th><th>Asunto</th><th>Usuario anterior</th><th>Estado</th></tr>\n";
	$l=1;
        for ($index = 0; $index < count($radicado); $index++) {

            // if( strtoupper($correo_usuario)=='MPERRY@DNE.GOV.CO'){
	    if($tipi[$index]==0){
		$estado="Sin tipificar";
	    }
	    else{
		$estado="Tipificado";
	    }
            $mensaje.= "<tr><td>$l</td><td>{$radicado [$index]}</td><td>".htmlentities(substr($asunto[$index],0,50))."</td><td>$loginOrigen</td><td>$estado</td></tr>";
            //        $mraba=1;
            //}
            /*  else{
              $mensaje.= "\n <a href='http://forfeo.dne.gov.co/orfeo/core/Modulos/radicacion/vista/image2.php?radicado={$radicado [$index]}'> {$radicado [$index]}</a> -  {$asunto[$index]}";
              } */
            //   $mensaje.= "\n {$radicado [$index]} -  {$asunto[$index]}";
            //           $mensaje.= "\n {$radicado [$index]} -  {$asunto[$index]}";
            //$subject.=' ' . $radicado[$index];
	    $l++;
        }
	$mensaje.="</table>";
	//include_once "$ruta_raiz/core/vista/correo.php";
         if($correo_usuario!=null && $correo_usuario!=''){
             $headers   = array();
             $headers[] = "MIME-Version: 1.0";
             $headers[] = "Content-type: text/html; charset=iso-8859-1";
             $headers[] = "From: ORFEO IMPRENTA NACIONAL DE COLOMBIA<".$correo_Sistema.">";
             $headers[] = "To: <".$correo_usuario.">";
             $headers[] = "Subject: {$subject}";
             $headers[] = "X-Mailer: PHP/".phpversion();
             mail($correo_usuario, $subject, $mensaje, implode("\r\n", $headers));
         }
    }
  function alertaEmail2($radicado,$asunto,$depe){

       $query = "SELECT   u.usua_email FROM   usuario u, sgd_urd_usuaroldep ur
                 WHERE      ur.usua_codi = u.usua_codi AND   ur.depe_codi = $depe AND
                 ur.rol_codi = 1 ";
        $rs=$this->db->conn->Execute($query);
        $correo_usuario = $rs->fields["USUA_EMAIL"];
        ini_set('display_errors', 'on');
        //include_once($ruta_raiz."/include/mail/infomail.php");
         $correo_Sistema='orfeo@dne.gov.co';
      //  include($ruta_raiz."/../core/config/config-inc.php");
        $subject='Alerta de Radicado(s) ';
        $mensaje="Usted ha recibido en su bandeja de Orfeo los siguientes radicado(s) : ";
        $mraba='';
          for ($index = 0; $index < count($radicado); $index++) {

       if( strtoupper($correo_usuario)=='MPERRY@DNE.GOV.CO'){
                   $mensaje.= "\n <a href='http://forfeo.dne.gov.co/orfeo/core/Modulos/radicacion/vista/image2.php?radicado={$radicado [$index]}'> {$radicado [$index]}</a> -  {$asunto[$index]}";
                   $mraba=1;
          }else{
                   //$mensaje.= "\n <a href='http://forfeo.dne.gov.co/orfeo/core/Modulos/radicacion/vista/image.php?radicado={$radicado [$index]}'> {$radicado [$index]}</a> -  {$asunto[$index]}";
                   $mensaje.= "\n {$radicado [$index]} -  {$asunto[$index]}";
                }
           $subject.=' '.$radicado[$index];
        }

        mailOrfeo($correo_Sistema,$correo_usuario,$subject,$mensaje);
      if( $mraba==1){
        mailOrfeo($correo_Sistema,'mraba@dne.gov.co',$subject,$mensaje);
       }
}
}

?>
