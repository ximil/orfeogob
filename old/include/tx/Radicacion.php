<?php 
//include_once($ruta_raiz . '/include/carpFuction.php');
//echo $ruta_raiz . '/include/carpFuction.php';
class Radicacion {
    /** Aggregations: */
    /** Compositions: */
    /*     * * Attributes: ** */
    /**
     * Clase que maneja los Historicos de los documentos
     *
     * @param int Dependencia Dependencia de Territorial que Anula
     * @db Objeto conexion
     * @access public
     */

    /**
     *  VARIABLES DE DATOS PARA LOS RADICADOS
     */
    var $db;
    var $tipRad;
    var $radiTipoDeri;
    var $nivelRad;
    var $radiCuentai;
    var $eespCodi;
    var $mrecCodi;
    var $radiFechOfic;
    var $radiNumeDeri;
    var $tdidCodi;
    var $descAnex;
    var $radiNumeHoja;
    var $radiPais;
    var $raAsun;
    var $radiDepeRadi;
    var $radiUsuaActu;
    var $radiDepeActu;
    var $carpCodi;
    var $radiNumeRadi;
    var $trteCodi;
    var $radiNumeIden;
    var $radiFechRadi;
    var $sgd_apli_codi;
    var $tdocCodi;
    var $nofolios;
    var $noanexos;
    var $guia;
    var $estaCodi;
    var $radiPath;
    var $nguia;
    var $tsopt;
    var $urgnt;
    var $dptcn;
    var $id_rol;
    var $radi_usa_doc;
    var $data_rol;
    var $radiRolRadi;

    /**
     *  VARIABLES DEL USUARIO ACTUAL
     */
    var $dependencia;
    var $usuaDoc;
    var $usuaLogin;
    var $usuaCodi;
    var $codiNivel;
    var $noDigitosRad;

    function Radicacion($db) {

        /**
         * Constructor de la clase Historico
         * @db variable en la cual se recibe el cursor sobre el cual se esta trabajando.
         *
         */
        global $HTTP_SERVER_VARS, $PHP_SELF, $HTTP_SESSION_VARS, $_GET, $krd;
        //global $_GET;
        $this->db = $db;

        $this->noDigitosRad = 6;
	$id=(isset($id))?$id:null;
        $curr_page = $id . '_curr_page';
        $this->dependencia = $_SESSION['dependencia'];
        $this->usuaDoc = $_SESSION['usua_doc'];
        //$this->usuaDoc = $_SESSION['codi_nivel'];
        $this->usuaLogin = $krd;
        $this->usuaCodi = $_SESSION['codusuario'];
        (isset($_GET['nivelus'])) ? $this->codiNivel = $_GET['nivelus'] : $this->codiNivel = $_SESSION['codi_nivel'];
    }

    function newRadicado($tpRad, $tpDepeRad, $anexo = NULL) {
        /** FUNCION QUE INSERTA UN RADICADO NUEVO
         *
         */
        /**
         * Busca el Nivel de Base de datos.
         *
         */
        //echo $tpRad;
        $rolsql = "sgd_urd_usuaroldep.rol_codi = " . $this->id_rol . " AND";
        if ($tpRad != 2 || $anexo != NULL) {
            $whereusuaactu = "usuario.usua_codi = " . $this->radiUsuaActu . " AND ";
        } else {
            $rolsql = "sgd_urd_usuaroldep.rol_codi = 1 AND";
        }

        $whereNivel = "";
        $sql = "SELECT 
  sgd_drm_dep_mod_rol.sgd_drm_valor as CODI_NIVEL ,
  sgd_drm_dep_mod_rol.sgd_drm_rolcodi,    usuario.usua_codi  ucodi ,usuario.usua_doc  doc
FROM 
  usuario, 
  sgd_urd_usuaroldep, 
  sgd_mod_modules, 
  sgd_drm_dep_mod_rol
WHERE 
  usuario.usua_codi = sgd_urd_usuaroldep.usua_codi AND
  sgd_mod_modules.sgd_mod_id = sgd_drm_dep_mod_rol.sgd_drm_modecodi AND
  sgd_drm_dep_mod_rol.sgd_drm_rolcodi = sgd_urd_usuaroldep.rol_codi AND
  sgd_drm_dep_mod_rol.sgd_drm_depecodi = sgd_urd_usuaroldep.depe_codi AND
  sgd_mod_modules.sgd_mod_estado = 1 AND 
  $whereusuaactu
     $rolsql
      sgd_urd_usuaroldep.depe_codi = " . $this->radiDepeActu . " AND
  sgd_mod_modules.sgd_mod_modulo = 'codi_nivel'";
        # Busca el usuairo Origen para luego traer sus datos.
        $rs = $this->db->conn->query($sql); # Ejecuta la busqueda
        $usNivel = $rs->fields["CODI_NIVEL"];
        $this->data_rol['codusua'] = $rs->fields["UCODI"];
        $this->data_rol['rol'] = $rs->fields["SGD_DRM_ROLCODI"];
        $usuadocactu = $rs->fields["DOC"];
        //$this->radi_usua_doc;
        # Busca el usuairo Origen para luego traer sus datos.
        $SecName = "SECR_TP$tpRad" . "_" . $tpDepeRad;
        $secNew = $this->db->conn->nextId($SecName);

        if ($secNew == 0) {
            $this->db->conn->RollbackTrans();
            $secNew = $this->db->conn->nextId($SecName);
            if ($secNew == 0)
                die("<hr><b><font color=red><center>Error no genero un Numero de Secuencia<br>SQL: $secNew</center></font></b><hr>");
        }
        // $this->dependencia
        $secp2 = date("Y") . '000';
        $secp1 = $secp2 + $_SESSION['dependencia']; //$tpDepeRad;
        $newRadicado = $secp1 . str_pad($secNew, $this->noDigitosRad, "0", STR_PAD_LEFT) . $tpRad;
        if (!$this->radiTipoDeri) {
            $recordR["radi_tipo_deri"] = "0";
        } else {
            $recordR["radi_tipo_deri"] = $this->radiTipoDeri;
        }
        if (!$this->carpCodi)
            $this->carpCodi = 0;
        if (!$this->radiNumeDeri)
            $this->radiNumeDeri = 0;
        if (!$this->nivelRad)
            $this->nivelRad = 0;
        if (!$this->mrecCodi)
            $this->mrecCodi = 0;
        if (!$this->carpPer)
            $this->carpPer = 0;
        $recordR["SGD_SPUB_CODIGO"] = $this->nivelRad;
        $recordR["RADI_CUENTAI"] = $this->radiCuentai;
        $recordR["EESP_CODI"] = $this->eespCodi ? $this->eespCodi : 0;
        $recordR["MREC_CODI"] = $this->mrecCodi;
        $recordR["RADI_ROL_RADI"] = $this->radiRolRadi;
        // Modificado SGD 06-Septiembre-2007
        /*        switch ($GLOBALS['driver']) {
          case 'postgres':
          $recordR["radi_fech_ofic"] = $this->radiFechOfic;
          break;
          default: */
        $recordR["radi_fech_ofic"] = $this->db->conn->sysTimeStamp;
        // = $this->db->conn->DBDate($this->radiFechOfic);
        //}
        //$recordR["radi_tipo_deri"]=$this->radiTipoDeri;
        $recordR["RADI_NUME_DERI"] = $this->radiNumeDeri;
        $recordR["RADI_USUA_RADI"] = $this->usuaCodi;
        $recordR["RADI_PAIS"] = "'" . $this->radiPais . "'";

        // $recordR["RA_ASUN"]="'".$this->raAsun."'";
        /* $recordR["radi_desc_anex"]="'".$this->descAnex."'";
         */
//                echo $this->raAsun.'hd';
        $recordR["RA_ASUN"] = $this->db->conn->qstr($this->raAsun);
        $recordR["radi_desc_anex"] = $this->db->conn->qstr($this->descAnex);
        $recordR["RADI_DEPE_RADI"] = $this->radiDepeRadi;
        $recordR["RADI_USUA_ACTU"] = $this->data_rol['codusua'];
        $recordR["carp_codi"] = $this->carpCodi;
        if (!$this->carpPer)
            $this->carpPer = 0;
        $recordR["CARP_PER"] = $this->carpPer;
        $recordR["RADI_NUME_RADI"] = $newRadicado;
        $recordR["TRTE_CODI"] = $this->trteCodi;
        //$recordR["RADI_FECH_RADI"] = $this->db->conn->OffsetDate(0, $this->db->conn->sysTimeStamp);
        $recordR["RADI_FECH_RADI"] = $this->db->conn->sysTimeStamp;
        $recordR["RADI_DEPE_ACTU"] = $this->radiDepeActu;
        //$recordR["TDOC_CODI"] = $this->tdocCodi;
                 if (!$this->tdocCodi) {
            $recordR["TDOC_CODI"] = 0;
        } else {
            $recordR["TDOC_CODI"] = $this->tdocCodi;
        }
        $recordR["RADI_NUME_GUIA"] = "'$this->guia'";
        if (!empty($this->nofolios)) {
            $recordR["RADI_NUME_FOLIO"] = $this->nofolios;
        }

        if (!empty($this->noanexos)) {
            $recordR["RADI_NUME_ANEXO"] = $this->noanexos;
        }
        //$recordR["TDID_CODI"] = $this->tdidCodi;
         if (!$this->tdidCodi) {
            $recordR["TDID_CODI"] = 0;
        } else {
            $recordR["TDID_CODI"] = $this->tdidCodi;
        }
        $recordR["ID_ROL"] = $this->data_rol['rol'];
        $recordR["DEPE_CODI"] = $tpDepeRad;
        $recordR["SGD_TRAD_CODIGO"] = $tpRad;
        if (!$usNivel) {
            $usNivel = 0;
        }
        $recordR["CODI_NIVEL"] = $usNivel;
        //print_r($GLOBALS);
        if ($GLOBALS['nivelus'])
            $recordR["CODI_NIVEL"] = $GLOBALS['nivelus'];

        $recordR["SGD_APLI_CODI"] = $this->sgd_apli_codi;
        $recordR["RADI_USUA_DOC_ACTU"] = $usuadocactu;
        if(!$this->radiPath)
            $recordR["RADI_PATH"] = "NULL";
            else
        $recordR["RADI_PATH"] = "$this->radiPath";

        $whereNivel = "";
        $insertSQL = $this->db->insert("RADICADO", $recordR, "true");
        //$insertSQL = $this->db->conn->Replace("RADICADO", $recordR, "RADI_NUME_RADI", false);       
        // bandeja($this->db, 'sgd_bcc_tp' . $tpRad, $this->id_rol, $this->radiDepeActu, $this->radiUsuaActu, 'new');

        if (!$insertSQL) {
            echo "<hr><b><font color=red>Error no se inserto sobre radicado<br>SQL: " . $this->db->querySql . "</font></b><hr>";
        }
        //$this->db->conn->CommitTrans();
        return $newRadicado;
    }

    function updateRadicado($radicado, $radPathUpdate = null) {
        $recordR["radi_cuentai"] = $this->radiCuentai;
        $recordR["eesp_codi"] = $this->eespCodi;
        $recordR["mrec_codi"] = $this->mrecCodi;
        $recordR["radi_fech_ofic"] = $this->db->conn->DBDate($this->radiFechOfic);
        //$recordR["radi_pais"] = "'" . $this->radiPais . "'";
        $recordR["ra_asun"] = $this->db->conn->qstr($this->raAsun);
        $recordR["radi_desc_anex"] = $this->db->conn->qstr($this->descAnex);
        $recordR["trte_codi"] = $this->trteCodi;
        $recordR["tdid_codi"] = $this->tdidCodi;
        $recordR["radi_nume_radi"] = $radicado;
        $recordR["SGD_APLI_CODI"] = $this->sgd_apli_codi;
        $recordR["RADI_NUME_FOLIO"] = $this->nofolios;
        //$recordR["TDOC_CODI"] 		= $this->tdocCodi;		
        $recordR["RADI_NUME_ANEXO"] = $this->noanexos;
        $recordR["RADI_NUME_GUIA"] = "'$this->guia'";

        // Linea para realizar radicacion Web de archivos pdf
        if (!empty($radPathUpdate) && $radPathUpdate != "") {
            $archivoPath = explode(".", $radPathUpdate);
            // Sacando la extension del archivo
            $extension = array_pop($archivoPath);
            if ($extension == "pdf") {
                $recordR["radi_path"] = "'" . $radPathUpdate . "'";
            }
        }
        $insertSQL = $this->db->conn->Replace("RADICADO", $recordR, "radi_nume_radi", false);
        return $insertSQL;
    }

    /** FUNCION ANEXOS IMPRESOS RADICADO
     * Busca los anexos de un radicado que se encuentran impresos.
     * @param $radicado int Contiene el numero de radicado a Buscar
     * @return Arreglo con los anexos impresos
     * Fecha de creaci�n: 10-Agosto-2006
     * Creador: Supersolidaria
     * Fecha de modificaci�n:
     * Modificador:
     */
    function getRadImpresos($radicado) {
        $sqlImp = "SELECT A.RADI_NUME_SALIDA
                   FROM ANEXOS A, RADICADO R
                   WHERE A.ANEX_RADI_NUME=R.RADI_NUME_RADI
                   AND ( A.ANEX_ESTADO=3 OR A.ANEX_ESTADO=4 )
                   AND R.RADI_NUME_RADI = " . $radicado;
        // print $sqlImp;
        $rsImp = $this->db->conn->query($sqlImp);

        if ($rsImp->EOF) {
            $arrAnexos[0] = 0;
        } else {
            $e = 0;
            while ($rsImp && !$rsImp->EOF) {
                $arrAnexos[$e] = $rsImp->fields['RADI_NUME_SALIDA'];
                $e++;
                $rsImp->MoveNext();
            }
        }
        return $arrAnexos;
    }

    /** FUNCION DATOS DE UN RADICADO
     * Busca los datos de un radicado.
     * @param $radicado int Contiene el numero de radicado a Buscar
     * @return Arreglo con los datos del radicado
     * Fecha de creaci�n: 29-Agosto-2006
     * Creador: Supersolidaria
     * Fecha de modificaci�n:
     * Modificador:
     */
    function getDatosRad($radicado) {
        $query = 'SELECT RAD.RADI_FECH_RADI, RAD.RADI_PATH, TPR.SGD_TPR_DESCRIP,';
        $query .= ' RAD.RA_ASUN';
        $query .= ' FROM RADICADO RAD';
        $query .= ' LEFT JOIN SGD_TPR_TPDCUMENTO TPR ON TPR.SGD_TPR_CODIGO = RAD.TDOC_CODI';
        $query .= ' WHERE RAD.RADI_NUME_RADI = ' . $radicado;
        // print $query;
        $rs = $this->db->conn->query($query);

        $arrDatosRad['fechaRadicacion'] = $rs->fields['RADI_FECH_RADI'];
        $arrDatosRad['ruta'] = $rs->fields['RADI_PATH'];
        $arrDatosRad['tipoDocumento'] = $rs->fields['SGD_TPR_DESCRIP'];
        $arrDatosRad['asunto'] = $rs->fields['RA_ASUN'];

        return $arrDatosRad;
    }

    function getDatosRadClass($radicado) {
        $query = 'SELECT RADI_FECH_RADI, RADI_PATH, RA_ASUN,
        			radi_cuentai,eesp_codi,mrec_codi,radi_fech_ofic,radi_pais,radi_desc_anex,trte_codi
        			MUNI_CODI, DPTO_CODI
        			,SGD_APLI_CODI,RADI_NUME_GUIA,RADI_NUME_FOLIO,RADI_NUME_ANEXO';
        //$query .= ' RAD.RA_ASUN';
        $query .= ' FROM RADICADO ';
        $query .= ' WHERE RADI_NUME_RADI = ' . $radicado;

        $rs = $this->db->conn->query($query);
        $this->sgd_apli_codi = $rs->fields["SGD_APLI_CODI"];
        $this->radiCuentai = $rs->fields['RADI_CUENTAI'];
        ;
        $this->eespCodi = $rs->fields['EESP_CODI'];
        $this->mrecCodi = $rs->fields['MREC_CODI'];
        $this->radiFechOfic = $rs->fields['RADI_FECH_OFIC'];
        $this->tdidCodiRadi = $rs->fields['TDID_CODI'];
        $this->descAnex = $rs->fields['RADI_DESC_ANEX'];
        $this->radiPais = $rs->fields['RADI_PAIS'];
        $this->raAsun = $rs->fields['RA_ASUN'];
        $this->radiNumeRadi = $radicado;
        $this->trteCodi = $rs->fields['TRTE_CODI'];
        $this->nofolios = $rs->fields['RADI_NUME_FOLIO'];
        $this->noanexos = $rs->fields['RADI_NUME_ANEXO'];
        $this->guia = $rs->fields['RADI_NUME_GUIA'];

        //return $query;
    }

}

// Fin de Class Radicacion
?>
