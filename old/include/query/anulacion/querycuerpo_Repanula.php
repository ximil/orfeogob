<?php 
	/**
	  * CONSULTA VERIFICACION PREVIA A LA RADICACION
	  */
	switch($db->driver)
	{  
	/* case 'mssql':
	  $isql = 'select
								convert(varchar(15), b.RADI_NUME_RADI) "IMG_Numero Radicado"
								,b.RADI_PATH "HID_RADI_PATH"
								,convert(varchar(15),b.RADI_NUME_DERI) "Radicado Padre"
								,b.RADI_FECH_RADI "HOR_RAD_FECH_RADI"
								,'.$sqlFecha.' "Fecha Radicado"
								,b.RA_ASUN "Descripcion"
								,c.SGD_TPR_DESCRIP "Tipo Documento"
								,d.usua_anu_acta "IMG_No Acta"
								,d.sgd_anu_path_acta "HID_PATH_ACTA"
						 from
						 radicado b,
						 SGD_TPR_TPDCUMENTO c,
						 sgd_anu_anulados d
					 where 
					 	b.radi_nume_radi is not null
						and b.radi_nume_radi=d.radi_nume_radi
						and '.$db->conn->substr.'(convert(char(15),b.radi_nume_radi), 5, 3)='.$dep_sel.'
						and b.tdoc_codi=c.sgd_tpr_codigo
						'.$whereTpAnulacion.'
						'.$whereFiltro.'
					  order by  select d.sgd_exp_numero, d.sgd_exp_estado, a.radi_path, d.RADI_NUME_RADI , a.RADI_NUME_HOJA, a.RADI_FECH_RADI, a.RADI_NUME_RADI, a.RA_ASUN , a.RADI_PATH , a.RADI_USUA_ACTU , TO_CHAR(a.RADI_FECH_RADI, 'DD-MM-YYYY HH24:MI AM') AS FECHA , b.sgd_tpr_descrip, b.sgd_tpr_codigo, b.sgd_tpr_termino, RADI_LEIDO, RADI_TIPO_DERI, RADI_NUME_DERI, a.radi_depe_actu, e.depe_nomb, f.usua_nomb, d.sgd_exp_fech_arch, d.sgd_exp_fech , d.SGD_EXP_ASUNTO from radicado a, sgd_tpr_tpdcumento b, SGD_EXP_EXPEDIENTE d, DEPENDENCIA e, USUARIO f where f.usua_codi=a.radi_usua_actu and f.depe_codi=a.radi_depe_actu and e.depe_codi=a.radi_depe_actu and a.tdoc_codi=b.sgd_tpr_codigo AND a.radi_nume_radi=d.radi_nume_radi and d.sgd_exp_estado=0 and cast(d.depe_codi as varchar) like '%' and (upper(d.sgd_exp_numero) like '%2011%' and upper(cast(d.RADI_NUME_RADI as varchar)) like '%%') and d.sgd_exp_fech <= '2011-09-03' and d.sgd_exp_fech >= '2011-08-02' order by d.sgd_exp_numero'.$order .' ' .$orderTipo;
	break;	*/	
	case 'oracle':
	case 'oci8':
	case 'oci805':	
	 $isql = 'select
								b.RADI_NUME_RADI	 as "IMG_Numero Radicado"
								,b.RADI_PATH 	       	as "HID_RADI_PATH"
								,b.RADI_NUME_DERI 	as "Radicado Padre"
								,b.RADI_FECH_RADI 	as "HOR_RAD_FECH_RADI"
								,b.RADI_FECH_RADI 	as "Fecha Radicado"
								,b.RA_ASUN 		as "Descripcion"
								,c.SGD_TPR_DESCRIP 	as "Tipo Documento"
								,d.usua_anu_acta 	as "IMG_No Acta"
								,d.sgd_anu_path_acta 	as "HID_PATH_ACTA"
						 from
						 radicado b,
						 SGD_TPR_TPDCUMENTO c,
						 sgd_anu_anulados d
					 where 
					 	b.radi_nume_radi is not null
						and b.radi_nume_radi=d.radi_nume_radi
						and b.radi_depe_radi = '.$dep_sel.'
						and b.tdoc_codi=c.sgd_tpr_codigo
						'.$whereTpAnulacion.'
						'.$whereFiltro.'
					  order by '.$order .' ' .$orderTipo;
				break;		
		case 'postgres':
	 $isql = 'select
								b.RADI_NUME_RADI	 as "IMG_Numero Radicado"
								,b.RADI_PATH 	       	as "HID_RADI_PATH"
								,b.RADI_NUME_DERI 	as "Radicado Padre"
								,b.RADI_FECH_RADI 	as "HOR_RAD_FECH_RADI"
								,b.RADI_FECH_RADI 	as "Fecha Radicado"
								,b.RA_ASUN 		as "Descripcion"
								,c.SGD_TPR_DESCRIP 	as "Tipo Documento"
								,d.usua_anu_acta 	as "IMG_No Acta"
								,d.sgd_anu_path_acta 	as "HID_PATH_ACTA"
						 from
						 radicado b,
						 SGD_TPR_TPDCUMENTO c,
						 sgd_anu_anulados d
					 where 
					 	b.radi_nume_radi is not null
						and b.radi_nume_radi=d.radi_nume_radi
						and b.radi_depe_radi = '.$dep_sel.'
						and b.tdoc_codi=c.sgd_tpr_codigo
						'.$whereTpAnulacion.'
						'.$whereFiltro.'
					  order by '.$order .' ' .$orderTipo;
				break;		
	}
?>
