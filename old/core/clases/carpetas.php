<?php /** 
 * Carpetas Orfeo es la clase encargada de gestionar las operaciones carpetas
 * @author Hardy Deimont Niño  Velasquez
 * @name modeloUsuarioOrfeo
 * @version	1.0
 */ 
if (! $ruta_raiz)
	$ruta_raiz = '../..';
include_once "$ruta_raiz/core/modelo/modeloCarpetas.php";
	//==============================================================================================	
	// carpetas usuarioOrfeo
	//==============================================================================================	
	
	/**
	 *  Objecto carpetas. 
	 */ 
class carpetas {
	private $carpertas;
    private $modelo;
	
	function __construct($ruta_raiz){
	 $this->modelo= new modeloCarpetas($ruta_raiz);
	}
	
	function ConsultarCapetasPersonalizado($depe_codi) {
		
	}
	function ConsultarCarpetas() {
	   return $this->modelo->carpetas();
	}
	function ConsultarCarpetasPer($dependecia,$rolcodi) {
	   return $this->modelo->carpetasper($rolcodi,$dependecia);
	}
	function ConsultarCarpetasContador($dependecia,$rolcodi,$usua_doc,$login) {
	     $capertas=$this->ConsultarCarpetas();
	     $agendado=$this->modelo->Agendado($usua_doc);
	     $agenVen=$this->modelo->AgenVencido($usua_doc);
	     $Informados=$this->modelo->informados($usua_doc,$rolcodi);
	     $VB=$this->modelo->VistoBueno($usua_doc,$rolcodi,$login);
	     $countcarp=$this->modelo->carpetasCuenta($dependecia,$rolcodi,$usua_doc);
	     
	     for ($index = 1; $index <= count($capertas); $index++) {
	     	 $capertas[$index]['CUENTA']=0;
	     	for ($ii = 0; $ii < count($countcarp); $ii++) {
	     		if($capertas[$index]['CARP_DESC']==$countcarp[$ii]['CARP_DESC']){
	     		 $capertas[$index]['CUENTA']=$countcarp[$ii]['CUENTA'];
	     		}

	     	}
	     	if($capertas[$index]['CARP_CODI']==11)
				$capertas[$index]['CUENTA']=$VB['CUENTA'];
	     }
	     $cot=count($capertas);
	      $capertas[$cot+1]['CUENTA']=$agendado['CUENTA'];
	      $capertas[$cot+1]['CARP_DESC']=$agendado['CARP_DESC'];
	      $capertas[$cot+2]['CUENTA']=$agenVen['CUENTA'];
	      $capertas[$cot+2]['CARP_DESC']=$agenVen['CARP_DESC'];
	      $capertas[$cot+3]['CUENTA']=$Informados['CUENTA'];
	      $capertas[$cot+3]['CARP_DESC']=$Informados['CARP_DESC'];
	     return $capertas;
	}
}
