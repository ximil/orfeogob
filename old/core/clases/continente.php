<?php 

/** 
 * continente es la clase encargada de gestionar las operaciones continente
 * @author Hardy Deimont Niño  Velasquez
 * @name continente
 * @version	1.0
 */ 
if (! $ruta_raiz)
	$ruta_raiz = '../..';
include_once "$ruta_raiz/core/modelo/modeloContinente.php";
	//==============================================================================================	
	// CLASS continente
	//==============================================================================================	
	
	/**
	 *  Objecto continente. 
	 */ 

class continente {
private $modelo;
	/**
   * Variable que se corresponde con su par, uno de los campos de la tabla sgd_def_continetes
   * @var numeric
   * @access public
   */
	private $idcont;
 /**
   * Variable que se corresponde con su par, uno de los campos de la tabla sgd_def_continete
   * @var string
   * @access public
   */
	private $name;


	
	function __construct($ruta_raiz) {
		$this->modelo = new modeloContinente ( $ruta_raiz );
	//TODO - Insert your code here
	}
	
	/**
	 * Funci
	 */
	/*function consultar() {
		;
	}*/
	
    function consultarTodo() {
		return $this->modelo->consultarTodo();
	}
    /*function crear() {
		;
	}
    function modificar() {
		;
	}*/
	
	/**
	 * 
	 */
	function __destruct() {
		$this->idcont=NULL;
		$this->name=NULL;		
		

	}
	/**
	 * @return the $idcont
	 */
	public function getIdcont() {
		return $this->idcont;
	}

	/**
	 * @return the $name
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @param $idcont the $idcont to set
	 */
	public function setIdcont($idcont) {
		$this->idcont = $idcont;
	}

	/**
	 * @param $name the $name to set
	 */
	public function setName($name) {
		$this->name = $name;
	}

	
	
}

/** 
 * pais es la clase encargada de gestionar las operaciones pais
 * @author Hardy Deimont Niño  Velasquez
 * @name pais
 * @version	1.0
 */ 

	//==============================================================================================	
	// CLASS pais
	//==============================================================================================	
	
	/**
	 *  Objecto pais. 
	 */ 


class pais {
 /**
   * @var numeric
   * @access public
   */
	private $idcont;
	 /**
   * @var numeric
   * @access public
   */
	private $idpais;
 /**
   * @var string
   * @access public
   */
	private $name;
	
	private $modelo;


	
	/**
	 * @return the $idpais
	 */
	public function getIdpais() {
		return $this->idpais;
	}

	/**
	 * @param $idpais the $idpais to set
	 */
	public function setIdpais($idpais) {
		$this->idpais = $idpais;
	}

	function __construct($ruta_raiz) {
		$this->modelo = new modeloPais ( $ruta_raiz );
	//TODO - Insert your code here
	}
	
	/**
	 * Funci
	 */
	function consultar() {
		 $rs=$this->modelo->consultar($this->idpais);
		 	$this->idcont=$rs['id_cont'];
		 	$this->name=$rs['nombre'];
	}
	
    function consultarTodo($idcontinente) {
		return $this->modelo->consultarTodo($idcontinente);
	}
    /*function crear() {
		;
	}
    function modificar() {
		;
	}*/
	
	/**
	 * 
	 */
	function __destruct() {
		$this->idcont=NULL;
		$this->name=NULL;		
		

	}
	/**
	 * @return the $idcont
	 */
	public function getIdcont() {
		return $this->idcont;
	}

	/**
	 * @return the $name
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @param $idcont the $idcont to set
	 */
	public function setIdcont($idcont) {
		$this->idcont = $idcont;
	}

	/**
	 * @param $name the $name to set
	 */
	public function setName($name) {
		$this->name = $name;
	}

	
	
}
?>