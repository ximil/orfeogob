<?php /** 
 * modulos es la clase encargada de gestionar las operaciones y los datos basicos referentes a una modulos
 * @author Hardy Deimont Niño  Velasquez
 * @name modulos
 * @version	1.0
 */
if (! $ruta_raiz)
$ruta_raiz = '../..';
include_once "$ruta_raiz/core/modelo/modeloModulos.php";
class modulos {
	 public $ruta_raiz;
	 public $id;  
	 public $nombre;  
	 public $code;  
	 public $valmax;  
	 public $path;  
	 public $deta;  
	 public $titulo;
	 public $estado;
	
	function __construct($ruta_raiz) {
		$this->ruta_raiz = $ruta_raiz;
		$this->modelo = new modeloDependencia ( $this->ruta_raiz );
	
	}
	
	/**
	 * consulta un dependencia
	 */
	function consultar() {
		return $this->modelo->consultar (  );
	
	}
	/**
	 * consulta varios dependecias
	 * @param $e estado
	 */
	
	function conusultarTodo($e) {
		return $this->modelo->consultarTodo ($e);
	
	}
	
	/**
	 * Crear una depdendencia
	 */
	function crear($usua_doc) {
		return $this->modelo->crear ($this->depe_codi, $this->depe_nomb, $this->dpto_codi, $this->depe_codi_padre, $this->muni_codi, $this->depe_codi_territorial, $this->dep_sigla, $this->dep_central, $this->dep_direccion, $this->depe_num_interna, $this->depe_num_resolucion, $this->id_cont, $this->id_pais, $this->depe_estado, $this->depe_rad_tp1, $this->depe_rad_tp2, $this->depe_rad_tp3, $this->depe_rad_tp4, $this->depe_rad_tp5, $this->depe_rad_tp6, $this->depe_rad_tp7, $this->depe_rad_tp8, $this->depe_rad_tp9 );
	
	}
	/**
	 * Modifica una dependecia
	 */
	function modificar($usua_doc) {
		$this->HistoricoConsultarActual();
		$res=$this->modelo->actualizar ( $this->id,  $this->nombre, $this->code,  $this->valmax, $this->path, $this->deta,  $this->titulo,  $this->estado );
		$this->HistoricoModificacion($usua_doc);
		return $res; 
	}

	/**
	 * Consulta el  historico  anterior
	 */
	function HistoricoConsulta() {
		//return $this->modelo->consultarHistorico ($this->depe_codi);
	}
	
	/**
	 * Consulta el  historico  anterior
	 */
	function HistoricoConsultarActual() {
		$hist=new modulos();
		$this->depehistAnt=$hist->consultar ( $this->depe_codi );
	}
	/**
	 * crear el  historico de creacion
	 */
	function HistoricoCreacion($usua_doc) {
		//$hist=new modulos();
		//$this->depehistAnt=$hist->consultar ( $this->depe_codi );
	}
	/**
	 * crear el  historico de modificacion
	 */
	function HistoricoModificacion($usua_doc) {
		/*$hist=new modulos();
		$this->depehistAnt=$hist->consultar ( $this->depe_codi );*/
	}
	
	/**
	 * @return the $id
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return the $nombre
	 */
	public function getNombre() {
		return $this->nombre;
	}

	/**
	 * @return the $code
	 */
	public function getCode() {
		return $this->code;
	}

	/**
	 * @return the $valmax
	 */
	public function getValmax() {
		return $this->valmax;
	}

	/**
	 * @return the $path
	 */
	public function getPath() {
		return $this->path;
	}

	/**
	 * @return the $deta
	 */
	public function getDeta() {
		return $this->deta;
	}

	/**
	 * @return the $titulo
	 */
	public function getTitulo() {
		return $this->titulo;
	}

	/**
	 * @return the $estado
	 */
	public function getEstado() {
		return $this->estado;
	}

	/**
	 * @param $id the $id to set
	 */
	public function setId($id) {
		$this->id = $id;
	}

	/**
	 * @param $nombre the $nombre to set
	 */
	public function setNombre($nombre) {
		$this->nombre = $nombre;
	}

	/**
	 * @param $code the $code to set
	 */
	public function setCode($code) {
		$this->code = $code;
	}

	/**
	 * @param $valmax the $valmax to set
	 */
	public function setValmax($valmax) {
		$this->valmax = $valmax;
	}

	/**
	 * @param $path the $path to set
	 */
	public function setPath($path) {
		$this->path = $path;
	}

	/**
	 * @param $deta the $deta to set
	 */
	public function setDeta($deta) {
		$this->deta = $deta;
	}

	/**
	 * @param $titulo the $titulo to set
	 */
	public function setTitulo($titulo) {
		$this->titulo = $titulo;
	}

	/**
	 * @param $estado the $estado to set
	 */
	public function setEstado($estado) {
		$this->estado = $estado;
	}

	function __destruct() {
	
	}
}

?>