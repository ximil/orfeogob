<?php 
/** 
 * sesion Orfeo es la clase encargada de gestionar las operaciones y  datosd pertinentes a la session
 * @author Hardy Deimont Niño  Velasquez
 * @name sessionOrfeo
 * @version	1.0
 */ 
if (! $ruta_raiz)
	$ruta_raiz = '../..';
include_once "$ruta_raiz/core/modelo/modeloSessionOrfeo.php";
	//==============================================================================================	
	// CLASS sessionOrfeo
	//==============================================================================================	
	
	/**
	 *  Objecto sessionOrfeo. 
	 */ 


class sessionOrfeo {
	private $tpNumRad= array ();
	private $tpDescRad= array ();
	private $tpImgRad= array ();
	private $tip3Nombre= array ();
	private $tip3desc= array ();
	private $tip3img= array ();
	private $permisos=array ();
	private $depecodi;
	private $rol;
	private $modelo;
	
	

	/**
	 * @param $rol the $rol to set
	 */
	public function setRol($rol) {
		$this->rol = $rol;
	}

		/**
	 * @return the $permisos
	 */
	public function getPermisos() {
		return $this->permisos;
	}
	
	/**
	 * @return the $tpNumRad
	 */
		public function getTpNumRad() {
		return $this->tpNumRad;
	}

	/**
	 * @return the $tpDescRad
	 */
	public function getTpDescRad() {
		return $this->tpDescRad;
	}

	/**
	 * @return the $tpImgRad
	 */
	public function getTpImgRad() {
		return $this->tpImgRad;
	}

	/**
	 * @return the $tip3Nombre
	 */
	public function getTip3Nombre() {
		return $this->tip3Nombre;
	}

	/**
	 * @return the $tip3desc
	 */
	public function getTip3desc() {
		return $this->tip3desc;
	}

	/**
	 * @return the $tip3img
	 */
	public function getTip3img() {
		return $this->tip3img;
	}

	/**
	 * @param $depecodi the $depecodi to set
	 */
	public function setDepecodi($depecodi) {
		$this->depecodi = $depecodi;
	}

		/**
	 * Funcion constructora
	 * @param $db
	 */
	function __construct($ruta_raiz) {
		$this->modelo = new modeloSessionOrfeo ( $ruta_raiz );
	}

		/**
	 * Funcion traer datos  trae los  datos de radicacion.
	 * 
	 */
	
	
	function traerDatos() {
		
		$resultado=$this->modelo->InfoContenido($this->depecodi);
			//print_r($resultado);
		$this->tpNumRad=$resultado['tpNumRad'];
	 	$this->tpDescRad=$resultado['tpDescRad'];
	 	$this->tpImgRad=$resultado['tpImgRad'];
		$this->tip3Nombre=$resultado['tip3Nombre'];
	 	$this->tip3desc=$resultado['tip3desc'];
	 	$this->tip3img=$resultado['tip3img'];
	 	/*echo '<hr>';
	 	print_r($this->tpNumRad);
	 	echo '<hr>';*/
	}
		/**
	 * Funcion traer datos  trae los  datos de radicacion.
	 * 
	 */
	
	
	function traerPermisos() {
		
		$resultado=$this->modelo->InfoModules($this->depecodi,$this->rol);
	 	$this->permisos=$resultado;
	 	
	}
	
}