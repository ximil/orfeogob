<?php
/**
 * Informacion de datos de logeo 
 * document management system
 */
define('LOGIN','USUSARIO');
define('PASSWD','CONTRASE&Ntilde;A');
define('NEWS','Noticias');
define('TITLE','ORFEO, M&oacute;dulo de validaci&oacute;n');
define('ACCESS','No se puede acceder a su cuenta?');
define('INFO','INFORMACI&Oacute;N');
define('PC','Ip del Equipo  :');
define('ERRORLOGEO','Debe digitar usuario y contrase\xf1a');
define('BOTON','INGRESAR');
define('MENU1','Carpetas');
define('MENU2','Radicación');
define('MENU3','Documento');
define('MENU4','Archivo');
define('MENU5','Envios');
define('MENU6','Trd');
define('MENU7','Opciones');
define('MENU8','Administrar');
define('AYUDA','Ayuda');
define('CERRAR','Cerrar');