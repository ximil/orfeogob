<?php 
session_start(); date_default_timezone_set('America/Bogota'); date_default_timezone_set('America/Bogota'); 
foreach ($_GET as $key => $valor)  $$key = $valor;
foreach ($_POST as $key => $valor)  $$key = $valor;
$krd = $_SESSION["krd"];
$dependencia = $_SESSION["dependencia"];
$usua_doc = $_SESSION["usua_doc"];
$codusuario = $_SESSION["codusuario"];
$ruta_raiz = ".."; 

$ruta_raiz="../../../..";
include($ruta_raiz.'/core/vista/validadte.php');
//include_once $ruta_raiz.'/core/language/'.$_SESSION ["lang"].'/data.inc';
?>

<html>
<head>
<link rel="stylesheet" type="text/css" 	href="<?php echo $ruta_raiz?>/js/calendario/calendar.css">
<link rel="stylesheet" href="<?php echo $ruta_raiz?>/estilos/default/orfeo.css">
<script language="JavaScript" src="<?php echo $ruta_raiz?>/js/common.js"></script>
<script language="JavaScript" type="text/javascript"	src="<?php echo $ruta_raiz?>/js/calendario/calendar_eu.js"></script>
<script type="text/javascript">
function pasardatos(codigo,deta,fecha,fecha2){
	document.adm_serie.codserieI2.value = codigo;
	document.adm_serie.detaserie2.value = deta;
	document.adm_serie.fecha_busq.value = fecha;
	document.adm_serie.fecha_busq2.value = fecha2;
	vistaFormUnitid('busIns',2);
}
function modiSerie(div,action){
		var cod = document.getElementById('codserieI2').value;
		var deta = document.getElementById('detaserie2').value;
		var fecha1 = document.getElementById('fecha_busq').value;
		var fecha2 = document.getElementById('fecha_busq2').value;
		if(cod.length==0 || deta.length==0 || fecha1.length ==0 || fecha2.length==0){
				alert('Debe llenar los campos');
				return false;
			}
		var poststr = "action="+action+"&cod="+cod+"&deta="+deta+"&fecha1="+fecha1+"&fecha2="+fecha2; 
		partes('<?php  echo $ruta_raiz.'/core/Modulos/trd/vista/operSerie.php'; ?>',div,poststr,'');
		partes('<?php  echo $ruta_raiz.'/core/Modulos/trd/vista/operSerie.php'; ?>','listadoSERIE','accion=listado','');
}
function buscar(){
	var deta = document.getElementById('detaserie2').value;
	if( deta.length==0 ){
			alert('Debe llenar campo de descripcion');
			return false;
		}
	var poststr = "action=buscar&deta="+deta; 
	partes('<?php  echo $ruta_raiz.'/core/Modulos/trd/vista/operSerie.php'; ?>','listadoSERIE',poststr,'');
}
</script>
</head>
<body bgcolor="#FFFFFF">
<table class=borde_tab width='100%' cellspacing="5"><tr><td  align='center' class="titulos4">
  SERIES DOCUMENTALES
</td></tr></table>
<form method="post" action="<?php echo $encabezadol?>" name="adm_serie"> 
<center>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr><td height="5"></td></tr><tr><td width="5"></td>
    <td width="210" valign="top"><TABLE width="100%" height="424" cellspacing="5" class="borde_tab">
      <TR>
        <TD height="21"  class='titulos2'> C&oacute;digo</td>
        </TR><TR><TD valign="top" align="left" class='listado2'><input type=text id="codserieI2" name=codserieI2  class='tex_area' size=11 maxlength="2" ></td>
      </tr>
      <tr>
        <TD height="26" class='titulos2'> Descripci&oacute;n</td>
       </tr>
      <tr> <TD height="62" align="left" valign="top" class='listado2'><textarea name="detaserie2" rows="4"  class="tex_area" id="detaserie2"></textarea></td>
      </tr>
      <tr>
        <TD height="26" class='titulos2'>Fecha desde<br></td>
        </tr>
      <tr><TD  align="right" valign="top" class='listado2'>
       <input readonly="true" type="text" class="tex_area" name="fecha_busq"	id="fecha_busq" size="10" value="" /> 
       <script	language="javascript">
					var A_CALTPL ={'imgpath' : '<?php echo $ruta_raiz ?>/js/calendario/img/',
						'months' : ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
						'weekdays' : ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],'yearscroll': true,	'weekstart': 1,	'centyear'  : 70}
					new tcal ({'formname': 'adm_serie','controlname': 'fecha_busq'},A_CALTPL);
			</script>
        </TD>
      </TR>	
      <TR>
        <TD height="26" class='titulos2'> Fecha Hasta<br></td>
       </tr>
      <tr> <TD  align="right" valign="top" class='listado2'>
      <input readonly="true" type="text" class="tex_area" name="fecha_busq2"	id="fecha_busq2" size="10" value="" /> 
       <script	language="javascript">
					new tcal ({'formname': 'adm_serie','controlname': 'fecha_busq2'},A_CALTPL);
			</script>
        </td>
      </TR>
      <tr>
        <td height="26"  align="center" class='titulos2'><div id='busIns'>
            <input type="button" name=buscar_serie2 Value='Buscar' onclick='buscar();' class=botones >
            <input type="button" name=insertar_serie2 Value='Insertar' class=botones onClick="modiSerie('actu','crear')" >
            </div>
      </td>
		 </TR>
      <tr>
		        <td height="26"   align="center"  class='titulos2'>
            <input type="button" name=actua_serie2 Value='Modificar' class=botones onClick="modiSerie('actu','mod')" >
            <input type="reset"  name=aceptar2 class=botones id=aceptar onclick="vistaFormUnitid('busIns',1);" value='Cancelar'>
		</td>
      </tr>
      <tr><td valign="top" ><div id='actu' > </div></td></tr>
    </table></td><td width="5"></td>
    <td valign="top" ><div id='listadoSERIE' > </div></td>
	<td width="5"></td>
  </tr>
</table>


<script language="javascript">
partes('<?php  echo $ruta_raiz.'/core/Modulos/trd/vista/operSerie.php'; ?>','listadoSERIE','accion=listado','');
</script>

</form>
</body>
</html>