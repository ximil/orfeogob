<?php 
/** 
 * tipoRadicado es la clase encargada de gestionar las operaciones tipoRadicado 
 * @author Hardy Deimont Niño  Velasquez
 * @name tipoRadicado
 * @version	1.0
 */ 
if (! $ruta_raiz)
	$ruta_raiz = '../../../..';
    include_once "$ruta_raiz/core/Modulos/radicacion/modelo/modeloTpradi.php";
	//==============================================================================================	
	// CLASS tipoRadicado
	//==============================================================================================	
	
	/**
	 *  Objecto tipoRadicado 
	 */ 

class tipoRadicado  {

	private $modelo;
	private $trad_genradsal; //login de  usuario
	private $trad_descr; //descripcion
	private $trad_icono; //icono
	private $codigo; //codigo 

	
	function __construct($ruta_raiz) {

		
	    $this->modelo = new modeloTpradi( $ruta_raiz );
	}
	

	/**
	 * Consulta los datos de las tipoRadicado 
	 * @return   void
	 */
	function consultar() {
			$rs = $this->modelo->consultar (  );
			return $rs;
		
	}
	
/*	function buscar() {
		$combi = $this->modelo->Buscar( 0,$this->descripcion,1);
			if($combi  ["ERROR"]=='OK'){
				return $combi;
			}
		return 'No se encotro la Descripción';
		
	}

	/**
	 * crea usuario
	 */
/*	function crear() {
		$combi = $this->modelo->Buscar( $this->codigo,'',0);
		if($combi  ["ERROR"]!='OK'){
			$combi2 = $this->modelo->Buscar( 0,$this->descripcion,0);
			if($combi2  ["ERROR"]!='OK'){
		 	return $this->modelo->crear($this->codigo,$this->descripcion, $this->fechaInicio, $this->fechaFin);
			}
			else
			return 'Ya existe la Descripción';
		}
		else 
		return 'Ya existe el codigo';
	}
	/**
	 * actualiza  usuario
	 */
/*	function actualizar() {

			$combi2 = $this->modelo->Buscar( 0,$this->descripcion);
			if($combi2  ["ERROR"]!='OK'||$combi2  [0]["DESCRIP"]==$this->descripcion){
		 	return $this->modelo->actualizar($this->codigo,$this->descripcion, $this->fechaInicio, $this->fechaFin);
			}
			else
			return 'Ya existe la Descripción';
		
		
	}
 	
	/** 
	 * Limpia los atributos de la instancia referentes a la informacion del usuario
	 * @return   void
	 */
	function __destruct() {
			
	}
	
}

?>