<?php 
/** 
 * modeloCarpetas es la clase encargada de gestionar las operaciones y los datos basicos referentes a un carpetas
 * @author Hardy Deimont Niño  Velasquez
 * @name modeloCarpetas
 * @version	1.0
 */
if (! $ruta_raiz)
	$ruta_raiz = '../../';
	//include_once "$ruta_raiz/include/db/ConnectionHandler.php";
include_once "$ruta_raiz/core/clases/ConnectionHandler.php";

class modeloCarpetas {
	
	public $link;
	
	function __construct($ruta_raiz) {
		$db = new ConnectionHandler ( "$ruta_raiz" );
		$db->conn->SetFetchMode ( ADODB_FETCH_NUM );
		$db->conn->SetFetchMode ( ADODB_FETCH_ASSOC );
		$this->link = $db;
	}
	
	
	/**
	 * @return resultado de la operacion obtienw los  datos requerido para el  usuario  en carpetas.
	 *  
	 */
	function carpetas() {
		// Esta consulta selecciona las carpetas Basicas de DocuImage que son extraidas de la tabla Carp_Codi
  	    $query="select CARP_CODI,CARP_DESC from carpeta order by carp_codi ";
		$rs = $this->link->conn->Execute ( $query );
		$varQuery = $query;
		//include "$ruta_raiz/include/tx/ComentarioTx.php";
		$i = 1;
		$carpetas = array ();	
		while ( ! $rs->EOF ) {
			$carpetas[$i]['CARP_CODI'] = $rs->fields ["CARP_CODI"];
			$carpetas[$i]['CARP_DESC'] = $rs->fields ["CARP_DESC"];
			$i ++;
			$rs->MoveNext ();
		}
		
		return $carpetas;
	
	}
	/**
	 * @return resultado de la operacion obtienw los  datos requerido para el  usuario  en carpetas personales.
	 *  
	 */
	function carpetasper($rolcodi,$dependencia) {
		// Esta consulta selecciona las carpetas Basicas de DocuImage que son extraidas de la tabla Carp_Codi
  	    $query="select CODI_CARP,DESC_CARP,NOMB_CARP from carpeta_per 
  	    where usua_codi=$rolcodi and depe_codi=$dependencia order by codi_carp  ";
		$rs = $this->link->conn->Execute ( $query );
		//$varQuery = $query;
		//include "$ruta_raiz/include/tx/ComentarioTx.php";
		$i = 1;
		$carpetas = array ();	
		while ( ! $rs->EOF ) {
			$carpetas[$i]['CARP_CODI'] = $rs->fields ["CODI_CARP"];
			$carpetas[$i]['NOMB_CARP'] = $rs->fields ["NOMB_CARP"];
			$carpetas[$i]['DESC_CARP'] = $rs->fields ["DESC_CARP"];
			$i ++;
			$rs->MoveNext ();
		}
		
		return $carpetas;
	
	}
	/**
	 * 
	 */
	function carpetasCuenta($dependecia,$rolcodi,$usua_doc) {
		$query="select carp_desc CARP_DESC,count(carp_desc) CUENTA from carpeta c, radicado r where c.carp_codi<>11 and  
		r.carp_codi=c.carp_codi and  r.radi_depe_actu=$dependecia
						and r.radi_usua_actu=$rolcodi and r.carp_per=0 and r.radi_usua_doc_actu='$usua_doc' group by c.carp_desc";
		$rs = $this->link->conn->Execute ( $query );
		$i=0;
		while ( ! $rs->EOF ) {
			$modulo[$i]["CARP_DESC"] = $rs->fields ["CARP_DESC"];
			$modulo[$i]["CUENTA"] = $rs->fields ["CUENTA"];
			$rs->MoveNext ();
			$i++;
		}
 			return $modulo;
	}
	function Agendado($usua_doc) {
	$sqlFechaHoy=$this->link->conn->DBTimeStamp(time());
	//$db->conn->debug = true;
	$sqlAgendado=" and (agen.SGD_AGEN_FECHPLAZO >= ".$sqlFechaHoy.")";
	$data="Agendados no vencidos";
    $query="select count(*) as CONTADOR from SGD_AGEN_AGENDADOS agen
	where  usua_doc='$usua_doc' and agen.SGD_AGEN_ACTIVO=1 $sqlAgendado";
		$rs = $this->link->conn->Execute ( $query );
			$modulo["CARP_DESC"] = 'Agendado';
			$modulo["CUENTA"] = $rs->fields ["CONTADOR"];
 			return $modulo;
	}	
	
function AgenVencido($usua_doc) {
	
		$sqlFechaHoy=$this->link->conn->DBTimeStamp(time());
	//$db->conn->debug = true;
	$sqlAgendado=" and (agen.SGD_AGEN_FECHPLAZO <= ".$sqlFechaHoy.")";
	$data="Agendados no vencidos";
    $query="select count(*) as CONTADOR from SGD_AGEN_AGENDADOS agen
	where  usua_doc='$usua_doc' and agen.SGD_AGEN_ACTIVO=1 $sqlAgendado";
		$rs = $this->link->conn->Execute ( $query );
			$modulo["CARP_DESC"] = 'Agen. Vencido';
			$modulo["CUENTA"] = $rs->fields ["CONTADOR"];
 			return $modulo;
	}
	
function informados($dependecia,$rolcodi) {
		$query="select count(*) as CONTADOR from informados where depe_codi=$dependencia and usua_codi=$rolcodi ";
		$rs = $this->link->conn->Execute ( $query );
			$modulo["CARP_DESC"] = 'Informados';
			$modulo["CUENTA"] = $rs->fields ["CONTADOR"]+0;
 			return $modulo;
		}
		
function VistoBueno($dependecia,$rolcodi,$login) {
			
if($rolcodi ==1)
			{
				$query="select count(1) as CONTADOR from radicado
					where carp_per=0 and carp_codi=11
					and  radi_depe_actu=$dependencia
					and radi_usua_actu=$rolcodi
					";
			}
			else
			{
				$query="select count(1) as CONTADOR from radicado
					where carp_per=0
						and carp_codi=11
						and radi_depe_actu=$dependencia
						and radi_usu_ante='$krd'
						";
			}
		    $rs = $this->link->conn->Execute ( $query );
			$modulo["CODI"] = 11;
			$modulo["CUENTA"] = $rs->fields ["CONTADOR"]+0;
 			return $modulo;
}
}

?>