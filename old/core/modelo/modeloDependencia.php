<?php 
/** 
 * modeloDependencia es la clase encargada de gestionar las operaciones y los datos basicos referentes a un dependencia
 * @author Hardy Deimont Niño  Velasquez
 * @name modeloDependencia
 * @version	1.0
 */
if (! $ruta_raiz)
	$ruta_raiz = '../../';
	//include_once "$ruta_raiz/include/db/ConnectionHandler.php";
include_once "$ruta_raiz/core/clases/ConnectionHandler.php";

class modeloDependencia {
	
	public $link;
	
	function __construct($ruta_raiz) {
		$db = new ConnectionHandler ( "$ruta_raiz" );
		$db->conn->SetFetchMode ( ADODB_FETCH_NUM );
		$db->conn->SetFetchMode ( ADODB_FETCH_ASSOC );
		$this->link = $db;
	}
	
	/**
	 * @return consulta los datos de rol.
	 * @param $idrol
	 */
	function consultar($codigo) {
		$q = "select * from dependencia where depe_codi =$codigo";
		$rs = $this->link->query ( $q );
		$retorno = array ();
		
		if (! $rs->EOF) {
			$retorno ['depe_nomb'] = $rs->fields ['DEPE_NOMB'];
			$retorno ['dep_sigla'] = $rs->fields ['DEP_SIGLA'];
			$retorno ['cont_codi'] = $rs->fields ['ID_CONT'];
			$retorno ['pais_codi'] = $rs->fields ['ID_PAIS'];
			$retorno ['dpto_codi'] = $rs->fields ['DPTO_CODI'];
			$retorno ['muni_codi'] = $rs->fields ['MUNI_CODI'];
			$retorno ['dep_sigla'] = $rs->fields ['DEP_SIGLA'];
			$retorno ['dep_direccion'] = $rs->fields ['DEP_DIRECCION'];
			$retorno ['dep_central'] = $rs->fields ['DEP_CENTRAL'];
			$retorno ['depe_codi'] = $rs->fields ['DEPE_CODI'];
			$retorno ['depe_codi_padre'] = $rs->fields ['DEPE_CODI_PADRE'];
			$retorno ['depe_codi_territorial'] = $rs->fields ['DEPE_CODI_TERRITORIAL'];
			$retorno ['depe_central'] = $rs->fields ['DEPE_CETRAL'];
			$retorno ['depe_num_interna'] = $rs->fields ['DEPE_NUM_INTERNA'];
			$retorno ['depe_num_resolucion'] = $rs->fields ['DEPE_NUM_RESOLUCION'];
			$retorno ['depe_rad_tp1'] = $rs->fields ['DEPE_RAD_TP1'];
			$retorno ['depe_rad_tp2'] = $rs->fields ['DEPE_RAD_TP2'];
			$retorno ['depe_rad_tp3'] = $rs->fields ['DEPE_RAD_TP3'];
			$retorno ['depe_rad_tp8'] = $rs->fields ['DEPE_RAD_TP8'];
			$retorno ['depe_estado'] = $rs->fields ['DEPE_ESTADO'];
		
		} else {
			$retono ['Error'] = 'No se contienen datos';
		
		}
		
		return ($retorno);
	}
	/**
	 * @return consulta los datos de las dependencias.
	 */
	function consultarTodo() {
		$q = "select * from dependencia "; 
		//where depe_codi =$codigo
		$rs = $this->link->query ( $q );
		$retorno = array ();
		if (! $rs->EOF) {
			
			$i = 0;
			while ( ! $rs->EOF ) {
				$retorno [$i] ['depe_nomb'] = $rs->fields ['DEPE_NOMB'];
				$retorno [$i] ['dep_sigla'] = $rs->fields ['DEP_SIGLA'];
				$retorno [$i] ['cont_codi'] = $rs->fields ['ID_CONT'];
				$retorno [$i] ['pais_codi'] = $rs->fields ['ID_PAIS'];
				$retorno [$i] ['dpto_codi'] = $rs->fields ['DPTO_CODI'];
				$retorno [$i] ['muni_codi'] = $rs->fields ['MUNI_CODI'];
				$retorno [$i] ['dep_sigla'] = $rs->fields ['DEP_SIGLA'];
				$retorno [$i] ['dep_direccion'] = $rs->fields ['DEP_DIRECCION'];
				$retorno [$i] ['dep_central'] = $rs->fields ['DEP_CENTRAL'];
				$retorno [$i] ['depe_codi'] = $rs->fields ['DEPE_CODI'];
				$retorno [$i] ['depe_codi_padre'] = $rs->fields ['DEPE_CODI_PADRE'];
				$retorno [$i] ['depe_codi_territorial'] = $rs->fields ['DEPE_CODI_TERRITORIAL'];
				$retorno [$i] ['depe_central'] = $rs->fields ['DEPE_CETRAL'];
				$retorno [$i] ['depe_num_interna'] = $rs->fields ['DEPE_NUM_INTERNA'];
				$retorno [$i] ['depe_num_resolucion'] = $rs->fields ['DEPE_NUM_RESOLUCION'];
				$retorno [$i] ['depe_rad_tp1'] = $rs->fields ['DEPE_RAD_TP1'];
				$retorno [$i] ['depe_rad_tp2'] = $rs->fields ['DEPE_RAD_TP2'];
				$retorno [$i] ['depe_rad_tp3'] = $rs->fields ['DEPE_RAD_TP3'];
				$retorno [$i] ['depe_rad_tp8'] = $rs->fields ['DEPE_RAD_TP8'];
				$retorno [$i] ['depe_estado'] = $rs->fields ['DEPE_ESTADO'];
				$i ++;
				$rs->MoveNext ();
			}
		} else {
			$retono ['Error'] = 'No se contienen datos';
		
		}
		return $retorno;
	}
	/**
	 * @return crea dependencia.
	 */
	function crear($depe_codi, $depe_nomb, $dpto_codi, $depe_codi_padre, $muni_codi, $depe_codi_territorial, $dep_sigla, $dep_central, $dep_direccion, $depe_num_interna, $depe_num_resolucion, $id_cont, $id_pais, $depe_estado, $depe_rad_tp1, $depe_rad_tp2, $depe_rad_tp3, $depe_rad_tp4, $depe_rad_tp5, $depe_rad_tp6, $depe_rad_tp7, $depe_rad_tp8, $depe_rad_tp9) {
		$record ['DEPE_CODI'] = $depe_codi;
		$record ['DEPE_NOMB'] = $depe_nomb;
		$record ['DEP_SIGLA'] = $dep_sigla;
		$record ['DEPE_ESTADO'] = $depe_estado;
		$record ['ID_CONT'] = $id_cont;
		$record ['ID_PAIS'] = $id_pais;
		$record ['DPTO_CODI'] = $dpto_codi;
		$record ['MUNI_CODI'] = $muni_codi;
		($_POST ['Slc_dpadre'] > 0) ? $record ['DEPE_CODI_PADRE'] = $depe_codi_padre : $record ['DEPE_CODI_PADRE'] = 'null';
		$record ['DEPE_CODI_TERRITORIAL'] = $depe_codi_territorial;
		$record ['DEP_DIRECCION'] = $dep_direccion;
		$record ['DEP_DIRECCION'] = $dep_central;
		$record ['DEP_DIRECCION'] = $depe_num_interna;
		$record ['DEP_DIRECCION'] = $depe_num_resolucion;
		$record ['DEPE_RAD_TP1'] = $depe_rad_tp1;
		$record ['DEPE_RAD_TP2'] = $depe_rad_tp2;
		$record ['DEPE_RAD_TP3'] = $depe_rad_tp3;
		$record ['DEPE_RAD_TP4'] = $depe_rad_tp4;
		$record ['DEPE_RAD_TP5'] = $depe_rad_tp5;
		$record ['DEPE_RAD_TP6'] = $depe_rad_tp6;
		$record ['DEPE_RAD_TP7'] = $depe_rad_tp7;
		$record ['DEPE_RAD_TP8'] = $depe_rad_tp8;
		$record ['DEPE_RAD_TP9'] = $depe_rad_tp9;
		//$sql = $this->link->conn->GetInsertSQL('dependecia',$record,true,null);
		//$rs = $this->link->conn->Execute($sql);
		$rs = $this->link->conn->insert ( 'dependecia', $record );
		
		if (! $rs) {
			return 'ERROR';
		}
		return 'OK';
	}
	
	/**
	 * @return resultado de la operacion de actualizacion un Rol.
	 * @param $nombre
	 * @param $tipo
	 * @param $id
	 */
	function actualizar($depe_codi, $depe_nomb, $dpto_codi, $depe_codi_padre, $muni_codi, $depe_codi_territorial, $dep_sigla, $dep_central, $dep_direccion, $depe_num_interna, $depe_num_resolucion, $id_cont, $id_pais, $depe_estado, $depe_rad_tp1, $depe_rad_tp2, $depe_rad_tp3, $depe_rad_tp4, $depe_rad_tp5, $depe_rad_tp6, $depe_rad_tp7, $depe_rad_tp8, $depe_rad_tp9) {
		$recordWhere ['DEPE_CODI'] = $depe_codi;
		$record ['DEPE_NOMB'] = $depe_nomb;
		$record ['DEP_SIGLA'] = $dep_sigla;
		$record ['DEPE_ESTADO'] = $depe_estado;
		$record ['ID_CONT'] = $id_cont;
		$record ['ID_PAIS'] = $id_pais;
		$record ['DPTO_CODI'] = $dpto_codi;
		$record ['MUNI_CODI'] = $muni_codi;
		($_POST ['Slc_dpadre'] > 0) ? $record ['DEPE_CODI_PADRE'] = $depe_codi_padre : $record ['DEPE_CODI_PADRE'] = 'null';
		$record ['DEPE_CODI_TERRITORIAL'] = $depe_codi_territorial;
		$record ['DEP_DIRECCION'] = $dep_direccion;
		$record ['DEP_DIRECCION'] = $dep_central;
		$record ['DEP_DIRECCION'] = $depe_num_interna;
		$record ['DEP_DIRECCION'] = $depe_num_resolucion;
		$record ['DEPE_RAD_TP1'] = $depe_rad_tp1;
		$record ['DEPE_RAD_TP2'] = $depe_rad_tp2;
		$record ['DEPE_RAD_TP3'] = $depe_rad_tp3;
		$record ['DEPE_RAD_TP4'] = $depe_rad_tp4;
		$record ['DEPE_RAD_TP5'] = $depe_rad_tp5;
		$record ['DEPE_RAD_TP6'] = $depe_rad_tp6;
		$record ['DEPE_RAD_TP7'] = $depe_rad_tp7;
		$record ['DEPE_RAD_TP8'] = $depe_rad_tp8;
		$record ['DEPE_RAD_TP9'] = $depe_rad_tp9;
		//$sql = $this->link->conn->GetInsertSQL('dependecia',$record,true,null);
		//$rs = $this->link->conn->Execute($sql);
		$rs = $this->link->conn->update ( 'dependecia', $record, $recordWhere );
		if (! $rs) {
			return 'ERROR';
		}
		return 'OK';
	}
}
?>