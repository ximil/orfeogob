<?php 
/** 
 * modeloSessionOrfeo es la clase encargada de gestionar las operaciones y los datos basicos referentes a un session
 * @author Hardy Deimont Niño  Velasquez
 * @name modeloSessionOrfeo
 * @version	1.0
 */
if (! $ruta_raiz)
	$ruta_raiz = '../../';
	//include_once "$ruta_raiz/include/db/ConnectionHandler.php";
include_once "$ruta_raiz/core/clases/ConnectionHandler.php";

class modeloSessionOrfeo {
	
	public $link;
	
	function __construct($ruta_raiz) {
		$db = new ConnectionHandler ( "$ruta_raiz" );
		$db->conn->SetFetchMode ( ADODB_FETCH_NUM );
		$db->conn->SetFetchMode ( ADODB_FETCH_ASSOC );
		$this->link = $db;
	}
	
	
	/**
	 * @return resultado de la operacion obtienw los  datos requerido para el  usuario  en carpetas.
	 * @param $dependecia
	 * 
	 */
	function InfoContenido($dependecia) {
		$query = "select a.SGD_TRAD_CODIGO AS SGD_TRAD_CODIGO,	a.SGD_TRAD_DESCR AS SGD_TRAD_DESCR,		a.SGD_TRAD_ICONO AS SGD_TRAD_ICONO 
		from SGD_TRAD_TIPORAD a	order by a.SGD_TRAD_CODIGO";
		$rs = $this->link->conn->Execute ( $query );
		$varQuery = $query;
		$comentarioDev = ' Busca todos los tipos de Radicado Existentes ';
		//include "$ruta_raiz/include/tx/ComentarioTx.php";
		$iTpRad = 0;
		$queryTip3 = "";
		$tpNumRad = array ();
		$tpDescRad = array ();
		$tpImgRad = array ();
		while ( ! $rs->EOF ) {
			$numTp = $rs->fields ["SGD_TRAD_CODIGO"];
			$sqlCarpDep = "select SGD_CARP_DESCR from SGD_CARP_DESCRIPCION where SGD_CARP_DEPECODI = " . $dependencia . " and SGD_CARP_TIPORAD = $numTp";
			$rsCarpDesc = $this->link->conn->Execute ( $sqlCarpDep );
			$descripcionCarpeta = $rsCarpDesc->fields ["SGD_CARP_DESCR"];
			if ($descripcionCarpeta) {
				$descTp = $descripcionCarpeta;
			} else {
				$descTp = $rs->fields ["SGD_TRAD_DESCR"];
			}
			$imgTp = $rs->fields ["SGD_TRAD_ICONO"];
			$queryTRad .= ",a.USUA_PRAD_TP$numTp";
			$queryDepeRad .= ",b.DEPE_RAD_TP$numTp";
			$queryTip3 .= ",a.SGD_TPR_TP$numTp";
			$tpNumRad [$iTpRad] = $numTp;
			$tpDescRad [$iTpRad] = $descTp;
			$tpImgRad [$iTpRad] = $imgTp;
			$iTpRad ++;
			$rs->MoveNext ();
		}
		/**
		 * BUSQUEDA DE ICONOS Y NOMBRES PARA LOS TERCEROS (Remitentes/Destinarios) AL RADICAR
		 * @param	$tip3[][][]  Array  Contiene los tipos de radicacion existentes.  En la primera dimencion indica la posicion dependiendo del tipo de rad. (ej. salida -> 1, ...). En la segunda dimencion almacenara los datos de nombre del tipo de rad. inidicado, Para la tercera dimencion indicara la descripcion del tercero y en la cuarta dim. contiene el nombre del archio imagen del tipo de tercero.
		 */
		$query = "select
			a.SGD_DIR_TIPO,
			a.SGD_TIP3_CODIGO,
			a.SGD_TIP3_NOMBRE,
			a.SGD_TIP3_DESC,
			a.SGD_TIP3_IMGPESTANA
			$queryTip3
			from SGD_TIP3_TIPOTERCERO a";
		$rs = $this->link->conn->Execute ( $query );
		while ( ! $rs->EOF ) {
			$dirTipo = $rs->fields ["SGD_DIR_TIPO"];
			$nombTip3 = $rs->fields ["SGD_TIP3_NOMBRE"];
			$descTip3 = $rs->fields ["SGD_TIP3_DESC"];
			$imgTip3 = $rs->fields ["SGD_TIP3_IMGPESTANA"];
			
			for($iTp = 0; $iTp < $iTpRad; $iTp ++) {
				$numTp = $tpNumRad [$iTp];
				$campoTip3 = "SGD_TPR_TP$numTp";
				$numTpExiste = $rs->fields [$campoTip3];
				if ($numTpExiste >= 1) {
					$tip3Nombre [$dirTipo] [$numTp] = $nombTip3;
					$tip3desc [$dirTipo] [$numTp] = $descTip3;
					$tip3img [$dirTipo] [$numTp] = $imgTip3;
				}
			}
			$rs->MoveNext ();
		}
 			$resutado_total['tpNumRad']=$tpNumRad;
	 		$resutado_total['tpDescRad']=$tpDescRad;
	 		$resutado_total['tpImgRad']=$tpImgRad;
			$resutado_total['tip3Nombre']=$tip3Nombre;
	 		$resutado_total['tip3desc']=$tip3desc;
	 		$resutado_total['tip3img']=$tip3img;
		return $resutado_total;
	
	}
	
	/**
	 * 
	 */
	function InfoModules($dependecia,$rolcodi) {
		 	$query="select drm.sgd_drm_valor valor,sgd_mod_modulo nombre from sgd_drm_dep_mod_rol drm,sgd_mod_modules mod
					where drm.sgd_drm_depecodi =$dependecia and drm.sgd_drm_rolcodi=$rolcodi 
					and mod.sgd_mod_id=sgd_drm_modecodi";
		$rs = $this->link->conn->Execute ( $query );
		$i=0;
		while ( ! $rs->EOF ) {
			$modulo[$i]["modulo"] = $rs->fields ["NOMBRE"];
			$modulo[$i]["valor"] = $rs->fields ["VALOR"];
			$rs->MoveNext ();
			$i++;
		}
 			return $modulo;
	}
}

?>