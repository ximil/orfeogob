<?php 
/** 
 * modeloContinente es la clase encargada de gestionar las operaciones y los datos basicos referentes a un Continente
 * @author Hardy Deimont Niño  Velasquez
 * @name modeloDependencia
 * @version	1.0
 */
if (! $ruta_raiz)
	$ruta_raiz = '../../';
//include_once "$ruta_raiz/include/db/ConnectionHandler.php";
include_once "$ruta_raiz/core/clases/ConnectionHandler.php";

class modeloContinente {
	
	public $link;
	
	function __construct($ruta_raiz) {
		$db = new ConnectionHandler ( "$ruta_raiz" );
		$db->conn->SetFetchMode ( ADODB_FETCH_NUM );
		$db->conn->SetFetchMode ( ADODB_FETCH_ASSOC );
		$this->link = $db;
	}
	
	/**
	 * @return consulta los datos de Continente.
	 * @param $codigo
	 */
	function consultar($codigo) {
		$q = "select * from sgd_def_continetes where id_cont =$codigo";
		$rs = $this->cursor->query ( $q );
		$retorno = array ();
		
		if (! $rs->EOF) {
			$retorno ['id_cont'] = $rs->fields ['ID_CONT'];
			$retorno ['nombre_cont'] = $rs->fields ['NOMBRE_CONT'];
			
		} else {
			$retono ['Error'] = 'No se contienen datos';
		
		}
		
		return ($retorno);
	}
	/**
	 * @return consulta los datos de las Continente.
	 */
	function consultarTodo() {
		$q = "select * from sgd_def_continetes ";
		$rs = $this->cursor->query ( $q );
		$retorno = array ();
		if (! $rs->EOF) {
			
			$i = 0;
			while ( ! $rs->EOF ) {
				$retorno [$i] ['id_cont'] = $rs->fields ['ID_CONT'];
				$retorno [$i] ['nombre_cont'] = $rs->fields ['NOMBRE_CONT'];
				$i ++;
				$rs->MoveNext ();
			}
		} else {
			$retono ['Error'] = 'No se contienen datos';
		
		}
		return $retorno;
	}
	/**
	 * @return crea Continente.
	 */
	function crear($nomb_cont) {
		$q = "select max(ID_CONT) maximo from sgd_def_continetes ";
		$rs = $this->cursor->query ( $q );
		$cont_codi= $rs->fields ['MAXIMO'] +1;
			$record['ID_CONT'] = $cont_codi;
		    $record['NOMBRE_CONT'] = $nomb_cont;
			$rs = $this->link->conn->insert('sgd_def_continetes',$record);
		
		if (! $rs) {
			return 'ERROR';
		}
		return 'OK';
	}
	
	/**
	 * @return resultado de la operacion de actualizacion un Continente.
	 * @param $cont_codi
	 * @param $nomb_cont
	 */
	function actualizar($cont_codi,$nomb_cont) {
  		if($cont_codi!='' && $nomb_cont!=''){	
		$recordWhere['ID_CONT'] = $cont_codi;
		$record['NOMBRE_CONT'] = $nomb_cont;
			
		$rs = $this->link->conn->update('sgd_def_continetes',$record,$recordWhere);
		if (! $rs) {
			return 'ERROR';
		}
		return 'OK';
  		}
  			return 'ERROR';
	}
}


/** 
 * modeloPais es la clase encargada de gestionar las operaciones y los datos basicos referentes a un Pais
 * @author Hardy Deimont Niño  Velasquez
 * @name modeloDependencia
 * @version	1.0
 */

class modeloPais {
	
	public $link;
	
	function __construct($ruta_raiz) {
		$db = new ConnectionHandler ( "$ruta_raiz" );
		$db->conn->SetFetchMode ( ADODB_FETCH_NUM );
		$db->conn->SetFetchMode ( ADODB_FETCH_ASSOC );
		$this->link = $db;
	}
	
	/**
	 * @return consulta los datos de un pais.
	 * @param $codigo
	 */
	function consultar($codigo) {
		$q = "select * from sgd_def_paises where id_pais =$codigo";
		$rs = $this->cursor->query ( $q );
		$retorno = array ();
		
		if (! $rs->EOF) {
			$retorno ['id_pais'] = $rs->fields ['ID_PAIS'];
			$retorno ['id_cont'] = $rs->fields ['ID_CONT'];
			$retorno ['nombre'] = $rs->fields ['NOMBRE_PAIS'];
			
		} else {
			$retono ['Error'] = 'No se contienen datos';
		
		}
		
		return ($retorno);
	}
	/**
	 * @return consulta los datos de las paices de un continete.
	 */
	function consultarTodo($codigo) {
		$q = "select * from sgd_def_paises where id_pais =$codigo";
		$rs = $this->cursor->query ( $q );
		$retorno = array ();
		if (! $rs->EOF) {
			
			$i = 0;
			while ( ! $rs->EOF ) {
				$retorno [$i] ['id_cont'] = $rs->fields ['ID_CONT'];
				$retorno [$i] ['id_pais'] = $rs->fields ['ID_PAIS'];
				$retorno [$i] ['nombre'] = $rs->fields ['NOMBRE_PAIS'];
				$i ++;
				$rs->MoveNext ();
			}
		} else {
			$retono ['Error'] = 'No se contienen datos';
		
		}
		return $retorno;
	}
	/**
	 * @return crea pais.
	 */
	function crear($nomb_cont,$cont_codi,$ID_PAIS) {
			$record ['ID_PAIS'] = $ID_PAIS;
			$record['ID_CONT'] = $cont_codi;
		    $record['NOMBRE_CONT'] = $nomb_cont;
			$rs = $this->link->conn->insert('sgd_def_paises',$record);
		
		if (! $rs) {
			return 'ERROR';
		}
		return 'OK';
	}
	
	/**
	 * @return resultado de la operacion de actualizacion un paises.
	 * @param $cont_codi
	 * @param $nomb_cont
	 * @param $id_pais
	 */
	function actualizar($cont_codi,$nomb_cont,$id_pais) {
  		if($cont_codi!='' && $nomb_cont!=''){	
		$recordWhere['ID_CONT'] = $cont_codi;
		$recordWhere['ID_PAIS'] = $id_pais;
		$record ['ID_PAIS'] = $id_pais;
		$record['NOMBRE_CONT'] = $nomb_cont;
			
		$rs = $this->link->conn->update('sgd_def_paises',$record,$recordWhere);
		if (! $rs) {
			return 'ERROR';
		}
		return 'OK';
  		}
  			return 'ERROR';
	}
}
?>