<?php /** Orfeo 4.0 
 ** Archivo de configuracion del aplicativo
**/

/**
 * Datos Entidad
 **/
//Acronimo de la empresa
$entidad= "IDEAM";
//Nombre de la Empresa Largo, Variable usada generalmente para los t�tulos en informes.
$entidad_largo= 'Instituto de Hidrolog&iacute;a, Meteorolog&iacute;a y Estudios Ambientales';	
$entidad_largo_Planilla= 'Instituto de Hidrolog&iacute;a, Meteorolog&iacute;a y Estudios Ambientales';
//Telefono o PBX de la empresa.
$entidad_tel = 000000 ;
//Direccion de la Empresa.
$entidad_dir = "direccion";
// Pais Empresa o Entidad
$pais = "Colombia";
$pinguino="pinguino.png";
/**
 * Datos de estilos y logos de la entidad.
 */

//carpeta de logos
$entLogos= "IDEAM";
// Directorio de estilos a Usar... Si no se establece una Ruta el sistema usara el que posee por Defecto en el directorio estilos.  orfeo.css para usarlo cree una carpeta con su personalizacion y luego copie el archivo orfeo.css y cambie sus colores.
$ESTILOS_PATH = "estilos/ideam/";
$entidaImage="ideam";

$info="<b> $entidad_largo </b>"; 
$info2="Sede Central: Carrera 10 No. 20-30 Bogotá D.C. - PBX (571)3527160 / Bogotá D.C. <br> Sede Puente Aranda: Calle 12 No 42B - 44 / Bogotá D.C.";
$info3="Aeropuerto, Laboratorio y 11 Áreas Operativas";
/**
 * Configuracion de tipos remitente
 * 
 */
$numTipRem=3;
$tipRem[1]['titulo']='Remitente';
$tipRem[2]['titulo']='Tercero';
$tipRem[3]['titulo']='Entidad';


/**Tiempo de de recarga menu 60000 milisegundos  =  a 1 min **/
$TimeRecarga= '600000';

/**
 * Configuracion de dependecias
 */

//Codigo de la dependencia de salida por omision 999
$entidad_depsal = 999;	
//determina el  tamaño del numero de las dependecias.
$TAM_DEPE_CODI=3;
// Datos que se usan en el formulario web disponible a los usuarios
$depeRadicaFormularioWeb=700;  // Es radicado en la Dependencia 900
$usuaRecibeWeb=1 ; // Usuario que Recibe los Documentos Web
$secRadicaFormularioWeb=700;

//Servidor que procesa los documentos
$servProcDocs = "127.0.0.1:8000";

$MODULO_RADICACION_DOCS_ANEXOS=1;
/**
 * Configuracion LDAP
 */
//Nombre o IP del servidor de autenticacion LDAP
$ldapServer = 'entsvrred.caprecom.rloc';
//Cadena de busqueda en el servidor.
$cadenaBusqLDAP = '';
//Campo seleccionado (de las variables LDAP) para realizar la autenticacion.
$campoBusqLDAP = 'mail';
//Si esta variable va en 1 mostrara en informacion geneal el menu de Rel. Procedimental, resolucion, sector, causal y detalle. en cero Omite este menu
$menuAdicional = 0;

//Servidor Fax
define('IPFAX','172.15.1.207');
define('PORTFAX','65432');
//clave usuario  nuevo
define('USUANUEVO','OrfeoIdeam');
//define('USUANUEVO','1234');

// Variables que se usan para la radicacion del correo electronio 
// Sitio en el que encontramos la libreria pear instalada
$PEAR_PATH="/var/www/orfeo-3.8.0/pear/";
// Servidor de Acceso al correo Electronico
$servidor_mail="imap.admin.gov.co";
// Tipo de servidor de correo Usado
$protocolo_mail="imap"; // imap  | pop3
// Puerto del servidor de Mail.
$puerto_mail=143; //Segun servidor defecto 143 | 110
$enviarMailMovimientos = "1";
//mostra causal 1, no mostrar 0
$vista_causal=0;
//mostra sector 1, no mostrar 0
$vista_sector=0; 
//mostra tema 1, no mostrar 0
$vista_tema=0;  
//desahabilitar entidad 1 activo, 0 inactivo
$vista_entidad=0; //se toma como essp
//desahabilitar predio 1 activo, 0 inactivo
$vista_predio=1; //esta variablle se toma como entidad
//desahabilitar Insercion de empresas 1 activo, 0 inactivo
//$vista_addemp=1;
// Correo Contacto o Administrador del Sistema
$administrador = "sunombre@dominio.com";
//version Aplicacion
$version='3.9.2 MVC 0.1';
//Lenguaje por  omision
$language='es';

define(RUTA_BODEGA,'/mnt/bodega/');
?>