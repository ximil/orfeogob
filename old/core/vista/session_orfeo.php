<?php /*************************************************************************************
 * Orfeo 
 * Version 3.9.1 MVC 0.1
 *
/* Rediseño de orfeogpl en modelo  vista  controlador y  estandarizacionde codigo    */
/*	Hardy Deimont Niño Velasquez													 */
/*************************************************************************************/
//ruta raiz
//inicializacionde variables
if(!$ruta_raiz)
	$ruta_raiz='../..';
if (! $krd)
	$krd = $_REQUEST ["krd"];
if (! $drd)
	$drd = $_REQUEST ["drd"];
	//echo $drd;
	
//Incluir clases de usuario , roles, dependencia y   modulos.
include_once $ruta_raiz . '/core/clases/usuarioOrfeo.php';
include_once $ruta_raiz . '/core/clases/dependencia.php';
include_once $ruta_raiz . '/core/clases/roles.php';
include_once $ruta_raiz . '/core/clases/SessionOrfeo.php';
include_once $ruta_raiz . '/core/clases/ldapauth.class.php';
/*include_once $ruta_raiz.'/core/clases/modulos.php';
*/


//incializacion de  clases usuario
$usuario = new usuarioOrfeo ( $ruta_raiz );
$authu = 'Bad';
//inicializacion de la  clase
$usuario->setLogin ( strtoupper ( $krd ) );
///consulta los datos  de  usuario
$usuario->consultar_usuario ();
if($usuario->getEstado()==0 ){
	$authu = 'Bad';
	$usuario->getNombre();
	$mensajeError2 = "EL USUARIO ".$usuario->getNombre()." (".strtoupper ( $krd ).") SE ENCUENTRA INACTIVO";
	$mensajeError3 = "Por favor consulte con el administrador del  sistema";
}
// validacion si se autentica por ldap
elseif ($usuario->getLdap () == 1) {
    
$ldapX= new LDAPAuth($ruta_raiz,$krd, $drd);
//print_r($ldapX->Conection);
//echo  $ldapX->error();
//$ldapX->login=$;
//$ldapX->pass=;
$authu=$ldapX->autenticar($krd, $drd);
	if($authu!='ok')
            $mensajeError2=$authu;
} //autentica  por clave de  usuario
elseif ($usuario->getClave () == SUBSTR ( md5 ( $drd ), 1, 26 )) {
	//echo 'Clave igual';
	$authu = 'ok';
} else {
	$mensajeError2 = "USUARIO O CONTRASE&Ntilde;A INCORRECTOS";
}
//echo $usuario->getClave ()." == ".SUBSTR ( md5 ( $drd ), 1, 26 ).'****'.$authu;
//die();
if ($authu == 'ok') {
	$depe = new dependencia ( $ruta_raiz );
	//Validacion si  la dependecia es encuentra activa.
	$depe->depe_codi = $usuario->getDepe_codi ();
	$depedata = $depe->consultar ();
	
	if ($depedata ['depe_estado'] == 1) {
		//validacion si el  rol esta activo
		$rol = new roles ( $ruta_raiz );
		$rol->setId_rol ( $usuario->getRol() );
		$rol->consultar ();
		if ($rol->getEstado () == 1) {
			
			$sessionOrfeo = new sessionOrfeo ( $ruta_raiz );
			$sessionOrfeo->setDepecodi ( $usuario->getDepe_codi () );
			$sessionOrfeo->setRol ( $usuario->getRol() );
			$sessionOrfeo->traerDatos ();
			$sessionOrfeo->traerPermisos ();
			//print_r($sessionOrfeo);				
		 	$nombSession = uniqid ( "ORF", true );
			//$nombSession = date("ymdhis")."o".str_replace(".","",$_SERVER['REMOTE_ADDR'])."$krd";
			session_id ( $nombSession );
			//session_id ( "$krd" );
			session_start(); date_default_timezone_set('America/Bogota'); date_default_timezone_set('America/Bogota');
			//echo session_id (  );
			$_SESSION ['depe_codi_padre']=$depedata['depe_codi_padre'];
            $_SESSION ['depe_codi_territorial']=$depedata['depe_codi_territorial'];
			$_SESSION ["rol_nomb"] =$rol->getNombre_rol();
            $_SESSION ["id_rol"] =$usuario->getRol() ;
			$_SESSION ["dirOrfeo"] = $dirOrfeo;
			$_SESSION ["drde"] = $usuario->getClave ();
			$_SESSION ["usua_doc"] = $usuario->getUsua_doc ();
			$_SESSION ["dependencia"] = $usuario->getDepe_codi ();
			$_SESSION ["codusuario"] = $usuario->getUsua_codi ();
			$_SESSION ["depe_nomb"] = $depedata ['depe_nomb'];
			$_SESSION ["cod_local"] = $cod_local; //municipio
			$_SESSION ["depe_municipio"] = $depe_municipio;
			$_SESSION ["usua_email"] = $usuario->getEmail ();
			$_SESSION ["usua_at"] = $usuario->getAt ();
			$_SESSION ["usua_nomb"] = $usuario->getNombre ();
			$_SESSION ["usua_debug"] = $usuario->getUsua_debug ();
			$_SESSION ["usua_nacim"] = $usuario->getNacimiento ();
			$_SESSION ["usua_nuevo"] = $usuario->getUsua_nuevo ();
			$_SESSION ["krd"] = $usuario->getLogin ();
			$_SESSION ["drd"] = $usuario->getClave ();
			$_SESSION ["tpNumRad"] = $sessionOrfeo->getTpNumRad ();
			$_SESSION ["tpDescRad"] = $sessionOrfeo->getTpDescRad ();
			$_SESSION ["tpImgRad"] = $sessionOrfeo->getTpImgRad ();
			$_SESSION ["tip3Nombre"] = $sessionOrfeo->getTip3Nombre ();
			$_SESSION ["tip3desc"] = $sessionOrfeo->getTip3desc ();
			$_SESSION ["lang"]=$language;
			$_SESSION ["tip3img"] = $sessionOrfeo->getTip3img;
			$_SESSION ["ldap"]=$usuario->getLdap ();
			$_SESSION["ESTILOS_PATH"] = $ESTILOS_PATH;
            $_SESSION ["graphic"]=$usuario->getGraphic();
			$modulosP = $sessionOrfeo->getPermisos ();
			 while (list($clave, $valor) = each($_SESSION)){
			 	// echo "<p>El vector con indice $clave tiene el valor $valor </p>";
			 	$w.=$clave."=".$valor.";";
			 }
        
			for($index = 0; $index < count ( $modulosP ); $index ++) {
				$item=trim($modulosP [$index]['modulo']);
				switch ($item) {
					case 'usua_prad_tp1':
					case 'usua_prad_tp2':
					case 'usua_prad_tp3':
					case 'usua_prad_tp4':
					case 'usua_prad_tp5':
					case 'usua_prad_tp6':
					case 'usua_prad_tp7':
					case 'usua_prad_tp8':
					case 'usua_prad_tp9':
						$_SESSION [$item] = $modulosP [$index] ['valor'];		
						$strI=substr($item, -1, 1);
					$tpPerRad[$strI]=$modulosP [$index] ['valor'];
					$tpDepeRad[$strI]=$depedata['depe_rad_tp'.$strI];
					//echo  "<hr>".$modulosP [$index] ['modulo']." = ".$modulosP [$index] ['valor']."<hr>";
					$_SESSION ["tip3img"] = $sessionOrfeo->getTip3img;
						
						$w.=$item."=".$modulosP [$index] ['valor'].";";					
					break;
					default:
						$_SESSION [$item] = $modulosP [$index] ['valor'];
						//$w.=$item."=".$modulosP [$index] ['valor']."!";
						$w.=$item."=".$modulosP [$index] ['valor'].";";
				        break;
				}
			
			}		
		
			$_SESSION ["tpDepeRad"] = $tpDepeRad;
			$_SESSION ["tpPerRad"] = $tpPerRad;
			//configuraciones de estilos
			if($_SESSION['usua_masiva']==1){
				include $ruta_raiz . "/core/clases/carpetas.php";
				$carp=new carpetas($ruta_raiz);
				$carp->validateCarpMas($usuario->getRol(), $usuario->getDepe_codi (),$usuario->getUsua_codi());
			}
			setcookie("krd",'');  /* expira en una hora */
		    setcookie("ORF4",'');  /* expira en una hora */
		    setcookie("krd", $usuario->getLogin (), time()+3600*4); /* expira en en 4 horas */ 
			//setcookie("ORF4", $w, time()+3600*4);  /* expira en en 4 horas */
			setcookie("ORF4", $w, time()+60*5);  /* expira en en 5 min */
			//setcookie('prueba','hd');
          $usuario->sessionUpdate(session_id ( ));			
			
			$ValidacionKrd = "Si";
		} else {
			$mensajeError2 = "EL ROL SE ENCUENTRA INACTIVA ";
			$mensajeError3 = "Comuniquese con el area pertinente sobre este error.";
		}
	} else {
		$mensajeError2 = "LA DEPENDECIA ES ENCUENTRA INACTIVA ";
		$mensajeError3 = "Comuniquese con el area pertinente sobre este error.";
	}
}
if($usuario->getUsua_nuevo ()==0 && $usuario->getLdap () == 0 && $usuario->getClave () == SUBSTR ( md5 ( $drd ), 1, 26 )){
	include_once $ruta_raiz . '/core/vista/changepass.php';
}
/*
print_r($_SESSION);
echo "<hr>";
print_r($_COOKIE);
die();*/
?>