<?php /** 
 * OrfeoGPL
 * Es un Software Mantenido por la Fundacion sin Animo de Lucro Correlibre.org [http://correlibre.org]
 * Version 3.8.0 
 * Licencia GNU/GPL.
 *
 *
 * FORMULARIO DE LOGIN A ORFEO
 * Aqui se inicia session 
 * @PHPSESID		String	Guarda la session del usuario
 * @db 					Objeto  Objeto que guarda la conexion Abierta.
 * @iTpRad				int		Numero de tipos de Radicacion
 * @$tpNumRad	array 	Arreglo que almacena los numeros de tipos de radicacion Existentes
 * @$tpDescRad	array 	Arreglo que almacena la descripcion de tipos de radicacion Existentes
 * @$tpImgRad	array 	Arreglo que almacena los iconos de tipos de radicacion Existentes
 * @numRegs		int		Numero de registros de una consulta
 * 
 */
if(!$ruta_raiz)
 $ruta_raiz = "../..";
$krd = $_POST['krd'];
$drd = $_POST['drd'];
include_once $ruta_raiz.'/core/config/config.inc';
include_once $ruta_raiz.'/core/language/'.$language.'/data.inc';
if ($krd)
{
	include "$ruta_raiz/core/vista/session_orfeo.php";
	//echo  "$ruta_raiz";
	//die();
//	require_once("$ruta_raiz/class_control/Mensaje.php");
//echo 3;
	echo $usua_nuevo;
	if($usua_nuevo=='0')
	{
		
		include($ruta_raiz."/core/vista/contraxx.php");
		//die();
		$ValidacionKrd = "NOOOO";
		if($j==1) die("<center> -- </center>");
	}
	
}
?>
<html>
<head>
<title>.:: <?php  echo TITLE; ?> ::.</title>
<link href="<?php echo $ESTILOS_PATH ?>orfeo.css" rel="stylesheet" type="text/css">
<link rel="shortcut icon" href="./imagenes/ico.png">
<script language="JavaScript" type="text/JavaScript">
function loginTrue()
{
	document.formulario.submit();
}

function loginCorreLibre()
{	var band;
	if ((document.getElementById('krd').value != '') && (document.getElementById('drd').value!=''))
	{	band = true;	}
	else
	{	band = false;
		alert ('<?php echo ERRORLOGEO;?>');
	}
	return band;
}
</script>
<style type="text/css">
<!--
.style10 {font-size: 12px}
.style11 {
	color: #FF0000;
	font-weight: bold;
	font-size: 14px;
}
-->
</style>
</head>
<style type="text/css">
</style>
<body  onLoad='document.getElementById("krd").focus();'    class="bordeInicial">	
<form name=formulario action='contenido.php'  method='post' >
	<input type="hidden" name="orno" value="1">
<?php echo $ValidacionKrd;
if($ValidacionKrd=="Si")	
{  
?>
<script>
loginTrue();
</script>
<?php }
?>
<input type="hidden" name="ornot" value="1">
</form>
<form action="index.php" method="post" onSubmit="return loginCorreLibre();" name="form33">
<table width="100%"  border="0" valign="top" cellpadding="0" cellspacing="0" >
<tr align="center">
  <td height="20"></td>
  <td  valign="top" ></td>
  <td></td>
</tr>
<tr align="center">
	<td  width="10%"></td>
	<td  valign="top" >
	<table width="100%"   border="0" cellpadding="0" cellspacing="0" ><tr><td  align="center" valign="top" >
	 <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" >
      <tr align="center" valign="top">
        <td><div align="left"><img src="imagenes/login/logo_orfeo_grande.gif" /></div></td>
		<td width="10"></td>
		   <td width="100"><table width="100%" border="0" align="right" cellpadding="0" cellspacing="5" class="tablasLogin">
             <tr>
               <td align="center" colspan="2"><img src="imagenes/logos/logoEmpresa2.png" alt="Logo  entidad"></td>
             </tr>
             <tr>
               <td align="center"><div align="right" class="menu_princ"><?php  echo LOGIN; ?></div></td>
               <td align="center"><font size="3" face="Arial, Helvetica, sans-serif">
                 <input type='text' id='krd' name='krd' size='20' class='tex_area'>
               </font></td>
             </tr>
             <tr align="left">
               <td width="50%" align="center"><div align="right" class="menu_princ"><?php  echo PASSWD; ?></div></td>
               <td width="50%" align="center" ><b><font size="3" face="Arial, Helvetica, sans-serif">
                 <input type='password' id='drd' name='drd' size='20' class='tex_area'>
               </font></b> </td>
             </tr>
             <tr class="trLogin">
               <td colspan="2"  align="center"><p>
                   <input type=hidden name=tipo_carp value=0>
                   <input type=hidden name=carpeta value=0>
                   <input type=hidden name=order value='radi_nume_radi'>
                   <input name="Submit" type="submit" class="botones" value="<?php echo BOTON;?>">
                 </p>
                   <p class="vinculos"><?php  echo ACCESS; ?> </p></td>
             </tr>
           </table></td>
      </tr>
	  <tr>   <td height="2"></td> 
	  <td></td> <td></td>   </tr>
      <tr>
        <td align="center" rowspan="3"><table width="100%" height="100%" border="0" cellpadding="0" class="tablasLogin">
          <tr>
            <td align="center"><?php  if($mensajeError2) {?>
                <p class="titulosError2"><?php  echo $mensajeError2;?></p>
              <?php  } else{?>
                <p><span class="no_leidos"><?php  echo NEWS; ?> </span><font face="Arial Narrow"  size="2"><br>
                    </span></p>
              <?php  } ?></td>
          </tr>
        </table></td>		<td width="10"></td>
		 <td align="center"><table width="100%" border="0" class="tablasLogin">
           <tr>
             <td colspan="2" align="center"><span class="menu_princ style10">Republica de Colombia </span>
                 <p><img src="imagenes/escudo.png" alt="Escudo de colombia"/></p></td>
           </tr>
         </table></td>
      </tr>
	  	  <tr>    <td></td> <td height="2"></td>   </tr>
      <tr>
        <td  align="center" valign="top">&nbsp;</td><td valign="top"><table width="100%" border="0" class="tablasLogin">
          <tr>
            <td colspan="2" align="center"><span class="menu_princ style10"><?php  echo INFO; ?></span>
                <p><span class="no_leidos"><?php  echo $entidad_largo ?> </span><font face="Arial Narrow"  size="2"><br>
                      <span class="leidos2"><?php  echo PC; ?>
                        <?php echo $_SERVER['REMOTE_ADDR']?>
                  </span></font><span class="leidos2"></span> </span></p></td>
          </tr>
        </table></td>
      </tr>
    </table>
	</td>
	    
	</tr>
	</table>	</td>
		<td  width="10%"></td>
		</tr>
	<tr align="center">
  <td height="20"></td>
  <td  valign="top" ></td>
  <td></td>
</tr>
<tr align="center">
 <td height="20" ></td>
  <td colspan="1">
      <table width="100%" border="0" cellpadding="0" class="tablasLogin">
      <tr>
	 
        <td align="center">
            <p><span class="no_leidos">Version <?php echo $version; ?> </span><font face="Arial Narrow"  size="2"><br>
                </span></p></td>
				
      </tr>
    </table></td>
	 <td></td>
</tr>
</table>
</form>
</body>
</html>