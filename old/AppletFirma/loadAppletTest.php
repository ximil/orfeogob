<html>
<head>
<title>Ideas En Acción Crytpo Applet Signer Test HTML</title>
</head>

<body>
<?php chdir("../");
$bodega = getcwd();
$bodegaFirma = "/mnt/bodega/tmpfirma/";
$archivo = trim($_GET['file'],'.');
$pos = strrpos($archivo, "/");
$archivo = substr($archivo, $pos + 1 , strlen($archivo)-1);
if(isset($_GET['perm'])){
    $perm = $_GET['perm'];
}
else{
    $perm = 0;
}
if(isset($_GET['tsa'])){
    $tsa = $_GET['tsa'];
}
else{
    $tsa=0;
}
?>
<applet code="co.com.iea.ui.CryptoAppletUI.class" 
	archive="AppletFirma/CryptoAppletClient.jar,
			AppletFirma/commons-logging.jar,
			AppletFirma/bcmail-jdk15on-151.jar,
			AppletFirma/bcpkix-jdk15on-151.jar,
			AppletFirma/bcprov-jdk15on-151.jar,
			AppletFirma/itextpdf-5.5.3.jar,
			AppletFirma/log4j-api-2.2.jar,
			AppletFirma/log4j-core-2.2.jar,
			AppletFirma/jarClient.jar,
			AppletFirma/jaxrpc.jar,
			AppletFirma/commons-discovery-0.2.jar,
			AppletFirma/axis.jar,
			AppletFirma/wsdl4j.jar" width=470 Height=440>
	 <param name="location" value="Bogota">
	 <param name="reason" value="Firma de Documentos desde Orfeo">
	 <param name="callingURL" value="http://<?php echo $_SERVER['SERVER_NAME']?>/orfeo/core/Modulos/radicacion/vista/respuestaFirma.php">
	 <param name="callingURLType" value="_self">
	 <param name="perm_firma_digital" value="<?php echo $perm?>">
	 <param name="tsa" value="<?php echo $tsa?>">
	 <?php 	 echo "<param name='sourcePath' value='".$bodega. trim($_GET['file'],'.')."'> ";
	 echo "<param name='targetPath' value='". $bodegaFirma . $archivo ."'> ";
	 ?>
	 <param name="user" value="<?php echo $_GET['user']?>">
	 <param name="codRadi" value="<?php echo $_GET['codAnexo']?>">
</applet>
</body>
</html>
