<?php session_start(); date_default_timezone_set('America/Bogota'); date_default_timezone_set('America/Bogota');

/* * ********************************************************************************** */
/* ORFEO GPL:Sistema de Gestion Documental		http://www.orfeogpl.org	             */
/* 	Idea Original de la SUPERINTENDENCIA DE SERVICIOS PUBLICOS DOMICILIARIOS         */
/* 	orfeogpl@gmail.com                   */
/* ===========================                                                       */
/*                                                                                   */
/* Este programa es software libre. usted puede redistribuirlo y/o modificarlo       */
/* bajo los terminos de la licencia GNU General Public publicada por                 */
/* la "Free Software Foundation"; Licencia version 2. 			                     */
/*                                                                                   */
/* Copyright (c) 2005 por :	  	  	                                                 */
/* SSPD "Superintendencia de Servicios Publicos Domiciliarios"                       */
/*   Jairo Hernan Losada  jlosada@gmail.com                Desarrollador             */
/*   Sixto Angel Pinzón López --- angel.pinzon@gmail.com   Desarrollador           */
/* C.R.A.  "COMISION DE REGULACION DE AGUAS Y SANEAMIENTO AMBIENTAL"                 */
/*   Liliana Gomez        lgomezv@gmail.com                Desarrolladora            */
/*   Lucia Ojeda          lojedaster@gmail.com             Desarrolladora            */
/* D.N.P. "Departamento Nacional de Planeación"                                     */
/*   Hollman Ladino       hollmanlp@gmail.com                Desarrollador           */
/*                                                                                   */
/* Colocar desde esta lInea las Modificaciones Realizadas Luego de la Version 3.5    */
/*  Nombre Desarrollador   Correo     Fecha   Modificacion                           */
/*  Infometrika            info@infometrika.com  05/2009  Arreglo Variables Globales */
/*  Jairo Losada           jlosada@gmail.com     05/2009  Eliminacion Funciones-Procesos */
/* * ********************************************************************************** */
/**
 * Pagina Realiza la radicacion de Documentos.
 * hay dos opciones ODT que realiza el mismo servidor para lo cual es requerido librerias xml
 *
 * Se añadio compatibilidad con variables globales en Off
 * @autor Jairo Losada 2009-05
 * @licencia GNU/GPL
 */
foreach ($_GET as $key => $valor)
   $$key = $valor;
foreach ($_POST as $key => $valor)
   $$key = $valor;
foreach ($_SESSION as $key => $valor)
   $$key = $valor;

define('ADODB_ASSOC_CASE', 1);

$krd = $_SESSION["krd"];
$dependencia = $_SESSION["dependencia"];
$dependencia_nombre = $_SESSION["depe_nomb"];
$usua_doc = $_SESSION["usua_doc"];
$codusuario = $_SESSION["codusuario"];
$nivelus = $_SESSION["nivelus"];
$tip3Nombre = $_SESSION["tip3Nombre"];
$tip3desc = $_SESSION["tip3desc"];
$tip3img = $_SESSION["tip3img"];
$id_rol = $_SESSION["id_rol"];
//$_SESSION['usua_debug'] = 1;
if ($_SESSION['usua_debug'] == 1) {
    print_r($_GET);
    echo "<hr>";
    print_r($_POST);
}

if (!$ruta_raiz)
    $ruta_raiz = ".";
include("$ruta_raiz/config.php");
if (isset($db))
    unset($db);
include_once("$ruta_raiz/include/db/ConnectionHandler.php");
$db = new ConnectionHandler("$ruta_raiz");
$db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
//$db->conn->debug=true;

require_once("$ruta_raiz/class_control/anexo.php");
require_once("$ruta_raiz/class_control/CombinaError.php");
require_once("$ruta_raiz/class_control/Sancionados.php");
require_once("$ruta_raiz/class_control/Dependencia.php");
require_once("$ruta_raiz/class_control/Esp.php");
require_once("$ruta_raiz/class_control/TipoDocumento.php");
require_once("$ruta_raiz/class_control/Radicado.php");
require_once("$ruta_raiz/include/tx/Radicacion.php");
require_once("$ruta_raiz/include/tx/Historico.php");
require_once("$ruta_raiz/class_control/ControlAplIntegrada.php");
require_once("$ruta_raiz/include/tx/Expediente.php");
//require_once("$ruta_raiz/include/tx/Historico.php");
//Cargando variables del log
if(isset($_SERVER['HTTP_X_FORWARD_FOR'])){
    $proxy=$_SERVER['HTTP_X_FORWARD_FOR'];
}else
    $proxy=$_SERVER['REMOTE_ADDR'];
$REMOTE_ADDR=$_SERVER['REMOTE_ADDR'];
$ruta_raiz2=$ruta_raiz;
$ruta_raiz='..';
include_once "$ruta_raiz/core/clases/log.php";
$log = new log($ruta_raiz);
$log->setAddrC($REMOTE_ADDR);
$log->setProxyAd($proxy);
$ruta_raiz=$ruta_raiz2;
$log->setUsuaCodi($codusuario);
$log->setDepeCodi($dependencia);
$log->setRolId($id_rol);
$log->setDenomDoc('Radicado');

error_reporting(0);
$dep = new Dependencia($db);
$espObjeto = new Esp($db);
$radObjeto = new Radicado($db);
$radObjeto->radicado_codigo($numrad);
$tdoc = new TipoDocumento($db); //objeto que maneja el tipo de documento del anexos
$tdoc2 = new TipoDocumento($db); //objeto que maneja el tipo de documento del radicado
$tdoc2->TipoDocumento_codigo($radObjeto->getTdocCodi());

$fecha_dia_hoy = date("Y-m-d");
//$sqlFechaHoy = $db->conn->OffsetDate(0,$db->conn->sysTimeStamp);
$sqlFechaHoy = $db->conn->sysTimeStamp; /// 2011-06-20 09:38:28.280162
//$sqlFechaHoy=date("Y-m-d H:i:s.u");
//echo "*** <hr> $sqlFechaHoy <hr>";
//OBJETO CONTROL DE APLICACIONES INTEGRADAS.
$objCtrlAplInt = new ControlAplIntegrada($db);
//OBJETO EXPEDIENTE
$objExpediente = new Expediente($db);
$expRadi = $objExpediente->consulta_exp($numrad);


$dep->Dependencia_codigo($dependencia);
$dep_sigla = $dep->getDepeSigla();
$nurad = trim($nurad);
$numrad = trim($numrad);
$hora = date("H") . "_" . date("i") . "_" . date("s");
// var que almacena el dia de la fecha
$ddate = date('d');
// var que almacena el mes de la fecha
$mdate = date('m');
// var que almacena el a�o de la fecha
$adate = date('Y');
// var que almacena  la fecha formateada
$fechaArchivo = $adate . "_" . $mdate . "_" . $ddate;
//var que almacena el nombre que tendr� la pantilla
$archInsumo = "tmp_" . $usua_doc . "_" . $fechaArchivo . "_" . $hora . ".txt";
//Var que almacena el nombre de la ciudad de la territorial
$terr_ciu_nomb = $dep->getTerrCiuNomb();
//Var que almacena el nombre corto de la territorial
$terr_sigla = $dep->getTerrSigla();
//Var que almacena la direccion de la territorial
$terr_direccion = $dep->getTerrDireccion();
//Var que almacena el nombre largo de la territorial
$terr_nombre = $dep->getTerrNombre();
//Var que almacena el nombre del recurso
$nom_recurso = $tdoc2->get_sgd_tpr_descrip(); //
?><HEAD>
    <TITLE>Gen  -  ORFEO - <?php echo  DATE ?></TITLE>
    <link rel="stylesheet" href="estilos_totales.css">
</HEAD>

<body>
    <?php     if (!$numrad) {
        $numrad = $verrad;
    }
    if (strlen(trim($radicar_a)) == 13 or strlen(trim($radicar_a)) == 18) {
        $no_digitos = 5;
    } else {
        $no_digitos = 6;
    }
    $linkArchSimple = strtolower($linkarchivo);
    $linkArchivoTmpSimple = strtolower($linkarchivotmp);

    $linkarchivo = "$ruta_raiz/" . strtolower($linkarchivo);
    $linkarchivotmp = "$ruta_raiz/" . strtolower($linkarchivotmp);
    $fechah = date("Ymd") . "_" . time("hms");
    $trozosPath = explode("/", $linkarchivo);
    $nombreArchivo = $trozosPath[count($trozosPath) - 1];

// ABRE EL ARCHIVO
    $a = new Anexo($db);
    $a->anexoRadicado($numrad, $anexo);
    $apliCodiaux = $a->get_sgd_apli_codi();
    $anex = $a;
    $secuenciaDocto = $a->get_doc_secuencia_formato($dependencia);
    $fechaDocumento = $a->get_sgd_fech_doc();
    $tipoDocumento = $a->get_sgd_tpr_codigo();
    $tdoc->TipoDocumento_codigo($tipoDocumento);

    $tipoDocumentoDesc = $tdoc->get_sgd_tpr_descrip();

    if ($radicar_documento) {
        $isql = "select RADI_NUME_SALIDA, SGD_EXP_NUMERO,ANEX_DESC,  SGD_DIR_TIPO from ANEXOS
				 where ANEX_CODIGO='$anexo' AND ANEX_RADI_NUME=$numrad
				 and (anex_estado <>4 or anex_estado is null)";

        if ($_SESSION ['usua_debug'] == 1)
            $db->conn->debug = true;
        $rs = $db->query($isql);
        if (!$rs->EOF) {
            $radicado_salida = $rs->fields ['RADI_NUME_SALIDA'];
            $expAnexoActual = $rs->fields ['SGD_EXP_NUMERO'];
            $Anexo_Asunto = $rs->fields ['ANEX_DESC'];
            $reservado = $rs->fields ['RADI_RESERVADO'];
            $rem_destino = $rs->fields ['SGD_DIR_TIPO'];
            if ($expAnexoActual != '') {
                $expRadi = $expAnexoActual;
            }
        } else {
            //$db->conn->RollbackTrans();
            die("<span class='etextomenu'>No se ha podido obtener la informacion del radicado");
        }

        //GENERACION DE LA SECUENCIA PARA DOCUMENTOS ESPECIALES  *******************************
        // Generar el Numero de Radicacion
        if (($ent != 2) and $nurad and $vpppp == "ddd") {
            $sec = $nurad;
            $anoSec = substr($nurad, 0, 4);
            // @tipoRad define el tipo de radicado el -X
            $tipoRad = substr($radicar_documento, -1);
        } else {
            if ($vp == "n" and $radicar_a == "si") {
                if ($generar_numero == "no") {
                    $sec = substr($nurad, 7, $no_digitos);
                    $anoSec = substr($nurad, 0, 4);
                    $tipoRad = substr($radicar_documento, -1);
                } else {
                    $isql = "select * from ANEXOS where ANEX_CODIGO='$anexo' AND ANEX_RADI_NUME=$numrad";
                    $rs = $db->query($isql);
                    if (!$rs->EOF) {
                        $radicado_salida = $rs->fields['RADI_NUME_SALIDA'];
                        $expAnexoActual = $rs->fields['SGD_EXP_NUMERO'];
                        if ($expAnexoActual != '') {
                            $expRadi = $expAnexoActual;
                        }
                    } else {
                        $db->conn->RollbackTrans();
                        die("<span class='etextomenu'>No se ha podido obtener la informacion del radicado");
                    }

                    if (!$radicado_salida) {
                        $no_digitos = 6;
                        $tipoRad = "1";
                    } else {
                        $sec = substr($radicado_salida, 7, $no_digitos);
                        $tipoRad = substr($radicar_documento, -1);
                        $anoSec = substr($radicado_salida, 0, 4);
                        $db->conn->RollbackTrans();
                        die("<span class='etextomenu'><br>Ya estaba radicado<br>");
                        $radicar_a = $radicado_salida;
                    }
                }
            } else {
                if ($vp == "s") {
                    $sec = "XXX";
                } else {
                    // EN ESTA PARTE ES EN LA CUAL SE ENTRA A ASIGNAR EL NUMERO DE RADICADO
                    $sec = substr($radicar_a, 7, $no_digitos);
                    $anoSec = substr($radicar_a, 0, 4);
                    $tipoRad = substr($radicar_a, 13, 1);
                }
            }
            // GENERACION DE NUMERO DE RADICADO DE SALIDA
            $sec = str_pad($sec, $no_digitos, "0", STR_PAD_LEFT);
            $plg_comentarios = "";
            $plt_codi = $plt_codi;
            if (!$anoSec) {
                $anoSec = date("Y");
            }
            if (!$tipoRad) {
                $tipoRad = "1";
            }

            //Adicion para que no reemplace el numero de radicado de un anexo al ser reasignado a otra dependencia
            if ($generar_numero == "no") {
                $rad_salida = $numrad;
            } else {
                //Es un anexo radicado en otra dependencia y no queremos que le genere un nuevo numero
                if ($radicar_a != null && $radicar_a != 'si') {
                    $rad_salida = $radicar_a;
                } else {
                    $rad_salida = $anoSec . $dependencia . $sec . $tipoRad;
                }
            }


            if ($numerar == 1) {
                //print ("CAMBIA LA SALIDA POR QUE NUMERA");
                $numResol = $a->get_doc_secuencia_formato();
                $rad_salida = date("Y") . $dependencia . str_pad($a->sgd_doc_secuencia(), 6, "0", STR_PAD_left) . $a->get_sgd_tpr_codigo();
            }
        }
        //**********************************************************************************************************************************
        // * FIN GENRACION DE NUMERO DE RADICADO DE SALIDA
        //$ext = substr(trim($linkarchivo), -3);
        $iArray = explode(".", $linkarchivo);

        $ext = $iArray[count($iArray) - 1];
        echo "<font size='3' color='#000000'><span class='etextomenu'>";

        $extVal = strtoupper($ext);
        if ($extVal == "XLS" or $extVal == "PPT") {//or $extVal == "PDF"
            echo "<br><font size='3' ><span class='etextomenu'>Sobre formato ($ext) no se puede realizar combinaci&oacute;n de correspondencia</br>";
            die();
        } else {
            require "$ruta_raiz/jh_class/funciones_sgd.php";
            $verrad = $numrad;
            $radicado_p = $verrad;
            $no_tipo = "true";
            require "$ruta_raiz/ver_datosrad.php";
            include "$ruta_raiz/radicacion/busca_direcciones.php";
            $a = new LOCALIZACION($codep_us1, $muni_us1, $db);
            $dpto_nombre_us1 = $a->departamento;
            $muni_nombre_us1 = $a->municipio;
            $a = new LOCALIZACION($codep_us2, $muni_us2, $db);
            $dpto_nombre_us2 = $a->departamento;
            $muni_nombre_us2 = $a->municipio;

            //Se agrega, pues no esta verificando si se modifico el municipio y/o departamento de la ESP
            $isql = "select MUNI_CODI, DPTO_CODI from ANEXOS
				   where ANEX_CODIGO='$anexo' AND ANEX_RADI_NUME=$numrad";
            $rs = $db->query($isql);
            if (!$rs->EOF) {
                $codigoCiudadESPMod = $rs->fields ['MUNI_CODI'];
                $codigoDeptoESPMod = $rs->fields ['DPTO_CODI'];
                if ($espcodi && $codigoCiudadESPMod && $codigoDeptoESPMod) {
                    $codep_us3 = $codigoDeptoESPMod;
                    $muni_us3 = $codigoCiudadESPMod;
                }
            }
            $a = new LOCALIZACION($codep_us3, $muni_us3, $db);
            $dpto_nombre_us3 = $a->departamento;
            $muni_nombre_us3 = $a->municipio;
            $espObjeto->Esp_nit($cc_documento_us3);
            $nuir_e = $espObjeto->getNuir();
            // Inicializacion de la fecha que va a pasar al reemplazable *F_RAD_S*
            $fecha_hoy_corto = "";
            include "$ruta_raiz/class_control/class_gen.php";

            $b = new CLASS_GEN();
            $date = date("m/d/Y");
            $fecha_hoy = $b->traducefecha($date);
            $fecha_e = $b->traducefecha($radi_fech_radi);
            $fechaDocumento2 = $b->traducefecha_sinDia($fechaDocumento);
            $fechaDocumento = $b->traducefechaDocto($fechaDocumento);

            if ($vp == "n")
                $archivoFinal = $linkArchSimple;
            else
                $archivoFinal = $linkArchivoTmpSimple;

            //almacena la extension del archivo a procedar
            $extension = (strrchr($archivoFinal, "."));
            $archSinExt = substr($archivoFinal, 0, strpos($archivoFinal, $extension));
            //Almacena el path completo hacia el archivo a producirse luego de la combinacion

            if (substr($archSinExt, -1) == "d") {
                $caracterDefinitivo = "";
            } else {
                $caracterDefinitivo = "d";
            }

            if ($ext == 'xml' || $ext == 'XML' || $ext == 'odt' || $ext == 'ODT' || $ext == 'DOCX' || $ext == 'docx') {
                $archivoFinal = $archSinExt . $caracterDefinitivo . "." . $ext;
                //$archivoFinal = $archSinExt .  "." . $ext;
            } else {
                $archivoFinal = $archSinExt . "." . $ext;
            }

            //Almacena el nombre de archivo a producirse luego de la combinacion y que ha de actualizarce en la tabla de anexos
            $archUpdate = substr($archivoFinal, strpos($archivoFinal, strrchr($archivoFinal, "/")) + 1, strlen($archivoFinal) - strpos($archivoFinal, strrchr($archivoFinal, "/")) + 1);
            //Almacena el path de archivo a producirse luego de la combinacion y que ha de actualizarce en la tabla de radicados
            $archUpdateRad = substr_replace($archivoFinal, "", 0, strpos($archivoFinal, "bodega") + strlen("bodega"));
        }
        //****************************************************************************************************
        //$db->conn->BeginTrans();
        $tipo_docto = $anex->get_sgd_tpr_codigo();

        //---------->>
        //Adiciones para DIR_E SSPD, no quieren que se reemplace DIR_R con DIR_E para Sancionados
        $campos = array();
        $datos = array();
        $anex->obtenerArgumentos($campos, $datos);

        $vieneDeSancionados = 0;

        //Trae la informacion de Sancionados y genera los campos de combinacion
        $camposSanc = array();
        $datosSanc = array();
        if ($_SESSION ['usua_debug'] == 1)
            $db->conn->debug = true;
        $objSancionados = new Sancionados($db);
        if ($objSancionados->sancionadosRad($anexo)) {

            $objSancionados->obtenerCampos($camposSanc, $datosSanc);
            $vieneDeSancionados = 1;
        } else if ($objSancionados->sancionadosRad($numrad)) {

            $objSancionados->obtenerCampos($camposSanc, $datosSanc);
            $vieneDeSancionados = 2;
        }
        else
            $vieneDeSancionados = 0;

        $isql = "select SGD_DIR_DIRECCION, SGD_DIR_TIPO from ANEXOS where ANEX_CODIGO='$anexo' AND ANEX_RADI_NUME=$numrad";
        $rs = $db->query($isql);

        //Verifica y cambia la direccion cuando modifican la dir de la ESP, se movio a
        //esta parte para que funcione tambien en previsualizacion


        $direccionAlterna = $rs->fields ["SGD_DIR_DIRECCION"];
        $sgd_dir_tipo = $rs->fields ["SGD_DIR_TIPO"];


        if ($sgd_dir_tipo == 2 && $vieneDeSancionados == 0) {
            $dir_tipo_us1 = $dir_tipo_us2;
            $tipo_emp_us1 = $tipo_emp_us2;
            $nombre_us1 = $nombre_us2;
            $grbNombresUs1 = $nombre_us2;
            $documento_us1 = $documento_us2;
            $cc_documento_us1 = $cc_documento_us2;
            $prim_apel_us1 = $prim_apel_us2;
            $seg_apel_us1 = $seg_apel_us2;
            $telefono_us1 = $telefono_us2;
            $direccion_us1 = $direccion_us2;
            $mail_us1 = $mail_us2;
            $muni_us1 = $muni_us2;
            $codep_us1 = $codep_us2;
            $tipo_us1 = $tipo_us2;
            $otro_us1 = $otro_us2;
        }
        if ($sgd_dir_tipo == 3 && $vieneDeSancionados == 0) {
            $dir_tipo_us1 = $dir_tipo_us3;
            $tipo_emp_us1 = $tipo_emp_us3;
            $nombre_us1 = $nombre_us3;
            $grbNombresUs1 = $nombre_us3;
            $documento_us1 = $documento_us3;
            $cc_documento_us1 = $cc_documento_us3;
            $prim_apel_us1 = $prim_apel_us3;
            $seg_apel_us1 = $seg_apel_us3;
            $telefono_us1 = $telefono_us3;
            $direccion_us1 = $direccion_us3;
            $mail_us1 = $mail_us3;
            $muni_us1 = $muni_us3;
            $codep_us1 = $codep_us3;
            $tipo_us1 = $tipo_us3;
            $otro_us1 = $otro_us3;
        }
        if ($direccionAlterna and $sgd_dir_tipo == 3) {
            $direccion_us3 = $direccionAlterna;
            $muni_us3 = $muniCodiAlterno;
            $codep_us3 = $dptoCodiAlterno;
        }
        $trte_codix = "null";
        if ($sgd_dir_tipo == 7) {
            $sqltipo7 = "select SGD_TRD_CODIGO , SGD_DIR_NOMREMDES NOMREMDES, SGD_DIR_DOC DOC, MUNI_CODI, DPTO_CODI,
			id_pais, id_cont, SGD_DOC_FUN, SGD_OEM_CODIGO, SGD_CIU_CODIGO, SGD_ESP_CODI, RADI_NUME_RADI,
			SGD_SEC_CODIGO,	SGD_DIR_DIRECCION, SGD_DIR_TELEFONO, SGD_DIR_MAIL, SGD_DIR_TIPO, SGD_DIR_CODIGO,
			 SGD_ANEX_CODIGO, SGD_DIR_NOMBRE
			 	from sgd_dir_drecciones
					where
					sgd_ANEX_CODIGO='$anexo' and  sgd_dir_tipo=7	";
            //$cc=NULL;

            if ($_SESSION ['usua_debug'] == 1) {
                echo "sqltipo7" . $sqltipo7;
            }

            $genDo = 7;
            $rs2 = $db->query($sqltipo7);
            $sgd_oem_codigo = $rs2->fields ["SGD_OEM_CODIGO"];
            $sgd_ciu_codigo = $rs2->fields ["SGD_CIU_CODIGO"];
            $sgd_esp_codigo = $rs2->fields ["SGD_ESP_CODI"];
            $sgd_fun_codigo = $rs2->fields ["SGD_DOC_FUN"];
            if ($sgd_ciu_codigo > 0) {
                $isql = "select SGD_CIU_NOMBRE NOMBRE,SGD_CIU_APELL1 APELL1,SGD_CIU_APELL2 APELL2,SGD_CIU_CEDULA IDENTIFICADOR,SGD_CIU_EMAIL MAIL,SGD_CIU_DIRECCION  DIRECCION
					 from sgd_ciu_ciudadano where sgd_ciu_codigo=$sgd_ciu_codigo";
            }
            if ($sgd_esp_codigo > 0) {
                $isql = "select nombre_de_la_empresa NOMBRE, identificador_empresa IDENTIFICADOR,EMAIL MAIL,DIRECCION DIRECCION from bodega_empresas where identificador_empresa=$sgd_esp_codigo";
            }
            if ($sgd_oem_codigo > 0) {
                $isql = "select sgd_oem_oempresa NOMBRE, SGD_OEM_DIRECCION DIRECCION, sgd_oem_codigo IDENTIFICADOR from sgd_oem_oempresas  where sgd_oem_codigo=$sgd_oem_codigo";
            }
            if ($sgd_fun_codigo > 0) {
                $isql = "select usua_nomb NOMBRE, d.depe_nomb DIRECCION, usua_doc IDENTIFICADOR, usua_email MAIL from usuario u ,dependencia d  where usua_doc='$sgd_fun_codigo'
	     		  and  u.DEPE_CODI = d.DEPE_CODI ";
            }
            $rs3 = $db->query($isql);

            $trte_codix = "2";
            $trte_codixSql = ", trte_codi= $trte_codix";
            $grbNombresUs1 = $rs2->fields ["NOMREMDES"];
            $sgdTrd = $rs2->fields ["SGD_TRD_CODIGO"];
            $telefono_us1 = $rs2->fields ["SGD_DIR_TELEFONO"];
            $Cool = 'jejeje';
            $documento_us1 = $rs3->fields ["IDENTIFICADOR"]; //$rs2->fields ["SGD_CIU_CODIGO"];
            $idcont1 = $rs2->fields ["ID_CONT"];
            $idpais1 = $rs2->fields ["ID_PAIS"];
            $codep_us1 = $rs2->fields ["DPTO_CODI"];
            $muni_us1 = $rs2->fields ["MUNI_CODI"];
            $cc_documento_us1 = $rs2->fields ["DOC"];
            $nombre_us1 = $rs3->fields ["NOMBRE"] . " " . $rs3->fields ["APELL1"] . " " . $rs3->fields ["APELL2"]; //$rs2->fields ["NOMREMDES"];
            $direccion_us1 = $rs2->fields ["SGD_DIR_DIRECCION"];
            $mail_us1 = $rs2->fields ["SGD_DIR_MAIL"];
            $otro_us1 = $rs2->fields ["SGD_DIR_NOMBRE"];


            //$tipo_emp_us1=1;
            /* 			$dir_tipo_us1 = $dixxr_tipo_us2;

              $nombre_us1=$nombre_us2;
              $grbNombresUs1=$nombre_us2;
              $tipo_us1 = $tipo_us2; */
            $nombret_us1_u = $nombre_us1;
            $sqlDm = "select m.muni_nomb,d.dpto_nomb from municipio m, departamento d where m.dpto_codi=d.dpto_codi and m.id_pais=d.id_pais
	          and m.muni_codi=$muni_us1 and m.dpto_codi=$codep_us1 and m.id_pais=$idpais1 and m.id_cont=$idcont1";
            $rs6 = $db->query($sqlDm);

            $dpto_nombre_us1 = $rs6->fields ["DPTO_NOMB"];
            ;
            $muni_nombre_us1 = $rs6->fields ["MUNI_NOMB"];
            ;
            $cc_documentous1 = $cc_documento_us1;
        }
        //--------------------->>



        if (!$tipo_docto)
            $tipo_docto = 0;
        if ($sec and $vp == "n") {
            if ($generar_numero != "no" and $radicar_a == "si") {
                if (!$tpradic) {
                    $tpradic = 'null';
                }

                $rad = new Radicacion($db);
                $hist = new Historico($db);
                $rad->radiTipoDeri = 0;
                $rad->radiCuentai = "''";
                $rad->eespCodi = $espcodi;
                $rad->id_rol = $id_rol;
                $rad->radiRolRadi = $id_rol;
                $rad->radi_usua_doc = $usua_doc;
                $rad->mrecCodi = "null";
                $rad->radiFechOfic = $sqlFechaHoy;
                $rad->radiNumeDeri = trim($verrad);
                $rad->descAnex = $desc_anexos;
                $rad->radiPais = "$pais";
                $rad->raAsun = $Anexo_Asunto; //$asunto ;

                if ($tpradic == 1) {
                    if ($entidad_depsal != 0) {
                        $rad->radiDepeActu = $entidad_depsal;
                        $rad->radiUsuaActu = 2;
                        $rad->id_rol = 1;
                    } else {
                        $rad->radiDepeActu = $dependencia;
                        $rad->radiUsuaActu = $codusuario;
                        $rad->id_rol = $id_rol;
                    }
                } else {
                    $rad->radiDepeActu = $dependencia;
                    $rad->radiUsuaActu = $codusuario;
                    $rad->id_rol = $id_rol;
                }
                $rad->usuaCodi = $codusuario;
                $rad->radiDepeRadi = $dependencia;
                $rad->trteCodi = $trte_codix;
                $rad->tdocCodi = $tipo_docto;

                $rad->tdidCodi = "null";
                if ($tpradic == 2)
                    $carcodi = 0;
                else
                    $carcodi = $tpradic;
                $rad->carpCodi = $carcodi; //por revisar como recoger el valor
                $rad->carPer = 0;
                //$rad->trteCodi = "null";
                $rad->ra_asun = "'$Anexo_Asunto'";

                $rad->radiPath = "'$archUpdateRad'";

                if (strlen(trim($apliCodiaux)) > 0 && $apliCodiaux > 0)
                    $aplinteg = $apliCodiaux;
                else
                    $aplinteg = "0";

                $rad->sgd_apli_codi = $aplinteg;
                $codTx = 2;
                $flag = 1;
                if ($_SESSION ['usua_debug'] == 1)
                    $db->conn->debug = true;
                // Se genera el numero de radicado del anexo
                $noRad = $rad->newRadicado($tpradic, $tpDepeRad[$tpradic], 1);

                // Se instancia un objeto para el radicado generado y obtener la fecha real de radicacion
                $radGenerado = new Radicado($db);
                $radGenerado->radicado_codigo($noRad);

                // Asgina la fecha de radicacion

                $fecha_hoy_corto = $radGenerado->getRadi_fech_radi("d-m-Y");

                //BUSCA QUERYS ADICIONALES RESPECTO DE APLICATIVOS INTEGRADOS
                $campos ["P_RAD_E"] = $noRad;
                $campos ["P_USUA_CODI"] = $codusuario;
                $campos ["P_DEPENDENCIA"] = $dependencia;
                $campos ["P_USUA_DOC"] = $usua_doc;
                $campos ["P_COD_REF"] = $anexo;

                //El nuevo radicado hereda la informacion del expediente del radicado padre
                if (isset($expRadi) && $expRadi != 0) {
                    $resultadoExp = $objExpediente->insertar_expediente($expRadi, $noRad, $dependencia, $codusuario, $usua_doc);
                    $radicados = "";
                    if ($resultadoExp == 1) {
                        $observa = "Se ingresa al expediente del radicado padre ($numrad)";
                        include_once "$ruta_raiz/include/tx/Historico.php";
                        $radicados [] = $noRad;
                        $tipoTx = 53;
                        $Historico = new Historico($db);
                        $Historico->insertarHistoricoExp($expRadi, $radicados, $dependencia, $codusuario, $observa, $tipoTx, 0, 0);
                        $log->setAction('document_added_to_record');
                        $log->setOpera("Se asocia radicado del padre($numrad) al expediente $expRadi");
                        $log->setNumDocu($noRad);
                        $log->registroEvento();
                    } else {
                        die('<hr><font color=red>No se anexo este radicado al expediente. Verifique que el numero del expediente exista e intente de nuevo.</font><hr>');
                    }
                }

                $estQueryAdd = $objCtrlAplInt->queryAdds($noRad, $campos, $MODULO_RADICACION_DOCS_ANEXOS);
                if ($estQueryAdd == "0") {
                    //$db->conn->RollbackTrans();
                    die();
                }

                $radicadosSel [0] = $noRad;
                $hist->insertarHistorico($radicadosSel, $dependencia, $codusuario, $id_rol, $dependencia, $codusuario, $id_rol, " ", $codTx);
                if ($noRad == "-1") {
                    //$db->conn->RollbackTrans();
                    die("<hr><b><font color=red><center>Error no genero un Numero de Secuencia o inserto el radicado </center></font></b><hr>");
                }
                $rad_salida = $noRad;
            } else {
                $linkarchivo_grabar = str_replace("bodega", "", $linkarchivo);
                $linkarchivo_grabar = str_replace("./", "", $linkarchivo_grabar);
                $posExt = strpos($linkarchivo_grabar, 'd.doc');
                if ($posExt === false) {
                    $temp = $linkarchivo_grabar;
                    $ruta = str_replace('.doc', 'd.doc', $temp);
                    $linkarchivo_grabar = $ruta;
                }
                // agregar cambio  al  asunto
                if ($espcodi == null)
                    $espcodi = 0;
		if(substr($Anexo_Asunto, 0, 19) == 'Imagen del radicado'){
		    $asun_estado="";
		}else{
                    $asun_estado=", RA_ASUN='$Anexo_Asunto' ";
		}
                $isql = "update RADICADO set EESP_CODI= $espcodi,RADI_PATH='$linkarchivo_grabar' $asun_estado  $trte_codixSql
					where RADI_NUME_RADI = $rad_salida";

                $radGenerado = new Radicado($db);
                $radGenerado->radicado_codigo($rad_salida);
                // Asgina la fecha de radicacion
                $fecha_hoy_corto = $radGenerado->getRadi_fech_radi("d-m-Y");
                $rs = $db->query($isql);
                if (!$rs) {
                    //$db->conn->RollbackTrans();
                    die("<span class='etextomenu'>No se ha podido Actualizar el Radicado");
                }
            }

            if ($ent == 1)
                $rad_salida = $nurad;

            $isql = "update ANEXOS set RADI_NUME_SALIDA=$rad_salida,
						ANEX_SOLO_LECT = 'S',
						ANEX_RADI_FECH = $sqlFechaHoy,
						ANEX_ESTADO = 2,
						ANEX_NOMB_ARCHIVO = '$archUpdate',
						ANEX_TIPO='$numextdoc',
						SGD_DEVE_CODIGO = null
		           where ANEX_CODIGO='$anexo' AND ANEX_RADI_NUME=$numrad
		           and (anex_estado <>4 or anex_estado is null)";

            $rs = $db->query($isql);
            $log->setAction('linked_document_committed');
            $log->setOpera("Anexo radicado y padre ($numrad)");
            $log->setNumDocu($rad_salida);
            $log->registroEvento();
            if (!$rs) {
                //$db->conn->RollbackTrans();
                die("<span class='etextomenu'>No se ha podido actualizar la informacion de anexos");
            }

            $isql = "select * from ANEXOS where ANEX_CODIGO='$anexo' AND ANEX_RADI_NUME=$numrad";
            $rs = $db->query($isql);
            if ($rs == false) {
                //$db->conn->RollbackTrans();
                die("<span class='etextomenu'>No se ha podido obtener la informacion de anexo");
            }

            $sgd_dir_tipo = $rs->fields["SGD_DIR_TIPO"];
            $anex_desc = $rs->fields["ANEX_DESC"];
            $anex_numero = $rs->fields["ANEX_NUMERO"];
            $direccionAlterna = $rs->fields["SGD_DIR_DIRECCION"];
            $pasar_direcciones = true;
            $dep_radicado = substr($rad_salida, 4, 3);
            //	 echo ("al radicar($dep_radicado)($rad_salida)");
            $carp_codi = 1;

            if (!$tipo_docto)
                $tipo_docto = 0;

            $linkarchivo_grabar = str_replace("bodega", "", $linkarchivo);
            $linkarchivo_grabar = str_replace("./", "", $linkarchivo_grabar);

            if ($sgd_dir_tipo == 1) {
                $grbNombresUs1 = $nombret_us1_u;
            }

            //---------->>
            //Adiciones para DIR_E SSPD, no quieren que se reemplace DIR_R con DIR_E
            $campos = array();
            $datos = array();
            $anex->obtenerArgumentos($campos, $datos);
            $vieneDeSancionados = 0;


            //Trae la informacion de Sancionados y genera los campos de combinacion
            $camposSanc = array();
            $datosSanc = array();
            $objSancionados = new Sancionados($db);
            if ($objSancionados->sancionadosRad($anexo)) {
                $objSancionados->obtenerCampos($camposSanc, $datosSanc);
                $vieneDeSancionados = 1;
            } else if ($objSancionados->sancionadosRad($numrad)) {
                $objSancionados->obtenerCampos($camposSanc, $datosSanc);
                $vieneDeSancionados = 2;
            }
            else
                $vieneDeSancionados = 0;

            //------->>
            if ($sgd_dir_tipo == 2 && $vieneDeSancionados == 0) {
                $dir_tipo_us1 = $dir_tipo_us2;
                $tipo_emp_us1 = $tipo_emp_us2;
                $nombre_us1 = $nombre_us2;
                $grbNombresUs1 = $nombre_us2;
                $documento_us1 = $documento_us2;
                $cc_documento_us1 = $cc_documento_us2;
                $prim_apel_us1 = $prim_apel_us2;
                $seg_apel_us1 = $seg_apel_us2;
                $telefono_us1 = $telefono_us2;
                $direccion_us1 = $direccion_us2;
                $mail_us1 = $mail_us2;
                $muni_us1 = $muni_us2;
                $codep_us1 = $codep_us2;
                $tipo_us1 = $tipo_us2;
                $otro_us1 = $otro_us2;
            }
            if ($sgd_dir_tipo == 3 && $vieneDeSancionados == 0) {
                $dir_tipo_us1 = $dir_tipo_us3;
                $tipo_emp_us1 = $tipo_emp_us3;
                $nombre_us1 = $nombre_us3;
                $grbNombresUs1 = $nombre_us3;
                $documento_us1 = $documento_us3;
                $cc_documento_us1 = $cc_documento_us3;
                $prim_apel_us1 = $prim_apel_us3;
                $seg_apel_us1 = $seg_apel_us3;
                $telefono_us1 = $telefono_us3;
                $direccion_us1 = $direccion_us3;
                $mail_us1 = $mail_us3;
                $muni_us1 = $muni_us3;
                $codep_us1 = $codep_us3;
                $tipo_us1 = $tipo_us3;
                $otro_us1 = $otro_us3;
            }
            if ($direccionAlterna and $sgd_dir_tipo == 3) {
                $direccion_us3 = $direccionAlterna;
                $muni_us3 = $muniCodiAlterno;
                $codep_us3 = $dptoCodiAlterno;
            }
            $nurad = $rad_salida;
            $documento_us2 = "";
            $documento_us3 = "";
            $conexion = $db;

            if ($numerar != 1)
                include "$ruta_raiz/radicacion/grb_direcciones.php";

            $actualizados = 4;
            $sgd_dir_tipo = 1;

            if ($numerar == 1 or $nurad == $numrad) {


                $isql = "update ANEXOS set
		  			sgd_rem_destino=1,sgd_dir_tipo=1
	               where
			       RADI_NUME_SALIDA=$nurad
			       and sgd_dir_tipo='7'";
                $rs0 = $db->query($isql);
                if (!$rs0) {
                    //$db->conn->RollbackTrans();
                    die("<span class='etextomenu'>No se ha borrar los datos previos del radicado");
                }
                $isql = "update  sgd_dir_drecciones set  SGD_DIR_TIPO=0,radi_nume_radi=$nurad
           	where  sgd_anex_codigo='$anexo' and  SGD_DIR_TIPO=7";
                $rs1c = $db->query($isql);
                if (!$rs1c) {
                    //$db->conn->RollbackTrans();
                    die("<span class='etextomenu'>No se ha borrar los datos previos del radicado");
                }
            }
            // Borro todo lo generando anteriormete .....  para el caso de regenerar
            $isql = "delete from ANEXOS where RADI_NUME_SALIDA=$nurad
			   and CAST( sgd_dir_tipo AS VARCHAR(4) ) like '7%' and sgd_dir_tipo !=7
			   and (anex_estado <>4 or anex_estado is null) ";

            $rs = $db->query($isql);
            if (!$rs) {
                //$db->conn->RollbackTrans();
                die("<span class='etextomenu'>No se ha borrar los datos previos del radicado");
            }
            // fIN BORRADO Para reproceso....
            //los que no han sido enviados
            $isql_chulos = "select sgd_dir_tipo
	         		from ANEXOS
	         		Where   Radi_Nume_Salida =$nurad
         			and (anex_estado <>4 or anex_estado is null)
			 		Order by Radi_Nume_Salida desc";

            $rs_chulos = $db->query($isql_chulos);

            //los que no han sido enviados
            $isql_chulosEnv = "select sgd_dir_tipo
	         		  from ANEXOS
	         		  Where   Radi_Nume_Salida =$nurad
         			  and anex_estado =4
         			  and sgd_dir_tipo >700
			 		  Order by Radi_Nume_Salida desc";

            $rs_chulosEnv = $db->query($isql_chulosEnv);

            while (!$rs_chulos->EOF) {
                $tipo_chulos .= $rs_chulos->fields ['SGD_DIR_TIPO'] . ",";
                $rs_chulos->MoveNext();
            }
            while (!$rs_chulosEnv->EOF) {
                $tipo_chulosEnv .= $rs_chulosEnv->fields ['SGD_DIR_TIPO'] . ",";
                $rs_chulosEnv->MoveNext();
            }

            $isql = "select ANEX_NUMERO
	         from ANEXOS
	         where
			   ANEX_RADI_NUME=$nurad
			   and (anex_estado <>4 or anex_estado is null)
			 Order by ANEX_NUMERO desc
			 ";

            $rs = $db->query($isql);
            if (!$rs->EOF)
                $i = $rs->fields ['ANEX_NUMERO'];
            include_once "./include/query/queryGenarchivo.php";
            $isql = $query1;
            $rs = $db->query($isql);
            $k = 0;

            while (!$rs->EOF) {
                $tipo_copia = $rs->fields ['TIPO_COPIA'];
                if (empty($tipo_copia))
                    $tipo_copia = $i + 1;
                $anexo_new = $rad_salida . substr("00000" . ($i + 1), -5);
                $sgd_dir_codigo = $rs->fields ['SGD_DIR_CODIGO'];
                $radi_nume_radi = $rs->fields ['RADI_NUME_RADI'];
                $sgd_dir_tipo = $rs->fields ['SGD_DIR_TIPO'];
                $anex_tipo = "20";
                $anex_creador = $krd;
                $anex_borrado = "N";
                $anex_nomb_archivo = " ";
                $anexo_num = $i + 1;

                if ($rad_salida == $verrad) {

                    $sqlG = "select max(anex_codigo) maximo from anexos where ANEX_RADI_NUME=$verrad ";
                    $rs2G = $db->query($sqlG);
                    $anexo_new = $rs2G->fields ['MAXIMO'] + 1;
                } else {
                    $anexo_new = $rad_salida . substr("00000" . ($tipo_copia), - 5);
                }

                $isql = "insert into ANEXOS (ANEX_RADI_NUME,RADI_NUME_SALIDA,ANEX_SOLO_LECT,ANEX_RADI_FECH,ANEX_ESTADO,ANEX_CODIGO  ,anex_tipo   ,ANEX_CREADOR  ,ANEX_NUMERO    ,ANEX_NOMB_ARCHIVO   ,ANEX_BORRADO   ,sgd_dir_tipo)
				VALUES ($verrad       ,$rad_salida     ,'S'           ,$sqlFechaHoy       ,2          ,'$anexo_new','$anex_tipo','$anex_creador','$anexo_num','$anex_nomb_archivo','$anex_borrado','$sgd_dir_tipo')";

                $rs2 = $db->query($isql);

                if (!$rs2) {
                    //$db->conn->RollbackTrans();
                    die("<span class='etextomenu'>No se pudo insertar en la tabla de anexos");
                }
                $isql = "UPDATE sgd_dir_drecciones
			         set RADI_NUME_RADI=$rad_salida
  				     where sgd_dir_codigo=$sgd_dir_codigo ";
                $rs2 = $db->query($isql);

                if (!$rs2) {
                    //$db->conn->RollbackTrans();
                    die("<span class='etextomenu'>No se pudo actualizar las direcciones");
                }
                $sgd_dir_tipo++;
                $i++;
                $k++;
                $rs->MoveNext();
            }
            echo "<br>Se han generado $k copias<br>";
            ?>
            <p>


        <center>
            <?php             if ($actualizados > 0) {
                if ($ent != 1) {
                    $mensaje = "<input type='button' value='cerrar' onclick='opener.history.go(0); window.close()'>";
                    $mensaje = "";
                    if ($numerar != 1) {
                        $numerar = $numerar;
                        ?>
                        <span class='etextomenu'>Ha sido Radicado el Documento con el N&uacute;mero <br><b>
                                <?php echo  $rad_salida ?><p><?php echo  $mensaje ?>
                                    <?php                                 }
                            }
                            else
                                $mensaje = "";
                        }
                        else {
                            ?>
                            <span class='etextomenu'>No se ha podido radicar el Documento con el N&uacute;mero
                                <?php                             }
                            ?>
                            </center>
                            <?php                         }
                    }
                    $ra_asun = ereg_replace("\n", "-", $ra_asun);
                    $ra_asun = ereg_replace("\r", " ", $ra_asun);
                    $archInsumo = "tmp_" . $usua_doc . "_" . $fechaArchivo . "_" . $hora . ".txt";
//echo "<hr>$archivoFinal<hr>";
                    $fp = fopen(RUTA_BODEGA."/masiva/$archInsumo", 'w+');
                    if (!$fp) {
                        echo "<br><font size='3' ><span class='etextomenu'>ERROR..No se pudo abrir el archivo $ruta_raiz/bodega/masiva/$archInsumo</br>";
                        $db->conn->RollbackTrans();
                        die;
                    }
                    fputs($fp, "archivoInicial=$linkArchSimple" . "\n");
                    fputs($fp, "archivoFinal=$archivoFinal" . "\n");
                    fputs($fp, "*RAD_S*=$rad_salida\n");
                    fputs($fp, "<Radicado>=$rad_salida\n");
                    fputs($fp, "*RAD_E_PADRE*=$radicado_p\n");
                    fputs($fp, "*CTA_INT*=$cuentai\n");
                    fputs($fp, "*ASUNTO*=$ra_asun\n");
                    fputs($fp, "*F_RAD_E*=$fecha_e\n");
                    fputs($fp, "*SAN_FECHA_RADICADO*=$fecha_e\n");
                    fputs($fp, "*NOM_R*=$nombret_us1_u\n");
                    fputs($fp, "<USUARIO>=$nombret_us1_u\n");
                    fputs($fp, "*DIR_R*=$direccion_us1\n");
                    fputs($fp, "*DIR_E*=$direccion_us3\n");
                    fputs($fp, "<SGD_CIU_DIRECCION>=$direccion_us1\n");
                    fputs($fp, "*DEPTO_R*=$dpto_nombre_us1\n");
                    fputs($fp, "*MPIO_R*=$muni_nombre_us1\n");
                    fputs($fp, "<DPTO_NOMB>=$dpto_nombre_us1\n");
                    fputs($fp, "<MUNI_NOMB>=$muni_nombre_us1\n");
                    fputs($fp, "*TEL_R*=$telefono_us1\n");
                    fputs($fp, "*MAIL_R*=$mail_us1\n");
                    fputs($fp, "*DOC_R*=$cc_documentous1\n");
                    fputs($fp, "*NOM_P*=$nombret_us2_u\n");
                    fputs($fp, "*DIR_P*=$direccion_us2\n");
                    fputs($fp, "*DEPTO_P*=$dpto_nombre_us2\n");
                    fputs($fp, "*MPIO_P*=$muni_nombre_us2\n");
                    fputs($fp, "*TEL_P*=$telefono_us1\n");
                    fputs($fp, "*MAIL_P*=$mail_us2\n");
                    fputs($fp, "*DOC_P*=$cc_documento_us2\n");
                    fputs($fp, "*NOM_E*=$nombret_us3_u\n");
                    fputs($fp, "<NOMBRE_DE_LA_EMPRESA>=$nombret_us3_u\n");
                    fputs($fp, "*DIR_E*=$direccion_us3\n");
                    fputs($fp, "*MPIO_E*=$muni_nombre_us3\n");
                    fputs($fp, "*DEPTO_E*=$dpto_nombre_us3\n");
                    fputs($fp, "*TEL_E*=$telefono_us3\n");
                    fputs($fp, "*MAIL_E*=$mail_us3\n");
                    fputs($fp, "*NIT_E*=$cc_documento_us3\n");
                    fputs($fp, "*NUIR_E*=$nuir_e\n");
                    fputs($fp, "*F_RAD_S*=$fecha_hoy_corto\n");
                    fputs($fp, "*RAD_E*=$radicado_p\n");
                    fputs($fp, "*SAN_RADICACION*=$radicado_p\n");
                    fputs($fp, "*SECTOR*=$sector_nombre\n");
                    fputs($fp, "*NRO_PAGS*=$radi_nume_hoja\n");
                    fputs($fp, "*DESC_ANEXOS*=$radi_desc_anex\n");
                    fputs($fp, "*F_HOY_CORTO*=$fecha_hoy_corto\n");
                    fputs($fp, "*F_HOY*=$fecha_hoy\n");
                    fputs($fp, "*NUM_DOCTO*=$secuenciaDocto\n");
                    fputs($fp, "*F_DOCTO*=$fechaDocumento\n");
                    fputs($fp, "*F_DOCTO1*=$fechaDocumento2\n");
                    fputs($fp, "*FUNCIONARIO*=$usua_nomb\n");
                    fputs($fp, "*LOGIN*=$krd\n");
//    fputs($fp, "*DEP_NOMB*=$dependencianomb\n");
                    fputs($fp, "*CIU_TER*=$terr_ciu_nomb\n");
                    fputs($fp, "*DEP_SIGLA*=$dep_sigla\n");
                    fputs($fp, "*TER*=$terr_sigla\n");
                    fputs($fp, "*DIR_TER*=$terr_direccion\n");
                    fputs($fp, "*TER_L*=$terr_nombre\n");
                    fputs($fp, "*NOM_REC*=$nom_recurso\n");
                    fputs($fp, "*EXPEDIENTE*=$expRadi\n");
                    fputs($fp, "*NUM_EXPEDIENTE*=$expRadi\n");
                    fputs($fp, "*DIGNATARIO*=$otro_us1\n");
                    fputs($fp, "*DEPE_CODI*=$dependencia\n");
                    fputs($fp, "*DEPENDENCIA*=$dependencia\n");
                    fputs($fp, "*DEPENDENCIA_NOMBRE*=$dependencia_nombre\n");
                    fputs($fp, "*DEP_NOMB*=$dependencia_nombre\n");

                    for ($i_count = 0; $i_count < count($camposSanc); $i_count++) {
                        fputs($fp, trim($camposSanc[$i_count]) . "=" . trim($datosSanc[$i_count]) . "\n");
                    }

                    for ($i_count = 0; $i_count < count($campos); $i_count++) {
                        fputs($fp, trim($campos[$i_count]) . "=" . trim($datos[$i_count]) . "\n");
                    }
                    fclose($fp);

//El include del servlet hace que se altere el valor de la variable  $estadoTransaccion como 0 si se pudo procesar el documento, -1 de lo
// contrario
                    $estadoTransaccion = -1;

                    if ($ext == "ODT" || $ext == "odt") {
                        //Se incluye la clase que maneja la combinaci�n masiva
                        include ( "$ruta_raiz/radsalida/masiva/OpenDocText.class.php" );
                        exec("mkdir -p " . RUTA_BODEGA . "/tmp/$codusuario/workDir/cacheODT");
                        ///echo "<hr>  mkdir -p ".RUTA_BODEGA."/tmp/$codusuario/cacheODT <hr>";
                        define('WORKDIR', "./bodega/tmp/$codusuario/workDir/");
                        define('CACHE', WORKDIR . 'cacheODT/');

                        //Se abre archivo de insumo para lectura de los datos
                        $fp = fopen("$ruta_raiz/bodega/masiva/$archInsumo", 'r');
                        if ($fp) {
                            $contenidoCSV = file("$ruta_raiz/bodega/masiva/$archInsumo");
                            fclose($fp);
                        } else {
                            echo "<br><b>No hay acceso para crear el archivo $archInsumo <b>";
                            exit();
                        }
                        $accion = false;
                        $odt = new OpenDocText();
                        if ($_SESSION ['usua_debug'] == 1)
                            $odt->setDebugMode(true);
                        else
                            $odt->setDebugMode(false);
                        //Se carga el archivo odt Original
                        $archivoACargar = str_replace('../', '', $linkarchivo);
                        $odt->cargarOdt("$archivoACargar", $nombreArchivo);
                        $odt->setWorkDir(WORKDIR);
                        $accion = $odt->abrirOdt();
                        if (!$accion) {
                            die("<CENTER><table class=borde_tab><tr><td class=titulosError>Problemas en el servidor abriendo archivo ODT para combinaci&oacute;n.</td></tr></table>");
                        }
                        $odt->cargarContenido();

                        //Se recorre el archivo de insumo
                        foreach ($contenidoCSV as $line_num => $line) {
                            if ($line_num > 1) { //Desde la linea 2 hasta el final del archivo de insumo estan los datos de reemplazo
                                $cadaLinea = explode("=", $line);
                                $cadaLinea[1] = str_replace("<", "'", $cadaLinea[1]);
                                $cadaLinea[1] = str_replace(">", "'", $cadaLinea[1]);
                                $cadaVariable[$line_num - 2] = $cadaLinea[0];
                                $cadaValor[$line_num - 2] = $cadaLinea[1];
                            }
                        }
                        $tipoUnitario = '1';
                        if ($vp == "s") {
                            $linkarchivo_grabar = str_replace("bodega/", "", $linkarchivotmp);
                            $linkarchivo_grabar = str_replace("./", "", $linkarchivo_grabar);
                            $odt->setVariable($cadaVariable, $cadaValor);
                            $archivoDefinitivo = $odt->salvarCambios(null, $linkarchivo_grabar, '1', RUTA_BODEGA);
                        } else {
                            $odt->setVariable($cadaVariable, $cadaValor);
                            $odt->salvarCambios(null, $linkarchivo_grabar, '1', RUTA_BODEGA);
                        }
                        $db->conn->CommitTrans();
                        $odt->borrar();
                        echo "<script> function abrirArchivo(url){nombreventana='Documento'; window.open(url, nombreventana,  'status, width=900,height=500,screenX=100,screenY=75,left=50,top=75');return; }</script>
<br><B><CENTER><span class='info'>Combinacion de Correspondencia Realizada <br>";
                        echo "<B><CENTER><a target=\"_blank\" class='vinculos' href='".$url_orfeo_power_tools."/document/linked_document/".$anexo."/download'> Ver Archivo </a><br>";
                    } elseif ($ext == "DOCX" || $ext == "docx") {

//Se incluye la clase que maneja la combinaci?n masiva
                        include ( "$ruta_raiz/radsalida/masiva/ooxml.class.php" );
                        exec("mkdir -p " . RUTA_BODEGA . "/tmp/$codusuario/workDir/cacheODT");
                        //echo "<hr>  mkdir -p ".RUTA_BODEGA."/tmp/$codusuario/cacheODT <hr>";
                        define('WORKDIR', "./bodega/tmp/$codusuario/workDir/");
                        //define ( 'WORKDIR', "./bodega/tmp/workDir/" );
                        define('CACHE', WORKDIR . 'cacheODT/');
                        //Se abre archivo de insumo para lectura de los datos
                        $fp = fopen(RUTA_BODEGA."/masiva/$archInsumo", 'r');

                        if ($fp) {
                            $contenidoCSV = file(RUTA_BODEGA."/masiva/$archInsumo");
                            fclose($fp);
                        } else {
                            echo "<br><b>No hay acceso para crear el archivo $archInsumo <b>";
                            exit();
                        }

                        $accion = false;
                        $docx = new OoXml();
                        if ($_SESSION ['usua_debug'] == 1) {
                            $docx->debug = true;
                            echo RUTA_BODEGA."/masiva/$archInsumo";
                        }
                        else
                            $docx->debug = false;


                        //Se carga el archivo odt Original
                        //$docx->setWorkDir( WORKDIR );
                        $archivoACargar = str_replace('../', '', $linkarchivo);

                        $nombreABorrar = str_replace(".docx", "d.docx", $archivoACargar);
                        $nombreABorrar = str_replace("././", "", $nombreABorrar);
                        //echo shell_exec ("rm -rf $nombreABorrar ");

                        $docx->cargarOdt("$archivoACargar", $nombreArchivo);
                        $docx->cargarOdt("$archivoACargar", $nombreArchivo);
                        $docx->setWorkDir(WORKDIR);
                        $accion = $docx->abrirOdt();
                        if (!$accion) {
                            die("<CENTER><table class=borde_tab><tr><td class=titulosError>Problemas en el servidor abriendo archivo DOCX para combinaci&oacute;n.</td></tr></table>");
                        }
                        $docx->cargarContenido();

//Se recorre el archivo de insumo
                        foreach ($contenidoCSV as $line_num => $line) {
                            if ($line_num > 1) { //Desde la linea 2 hasta el final del archivo de insumo estan los datos de reemplazo
                                $cadaLinea = explode("=", $line);
                                //$cadaLinea[1] = str_replace("<", "'", $cadaLinea[1]);
                                //$cadaLinea[1] = str_replace(">", "'", $cadaLinea[1]);
                                $cadaVariable[$line_num - 2] = $cadaLinea[0];
                                $cadaValor[$line_num - 2] = $cadaLinea[1];
                            }
                        }
                        $tipoUnitario = '1';
                        if ($vp == "s") {
                            $linkarchivo_grabar = str_replace("bodega/", "", $linkarchivotmp);
                            $linkarchivo_grabar = str_replace("./", "", $linkarchivo_grabar);
                            $docx->setVariable($cadaVariable, $cadaValor);
                            $archivoDefinitivo = $docx->salvarCambios(null, $linkarchivo_grabar, '1', RUTA_BODEGA);
                        } else {
                            $docx->setVariable($cadaVariable, $cadaValor);
                            //echo ".docx","d.docx", $linkarchivo_grabar;
                            $linkarchivo_grabar = str_replace(".docx", "d.docx", $linkarchivo_grabar);
                            $docx->salvarCambios(null, $linkarchivo_grabar, '1', RUTA_BODEGA);
                        }
                        $db->conn->CommitTrans();
                        $docx->borrar();
                        /* if($docx->directorio){
                          $cmd_rm = "rm -rf /mnt/bodega/tmp/workDir/cacheODT/". str_replace(".docx", "", trim($docx->nombreOdt)) . "*";
                          //exec ($cmd_rm, $aa);
                          //$cmd_rm = "rm -rf /mnt/bodega/tmp/workDir/". str_replace("d.", "", trim($docx->nombreOdt)) . "";
                          $ggg=explode('/',$linkArchSimple);
                          $cmd_rm = "rm -rf /mnt/bodega/tmp/workDir/". $ggg[count($ggg)-1] . "";
                          exec ($cmd_rm, $aa);
                          //echo "$aa  ->" . $cmd_rm;
                          //print_r(print_r($aa));
                          // echo "rm -rf ./bodega/tmp/workDir/cacheODT/". str_replace(".docx", "", $docx->nombreOdt . "*");
                          } */
                        echo "<script> function abrirArchivo(url){nombreventana='Documento'; window.open(url, nombreventana,  'status, width=900,height=500,screenX=100,screenY=75,left=50,top=75');return; }</script>
<br><B><CENTER><span class='info'>Combinacion de Correspondencia Realizada <br>";
                        echo "<B><CENTER><a target=\"_blank\" class='vinculos' href='".$url_orfeo_power_tools."/document/linked_document/".$anexo."/download'> Ver Archivo </a><br>";
                    } elseif ($ext == "XML" || $ext == "xml") {
                        //Se incluye la clase que maneja la combinacion masiva
                        include ( "$ruta_raiz/include/AdminArchivosXML.class.php" );
                        define('WORKDIR', './bodega/tmp/workDir/');
                        define('CACHE', WORKDIR . 'cacheODT/');

                        //Se abre archivo de insumo para lectura de los datos
                        $fp = fopen("$ruta_raiz/bodega/masiva/$archInsumo", 'r');
                        if ($fp) {
                            $contenidoCSV = file("$ruta_raiz/bodega/masiva/$archInsumo");
                            fclose($fp);
                        } else {
                            echo "<br><b>No hay acceso para crear el archivo $archInsumo <b>";
                            exit();
                        }
                        $accion = false;
                        $xml = new AdminArchivosXML();
                        //Se carga el archivo odt Original
                        $archivoACargar = str_replace('../', '', $linkarchivo);
                        $xml->cargarXML("$archivoACargar", $nombreArchivo);
                        $xml->setWorkDir(WORKDIR);
                        $accion = $xml->abrirXML();
                        $xml->cargarContenido();

                        //Se recorre el archivo de insumo
                        foreach ($contenidoCSV as $line_num => $line) {
                            if ($line_num > 1) { //Desde la linea 2 hasta el final del archivo de insumo estan los datos de reemplazo
                                $cadaLinea = explode("=", $line);
                                $cadaLinea[1] = str_replace("<", "'", $cadaLinea[1]);
                                $cadaLinea[1] = str_replace(">", "'", $cadaLinea[1]);
                                $cadaVariable[$line_num - 2] = $cadaLinea[0];
                                $cadaValor[$line_num - 2] = $cadaLinea[1];
                            }
                        }
                        if ($vp == "s") {
                            $linkarchivo_grabar = str_replace("bodega", "", $linkarchivotmp);
                            $linkarchivo_grabar = str_replace("./", "", $linkarchivo_grabar);
                        }

                        $xml->setVariable($cadaVariable, $cadaValor);
                        $xml->salvarCambios(null, $linkarchivo_grabar);
                        $db->conn->CommitTrans();
                        echo "<script> function abrirArchivo(url){nombreventana='Documento'; window.open(url, nombreventana,  'status, width=900,height=500,screenX=100,screenY=75,left=50,top=75');return; }</script>
<br><B><CENTER><span class='info'>Combinacion de Correspondencia Realizada <br>";
                        echo "<B><CENTER><a target=\"_blank\" class='vinculos' href='".$url_orfeo_power_tools."/document/linked_document/".$anexo."/download'> Ver Archivo </a><br>";
                    } elseif ($extVal == "PDF") {
                        echo "<script> function abrirArchivo(url){nombreventana='Documento'; window.open(url, nombreventana,  'status, width=900,height=500,screenX=100,screenY=75,left=50,top=75');return; }</script>
<br><B><CENTER><span class='info'>Combinacion de Correspondencia Realizada <br>";
                        echo "<B><CENTER><a target=\"_blank\" class='vinculos' href='".$url_orfeo_power_tools."/document/linked_document/".$anexo."/download'> Ver Archivo </a><br>";
                    } else {
                        //echo "<hr>Entrando a COmbinar DOcs<hr> http://$servProcDocs/docgen/servlet/WorkDistributor?accion=1&ambiente=$ambiente&archinsumo=$archInsumo&vp=$vp <hr>";
                        include ("http://$servProcDocs/docgen/servlet/WorkDistributor?accion=1&ambiente=$ambiente&archinsumo=$archInsumo&vp=$vp");

                        if ($estadoTransaccion != 0) {
                            $db->conn->RollbackTrans();
                            $objError = new CombinaError(NO_DEFINIDO);
                            echo ($objError->getMessage());
                            die;
                        }

                        print ("<BR> El estado de la transaccion....$estadoTransaccion");
                        error_reporting(0);
                        $linkarchivo_grabar = $linkarchivo;
                        if (!strrpos($rad_salida, "XXX")) {
                            copy("$ruta_raiz/$linkarchivo", "$ruta_raiz/bodega/masiva/$nombreArchivo.cb");
                            copy("$ruta_raiz/bodega/masiva/$nombreArchivo", "$ruta_raiz/$linkarchivo");
                        }

                        $db->conn->CommitTrans();
                        if (!strrpos($rad_salida, "XXX") && $radObjeto->radicado_codigo($rad_salida))
                            copy("$ruta_raiz/bodega/masiva/$nombreArchivo.cb", "$ruta_raiz/$linkarchivo");
                    }
                    //               $_SESSION['usua_debug'] = 0;
                    ?>
                    </body>
<?php
/* Functionality added to complement papiro cloud online templates module, to fix the
addresse on the document commited based on the addressee selected for the user in the
linked document creation form. Also regenerate the PDF document replacing the variables
available only after the commit of the document */

/* Get ready the curl object to call the papiro cloud restful service */
$papirocloud_api = curl_init();
curl_setopt($papirocloud_api, CURLOPT_RETURNTRANSFER, true);
curl_setopt($papirocloud_api, CURLOPT_URL, $papirocloud_api_url.'/documents/linked_document/commit');
curl_setopt($papirocloud_api, CURLOPT_POST, 1);

/* Prepare the array to send the document info to online templates module */
$data = array(
    'key' => $papirocloud_api_key,
    'attachment_id' => $anexo,
    'user_id' => $_SESSION['krd'],
    );

curl_setopt($papirocloud_api, CURLOPT_POSTFIELDS, $data);

$result = curl_exec($papirocloud_api);

curl_close($papirocloud_api);

?>