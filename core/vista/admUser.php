<?php
session_start(); date_default_timezone_set('America/Bogota'); date_default_timezone_set('America/Bogota');
foreach ($_GET as $key => $valor)  $$key = $valor;
foreach ($_POST as $key => $valor)  $$key = $valor;
$krd = $_SESSION["krd"];
$dependencia = $_SESSION["dependencia"];
$usua_doc = $_SESSION["usua_doc"];
$codusuario = $_SESSION["codusuario"];
$id_rol = $_SESSION["id_rol"];
$ruta_raiz="../..";
include($ruta_raiz.'/core/vista/validadte.php');
//include_once $ruta_raiz.'/core/language/'.$_SESSION ["lang"].'/data.inc';
$scripturl= $ruta_raiz.'/core/vista/operUsuario.php';
?>

<html>
<head>
<link rel="stylesheet" type="text/css" 	href="<?php echo $ruta_raiz?>/js/calendario/calendar.css">
<link rel="stylesheet" href="<?php echo $ruta_raiz?>/estilos/default/orfeo.css">
<script language="JavaScript" src="<?php echo $ruta_raiz?>/js/common.js"></script>
<script language="JavaScript" type="text/javascript"	src="<?php echo $ruta_raiz?>/js/calendario/calendar_eu.js"></script>
<script type="text/javascript">
//
function buscar(){
	var login = document.getElementById('login').value;
	var doc_cc = document.getElementById('documento').value;
	var nombre = document.getElementById('nombres').value;
	if( login.length==0 && doc_cc.length==0 && nombre.length==0 ){
		alert('Debe llenar alguno de los campos login o documento o Nombres y  Apellidos');
		return false;
	}
	/**
  * Modificacion campo numerico
  * @autor Luis Jaime Benavides P.
  * @fecha 2011/11
  */
	else if(isNaN(doc_cc)){
		alert('El campo documento debe ser num\xe9rico');
		return false;
	}
	var poststr = "action=buscar&login="+login+"&numdoc="+doc_cc+"&nombres="+nombre;
	//alert(poststr);
	partes('<?php  echo $scripturl; ?>','listados',poststr,'');
}

function authL(){
	var auth = document.getElementById('aldap').value;
	//alert(auth);
	if(auth==1){
	   document.UsuarioOrfeo.usua_nuevo.disabled = true;
	   document.UsuarioOrfeo.usua_nuevo.value = 0;
	}
	else{
		   document.UsuarioOrfeo.usua_nuevo.disabled = false;

		}
}

function pasardatos(usuaLogin,nombre,estado,email,ldap,fechCrea,unuevo,doc_cc,fechnaci,debugUsua,pcnomb,cod_usuario,tel,externo){
    //alert(usuaLogin+","+nombre+","+estado+","+email+","+ldap+","+fechCrea+","+unuevo+","+doc_cc+","+fechnaci+","+debugUsua+","+pcnomb+",cod"+cod_usuario+","+tel+","+externo);
	var dg=0;
	document.UsuarioOrfeo.login.value = usuaLogin;
        //document.UsuarioOrfeo.ucodi.value = cod_usuario;
        document.UsuarioOrfeo.ucodi2.value = cod_usuario;
	document.UsuarioOrfeo.documento.value = doc_cc;
	document.UsuarioOrfeo.nombres.value = nombre;
	document.UsuarioOrfeo.fecha_busq.value = fechnaci;
	//document.getElementById('fecha_busq').value=fechnaci;
	document.UsuarioOrfeo.email.value = email;
	document.UsuarioOrfeo.pc.value = pcnomb;
	document.UsuarioOrfeo.ext.value = tel;
	document.UsuarioOrfeo.aldap.value = ldap;
	document.UsuarioOrfeo.usua_nuevo.value = unuevo;
	document.UsuarioOrfeo.loginext.value = externo;
	if(debugUsua == 1)
		dg=1;
	document.UsuarioOrfeo.debug.value = debugUsua;
	document.UsuarioOrfeo.estado.value = estado;
	document.UsuarioOrfeo.login.disabled = true;
        document.UsuarioOrfeo.documento.disabled = true;
	authL();
	//nombre,estado,email,ldap,fechCrea,unuevo,doc_cc,fechnaci,debugUsua,pcnomb,cod_usuario,tel,asociar

	vistaFormUnitid('busIns',2);
	vistaFormUnitid('busModi',1);

}
function pressNotKey(evt){
    var charCode=(evt.which) ? evt.which : event.keyCode
    if(charCode==241 || charCode==209)
	return false
    return true;
}
function ejecutar(div,action){
		var cod = document.getElementById('ucodi2').value;
		var login = document.getElementById('login').value;
		var numdoc = document.getElementById('documento').value;
                var loginext = document.getElementById('loginext').value;
		var nombres = document.getElementById('nombres').value;
		var fecha = document.getElementById('fecha_busq').value;
		var email = document.getElementById('email').value;
		var pc = document.getElementById('pc').value;
		var ext = document.getElementById('ext').value;
		var ldap = document.getElementById('aldap').value;
		var nuevo = document.getElementById('usua_nuevo').value;
		//var orfeoScan = document.getElementById('orfeoscan').value;
		var debug = document.getElementById('debug').value;
		var estado = document.getElementById('estado').value;
                if(action==='crear'){
                   var r=confirm('Esta Seguro de Crear el usuario '+login+' ?');
                    if (r==true){}
                    else return false;
                }
                if(action==='mod'){
                    var r=confirm('Esta Seguro de Modificar  el usuario '+login+'?');
                    if (r==true){}
                    else return false;
                }
		var poststr = "action="+action+"&ucodi="+cod+"&login="+login+"&numdoc="+numdoc+"&nombres="+nombres+"&fecha="+fecha+"&email="+email+"&pc="+pc+"&ext="+ext+"&ldap="+ldap+"&nuevo="+nuevo+"&debug="+debug+"&loginext="+loginext+"&estado="+estado;
		var poststrB = "action=buscar&login="+login+"&numdoc="+numdoc+"&nombres="+nombres;
		partes('<?php  echo $scripturl; ?>',div,poststr,'');
		//partes('<?php  echo $scripturl; ?>','listados','accion=poststrB','');
		buscar();
}

</script>
</head>
<body bgcolor="#FFFFFF">
<table class=borde_tab width='100%' cellspacing="2"><tr><td  align='center' class="titulos4">
  Administrador de usuario
</td></tr></table>
<?php include 'admUsertab.php';?>
<form method="post" action="#" name="UsuarioOrfeo"  style="margin:0">
<center>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style='border-collapse: collapse;'>
  <tr><td height="5"></td></tr><tr><td width="5"></td>
    <td width="260" valign="top"><TABLE width="100%" height="424" cellspacing="2" class="borde_tab">
      <TR>
        <TD height="21"  class='titulos2'> Login </td>
        <TD valign="top" align="left" class='listado2'><input type=text id="login" name='login'  class='tex_area' size=20 maxlength="100" onkeypress="return pressNotKey(event);" >
        <input type="hidden" id='ucodi' name='ucodi'  class='tex_area' size=11 maxlength="70" >
        <input type="hidden" id='ucodi2' name='ucodi2'  class='tex_area' size=11 maxlength="70" >
        </td>
      </tr>
      <tr>
        <TD height="26" class='titulos2'> Documento</td>
        <TD height="62" align="left" valign="top" class='listado2'><input type=text id="documento" name='documento'  class='tex_area' size=20 maxlength="14" ></td>
      </tr>
      <tr>
        <TD height="26" class='titulos2'> Nombres y Apellidos</td>
        <TD height="62" align="left" valign="top" class='listado2'><input type=text id="nombres" name='nombres'  class='tex_area' size=20 maxlength="45" ></td>
      </tr>
      <tr>
        <TD height="26" class='titulos2'>Fecha de nacimiento<br></td>
        <TD  align="right" valign="top" class='listado2'>
       <input readonly="true" type="text" class="tex_area" name="fecha_busq"	id="fecha_busq" size="15" value="" />
       <script	language="javascript">
					var A_CALTPL ={'imgpath' : '<?php echo $ruta_raiz ?>/js/calendario/img/',
						'months' : ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
						'weekdays' : ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],'yearscroll': true,	'weekstart': 1,	'centyear'  : 70}
					new tcal ({'formname': 'UsuarioOrfeo','controlname': 'fecha_busq'},A_CALTPL);
			</script>
        </TD>
      </TR>
      <tr>
        <TD height="26" class='titulos2'> E-mail</td>
        <TD height="62" align="left" valign="top" class='listado2'><input type=text id="email" name='email'  class='tex_area' size=20 maxlength="50" ></td>
      </tr>
      <TR>
        <TD height="21"  class='titulos2'> Login Externo</td>
        <TD valign="top" align="left" class='listado2'><input type=text id="loginext" name='loginext'  class='tex_area' size=20 maxlength="30" >
        <input type=HIDDEN id="ucodi" name='ucodi'  class='tex_area' size=11 maxlength="70" >
        </td>
      </tr>
            <tr>
        <TD height="26" class='titulos2'> Nombre computador</td>
        <TD height="62" align="left" valign="top" class='listado2'><input type=text id="pc" name='pc'  class='tex_area' size=11 maxlength="4" ></td>
      </tr>
            <tr>
        <TD height="26" class='titulos2'>Extensi&oacute;n</td>
        <TD height="62" align="left" valign="top" class='listado2'><input type=text id="ext" name='ext'  class='tex_area' size=11 maxlength="15" ></td>
      </tr>
                  <tr>
        <TD height="26" class='titulos2' >Autenticaci&oacute;n</td>
        <TD height="62" align="left" valign="top" class='listado2'>        <select name="aldap" id="aldap" onchange='authL()'>
          <option value="0">Local</option>
          <option value="1">Ldap</option>

        </select></td>
      </tr>
      	<tr>
        <TD height="26" class='titulos2'>Usuario Nuevo</td>
        <TD height="62" align="left" valign="top" class='listado2'>        <select name="usua_nuevo" id="usua_nuevo">
            <option value="0">si</option>
          <option value="1">no</option>

        </select></td>
      </tr>
           <!--            <tr>
        <TD height="26" class='titulos2'>Orfeo Scan</td>
        <TD height="62" align="left" valign="top" class='listado2'>        <select name="orfeoscan" id="orfeoscan">
          <option value="0">no</option>
          <option value="1">si</option>
        </select></td>
      </tr>-->
	<tr>
        <TD height="26" class='titulos2'>Estado</td>
        <TD height="62" align="left" valign="top" class='listado2'>        <select name="estado" id="estado">
          <option value="0">Inactivo</option>
          <option value="1">Activo</option>
        </select></td>
      </tr>
      	<tr>
        <TD height="26" class='titulos2'>Debug</td>
        <TD height="62" align="left" valign="top" class='listado2'>        <select name="debug" id="debug">
          <option value="0">Inactivo</option>
          <option value="1">Activo</option>
        </select></td>
      </tr>
      <tr>
        <td height="26" colspan=2  align="center" class='titulos2'><div id='busIns'>
            <input type="button" name=buscar_serie2 Value='Buscar' onclick='buscar();' class=botones >
            <input type="button" name=insertar_serie2 Value='Insertar' class=botones onClick="ejecutar('actu','crear')" >
            </div>
            <div id='busModi'>
            <input type="button" name=actua_serie2 Value='Modificar' class=botones onClick="ejecutar('actu','mod')" >
            </div>
      </td>
		 </TR>
      <tr>
		        <td height="26" colspan=2  align="center"  class='titulos2'>
            <input type="reset"  name=aceptar2 class=botones id=aceptar onclick="vistaFormUnitid('busIns',1);vistaFormUnitid('busModi',2);document.UsuarioOrfeo.login.disabled = false;document.UsuarioOrfeo.documento.disabled = false;" value='Limpiar'>
		</td>
      </tr>
      <tr><td valign="top" ></td></tr>
    </table></td><td width="5" ></td>

   <td valign="top" >
   <table width="100%"><tr><td>
   <div id='actu' >
   <table class="borde_tab" width="100%"><tbody><tr><td class="listado2"><center><span style="font-size: 16;color: red;font-style: inherit">
     </span></center></td></tr></tbody></table>
   </div></td></tr><tr><td>
    <div id="listados" style="vertical-align : top; padding : 4px; width : 98%; height : 400px; overflow : auto;  " class="borde_tab"><table class="borde_tab" width="100%"></div>
    </td></tr>
    </table>
    </td>
	<td width="5"></td>
  </tr>
</table>


<script language="javascript">
vistaFormUnitid('busModi',2);
//partes('<?php  echo $scripturl ?>','listados','accion=listado','');
</script>

</form>
</body>
</html>
