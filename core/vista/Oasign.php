<?php date_default_timezone_set('America/Bogota');session_start();
foreach ( $_GET as $key => $valor )
	${$key} = $valor;
foreach ( $_POST as $key => $valor )
	${$key} = $valor;
$krd = $_SESSION ["krd"];
$dependencia = $_SESSION ["dependencia"];
$usua_doc = $_SESSION ["usua_doc"];
$codusuario = $_SESSION ["codusuario"];
$id_rol = $_SESSION ["id_rol"];

$accion = $_POST ['action'];
$depe = (isset($_POST ['depe']))?$_POST ['depe']:null;
$ruta_raiz = "../..";
//Cargando variables del log
if(isset($_SERVER['HTTP_X_FORWARD_FOR'])){
    $proxy=$_SERVER['HTTP_X_FORWARD_FOR'];
}else
    $proxy=$_SERVER['REMOTE_ADDR'];
$REMOTE_ADDR=$_SERVER['REMOTE_ADDR'];
include_once "$ruta_raiz/core/clases/log.php";
$log = new log($ruta_raiz);
$log->setAddrC($REMOTE_ADDR);
$log->setProxyAd($proxy);
$log->setUsuaCodi($codusuario);
$log->setDepeCodi($dependencia);
$log->setRolId($id_rol);
$log->setDenomDoc('');
//print_r($_POST);
include_once $ruta_raiz . '/core/clases/urd.php';
include_once $ruta_raiz . '/core/clases/roles.php';
$urd = new urd ( $ruta_raiz );

if ($accion == 'listarU') {
	$Listado = $urd->listaUrd ( $depe );
	//print_r($Listado);
	$numn = count ( $Listado );
	// [depe] => 900 ) Array ( [ROL_NOMB] => [ID_ROL] => [LOGIN] => GIOVANNY MOSQUERA SANCHEZ [USUA_NOMB] => [USUA_CODI
	$resultado = '';
	for($i = 0; $i < $numn; $i ++) {
		$codusua = $Listado [$i] ["USUA_CODI"];
		$usuaNom = $Listado [$i] ["USUA_NOMB"];
		$login = $Listado [$i] ["LOGIN"];
		$rol = $Listado [$i] ["ID_ROL"];
		$usuadoc = $Listado [$i] ["USUA_DOC"];
		$rol_nomb = $Listado [$i] ["ROL_NOMB"];
		if ($rol_nomb) {
			$resultado .= "<tr class='listado2'><td> $login</td> <td>$usuaNom</td> <td>$rol_nomb</td> <td><a href='#' onclick='borrar(\"resl$i\",\"$rol\",\"$codusua\",\"$depe\",\"$usuadoc\");'>Desenlazar</a></td></tr>
		<tr><td colspan='4' align='center'><div id='resl$i'></div></td></tr>";
		}
	}
	echo "<table width='100%'  cellspacing=0 ><tr class='titulos3'><td>Login</td><td>Nombres y  apellidos</td><td>Rol</td><td>Accion</td></tr>$resultado</table>";
} elseif ($accion == 'borrar') {
	$idrol = $_POST ['idrol'];
	$codusua = $_POST ['usuid'];
	$usuadoc = $_POST ['usuadoc'];
	echo "<span style='font-size: 15;color: red;'>";
	echo $urd->eliminarRelacioUrd ( $depe, $idrol, $codusua ,$usuadoc);
	echo "</span>";
		$log->setAction('user_modification');
        $log->setOpera("Eliminada rel. Dep. $depe con rol $idrol y el usuario $codusua");
        $log->setNumDocu('');
        $log->registroEvento();


} elseif ($accion == 'add') {
	echo 'creacion';
	$idrol = $_POST ['idrol'];
	$codusua = $_POST ['usuid'];
	//depe] => 900 [idrol] => 2 [usuid]
	$urd->CrearRelacionUrd ( $depe, $idrol, $codusua );
	$log->setAction('user_modification');
        $log->setOpera("Relacionada Dep. $depe con rol $idrol y el usuario $codusua");
        $log->setNumDocu('');
        $log->registroEvento();
}

elseif ($accion == 'users') {
	//inicializa  urd
	$urd = new urd ( $ruta_raiz );
	$Listado = $urd->CosultaUsuaLibres ();
	//print_r($Listado);
	$numn = count ( $Listado )-1;

	$resultado = '';
	$optionUSU = "";
	for($i = 0; $i < $numn; $i ++) {
		$codusua = $Listado [$i] ["USUA_CODI"];
		$usuaNom = $Listado [$i] ["USUA_NOMB"];
		$login = $Listado [$i] ["LOGIN"];
		if ($login) {
			$optionUSU .= "<option value='$codusua'>$login - $usuaNom</option>";
		}
	}
	?><select id="Iusu" name='Iusu' size='3' class='select'>
      <?php 	echo $optionUSU;
	?>
        </select><?php
}

elseif ($accion == 'rolxdep') {
	//inicializa  urd
	$urd = new urd ( $ruta_raiz );
	$dep = $_POST ['dep'];
	$Listado = $urd->CosultaRolxDep ( $dep );
	//print_r($Listado);
	$numn = count ( $Listado );
	// [depe] => 900 ) Array ( [ROL_NOMB] => [ID_ROL] => [LOGIN] => GIOVANNY MOSQUERA SANCHEZ [USUA_NOMB] => [USUA_CODI
	//$resultado [$i]['IDROL'] = $rs->fields ['SGD_ROL_ID'];
	//$resultado [$i]['ROL_NOMB'] = $rs->fields ['SGD_ROL_NOMB'];
	$resultado = '';
	$optionUSU = "";

	for($i = 0; $i < $numn; $i ++) {
		$codusua = $Listado [$i] ["IDROL"];
		$usuaNom = $Listado [$i] ["ROL_NOMB"];
		if ($usuaNom) {
			$optionUSU .= "<option value='$codusua'>$usuaNom</option>";
		}
	}
	?><select id="Irol" name='Irol'  class='select'>
	<option value='0'>--- rol ---</option>
      <?php 	echo $optionUSU;
	?>
        </select><?php
}

