<?php 
session_start(); date_default_timezone_set('America/Bogota'); date_default_timezone_set('America/Bogota');
if (!$ruta_raiz)
    $ruta_raiz = '../..';
include $ruta_raiz . '/core/config/config-inc.php';
include_once $ruta_raiz . '/core/clases/dependencia.php';
include_once $ruta_raiz . '/core/clases/continente.php';
include($ruta_raiz . '/core/Modulos/radicacion/clases/tipoRadicado.php');
$scriptname = $ruta_raiz . '/core/vista/operDependencia.php';
$depe = new dependencia($ruta_raiz);
$tprad = new tipoRadicado($ruta_raiz);
$pais=new pais($ruta_raiz);
$dependecias = $depe->consultarTodo();
$numndep = count($dependecias);
$optionDepe = "";

for ($i = 0; $i < $numndep; $i++) {
    $nomDepe = $dependecias[$i]["depe_nomb"];
    $codDepe = $dependecias[$i]["depe_codi"];
    $optionDepe.="<option value='$codDepe'>$codDepe - $nomDepe</option>";
}
$ListadoPais=$pais->consultarTodo(1);
$numnPais = count($ListadoPais);
//print_r($ListadoPais);
$optionPaiss = "";
for ($i = 0; $i < $numnPais; $i++) {
    $nomPais = $ListadoPais[$i]["nombre"];
    $codPais = $ListadoPais[$i]["id_pais"];
    $optionPais.="<option value='$codPais'>$nomPais</option>";
}
$tprad = new tipoRadicado($ruta_raiz);
$tparray = $tprad->consultar();
$cad = "perm_tp";
for ($index = 0; $index < count($tparray); $index++) {
    $nombrecampo = $cad . $tparray[$index]['CODIGO'];
    $val.="<tr><td class='titulos2'>" . $tparray[$index]['DESCRIP'] . "</td>
    <td ><select name='$nombrecampo' id='$nombrecampo' class='select'>
    <option value='0' selected>&lt;&lt; Seleccione &gt;&gt;</option>
    $optionDepe</select></td></tr>";
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//ES" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <html>
        <head>
            <title>Orfeo - Administrador de Dependencias.</title>
            <link rel="stylesheet" href="<?php  echo $ruta_raiz ?>/<?php echo $ESTILOS_PATH ?>/orfeo.css" type="text/css">
            <link rel="stylesheet" type="text/css" 	href="<?php echo  $ruta_raiz ?>/js/calendario/calendar.css" rel="stylesheet" type="text/css">
            <script language="JavaScript" src="<?php echo  $ruta_raiz ?>/js/common.js"></script>
            <script type="text/javascript">
                function pasardatos(codigo,deta,fecha,fecha2){
                    document.adm_serie.codserieI2.value = codigo;
                    document.adm_serie.detaserie2.value = deta;
                    document.adm_serie.fecha_busq.value = fecha;
                    document.adm_serie.fecha_busq2.value = fecha2;
                    vistaFormUnitid('busIns',2);
                }
                function modiSerie(div,action){
                    var cod = document.getElementById('codserieI2').value;
                    var deta = document.getElementById('detaserie2').value;
                    var fecha1 = document.getElementById('fecha_busq').value;
                    var fecha2 = document.getElementById('fecha_busq2').value;
                    if(cod.length==0 || deta.length==0 || fecha1.length ==0 || fecha2.length==0){
                        alert('Debe llenar los campos');
                        return false;
                    }
                    var poststr = "action="+action+"&cod="+cod+"&deta="+deta+"&fecha1="+fecha1+"&fecha2="+fecha2; 
                    partes('<?php  echo $scriptname ?>',div,poststr,'');
                    //partes('<?php  echo $scriptname ?>','listadoSERIE','accion=listado','');
                }
            </script>
        <body>
            <form name="formSeleccion" id="formSeleccion" method="post" action="<?php echo  $_SERVER['PHP_SELF'] ?>">
                <table width="100%" border="0" align="center" class="borde_tab">
                    <tr bordercolor="#FFFFFF">
                        <td width="100%" colspan="2" height="40" align="center" class="titulos4"><b>ADMINISTRADOR DE DEPENDENCIAS</b></td>
                    </tr>
                </table>
                <table width="100%" border="0" align="center" class="">
                    <tr ><td>
                            <table width="100%" border="0" align="center" class="borde_tab">
                                <tr >
                                    <td  align="left" class="titulos2"><b>C&oacute;digo.</b></td>
                                    <td  class="listado2"><span class="titulos2"><b>
                                                <input name="txtIdDep" id="txtIdDep" type="text" size="6" maxlength="6" value="<?php echo  $txtIdDep ?>">
                                            </b></span></td>
                                </tr>
                                <tr>

                                    <td class="titulos2"><b>Sigla</b></td>
                                    <td class="listado2"><input name="txtSigla" id="txtSigla" type="text" size="10" maxlength="15" value="<?php echo  $txtSigla ?>"></td>
                                </tr>
                                <tr>
                                    <td class="titulos2"><b>Estado</b></td>
                                    <td class="listado2"><select name="Slc_destado" id="Slc_destado" class="select">
                                            <option value="" selected>&lt; seleccione &gt;</option>
                                            <option value="0" <?php echo  $off ?>>Inactiva</option>
                                            <option value="1" <?php echo  $on ?>>Activa</option>
                                        </select>        </td>
                                </tr>
                                <tr>
                                    <td align="left" class="titulos2"><b>Nombre.</b></td>
                                    <td  class="listado2">
                                        <textarea name="txtModelo" cols="20" rows="2" id="txtModelo" type="text" ><?php echo  $txtModelo ?></textarea></td>
                                </tr>
                                <tr>
                                    <td align="left" class="titulos2"><b>Direcci&oacute;n.</b></td>
                                    <td  class="listado2"><textarea  name="txtDir" cols="20" rows="2" id="txtDir" type="text" ><?php echo  $txtDir ?></textarea></td>
                                </tr>
                                <tr>
                                    <td align="left" class="titulos2"><b>Ubicaci&oacute;n</b></td>
                                    <td  class="listado2"><?php 
// Listamos los continentes.
echo $slc_cont;
?>
                                        <select name="idpais1" id="idpais1" class="select" onChange="cambia(this.form, 'codep_us1', 'idpais1')">
                                            <option value="0" selected>&lt;&lt; Pais  &gt;&gt;</option>
                                            <?php  echo $optionPais;?>
                                        </select>
                                        <select name='codep_us1' id ="codep_us1" class='select' onChange="cambia(this.form, 'muni_us1', 'codep_us1')" >
                                            <option value='0' selected>&lt;&lt; Departamento &gt;&gt;</option>
                                        </select>
                                        <select name='muni_us1' id="muni_us1" class='select'>
                                            <option value='0' selected>&lt;&lt; Municipio &gt;&gt;</option>
                                        </select>        </td>
                                </tr>
                                <tr>
                                    <td align="left" class="titulos2"><b>D. Padre</b></td>
                                    <td  class="listado2"><select name='depec' id="depec" class='select'>
                                            <option value='0' selected>&lt;&lt; Seleccione &gt;&gt;</option>
                                            <?php                                             echo $optionDepe;
                                            ?>
                                        </select>        </td>
                                </tr>
                                <tr>
                                    <td align="left" class="titulos2"><b>Territorial</b></td>
                                    <td  class="listado2"><select name='depec' id="depec" class='select'>
                                            <option value='0' selected>&lt;&lt; Seleccione &gt;&gt;</option>
                                            <?php                                             echo $optionDepe;
                                            ?>
                                        </select>        </td>
                                </tr>
                                <tr>
                                    <?php echo $val; ?>
                                </tr>
                                <tr><td colspan="2"><div align="center">
                                            <input name="btn_accion2" type="submit" class="botones" id="btn_accion2" value="Agregar" onClick="return ValidarInformacion(this.value);" accesskey="A">
                                            <input name="btn_accion3" type="submit" class="botones" id="btn_accion3" value="Modificar" onClick="return ValidarInformacion(this.value);" accesskey="M">
                                        </div></td></tr>
                            </table>
                        </td><td  width="80%" valign="top"><span class="listado2">
                                <div id="listado"></div></td></tr></table>
            </div>


        </div>
        <?php         echo $error_msg;
        ?>

        <script language="javascript" type="text/javascript">
            partes('<?php  echo $scriptname; ?>','listado','accion=listado','');
        </script>
    </form>
</body>
</html>
