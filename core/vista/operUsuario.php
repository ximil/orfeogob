<?php /**
 * Controla las operaciones de la vista de usuario
 * @author Hardy Deimont Niño  Velasquez
 *
 * @version	1.0
 */
session_start();
date_default_timezone_set('America/Bogota');
header('Content-type: text/html; charset=UTF-8');
foreach ($_GET as $key => $valor)
    $$key = $valor;
foreach ($_POST as $key => $valor)
    $$key = $valor;
//print_r($_POST);

function getRealIP() {
    if (!empty($_SERVER['HTTP_CLIENT_IP']))
        return $_SERVER['HTTP_CLIENT_IP'];
    if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
        return $_SERVER['HTTP_X_FORWARDED_FOR'];
    return $_SERVER['REMOTE_ADDR'];
}

$krd = $_SESSION["krd"];
$dependencia = $_SESSION["dependencia"];
$usua_doc = $_SESSION["usua_doc"];
$codusuario = $_SESSION["codusuario"];
$ruta_raiz = "../..";
//Modificacion de Log
include_once "$ruta_raiz/core/clases/log.php";
$log = new log($ruta_raiz);

if (!isset($fecha_busq))
    $fecha_busq = Date('Y-m-d');
if (!isset($fecha_busq2))
    $fecha_busq2 = Date('Y-m-d');

include_once $ruta_raiz . '/core/clases/usuarioOrfeo.php';

$usuario = new usuarioOrfeo($ruta_raiz);
$tabini = '<table class="borde_tab" width="100%"><tbody><tr><td class="listado2"><center><span style="font-size: 16;color: red;font-style: inherit">';
$tabend = '</span></center></td></tr></tbody></table>';
$accion = $_POST['action'];
if ($accion == 'crear') {

    $orfeoScan = $_POST['orfeoScan'];
    $usuario->setUsua_doc($_POST['numdoc']);
    $usuario->setTelefono($_POST['ext']);
    $usuario->setAt($_POST['pc']);
    $usuario->setNombre($_POST['nombres']);
    $usuario->setLogin($_POST['login']);
    $usuario->setOrfeoScan($orfeoScan);
    $usuario->setEmail($_POST['email']);
    $usuario->setNacimiento($_POST['fecha']);
    $usuario->setUsua_nuevo($_POST['nuevo']);
    $usuario->setLdap($_POST['ldap']);
    $usuario->setEstado($_POST['estado']);
    $usuario->setUsua_debug($_POST['debug']);
    $usuario->setUsua_login_externo($_POST['loginext']);

    echo $tabini . $usuario->crear() . $tabend;
    $log->setAddrC($_SERVER['REMOTE_ADDR']);
    $proxy = getRealIP();
    $log->setProxyAd($proxy);
    $log->setUsuaCodi($codusuario);
    $log->setDepeCodi($dependencia);
    $log->setRolId($_SESSION['id_rol']);
    $log->setDenomDoc(' ');
    $log->setAction('user_created');
    $log->setOpera('Creado usuario' . $_POST['login']);
    $log->setNumDocu(' ');
    $log->registroEvento();
} elseif ($accion == 'pass') {
// 	print_r($_POST);
//	print_r($_SESSION);
    if ($_SESSION['ok'] == 1) {
        echo 'Por favor Inicie la session de Orfeo nuevamente';
    } else {
        $usuario = new usuarioOrfeo($ruta_raiz);
//echo $_SESSION['drde'].'=='.substr(md5($_POST['origpass']),1,26);
        if ($_SESSION['drde'] == substr(md5($_POST['origpass']), 1, 26)) {
            $prueba = $usuario->claverepetida($codusuario);
//print_r ($prueba);

            $vacia = $prueba['Error'];
            if ($vacia != '') {
//echo $vacia;
                $fecha = date("Y/m/d H:i:s");
                $ipserver = $_SERVER ['REMOTE_ADDR'];
                $opsys = $_SERVER['HTTP_USER_AGENT'];
                $iplocal = getRealIP();
                $usuario->cambioClave($_SESSION['krd'], $_POST['passwd'], $codusuario, $fecha, $ipserver, $iplocal, $opsys); //$login, $pass, $codusuario, $fecha, $ipserver, $iplocal, $opsys
                echo 'Cambio realizado';
                echo '<br> por favor Inicie la session de Orfeo nuevamente';
                $_SESSION['ok'] = 1;
            } else {
//print_r ($prueba);
                $coincide = '';
                $passprueba = $_POST['passwd'];
//echo $passprueba;
                $claveorigen = substr(md5($passprueba), 1, 26);
//echo $claveorigen;
                foreach ($prueba as $a => $b) {
                    if (!strcmp($b, $claveorigen)) {
                        $coincide = 1;
                    }
//echo '<br>'.$b.' pos '.$coincide;
                }
                if ($coincide == 1) {
                    echo 'La contrase&ntilde;a ya fue utilizada con anterioridad.';
                    echo '<br> Por favor ingrese una nueva';
                    $coincide = '';
                } else {
//echo 'listo okokokokok';
//print_r $b;
                    $fecha = date("Y/m/d H:i:s");
                    $ipserver = $_SERVER ['REMOTE_ADDR'];
                    $opsys = $_SERVER['HTTP_USER_AGENT'];
                    $iplocal = getRealIP();
                    $usuario->cambioClave($_SESSION['krd'], $_POST['passwd'], $codusuario, $fecha, $ipserver, $iplocal, $opsys); //$login, $pass, $codusuario, $fecha, $ipserver, $iplocal, $opsys
                    echo 'Cambio realizado';
                    echo '<br> por favor Inicie la session de Orfeo nuevamente';
                    $_SESSION['ok'] = 1;
                }
            }
        } else {
            echo 'La clave Actual no coincide';
        }
    }
} elseif ($accion == 'mod') {
//
    $usuario->setUsua_codi($_POST['ucodi']);
    /* $usuario->setDescripcion($_POST['deta']);
      $usuario->setFechaFin($_POST['fecha2']);
      $usuario->setFechaInicio($_POST['fecha1']);
      echo $usuario->actualizar(); */

    $orfeoScan = $_POST['orfeoScan'];
    $usuario->setUsua_doc($_POST['numdoc']);
    $usuario->setTelefono($_POST['ext']);
    $usuario->setAt($_POST['pc']);
    $usuario->setNombre($_POST['nombres']);
    $usuario->setLogin($_POST['login']);
    $usuario->setOrfeoScan($orfeoScan);
    $usuario->setEmail($_POST['email']);
    $usuario->setNacimiento($_POST['fecha']);
    $usuario->setUsua_nuevo($_POST['nuevo']);
    $usuario->setLdap($_POST['ldap']);
    $usuario->setEstado($_POST['estado']);
    $usuario->setUsua_debug($_POST['debug']);
    $usuario->setUsua_login_externo($_POST['loginext']);
    echo $tabini . $usuario->actualizar() . $tabend;
    $log->setAddrC($_SERVER['REMOTE_ADDR']);
    $proxy = getRealIP();
    $log->setProxyAd($proxy);
    $log->setUsuaCodi($codusuario);
    $log->setDepeCodi($dependencia);
    $log->setRolId($_SESSION['id_rol']);
    $log->setDenomDoc(' ');
    $log->setAction('document_modification');
    $log->setOpera('Modificacion usuario' . $_POST['login']);
    $log->setNumDocu(' ');
    $log->registroEvento();
} elseif ($accion == 'buscar2') {


//print_r($_POST);
    $usuario->setUsua_doc($_POST['numdoc']);
    $usuario->setNombre($_POST['nombres']);
    $usuario->setLogin($_POST['login']);
    $Listado = $usuario->buscarInfo();
// print_r($Listado);
    $numn = count($Listado);
    ?>

    <TABLE width='100%' cellspacing="2">
        <tr class=tpar>
            <td class='titulos3' align=center>COD </td>
            <td class='titulos3' align=center>LOGIN </td>
            <td class=titulos3 align=center>NOMBRE </td>
            <td class=titulos3 align=center>DOCUMENTO </td>
            <td class=titulos3 align=center>E-MAIL </td>
            <td class=titulos3 align=center>ESTADO </td>
            <td class=titulos3 align=center>AUTH </td>
            <td class=titulos3 align=center>DEBUG </td>
            <td class=titulos3 align=center>DEPENDENCIA</td>
            <td class=titulos3 align=center>ROL</td>
        </tr>
        <?php
        for ($i = 0; $i < $numn; $i++) {
            $usuaLogin = $Listado[$i]['USUA_LOGIN'];
            ;
            $nombre = $Listado[$i]["USUA_NOMB"];
            $estado = $Listado[$i]['USUA_ESTA'];
            $email = $Listado[$i]['USUA_EMAIL'];
            $ldap = $Listado[$i]['LDAP'];
            $fechCrea = $Listado[$i]['USUA_FECH_CREA'];
            $unuevo = $Listado[$i]['USUA_NUEVO'];
            $doc_cc = $Listado[$i]['USUA_DOC'];
            $fechnaci = $Listado[$i]['USUA_NACIM'];
            $debugUsua = $Listado[$i]['USUA_DEBUG'];

            $ucodi = $Listado[$i]['UCODI'];
            $coddepx = $Listado[$i]['DEPCODI'];
            $nombdepex = $Listado[$i]['NOMBDEPE'];
            $idrolx = $Listado[$i]['IDROL'];
            $rolnombx = $Listado[$i]['ROLNOMB'];


            if ($debugUsua == Null || $debugUsua == '')
                $debugUsua = 0;
            $pcnomb = $Listado[$i]['USUA_AT'];
            $cod_usuario = $Listado[$i]['USUA_CODI'];
            $tel = $Listado[$i]['US_TELEFONO1'];
            $asociar = null;
            $da = "'$usuaLogin','$nombre','$estado','$email','$ldap','$fechCrea','$unuevo','$doc_cc','$fechnaci','$debugUsua','$pcnomb','$cod_usuario','$tel','$asociar'";
            $estaData = 'Activo';
            if ($estado == 0)
                $estaData = 'Inactivo';
            $ldapData = 'Ldap';
            if ($ldap == 0)
                $ldapData = 'Local';
            $debugUsuaData = 'Activo';
            if ($debugUsua == 0)
                $debugUsuaData = 'Inactivo';
            ?>
            <tr class=paginacion>
                <td> <?php echo  $ucodi ?></td>
                <td> <?php echo  $usuaLogin ?></td>
                <td align=left> <?php echo  $nombre ?> </td>
                <td> <?php echo  $doc_cc ?> </td>
                <td> <?php echo  $email ?> </td>
                <td> <?php echo  $estaData ?> </td>
                <td> <?php echo  $ldapData ?> </td>
                <td> <?php echo  $debugUsuaData ?> </td>
                <td> <?php echo  $coddepx . " - " . $nombdepex ?></td>
                <td> <?php echo  $rolnombx ?></td>

            </tr>
        <?php } ?>
    </table>
    <?php     //}
}else {


    if ($accion == 'buscar') {
//print_r($_POST);
        $usuario->setUsua_doc($_POST['numdoc']);
        $usuario->setNombre($_POST['nombres']);
        $usuario->setLogin($_POST['login']);
        $Listado = $usuario->buscar();
 //print_r($Listado);
        $numn = count($Listado);
    } else {
        $Listado = $usuario->consultarTodos();

        $numn = count($Listado);
    }
    ?>

    <TABLE width='100%' cellspacing="2">
        <tr class=tpar>
            <td class='titulos3' align=center>LOGIN </td>
            <td class=titulos3 align=center>EDITAR</td>
            <td class=titulos3 align=center>NOMBRE </td>
            <td class='titulos3' align=center>LOGIN EXTERNO</td>
            <td class=titulos3 align=center>DOCUMENTO </td>
            <td class=titulos3 align=center>E-MAIL </td>
            <td class=titulos3 align=center>ESTADO </td>
            <td class=titulos3 align=center>AUTH </td>
            <td class=titulos3 align=center>DEBUG </td>
        </tr>
        <?php for ($i = 0; $i < $numn; $i++) {
            $usuaLogin = $Listado[$i]['USUA_LOGIN'];
            $LoginExt = $Listado[$i]['USUA_LOGIN_EXTERNO'];
            $nombre = $Listado[$i]["USUA_NOMB"];
            $estado = $Listado[$i]['USUA_ESTA'];
            $email = $Listado[$i]['USUA_EMAIL'];
            $ldap = $Listado[$i]['LDAP'];
            $fechCrea = $Listado[$i]['USUA_FECH_CREA'];
            $unuevo = $Listado[$i]['USUA_NUEVO'];
            $doc_cc = $Listado[$i]['USUA_DOC'];
            $fechnaci = $Listado[$i]['USUA_NACIM'];
            $debugUsua = $Listado[$i]['USUA_DEBUG'];
            if ($debugUsua == Null || $debugUsua == '')
                $debugUsua = 0;
            $pcnomb = $Listado[$i]['USUA_AT'];
            $cod_usuario = $Listado[$i]['USUA_CODI'];
            $tel = $Listado[$i]['US_TELEFONO1'];
            $asociar = $Listado[$i]['ASOCIMG'];

            $da = "'$usuaLogin','$nombre','$estado','$email','$ldap','$fechCrea','$unuevo','$doc_cc','$fechnaci','$debugUsua','$pcnomb','$cod_usuario','$tel','$LoginExt'";
            $estaData = 'Activo';
            if ($estado == 0)
                $estaData = 'Inactivo';
            $ldapData = 'Ldap';
            if ($ldap == 0)
                $ldapData = 'Local';
            $debugUsuaData = 'Activo';
            if ($debugUsua == 0)
                $debugUsuaData = 'Inactivo';
            ?>
            <tr class=paginacion>
                <td> <?php echo  $usuaLogin ?></td>
                <td> <a onclick="pasardatos(<?php echo  $da ?>)"><img width="20px" src='<?php echo $ruta_raiz; ?>/imagenes/fleditar.png'/></a></td>
                <td align=left> <?php echo  $nombre ?> </td>
                <td> <?php echo  $LoginExt ?></td>
                <td> <?php echo  $doc_cc ?> </td>
                <td> <?php echo  $email ?> </td>
                <td> <?php echo  $estaData ?> </td>
                <td> <?php echo  $ldapData ?> </td>
                <td> <?php echo  $debugUsuaData ?> </td>

            </tr>
        <?php } ?>
    </table>
    <?php }
//}
