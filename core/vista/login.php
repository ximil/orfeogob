<?php /**
 * OrfeoGPL
 * Es un Software Mantenido por la Fundacion sin Animo de Lucro Correlibre.org [http://correlibre.org]
 * Version 3.8.0
 * Licencia GNU/GPL.
 *
 *
 * FORMULARIO DE LOGIN A ORFEO
 * Aqui se inicia session
 * @PHPSESID		String	Guarda la session del usuario
 * @db 					Objeto  Objeto que guarda la conexion Abierta.
 * @iTpRad				int		Numero de tipos de Radicacion
 * @$tpNumRad	array 	Arreglo que almacena los numeros de tipos de radicacion Existentes
 * @$tpDescRad	array 	Arreglo que almacena la descripcion de tipos de radicacion Existentes
 * @$tpImgRad	array 	Arreglo que almacena los iconos de tipos de radicacion Existentes
 * @numRegs		int		Numero de registros de una consulta
 *
 */
session_start();
date_default_timezone_set('America/Bogota');
header('Content-type: text/html; charset=UTF-8');

/* funcion para validar la dirreccion ip */
function getRealIP() {
    if (!empty($_SERVER['HTTP_CLIENT_IP']))
        return $_SERVER['HTTP_CLIENT_IP'];
    if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
        return $_SERVER['HTTP_X_FORWARDED_FOR'];
    return $_SERVER['REMOTE_ADDR'];
}

if(!empty($_SESSION)){
    $krd = $_SESSION ["krd"];
    $dependencia = (isset($_SESSION["dependencia"]))?$_SESSION["dependencia"]:'';
    $usua_doc = (isset($_SESSION["usua_doc"]))?$_SESSION["usua_doc"]:'';
    $codusuario = $_SESSION ["codusuario"];
    $usua_nuevo=$_SESSION["usua_nuevo"];
}
else{
    $krd = null;
    $dependencia = null;
    $usua_doc = null;
    $codusuario = null;
    $usua_nuevo=null;
}
if(isset($_GET['m'])){
if ($_GET['m'] == 2) {
    $mensajeError2 = "Su sesi&oacute;n ha expirado o ha ingresado en otro equipo";
}
if ($_GET['m'] == 1) {
    $mensajeError2 = "La sesi&oacute;n se ha cerrado correctamente";
}
}
if ($usua_doc && $usua_nuevo == 1) {

    $refererido = 1;
    include ('contenido.php');
    die();
} else {
    if ($usua_nuevo == 0 && $krd) {
        include_once $ruta_raiz . '/core/clases/usuarioOrfeo.php';
        $passllegada = $_POST['contraver'];
        if ($passllegada != '') {
            $usuario = new usuarioOrfeo($ruta_raiz);
            $prueba = $usuario->claverepetida($codusuario);
            $vacia = $prueba['Error'];
            if ($vacia != '') {
                $fecha = date("Y/m/d H:i:s");
                $ipserver = $_SERVER ['REMOTE_ADDR'];
                $opsys = $_SERVER['HTTP_USER_AGENT'];
                $iplocal = getRealIP();
                $usuario->cambioClave($_SESSION['krd'], $passllegada, $codusuario, $fecha, $ipserver, $iplocal, $opsys);
                $mensajeError2 = 'Se Realizo el Cambio de clave correctamente <br>del usuario ' . $_SESSION['krd'];
            } else {
                $coincide = '';
                $passprueba = $passllegada;
                $claveorigen = substr(md5($passprueba), 1, 26);
                foreach ($prueba as $a => $b) {
                    if (!strcmp($b, $claveorigen)) {
                        $coincide = 1;
                    }
                }
                if ($coincide == 1) {
                    ?>
                    <script type='text/javascript' charset='utf8'>alert('La Clave ya fue utilizada con anterioridad. Por favor ingrese una nueva');</script>
                    <?php                     include_once $ruta_raiz . '/core/vista/changepass.php';
                    //$mensajeError2 =:w 'La contrase&ntilde;a ya fue utilizada con anterioridad. <br> Por favor ingrese una nueva';
                    $coincide = '';
                } else {
                    $fecha = date("Y/m/d H:i:s");
                    $ipserver = $_SERVER ['REMOTE_ADDR'];
                    $opsys = $_SERVER['HTTP_USER_AGENT'];
                    $iplocal = getRealIP();
                    $usuario->cambioClave($_SESSION['krd'], $passllegada, $codusuario, $fecha, $ipserver, $iplocal, $opsys); //$login, $pass, $codusuario, $fecha, $ipserver, $iplocal, $opsys
                    $mensajeError2 = 'Se Realizo el Cambio de clave correctamente <br>del usuario ' . $_SESSION['krd'];
                }
            }
        } else {
            include_once $ruta_raiz . '/core/clases/usuarioOrfeo.php';
        }
    }
    session_destroy();
}

if (!$ruta_raiz)
    $ruta_raiz = "../..";
if(!empty($_POST)){
$krd = $_POST ['krd'];
$drd = $_POST ['drd'];
}
include_once $ruta_raiz . '/core/config/config-inc.php';
include_once $ruta_raiz . '/core/language/' . $language . '/data.inc';
if ($krd) {
    include "$ruta_raiz/core/vista/session_orfeo.php";
}
$date = new DateTime();
$current_timestamp = $date->getTimestamp();
?>
<html>
    <head>
        <title>.:: <?php             echo $TITLE;
            ?> ::.</title>

        <link href="<?php echo $ruta_raiz ?>/<?php echo $ESTILOS_PATH ?>/orfeo.css"
              rel="stylesheet" type="text/css">
        <link rel="shortcut icon" href="<?php echo $ruta_raiz ?>/imagenes/ico1.png">
	<style>
	.bordeInicial{
	    <?php
	        /* Verificación de existencia de la imagen de fondo personalizada,
	        en la carpeta orfeo/imagenes/custom, si no existe la imagen personalizada
	        se usa la imagen por defecto de la carpeta orfeo/imagenes/default */
    	    $images_folder = 'custom';
    	    if(!file_exists($ruta_raiz.'/imagenes/'.$images_folder.'/login_background_image.jpg')){
    	        $images_folder = 'default';
    	    }
	    ?>
	    background-image: url("<?php echo $ruta_raiz?>/imagenes/<?= $images_folder ?>/login_background_image.jpg");
	    -webkit-background-size: cover;
  	    -moz-background-size: cover;
            -o-background-size: cover;
  	     background-size: cover;
	}
	.tablasLogin{
      	    background-image: url("./imagenes/fondoazul2.png");
   	}
	</style>
        <script language="JavaScript" type="text/JavaScript">
            function loginTrue()
            {
            document.formulario.submit();
            }

            function loginCorreLibre()
            {	var band;
            if ((document.getElementById('krd').value != '') && (document.getElementById('drd').value!=''))
            {	band = true;	}
            else
            {	band = false;
            alert ('Ingrese datos completos para loguearse');
            }
            return band;
            }
	    flag=true;
	    time = '';
            setInterval(function(){phpJavascriptClock();},1000);
	    recorrido=0;

            function phpJavascriptClock(){
		if(flag){
		    timer = <?php echo $current_timestamp;?>*1000;
		}
		var b = new Date(timer);
		var timeZone = -5.00;
		var tzDifference = timeZone * 60 + b.getTimezoneOffset();
		var d=new Date(b.getTime() + tzDifference * 60 * 1000);
		month_array = new Array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
		dayweek_array= new Array('Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado');
		currentYear = d.getFullYear();
                month = d.getMonth();
		dayofweek=d.getDay();
		var currentDayweek= dayweek_array[dayofweek];
		var currentMonth1 = month_array[month];
		var currentDate = d.getDate();
                //currentDate = currentDate < 10 ? '0'+currentDate : currentDate;
		var hours = d.getHours();
                var minutes = d.getMinutes();
                var seconds = d.getSeconds();
		//var ampm = hours >= 12 ? 'PM' : 'AM';
		//hours = hours % 12;
                //hours = hours ? hours : 12; // the hour ’0' should be ’12'
		hours = hours < 10 ? '0'+hours :hours;
                minutes = minutes < 10 ? '0'+minutes : minutes;
                seconds = seconds < 10 ? '0'+seconds : seconds;
                //var strTime = hours + ':' + minutes+ ':' + seconds + ' ' + ampm;
		var strTime = hours + ':' + minutes+ ':' + seconds;
		document.getElementById("hora").innerHTML= strTime ;
		document.getElementById("fecha").innerHTML=currentDayweek+' '+currentDate+' de '+currentMonth1+' de ' +currentYear;
		flag = false;
                timer = timer + 1000;
	    }
        </script>
        <style type="text/css">
            <!--
            .style10 {
                font-size: 12px
            }

            .style11 {
                color: #FF0000;
                font-weight: bold;
                font-size: 14px;
            }
            .Estilo1 {
                color: #FFFFFF;
                font-family: Arial, Helvetica, sans-serif;
                font-size: 12px;
            }
            -->
        </style>
    </head><body onLoad='document.getElementById("krd").focus();'
                 class="bordeInicial">
        <form name="formulario" action='contenido.php' method='post'><input
                type="hidden" name="orno" value="1">
                <?php //echo $ValidacionKrd;
	    if(isset($ValidacionKrd)){
                if ($ValidacionKrd == "Si") {
                    ?>
                <script>
                    loginTrue();
                </script>
                <?php             }
	    }
            ?>
            <input type="hidden" name="ornot" value="1"></form>
        <form action="<?php echo $ruta_raiz; ?>/index.php" method="post"
              onSubmit="return loginCorreLibre();" name="form33">
            <table width="100%" border="0" valign="top" cellpadding="0"
                   cellspacing="0">
                <!--<tr align="center">
                    <td height="20"></td>
                    <td valign="top"></td>
                    <td></td>
                </tr>-->
                <tr align="center">
                    <td width="10%"></td>
                    <td valign="top">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="center" valign="bottom">
                                    <table width="100%" height="100%"  border="0" cellspacing="0"
                                           cellpadding="0">
                                        <tr align="center" valign="top">
                                            <td width="10%" valign="top">
                                                <table width="100%" border="0" align="right" cellpadding="0"
                                                       cellspacing="10" class="tablasLogin">
                                                    <tr>
                                                        <td align="center" colspan="2">
                                                            <span class='menu_princ style10'>
                                                                <?php echo $entidad_largo?>
                                                            </span>
                                                            <br>
                                                            <?php
                                                            /* Verificación de existencia de la imagen de fondo personalizada,
                                                	        en la carpeta orfeo/imagenes/custom, si no existe la imagen personalizada
                                                	        se usa la imagen por defecto de la carpeta orfeo/imagenes/default */
                                                    	    $images_folder = 'custom';
                                                    	    if(!file_exists($ruta_raiz.'/imagenes/'.$images_folder.'/logo_home.jpg')){
                                                    	        $images_folder = 'default';
                                                    	    }
                                                    	    ?>
                                                            <img
                                                                height="100px"
                                                                src="<?php echo $ruta_raiz ?>/imagenes/<?= $images_folder ?>/logo_home.jpg"
                                                                alt="Logo entidad">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <div align="right" class="menu_princ"><?php                                                                 echo $LOGIN;
                                                                ?></div>
                                                        </td>
                                                        <td align="center"><font size="3"
                                                                                 face="Arial, Helvetica, sans-serif"> <input type='text'
                                                                                 id='krd' name='krd' size='20' class='tex_area'> </font></td>
                                                    </tr>
                                                    <tr align="left">
                                                        <td width="50%" align="center">
                                                            <div align="right" class="menu_princ">
                                                                <?php echo $PASSWD;?></div>
                                                        </td>
                                                        <td width="50%" align="center"><b>
                                                                <font size="3" face="Arial, Helvetica, sans-serif">
                                                                <input type='password' id='drd' name='drd' size='20' class='tex_area'>
                                                                </font></b>
                                                        </td>
                                                    </tr>
                                                    <tr class="trLogin">
                                                        <td colspan="2" align="center">
                                                            <p><input type="hidden" name="tipo_carp" value="0">
                                                                <input type="hidden" name="carpeta" value="0">
                                                                <input type="hidden" name="order" value='radi_nume_radi'>
                                                                <input name="Submit" type="submit" class="botones" value="<?php echo $BOTON; ?>"></p>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="10"></td>
                                            <td align="center" valign='top'>
                                                <table width="100%" height="100%" border="0" cellpadding="0"
                                                       class="tablasLogin"  valign='top'>
                                                    <tr>
                                                        <td align="center" valign='top'>
                                                            <div style="overflow: auto;">
                                                                <span class='leidos' style="text-align:left;text-align:left;color:black;">
                                                                    <div id='fecha' style="float:left;"></div><div id='hora'></div>
                                                                </span>
                                                                <?php                                                                 if (isset($mensajeError2)) {
                                                                    ?><p class="titulosError2"><?php                                                                     echo $mensajeError2;
                                                                    ?><br><?php echo (isset($mensajeError3))?$mensajeError3:''?></p><?php                                                                 }
                                                                ?>
                                                                <table width="100%" height="100%" valign='top' cellpadding="0">
                                                                    <tr><td align='center'><span class="no_leidos"><?php                                                                                 echo $NEWS . '</td></tr><tr><td valign="top">';
                                                                                include $ruta_raiz . '/core/vista/noticias.php';
                                                                                ?> </td></tr></table>
                                                                </span></p>
                                                            </div><!--<span class='leidos' style="text-align:left"><div id='hofe'></div></span>--></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="2"></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td width="10"></td>
                                            <td align="center">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td height="2"></td>
                                        </tr>
                                        <tr>
                                            <td align="center" valign="top">&nbsp;</td>
                                            <td valign="top">
                                            </td>
                                        </tr>
                                    </table>
                                </td>

                            </tr>
                        </table>
                    </td>
                    <td width="10%"></td>
                </tr>
                <tr align="center">
                    <td height="20"></td>
                    <td valign="top"></td>
                    <td></td>
                </tr>
                <tr align="center">
                    <td height="20"></td>
                    <td colspan="1">
                    </td>
                    <td></td>
                </tr>
            </table>
            <br>
            <table width="100%"  >
                <tr>
                    <td align="center"><span class="Estilo1"><?php echo (isset($info))?$info:''?>
                            <br>
                            <?php echo (isset($info2))?$info2:''?><br>
                            <?php echo (isset($info3))?$info3:''?></span></td>
                </tr>
            </table>

        </form>
    </body>
</html>
