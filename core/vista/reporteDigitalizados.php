<?php ini_set('display_errors','On');
date_default_timezone_set('America/Bogota');
$ruta_raiz="../..";
include_once "$ruta_raiz/core/clases/ConnectionHandler.php";
$db=new ConnectionHandler($ruta_raiz);
include_once "$ruta_raiz/core/config/config-inc.php";
include_once "$ruta_raiz/core/vista/correo.php";
if($_SERVER['SERVER_NAME']!='localhost'){
   die("Acceso denegado");
}
$fecha=date('d/m/Y');
$sql="select r.ra_asun asunto,r.radi_nume_radi radicado,r.radi_depe_actu , to_char(r.radi_fech_radi,'YYYY-MM-DD hh24:mi:ss') fecha_rad, u.usua_login||'-'||u.usua_nomb usua_radica,u.usua_codi,u.usua_email from radicado r, usuario u where (r.radi_path is null or trim(radi_path)='') and r.radi_usua_radi=u.usua_codi and r.radi_depe_radi=".DPCORRES." and cast(r.radi_nume_radi as varchar(15)) like '%2' and r.radi_fech_radi between to_timestamp('$fecha 00:00:00','dd/mm/yyyy hh24:mi:ss') and to_timestamp('$fecha 23:59:59','dd/mm/yyyy hh24:mi:ss') order by u.usua_codi,r.radi_fech_radi";
$rs=$db->conn->Execute($sql);
$correo=array();

if(!$rs->EOF){
$codusu=0;
$j=0;
$contenido1="Existen radicados sin imagen que son de su inter&eacute;s:\n<table border=1><tr><th>No.</th><th>Radicado</th><th>Dependencia Actual</th><th>Fecha de radicacion</th><th>Usuario Radicador</th></tr>";
$asunto="ATENCION!! Radicados ORFEO faltantes por imagen del d&iacute;a $fecha";
$footer="</table>\n Gracias por su atenci&oacute;n.<br><br>\n ADMON-ADMINISTRADOR ORFEO<br>Sistema de Gestion Documental";
while(!$rs->EOF){
    $usua_codi=$rs->fields['USUA_CODI'];
    if($codusu!=$usua_codi){
	if($codusu!=0){
	    $m=1;
            do{
                $contenido=preg_replace("/\?/",$m,$contenido,1);
                $m++;
            }while(strpos($contenido,'?')!==false);
	    $correo[$j]['cuerpo']=$contenido.$footer;
	    $correo[$j]['email']=$email;
	    $j++;
	}
	$contenido="Existen radicados sin imagen que son de su inter&eacute;s:\n<table border=1><tr><th>No.</th><th>Radicado</th><th>Dependencia Actual</th><th>Fecha de radicacion</th></tr>";
	$codusu=$usua_codi;
    }
    $email=$rs->fields['USUA_EMAIL'];
    $numrad=$rs->fields['RADICADO'];
    $depe_actu=$rs->fields['RADI_DEPE_ACTU'];
    $usua_radica=$rs->fields['USUA_RADICA'];
    $fecha_rad=$rs->fields['FECHA_RAD'];
    $contenido.="<tr><td>?</td><td>$numrad</td><td>$depe_actu</td><td>$fecha_rad</td></tr>";
    $contenido1.="<tr><td>?</td><td>$numrad</td><td>$depe_actu</td><td>$fecha_rad</td><td>$usua_radica</td></tr>";
    $rs->MoveNext();
}
$m=1;
do{
    $contenido=preg_replace("/\?/",$m,$contenido,1);
    $contenido1=preg_replace("/\?/",$m,$contenido1,1);
    $m++;
}while(strpos($contenido1,'?')!==false);
$correo[$j]['cuerpo']=$contenido.$footer;
$correo[$j]['email']=$email;
for($j=0;$j<count($correo);$j++){
    echo "Asunto:$asunto<br>\n";
    echo "Para: {$correo[$j]['email']}<br>";
    echo $correo[$j]['cuerpo'];
    echo "<br><br><br>";
    if(trim($correo[$j]['email'])!=''){
	mailOrfeo('orfeo@imprenta.gov.co',$correo[$j]['email'],$asunto,$correo[$j]['cuerpo']);
    }
}
$sql="select u.usua_email from usuario u, sgd_urd_usuaroldep urd where urd.rol_codi=1 and urd.depe_codi=".DPCORRES." and urd.usua_codi=u.usua_codi";
$rs=$db->conn->Execute($sql);
$email=$rs->fields['USUA_EMAIL'];
echo "Asunto:$asunto<br>\n";
    echo "Para: $email<br>";
    echo $contenido1.$footer;
    echo "<br><br><br>";
    mailOrfeo('orfeo@imprenta.gov.co',$email,$asunto,$contenido1.$footer);
//mailOrfeo('orfeo@imprenta.gov.co','orfeo@imprenta.gov.co',$asunto,$mensaje);
}
?>
