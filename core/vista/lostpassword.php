<?php error_reporting(E_ALL);
ini_set('display_errors', '5');
function getRealIP() {
    if (!empty($_SERVER['HTTP_CLIENT_IP']))
        return $_SERVER['HTTP_CLIENT_IP'];
    if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
        return $_SERVER['HTTP_X_FORWARDED_FOR'];
    return $_SERVER['REMOTE_ADDR'];
} 
if(isset($_POST['user']) and $_POST['user']!="")

{
	
	$user = $_POST['user'];
	//if (! $ruta_raiz)
	$ruta_raiz = "../..";
	
	include_once $ruta_raiz . '/core/clases/usuarioOrfeo.php';
	//include_once $ruta_raiz . '/core/clases/dependencia.php';
	//include_once $ruta_raiz . '/core/clases/roles.php';
	//include_once $ruta_raiz . '/core/clases/ldapauth.class.php';
	
	$usuario= new usuarioOrfeo($ruta_raiz);
	$usuario->setLogin ( strtoupper ( $user ) );
	$usuario->consultar_usuario ();
	
	$ldapok = $usuario->getLdap ();
	if($usuario->getEstado()==0 )
	{
		$usuario->getNombre();
		$errorMsg= "EL USUARIO SE ENCUENTRA INACTIVO";
		$errorMsg.= "<br><br>Por favor consulte con el administrador del  sistema";
	}
	elseif ($usuario->getLdap () == 1) 
	{ 
		$errorMsg="NO SE PUEDE RECUPERAR CLAVE POR MEDIO DE ORFEO";
		$errorMsg.= "<br><br>Por favor consulte con el administrador del  sistema";
	}
	elseif($usuario->getUsua_nuevo()==0 && $usuario->getLdap() == 0)
	{
		$errorMsg="EL USUARIO ES NUEVO";
		$errorMsg.= "<br><br>Por favor consulte con el administrador del  sistema";
	}
	else
	{
		//$registrationemail = "desarrollador2@davinci.iea.net.co";
		$tempEmail = $usuario->getEmail();
		if($tempEmail!='' || isset($tempEmail))
		{
		//$tempId = $usuario->getUsua_codi();
		$tempId = 'giovannymosquera@gmail.com';
		$random_password = '';
			function makeRandomPassword() 
			{ 
				$salt = "abchefghjkmnpqrstuvwxyz0123456789"; 
				srand((double)microtime()*1000000); 
				$i = 0; 
				$pass = '';
				while ($i < 7) 
				{ 
					$num = rand() % 33; 
					$tmp = substr($salt, $num, 1); 
					$pass = $pass . $tmp; 
					$i++; 
				} 
	
					return $pass; 
	
			}
	
			$random_password = makeRandomPassword();
			//****************************copiar el nuevo pass en la db*******************************************************
			date_default_timezone_set('America/Bogota');
			$fecha = date("Y/m/d H:i:s");
		$ipserver = $_SERVER ['REMOTE_ADDR'];
		$opsys = $_SERVER['HTTP_USER_AGENT'];
		$iplocal = getRealIP();
		$user = strtoupper($user);
		$registrationemail = "desarrollador2@davinci.iea.net.co";
			$subject = "Nueva contraseña para su cuenta en Orfeo"; 	   		
			$charset = "Content-type: text/plain; charset=iso-8859-1\r\n";
			$message = "

su contraseña fue borrada. 

     

Nueva Contraseña: $random_password 

	 	

Orfeo / organización. 



Por su seguridad todas las contraseñas se Encriptan. 

Por esta razón no podemos recuperar su contraseña Perdida.



Este mensaje se genera de manera automatica, Por favor no lo responda!"; 

	

			if(mail($tempEmail, $subject, $message,
			
			"MIME-Version: 1.0\r\n".
			
			$charset.
			
			"From:$registrationemail\r\n".
			
			"Reply-To:$registrationemail\r\n".
			
			"X-Mailer:PHP/" . phpversion() ))
			{
                            $usuario->cambioClave($user, $random_password, $tempId, $fecha, $ipserver, $iplocal, $opsys);
                            header("Location: $ruta_raiz/index.php?from=recoverpass");
                            exit();
			}
			else
			{
                            $Name = "pruebas de correo"; //senders name 
                            $email = "desarrollador2@davinci.iea.net.co "; //senders e-mail adress 
                            $recipient = "giovannymosquera@gmail.com"; //recipient 
                            $mail_body = "The text for the mail..."; //mail body 
                            $subject = "pruebas de correo orfeo"; //subject 
                            $header = "From: ". $Name . " <" . $email . ">\r\n"; //optional headerfields 

                            mail($recipient, $subject, $mail_body, $header); //mail 
				$errorMsg='LA INFORMACIÓN CON SU NUEVA CLAVE NO PUDO SER ENVIADA';
				$errorMsg.= "<br><br>Por favor consulte con el administrador del  sistema";
			}
		
		}
		else
		{
			$errorMsg='EL USUARIO UTILIZADO PRESENTA INCONSISTENCIAS';
			$errorMsg.= "<br><br>Por favor consulte con el administrador del  sistema";
		}
	}
	
} 
else
{
	$errorMsg="Debe especificar un nombre de Usuario";
}

?>

<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Generar Contraseña</title>
<link href="../../estilos/admonstyle.css" type="text/css" rel="stylesheet"/>
<link rel="icon" href="iconos/favicon.ico" type="image/x-icon"/>
</head>

<body>
<div id="contedorlostpal">
<div id="contenedorlost">
<form action="lostpassword.php" method="post" name="form1">
<table id="tbllostpassint">
	<tr>
    	<td colspan="2" id="admhtitle2"><h2>RECUPERAR CONTRASEÑA</h2></td>
    </tr>
    <tr>
    	<td colspan="2" class="space">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="2" id="error_msn"><?php if ( isset($errorMsg) && $errorMsg!=""){ echo $errorMsg; } ?></td>
    </tr>
    <tr>
    	<td colspan="2" class="space">&nbsp;</td>
    </tr>
    <tr>
        <td>Usuario:</td>
		<td><input type="text" id="user" name="user" value="" class="inputtp1"/></td>
    </tr>
    <tr>
    	<td colspan="2" class="space">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="2" id="carreta">La nueva Contraeña será enviada a su cuenta de correo registrada en el sistema.</td>
	</tr>
    <tr>
    	<td colspan="2" class="space">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="2" id="carreta">Para su protección se encriptan las contraseñas antes de almacenarlas, por lo tanto no puede recuperar su contraseña perdida. El sistema generará una nueva para usted. Después de iniciar sesión de nuevo usted podrá cambiarla en el &quot;Perfil&quot; de su cuenta. Gracias.</td>
    </tr>
    <tr>
        <td colspan="2" id="btnindexh"><input type="submit" name="Submit" value="Generar Nueva Contraseña"></td>
    </tr>
    
</table>
</form>
</div>
</div>               
</body>
</html>