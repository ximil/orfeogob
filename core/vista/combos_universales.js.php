<?php if (!isset($ruta_raiz))
    $ruta_raiz = '../..';
include $ruta_raiz . '/core/config/config-inc.php';
include_once $ruta_raiz . '/core/clases/continente.php';
include_once $ruta_raiz . '/core/clases/departamento.php';


/**
 * Consulta de paises
 */
$pais=new pais($ruta_raiz);
$ListadoPais=$pais->consultarTodoData();
$numnPais = count($ListadoPais);
//print_r($ListadoPais);
echo 'vp = new Array();';
for ($i = 0; $i < $numnPais; $i++) {
    $nomPais = $ListadoPais[$i]["nombre"];
    $codPais = $ListadoPais[$i]["id_pais"];
    $codContPais = $ListadoPais[$i]["id_cont"];
echo " vp[$i] = new Array();
    vp[$i]['NOMBRE'] = '$nomPais';
    vp[$i]['ID1'] = $codPais;
    vp[$i]['ID0'] = $codContPais;";
}


/**
 * Consulta de departamento
 */

$depto=new departamento($ruta_raiz);
$ListadoDepartamento=$depto->consultarTodo();
$numndepto = count($ListadoDepartamento);
//print_r($ListadoPais);
echo 'vd = new Array();';
for ($i = 0; $i < $numndepto; $i++) {
    $nomDepto = $ListadoDepartamento[$i]["dpto_nomb"];
    $codDepto = $ListadoDepartamento[$i]["dpto_codi"];
    $codPais = $ListadoDepartamento[$i]["id_pais"];
    $codContPais = $ListadoDepartamento[$i]["id_cont"];
echo "
    vd[$i] = new Array();
    vd[$i]['NOMBRE'] = '$nomDepto';
    vd[$i]['ID1'] = '$codPais-$codDepto';
    vd[$i]['ID0'] = $codPais;";
}

/**
 * Consulta de municipio
 */
$muni=new municipio($ruta_raiz);
$ListadoMuni=$muni->consultarTodoData();
$numnMuni = count($ListadoMuni);
//print_r($ListadoMuni);
echo 'vm = new Array();';
for ($i = 0; $i < $numnMuni; $i++) {
    $nomMunicipio = $ListadoMuni[$i]["muni_nomb"];
    $codMuni = $ListadoMuni[$i]["muni_codi"];
    $codPais = $ListadoMuni[$i]["id_pais"];
    $codDepto = $ListadoMuni[$i]["dpto_codi"];
    $codCont = $ListadoMuni[$i]["id_cont"];
   // print_r($ListadoMuni[$i]);
echo "
      vm[$i] = new Array();
      vm[$i]['NOMBRE'] = '$nomMunicipio';
      vm[$i]['ID0'] = $codDepto;
       vm[$i]['ID'] = $codPais;
      vm[$i]['ID1'] = '$codPais-$codDepto-$codMuni';
      vm[$i]['HOMO_MCPIO'] = null;
      vm[$i]['HOMO_IDMCPIO'] = '';   
    ";
}
//echo $i;
?>
