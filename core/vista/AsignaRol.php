<?php date_default_timezone_set('America/Bogota');session_start(); 
foreach ($_GET as $key => $valor)   ${$key} = $valor;
foreach ($_POST as $key => $valor)   ${$key} = $valor;
$krd = $_SESSION["krd"];
$dependencia = $_SESSION["dependencia"];
$usua_doc = $_SESSION["usua_doc"];
$codusuario = $_SESSION["codusuario"];
$id_rol = $_SESSION["id_rol"];
$ruta_raiz="../..";
include($ruta_raiz.'/core/vista/validadte.php');
include_once $ruta_raiz.'/core/clases/dependencia.php';
include_once $ruta_raiz.'/core/clases/roles.php';
//include_once $ruta_raiz.'/core/clases/urd.php';
$script=$ruta_raiz.'/core/vista/Oasign.php';

//inicializa  dependecias
$depe= new dependencia($ruta_raiz);
$dependecias=$depe->consultarTodo();
$numndep=count($dependecias);
//include_once $ruta_raiz.'/core/language/'.$_SESSION ["lang"].'/data.inc';
$optionDepe="";
 	for ($i = 0; $i < $numndep; $i++) {
  				$nomDepe  =$dependecias[$i]["depe_nomb"];
	  			$codDepe   =$dependecias[$i]["depe_codi"];
$optionDepe.="<option value='$codDepe'>$codDepe - $nomDepe</option>";				
  			}
  			//inicializa  Roles
  			
$rolesl= new roles($ruta_raiz);
$listars=$rolesl->consultarTodo(1);
$numnR=count($listars);
//include_once $ruta_raiz.'/core/language/'.$_SESSION ["lang"].'/data.inc';
//print_r($listars); // [ID_ROL] => 3 [NOMBRE] => Asesor [ESTADO] => 1
$optionrol="";
 	for ($i = 0; $i < $numnR; $i++) {
  				$nomRol  =$listars[$i]["NOMBRE"];
	  			$id_rol   =$listars[$i]["ID_ROL"];
$optionrol.="<option value='$id_rol'>$nomRol</option>";				
  			}			
	
  		?>	
  			
 <html>
<head>
<link rel="stylesheet" type="text/css" 	href="<?php echo $ruta_raiz?>/js/calendario/calendar.css">
<link rel="stylesheet" href="<?php echo $ruta_raiz?>/estilos/default/orfeo.css">
<script language="JavaScript" src="<?php echo $ruta_raiz?>/js/common.js"></script>
<script language="JavaScript" type="text/javascript"	src="<?php echo $ruta_raiz?>/js/calendario/calendar_eu.js"></script>
<script type="text/javascript">

function adicionar(div){
	var depe = document.getElementById('depe').value;
	var usu = document.getElementById('Iusu').value;
	var rol = document.getElementById('Irol').value;
	if(depe==0){
   	alert('Debe seleccionar una dependencia');
   	return false
		}
	if(rol==0){
	   	alert('Debe seleccionar un rol');
	   	return false
			}
	if(usu==0){
	   	alert('Debe seleccionar un usuario');
	   	return false
			}
	var poststr = "action=add&depe="+depe+"&idrol="+rol+"&usuid="+usu; 
	url="<?php  echo $script; ?>";
	partes(url,div,poststr,'');
	ListarUsu();
	usuarios();
	if(rol==1){
		 rolesdep();
	}
}

function usuarios(){
	var divx='userx';
	var poststr = "action=users"; 
	url="<?php  echo $script; ?>";
	partes(url, divx , poststr,'');
} 

function rolesdep(){
	var divx='rolx';
	var dep = document.getElementById('depe').value;
	//alert(dep);
	var poststr = "action=rolxdep&dep="+dep; 
	url="<?php  echo $script; ?>";
	partes(url, divx , poststr,'');
} 

function borrar(div,rol,usu,depe,usuadoc){
	var poststr = "action=borrar&depe="+depe+"&idrol="+rol+"&usuid="+usu+"&usuadoc"+usuadoc; 
	url="<?php  echo $script; ?>";
	partes(url,div,poststr,'');
	window.setTimeout(usuarios(),10000);
	window.setTimeout(rolesdep(),10000);
}

function ListarUsu(){
	var codserie = document.getElementById('depe').value;
	//alert(codserie);
	var poststr = "action=listarU&depe="+codserie; 
	url="<?php  echo $script; ?>";
	partes(url,'data',poststr,'');
	//document.getElementById('xasi').innerHTML = 'Lista de tipos documentales por asignar';
	//document.getElementById('asi').innerHTML = 'Lista de tipos documentales asignados';
}


</script>
</head>
<body bgcolor="#FFFFFF">
<table class=borde_tab width='100%' border="0" cellspacing="01"><tr>
  <td colspan=2	  align='center'  class="titulos41">
  Administrador de Dependencia vs Usuarios</td>
  </tr>
  </table>
  
  <?php include 'admUsertab.php';?>
  
  <table class=borde_tab align='center' width='99%' border="0" cellspacing="01" class='titulos2'><tr class='titulos2'>
  <td  colspan=1 >Dependencias</td>
  <td  colspan=2 width='35%'><span class="titulos2">
    <select  id="depe" name='depe'  class='select' style="width:96%; " onchange='rolesdep();ListarUsu();'  >
      <option value='0'>--- Todas Las Dependecias ---</option>
      <?php echo $optionDepe; ?>
      </select>
  </span>
  </td>
  <td rowspan='2'>Usuarios sin Grupo</td>
   <td rowspan='2'>
 <div id='userx'></div>
 <td rowspan='2'><input type="button" value="Relacionar"  onclick='adicionar("result");' name="Agregar" valign="middle" class="botones"></td>  
</td>
   </tr>
 <tr class='titulos2'>
 <td >Rol</td>
 <td colspan='2'> <div id='rolx'>
  <select  id="Irol" name='Irol'  class='select'   >
      <option value='0'>--- Rol ---</option>
      <?php echo $optionrol; ?>
        </select>
        </div></td> 

 </tr>
  </table>
  
<table width="99%" valign="top" align="center" border="0" cellspacing="0" cellpadding="0">
  <tr><td width="1"></td>
    <td  valign="top" width="100%" > 
    <table  width="100%"><tr><td class='titulos4'> Lista de Usuarios Asignado Rol</td></tr><tr><td>   
  <div id='data' style="vertical-align : top; padding : 4px; width : 99%; height : 390px; overflow : auto;  " class="borde_tab">
 </div>
 </td></tr></table>
 </td>
	<td width="1"></td>
  </tr>
</table>
<script> usuarios();</script>
</body>
</html> 			