<?php 
session_start(); date_default_timezone_set('America/Bogota'); date_default_timezone_set('America/Bogota');
// Modificado SGD 20-Septiembre-2007
/**
  * Paggina Cuerpo.php que muestra el contenido de las Carpetas
	* Creado en la SSPD en el año 2003
  * 
	* Se añadio compatibilidad con variables globales en Off
  * @autor Jairo Losada 2009-05
  * @licencia GNU/GPL V 3
  */

foreach ($_GET as $key => $valor)  $$key = $valor;
foreach ($_POST as $key => $valor)  $$key = $valor;

define('ADODB_ASSOC_CASE', 1);

$krd = $_SESSION["krd"];
$dependencia = $_SESSION["dependencia"];
$usua_doc = $_SESSION["usua_doc"];
$codusuario = $_SESSION["codusuario"];
$tip3Nombre=$_SESSION["tip3Nombre"];
$tip3desc = $_SESSION["tip3desc"];
$tip3img =$_SESSION["tip3img"];

$ruta_raiz = ".";
//include_once "$ruta_raiz/include/db/ConnectionHandler.php";
//$db = new ConnectionHandler($ruta_raiz);	 
//$db->conn->debug = true;
include $ruta_raiz.'/core/config/config-inc.php';

/** Get ready the curl object to call the papiro cloud restful service in order
 * to get the intro text for change password screen from papiro cloud parameters
 */
global $papirocloud_api_key;
global $papirocloud_api_url;

$data = ['key' => $papirocloud_api_key];

$papirocloud_api = curl_init();

curl_setopt($papirocloud_api, CURLOPT_RETURNTRANSFER, true);
curl_setopt($papirocloud_api, CURLOPT_URL, $papirocloud_api_url.'/config/changepass/into_text');
curl_setopt($papirocloud_api, CURLOPT_POST, 1);
curl_setopt($papirocloud_api, CURLOPT_POSTFIELDS, $data);
$changepass_intro_text = curl_exec($papirocloud_api);
curl_close($papirocloud_api);

?>
<!DOCTYPE html>
<html>
<head>
<script src="./js/jquery-1.4.4.min.js" type="text/javascript"></script>
<script src="./js/pschecker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/JavaScript">
function envio()
{
	document.frmchangepass.submit();
}
function DetectoEnter(e)  
{  
    var key;  
    var keychar;  
  
    if(window.event || !e.which) // IE  
    {  
        key = e.keyCode; // para IE  
    }  
    else if(e) // netscape  
    {  
        key = e.which;  
    }  
    else  
    {  
        return true;  
   }  
 
    if (key==13) //Enter  
    {  
      return false;
    }  
}  

$(document).ready(function () {
           
            //Demo code
            $('.password-container').pschecker({ onPasswordValidate: validatePassword, onPasswordMatch: matchPassword });

            var submitbutton = $('.submit-button');
            var errorBox = $('.error');
            errorBox.css('visibility', 'hidden');
			var errorBox3 = $('.error3');
            errorBox3.css('visibility', 'hidden');
            submitbutton.attr("disabled", "disabled");
			
			var btncambio =$('.btndiv');
			btncambio.css('visibility', 'hidden');

            //this function will handle onPasswordValidate callback, which mererly checks the password against minimum length
            function validatePassword(isValid) {
                if (!isValid)
				{
					errorBox.css('visibility', 'visible');
				}
                else
				{
                    errorBox.css('visibility', 'hidden');
					var resultPos = matchPassword();
					if (resultPos = 1)
					{
						/*var resultPass = validatePassword();
						if(resultPass == 2)
						{*/
							submitbutton.addClass('unlocked').removeClass('locked');
							submitbutton.removeAttr("disabled", "disabled");
							/*errorBox.css('visibility', 'hidden');*/
						//}
					}
				}
					
            }
            //this function will be called when both passwords match
            function matchPassword(isMatched) {
                if (isMatched) {
					return 1;
                }
                else {
                    submitbutton.attr("disabled", "disabled");
                    submitbutton.addClass('locked').removeClass('unlocked');
                }
            }
        });
</script>
<link href="./estilos/user_security.css" type="text/css" rel="stylesheet"/>
<title>Cambio de Contrase&ntilde;as</title>
<link rel="stylesheet" href="<?php  echo $ruta_raiz ?>/<?php echo $ESTILOS_PATH ?>/orfeo.css" type="text/css">
</head>
<body class="bordeInicial">
	<center>
		<img src='imagenes/login/logo_orfeo_grande.gif'>
	</center>
	<center><B><font color='white' face='Verdana, Arial, Helvetica, sans-serif' size='4'>CAMBIO DE CONTRASE&Ntilde;A USUARIOS</font> </center><br>
		<center><font face='Verdana, Arial, Helvetica, sans-serif' size='3' color='white'>Por favor introduzca la nueva contrase&ntilde;a</font></center><br>
	<form name='frmchangepass' action='index.php' method='post'>
    <div class="wrapper wtipo1">
        <!--<div class="logo">
            <img src="./core/vista/images/logo.jpg" alt="logo" /></div>-->
		<center><div id='lbluser'>USUARIO: <?php echo $krd; ?></div></center>
        <p>
            <div style="width:100%;"><?= $changepass_intro_text ?> </div>
            <span class="error etipo3">La contrase&ntilde;a debe tener minimo 8 caracteres de longitud.</span>
        </p>
        <div class="password-container containernuevo">
            <p>
                <label>
                    CONTRASE&Ntilde;A:</label>
                <input class="strong-password" type="password" name='contradrd'/>
            </p>
            <p>
                <label>
                    RE-ESCRIBA LA CONTRASE&Ntilde;A:</label>
                <input class="strong-password" type="password" name='contraver'/>
            </p>
			<p>
            <span class="error3 etipo1">Si la contrase&ntilde;a. es debil no se podr&aacute; modificar</span>
			</p>
            <div class="strength-indicator">
                <div class="meter">
                </div>
                Las contrase&ntilde;as seguras contienen de 8 a 16 caracteres, no incluya palabras ni nombres comunes,
                 y combine letras may&uacute;sculas, letras min&uacute;sculas, n&uacute;meros y s&iacute;mbolos.
            </div>
        </div>
    </div>
	<div class='btndiv btndiv1'>
	<p>
		<center><input type='button' class="submit-button locked" value='Aceptar' onkeypress="DetectoEnter()" onclick="envio()"/></center>
    </p>
	</div>
	</form>
	</body>
</html>
<?php die();?>