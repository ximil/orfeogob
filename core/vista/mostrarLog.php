<?php session_start();
/*error_reporting(E_ALL);
ini_set('display_errors', 'On');*/
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
foreach ($_POST as $key => $valor)  $$key = $valor;
$ruta_raiz="../..";
include_once "$ruta_raiz/core/clases/log.php";
$log = new log($ruta_raiz);
include_once "$ruta_raiz/core/clases/conslog.php";
$conslog = new conslog($ruta_raiz);
//Cargando las dependencias de la entidad
include_once $ruta_raiz . '/core/clases/dependencia.php';
//Inicializando las dependencias
$depe = new dependencia($ruta_raiz);
//$depe->setDepe_codi($dependencia);
$dependenciasX= $depe->consultarTodo();
$numdep=  count($dependenciasX);
$optionDepe="";
//Carga en una variable para imprimir las dependencias
for($i=0; $i<$numdep;$i++){
    $nomDepe = $dependenciasX [$i] ['depe_nomb'];
    $codDepe = $dependenciasX [$i] ['depe_codi'];
    $optionDepe .="\t \t \t<option value='$codDepe'>$codDepe - $nomDepe</option>\n";
}
?>
<html>
    <head>
        <title>Visualizaci&oacute;n de log de usuario</title>
    </head>
    <link rel="stylesheet" href="<?php echo $ruta_raiz?>/estilos/caprecom/orfeo.css">
    <script language="JavaScript" src="<?php echo  $ruta_raiz ?>/js/common.js"></script>
    <script type="text/javascript" language="javascript">
function obtiene_http_request(){
    var req=false;
    try{
	    req=new XMLHttpRequest();
	}
    catch(err1){
	    try{
		req= new ActiveXObject("Msxml2.XMLHTTP");	
	    }
	    catch(err2){
	        try{
		    req=new ActiveXObject("Microsoft.XMLHTTP");
	        }
		catch(err3){
		    req=false;
		}
	    }
	}
    return req;
}
var miPeticion= obtiene_http_request();
//***********************
function from(id,ide,url){
    var mialt=parseInt(Math.random()*99999999);
    var vinculo=url+"?id="+id+"&rand="+mialt;
    miPeticion.open("get",vinculo,true);
    miPeticion.onreadystatechange=miPeticion.onreadystatechange=function(){
	if(miPeticion.readyState=4){
	    if(miPeticion.status=200){
	        var http=miPeticion.responseText;
	    	document.getElementById(ide).innerHTML=http;
	    }
	}
    }
    miPeticion.send(null);
}
function limpiar(){
    document.form1.reset();
}
function consDatLog(){
    var depen=document.getElementById('depsel').value;
    var usuaid=document.getElementById('usuasel').value;
    var dbsel=document.getElementById('prueba').value;
    var parameter='accion=ConsLog&depsel='+depen+'&usuasel='+usuaid+'&prueba='+dbsel;
    var getpara='';
    partes('operLog.php','resultado_log',parameter,getpara);
}
    </script>
    <body onLoad="limpiar();">
       <table class=borde_tab width='100%' cellspacing="2">
            <tr>
		<td  align='center' class="titulos4">
	  		Registro de seguimiento del Log 
		</td>
            </tr>
       </table>
       <form action="mostraLog.php" method="post" name="form1">
           <table style="border-collapse: collapse; margin:0 ;  width: 100%; padding: 0 ; border-top: 0; border-spacing: 0">
               <tr class="titulo1">
                    <td colspan="6">Datos de B&uacute;squeda</td>
		</tr>
               <tr class="titulos2">
                   <td>Mes a revisi&oacute;n</td>
                   <td>Dependencia</td>
                   <td>Usuario</td>
                   <td></td>
               </tr>
               <tr class="titulos2">
                    <td><select name='prueba' id="prueba" class="select">
       <?php        $matrix=$log->buscaDB();
       for($i=0;$i<count($matrix);$i++){
            $tmp1=$matrix[$i]['select'];
            $tmp2=$matrix[$i]['ruta'];
            echo "\t\t\t<option value='$tmp2'>$tmp1</option>\n";
       }
       ?>
            </select></td>
            <td><select name="depsel" id="depsel" onChange="from(document.form1.depsel.value,'selUsua','comboUser.php')" id="depsel" size="1" class="select">
                    <option value='0'>Seleccione una Dependencia</option>
            <?php echo $optionDepe;?>
                    <option value="1000">Usuarios sin dependencia</option>
            </select></td>
            <td>
                <div id="selUsua" >
                    <select name="usuasel" id="usuasel" class="select">
                        <option value='0'>Elija un usuario</option>
                    </select>
                </div>
            </td>
            <td><input type="button" name="subinfo" value="Abrir" class="botones" onClick="consDatLog()"></td>
           </tr>
           </table>
       </form>
        <div id="resultado_log" style=" padding : 4px; width :100%; height : 400px; overflow : auto;"></div>
    </body>
</html>
