<?php session_start();
error_reporting(E_ALL);
ini_set('display_errors','On');
date_default_timezone_set('America/Bogota');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$ruta_raiz="../..";
include_once "$ruta_raiz/core/config/config-inc.php";
if($_SERVER['SERVER_NAME']!='localhost'){
   die("Acceso denegado");
}
//Cargar las funciones de alertas
include_once "$ruta_raiz/core/clases/alertas.php";
include_once "$ruta_raiz/core/vista/correo.php";
$alertas= new alertas($ruta_raiz);
//Carga la data de todos los usuarios
$data=$alertas->dataUsua();
$codius=array();
//if(date('w')==1){
$vencidos=$alertas->numVencidos();
if($vencidos['error']==''){
    for($i=0;$i<count($vencidos)-1;$i++){
        $numvencidos=$vencidos[$i]['vencidos'];
        $depsel=$vencidos[$i]['numdep'];
        $depnom=$vencidos[$i]['nomdep'];
        $alertas->setDepeNume($depsel);
        $jefusuacod=$alertas->jefexDep();
        $codius[]=trim($jefusuacod);
        $correo[$jefusuacod]['cuerpo']="Cordial Saludo:<br>A continuaci&oacute;n se detallan los radicados vencidos y/o por vencer en la dependencia $depnom<br><table border=1>";
        $venxdep=$alertas->vencidoxDepe();
        //print_r($venxdep);
        $tmpstr="";
        $tmpua=0;
	//Variable que controla en caso que direccione solo radicados sin tipificar al jefe
	$infjef=0;
        $l=count($venxdep);
	$ctrlusvsjef=1;
	$tmpjefvsus="";
        for($j=0;$j<$l;$j++){
            $tmpcodus=$venxdep[$j]['codus'];
            if(in_array(trim($tmpcodus), $codius)==false){
                $codius[]=trim($tmpcodus);
                if(!isset($correo[$tmpcodus]['cuerpo']))
                    $correo[$tmpcodus]['cuerpo']="Cordial Saludo:<br>\nA continuacion se detallan los radicados que estan vencidos ó proximos a vencer  en la cuenta orfeo <br><table border=1><tr><th>No.</th><th>Radicado</th><th>Asunto</th><th>D&iacute;as</th><th>Estado</th></tr>\n";
                //print_r($codius);
            }
            if($tmpcodus!=$tmpua){
                $nomus=$venxdep[$j]['nomus'];
		    if($jefusuacod!=$tmpcodus){
			$tmpcuerpo="<tr><th colspan='5'>Usuario $nomus</th></tr><tr><th>No.</th><th>Radicado</th><th>Asunto</th><th>D&iacute;as</th><th>Estado</th></tr>";
                        $tmpjefvsus="<tr><th colspan='5'>Usuario $nomus</th></tr><tr><th>No.</th><th>Radicado</th><th>Asunto</th><th>D&iacute;as</th><th>Estado</th></tr>";
			$ctrlusvsjef=0;
		    }
                $tmpua=$tmpcodus;
            }
            $tmprad=$venxdep[$j]['numrad'];
            $diaven=$venxdep[$j]['diasven'];
	    if($diaven>0){
		$par='Proximo a vencer';
	    }
	    else{
		$par='Vencido';
	    }
	    $asunto=substr($venxdep[$j]['asunto'],0,50);
            if($jefusuacod!=$tmpcodus){
                if($venxdep[$j]['trd']!=0){
		    $tmpjefvsus.="<tr><td>?</td><td>$tmprad</td><td>$asunto</td><td>$diaven</td><td>$par</td></tr>\n";
		    $correo[$tmpcodus]['cuerpo'].="<tr><td>?</td><td>$tmprad</td><td>$asunto</td><td>$diaven</td><td>$par</td></tr>\n";
		    $infjef=1;
		    $ctrlusvsjef=1;
		}else{
		    //$correo[$jefusuacod]['cuerpo'].="<tr><td>$m</td><td>$tmprad</td><td>$asunto</td><td>$diaven</td><td>Sin tipificar</td></tr>\n";
                    $correo[$tmpcodus]['cuerpo'].="<tr><td>?</td><td>$tmprad</td><td>$asunto</td><td>$diaven</td><td>Sin tipificar</td></tr>\n";
		}
	    }
            else{
		if($venxdep[$j]['trd']!=0){
                    $tmpstr.="<tr><td>?</td><td>$tmprad</td><td>$asunto</td><td>$diaven</td><td>Vencido</td></tr>\n";
		}else{
		    $tmpstr.="<tr><td>?</td><td>$tmprad</td><td>$asunto</td><td>$diaven</td><td>Sin Tipificar</td></tr>\n";
		}
            }
	    if(isset($venxdep[$j+1]['codus']) && $ctrlusvsjef==1){
		    if($venxdep[$j+1]['codus']!=$venxdep[$j]['codus']){
			$m=1;
			do{
			    $correo[$tmpcodus]['cuerpo']=preg_replace("/\?/",$m,$correo[$tmpcodus]['cuerpo'],1);
			    $tmpjefvsus=preg_replace("/\?/",$m,$tmpjefvsus,1);
			    $m++;
			}while(strpos($correo[$tmpcodus]['cuerpo'],'?')!==false);
			$correo[$jefusuacod]['cuerpo'].=$tmpjefvsus;
		     }
		//$correo[$jefusuacod]['cuerpo']=str_replace($tmpcuerpo,'',$correo[$jefusuacod]['cuerpo']);
	    }
           elseif(!isset($venxdep[$j+1]['codus']) && $ctrlusvsjef==1){
		$m=1;
		do{
                    $correo[$tmpcodus]['cuerpo']=preg_replace("/\?/",$m,$correo[$tmpcodus]['cuerpo'],1);
		    $tmpjefvsus=preg_replace("/\?/",$m,$tmpjefvsus,1);
                    $m++;
                }while(strpos($correo[$tmpcodus]['cuerpo'],'?')!==false);
		$correo[$jefusuacod]['cuerpo'].=$tmpjefvsus;
	   }
	   elseif(!isset($venxdep[$j+1]['codus']) && $ctrlusvsjef==0){
		$m=1;
		do{
                    $correo[$tmpcodus]['cuerpo']=preg_replace("/\?/",$m,$correo[$tmpcodus]['cuerpo'],1);
                    $m++;
                }while(strpos($correo[$tmpcodus]['cuerpo'],'?')!==false);
	   }
	   elseif(!isset($venxdep[$j+1]['codus'])){
		echo "<br>\n";
	   }
	   elseif($venxdep[$j]['codus']!=$venxdep[$j+1]['codus'] && $ctrlusvsjef==0){
		$m=1;
                do{
                    $correo[$tmpcodus]['cuerpo']=preg_replace("/\?/",$m,$correo[$tmpcodus]['cuerpo'],1);
                    $m++;
                }while(strpos($correo[$tmpcodus]['cuerpo'],'?')!==false);
	   }
        }
        if($tmpstr!=""){
	    $m=1;
	    do{
                $tmpstr=preg_replace("/\?/",$m,$tmpstr,1);
                $m++;
            }while(strpos($tmpstr,'?')!==false);
            $correo[$jefusuacod]['cuerpo'].="<tr><th colspan='5'>En su bandeja</th></tr><tr><th>No.</th><th>Radicado</th><th>Asunto</th><th>D&iacute;as</th><th>Estado</th></tr>\n".$tmpstr;
	    $infjef=1;
        }
	//Remueve contenido del jefe si no tiene nada de contenido requerido para enviar
	if($infjef==0){
	    unset($correo[$jefusuacod]);
	    $keybus=array_search($jefusuacod,$codius);
	    array_splice($codius,$keybus,1);
	}
	//$tmpjefvsus="";
    }
}
//}
/*$xvencer=$alertas->numxVencer();
if($xvencer['error']==""){
    for($i=0;$i<count($xvencer)-1;$i++){
        $porvencer=$xvencer[$i]['porvencer'];
        $depsel=$xvencer[$i]['numdep'];
        $alertas->setDepeNume($depsel);
        $jefusuacod=$alertas->jefexDep();
	$existe=1;
        if(in_array(trim($jefusuacod), $codius)==false){
            $codius[]=trim($jefusuacod);
            $correo[$jefusuacod]['cuerpo']="A continuaci&oacute;n se detallan los radicados vencidos y/o por vencer en la dependencia $depnom\n<br><table border='1'>";
	    $existe=0;
        }
        $depnom=$xvencer[$i]['nomdep'];
        $porvencidos=$alertas->xvencerDepe();
        $tmpstr="";
        $tmpua=0;
        for($j=0;$j<count($porvencidos);$j++){
            $tmpcodus=$porvencidos[$j]['codus'];
            if(in_array(trim($tmpcodus), $codius)==false){
                $codius[]=trim($tmpcodus);
                $correo[$tmpcodus]['cuerpo']="<table border=1><tr><th>No.</th><th>Radicado</th><th>Asunto</th><th>D&iacute;as</th><th>Estado</th></tr>\n";
            }
            if($tmpcodus!=$tmpua){
                $nomus=$porvencidos[$j]['nomus'];
                $busqj="<tr><th colspan='5'>Usuario $nomus</th></tr><tr><th>No.</th><th>Radicado</th><th>Asunto</th><th>D&iacute;as</th><th>Estado</th></tr>\n";
		$strjefe="";
		$tmpua=$tmpcodus;
            }
            $tmprad=$porvencidos[$j]['numrad'];
            $diaven=$porvencidos[$j]['diasaven'];
	    $asunto=substr($venxdep[$j]['asunto'],0,30);
            if($jefusuacod!=$tmpcodus){
		if($porvencidos[$j]['trd']!=0){
		    $strjefe.="<tr><td>$m</td><td>$tmprad</td><td>$asunto</td><td>$diaven</td><td>Proximo a vencer</td></tr>\n";
		    $correo[$tmpcodus]['cuerpo'].="<tr><td>$m</td><td>$tmprad</td><td>$asunto</td><td>$diaven</td><td>Proximo a vencer</td></tr>\n";
		}
		else{
		    if(!isset($sinTRD)){
                        $sinTRDJ="<br>Por Vencer sin Aplicar TRD<br>\nRadicados&nbsp;\t&nbsp;Dias por Vencerse<br>\n";
                        $sinTRD="<br>Por Vencer sin Aplicar TRD<br>\n";
                    }
                    $strjefe.="<tr><td>$m</td><td>$tmprad</td><td>$asunto</td><td>$diaven</td><td>Sin tipificar</td></tr>\n";
                    $sinTRDJ.="$tmprad&nbsp;\t&nbsp;$diaven<br>\n";
		}
		if($j+1!=$l){
                    if($tmpcodus!=$venxdep[$j+1]['codus']){
                        $correo[$jefusuacod]['cuerpo'].=$sinTRDJ;
                        $correo[$tmpcodus]['cuerpo'].=$sinTRD;
                        unset($sinTRD);
                        unset($sinTRDJ);
                    }
                }else{
                   $correo[$jefusuacod]['cuerpo'].=$sinTRDJ;
                   $correo[$tmpcodus]['cuerpo'].=$sinTRD;
                   unset($sinTRD);
                   unset($sinTRDJ);
                }
	    }
            else{
		if($porvencidos[$j]['trd']!=0){
		    $tmpstr.="Radicado No. $tmprad vence en $diaven dias<br>\n";
                    $correo[$jefusuacod]['cuerpo'].="$tmprad&nbsp;\t&nbsp;$diaven<br>\n";
                }
                else{
                    if(!isset($sinTRD)){
                        $sinTRDJ="<br>Por Vencer sin Aplicar TRD<br>\nRadicados&nbsp;\t&nbsp;Dias Vencidos<br>\n";
                        $sinTRD="<br>Por Vencer sin Aplicar TRD<br>\n";
                    }
                    $sinTRD.="Radicado No. $tmprad vencido por $diaven dias<br>\n";
                    $sinTRDJ.="$tmprad&nbsp;\t&nbsp;$diaven<br>\n";
                }
                if($j+1!=$l){
                    if($tmpcodus!=$venxdep[$j+1]['codus']){
                        $correo[$jefusuacod]['cuerpo'].=$sinTRDJ;
                        $tmpstr.=$sinTRD;
                        unset($sinTRD);
                        unset($sinTRDJ);
                    }
                }else{
                   $correo[$jefusuacod]['cuerpo'].=$sinTRDJ;
                   $tmpstr.=$sinTRD;
                   unset($sinTRD);
                   unset($sinTRDJ);
                }
            }
        }
        if($tmpstr!="")
            $correo[$jefusuacod]['cuerpo'].="<br>En su bandeja<br>\n".$tmpstr;
    }
}*/
?>
<html>
    <head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    </head>
    <body>
<?php if(isset($correo)){
    //print_r($codius);
    $subject="! ALERTA ! - Documentos que requieren su atención PRIORITARIA";
    $subject2=iconv('utf-8','iso-8859-1',$subject);
    echo "<pre><br>";
    for($i=0;$i<count($codius);$i++){
        $tmpvar=$codius[$i];
        //$alertas->setUsuaCodi($tmpvar);
        //$data=$alertas->dataUsua();
        $nomb=$data[$tmpvar]['usua_nomb'];
        $email=$data[$tmpvar]['email'];
        $to="$nomb &lt;".$email."&gt;";
        //$correodef[$i]="From: $from<br>";
        $correodef[$i]="To: $to<br>";
        $correodef[$i].="Subject: $subject<br>";
        $correodef[$i].=$correo[$tmpvar]['cuerpo']."</table><br><br><br>";
        if($email!='No tiene correo'){
	    echo $correodef[$i];
            mailOrfeo('orfeo@imprenta.gov.co', $email, $subject2, $correo[$tmpvar]['cuerpo']);
	}
    }
    echo "</pre>";
}
?>
    </body>
</html>
