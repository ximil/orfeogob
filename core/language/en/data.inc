﻿<?php
/**
 * Informacion datos de logeo al programa 
 *  document management system
 */
define('LOGIN','LOGIN');
define('PASSWD','PASSWORD');
define('NEWS','NEWS');
define('TITLE','ORFEO, Validation module');
define('ACCESS',"Can't access your account? ");
define('INFO','INFOMATION');
define('PC','Ip   :');
define('ERRORLOGEO','You must type in username and password');
define('BOTON','Sign in');

//**Datos del el menu **//
define('MENU1','Folders');
define('MENU2','Radicación');
define('MENU3','Document');
define('MENU4','File');
define('MENU5','Send');
define('MENU6','Settings');
define('MENU7','Admin');
define('AYUDA','Help');
define('CERRAR','Close');