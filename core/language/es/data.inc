<?php
/**
 * Informacion de datos de logeo 
 * document management system
 */
$LOGIN='USUARIO';
$PASSWD='CONTRASE&Ntilde;A';
$NEWS='Noticias';
$TITLE='ORFEO, M&oacute;dulo de validaci&oacute;n';
$ACCESS='No se puede acceder a su cuenta?';
$INFO='INFORMACI&Oacute;N';
$PC='Ip del Equipo  :';
$ERRORLOGEO='Debe digitar usuario y contrase\xf1a';
$BOTON='INGRESAR';
$MENU1='Carpetas';
$MENU2='Radicación';
$MENU3='Documento';
$MENU4='Archivo';
$MENU5='Envios';
$MENU6='Trd';
$MENU7='Opciones';
$MENU8='Administrar';
$AYUDA='Ayuda';
$CERRAR='Cerrar';