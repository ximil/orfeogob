<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

if (!$ruta_raiz)
    $ruta_raiz = '../../..';

include_once "$ruta_raiz/core/clases/ConnectionHandler.php";

/**
 * Description of modeloEnvios
 *
 * @author derodriguez
 *
 * In this section defines the model of data of Envio
 *
 */
 class modeloEnvio{
     public $link;

     function __construct($ruta_raiz) {

		$db = new ConnectionHandler ( "$ruta_raiz" );
		$db->conn->SetFetchMode ( ADODB_FETCH_NUM );
		$db->conn->SetFetchMode ( ADODB_FETCH_ASSOC );
		$this->link = $db;
                //$this->link->conn->debug=true;
	}
        /*Funcion que consulta en los estados de los documentos o anexos y el estado que tienen después de la radicación
         *
         * @param  numeric $depe_codi, Numero que representa la dependencia del radicado a consultar
         * @param  integer $estado Estado en el cual se encuentra el anexo
         * @return $recordX Arreglo de datos de devoluci&oacute;n de la funci&oacute;n
         *
         */
    function consultarxEstado($depe_codi, $estado){
        $sqlfech = $this->link->conn->SQLDate("d-m-Y H:i A", "r.RADI_FECH_RADI ");
        $isql="select r.radi_nume_radi as nume_radicado, r.radi_nume_deri as radicado_padre, a.anex_ubic, anex_desc, $sqlfech as RADI_FECHA
          , u.usua_login, a.sgd_dir_tipo as copia, substr(cast(sgd_dir_tipo as varchar(3)), 2, 3) as valcop from anexos a, radicado r, usuario u where a.radi_nume_salida = r.radi_nume_radi and a.anex_estado=$estado
          and r.radi_depe_radi = $depe_codi and (r.sgd_eanu_codigo not in(1,2)  or r.sgd_eanu_codigo is null) and (a.sgd_deve_codigo >= 90 or a.sgd_deve_codigo =0
          or a.sgd_deve_codigo is null) and r.radi_usua_radi=usua_codi and a.anex_borrado= 'N' and a.sgd_dir_tipo != 0 and r.radi_nume_radi not in
		  (select radi_nume_sal from sgd_renv_regenvio where radi_nume_sal=r.radi_nume_radi and sgd_renv_planilla='00') and cast(a.radi_nume_salida as varchar(20)) not like '%2' ";
        $rs=$this->link->conn->Execute($isql);
        $i=0;
        $recordX['error']="";
        if(!$rs->EOF){
        while (!$rs->EOF){
            $recordX[$i]['numrad']=$rs->fields['NUME_RADICADO'];
            $recordX[$i]['radicado_padre']=$rs->fields['RADICADO_PADRE'];
            $recordX[$i]['descri_anex']=$rs->fields['ANEX_DESC'];
            $recordX[$i]['fecharadi']=$rs->fields['RADI_FECHA'];
            $recordX[$i]['usua_login']=$rs->fields['USUA_LOGIN'];
            $recordX[$i]['copia']=$rs->fields['COPIA'];
            $recordX[$i]['valcop']=$rs->fields['VALCOP'];
            $i++;
            $rs->MoveNext();
            }
        }
        else{
            //$recordX="<hr><b><font color=red><center>No hay documentos para realizar esta operaci&oacute;n</center></font></b><hr>\n";
            $recordX['error']="<hr><b><font color=red><center>No hay documentos para realizar esta operaci&oacute;n</center></font></b><hr>\n";
        }
            return $recordX;

    }
    /*function consultarxRadicado Funci&oacute;n encargada de mostrar los radicados en un estado del anexo determinado y el
     * n&uacute;mero exacto del radicado.
     *
     * @param numeric $numrad N&uacute;mero exacto del radicado a consultar sin pasar por dependencia
     * @param integer $estado N&uacute;mero que representa el estado del anexo
     * @returm $recordR Arreglo que devuelve el almacenaje de la consulta
     *
     */
function consultarxRadicado($numrad,$estado){
    if(is_array($numrad)){
        $numelem=count($numrad);
        if($numelem>1){
            $i=0;
            $cond_rad="(";
            while($i<$numelem){
                $cond_rad .="cast(r.radi_nume_radi as varchar(18)) like '%".$numrad[$i]."'";
                $i++;
                if($i<$numelem)
                    $cond_rad .=" or ";
            }
            $cond_rad .=")";
        }else{
            $cond_rad="cast(r.radi_nume_radi as varchar(18)) like '%".$numrad[0]."'";
        }
    }else{
        $cond_rad="cast(r.radi_nume_radi as varchar(18)) like '%".$numrad."'";
    }
    $sqlfech = $this->link->conn->SQLDate("d-m-Y H:i A", "r.RADI_FECH_RADI ");
    $isql="select r.radi_nume_radi as nume_radicado, r.radi_nume_deri as radicado_padre, a.anex_ubic, anex_desc, $sqlfech as RADI_FECHA
          , u.usua_login,substr(cast(sgd_dir_tipo as varchar(3)), 2, 3) as valcop, sgd_dir_tipo as copia from anexos a, radicado r, usuario u where a.radi_nume_salida = r.radi_nume_radi and a.anex_estado=$estado
          and $cond_rad and (r.sgd_eanu_codigo not in(1,2)  or r.sgd_eanu_codigo is null) and (a.sgd_deve_codigo <= 90 or a.sgd_deve_codigo >=0
          or a.sgd_deve_codigo is null) and r.radi_usua_radi=usua_codi and a.anex_borrado= 'N' and a.sgd_dir_tipo != 0 and r.radi_nume_radi not in
		  (select radi_nume_sal from sgd_renv_regenvio where radi_nume_sal=r.radi_nume_radi and sgd_renv_planilla='00') and cast(a.radi_nume_salida as varchar(14)) not like '%2'";
        $rs=$this->link->conn->Execute($isql);
        $i=0;
        $recordR['error']="";
        if(!$rs->EOF){
        while (!$rs->EOF){
            $recordR[$i]['numrad']=$rs->fields['NUME_RADICADO'];
            $recordR[$i]['radicado_padre']=$rs->fields['RADICADO_PADRE'];
            $recordR[$i]['descri_anex']=$rs->fields['ANEX_DESC'];
            $recordR[$i]['fecharadi']=$rs->fields['RADI_FECHA'];
            $recordR[$i]['usua_login']=$rs->fields['USUA_LOGIN'];
            $recordR[$i]['copia']=$rs->fields['COPIA'];
            $recordR[$i]['valcop']=$rs->fields['VALCOP'];
            $i++;
            $rs->MoveNext();
            }
        }
        else{
            $recordR['error']="<hr><b><font color=red><center>No hay documentos para realizar esta operaci&oacute;n</center></font></b><hr>\n";
        }
            return $recordR;
}
    /*function accionxEstado Funcion que marca radicados como impresos
     *
     * @param numeric $nume_rad  Numero exacto del radicado para marcar como Impreso Enviado
     * @param integer $estado    Estado del documento anexo
     * @param numeric $depe_codi El n&uacute;mero de la dependencia
     * @param numeric $usua_codi El c&oacute;digo del usuario
     *
     */
    function accionxEstado($nume_rad, $estado,$depe_codi,$usua_codi,$id_rol,$copia){
        //$this->link->conn->StartTrans();
        $estatemp=$estado-1;
        $ssql="SELECT b.RADI_NUME_RADI as RADI_NUME_RADI, a.ANEX_NOMB_ARCHIVO, a.ANEX_DESC, a.SGD_REM_DESTINO, a.SGD_DIR_TIPO
            , a.ANEX_RADI_NUME as ANEX_RADI_NUME, a.RADI_NUME_SALIDA as RADI_NUME_SALIDA FROM ANEXOS a, RADICADO b
            WHERE a.radi_nume_salida=b.radi_nume_radi and ((b.radi_nume_radi = $nume_rad and sgd_dir_tipo = $copia))
            and anex_estado=$estatemp";
        $rsval=  $this->link->conn->Execute($ssql);
        if(!$rsval->EOF){
            $fechasql=  $this->link->conn->OffsetDate(0,  $this->link->conn->sysTimeStamp);
            if($estado==3){
                $isql ="update anexos set anex_estado=$estado, sgd_fech_impres=$fechasql where radi_nume_salida=$nume_rad and
                    sgd_dir_tipo=$copia";
                $rs1 = $this->link ->conn->query($isql);
                /*$isql="INSERT INTO ANEXOS_HISTORICO(ANEX_HIST_COD, ANEX_HIST_RADI_NUME, SGD_TTR_CODIGO, USUA_CODI, DEPE_CODI,
                    ROL_ID, ANEX_HIST_OBSER,FECH_HIST) VALUES (nextval('seq_hist_anex'),'$nume_rad',72, '$usua_codi','$depe_codi',
                        '$id_rol','Este radicado se ha pasado a estado impreso',now())";
                $rs2=  $this->link->conn->query($isql);*/
            }else{
                $isql ="update anexos set anex_estado=$estado, anex_fech_envio=$fechasql where radi_nume_salida=$nume_rad and
                    sgd_dir_tipo=$copia";
                $rs1 = $this->link ->conn->query($isql);
                if(strlen($copia)!=3){
                    $isql = "update RADICADO set SGD_EANU_CODIGO=9 where RADI_NUME_RADI =$nume_rad";
                    $rs2=  $this->link->conn->query($isql);
                }
                /*$isql="INSERT INTO ANEXOS_HISTORICO(ANEX_HIST_COD, ANEX_HIST_RADI_NUME, SGD_TTR_CODIGO, USUA_CODI, DEPE_CODI,
                        ROL_ID, ANEX_HIST_OBSER,FECH_HIST) VALUES (nextval('seq_hist_anex'),'$nume_rad',71, '$usua_codi','$depe_codi',
                        '$id_rol','Este radicado tiene un anexo estado Enviado',now())";
                $rs3= $this->link ->conn->query($isql);*/
            }
            $status="ok";
        }else{
            $status="error";
        }
        //$this->link->conn->RollbackTrans();
        return $status;
    }
    /*Funcion que consulta la informaci&oacute;n total del radicado para mostrar en pantalla para operaci&acute;n envio e impresi&oacute;n
     *
     * @param numeric $numrad N&uacute;mero del radicado
     *
     * @return array Arreglo con la informaci&oacute;n
     *
     */
    function infoxAccion($numrad, $copia){
        $nsql="Select b.Radi_Nume_Radi, b.Radi_Nume_Deri, D.Sgd_Dir_Nomremdes as destinatario, D.Sgd_Dir_Direccion, p.nombre_pais
            , De.Dpto_Nomb, M.Muni_Nomb, sgd_dir_tipo as copia
            , b.Ra_Asun, b.radi_desc_anex From Sgd_Dir_Drecciones D, radicado b, departamento de, municipio m, sgd_def_paises p
            Where b.Radi_Nume_Radi = D.Radi_Nume_Radi And D.Dpto_Codi = De.Dpto_Codi And M.Muni_Codi = D.Muni_Codi 
            and m.id_pais = p.id_pais
            and de.id_pais = p.id_pais
            And M.Dpto_Codi = De.Dpto_Codi And P.Id_Pais = D.Id_Pais and ((b.radi_nume_radi = $numrad and sgd_dir_tipo = $copia))
            order by b.Radi_Nume_Radi, sgd_dir_tipo";

        $rs4=  $this->link->conn->Execute($nsql);
        $recordIA['numrad']=$rs4->fields['RADI_NUME_RADI'];
        $recordIA['radi_padre']=$rs4->fields['RADI_NUME_DERI'];
        $recordIA['destinatario']= $rs4->fields['DESTINATARIO'];
        $recordIA['direccion']=$rs4->fields['SGD_DIR_DIRECCION'];
        $recordIA['pais']=$rs4->fields['NOMBRE_PAIS'];
        $recordIA['depto']=$rs4->fields['DPTO_NOMB'];
        $recordIA['municipio']=$rs4->fields['MUNI_NOMB'];
        $recordIA['copia']=$rs4->fields['COPIA'];
        $recordIA['asunto']=$rs4->fields['RA_ASUN'];
        $recordIA['radi_desc']=$rs4->fields['RADI_DESC_ANEX'];
        return $recordIA;
    }
    /*
     *
     *
     *
     *
     */

    function infoxModif($numrad,$copia){
        $nsql="Select re.Radi_Nume_sal numrad, ra.radi_nume_deri ,re.sgd_renv_nombre as destinatario , re.sgd_renv_dir direccion
            , re.sgd_renv_pais as nombre_pais, re.sgd_renv_depto as dpto_nomb, sgd_renv_mpio muni_nomb, re.sgd_dir_tipo as copia
            ,ra.Ra_Asun, re.sgd_renv_observa From sgd_renv_regenvio re, radicado ra Where ra.Radi_Nume_Radi = re.radi_nume_sal and
            ((ra.radi_nume_radi = $numrad and re.sgd_dir_tipo=$copia)) order by re.Radi_Nume_sal, re.sgd_dir_tipo";
        $rs4=  $this->link->conn->Execute($nsql);
        $recordIM['numrad']=$rs4->fields['NUMRAD'];
        $recordIM['radi_padre']=$rs4->fields['RADI_NUME_DERI'];
        $recordIM['destinatario']= $rs4->fields['DESTINATARIO'];
        $recordIM['direccion']=$rs4->fields['DIRECCION'];
        $recordIM['pais']=$rs4->fields['NOMBRE_PAIS'];
        $recordIM['depto']=$rs4->fields['DPTO_NOMB'];
        $recordIM['municipio']=$rs4->fields['MUNI_NOMB'];
        $recordIM['copia']=$rs4->fields['COPIA'];
        $recordIM['asunto']=$rs4->fields['RA_ASUN'];
        $recordIM['radi_desc']=$rs4->fields['SGD_RENV_OBSERVA'];
        return $recordIM;
    }

    /*Funcion que consulta el medio de envio disponibles para la aplicaci&oacute;n
     *
     * @return array $recordME Arreglo que devuelve la consulta los datos del medio de envio.
     *
     */
    function consultarMedioEnvio(){
        $isql="select SGD_FENV_DESCRIP as NOMBC_MENVIO
            , SGD_FENV_CODIGO from SGD_FENV_FRMENVIO WHERE SGD_FENV_ESTADO=1 AND SGD_FENV_PLANILLA=1
            order by SGD_FENV_CODIGO";
        $rs=$this->link->conn->Execute($isql);
        $i=0;
        //print_r($rs2);
        if(!$rs->EOF){
            while(!$rs->EOF){
                $recordME[$i]['medenvio']=$rs->fields['NOMBC_MENVIO'];
                $recordME[$i]['codenvio']=$rs->fields['SGD_FENV_CODIGO'];
                $i++;
                $rs->MoveNext();
            }
        }
        else{
            $recordME="No hay datos en la consulta";
        }

        return $recordME;
    }
    /*Esta funci&oacute;n
     * @param numeric $num_rad El n&uacute;mero del radicado
     * @param numeric $codus   El n&uacute;mero del c&oacute;digo del usuario
     * @param numeric $medxenv El id del medio de envio
     * @param string  $descnenv La descripcion del envio
     * @param int $pesoenv     Tiene el peso del envio calculado
     *
     */
    function generarEnvio($num_rad, $codus, $depus, $medxenv,$descnenv,$pesoenv,$copia){
        //$this->link->conn->StartTrans();
        $isql1="SELECT r.radi_pais, r.muni_codi, r.dpto_codi, radi_nume_radi as radicado_padre
            FROM RADICADO r, usuario u, sgd_urd_usuaroldep urd, sgd_mod_modules mod
            , sgd_drm_dep_mod_rol drm WHERE r.RADI_NUME_RADI=$num_rad and u.usua_codi = urd.usua_codi and
            r.RADI_USUA_ACTU= u.USUA_CODI AND mod.sgd_mod_id = drm.sgd_drm_modecodi AND drm.sgd_drm_rolcodi = urd.rol_codi AND
            drm.sgd_drm_depecodi = urd.depe_codi and r.RADI_DEPE_ACTU= urd.depe_codi AND mod.sgd_mod_modulo = 'codi_nivel' ";
        $rs1=  $this->link->conn->Execute($isql1);
            $radicado_padre=$rs1->fields['RADICADO_PADRE'];
            if($radicado_padre==0)
                $radicado_padre=$num_rad;
        $isql2="Select sgd_dir_tipo as dir_tipo, d.sgd_dir_codigo ,D.Sgd_Dir_Nomremdes as destinatario, D.Sgd_Dir_Direccion as direccion, p.nombre_pais
            , De.Dpto_Nomb, M.Muni_Nomb, sgd_dir_telefono as telefono, sgd_dir_mail as email From Sgd_Dir_Drecciones D, departamento de, municipio m, sgd_def_paises p
            , sgd_def_continentes c Where D.Dpto_Codi = De.Dpto_Codi And M.Muni_Codi = D.Muni_Codi 
            And de.id_pais = p.id_pais
            and m.id_pais = p.id_pais
            And M.Dpto_Codi = De.Dpto_Codi And P.Id_Pais = D.Id_Pais and d.radi_nume_radi = $num_rad and m.id_cont=c.id_cont and de.id_cont=c.id_cont and c.id_cont=p.id_cont
            and sgd_dir_tipo=$copia";

        $rs2=  $this->link->conn->Execute($isql2);
        $dir_tipo=$rs2->fields['DIR_TIPO'];
        $sgd_dir_codigo=$rs2->fields['SGD_DIR_CODIGO'];
        $direccion=$rs2->fields['DIRECCION'];
        $telefono=$rs2->fields['TELEFONO'];
        $email=$rs2->fields['EMAIL'];
        $nomb_destinatario=$rs2->fields['DESTINATARIO'];
        $dpto_nomb=$rs2->fields['DPTO_NOMB'];
        $muni_nomb=$rs2->fields['MUNI_NOMB'];
        $nombre_pais=$rs2->fields['NOMBRE_PAIS'];
        $isql6="select usua_doc from usuario where usua_codi=$codus";
        $rs6=  $this->link->conn->Execute($isql6);
        $usua_doc=$rs6->fields['USUA_DOC'];
        $fechasql=  $this->link->conn->OffsetDate(0,  $this->link->conn->sysTimeStamp);
        $isql9="select max(sgd_renv_codigo) as nextval from sgd_renv_regenvio order by sgd_renv_codigo desc";
        $rs9=  $this->link->conn->Execute($isql9);
		if(!$rs9->EOF){
			$nextval=$rs9->fields['NEXTVAL'];
			$nextval=$nextval+1;
		}else{
			$nextval=1;
                }
        $isql10="INSERT INTO SGD_RENV_REGENVIO(USUA_DOC , SGD_RENV_CODIGO , SGD_FENV_CODIGO , SGD_RENV_FECH , RADI_NUME_SAL
            , SGD_RENV_DESTINO , SGD_RENV_TELEFONO , SGD_RENV_TEL , SGD_RENV_MAIL , SGD_RENV_PESO , SGD_RENV_VALOR
            , SGD_RENV_CERTIFICADO , SGD_RENV_ESTADO , SGD_RENV_NOMBRE , SGD_DIR_CODIGO , DEPE_CODI , SGD_DIR_TIPO
            , RADI_NUME_GRUPO , SGD_RENV_PLANILLA , SGD_RENV_DIR , SGD_RENV_DEPTO, SGD_RENV_MPIO, SGD_RENV_PAIS
            , SGD_RENV_OBSERVA , SGD_RENV_CANTIDAD) VALUES('$usua_doc' , $nextval , $medxenv , $fechasql
            , $num_rad, '$muni_nomb', '$telefono', '$telefono', '$email', '$pesoenv', '0', 0, 1, '$nomb_destinatario'
            , $sgd_dir_codigo, $depus, $dir_tipo, '$radicado_padre', ' ', '$direccion' , '$dpto_nomb' , '$muni_nomb', '$nombre_pais'
            , '$descnenv', 1 )";
        $rs10=  $this->link->conn->query($isql10);
        //$this->link->conn->RollbackTrans();
    }
    /*@function modificarEnvio
     *
     * Esta funci&oacute;n modifica un envio generado previamente modificandole la descripci&oacute;n y el peso del envio
     * original
     *
     * @param numeric $codenvio    El c&oacute;digo identificador del envio
     * @param string  $descripcion La descripcion del envio por usuario
     * @param numeric $medxenvio   El identificador del medio de envio usado
     *
     */

    function modificarEnvio($copia,$numrad,$descripcion,$medxenvio,$peso){
        //$this->link->conn->StartTrans();
        $isql="update sgd_renv_regenvio set sgd_fenv_codigo=$medxenvio, sgd_renv_observa='$descripcion',
                   sgd_renv_peso=$peso where radi_nume_sal=$numrad and sgd_dir_tipo=$copia";
        $rs=  $this->link->conn->query($isql);
        //$this->link->conn->RollbackTrans();
    }

    function consultaEModificable($depus){
        date_default_timezone_set("America/Bogota");
        $ano_ini = date("Y");
        $mes_ini = substr("00".(date("m")-1),-2);
        if ($mes_ini==0) {$ano_ini=$ano_ini-1; $mes_ini="12";}
        $dia_ini = date("d");
        $fecha_ini = "$ano_ini/$mes_ini/$dia_ini";
        $fecha_fin = date("Y/m/d") ;
        $fecha_ini = mktime(00,00,00,substr($fecha_ini,5,2),substr($fecha_ini,8,2),substr($fecha_ini,0,4));
	$fecha_fin = mktime(23,59,59,substr($fecha_fin,5,2),substr($fecha_fin,8,2),substr($fecha_fin,0,4));
        $where_fecha = " (a.SGD_RENV_FECH >= ". $this->link->conn->DBTimeStamp($fecha_ini) ." and a.SGD_RENV_FECH <= ". $this->link->conn->DBTimeStamp($fecha_fin).") " ;
        $sqlChar = $this->link->conn->SQLDate("d-m-Y H:i A","SGD_RENV_FECH");
        $ssql="select a.radi_nume_sal AS \"RADICADO\" , c.RADI_NUME_DERI AS \"RADICADO_PADRE\"
            , $sqlChar AS \"FECHA_ENVIO\" , a.sgd_renv_planilla AS \"PLANILLA\"
            , a.sgd_renv_nombre AS \"DESTINATARIO\" , a.sgd_renv_dir AS \"DIRECCION\" , a.SGD_RENV_PAIS AS \"PAIS\"
            , a.sgd_renv_depto AS \"DEPARTAMENTO\" , a.sgd_renv_mpio AS \"MUNICIPIO\" , b.sgd_fenv_descrip AS \"MEDIO_ENVIO\"
            , d.USUA_LOGIN AS \"USUA_ACTU\" , a.sgd_renv_valor AS \"VALORENVIO\", a.sgd_renv_peso as peso, a.sgd_fenv_codigo as codmenvio
            , a.sgd_dir_tipo as copia, substr(cast(a.sgd_dir_tipo as varchar(3)), 2, 3) as valcop from sgd_renv_regenvio a
            LEFT OUTER JOIN sgd_fenv_frmenvio b ON a.sgd_fenv_codigo=b.sgd_fenv_codigo, radicado c, usuario d
            where $where_fecha and substr(cast(a.radi_nume_sal as varchar(18)), 5, 3)='$depus' and a.sgd_renv_estado < 8
                and c.radi_nume_radi= a.radi_nume_sal and c.radi_usua_actu=d.usua_codi and a.sgd_renv_planilla <> '00' and sgd_renv_nombre<>'Varios'
		order by 5";
        $rs=  $this->link->conn->Execute($ssql);
        $recordEM['error']="";
        $i=0;
        if(!$rs->EOF){
            while(!$rs->EOF){
                $recordEM[$i]['numrad']=$rs->fields['RADICADO'];
                $recordEM[$i]['radi_padre']=$rs->fields['RADICADO_PADRE'];
                $recordEM[$i]['fecha_envio']=$rs->fields['FECHA_ENVIO'];
                $recordEM[$i]['planilla']=$rs->fields['PLANILLA'];
                $recordEM[$i]['destinatario']=$rs->fields['DESTINATARIO'];
                $recordEM[$i]['direccion']=$rs->fields['DIRECCION'];
                $recordEM[$i]['pais']=$rs->fields['PAIS'];
                $recordEM[$i]['departamento']=$rs->fields['DEPARTAMENTO'];
                $recordEM[$i]['municipio']=$rs->fields['MUNICIPIO'];
                $recordEM[$i]['medio_envio']=$rs->fields['MEDIO_ENVIO'];
                $recordEM[$i]['usua_actu']=$rs->fields['USUA_ACTU'];
                $recordEM[$i]['valorenvio']=$rs->fields['VALORENVIO'];
                $recordEM[$i]['peso']=$rs->fields['PESO'];
                $recordEM[$i]['codmenvio']=$rs->fields['CODMENVIO'];
                $recordEM[$i]['copia']=$rs->fields['COPIA'];
                $recordEM[$i]['valcop']=$rs->fields['VALCOP'];
                $rs->MoveNext();
                $i++;
            }
        }else{
            $recordEM['error']="<hr><b><font color=red><center>No hay documentos para realizar esta operaci&oacute;n</center></font></b><hr>\n";
        }
        return $recordEM;
    }

    function consultaRModificable($numrad){
        if(is_array($numrad)){
        $numelem=count($numrad);
            if($numelem>1){
                $i=0;
                $cond_rad="(";
                while($i<$numelem){
                    $cond_rad .="cast(a.radi_nume_sal as char(18)) like '%".$numrad[$i]."%'";
                    $i++;
                    if($i<$numelem)
                        $cond_rad .=" or ";
                }
                $cond_rad .=")";
            }else{
                $cond_rad="cast(a.radi_nume_sal as char(18)) like '%".$numrad[0]."%'";
            }
        }else{
        $cond_rad="cast(a.radi_nume_sal as char(18)) like '%".$numrad."%'";
    }
        date_default_timezone_set("America/Bogota");
        $ano_ini = date("Y");
        $mes_ini = substr("00".(date("m")-1),-2);
        if ($mes_ini==0) {$ano_ini=$ano_ini-1; $mes_ini="12";}
        $dia_ini = date("d");
        $fecha_ini = "$ano_ini/$mes_ini/$dia_ini";
        $fecha_fin = date("Y/m/d") ;
        $fecha_ini = mktime(00,00,00,substr($fecha_ini,5,2),substr($fecha_ini,8,2),substr($fecha_ini,0,4));
	$fecha_fin = mktime(23,59,59,substr($fecha_fin,5,2),substr($fecha_fin,8,2),substr($fecha_fin,0,4));
        $where_fecha = " (a.SGD_RENV_FECH >= ". $this->link->conn->DBTimeStamp($fecha_ini) ." and a.SGD_RENV_FECH <= ". $this->link->conn->DBTimeStamp($fecha_fin).") " ;
        $sqlChar = $this->link->conn->SQLDate("d-m-Y H:i A","SGD_RENV_FECH");
        $ssql="select a.radi_nume_sal AS \"RADICADO\" , c.RADI_NUME_DERI AS \"RADICADO_PADRE\"
            , $sqlChar AS \"FECHA_ENVIO\" , a.sgd_renv_planilla AS \"PLANILLA\"
            , a.sgd_renv_nombre AS \"DESTINARIO\" , a.sgd_renv_dir AS \"DIRECCION\" , a.SGD_RENV_PAIS AS \"PAIS\"
            , a.sgd_renv_depto AS \"DEPARTAMENTO\" , a.sgd_renv_mpio AS \"MUNICIPIO\" , b.sgd_fenv_descrip AS \"MEDIO_ENVIO\"
            , d.USUA_LOGIN AS \"USUA_ACTU\" , a.sgd_renv_valor AS \"VALORENVIO\", a.sgd_renv_peso as peso, a.sgd_fenv_codigo as codmenvio
            , a.sgd_dir_tipo as copia, substr(cast(a.sgd_dir_tipo as varchar(3)), 2, 3) as valcop from sgd_renv_regenvio a
            LEFT OUTER JOIN sgd_fenv_frmenvio b ON a.sgd_fenv_codigo=b.sgd_fenv_codigo, radicado c, usuario d
            where $where_fecha and $cond_rad and a.sgd_renv_estado < 8
                and c.radi_nume_radi= a.radi_nume_sal and c.radi_usua_actu=d.usua_codi and sgd_renv_planilla<>'00' and sgd_renv_nombre<>'Varios' order by 5";
        $rs=  $this->link->conn->Execute($ssql);
        $recordEM['error']="";
        $i=0;
        if(!$rs->EOF){
            while(!$rs->EOF){
                $recordEM[$i]['numrad']=$rs->fields['RADICADO'];
                $recordEM[$i]['radi_padre']=$rs->fields['RADICADO_PADRE'];
                $recordEM[$i]['fecha_envio']=$rs->fields['FECHA_ENVIO'];
                $recordEM[$i]['planilla']=$rs->fields['PLANILLA'];
                $recordEM[$i]['destinatario']=$rs->fields['DESTINATARIO'];
                $recordEM[$i]['direccion']=$rs->fields['DIRECCION'];
                $recordEM[$i]['pais']=$rs->fields['PAIS'];
                $recordEM[$i]['departamento']=$rs->fields['DEPARTAMENTO'];
                $recordEM[$i]['municipio']=$rs->fields['MUNICIPIO'];
                $recordEM[$i]['medio_envio']=$rs->fields['MEDIO_ENVIO'];
                $recordEM[$i]['usua_actu']=$rs->fields['USUA_ACTU'];
                $recordEM[$i]['valorenvio']=$rs->fields['VALORENVIO'];
                $recordEM[$i]['peso']=$rs->fields['PESO'];
                $recordEM[$i]['codmenvio']=$rs->fields['CODMENVIO'];
                $recordEM[$i]['copia']=$rs->fields['COPIA'];
                $recordEM[$i]['valcop']=$rs->fields['VALCOP'];
                $rs->MoveNext();
                $i++;
            }
        }else{
            $recordEM['error']="<hr><b><font color=red><center>No se encontro el n&uacute;mero del radicado</center></font></b><hr>\n";
        }
        return $recordEM;
    }

    function estadisticaEnvios($depus,$cmedenvio, $fecha_ini, $fecha_fin){
        if($depus==0){
            $depwhere="";
        }  else {
            $depwhere="and r.radi_depe_radi=$depus";
        }
        if($cmedenvio==0){
            $cmewhere="";
        }else
            $cmewhere="and renv.sgd_fenv_codigo=$cmedenvio";
        $ssql="select r.radi_depe_radi, d.depe_nomb, renv.radi_nume_sal, renv.sgd_renv_nombre, renv.sgd_renv_dir, renv.sgd_renv_mpio, renv.sgd_renv_depto
            ,sgd_renv_pais as pais ,renv.sgd_renv_fech, fenv.sgd_fenv_codigo, sgd_fenv_descrip, renv.sgd_renv_planilla ,case when renv.sgd_renv_tipo=2
	    then 'Envio Masiva' else 'Envio Normal' end as origen
            from dependencia d, sgd_renv_regenvio renv, radicado r, sgd_fenv_frmenvio fenv where renv.sgd_renv_fech between
	    to_timestamp('$fecha_ini 0:00:00','dd/mm/yyyy hh24:mi:ss') and to_timestamp('$fecha_fin 23:59:59','dd/mm/yyyy hh24:mi:ss')
            and d.depe_codi=r.radi_depe_radi and radi_nume_sal=radi_nume_radi and renv.sgd_fenv_codigo=fenv.sgd_fenv_codigo and
            renv.sgd_renv_nombre <> 'Varios' $depwhere $cmewhere and sgd_renv_planilla <>' ' and sgd_renv_planilla<>'00'";
        $rs=$this->link->conn->Execute($ssql);
        $recordEE['error']="";
        $i=0;
        if(!$rs->EOF){
            while(!$rs->EOF){
                $recordEE[$i]['dependencia']=$rs->fields['DEPE_NOMB'];
                $recordEE[$i]['numrad']=$rs->fields['RADI_NUME_SAL'];
                $recordEE[$i]['destinatario']=$rs->fields['SGD_RENV_NOMBRE'];
                $recordEE[$i]['direccion']=$rs->fields['SGD_RENV_DIR'];
                $recordEE[$i]['municipio']=$rs->fields['SGD_RENV_MPIO'];
                $recordEE[$i]['departamento']=$rs->fields['SGD_RENV_DEPTO'];
                $recordEE[$i]['pais']=$rs->fields['PAIS'];
                $recordEE[$i]['fecha']=$rs->fields['SGD_RENV_FECH'];
                $recordEE[$i]['medenvio']=$rs->fields['SGD_FENV_CODIGO']." - ".$rs->fields['SGD_FENV_DESCRIP'];
                $recordEE[$i]['planilla']=$rs->fields['SGD_RENV_PLANILLA'];
                $recordEE[$i]['origen']=$rs->fields['ORIGEN'];
                $i++;
                $rs->MoveNext();
            }
        }else{
            $recordEE['error']="No hay registros";
        }
        return $recordEE;
    }
}
?>

