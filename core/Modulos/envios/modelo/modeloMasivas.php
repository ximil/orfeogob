<?php 
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of modeloMasivas
 *
 * @author derodriguez
 */
if (!$ruta_raiz)
    $ruta_raiz = '../../..';

include_once "$ruta_raiz/core/clases/ConnectionHandler.php";

class modeloMasivas {
    //put your code here
    public $link;
    
    function __construct($ruta_raiz) {
        $db = new ConnectionHandler ( "$ruta_raiz" );
		$db->conn->SetFetchMode ( ADODB_FETCH_NUM );
		$db->conn->SetFetchMode ( ADODB_FETCH_ASSOC );
		$this->link = $db;
        //$this->link->conn->debug=true;
    }
    /*@function listarMasivas
     * 
     * Esta funci&oacute;n busca lista de radicados como masivas para enviar
     * 
     * @param numeric $depus Es el numero de la dependencia consultada
     * 
     * @return $recordLM Arreglo de la tabla que indica cuantas masivas hay para enviar
     * 
     */
    function listarMasivas($depus){
        $ssql="select r.RADI_NUME_GRUPO , count(*) as DOCUMENTOS , min(r.RADI_NUME_SAL) as RAD_INI 
            , MAX(r.RADI_NUME_SAL) as RAD_FIN , TO_CHAR(r.SGD_RENV_FECH, 'YYYY/MM/DD') AS FECHA , r.USUA_DOC 
            , rd.TDOC_CODI from sgd_renv_regenvio r, radicado rd WHERE r.sgd_renv_planilla = '00' 
            and r.sgd_renv_tipo = 1 and rd.RADI_NUME_RADI= r.RADI_NUME_GRUPO and sgd_depe_genera = '$depus' and
            not exists(select 1 from sgd_renv_regenvio r1, radicado r2 where r1.radi_nume_grupo=r.radi_nume_grupo and r1.radi_nume_sal=r2.radi_nume_radi and
            (trim(r2.radi_path)='' or r2.radi_path is NULL))
            group by r.radi_nume_grupo, TO_CHAR(r.SGD_RENV_FECH, 'YYYY/MM/DD'), r.usua_doc, rd.TDOC_CODI 
            order by r.radi_nume_grupo";
        $rs=  $this->link->conn->Execute($ssql);
        if (!$rs->EOF){
            $i=0;
            $recordLM['error']="";
            while(!$rs->EOF){
                $numtemprad=$rs->fields['RADI_NUME_GRUPO'];
                $recordLM[$i]['grupo']=$rs->fields['RADI_NUME_GRUPO'];
                $sqlTmp="select count(*) as numero_eliminado from sgd_rmr_radmasivre where sgd_rmr_grupo=$numtemprad";
                $rstmp=  $this->link->conn->Execute($sqlTmp);
                $recordLM[$i]['rad_ini']=$rs->fields['RAD_INI'];
                $recordLM[$i]['rad_fin']=$rs->fields['RAD_FIN'];
                $recordLM[$i]['fecha']=$rs->fields['FECHA'];
                $recordLM[$i]['eliminados']=$rstmp->fields['NUMERO_ELIMINADO'];
                $recordLM[$i]['documentos']= $rs->fields['DOCUMENTOS'];
                $usua_doc= $rs->fields['USUA_DOC'];
                $sqlTmp="select usua_nomb from usuario where USUA_DOC ='$usua_doc'";
                $rstmp= $this->link->conn->Execute($sqlTmp);
                $recordLM[$i]['usua_nomb']=$rstmp->fields['USUA_NOMB'];
                $tdoc_codi=$rs ->fields['TDOC_CODI'];
                $sqlTmp="select sgd_tpr_descrip as tipdocum from sgd_tpr_tpdcumento where sgd_tpr_codigo=$tdoc_codi";
                $rstmp=  $this->link->conn->Execute($sqlTmp);
                $recordLM[$i]['tipdocum']=$rstmp->fields['TIPDOCUM'];
                $i++;
                $rs->MoveNext();
                unset($rstmp);
            }
        }else{
           $recordLM['error']="<hr><b><font color=red><center>No hay documentos para realizar esta operaci&oacute;n</center></font></b><hr>\n"; 
        }
        return $recordLM;
    }
    
    function listarGrupoMasivas($depus,$gruporad){
        $ssql="select a.radi_nume_sal, a.sgd_renv_codigo,TO_CHAR(b.radi_fech_radi, 'YYYY/MM/DD') as fecharad,a.sgd_renv_depto as dpto_nomb,a.sgd_renv_mpio as muni_nomb,a.sgd_renv_nombre as destinatario
        ,sgd_renv_dir as direccion from sgd_renv_regenvio a, radicado b where sgd_renv_planilla = '00' and sgd_renv_tipo = 1 and radi_nume_grupo=$gruporad
        and sgd_depe_genera = $depus and a.radi_nume_sal=b.radi_nume_radi and not exists (select * from sgd_rmr_radmasivre rm where a.radi_nume_sal=rm.sgd_rmr_radi)
        order by radi_nume_sal asc";
        $rs =  $this->link->conn->Execute($ssql);
        $recordGM['error']="";
        $i=0;
        if(!$rs->EOF){
            while(!$rs->EOF){
                $recordGM[$i]['numrad']=$rs->fields['RADI_NUME_SAL'];
                $recordGM[$i]['codenvio']=$rs->fields['SGD_RENV_CODIGO'];
                $recordGM[$i]['fecharad']= $rs->fields['FECHARAD'];
                $recordGM[$i]['dpto_nomb']=$rs->fields['DPTO_NOMB'];
                $recordGM[$i]['muni_nomb']=$rs->fields['MUNI_NOMB'];
                $recordGM[$i]['destinatario']=$rs->fields['DESTINATARIO'];
                $recordGM[$i]['direccion']=$rs->fields['DIRECCION'];
                $i++;
                $rs->MoveNext();
            }
        }else{
             $recordGM['error']="<hr><b><font color=red><center>No hay documentos para realizar esta operaci&oacute;n</center></font></b><hr>\n";
             }
        return $recordGM;
    }
    function retirarUsuaMasiva($depus,$gruporad,$radret,$codus,$docus){
        $isql="insert into sgd_rmr_radmasivre(sgd_rmr_radi,sgd_rmr_grupo) values ($radret,$gruporad)";
        $rs= $this->link->conn->query($isql);
        $ipaddress=$_SERVER['REMOTE_ADDR'];
        $systemDate = $this->link->conn->OffsetDate(0,$this->link->conn->sysTimeStamp);
        $isql="insert into hist_eventos(depe_codi,hist_fech,usua_codi,radi_nume_radi,hist_obse,usua_codi_dest,usua_doc) 
            values ($depus, $systemDate,$codus,$radret,'Retirado de grupo de Masiva ($gruporad) ',$codus,$docus)";
        $rs= $this->link->conn->query($isql);
    }
    function estadoExcMasiva($gruporad,$radret){
        $numelem=count($radret);
        $i=0;
        $cond="(";
        while ($i<$numelem){
            $cond .=" sgd_rmr_radi=$radret[$i] ";
            $i++;
            if ($i<$numelem){
                $cond .="or";
            }else{
                $cond .=")";
            }
        }
        $sql="select count(*) as contador from sgd_rmr_radmasivre where $cond";
        $rs=  $this->link->conn->Execute($sql);
        $contador=$rs->fields['CONTADOR'];
        if($contador==0){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    function marcarEnvioMasiva($gruporad, $depus, $empresa_envio, $numplan,$peso){
        //$this->link->conn->StartTrans();;
		$fecha_hoy =  $this->link->conn->OffsetDate(0, $this->link->conn->sysTimeStamp);
        $usql="UPDATE SGD_RENV_REGENVIO 
		SET sgd_renv_planilla='$numplan',
			sgd_renv_tipo = '2',
			depe_codi=$depus,
			sgd_fenv_codigo='$empresa_envio',
                        sgd_renv_peso=$peso,
						sgd_renv_fech=$fecha_hoy
		WHERE sgd_renv_planilla = '00' 
			and radi_nume_grupo = $gruporad and radi_nume_sal not in 
			(select sgd_rmr_radi  from sgd_rmr_radmasivre  where sgd_rmr_grupo=$gruporad)";
        $this->link->conn->query($usql);
	//Modificacion para DNE para anexos dados a la radicacion masiva
	$usql2="UPDATE ANEXOS SET anex_estado=4, anex_fech_envio=current_timestamp where radi_nume_salida in (select radi_nume_sal from sgd_renv_regenvio 
	where radi_nume_grupo=$gruporad) and radi_nume_salida not in (select sgd_rmr_radi from sgd_rmr_radmasivre  where sgd_rmr_grupo=$gruporad)";
	$this->link->conn->query($usql2);
    $usql3 = "update RADICADO set SGD_EANU_CODIGO=9 where RADI_NUME_RADI in (select radi_nume_sal from sgd_renv_regenvio 
	where radi_nume_grupo=$gruporad) and radi_nume_radi not in (select sgd_rmr_radi from sgd_rmr_radmasivre  where sgd_rmr_grupo=$gruporad)";
    $this->link->conn->query($usql3);
        //$this->link->conn->RollbackTrans();
    }
    
    function dataMasEnviar($gruporad,$depus){
        $ssql="select dir.id_pais,dir.id_cont, dir.muni_codi, dir.dpto_codi
            from sgd_renv_regenvio reg, sgd_dir_drecciones dir WHERE sgd_renv_planilla = '00' and 
            sgd_renv_tipo = 1 and radi_nume_grupo=$gruporad and sgd_depe_genera = $depus and 
            dir.radi_nume_radi=reg.radi_nume_sal
            and not exists(select * from sgd_rmr_radmasivre rm where reg.radi_nume_sal=rm.sgd_rmr_radi)
            order by radi_nume_sal asc";
        $rs =  $this->link->conn->Execute($ssql);
        $recordGM['error']="";
        $i=0;
        $local=0;
        $nacional=0;
        $internacional=0;
        $intercontinen=0;
            while(!$rs->EOF){
                //$muni_codi=$rs->fields['MUNI_CODI'];
                $dpto_codi=$rs->fields['DPTO_CODI'];
                $id_cont=$rs->fields['ID_CONT'];
                $id_pais=$rs->fields['ID_PAIS'];
                if ($id_cont == 1) //Si va para el mismo continente
                        {
                                if ($id_pais == 170) //Si va para el mismo pais
                                {       //Hacemos la validacion de si es local, aca hay un codigo metido a machetazo el cual
                                        //valida unos municipios que actuan como precio local en algunos dptos, hay que
                                        //cambiar esta validacion para que tome los datos de homologacion de precios capturados
                                        //en el administrador de municipios.
                                        if ($dpto_codi==11) //Si va para la misma ciudad
                                        {
                                                $local++;
                                               
                                        }
                                        else
                                        {
                                                $nacional++;;
                                        }
                                }
                                else
                                {
                                        $internacional++;
                                }
                        }else{
                            $intercontinen++;
                        }
                $rs->MoveNext();
            }
            $listades[0]=$local;
            $listades[1]=$nacional;
            $listades[2]=$internacional;
            $listades[3]=$intercontinen;
            return $listades;
    }
    
    function enviarMasiva($depus,$gruporad,$listades,$codmdenvio,$numplan,$obserenvio,$peso,$usua_doc){
        //$this->link->conn->StartTrans();
        $fecha_hoy =  $this->link->conn->OffsetDate(0, $this->link->conn->sysTimeStamp);
        $local=$listades[0];
        $nacional=$listades[1];
        $internacional=$listades[2];
        $intercontinen=$listades[3];
        if($local!=0){
            $ssql="select sgd_renv_codigo as nextval from sgd_renv_regenvio order by sgd_renv_codigo desc limit 1";
            $rs=  $this->link->conn->Execute($ssql);
            $nextval=$rs->fields['NEXTVAL'];
            $nextval=$nextval++;
            $isql="insert into sgd_renv_regenvio(usua_doc, sgd_renv_codigo, sgd_fenv_codigo, sgd_renv_fech, sgd_renv_telefono
                , sgd_renv_mail, sgd_renv_peso, sgd_renv_certificado, sgd_renv_estado, sgd_renv_nombre, sgd_dir_codigo
                , sgd_dir_tipo, depe_codi, radi_nume_grupo, sgd_renv_dir, sgd_renv_depto, sgd_renv_planilla, sgd_renv_observa,
                radi_nume_sal, sgd_renv_destino, sgd_renv_valor, sgd_renv_mpio, sgd_renv_cantidad) values ('$usua_doc', '$nextval'
                ,'$codmdenvio', $fecha_hoy, '', '', '$peso', 0, 1, 'Varios', 0, 1, '$depus', '$gruporad', 'Varios', '', '$numplan'
                ,'$obserenvio', '$gruporad', 'Local', '', 'Local', '$local')";
            $this->link->conn->query($isql);
        }
       if($nacional!=0){
            $ssql="select sgd_renv_codigo as nextval from sgd_renv_regenvio order by sgd_renv_codigo desc limit 1";
            $rs=  $this->link->conn->Execute($ssql);
            $nextval=$rs->fields['NEXTVAL'];
            $nextval=$nextval++;
            $isql="insert into sgd_renv_regenvio(usua_doc, sgd_renv_codigo, sgd_fenv_codigo, sgd_renv_fech, sgd_renv_telefono
                , sgd_renv_mail, sgd_renv_peso, sgd_renv_certificado, sgd_renv_estado, sgd_renv_nombre, sgd_dir_codigo
                , sgd_dir_tipo, depe_codi, radi_nume_grupo, sgd_renv_dir, sgd_renv_depto, sgd_renv_planilla, sgd_renv_observa,
                radi_nume_sal, sgd_renv_destino, sgd_renv_valor, sgd_renv_mpio, sgd_renv_cantidad) values ('$usua_doc', '$nextval'
                ,'$codmdenvio', $fecha_hoy, '', '', '$peso', 0, 1, 'Varios', 0, 1, '$depus', '$gruporad', 'Varios', '', '$numplan'
                ,'$obserenvio', '$gruporad', 'Nacional', '', 'Nacional', '$nacional')";
            $this->link->conn->query($isql);
        }
       if($internacional!=0){
            $ssql="select sgd_renv_codigo as nextval from sgd_renv_regenvio order by sgd_renv_codigo desc limit 1";
            $rs=  $this->link->conn->Execute($ssql);
            $nextval=$rs->fields['NEXTVAL'];
            $nextval=$nextval++;
            $isql="insert into sgd_renv_regenvio(usua_doc, sgd_renv_codigo, sgd_fenv_codigo, sgd_renv_fech, sgd_renv_telefono
                , sgd_renv_mail, sgd_renv_peso, sgd_renv_certificado, sgd_renv_estado, sgd_renv_nombre, sgd_dir_codigo
                , sgd_dir_tipo, depe_codi, radi_nume_grupo, sgd_renv_dir, sgd_renv_depto, sgd_renv_planilla, sgd_renv_observa,
                radi_nume_sal, sgd_renv_destino, sgd_renv_valor, sgd_renv_mpio, sgd_renv_cantidad) values ('$usua_doc', '$nextval'
                ,'$codmdenvio', $fecha_hoy, '', '', '$peso', 0, 1, 'Varios', 0, 1, '$depus', '$gruporad', 'Varios', '', '$numplan'
                ,'$obserenvio', '$gruporad', 'Internacional', '', 'Internacional', '$internacional')";
            $this->link->conn->query($isql);
        }
       if($intercontinen!=0){
            $ssql="select sgd_renv_codigo as nextval from sgd_renv_regenvio order by sgd_renv_codigo desc limit 1";
            $rs=  $this->link->conn->Execute($ssql);
            $nextval=$rs->fields['NEXTVAL'];
            $nextval=$nextval++;
            $isql="insert into sgd_renv_regenvio(usua_doc, sgd_renv_codigo, sgd_fenv_codigo, sgd_renv_fech, sgd_renv_telefono
                , sgd_renv_mail, sgd_renv_peso, sgd_renv_certificado, sgd_renv_estado, sgd_renv_nombre, sgd_dir_codigo
                , sgd_dir_tipo, depe_codi, radi_nume_grupo, sgd_renv_dir, sgd_renv_depto, sgd_renv_planilla, sgd_renv_observa,
                radi_nume_sal, sgd_renv_destino, sgd_renv_valor, sgd_renv_mpio, sgd_renv_cantidad) values ('$usua_doc', '$nextval'
                ,'$codmdenvio', $fecha_hoy, '', '', '$peso', 0, 1, 'Varios', 0, 1, '$depus', '$gruporad', 'Varios', '', '$numplan'
                ,'$obserenvio', '$gruporad', 'Intercontinental', '', 'Intercontinental', '$intercontinen')";
            $this->link->conn->query($isql);
        }
        //$this->link->conn->RollbackTrans();
    }
    
    function accionExcluidos($gruporad,$krd){
        $ssql="select count(*) as cont from sgd_rmr_radmasivre where sgd_rmr_grupo=$gruporad";
        $rs=  $this->link->conn->Execute($ssql);
        $i=0;
        if($rs->fields['CONT']!=0){
            $systemDate = $this->link->conn->OffsetDate(0,$this->link->conn->sysTimeStamp);
            $comenta="Radicados excluidos masiva grupo($gruporad)";
            $usql2="update sgd_renv_regenvio set sgd_renv_planilla=NULL, sgd_deve_codigo=23, sgd_deve_fech=$systemDate, sgd_renv_observa='$comenta'
            where radi_nume_grupo=$gruporad and radi_nume_sal in (select sgd_rmr_radi  from sgd_rmr_radmasivre  where sgd_rmr_grupo=$gruporad)";
            $this->link->conn->query($usql2);
	    //Se comenta lo siguiente debido a que el anexo lo tiene desde la digitalizacion
            /*$ssql2="select radi_nume_radi as numrad, radi_path, radi_depe_radi as depe_radi, radi_fech_radi, radi_desc_anex from radicado where radi_nume_radi
                in (select sgd_rmr_radi from sgd_rmr_radmasivre  where sgd_rmr_grupo=$gruporad)";
            $rs2=  $this->link->conn->Execute($ssql2);
            //print_r($rs2->fields);
            $anex_codi='00001';
            while(!$rs2->EOF){
                $numrad=$rs2->fields['NUMRAD'];
                $radi_path=$rs2->fields['RADI_PATH'];
                $radi_desc=$rs2->fields['RADI_DESC_ANEX'];  
                $ext=  end(explode('.',$radi_path));
                $filename=end(explode('/',$radi_path));
                $filesz=filesize(RUTA_BODEGA.$radi_path)/1024;
                $anex_radi=$numrad.$anex_codi;
                $ansql="select anex_tipo_codi from anexos_tipo where anex_tipo_ext='$ext'";
                $rs3=  $this->link->conn->Execute($ansql);
                $anex_tipo=$rs3->fields['ANEX_TIPO_CODI'];
                $isql="insert into anexos(anex_radi_nume,anex_codigo,anex_tipo,anex_creador,anex_numero,anex_nomb_archivo,anex_borrado,anex_solo_lect,anex_tamano,anex_radi_fech,anex_estado,radi_nume_salida,sgd_dir_tipo,anex_desc, sgd_fech_impres)
                    values ($numrad,$anex_radi,$anex_tipo,'$krd',$anex_codi,'$filename','N','S',$filesz,now(),3,$numrad,1,'$radi_desc',now())";
                $this->link->conn->query($isql);
                $rs2->MoveNext();
            }*/
        }
    }
}

?>
