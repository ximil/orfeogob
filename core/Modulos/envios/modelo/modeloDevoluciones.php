<?php 
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
if (!$ruta_raiz)
    $ruta_raiz = '../../..';

include_once "$ruta_raiz/core/clases/ConnectionHandler.php";
/**
 * Description of modeloDevoluciones
 *
 * @author derodriguez
 */
class modeloDevoluciones {
    //put your code here
    private $link;
    
    public function __construct($ruta_raiz) {
        $db = new ConnectionHandler ( "$ruta_raiz" );
	$db->conn->SetFetchMode ( ADODB_FETCH_NUM );
	$db->conn->SetFetchMode ( ADODB_FETCH_ASSOC );
	$this->link = $db;
        //$this->link->conn->debug=true;
    }
    
    /*
     * Funcion que genera el listado para los posibles envios que estan dentro del lapso de tiempo para ser devueltos normales
     * 
     */
    function consTodoxDevolver($depe_sel){
        date_default_timezone_set("America/Bogota");
        $anoActual = date("Y");
        $ano_ini = date("Y")-1;
/*        $mes_ini = substr("00".(date("m")-1),-2);
        if ($mes_ini==0) {$ano_ini=$ano_ini-1; $mes_ini="12";}*/
        $dia = date("d");
        $mes= date("m");
        /*$ano_ini = date("Y");
        if(!isset($fecha_ini)) $fecha_ini = "$ano_ini/$mes_ini/$dia_ini";*/
        $fecha_ini=$ano_ini.'/'.$mes.'/'.$dia;
        $fecha_fin = $anoActual.'/'.$mes.'/'.$dia;
        $fecha_ini = mktime(00,00,00,substr($fecha_ini,5,2),substr($fecha_ini,8,2),substr($fecha_ini,0,4));
        $fecha_fin = mktime(23,59,59,substr($fecha_fin,5,2),substr($fecha_fin,8,2),substr($fecha_fin,0,4));
        $where_fecha = " (a.SGD_RENV_FECH >= ". $this->link->conn->DBTimeStamp($fecha_ini) ." and a.SGD_RENV_FECH <= ". $this->link->conn->DBTimeStamp($fecha_fin).") " ;
        $dependencia_busq1 = " $where_fecha and ";
        $sqlChar = $this->link->conn->SQLDate("d-m-Y H:i A","SGD_RENV_FECH");
        //$valor    = "(cast(a.sgd_renv_cantidad as numeric) * (case sgd_renv_valor when '' then 0 else (cast(sgd_renv_valor as numeric)) end))";
        $valor    = "cast(sgd_renv_valor as numeric)";
        //$dependencia_busq1='';
        
            $sqlConcat = $this->link->conn->Concat("a.radi_nume_sal","'-'","a.sgd_renv_codigo");
        
                $isql = 'select ' . "4" . '             AS CHU_ESTADO
                                ,' . 0 . '              AS HID_DEVE_CODIGO
                                ,a.radi_nume_sal        AS Radicado
                                ,c.RADI_NUME_DERI       AS Radicado_Padre
                                ,' . $sqlChar . '       AS Fecha_Envio
                                ,a.sgd_renv_planilla    AS Planilla
                                ,a.sgd_renv_nombre      AS Destinatario
                                ,a.sgd_renv_dir         AS Direccion
                                ,a.sgd_renv_depto       AS Departamento
                                ,a.sgd_renv_mpio        AS Municipio
                                ,b.sgd_fenv_descrip     AS Empresa_Envio
                                ,d.USUA_LOGIN           AS Usuario_actual
                                ,'.$valor.'             AS Valorenvio
                                , '. $sqlConcat .  '    AS CHK_RADI_NUME_SAL
                                ,a.sgd_dir_tipo         AS HID_sgd_dir_tipo
                                ,a.sgd_renv_cantidad    AS HID_sgd_renv_cantidad
                                ,a.sgd_renv_codigo      AS HID_sgd_renv_codigo
                                ,c.RADI_FECH_RADI       AS HID_RADI_FECH_RADI
                                ,c.RA_ASUN              AS HID_RA_ASUN
                                ,d.USUA_NOMB            AS HID_USUA_NOMB
                                ,c.radi_depe_actu       AS HID_radi_depe_actu
                                ,a.sgd_dir_tipo         AS copia
                        from sgd_renv_regenvio a,
                                sgd_fenv_frmenvio b,
                                radicado c, usuario d
                        where sgd_deve_codigo is null and' .
                                $dependencia_busq1 . ' '.
                                'c.radi_depe_radi = '.$depe_sel.' 
                                
                                and a.sgd_renv_estado < 8
                                and     c.radi_nume_radi= a.radi_nume_sal
                and a.sgd_fenv_codigo = b.sgd_fenv_codigo
                                and c.radi_usua_actu=d.usua_codi and a.sgd_renv_nombre <> \'Varios\'';

                $rs=  $this->link->conn->Execute($isql);
                $i=0;
                $recordDev['error']="";
                if(!$rs->EOF){
                    while (!$rs->EOF){
                        $recordDev[$i]['CHU_ESTADO']=$rs->fields['CHU_ESTADO'];
                        //$recordDev[$i]['HID_DEVE_CODIGO']=$rs->fields['HIDI_DEVE_CODIGO'];
                        $recordDev[$i]['numradi']=$rs->fields['RADICADO'];
                        $recordDev[$i]['numradideri']=$rs->fields['RADICADO_PADRE'];
                        $recordDev[$i]['fechenvio']=$rs->fields['FECHA_ENVIO'];
                        $recordDev[$i]['planilla']=$rs->fields['PLANILLA'];
                        $recordDev[$i]['destinatario']=$rs->fields['DESTINATARIO'];
                        $recordDev[$i]['direccion']=$rs->fields['DIRECCION'];
                        $recordDev[$i]['departamento']=$rs->fields['DEPARTAMENTO'];
                        $recordDev[$i]['municipio']=$rs->fields['MUNICIPIO'];
                        $recordDev[$i]['usua_actual']=$rs->fields['USUARIO_ACTUAL'];
                        $recordDev[$i]['empresa_envio']=$rs->fields['EMPRESA_ENVIO'];
                        $recordDev[$i]['valorenvio']=$rs->fields['VALORENVIO'];
                        $recordDev[$i]['copia']=$rs->fields['COPIA'];
                        $i++;
                        $rs->MoveNext();
                    }
                }else{
                    $recordDev['error']="<hr><b><font color=red><center>No hay documentos para realizar esta operaci&oacute;n</center></font></b><hr>\n";
                }
                return $recordDev;
        
    }
    
    function consxDevolver($numrad){
        if(is_array($numrad)){
        $numelem=count($numrad);
            if($numelem>1){
                $i=0;
                $cond_rad="(";
                while($i<$numelem){
                    $cond_rad .="cast(a.radi_nume_sal as char(18)) like '%".$numrad[$i]."%'";
                    $i++;
                    if($i<$numelem)
                        $cond_rad .=" or ";
                }
                $cond_rad .=")";
            }else{
                $cond_rad="cast(a.radi_nume_sal as char(18)) like '%".$numrad[0]."%'";
            }
        }else{
            $cond_rad="cast(a.radi_nume_sal as char(18)) like '%".$numrad."%'";
        }
        date_default_timezone_set("America/Bogota");
        $anoActual = date("Y");
        $ano_ini = date("Y")-1;
/*        $mes_ini = substr("00".(date("m")-1),-2);
        if ($mes_ini==0) {$ano_ini=$ano_ini-1; $mes_ini="12";}*/
        $dia = date("d");
        $mes= date("m");
        /*$ano_ini = date("Y");
        if(!isset($fecha_ini)) $fecha_ini = "$ano_ini/$mes_ini/$dia_ini";*/
        $fecha_ini=$ano_ini.'/'.$mes.'/'.$dia;
        $fecha_fin = $anoActual.'/'.$mes.'/'.$dia;
        $fecha_ini = mktime(00,00,00,substr($fecha_ini,5,2),substr($fecha_ini,8,2),substr($fecha_ini,0,4));
        $fecha_fin = mktime(23,59,59,substr($fecha_fin,5,2),substr($fecha_fin,8,2),substr($fecha_fin,0,4));
        $where_fecha = " (a.SGD_RENV_FECH >= ". $this->link->conn->DBTimeStamp($fecha_ini) ." and a.SGD_RENV_FECH <= ". $this->link->conn->DBTimeStamp($fecha_fin).") " ;
        $dependencia_busq1 = " $where_fecha and ";
        $sqlChar = $this->link->conn->SQLDate("d-m-Y H:i A","SGD_RENV_FECH");
        //$valor    = "(cast(a.sgd_renv_cantidad as numeric) * (case sgd_renv_valor when '' then 0 else (cast(sgd_renv_valor as numeric)) end))";
        $valor    = "cast(sgd_renv_valor as numeric)";
        //$dependencia_busq1='';
        
            $sqlConcat = $this->link->conn->Concat("a.radi_nume_sal","'-'","a.sgd_renv_codigo");
        
                $isql = 'select ' . "4" . '             AS CHU_ESTADO
                                ,' . 0 . '              AS HID_DEVE_CODIGO
                                ,a.radi_nume_sal        AS Radicado
                                ,c.RADI_NUME_DERI       AS Radicado_Padre
                                ,' . $sqlChar . '       AS Fecha_Envio
                                ,a.sgd_renv_planilla    AS Planilla
                                ,a.sgd_renv_nombre      AS Destinatario
                                ,a.sgd_renv_dir         AS Direccion
                                ,a.sgd_renv_depto       AS Departamento
                                ,a.sgd_renv_mpio        AS Municipio
                                ,b.sgd_fenv_descrip     AS Empresa_Envio
                                ,d.USUA_LOGIN           AS Usuario_actual
                                ,'.$valor.'             AS Valorenvio
                                , '. $sqlConcat .  '    AS CHK_RADI_NUME_SAL
                                ,a.sgd_dir_tipo         AS HID_sgd_dir_tipo
                                ,a.sgd_renv_cantidad    AS HID_sgd_renv_cantidad
                                ,a.sgd_renv_codigo      AS HID_sgd_renv_codigo
                                ,c.RADI_FECH_RADI       AS HID_RADI_FECH_RADI
                                ,c.RA_ASUN              AS HID_RA_ASUN
                                ,d.USUA_NOMB            AS HID_USUA_NOMB
                                ,c.radi_depe_actu       AS HID_radi_depe_actu
                                ,a.sgd_dir_tipo         AS copia
                        from sgd_renv_regenvio a,
                                sgd_fenv_frmenvio b,
                                radicado c, usuario d
                        where sgd_deve_codigo is null and' .
                                $dependencia_busq1 . ' '.
                                $cond_rad.'
                                
                                and a.sgd_renv_estado < 8
                                and     c.radi_nume_radi= a.radi_nume_sal
                and a.sgd_fenv_codigo = b.sgd_fenv_codigo
                                and c.radi_usua_actu=d.usua_codi and a.sgd_renv_nombre <> \'Varios\'';
                $rs=  $this->link->conn->Execute($isql);
                $i=0;
                $recordDev['error']="";
                if(!$rs->EOF){
                    while (!$rs->EOF){
                        $recordDev[$i]['CHU_ESTADO']=$rs->fields['CHU_ESTADO'];
                        //$recordDev[$i]['HID_DEVE_CODIGO']=$rs->fields['HIDI_DEVE_CODIGO'];
                        $recordDev[$i]['numradi']=$rs->fields['RADICADO'];
                        $recordDev[$i]['numradideri']=$rs->fields['RADICADO_PADRE'];
                        $recordDev[$i]['fechenvio']=$rs->fields['FECHA_ENVIO'];
                        $recordDev[$i]['planilla']=$rs->fields['PLANILLA'];
                        $recordDev[$i]['destinatario']=$rs->fields['DESTINATARIO'];
                        $recordDev[$i]['direccion']=$rs->fields['DIRECCION'];
                        $recordDev[$i]['departamento']=$rs->fields['DEPARTAMENTO'];
                        $recordDev[$i]['municipio']=$rs->fields['MUNICIPIO'];
                        $recordDev[$i]['usua_actual']=$rs->fields['USUARIO_ACTUAL'];
                        $recordDev[$i]['empresa_envio']=$rs->fields['EMPRESA_ENVIO'];
                        $recordDev[$i]['valorenvio']=$rs->fields['VALORENVIO'];
                        $recordDev[$i]['copia']=$rs->fields['COPIA'];
                        $i++;
                        $rs->MoveNext();
                    }
                }else{
                    $recordDev['error']="<hr><b><font color=red><center>No hay documentos para realizar esta operaci&oacute;n</center></font></b><hr>\n";
                }
                return $recordDev;
        
    }
    
    function consulxDevolver($num_rad,$copia){
        /*$anoActual = date("Y");
        $ano_ini = date("Y");
        $mes_ini = substr("00".(date("m")-1),-2);
        if ($mes_ini==0) {$ano_ini=$ano_ini-1; $mes_ini="12";}
        $dia_ini = date("d");
        $ano_ini = date("Y");
        if(!isset($fecha_ini)) $fecha_ini = "$ano_ini/$mes_ini/$dia_ini";
        $fecha_fin = date("Y/m/d") ;
        $fecha_ini = mktime(00,00,00,substr($fecha_ini,5,2),substr($fecha_ini,8,2),substr($fecha_ini,0,4));
        $fecha_fin = mktime(23,59,59,substr($fecha_fin,5,2),substr($fecha_fin,8,2),substr($fecha_fin,0,4));
        $where_fecha = " (a.SGD_RENV_FECH >= ". $this->link->conn->DBTimeStamp($fecha_ini) ." and a.SGD_RENV_FECH <= ". $this->link->conn->DBTimeStamp($fecha_fin).") " ;*/
        //$dependencia_busq1 = " $where_fecha and ";
        $cond_rad="a.radi_nume_sal=$num_rad";
        //$valor    = "(cast(a.sgd_renv_cantidad as numeric) * (case sgd_renv_valor when '' then 0 else (cast(sgd_renv_valor as numeric)) end))";
        $valor    = "cast(sgd_renv_valor as numeric)";
        $sqlChar = $this->link->conn->SQLDate("d-m-Y H:i A","SGD_RENV_FECH");
                $isql = "select
			 a.radi_nume_sal   as Radicado,
			 sgd_renv_planilla  as Planilla,
			 $sqlChar      as FECHA_ENVIO,
			 sgd_renv_nombre    as Destinatario,
			 sgd_renv_dir       as Direccion,
			 sgd_renv_depto     as Departamento,
       			 sgd_renv_mpio	    as Municipio,
			 sgd_renv_cantidad  as Documentos,
			 $valor             as Valor_Envio
                         ,d.USUA_LOGIN           AS Usuario_actual
                         ,a.sgd_renv_planilla    AS Planilla
                         ,sgd_fenv_descrip as empresa_envio
                         ,b.RADI_NUME_DERI       AS Radicado_Padre
                         ,a.sgd_dir_tipo         AS copia
	 		 from SGD_RENV_REGENVIO a , radicado b,
                         sgd_fenv_frmenvio c, usuario d
			 where
			 a.radi_nume_sal=b.radi_nume_radi
                         and b.radi_usua_actu=d.usua_codi and a.sgd_renv_nombre <> 'Varios'
                         and a.sgd_fenv_codigo = c.sgd_fenv_codigo
			 and $cond_rad and a.sgd_dir_tipo=$copia";
                        //order by ' . $order .$orderTipo;
                $rs=  $this->link->conn->Execute($isql);
                $recordDev['error']="";
                $i=0;
                if(!$rs->EOF){
                    while(!$rs->EOF){
                        $recordDev[$i]['numradi']=$rs->fields['RADICADO'];
                        $recordDev[$i]['planilla']=$rs->fields['PLANILLA'];
                        $recordDev[$i]['fechenvio']=$rs->fields['FECHA_ENVIO'];
                        $recordDev[$i]['destinatario']=$rs->fields['DESTINATARIO'];
                        $recordDev[$i]['direccion']=$rs->fields['DIRECCION'];
                        $recordDev[$i]['departamento']=$rs->fields['DEPARTAMENTO'];
                        $recordDev[$i]['municipio']=$rs->fields['MUNICIPIO'];
                        $recordDev[$i]['usua_actual']=$rs->fields['USUARIO_ACTUAL'];
                        $recordDev[$i]['valorenvio']=$rs->fields['VALOR_ENVIO'];
                        $recordDev[$i]['empresa_envio']=$rs->fields['EMPRESA_ENVIO'];
                        $recordDev[$i]['planilla']=$rs->fields['PLANILLA'];
                        $recordDev[$i]['numradideri']=$rs->fields['RADICADO_PADRE'];
			$recordDev[$i]['documentos']=$rs->fields['DOCUMENTOS'];
                        $recordDev[$i]['copia']=$rs->fields['COPIA'];
                        $i++;
                        $rs->MoveNext();
                    }
                }else{
                    $recordDev['error']="<hr><b><font color=red><center>No hay documentos para realizar esta operaci&oacute;n</center></font></b><hr>\n";
                }
                return $recordDev;
        
    }
    
    function generarMotivoDev(){
        $sql = "select * from SGD_DEVE_DEV_ENVIO
				WHERE SGD_DEVE_CODIGO < 99 and sgd_deve_codigo<>23
				 order by SGD_DEVE_CODIGO";
			$rsDev = $this->link->conn->Execute($sql);
                        if(!$rsDev->EOF){
                            $i=0;
                            while(!$rsDev->EOF){
                                $recordMD[$i]['deve_codigo']=$rsDev->fields['SGD_DEVE_CODIGO'];
                                $recordMD[$i]['deve_motivo']=$rsDev->fields['SGD_DEVE_DESC'];
                                $i++;
                                $rsDev->MoveNext();
                            }
                        }else{
                            $recordMD="<hr><b><font color=red><center>No hay documentos para realizar esta operaci&oacute;n</center></font></b><hr>\n";
                        }
                        return $recordMD;
    }
    function reportarDevolucion($numrad,$motidevo,$codus,$depus,$rolus,$obserdev,$docus,$copia){
        //$this->link->conn->StartTrans();
        $systemDate = $this->link->conn->OffsetDate(0,$this->link->conn->sysTimeStamp);
	//$sqlConcat = $this->link->conn->Concat("'$motidevo'","'-'","sgd_renv_observa");
        $isqlu = "update sgd_renv_regenvio
		set
		sgd_deve_fech=$systemDate,
		sgd_deve_codigo = $motidevo,
		sgd_renv_observa = '$obserdev'
	  where radi_nume_sal='$numrad' and sgd_dir_tipo = $copia
          and sgd_deve_codigo is NULL and sgd_renv_nombre<>'Varios'";
        $rs1=$this->link->conn->query($isqlu);
        $isql = "update anexos
                    set anex_estado=3, sgd_deve_fech=$systemDate, sgd_deve_codigo = $motidevo
			where sgd_dir_tipo=$copia and radi_nume_salida=$numrad";
        $rs2=$this->link->conn->query($isql);
        $ssql2="select SGD_DEVE_DESC from SGD_DEVE_DEV_ENVIO WHERE SGD_DEVE_CODIGO = $motidevo";
        $rs3=  $this->link->conn->Execute($ssql2);
        $motivo=$rs3->fields['SGD_DEVE_DESC'];
            $isql_he="insert into hist_eventos(DEPE_CODI, HIST_FECH, USUA_CODI, USUA_CODI_DEST,USUA_DOC,RADI_NUME_RADI, HIST_OBSE,  SGD_TTR_CODIGO)
			values ($depus, $systemDate ,$codus,NULL,$docus,$numrad,'Devolucion($motivo) $obserdev','28')";
            $this->link->conn->query($isql_he);
           $selsql="SELECT case when RADI_NUME_DERI is null then 0 else radi_nume_deri end FROM RADICADO WHERE RADI_NUME_RADI=$numrad";
           $rs4=$this->link->conn->Execute($selsql);
           $radpadre=$rs4->fields['RADI_NUME_DERI'];
           if($radpadre!=0){
               $isql_he2="insert into hist_eventos(DEPE_CODI, HIST_FECH, USUA_CODI,USUA_CODI_DEST,USUA_DOC,RADI_NUME_RADI, HIST_OBSE,  SGD_TTR_CODIGO)
			values ($depus, $systemDate ,$codus,NULL,$docus,$radpadre,'Devolucion($motivo - $numrad) $obserdev','28')";
               $this->link->conn->query($isql_he2);
           }
        //$rs4=  $this->link->conn->query($isql_ha);
        //$this->link->conn->RollbackTrans();
    }
    
    function estadisticaDevolucion($depus,$cmedenvio,$fecha_ini,$fecha_fin){
        if($depus==0){
            $depwhere="";
        }  else {
            $depwhere="and r.radi_depe_radi=$depus";
        }
        if($cmedenvio==0){
            $cmewhere="";
        }else
            $cmewhere="and renv.sgd_fenv_codigo=$cmedenvio";
        $ssql="select fenv.sgd_fenv_descrip, d.depe_nomb, renv.radi_nume_sal, renv.sgd_renv_nombre, renv.sgd_renv_dir, renv.sgd_renv_fech
            , renv.sgd_renv_mpio, renv.sgd_renv_depto, renv.sgd_renv_pais as pais,renv.sgd_deve_fech, dev.sgd_deve_desc  from dependencia d, sgd_renv_regenvio renv
            ,radicado r, sgd_fenv_frmenvio fenv, sgd_deve_dev_envio dev where renv.sgd_deve_fech between to_timestamp('$fecha_ini 0:00:00','dd/mm/yyyy hh24:mi:ss') 
	    and to_timestamp('$fecha_fin 23:59:59','dd/mm/yyyy hh24:mi:ss')
            and renv.sgd_deve_codigo=dev.sgd_deve_codigo and d.depe_codi=r.radi_depe_radi and renv.radi_nume_sal=r.radi_nume_radi and renv.sgd_fenv_codigo=fenv.sgd_fenv_codigo 
            $depwhere $cmewhere and sgd_renv_planilla<>'00' order by sgd_deve_fech";
        $rs=  $this->link->conn->Execute($ssql);
        $recordED['error']="";
        $i=0;
        if(!$rs->EOF){
            while(!$rs->EOF){
                $recordED[$i]['medenvio']=$rs->fields['SGD_FENV_DESCRIP'];
                $recordED[$i]['depen']=$rs->fields['DEPE_NOMB'];
                $recordED[$i]['numrad']=$rs->fields['RADI_NUME_SAL'];
                $recordED[$i]['fechaenv']=$rs->fields['SGD_RENV_FECH'];
                $recordED[$i]['destinatario']=$rs->fields['SGD_RENV_NOMBRE'];
                $recordED[$i]['direccion']=$rs->fields['SGD_RENV_DIR'];
                $recordED[$i]['municipio']=$rs->fields['SGD_RENV_MPIO'];
                $recordED[$i]['depto']=$rs->fields['SGD_RENV_DEPTO'];
                $recordED[$i]['pais']=$rs->fields['PAIS'];
                $recordED[$i]['fechadeve']=$rs->fields['SGD_DEVE_FECH'];
                $recordED[$i]['motivo']=$rs->fields['SGD_DEVE_DESC'];
                $i++;
                $rs->MoveNext();
            }
        }else{
            $recordED['error']="No hay datos en la busqueda";
        }
        return $recordED;
    }

    function devolTEspe($depus,$fecha_ini,$fecha_fin){
        if($depus==0){
            $depwhere="select depe_codi from dependencia where depe_estado=1";
        }else
            $depwhere=$depus;
        $fechadb=$this->link->conn->DBTimeStamp($fecha_ini);
        $ssql="select radi_nume_salida as numrad, cast(substr(cast (radi_nume_salida as varchar(18)), 5, 3) as integer) as Dependencia
            ,('$fecha_fin' - sgd_fech_impres) as T_ESPERA from anexos where sgd_fech_impres<=$fechadb and anex_estado=3 
            and cast(substr(cast (radi_nume_salida as varchar(18)), 5, 3) as integer) in ($depwhere)
            and sgd_deve_codigo is null";
        $rs=$this->link->conn->Execute($ssql);
        $i=0;
        while(!$rs->EOF){
            $resul[$i]['numrad']=$rs->fields['NUMRAD'];
            $resul[$i]['dependencia']=$rs->fields['DEPENDENCIA'];
            $resul[$i]['t-espera']=$rs->fields['T_ESPERA'];
            $i++;
            $rs->MoveNext();
        }
        return $resul;
    }
    
    function devolCTEspe($depus,$fecha_ini){
        if($depus==0){
            $depwhere="select depe_codi from dependencia where depe_estado=1";
        }else
            $depwhere=$depus;
        $fechadb=$this->link->conn->DBTimeStamp($fecha_ini);
        $ssql="select count(*) as contador from anexos where sgd_fech_impres<=$fechadb and anex_estado=3
            and cast(substr(cast (radi_nume_salida as varchar(18)), 5, 3) as integer) in ($depwhere)
                            and sgd_deve_codigo is null";
        $rs=$this->link->conn->Execute($ssql);
        $contador=$rs->fields['CONTADOR'];
        return $contador;
    }
    
    function devolxTEspe($depus,$fecha_ini,$fecha_fin){
        $this->link->conn->StartTrans();
        if($depus==0){
            $depwhere="select depe_codi from dependencia where depe_estado=1";
        }else
            $depwhere=$depus;
        $fechadb=$this->link->conn->DBTimeStamp($fecha_ini);
        $usql="update anexos set anex_estado=3, sgd_deve_codigo=99, sgd_deve_fech='$fecha_fin' where sgd_fech_impres<=$fechadb 
            and anex_estado=3 and cast(substr(cast (radi_nume_salida as varchar(18)), 5, 3) as integer) in ($depwhere)
            and sgd_deve_codigo is null";
        $this->link->conn->query($usql);
        $this->link->conn->RollbackTrans();
    }
}

?>
