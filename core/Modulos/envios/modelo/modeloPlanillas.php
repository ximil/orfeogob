<?php 
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
if (!$ruta_raiz)
    $ruta_raiz = '../../..';

include_once "$ruta_raiz/core/clases/ConnectionHandler.php";
/**
 * Description of modeloPlanillas
 *
 * @author derodriguez
 */
class modeloPlanillas {
    //put your code here
    private $link;
    
    function __construct($ruta_raiz) {
        $db = new ConnectionHandler ( "$ruta_raiz" );
	$db->conn->SetFetchMode ( ADODB_FETCH_NUM );
	$db->conn->SetFetchMode ( ADODB_FETCH_ASSOC );
	$this->link = $db;
        //$this->link->conn->debug=true;
    }
    /*Funci&oacute;n encargada de generar un nuevo n&uacute;mero del consecutivo para la planilla de un reporte de envio
     * 
     * @return numeric $codgenplan Nuevo c&oacute;digo para la secuencia de generaci&oacute;n de plantilla 
     * 
     */
    
    function genNumPlanilla($depus){
        date_default_timezone_set("America/Bogota");
        $sql="SELECT   sgd_cen_pfech as a,  sgd_cen_con as b,  depe_codi as c FROM sgd_cen_codenvio where depe_codi=$depus and sgd_cen_pfech=".date('y');
        $rs= $this->link->conn->Execute($sql);
        if(!$rs->EOF){
            $consec=$rs->fields['B'];
        }
        else{
            $consec=0;
        }
        $consec=$consec+1;
        $val=4-strlen($consec);
        $ceros="";
        for ($i=0; $i < $val; $i++){
            $ceros="0".$ceros;
        }
        $numplanil= date("y").$depus.$ceros.$consec;
        return $numplanil;
    }
    
    function valNumPlanilla($depus,$numplan){
            $ano=  substr($numplan,0,2);
            $cons= substr($numplan,-4);
            $sql="select case when sgd_cen_con < $cons then 0 else 1 end as val from sgd_cen_codenvio where depe_codi=$depus
                and sgd_cen_pfech=$ano";
            $rs=  $this->link->conn->Execute($sql);
            $val=$rs->fields['VAL'];
        return $val;
    }
    
    function planillaVerificada($depus, $numplan){
        date_default_timezone_set("America/Bogota");
        $ano=date('y');
        $sec=substr($numplan,-4);
        $sql="SELECT   sgd_cen_pfech as a,  sgd_cen_con as b,  depe_codi as c FROM 
            sgd_cen_codenvio where depe_codi=$depus and sgd_cen_pfech=$ano";
        $rs= $this->link->conn->Execute($sql);
        if(!$rs->EOF){
            $consec=$rs->fields['B']+1;
            if ($consec<$sec)
                $consec=$sec;
            $sql="update sgd_cen_codenvio set sgd_cen_con=$consec where
                     depe_codi=$depus and sgd_cen_pfech=$ano";
        }
        else{
            $consec=1;
            $sql="insert into sgd_cen_codenvio(sgd_cen_pfech, sgd_cen_con, depe_codi) 
                values ($ano,$consec,$depus)";
        }
        $rs=  $this->link->conn->query($sql);
    }
    
    function listarPlanillas($depus){
        $sql="select sgd_cen_pfech as aa, depe_codi,sgd_cen_con as con from sgd_cen_codenvio where depe_codi=$depus";
        $rs=$this->link->conn->Execute($sql);
        $i=0;
        $recordLP['error']="";
        if(!$rs->EOF){
            while (!$rs->EOF){
                $recordLP[$i]['fechaplan']= $rs->fields['AA'];
                $recordLP[$i]['depeplan']= $rs->fields['DEPE_CODI'];
                $recordLP[$i]['id_planilla']= $rs->fields['CON'];
                $i++;
                $rs->MoveNext();
            }
            
        }else{
            $recordLP="<hr><b><font color=red><center>No hay documentos para realizar esta operaci&oacute;n</center></font></b><hr>\n";
        }
        return $recordLP;
    }
    
    function listarPendientes($usua_doc){
        $sql="select reg.radi_nume_sal, reg.radi_nume_grupo ,m.muni_codi,m.id_pais,m.dpto_codi,m.muni_nomb,reg.sgd_fenv_codigo as codmd 
                  , f.sgd_fenv_descrip as nomb_medio from sgd_renv_regenvio reg, municipio m, sgd_fenv_frmenvio f
                  where reg.usua_doc='$usua_doc' and reg.sgd_renv_destino= m.muni_nomb AND 
                  reg.SGD_RENV_PLANILLA=' ' and (reg.sgd_renv_tipo <2 or reg.sgd_renv_tipo is null) and reg.sgd_fenv_codigo=f.sgd_fenv_codigo
                  order by reg.sgd_fenv_codigo";
        $rs = $this->link->conn->Execute($sql);
        $i=0;
        $recordLP['error']="";
        if(!$rs->EOF){
            while(!$rs->EOF){
                $recordLP[$i]['radi_nume_sal']=$rs->fields['RADI_NUME_SAL'];
                $recordLP[$i]['codmd'] = $rs->fields['CODMD'];
                $recordLP[$i]['nomb_medio'] = $rs ->fields['NOMB_MEDIO'];
                $i++;
                $rs->MoveNext();
            }
        }
        else{
            $recordLP['error']="<hr><b><font color=red><center>No hay documentos para realizar esta operaci&oacute;n</center></font></b><hr>\n";
        }
        return $recordLP;
    }
    
    function pendientesXMenvio($usua_doc){
        $sql="select reg.sgd_fenv_codigo as codmd ,f.sgd_fenv_descrip as nomb_medio 
            from sgd_renv_regenvio reg, municipio m, sgd_fenv_frmenvio f 
            where reg.usua_doc='$usua_doc' and reg.sgd_renv_destino= m.muni_nomb AND reg.SGD_RENV_PLANILLA=' ' 
            and (reg.sgd_renv_tipo <2 or reg.sgd_renv_tipo is null) and reg.sgd_fenv_codigo=f.sgd_fenv_codigo
            group by reg.sgd_fenv_codigo, f.sgd_fenv_descrip order by reg.sgd_fenv_codigo";
        $rs=  $this->link->conn->Execute($sql);
        $recordME['error']="";
        $i=0;
        if(!$rs->EOF){
            while(!$rs->EOF){
                $recordME[$i]['codmenvio']=$rs->fields['CODMD'];
                $recordME[$i]['medenvio']=$rs->fields['NOMB_MEDIO'];
                $i++;
                $rs->MoveNext();
            }
        }else{
            $recordME['error']="<option value='0'>--No hay datos de envios seleccionados--</option>";
        }
        return $recordME;
    }
    
    function generarPENormal($codmenvio,$usua_doc,$numplan){
        //$this->link->conn->StartTrans();
        $sql="update sgd_renv_regenvio set sgd_renv_planilla='$numplan' where usua_doc='$usua_doc' AND SGD_RENV_PLANILLA=' ' 
            and (sgd_renv_tipo <2 or sgd_renv_tipo is null) and sgd_fenv_codigo=$codmenvio";
        $rs=  $this->link->conn->query($sql);
        //$this->link->conn->RollbackTrans();
    }
    
    function generarDataPlantilla($numplan){
	$sql="select fenv.SGD_FENV_CODIGO as codenv, fenv.SGD_FENV_DESCRIP as NOMB_MEDIO, SGD_RENV_PESO as PESO, RADI_NUME_FOLIO AS FOLIOS, RADI_NUME_ANEXO as NUMANEX
            ,RADI_CUENTAI AS CUENTAI, SGD_RENV_DIR AS DIRECCION, raDI_NUME_SAL, SGD_RENV_DEPTO AS UBICACIOND
            , SGD_RENV_MPIO AS UBICACIONM, SGD_RENV_NOMBRE AS DESTINATARIO
            , SGD_RENV_TEL AS TELEFONO, SGD_RENV_PAIS AS UBICACIONP from SGD_RENV_REGENVIO renv, SGD_FENV_FRMENVIO fenv, RADICADO r where 
            renv.SGD_FENV_CODIGO = fenv.SGD_FENV_CODIGO AND SGD_RENV_PLANILLA= '$numplan' AND R.RADI_NUME_RADI=renv.RADI_NUME_SAL AND
            sgd_renv_tipo < 3 ORDER BY SGD_RENV_FECH";
        $rs= $this->link->conn->Execute($sql);
        $i=0;
        $masiva="no";
        $recordGIP['error']="";
        if(!$rs->EOF){
            while (!$rs->EOF){  
                $recordGIP[$i]['codmenvio']=$rs->fields['CODENV'];
                $recordGIP[$i]['nomb_medio']=$rs->fields['NOMB_MEDIO'];
                $recordGIP[$i]['peso']=$rs->fields['PESO'];
                $recordGIP[$i]['direccion']=$rs->fields['DIRECCION'];
                $recordGIP[$i]['numrad']=$rs->fields['RADI_NUME_SAL'];
                $recordGIP[$i]['ubicaciond']=$rs->fields['UBICACIOND'];
                $recordGIP[$i]['ubicacionm']=$rs->fields['UBICACIONM'];
                $recordGIP[$i]['destinatario']=$rs->fields['DESTINATARIO'];
                $recordGIP[$i]['telefono']= $rs->fields['TELEFONO'];
                $recordGIP[$i]['ubicacionp']=$rs->fields['UBICACIONP'];
				$tmpfol=$rs->fields['FOLIOS'];
				$tmpnanex=$rs->fields['NUMANEX'];
                $recordGIP[$i]['folios']=$tmpfol+$tmpnanex;
                $recordGIP[$i]['cuentai']=$rs->fields['CUENTAI'];
                if($recordGIP[$i]['ubicacionm']=='Local' || $recordGIP[$i]['ubicacionm']=='Nacional' ){
                    $masiva="ok";
                    $codenv=$recordGIP[$i]['codmenvio'];
                }
                $i++;
                $rs->MoveNext();
            }
            if($masiva=="ok"){
		$sql="select SGD_RENV_DIR as DIRECCION, SGD_RENV_PESO as PESO, raDI_NUME_SAL,RADI_NUME_FOLIO AS FOLIOS, RADI_NUME_ANEXO as NUMANEX
                ,RADI_CUENTAI AS CUENTAI, SGD_RENV_DEPTO AS UBICACIOND, SGD_RENV_MPIO AS UBICACIONM, sgd_renv_pais as UBICACIONP
                ,SGD_RENV_NOMBRE AS DESTINATARIO, SGD_RENV_TEL AS TELEFONO from SGD_RENV_REGENVIO,RADICADO  
                WHERE SGD_FENV_CODIGO = $codenv AND SGD_RENV_PLANILLA= '$numplan' AND RADI_NUME_RADI=RADI_NUME_SAL AND
                    sgd_renv_tipo = 2 ORDER BY RADI_NUME_SAL";
                $rs2=$this->link->conn->Execute($sql);
                $i=0;
                /*unset($recordGIP);
                $recordGIP['error']="";*/
                
                while(!$rs2->EOF){
                    $recordGIP[$i]['peso']=$rs2->fields['PESO'];
                    $recordGIP[$i]['direccion']=$rs2->fields['DIRECCION'];
                    $recordGIP[$i]['numrad']=$rs2->fields['RADI_NUME_SAL'];
                    $recordGIP[$i]['ubicaciond']=$rs2->fields['UBICACIOND'];
                    $recordGIP[$i]['ubicacionm']=$rs2->fields['UBICACIONM'];
                    $recordGIP[$i]['destinatario']=$rs2->fields['DESTINATARIO'];
                    $recordGIP[$i]['telefono']= $rs2->fields['TELEFONO'];
                    $recordGIP[$i]['ubicacionp']=$rs2->fields['UBICACIONP'];
                    $tmpfol=$rs->fields['FOLIOS'];
					$tmpnanex=$rs->fields['NUMANEX'];
					$recordGIP[$i]['folios']=$tmpfol+$tmpnanex;
                    $recordGIP[$i]['cuentai']=$rs->fields['CUENTAI'];
                    $i++;
                    $rs2->MoveNext();
                }
            }
        }else{
            $recordGIP['error']="<font color='red'>Este n&uacute;mero de planilla no existe.</font>";
        }
        return $recordGIP;
    }
    
    
    
    function consultarPlanilla($numplan){
        $ssql="SELECT COUNT(*) as CONTADOR FROM SGD_RENV_REGENVIO WHERE SGD_RENV_PLANILLA= '$numplan'";
        $rs=  $this->link->conn->Execute($ssql);
        $cen=$rs->fields['CONTADOR'];
        if($cen==0){
            return 0;
        }else{
            return 1;
        }
    }

    function generarPlanSipos($numrad,$plansipos){
	$usql="update sgd_renv_regenvio set sgd_renv_sipostplan='$plansipos' where radi_nume_sal=$numrad and sgd_renv_sipostplan is null and 
		sgd_renv_nombre<>'Varios' and sgd_deve_codigo is null and sgd_renv_planilla<>'00' and sgd_fenv_codigo=101";
	$a=$this->link->conn->Execute($usql);
	if($a){
	    $status=1;
	}
	else{
	    $status=0;
	}
	return array($numrad,$status);
    }
    
}

?>
