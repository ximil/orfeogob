<?php 
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of modeloPlantillas
 *
 * @author derodriguez
 */
if (!$ruta_raiz)
    $ruta_raiz = '../../..';
//$ruta_bodega=$ruta_raiz.'/old/bodega/';

include_once "$ruta_raiz/core/clases/ConnectionHandler.php";

//require_once ("$ruta_raiz/dompdf/dompdf_config.inc.php");

class modeloPlantillas {
    //put your code here
    private $link;
    private $documento;
    
    function __construct($ruta_raiz) {
        $db = new ConnectionHandler ( "$ruta_raiz" );
	$db->conn->SetFetchMode ( ADODB_FETCH_NUM );
	$db->conn->SetFetchMode ( ADODB_FETCH_ASSOC );
	$this->link = $db;
        //$this->link->conn->debug=true;
        /**$dompdf = new DOMPDF ();
        $this->documento=$dompdf;*/
    }
    
    //function __constructor(){}
    
    function cargarCPlantilla($cmedenvio){
        $ssql="select alias, sgd_fmenv_codigo, case estado when 't' then 0 else 1 end as estado from sgd_fmenv_eventos 
            where sgd_fenv_codigo=$cmedenvio order by sgd_fmenv_codigo";
        $rs=$this->link->conn->Execute($ssql);
        $record['error']="";
        $i=0;
        if (!$rs->EOF){
            while(!$rs->EOF){
                $record[$i]['alias']=$rs->fields['ALIAS'];
                $record[$i]['sgd_fmenv_codigo']=$rs->fields['SGD_FMENV_CODIGO'];
                $record[$i]['estado']=$rs->fields['ESTADO'];
                $i++;
                $rs->MoveNext();
            }
        }else{
            $record['error']="<font color='red'>No hay campos dentro de la plantilla</font>\n";
        }
        return $record;
    }
    
    function cargarQPlanilla($cmedenvio){
        $ssql="select feve.alias, ms.query_campo from sgd_fmenv_eventos feve left outer join sgd_menv_metodos ms on ms.sgd_menv_codigo=feve.sgd_menv_codigo where
             estado='t' and sgd_fenv_codigo=$cmedenvio order by sgd_fmenv_codigo";
        $rs=$this->link->conn->Execute($ssql);
        $recordP['error']="";
        $i=0;
        if(!$rs->EOF){
            while(!$rs->EOF){
                $recordP[$i]['alias']=$rs->fields['ALIAS'];
                $recordP[$i]['query']=$rs->fields['QUERY_CAMPO'];
                $i++;
                $rs->MoveNext();
            }
        }else{
            $recordP['error']="<font color='red'>No hay plantilla cargada para planilla</font>";
        }
        return $recordP;
    }

    function agregarCPlantilla($cmedenvio){
        $ssql="select sgd_menv_codigo as num from sgd_fmenv_eventos where sgd_fenv_codigo=$cmedenvio";
        $rs=  $this->link->conn->Execute($ssql);
        if(!$rs->EOF){
            $cond="where";
            while(!$rs->EOF){
                $num=$rs->fields['NUM'];
                $cond .=" sgd_menv_codigo <> $num";
                $rs->MoveNext();
                if(!$rs->EOF){
                    $cond .=" and ";
                }
            }
            //$cond .=")";
        }else{
            $cond="";
        }
        $ssql2="select sgd_menv_codigo as codmetodo, tipo_metodo from sgd_menv_metodos $cond";
        $rs2=  $this->link->conn->Execute($ssql2);
        $i=0;
        $record['error']="";
        if(!$rs2->EOF){
            while(!$rs2->EOF){
                $record[$i]['codmetodo']=$rs2->fields['CODMETODO'];
                $record[$i]['tipo_metodo']=$rs2->fields['TIPO_METODO'];
                $rs2->MoveNext();
                $i++;
            }
        }else{
            $record['error']="<option>No hay campos precargados disponibles</option>";
        }
        return $record;
    }
    
    function agregarCampo($cmedenvio,$codmetodo,$alias){
        $trimalias=trim($alias," ");
        if($alias==""){
            $ssql="select tipo_metodo from sgd_menv_metodos where sgd_menv_codigo=$codmetodo";
            $rs=$this->link->conn->Execute($ssql);
            $alias=$rs->fields['TIPO_METODO'];
        }
        $isql="insert into sgd_fmenv_eventos(sgd_fenv_codigo,sgd_menv_codigo,alias) values
            ($cmedenvio,$codmetodo,'$alias')";
        $this->link->conn->query($isql);
    }
    
    function editarCampo($codfmenv,$alias){
        $usql="update sgd_fmenv_eventos set alias='$alias' where sgd_fmenv_codigo=$codfmenv";
        $this->link->conn->query($usql);
    }
    
    function activarCampo($codfmenv){
        $usql="update sgd_fmenv_eventos set estado='t' where sgd_fmenv_codigo=$codfmenv";
        $this->link->conn->query($usql);
    }
    
    function desactCampo($codfmenv){
        $usql="update sgd_fmenv_eventos set estado='f' where sgd_fmenv_codigo=$codfmenv";
        $this->link->conn->query($usql);
    }
    
 
    
}

?>
