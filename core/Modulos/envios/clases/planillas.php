<?php 
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of planillas
 *
 * @author Diego Enrique Rodriguez Delgado IEA
 */
if (! $ruta_raiz)
	$ruta_raiz = '../../../..';
    include_once "$ruta_raiz/core/Modulos/envios/modelo/modeloPlanillas.php";


class planillas {
    //put your code here
    private $NumPlanilla;
    private $NumeRadi;
    private $PlanSipos;
    private $RutaPlanilla;
    private $EstadoPlanilla;
    private $RadiAdjuntados;
    private $MedEnvio;
    private $DepeNume;
    private $FechaRango;
    private $DocUsuario;
    private $modelo;
    
    function __construct($ruta_raiz) {
        $this->modelo = new modeloPlanillas($ruta_raiz);
    }

    public function getNumeRadi() {
        return $this->NumeRadi;
    }

    public function setNumeRadi($NumeRadi) {
        $this->NumeRadi = $NumeRadi;
    }

    public function getPlanSipos() {
        return $this->PlanSipos;
    }

    public function setPlanSipos($PlanSipos) {
        $this->PlanSipos = $PlanSipos;
    }
    
    public function getNumPlanilla() {
        return $this->NumPlanilla;
    }

    public function setNumPlanilla($NumPlanilla) {
        $this->NumPlanilla = $NumPlanilla;
    }

    public function getRutaPlanilla() {
        return $this->RutaPlanilla;
    }

    public function setRutaPlanilla($RutaPlanilla) {
        $this->RutaPlanilla = $RutaPlanilla;
    }

    public function getEstadoPlanilla() {
        return $this->EstadoPlanilla;
    }

    public function setEstadoPlanilla($EstadoPlanilla) {
        $this->EstadoPlanilla = $EstadoPlanilla;
    }
    public function getRadiAdjuntados() {
        return $this->RadiAdjuntados;
    }

    public function setRadiAdjuntados($RadiAdjuntados) {
        $this->RadiAdjuntados = $RadiAdjuntados;
    }
    
    public function getMedEnvio() {
        return $this->MedEnvio;
    }

    public function setMedEnvio($MedEnvio) {
        $this->MedEnvio = $MedEnvio;
    }
    
    public function getDepeNume() {
        return $this->DepeNume;
    }

    public function setDepeNume($DepeNume) {
        $this->DepeNume = $DepeNume;
    }
    
    public function getFechaRango() {
        return $this->FechaRango;
    }

    public function setFechaRango($FechaRango) {
        $this->FechaRango = $FechaRango;
    }
    
    public function getDocUsuario() {
        return $this->DocUsuario;
    }

    public function setDocUsuario($DocUsuario) {
        $this->DocUsuario = $DocUsuario;
    }

    function genNumPlanilla(){
        return $this->modelo->genNumPlanilla($this->DepeNume);
    }
    
    function valNumPlanilla(){
        return $this->modelo->valNumPlanilla($this->DepeNume, $this->NumPlanilla);
    }

    function planillaVerificada(){
        return $this->modelo->planillaVerificada($this->DepeNume,$this->NumPlanilla);
    }
    
    function listarPlanillas(){
        return $this->modelo->listarPlanillas($this->DepeNume);
    }
    function listarPendientes(){
        return $this->modelo->listarPendientes($this->DocUsuario);
    }
    
    function pendientesXMenvio(){
        return $this->modelo->pendientesXMenvio($this->DocUsuario);
    }
    
    function generarPENormal(){
        $this->modelo->generarPENormal($this->MedEnvio, $this->DocUsuario, $this->NumPlanilla);
    }


    function generarDataPlantilla(){
        return $this->modelo->generarDataPlantilla($this->NumPlanilla);
    }
    
    function consultarPlanilla(){
        return $this->modelo->consultarPlanilla($this->NumPlanilla);
    }

    function generarPlanSipos(){
	list($numrad,$status)=$this->modelo->generarPlanSipos($this->NumeRadi,$this->PlanSipos);
	if($status==1){
	    $retorno="<tr class='listado2'><td>$numrad</td><td>Registro de planilla actualizado</td></tr>";
	}
	else{
	    $retorno="<tr class='listado2'><td>$numrad</td><td style='font-color:red;'>Registro de planilla no encontrado</td></tr>";
	}
	return $retorno;
    }
}

?>
