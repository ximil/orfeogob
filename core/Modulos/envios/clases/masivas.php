<?php 
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of masivas
 *
 * @author derodriguez
 */
if (! $ruta_raiz)
	$ruta_raiz = '../../../..';
    include_once "$ruta_raiz/core/Modulos/envios/modelo/modeloMasivas.php";

class masivas {
    //put your code here
    private $DepeRadi;
    private $RadiGrupo;
    private $RadiNume;
    private $UsuaDoc;
    private $MedEnvio;
    private $NumPlanilla;
    private $DestinosRad;
    private $ObservaEnv;
    private $Peso;
    private $UsuaCodi;
    private $Krd;
    private $modelo;
    
    function __construct($ruta_raiz) {
        $this->modelo = new modeloMasivas($ruta_raiz);
    }

    
    public function getDepeRadi() {
        return $this->DepeRadi;
    }

    public function setDepeRadi($DepeRadi) {
        $this->DepeRadi = $DepeRadi;
    }

    public function getRadiGrupo() {
        return $this->RadiGrupo;
    }

    public function setRadiGrupo($RadiGrupo) {
        $this->RadiGrupo = $RadiGrupo;
    }

    public function getRadiNume() {
        return $this->RadiNume;
    }

    public function setRadiNume($RadiNume) {
        $this->RadiNume = $RadiNume;
    }
  
    public function getUsuaDoc() {
        return $this->UsuaDoc;
    }

    public function setUsuaDoc($UsuaDoc) {
        $this->UsuaDoc = $UsuaDoc;
    }
    public function getMedEnvio() {
        return $this->MedEnvio;
    }

    public function setMedEnvio($MedEnvio) {
        $this->MedEnvio = $MedEnvio;
    }

    public function getNumPlanilla() {
        return $this->NumPlanilla;
    }

    public function setNumPlanilla($NumPlanilla) {
        $this->NumPlanilla = $NumPlanilla;
    }
    
    public function getDestinosRad() {
        return $this->DestinosRad;
    }

    public function setDestinosRad($DestinosRad) {
        $this->DestinosRad = $DestinosRad;
    }
    
    public function getObservaEnv() {
        return $this->ObservaEnv;
    }

    public function setObservaEnv($ObservaEnv) {
        $this->ObservaEnv = $ObservaEnv;
    }
    
    public function getPeso() {
        return $this->Peso;
    }

    public function setPeso($Peso) {
        $this->Peso = $Peso;
    }

    public function getUsuaCodi(){
	return $this->UsuaCodi;
    }

    public function setUsuaCodi($UsuaCodi){
	$this->UsuaCodi = $UsuaCodi;
    }
    
    public function getKrd() {
        return $this->Krd;
    }

    public function setKrd($Krd) {
        $this->Krd = $Krd;
    }
    
    function listarMasivas(){
        return $this->modelo->listarMasivas($this->DepeRadi);
    }
    
    function listarGrupoMasivas(){
        return $this->modelo->listarGrupoMasivas($this->DepeRadi,$this->RadiGrupo);
    }
    
    function retirarUsuaMasiva(){
        $this->modelo->retirarUsuaMasiva($this->DepeRadi,$this->RadiGrupo, $this->RadiNume, $this->UsuaCodi,  $this->UsuaDoc);
    }
    
    function estadoExcMasiva(){
        return $this->modelo->estadoExcMasiva($this->RadiGrupo, $this->RadiNume);
    }
    
    function marcarEnvioMasiva(){
        $this->modelo->marcarEnvioMasiva($this->RadiGrupo, $this->DepeRadi, $this->MedEnvio, $this->NumPlanilla,$this->Peso);
    }
    
    function dataMasEnviar(){
        return $this->modelo->dataMasEnviar($this->RadiGrupo, $this->DepeRadi);
    }
    
    function enviarMasiva(){
        $this->modelo->enviarMasiva($this->DepeRadi,$this->RadiGrupo,$this->DestinosRad, $this->MedEnvio, $this->NumPlanilla, $this->ObservaEnv, $this->Peso, $this->UsuaDoc);
    }
    
    function accionExcluidos(){
        $this->modelo->accionExcluidos($this->RadiGrupo,  $this->Krd);
    }

}

?>
