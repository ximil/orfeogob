<?php 
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
if (! $ruta_raiz)
	$ruta_raiz = '../../../..';
    include_once "$ruta_raiz/core/Modulos/envios/modelo/modeloPlantillas.php";
  //  include_once "$ruta_raiz/extra/dompdf/dompdf_config.inc.php";
/**
 * Description of plantillas
 *
 * @author derodriguez
 * @author Hardy Deimont Niño Velasquez
 */
class plantillas {
    //put your code here
    private $MedEnvio;
    private $Alias;
    private $CodEvento;
    private $Metodo;
    private $Evento;
    private $DataPlanillas;
    private $DataPlantillas;
    private $DataEncabezado;
    private $NumPlanilla;
    private $modelo;
    
    public function __construct($ruta_raiz) {
        $this->modelo= new modeloPlantillas($ruta_raiz);
        
    }
    
    public function getMedEnvio() {
        return $this->MedEnvio;
    }

    public function setMedEnvio($MedEnvio) {
        $this->MedEnvio = $MedEnvio;
    }

    public function getAlias() {
        return $this->Alias;
    }

    public function setAlias($Alias) {
        $this->Alias = $Alias;
    }

    public function getCodEvento() {
        return $this->CodEvento;
    }

    public function setCodEvento($CodEvento) {
        $this->CodEvento = $CodEvento;
    }

    public function getMetodo() {
        return $this->Metodo;
    }

    public function setMetodo($Metodo) {
        $this->Metodo = $Metodo;
    }

    public function getEvento() {
        return $this->Evento;
    }

    public function setEvento($Evento) {
        $this->Evento = $Evento;
    }
    
    public function getDataPlanillas() {
        return $this->DataPlanillas;
    }

    public function setDataPlanillas($DataPlanillas) {
        $this->DataPlanillas = $DataPlanillas;
    }

    public function getDataPlantillas() {
        return $this->DataPlantillas;
    }

    public function setDataPlantillas($DataPlantillas) {
        $this->DataPlantillas = $DataPlantillas;
    }
    
    public function getDataEncabezado() {
        return $this->DataEncabezado;
    }

    public function setDataEncabezado($DataEncabezado) {
        $this->DataEncabezado = $DataEncabezado;
    }

    public function getNumPlanilla() {
        return $this->NumPlanilla;
    }

    public function setNumPlanilla($NumPlanilla) {
        $this->NumPlanilla = $NumPlanilla;
    }

        
    /**
     * 
     * @return type
     */
    function cargarCPlantilla(){
        return $this->modelo->cargarCPlantilla($this->MedEnvio);
    }
    
    function cargarQPlantilla(){
        return $this->modelo->cargarQPlanilla($this->MedEnvio);
    }
    
    function agregarCPlantilla(){
        return $this->modelo->agregarCPlantilla($this->MedEnvio);
    }
    function agregarCampo(){
        $this->modelo->agregarCampo($this->MedEnvio, $this->Metodo, $this->Alias);
    }
    function editarCampo(){
        $this->modelo->editarCampo($this->Evento, $this->Alias);
    }
    
    function activarCampo(){
        $this->modelo->activarCampo($this->Evento);
    }
    
    function desactCampo(){
        $this->modelo->desactCampo($this->Evento);
    }
    

   function generarFormato(){
       $datahead=$this->DataEncabezado;
       $dataplani=$this->DataPlanillas;
       $dataplant=$this->DataPlantillas;
       $no_planillaC=$this->NumPlanilla;
       $head="";
       $modo=NULL;
       $html=NULL;
       $tabla=NULL;
       $tabla2=NULL;
        //$dompdf= new DOMPDF();
        //$this->documento=$dompdf;
        $entidad_largo_Planilla=$datahead['entidad_largo_planilla'];
        date_default_timezone_set("America/Bogota");
        $fecha=$fecha_hoy = Date("Y-m-d");
        //$fecha=$this->link->conn->DBDate($fecha_hoy);
        $numelem1=count($dataplant)-1;
        $numelem2=count($dataplani)-1;
        $contenidoCsv ="";
        $contenidoTxt ="";
        $contenido2="<br><br><table width='99%' border='1'  cellpadding='0' cellspacing='0' style='border-collapse: collapse;font-style:normal;font-size:12;text-align:center;letter-spacing: 0.1em;font-stretch: ultra-expanded;vertical-align:inherit;'>
            <tr bgcolor='#999999' style='height:50px;'>";
        for($i=0;$i<$numelem1;$i++){
            $contenidoCsv .=$dataplant[$i]['alias'].";";
            if(strlen($dataplant[$i]['alias'])>8){
                $contenidoTxt .=$dataplant[$i]['alias']."\t\t";
            }else
                $contenidoTxt .=$dataplant[$i]['alias']."\t";
            $contenido2 .= "<td width='20%'>".$dataplant[$i]['alias']."</td>\n";
        }
        $contenidoTxt .="\r\n";
        $contenidoCsv .="\n";
        $contenido2 .="\t\t</tr>\n";
	$head = "<html lang='es'><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'></head><body>
	<div id='head' style='text-align:center;'><span style='text-align:center;'>$entidad_largo_Planilla</span></div>
<p>Planilla No.: $no_planillaC</p>
<p>Fecha de emisi&oacute;n " . $fecha . "</p>";
	$footer = "<br><br><table width='100%' border='1'  cellpadding='0' cellspacing='0' style='border-collapse: collapse;font-style:normal;font-size:12;letter-spacing: 0.1em;font-stretch: ultra-expanded;'>
<tr><td  width='50%' align='center'><br><br><br></td><td  width='50%' align='center'></td></tr>
<tr><td  width='50%' align='left'>Elabor&oacute;:</td><td  width='50%' align='left'>Recibi&oacute;</td></td></tr>
</table><br><br><br></body><html>";
        for($i=0;$i<$numelem2;$i++){
            if($i%28==0 && $i!=0){
                //$tabla2.=$contenido2;
            }
            $tabla .="\t\t<tr>\n";
            //$tabla2 .="\t\t<tr>\n";
            $tmptxt="";
            $tmpcsv="";
            for($j=0;$j<$numelem1;$j++){
                $query=$dataplant[$j]['query'];
		if($query!=''){
		    $tabla .="\t\t\t<td style='height:100px;'>".$dataplani[$i][$query]."</td>\n";
                    //$tabla2 .="\t\t\t<td>".$dataplani[$i][$query]."</td>\n";
                    $tmpcsv .= trim($dataplani[$i][$query]);
                    $tmptxt .=str_replace('\t',' ',str_replace('\n','',trim ( $dataplani[$i][$query])))."\t";
		}else{
		    $tabla .="\t\t\t<td style='height:100px;'></td>\n";
                    //$tabla2 .="\t\t\t<td></td>\n";
                    $tmpcsv .= trim(' ');
                    $tmptxt .=str_replace('\t',' ',str_replace('\n','',trim (' ')))."\t";
		}
                //$h=$i+1;
                //if($h<$numelem1)
                    $tmpcsv .=";";
            }
            $tabla .="\t\t</tr>\n";
            //$tabla2 .="\t\t</tr>\n";
            $contenidoCsv .= str_replace('\t',' ',  str_replace('\n','', $tmpcsv))."\n";
            $contenidoTxt .=str_replace('\t',' ',str_replace('\n','',trim ( $tmptxt)))."\r\n";
        }
        $tabla .="\t\t</table>\n";
	//Construyendo el HTML
        $contenido2 =$contenido2.$tabla.$footer;
	$html .= $head . $contenido2;
	//Llamado a la libreria que arma el PDF
	include_once("../../../../extra/MPDF57/mpdf.php");
        $mpdf=new mPDF('es-ES','legal-L');
        $mpdf->WriteHTML($html);
	// ruta de guardado
	$arpdf_tmp = RUTA_BODEGA . "/pdfs/planillas/$no_planillaC.pdf";
	//salida 
	//Copiar al  disco
	$mpdf->Output($arpdf_tmp,'F');
	//Archivo  csv
	$nombre_archivo = RUTA_BODEGA . "/pdfs/planillas/$no_planillaC.csv";
	fopen ( $nombre_archivo, 'wra+' );
	
	// Asegurarse primero de que el archivo existe y puede escribirse sobre el. 
	if (is_writable ( $nombre_archivo )) {
		
		// En nuestro ejemplo estamos abriendo $nombre_archivo en modo de adicion. 
		// El apuntador de archivo se encuentra al final del archivo, asi que 
		// alli es donde ira $contenido cuando llamemos fwrite(). 
		if (! $gestor = fopen ( $nombre_archivo, 'a' )) {
			echo "No se puede abrir el archivo ($nombre_archivo)";
			exit ();
		}
		
		// Escribir $contenido a nuestro arcivo abierto. 
		if (fwrite ( $gestor, $contenidoCsv ) === FALSE) {
			echo "No se puede escribir al archivo ($nombre_archivo)";
			exit ();
		}
		
		//echo "&Eacute;xito, se escribi&oacute; ($contenidoCsv) d al archivo ($nombre_archivo)"; 
		

		fclose ( $gestor );
	
	} else {
		echo "No se puede escribir sobre el archivo $nombre_archivo";
	}
//Archivo  TXT
	$nombre_archivo2 = RUTA_BODEGA . "pdfs/planillas/$no_planillaC.txt";
	fopen ( $nombre_archivo2, 'wra+' );
	
	// Asegurarse primero de que el archivo existe y puede escribirse sobre el. 
	if (is_writable ( $nombre_archivo2 )) {
		
		// En nuestro ejemplo estamos abriendo $nombre_archivo en modo de adicion. 
		// El apuntador de archivo se encuentra al final del archivo, asi que 
		// alli es donde ira $contenido cuando llamemos fwrite(). 
		if (! $gestor = fopen ( $nombre_archivo2, 'a' )) {
			echo "No se puede abrir el archivo ($nombre_archivo2)";
			exit ();
		}
		
		// Escribir $contenido a nuestro arcivo abierto. 
		$tmp = chr(255).chr(254).mb_convert_encoding( $contenidoTxt, 'UTF-16LE', 'UTF-8'); 
		if (fwrite ( $gestor, $tmp ) === FALSE) {
			echo "No se puede escribir al archivo ($nombre_archivo2)";
			exit ();
		}
		
		//echo "&Eacute;xito, se escribi&oacute; ($contenidoCsv) d al archivo ($nombre_archivo)"; 
		

		fclose ( $gestor );
	
	} else {
		echo "No se puede escribir sobre el archivo $nombre_archivo";
	}
        //print_r($nombre_archivo2);
        $resultado ['fecha']= $fecha;
	$resultado ['html'] = $html;
	$resultado ['pdf'] = $arpdf_tmp;
	$resultado ['csv'] = $nombre_archivo;
	$resultado ['txt'] = $nombre_archivo2;
	return $resultado;
    }
    
    function valForGen(){
        $numplan=  $this->NumPlanilla;
        $path=RUTA_BODEGA."/pdfs/planillas/";
        if(file_exists($path.$numplan.".txt") && file_exists($path.$numplan.".csv") && file_exists($path.$numplan.".pdf")){
            return 0;
        }else{
            return 1;
        }
    }
    
    function cargaUnFormato(){
       $datahead=$this->DataEncabezado;
       $dataplani=$this->DataPlanillas;
       $dataplant=$this->DataPlantillas;
       $no_planillaC=$this->NumPlanilla;
       $head="";
       $modo=NULL;
       $html=NULL;
       $tabla=NULL;
        $entidad_largo_Planilla=$datahead['entidad_largo_planilla'];
        date_default_timezone_set("America/Bogota");
        $fecha=$fecha_hoy = Date("Y-m-d");
        $numelem1=count($dataplant)-1;
        $numelem2=count($dataplani)-1;
        $contenido2="<br><br><table width='99%' border='1'  cellpadding='0' cellspacing='0' style='border-collapse: collapse;font-style:normal;font-size:12;text-align:center;letter-spacing: 0.1em;font-stretch: ultra-expanded;vertical-align:inherit;'>
            <tr bgcolor='#999999'>";
        for($i=0;$i<$numelem1;$i++){
            $contenido2 .= "<td width='20%'>".$dataplant[$i]['alias']."</td>\n";
        }
        $contenido2 .="\t\t</tr>\n";
	$head = "<html lang='es'><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'></head><body>
        <div id='head' style='text-align:center;'><span style='text-align:center;'>$entidad_largo_Planilla</span></div>
<p>Planilla No.: $no_planillaC</p>
<p>Fecha de emisi&oacute;n " . $fecha . "</p>";
	$footer = "<br><br><table width='100%' border='1'  cellpadding='0' cellspacing='0' style='border-collapse: collapse;font-style:normal;font-size:12;letter-spacing: 0.1em;font-stretch: ultra-expanded;'>
<tr><td  width='50%' align='center'><br><br><br></td><td  width='50%' align='center'></td></tr>
<tr><td  width='50%' align='center'><br></td><td  width='50%' align='center'></td></td></tr>
</table><br><br><br></body></html>";
        for($i=0;$i<$numelem2;$i++){
            $tabla .="\t\t<tr>\n";
            for($j=0;$j<$numelem1;$j++){
                $query=$dataplant[$j]['query'];
		if($query!=''){
		    $tabla .="\t\t\t<td>".$dataplani[$i][$query]."</td>\n";
		}else
		    $tabla .="\t\t\t<td> </td>\n";
            }
            $tabla .="\t\t</tr>\n";
        }
        $tabla .="\t\t</table>\n";
        $contenido2 =$contenido2.$tabla.$footer;
	$html .= $head . $contenido2;
        $arpdf_tmp = RUTA_BODEGA . "/pdfs/planillas/$no_planillaC.pdf";
        $nombre_archivo = RUTA_BODEGA . "/pdfs/planillas/$no_planillaC.csv";
        $nombre_archivo2 = RUTA_BODEGA . "pdfs/planillas/$no_planillaC.txt";
        $resultado ['fecha']= $fecha;
	$resultado ['html'] = $html;
	$resultado ['pdf'] = $arpdf_tmp;
	$resultado ['csv'] = $nombre_archivo;
	$resultado ['txt'] = $nombre_archivo2;
	return $resultado;
    }
    
    function cargarsinPlantilla(){
       $datahead=$this->DataEncabezado;
       $dataplani=$this->DataPlanillas;
       $no_planillaC=$this->NumPlanilla;
        $head="";
       $modo=NULL;
       $html=NULL;
       $tabla=NULL;
        $entidad_largo_Planilla=$datahead['entidad_largo_planilla'];
        date_default_timezone_set("America/Bogota");
        $fecha=$fecha_hoy = Date("Y-m-d");
        $numelem2=count($dataplani)-1;
        $contenido2="<br><br><table width='99%' border='1'  cellpadding='0' cellspacing='0' style='border-collapse: collapse;font-style:normal;font-size:12;text-align:center;letter-spacing: 0.1em;font-stretch: ultra-expanded;vertical-align:inherit;'>
            <tr bgcolor='#999999'>";
        $contenido2.="<td width='20%'>NOMBRE<br>DESTINATARIO</td><td width='20%'>DIRECCION</td><td width='20%'>CIUDAD</td><td width='20%'>DEPARTAMENTO</td>
            <td width='20%'>PESO</td><td width='20%'>TELEFONO</td><td width='20%'>REFERENCIA</td>";
        $contenidoTxt="NOMBRE DESTINATARIO\t\tDIRECCION\t\tCIUDAD\t\tDEPARTAMENTO\tPESO\tTELEFONO\tREFERENCIA\r\n";
        $contenidoCSV="NOMBRE DESTINATARIO;DIRECCION;CIUDAD;DEPARTAMENTO;PESO;TELEFONO;REFERENCIA;\n";
        $contenido2 .="\t\t</tr>\n";
	$head = "<html lang='es'><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'></head><body>
        <div id='head' style='text-align:center;'><span style='text-align:center;'>$entidad_largo_Planilla</span></div>
<p>Planilla No.: $no_planillaC</p>
<p>Fecha de emisi&oacute;n " . $fecha . "</p>";
	$footer = "<br><br><table width='100%' border='1'  cellpadding='0' cellspacing='0' style='border-collapse: collapse;font-style:normal;font-size:12;letter-spacing: 0.1em;font-stretch: ultra-expanded;'>
<tr><td  width='50%' align='center'><br><br><br></td><td  width='50%' align='center'></td></tr>
<tr><td  width='50%' align='left'>Elabor&oacute;:</td><td  width='50%' align='left'>Recibi&oacute;:</td></td></tr>
</table></body></html>";
        for($i=0;$i<$numelem2;$i++){
            $tabla .="\t\t<tr>\n";
                $tabla .="\t\t\t<td style='height:100px;'>".$dataplani[$i]['destinatario']."</td>\n";
                $tabla .="\t\t\t<td style='height:100px;'>".$dataplani[$i]['direccion']."</td>\n";
                $tabla .="\t\t\t<td style='height:100px;'>".$dataplani[$i]['ubicacionm']."</td>\n";
                $tabla .="\t\t\t<td style='height:100px;'>".$dataplani[$i]['ubicaciond']."</td>\n";
                $tabla .="\t\t\t<td style='height:100px;'>".$dataplani[$i]['peso']."</td>\n";
                $tabla .="\t\t\t<td style='height:100px;'>".$dataplani[$i]['telefono']."</td>\n";
                $tabla .="\t\t\t<td style='height:100px;'>".$dataplani[$i]['numrad']."</td>\n";
            $tabla .="\t\t</tr>\n";
            $contenidoCSV.=str_replace('\t',' ',  str_replace('\n','',trim($dataplani[$i]['destinatario']).";".trim($dataplani[$i]['direccion']).";".trim($dataplani[$i]['ubicacionm']).";".trim($dataplani[$i]['ubicaciond']).";".trim($dataplani[$i]['peso']).";".trim($dataplani[$i]['telefono']).";".trim($dataplani[$i]['numrad'])))."\n";
            $contenidoTxt.=str_replace('\t', ' ', str_replace('\n','', trim($dataplani[$i]['destinatario'])))."\t";
            $contenidoTxt.=str_replace('\t', ' ', str_replace('\n','', trim($dataplani[$i]['direccion'])))."\t";
            $contenidoTxt.=str_replace('\t', ' ', str_replace('\n','', trim($dataplani[$i]['ubicacionm'])))."\t";
            $contenidoTxt.=str_replace('\t', ' ', str_replace('\n','', trim($dataplani[$i]['ubicaciond'])))."\t";
            $contenidoTxt.=str_replace('\t', ' ', str_replace('\n','', trim($dataplani[$i]['peso'])))."\t";
            $contenidoTxt.=str_replace('\t', ' ', str_replace('\n','', trim($dataplani[$i]['telefono'])))."\t";
            $contenidoTxt.=str_replace('\t', ' ', str_replace('\n','', trim($dataplani[$i]['numrad'])))."\r\n";
        }
        $tabla .="\t\t</table>\n";
	/*$contenido2 .= "<tr height='15'><td></td><td></td><td></td><td></td><td></td><td></td><td width='10%'></td>
                  </tr><tr height='15'><td></td>
                  <td></td><td></td><td></td><td></td><td></td><td width='10%'></td></tr></table>" . $footer;*/
        $contenido2 =$contenido2.$tabla.$footer;
	$html .= $head . $contenido2;
	//Llamado a la libreria que arma el PDF
        include_once("../../../../extra/MPDF57/mpdf.php");
        $mpdf=new mPDF('es-ES','legal-L');
        $mpdf->WriteHTML($html);
        // ruta de guardado
        $arpdf_tmp = RUTA_BODEGA . "/pdfs/planillas/$no_planillaC.pdf";
        //salida
        //Copiar al  disco
        $mpdf->Output($arpdf_tmp,'F');
        //Archivo  csv
	$nombre_archivo = RUTA_BODEGA . "/pdfs/planillas/$no_planillaC.csv";
	fopen ( $nombre_archivo, 'wra+' );
	
	// Asegurarse primero de que el archivo existe y puede escribirse sobre el. 
	if (is_writable ( $nombre_archivo )) {
		
		// En nuestro ejemplo estamos abriendo $nombre_archivo en modo de adicion. 
		// El apuntador de archivo se encuentra al final del archivo, asi que 
		// alli es donde ira $contenido cuando llamemos fwrite(). 
		if (! $gestor = fopen ( $nombre_archivo, 'a' )) {
			echo "No se puede abrir el archivo ($nombre_archivo)";
			exit ();
		}
		
		// Escribir $contenido a nuestro arcivo abierto. 
		if (fwrite ( $gestor, $contenidoCSV ) === FALSE) {
			echo "No se puede escribir al archivo ($nombre_archivo)";
			exit ();
		}
		
		//echo "&Eacute;xito, se escribi&oacute; ($contenidoCsv) d al archivo ($nombre_archivo)"; 
		

		fclose ( $gestor );
	
	} else {
		echo "No se puede escribir sobre el archivo $nombre_archivo";
	}
        //Archivo  TXT
	$nombre_archivo2 = RUTA_BODEGA . "pdfs/planillas/$no_planillaC.txt";
	fopen ( $nombre_archivo2, 'wra+' );
	
	// Asegurarse primero de que el archivo existe y puede escribirse sobre el. 
	if (is_writable ( $nombre_archivo2 )) {
		
		// En nuestro ejemplo estamos abriendo $nombre_archivo en modo de adicion. 
		// El apuntador de archivo se encuentra al final del archivo, asi que 
		// alli es donde ira $contenido cuando llamemos fwrite(). 
		if (! $gestor = fopen ( $nombre_archivo2, 'a' )) {
			echo "No se puede abrir el archivo ($nombre_archivo2)";
			exit ();
		}
		
		// Escribir $contenido a nuestro arcivo abierto. 
		$tmp = chr(255).chr(254).mb_convert_encoding( $contenidoTxt, 'UTF-16LE', 'UTF-8'); 
		if (fwrite ( $gestor, $tmp ) === FALSE) {
			echo "No se puede escribir al archivo ($nombre_archivo2)";
			exit ();
		}
		
		//echo "&Eacute;xito, se escribi&oacute; ($contenidoCsv) d al archivo ($nombre_archivo)"; 
		

		fclose ( $gestor );
	
	} else {
		echo "No se puede escribir sobre el archivo $nombre_archivo";
	}
        $resultado ['fecha']= $fecha;
	$resultado ['html'] = $html;
        $resultado ['pdf'] = $arpdf_tmp;
	$resultado ['csv'] = $nombre_archivo;
	$resultado ['txt'] = $nombre_archivo2;
	return $resultado;
    }
}

?>
