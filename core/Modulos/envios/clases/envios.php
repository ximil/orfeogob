<?php 
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of envios
 *
 * @author derodriguez Diego E. Rodriguez D. IEA
 * 
 */
if (! $ruta_raiz)
	$ruta_raiz = '../../../..';
    include_once "$ruta_raiz/core/Modulos/envios/modelo/modeloEnvios.php";

class envios{
    //El id del envio
    private $idEnvio;
    //Medio de envio
    private $medioEnvio;
    //Descripci&oacute;n u observaci&oacute;n del envio
    private $descripcionEnvio;
    //El estado del documento Anexo
    private $estaCodi;
    //El n&uacute;mero de la dependencia
    private $radiDepeRadi;
    //C&oacute;digo del usuario
    private $usuaCodi;
    //N&uacute;mero del radicado
    private $radiNumeRadi;
    //Peso del envio
    private $pesoEnvio;
    //Rol del usuario
    private $rolUsua;
    //Variable de validacion de fechas
    private $FechaIni;
    //Variable de validacion de fechas
    private $FechaFin;
    //Variable que trae el reporte de envios
    private $Reporte;
    //Variable de tamaño del reporte 
    private $NumReporte;
    //Variable para diferenciar si el radicado tiene mas de una copia
    private $Copia;
    //Varible que define el modelo Envios
    private $modelo;
    
    /*function __construct
     * Es la funcion constructora del modelo
     * 
     * @param character $ruta_raiz La ruta raiz del aplicativo
     * 
     */	
	function __construct($ruta_raiz) {

		
	    $this->modelo = new modeloEnvio( $ruta_raiz );
	}
	
    /*function getIdEnvio
     * 
     * Funcion encargada de devolver el valor de idEnvio
     * 
     * @return int idEnvio
     * 
     */
    public function getIdEnvio() {
        return $this->idEnvio;
    }
    /*function setIdEnvio
     *Funcion encargada de darle el valor al idEnvio 
     *
     *@param  int idEnvio
     * 
     */
    public function setIdEnvio($idEnvio) {
        $this->idEnvio = $idEnvio;
    }

    /*function getDescripcionEnvio
     * 
     * Devuelve el valor descripcionEnvio
     * 
     * @return character descripcionEnvio
     */
    public function getDescripcionEnvio() {
        return $this->descripcionEnvio;
    }
    
    /*function setDescripcionEnvio
     * Asigna el valor del descripcion del envio
     * 
     * @param descripcionEnvio 
     * 
     */
    public function setDescripcionEnvio($descripcionEnvio) {
        $this->descripcionEnvio = $descripcionEnvio;
    }
    
    /*function getEstaCodi
     * Devuelve el valor estado del anexo(los chulos)
     * 
     * @return integer Valor devuelvo del estado chulo del anexo del documento
     */

    public function getEstaCodi() {
        return $this->estaCodi;
    }
    
    /*function setEstaCodi
     * Asigna el valor del estado del anexo
     * 
     * @param integer Asigna este valor el de estado de codigo
     */
    
    public function setEstaCodi($estaCodi) {
        $this->estaCodi = $estaCodi;
    }

    /*function getRadiDepeRadi
     * Devuelve el n&uacute;mero de la dependencia del radicado
     * 
     * @return numeric radiDepeRadi
     */
    
    public function getRadiDepeRadi() {
        return $this->radiDepeRadi;
    }
    
    /*function setRadiDepeRadi
     * Asigna el valor de la dependencia del radicado
     * 
     * @param numeric radiDepeRadi
     */

    public function setRadiDepeRadi($radiDepeRadi) {
        $this->radiDepeRadi = $radiDepeRadi;
    }
    
    /*function getUsuaCodi
     * Devuelve el valor del codigo del usuario
     * 
     * @return numeric usuaCodi
     * 
     */
    
    public function getUsuaCodi(){
        return $this->usuaCodi;
    }
    
    /*function setUsuaCodi
     * Asigna el valor del codigo del usuario
     * 
     * @param numeric usuaCodi
     * 
     */
    
    public function setUsuaCodi($usuaCodi){
        $this->usuaCodi = $usuaCodi;
    }
    
    /*function getMedioEnvio
     * Devuelve el numero del codigo del medio de envio
     * 
     * @param numeric medioEnvio 
     */

    public function getMedioEnvio() {
        return $this->medioEnvio;
    }
    
    /*function setMedioEnvio
     * Asigna el valor del codigo del medio de envio
     * 
     * @return numeric medioEnvio Devuelve el valor el valor del medio de envio
     */

    public function setMedioEnvio($medioEnvio) {
        $this->medioEnvio = $medioEnvio;
    }
    
    /*function getRadiNumeRadi
     * Devuelve el numero del radicado
     * 
     * @return numeric radiNumeRadi 
     * 
     */
    
    public function getRadiNumeRadi(){
        return $this->radiNumeRadi = $radiNumeRadi;
    }
    
    /*function setRadiNumeRadi
     * Asigna el numero del radicado
     * 
     * @param numeric radiNumeRadi
     */
    public function setRadiNumeRadi($radiNumeRadi){
        $this->radiNumeRadi=$radiNumeRadi;  
    }
    /*function getPesoEnvio
     * Devuelve el valor del peso de los documentos enviados
     * 
     * @return numeric pesoEnvio
     * 
     */
    
    public function getPesoEnvio() {
        return $this->pesoEnvio;
    }
    
    /*function setPesoEnvio
     *Asigna a la variable el valor del peso de los documentos del envio 
     *
     * @param numeric pesoEnvio
     * 
     */
    
    public function setPesoEnvio($pesoEnvio) {
        $this->pesoEnvio = $pesoEnvio;
    }
    
    /*function getRolUsua
     * Devuelve el valor de id del rol del usuario
     * 
     * @return numeric rolUsua
     */

    public function getRolUsua(){
        return $this->rolUsua;
    }
    
    /*function setRolUsua
     * Asigna el numero del rol del usuario
     * 
     * @param numeric
     */
    public function setRolUsua($rolUsua){
        $this->rolUsua=$rolUsua;
    }
    /*function getFechaIni
     * Devuelve el valor de la fecha inicial de busqueda
     * 
     * @return date 
     * 
     */
    public function getFechaIni() {
        return $this->FechaIni;
    }
    /*function getFechaIni
     * Asigna el valor de la fecha inicial de busqueda
     * 
     * @param date 
     * 
     */
    public function setFechaIni($FechaIni) {
        $this->FechaIni = $FechaIni;
    }
    /*function getFechaIni
     * Devuelve el valor de la fecha inicial de busqueda
     * 
     * @return date 
     * 
     */
    public function getFechaFin() {
        return $this->FechaFin;
    }
    
    /*function setFechaFin
     * Asigna el valor de la fecha final de busqueda
     * 
     * @param date $FechaFin
     */

    public function setFechaFin($FechaFin) {
        $this->FechaFin = $FechaFin;
    }
    /*function getReporte
     * Devuelve el reporte generado en envios
     * 
     * @return array
     */
    public function getReporte() {
        return $this->Reporte;
    }
    /*function setReporte
     * Asigna el reporte realizado a envios
     * 
     * @param array
     */
    public function setReporte($Reporte) {
        $this->Reporte = $Reporte;
    }
    /*function getNumReporte
     * 
     * @return numeric
     */
    public function getNumReporte() {
        return $this->NumReporte;
    }
    /*function setNumReporte
     * 
     * @param numeric
     */

    public function setNumReporte($NumReporte) {
        $this->NumReporte = $NumReporte;
    }

    public function getCopia() {
        return $this->Copia;
    }

    public function setCopia($Copia) {
        $this->Copia = $Copia;
    }

            
     function consultarxEstado(){
        return $this->modelo->consultarxEstado($this->radiDepeRadi, $this->estaCodi,$this->Copia);
    }       

    function consultarxRadicado(){
        return $this->modelo->consultarxRadicado($this->radiNumeRadi, $this->estaCodi,$this->Copia);
    }
    
    function accionxEstado(){      
        return $this->modelo->accionxEstado($this->radiNumeRadi,  $this->estaCodi, $this->radiDepeRadi, $this->usuaCodi, $this->rolUsua,  $this->Copia);
    }
    
    function infoxAccion(){
        return $this->modelo->infoxAccion($this->radiNumeRadi,  $this->Copia);
    }
    
    function infoxModif(){
        return $this->modelo->infoxModif($this->radiNumeRadi, $this->Copia);
    }
    
    function consultarMedioEnvio(){
        return $this->modelo->consultarMedioEnvio();
    }
    
    function generarEnvio(){
        $this->modelo->generarEnvio($this->radiNumeRadi, $this->usuaCodi, $this->radiDepeRadi ,$this->medioEnvio, $this->descripcionEnvio, $this->pesoEnvio, $this->Copia);
    }
    function modificarEnvio(){
        $this->modelo->modificarEnvio($this->Copia, $this->radiNumeRadi ,$this->descripcionEnvio,  $this->medioEnvio, $this->pesoEnvio);
    }
    function consultaEModificable(){
        return $this->modelo->consultaEModificable($this->radiDepeRadi);
    }
    function consultaRModificable(){
        return $this->modelo->consultaRModificable($this->radiNumeRadi);
    }
    
    
    function estadisticaEnvios(){
        return $this->modelo->estadisticaEnvios($this->radiDepeRadi, $this->medioEnvio,  $this->FechaIni,  $this->FechaFin);
    }
    
    function generarReporte(){
        $reporte=$this->Reporte;
        $numreporte=  $this->NumReporte;
        $fecha_ini=$this->FechaIni;
        $fecha_fin=$this->FechaFin;
        $depus=  $this->radiDepeRadi;
        $codus= $this->usuaCodi;
        date_default_timezone_set("America/Bogota");
        $fecha= date('Y-m-d'); 
        $hora=date("h:i");
        $head="<html lang='es'><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'></head><body><table width='100%' border='1'   style='border-collapse: collapse;font-style:normal;font-size:8;letter-spacing: 0.1em;font-stretch: ultra-expanded;'>
<tr><td  colspan='3' align='center'>Informe de reporte de envios</td></tr>
<tr><td width='30%'></td><td  align='center'>Fecha y Hora Generacion de Reporte</td><td  align='center'>".$fecha. " ".$hora."</td></tr>
<tr><td>Reporte de $numreporte registro(s)</td><td  align='center'>Fecha Rango</td><td align='center'>Desde ".$fecha_ini." hasta " .$fecha_fin. "</td></tr></table>";
        $contenidoh = "<br><br><table width='99%' border='1'  cellpadding='0' cellspacing='0' style='border-collapse: collapse;font-style:normal;font-size:7;text-align:center;letter-spacing: 0.1em;font-stretch: ultra-expanded;vertical-align:inherit;'>
<tr bgcolor='#999999'><td width='20%'>DEPENDENCIA</td><td width='20%'>RADICADO</td><td width='10%'>DESTINATARIO</td><td width='10%'>DIRECCION</td><td width='20%'>MUNICIPIO</td><td width='20%'>DEPARTAMENTO</td><td>PAIS</td><td width='10%'>FECHA</td><td width='10%'>MEDIO DE<br>ENVIO</td><td width='10%'>
No. Planilla</td><td width='10%'>Origen de<br>Envio</td></tr>";
        $contenidoCSV="DEPENDENCIA;RADICADO;DESTINATARIO;DIRECCION;MUNICIPIO;DEPARTAMENTO;PAIS;FECHA;MEDIO DE ENVIO\n";
            $contenido="";
            $contenido2="";
        if($numreporte!=0){
            for($i=0;$i<count($reporte)-1;$i++){
                /*if($i%28==0 && $i!=0)
                    $contenido2.=$contenidoh;*/
                $depe=$reporte[$i]['dependencia'];
                $numrad=$reporte[$i]['numrad'];
                $destinatario=$reporte[$i]['destinatario'];
                $direccion=$reporte[$i]['direccion'];
                $municipio=$reporte[$i]['municipio'];
                $depto=$reporte[$i]['departamento'];
                $pais=$reporte[$i]['pais'];
                $fecha=$reporte[$i]['fecha'];
                $medenvio=$reporte[$i]['medenvio'];
                $planilla=$reporte[$i]['planilla'];
                $origen=$reporte[$i]['origen'];
                $contenidoCSV.=str_replace('\t',' ',str_replace('\n','',trim ( $depe ) . ";" . trim ( $numrad )  . ";" . trim ( $destinatario )  .";".trim($municipio).";" . trim ( $depto ) . ";" . trim ( $pais ). ";" . trim ( $fecha ). ";" . trim ( $medenvio ).";".trim($planilla).";".trim($origen)  ))."\n";
                $contenido.="<tr><td>$depe</td><td>$numrad</td><td>$destinatario</td><td>$direccion</td><td>$municipio</td>
                    <td>$depto</td><td>$pais</td><td>$fecha</td><td>$medenvio</td><td>$planilla</td><td>$origen</td></tr>";
                $contenido2.="<tr><td>$depe</td><td>$numrad</td><td>$destinatario</td><td>$direccion</td><td>$municipio</td>
                    <td>$depto</td><td>$pais</td><td>$fecha</td><td>$medenvio</td><td>$planilla</td><td>$origen</td></tr>";
            }
            $contenido.="<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr></table></body></html>";
            $contenido2.="<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr></table></body></html>";
        }
        $html=$head.$contenidoh.$contenido;
        //$html2pdf=$head.$contenidoh.$contenido2;
//Retirado dompdf por renderizado, comienzo pruebas con nPDF
        /*require_once ("../../../../extra/dompdf/dompdf_config.inc.php");
        $dompdf = new DOMPDF ();
	$dompdf->load_html ( $html2pdf,'UTF-8');
	//	tipo de palpe
	$dompdf->set_paper ( 'legal', 'landscape' );
	//$dompdf->set_paper('portrait');//Paper orientation ('portrait' or 'landscape') 
	// renderizando
	$dompdf->render ();*/
	// ruta de guardado
	include_once("../../../../extra/MPDF57/mpdf.php");
	$mpdf=new mPDF('es-ES','legal-L');
	$mpdf->WriteHTML($html);
        $dia=date('d');
        $mes=date('m');
        $anyo=date('Y');
        $fecha2="$codus.$depus"."_"."$anyo-$mes-$dia"."_$hora";
	$arpdf_tmp = RUTA_BODEGA . "/pdfs/guias/ReporteEnvios$fecha2.pdf";
	//salida 
	//$pdf = $dompdf->output ();
	//Copiar a la bodega
	//file_put_contents ( $arpdf_tmp, $pdf );
	$mpdf->Output($arpdf_tmp,'F');
        //Ruta de guardado del CSV
        $arcsv = RUTA_BODEGA ."/pdfs/guias/ReporteEnvios$fecha2.csv";
        fopen ( $arcsv, 'wra+' );
	
	// Asegurarse primero de que el archivo existe y puede escribirse sobre el. 
	if (is_writable ( $arcsv )) {
		
		// En nuestro ejemplo estamos abriendo $nombre_archivo en modo de adicion. 
		// El apuntador de archivo se encuentra al final del archivo, asi que 
		// alli es donde ira $contenido cuando llamemos fwrite(). 
		if (! $gestor = fopen ( $arcsv, 'a' )) {
			echo "No se puede abrir el archivo ($arcsv)";
			exit ();
		}
		
		// Escribir $contenido a nuestro arcivo abierto. 
		if (fwrite ( $gestor, $contenidoCSV ) === FALSE) {
			echo "No se puede escribir al archivo ($arcsv)";
			exit ();
		}
		
		//echo "&Eacute;xito, se escribi&oacute; ($contenidoCvs) d al archivo ($nombre_archivo)"; 
		

		fclose ( $gestor );
	
	} else {
		echo "No se puede escribir sobre el archivo $arcsv";
	}
	$resultado ['html'] = $html;
	$resultado ['pdf'] = $arpdf_tmp;
        $resultado ['csv'] = $arcsv;
        return $resultado;
    }

}

?>
