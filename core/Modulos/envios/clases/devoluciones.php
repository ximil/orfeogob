<?php 
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
if (! $ruta_raiz)
	$ruta_raiz = '../../../..';
    include_once "$ruta_raiz/core/Modulos/envios/modelo/modeloDevoluciones.php";
/**
 * Description of devoluciones
 *
 * @author derodriguez Diego E. Rodriguez D. IEA (Ideas En Acción)
 */
class devoluciones {
    //N&uacute;mero del radicado
    private $NumeRadi;
    //N&uacute;mero de copia 
    private $Copia;
    //Motivo de devoluci&oacute;n
    private $MotiDevol;
    //Observaci&oacute;n de la devoluci&oacute;n
    private $ObserDevol;
    //Medio de envio usuado
    private $MedEnvio;
    //N&uacute;mero de la dependencia
    private $NumeDepe;
    //Estado del radicado
    private $EstaRadi;
    //C&oacute;digo del usuario que registro la devoluci&oacute;n del envio
    private $UsuaCodi;
    //El rol que tiene el usuario de la devoluci&oacute;n
    private $RolId;
    //Reporte de las devoluciones
    private $Reporte;
    //Numero de filas en el reporte
    private $NumReporte;
    //Fecha inicial reporte
    private $FechaIni;
    //Fecha final reporte
    private $FechaFin;
    //N&uacute;mero de documento de usuario
    private $DocUsuario;
    //El modelo del radicado
    private $modelo;
    
    public function __construct($ruta_raiz) {
        $this-> modelo = new modeloDevoluciones($ruta_raiz);
    }


    public function getNumeRadi() {
        return $this->NumeRadi;
    }

    public function setNumeRadi($NumeRadi) {
        $this->NumeRadi = $NumeRadi;
    }
    public function getCopia() {
        return $this->Copia;
    }

    public function setCopia($Copia) {
        $this->Copia = $Copia;
    }

        public function getMotiDevol() {
        return $this->MotiDevol;
    }

    public function setMotiDevol($MotiDevol) {
        $this->MotiDevol = $MotiDevol;
    }

    public function getObserDevol() {
        return $this->ObserDevol;
    }

    public function setObserDevol($ObserDevol) {
        $this->ObserDevol = $ObserDevol;
    }

    public function getMedEnvio() {
        return $this->MedEnvio;
    }

    public function setMedEnvio($MedEnvio) {
        $this->MedEnvio = $MedEnvio;
    }

    public function getNumeDepe() {
        return $this->NumeDepe;
    }

    public function setNumeDepe($NumeDepe) {
        $this->NumeDepe = $NumeDepe;
    }
    
    public function getEstaRadi(){
        return $this->EstaRadi;
    }
    
    public function setEstaRadi($EstaRadi){
        $this->EstaRadi = $EstaRadi;
    }
    
    public function getUsuaCodi(){
        return $this->UsuaCodi;
    }
    
    public function setUsuaCodi($UsuaCodi){
        $this->UsuaCodi = $UsuaCodi;
    }
    
    public function getRolId(){
        return $this->RolId;
    }
    public function setRolId($RolId){
        $this->RolId = $RolId;
    }
    
    public function getReporte() {
        return $this->Reporte;
    }

    public function setReporte($Reporte) {
        $this->Reporte = $Reporte;
    }

    public function getNumReporte() {
        return $this->NumReporte;
    }

    public function setNumReporte($NumReporte) {
        $this->NumReporte = $NumReporte;
    }

    public function getFechaIni() {
        return $this->FechaIni;
    }

    public function setFechaIni($FechaIni) {
        $this->FechaIni = $FechaIni;
    }

    public function getFechaFin() {
        return $this->FechaFin;
    }

    public function setFechaFin($FechaFin) {
        $this->FechaFin = $FechaFin;
    }
    public function getDocUsuario() {
        return $this->DocUsuario;
    }

    public function setDocUsuario($DocUsuario) {
        $this->DocUsuario = $DocUsuario;
    }

                    
    function consTodoxDevolver(){
        return $this->modelo->consTodoxDevolver($this->NumeDepe);
    }
    
    function consxDevolver(){
        return $this->modelo->consxDevolver($this->NumeRadi);
    }
    
    function consulxDevolver(){
        return $this->modelo->consulxDevolver($this->NumeRadi,  $this->Copia);
    }
    
    function generarMotivoDev(){
        return $this->modelo->generarMotivoDev();
    }
    function reportarDevolucion(){
        return $this->modelo->reportarDevolucion($this->NumeRadi,  $this->MotiDevol, $this->UsuaCodi, $this->NumeDepe, $this->RolId, $this->ObserDevol, $this->DocUsuario,$this->Copia);
    }
    
    function estadisticaDevolucion(){
        return $this->modelo->estadisticaDevolucion($this->NumeDepe, $this->MedEnvio,$this->FechaIni,  $this->FechaFin);
    }
    
    function devolCTEspe(){
        return $this->modelo->devolCTEspe($this->NumeDepe, $this->FechaIni);
    }
    
    function devolTEspe(){
        return $this->modelo->devolTEspe($this->NumeDepe, $this->FechaIni, $this->FechaFin);
    }
    
    function devolxTEspe(){
        $this->modelo->devolxTEspe($this->NumeDepe, $this->FechaIni, $this->FechaFin);
    }
            
    function generarReporte($nomus,$nomdep){
        $reporte=$this->Reporte;
        $numreporte=  $this->NumReporte;
        $fecha_ini=$this->FechaIni;
        $fecha_fin=$this->FechaFin;
        $depus=  $this->NumeDepe;
        $codus= $this->UsuaCodi;
        date_default_timezone_set("America/Bogota");
	$contenido2='';
        $fecha= date('Y-m-d'); 
        $hora=date("g:i");
	$dia=date('d');
        $mes=date('m');
        $anyo=date('Y');
	switch($mes){
	   case 1:
		$nmes='Enero';
		break;
 	   case 2:
                $nmes='Febrero';
                break;
 	   case 3:
                $nmes='Marzo';
                break;
 	   case 4:
                $nmes='Abril';
                break;
 	   case 5:
                $nmes='Mayo';
                break;
 	   case 6:
                $nmes='Junio';
                break;
 	   case 7:
                $nmes='Julio';
                break;
 	   case 8:
                $nmes='Agosto';
                break;
 	   case 9:
                $nmes='Septiembre';
                break;
 	   case 10:
                $nmes='Octubre';
                break;
 	   case 11:
                $nmes='Noviembre';
                break;
 	   case 12:
                $nmes='Diciembre';
                break;
	}
        $fecha2="$codus.$depus"."_"."$anyo-$mes-$dia"."_$hora";
        $head="<html lang='es'><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'></head><body><table width='100%' border='1'   style='border-collapse: collapse;font-style:normal;font-size:8;letter-spacing: 0.1em;font-stretch: ultra-expanded;'>
<tr><td  colspan='3' align='center'>Informe de reporte de devoluciones</td></tr>
<tr><td width='30%'></td><td  align='center'>Fecha y Hora Generacion de Reporte</td><td  align='center'>".$fecha. " ".$hora."</td></tr>
<tr><td>Reporte de $numreporte registro(s)</td><td  align='center'>Fecha Rango</td><td align='center'>Desde ".$fecha_ini." hasta " .$fecha_fin. "</td></tr></table>";
/*	$head2="<html lang='es'><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'></head><body>
	<table width='100%' border='1'   style='border-collapse: collapse;font-style:normal;font-size:12;letter-spacing: 0.1em;font-stretch: ultra-expanded;'>
<tr><td  colspan='2' rowspan='2' align='center'><img src='../../../../imagenes/dne/encabezado.jpg' style=' height: 100px;'><td>C&oacute;digo: AP-GDO-FO-04</td></td></tr>
	<tr><td>Version: 1.0</td></tr>
<tr><td  align='center' colspan='2'>PLANILLA DE DEVOLUCI&Oacute;N DE ENVIOS A LAS DEPENDENCIAS</td><td  align='left'>Fecha: 10/10/2012</td></tr></table>
	<p align='left' style='font-style:normal;font-size:14;letter-spacing: 0.1em;font-stretch: ultra-expanded;'>Dependencia: &nbsp;&nbsp; $nomdep &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Fecha: &nbsp; $dia de $nmes de $anyo  &nbsp;&nbsp;</p>";*/
        $contenidoh = "<br><br><table width='99%' border='1'  cellpadding='0' cellspacing='0' style='border-collapse: collapse;font-style:normal;font-size:12;text-align:center;letter-spacing: 0.1em;font-stretch: ultra-expanded;vertical-align:inherit;'>
<tr bgcolor='#999999'><td width='15%'>MEDIO DE<br>ENVIO</td><td width='20%'>DEPENDENCIA</td><td width='15%'>No. RADICADO</td><td width='10%'>FECHA DE<br>ENVIO</td><td width='20%'>DESTINATARIO</td><td width='20%'>DIRECCION</td><td width='10%'>MUNICIPIO</td><td width='10%'>DEPARTAMENTO</td><td width='10%'>PAIS</td><td width='10%'>
FECHA DE<br>DEVOLUCION</td><td width='10%'>MOTIVO DE<br>DEVOLUCION</td></tr>";
	$contenidoh2 = "<br><br><table width='99%' border='1'  cellpadding='0' cellspacing='0' style='border-collapse: collapse;font-style:normal;font-size:8;text-align:center;letter-spacing: 0.1em;font-stretch: ultra-expanded;vertical-align:inherit;'>
<tr bgcolor='#999999'><td width='20%'>MEDIO DE<br>ENVIO</td><td width='20%'>DEPENDENCIA</td><td width='10%'>No. RADICADO</td><td width='10%'>FECHA DE<br>ENVIO</td><td width='20%'>DESTINATARIO</td><td width='20%'>DIRECCION</td><td width='10%'>MUNICIPIO</td><td width='10%'>DEPARTAMENTO</td><td width='10%'>PAIS</td><td width='10%'>
FECHA DE<br>DEVOLUCION</td><td width='10%'>MOTIVO DE<br>DEVOLUCION</td></tr>";	
        $contenidoCSV="MEDIO DE ENVIO;DEPENDENCIA;No. RADICADO;FECHA DE ENVIO;DESTINATARIO;DIRECCION;MUNICIPIO;DEPARTAMENTO;PAIS;FECHA DE DEVOLUCION;MOTIVO DE DEVOLUCION\n";
            $contenido="";
        if($numreporte!=0){
            for($i=0;$i<count($reporte)-1;$i++){
                /*if($i%28==0 && $i!=0)
                    $contenido2.=$contenidoh;*/
                $medenvio=$reporte[$i]['medenvio'];
                $depen=$reporte[$i]['depen'];
                $numrad=$reporte[$i]['numrad'];
                $fechaenv=$reporte[$i]['fechaenv'];
                $destinatario=$reporte[$i]['destinatario'];
                $direccion=$reporte[$i]['direccion'];
                $municipio=$reporte[$i]['municipio'];
                $depto=$reporte[$i]['depto'];
                $pais=$reporte[$i]['pais'];
                $fechadeve=$reporte[$i]['fechadeve'];
                $motivo=$reporte[$i]['motivo'];
                $contenido.="<tr><td>$medenvio</td><td>$depen</td><td style='font-size:10'>$numrad</td><td>$fechaenv</td><td>$destinatario</td>
                    <td>$direccion</td><td>$municipio</td><td>$depto</td><td>$pais</td><td>$fechadeve</td><td>$motivo</td></tr>";
                /*$contenido2.="<tr><td>$medenvio</td><td>$depen</td><td>$numrad</td><td>$fechaenv</td><td>$destinatario</td>
                    <td>$direccion</td><td>$municipio</td><td>$depto</td><td>$pais</td><td>$fechadeve</td><td>$motivo</td></tr>";*/
                $contenidoCSV.=str_replace('\t',' ',str_replace('\n','',trim ( $medenvio ) . ";" . trim ( $depen )  . ";" . trim ( $numrad )  .";".trim($fechaenv).";" . trim ( $destinatario ) . ";" . trim ( $direccion ). ";" . trim ( $municipio ). ";" . trim ( $depto ).";".trim($pais).";".trim($fechadeve).";".trim($motivo)  ))."\n";
            }
	    /*$contenido2=$contenido."<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr></table>
		<br><p style='font-style:normal;font-size:14;letter-spacing: 0.1em;font-stretch: ultra-expanded;'>Proyect&oacute;: $nomus
		</body></html>";*/
            $contenido.="<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr></table><!--/body></html-->";
        }
        $html=$head.$contenidoh2.$contenido;
        //$html2=$head2.$contenidoh.$contenido2;
	//Carga la libreria MPDF
	include_once("../../../../extra/MPDF57/mpdf.php");
        $mpdf=new mPDF('es-ES','legal-L');
        //$mpdf->SetFooter('PLANILLA DE DEVOLUCION DE ENVIOS A LAS DEPENDENCIAS|Pagina {PAGENO} de {nb}');
        $mpdf->WriteHTML($html);
	$arpdf_tmp = RUTA_BODEGA . "/pdfs/guias/ReporteDevol$fecha2.pdf";
	//salida 
	//Copiar a la bodega
	$mpdf->Output($arpdf_tmp,'F');
        //Ruta de guardado del CSV
        $arcsv = RUTA_BODEGA ."/pdfs/guias/ReporteDevol$fecha2.csv";
        fopen ( $arcsv, 'wra+' );
	
	// Asegurarse primero de que el archivo existe y puede escribirse sobre el. 
	if (is_writable ( $arcsv )) {
		
		// En nuestro ejemplo estamos abriendo $nombre_archivo en modo de adicion. 
		// El apuntador de archivo se encuentra al final del archivo, asi que 
		// alli es donde ira $contenido cuando llamemos fwrite(). 
		if (! $gestor = fopen ( $arcsv, 'a' )) {
			echo "No se puede abrir el archivo ($arcsv)";
			exit ();
		}
		
		// Escribir $contenido a nuestro arcivo abierto. 
		if (fwrite ( $gestor, $contenidoCSV ) === FALSE) {
			echo "No se puede escribir al archivo ($arcsv)";
			exit ();
		}
		
		//echo "&Eacute;xito, se escribi&oacute; ($contenidoCvs) d al archivo ($nombre_archivo)"; 
		

		fclose ( $gestor );
	
	} else {
		echo "No se puede escribir sobre el archivo $arcsv";
	}
	$resultado ['html'] = $html;
	$resultado ['pdf'] = $arpdf_tmp;
        $resultado ['csv'] = $arcsv;
        return $resultado;
    }

}

?>
