<?php session_start(); date_default_timezone_set('America/Bogota'); date_default_timezone_set('America/Bogota');
//error_reporting(E_ALL);
//ini_set('display_errors','On');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//foreach ($_GET as $key => $valor)  $$key = $valor;
foreach ($_POST as $key => $valor)  $$key = $valor;
foreach ($_SESSION as $key => $valor)  $$key = $valor;
foreach ($_SERVER as $key => $valor)  $$key = $valor;
$ruta_raiz='../../../..';
include_once "$ruta_raiz/core/Modulos/envios/clases/envios.php";
$envios= new envios($ruta_raiz);
//Validando session
include_once "$ruta_raiz/core/vista/validadte.php";
//Cargando variables de log
if(isset($_SERVER['HTTP_X_FORWARD_FOR'])){
    $proxy=$_SERVER['HTTP_X_FORWARD_FOR'];
}else
    $proxy=$_SERVER['REMOTE_ADDR'];
include_once "$ruta_raiz/core/clases/log.php";
$log = new log($ruta_raiz);
$log->setAddrC($REMOTE_ADDR);
$log->setProxyAd($proxy);
//Cargando las dependencias de la entidad
include_once $ruta_raiz . '/core/clases/dependencia.php';
if(isset($depsel)){
    $dependencia=$depsel;
}
//Inicializando las dependencias
$depe = new dependencia($ruta_raiz);
$depe->setDepe_codi($dependencia);
$dependenciasX= $depe->consultarTodo();
$numdep=  count($dependenciasX);
$optionDepe="";
//Carga en una variable para imprimir las dependencias
for($i=0; $i<$numdep;$i++){
    $nomDepe = $dependenciasX [$i] ['depe_nomb'];
    $codDepe = $dependenciasX [$i] ['depe_codi'];
    $optionDepe .="\t \t \t<option ";
    if($codDepe == $dependencia){
        $optionDepe .="selected='selected'";
    }
    $optionDepe .=" value='$codDepe'>$codDepe - $nomDepe</option>\n";
}
if(!isset($nomcarpeta)){
    $nomcarpeta="Modificacion de envios";
}else{
    $nomcarpeta="Modificacion de envios";
}
//$encabezado=session_name() . "=" . session_id() . "&krd=$krd";
?>
<html>
    <head>
        <meta http-equiv="Cache-Control" content="cache">
        <meta http-equiv="Pragma" content="public">
        <!--link rel="stylesheet" href="<?php echo $ruta_raiz;?>/old/estilos/table.css" type="text/css"-->
        <link rel="stylesheet" href="<?php echo $ruta_raiz;?>/estilos/default/orfeo.css">
		<link rel="stylesheet" href="<?php echo $ruta_raiz; ?>/estilos/Style.css" type="text/css">
        <title>Prueba de Envios</title>
        <script type="text/javascript">
   /*function isNumberKey(evt)
   {
    var charCode = (evt.which) ? evt.which : event.keyCode
    var ctrlDown = evt.ctrlKey
        if ((charCode > 44 && (charCode < 48 || charCode > 57)) || (ctrlDown && charCode==86))
        return false;
    return true;
   }*/
   function valRadio(){
       var chk=document.forms["marcarModif"].elements["modifenv"];
       var marcado=false;
       for(var i=0; i<chk.length;i++){
           if(chk[i].checked){
               marcado=true;
               break;
           }
       }
       if(marcado==false){
           alert("Marque documento(s) a enviar");
           return false;
       }
       return true;
   }
        </script>
    </head>
    <body bgcolor="#FFFFFF">
        <?php include $ruta_raiz . '/core/Modulos/envios/vista/miniMenu.php' ?>
        <header id="page_header" style="width:100%">

            <h1>Modificaci&oacute;n de Envios</h1>
            <?php             if(!isset($modifenv)){
            ?>
            <form style="margin:0" action="modifEnvio.php" method="post">
                <table style="width:100% ; border-spacing: 1 " class="tabS">
                    <tr>
                        <td class="titulo1" style="width:33%">CARPETA DE <?php echo strtoupper($nomcarpeta); ?></td>
                        <td class="titulo1" style="width:33%"><center><?php echo $usua_nomb;?></center></td>
                        <td class="titulo1" style="width:33%">Dependencia del sistema
                        <select class="select" name="depsel" onchange='this.form.submit()'><?php echo $optionDepe;?></select></td>
                    </tr>
                </table>
                <table width="100%" border="0" cellpadding="0" cellspacing="2" class="tabS">
                    <tr class="tablas">
                        <td style="width:25%">N&uacute;mero de radicado</td>
                        <td colspan="2" ><center><input type="text" name="numrad_sol" size="100" id="txtCra" autocomplete="off"/></center></td>
                        <td style="width: 10%" ><input type="hidden" name="nomcarpeta" value="<?php echo $nomcarpeta;?>"><input type="submit" name="submitrad" value="Buscar" class="botones"></td>
                    </tr>
                </table>
            </form>
            <table width="100%" class="tabS"><tr><th class="titulos2"><input type="submit" name="submodif" form="marcarModif" value="Modificar Envio" class='botones_largo'></th></tr></table>
            </header>
            <?php             $correrad=0;
            if(isset($numrad_sol)){
                $trimnumrad=preg_replace('/[\s]+/', '', $numrad_sol );
                if(is_numeric(preg_replace('/,/','', $trimnumrad))){
                       $multirad=preg_split("/,/",$trimnumrad);
                        $j=0;
                        for($h=0;$h<count($multirad);$h++){
                            $lenum=strlen($multirad[$h]);
                            if($lenum>=6 && $lenum<=14){
                                $radxeva[$j] = $multirad[$h];
                                $j++;
                            }
                        }
                        if($j>0){
                            $correrad=2;
                        }
                } if(is_numeric($trimnumrad) && strlen($trimnumrad)==14){
                    $correrad=1;
                }
            }

        $envios->setRadiDepeRadi($dependencia);
            //
            if(isset($submitrad) && $correrad!=0){
                if($correrad==1){
                    $envios->setRadiNumeRadi($numrad_sol);
                    $recordEM=$envios->consultaRModificable();
                }else{
                    $envios->setRadiNumeRadi($radxeva);
                    $recordEM=$envios->consultaRModificable();
                }
                if($recordEM['error']!=""){
                    echo $recordEM['error'];
                    unset($recordEM);
                    $recordEM=$envios->consultaEModificable();
                }
            }
            else{
                $recordEM=$envios->consultaEModificable();
            }
            if ($recordEM['error']!= ""){
                echo $recordEM['error'];
            }else{
            ?>
            <div style=" padding : 4px; width :100%; height : 400px; overflow : auto;">
                <form action="modifEnvio.php" id="marcarModif" method="post" onsubmit="return valRadio();">
                    <input type="radio" name="modifenv" value="" id="chk000" style="display:none">
        <!--table style="width:100%">
            <tr class="grisCCCCCC">
                <!--div style="padding : 4px; width :100%; height : 400px; overflow : auto;" -->
                    <table style="width:100% ; border-spacing: 1 ">
                        <tr class="titulos4">
                            <th width="8%" align="center">N&uacute;mero del<br>radicado</th>
                            <th  align="center">CP</th>
                            <th width="8%" align="center">Radicado<br>padre</th>
                            <th width="10%" align="center">Fecha de envio</th>
                            <th width="10%" align="center">Planilla</th>
                            <th width="20%" align="center">Destinatario</th>
                            <th width="10%" align="center">Direcci&oacute;n</th>
                            <th width="10%" align="center">Pais</th>
                            <th width="10%" align="center">Departamento</th>
                            <th width="10%" align="center">Municipio</th>
                            <th width="10%" align="center">Empresa de<br>Envio</th>
                            <th width="10%" align="center">Usuario<br/>Actual</th>
                            <th></th>
                            <th></th>
                        </tr>

            <?php
                for ($i = 0; $i < (count($recordEM)-1); $i++) {
                    $numrad        = $recordEM[$i]['numrad'];
                    $radi_padre    = $recordEM[$i]['radi_padre'];
                    $fecha_envio     = $recordEM[$i]['fecha_envio'];
                    $planilla      = $recordEM[$i]['planilla'];
                    $destinatario  = $recordEM[$i]['destinatario'];
                    $direccion     = $recordEM[$i]['direccion'];
                    $pais          = $recordEM[$i]['pais'];
                    $departamento  = $recordEM[$i]['departamento'];
                    $municipio     = $recordEM[$i]['municipio'];
                    $medio_envio   = $recordEM[$i]['medio_envio'];
                    $usua_actu     = $recordEM[$i]['usua_actu'];
                    $valorenvio    = $recordEM[$i]['valorenvio'];
                    $peso          = $recordEM[$i]['peso'];
                    $codmenvio     = $recordEM[$i]['codmenvio'];
                    $copia         = $recordEM[$i]['copia'];
                    $valcop        = $recordEM[$i]['valcop'];
                    $subdata="$copia,$numrad,$peso,$codmenvio";
                    if($i%2 == 0)
                        $atr="listado1";
                    else{
                        $atr="listado2";
                    }
                ?>
              <tr>
                  <td  style="white-space: pre-line" class="<?php echo $atr?>" ><span class='leidos' align="center"><?php echo $numrad; ?></span></td>
                  <td  style="white-space: pre-line" class="<?php echo $atr?>" ><span class='leidos' align="center"><?php if($valcop!='')echo "$valcop"; else echo '-'; ?></span></td>
                  <td style="white-space: pre-line" class="<?php echo $atr;?>" ><span class='leidos'><?php echo $radi_padre; ?></span></td>
                  <td style="white-space: pre-line" class="<?php echo $atr;?>" ><span class='leidos'><?php echo $fecha_envio; ?></span></td>
                  <td style="white-space: pre-line" class="<?php echo $atr;?>" ><span class='leidos'><?php echo $planilla; ?></span></td>
                  <td style="white-space: pre-line" class="<?php echo $atr;?>" ><span class='leidos'><?php echo $destinatario;?></span></td>
                  <td style="white-space: pre-line" class="<?php echo $atr;?>" ><span class='leidos'><?php echo $direccion;?></span></td>
                  <td style="white-space: pre-line" class="<?php echo $atr;?>" ><span class='leidos'><?php echo $pais;?></span></td>
                  <td style="white-space: pre-line" class="<?php echo $atr;?>" ><span class='leidos'><?php echo $departamento;?></span></td>
                  <td style="white-space: pre-line" class="<?php echo $atr;?>" ><span class='leidos'><?php echo $municipio;?></span></td>
                  <td style="white-space: pre-line" class="<?php echo $atr;?>" ><span class='leidos'><?php echo $medio_envio;?></span></td>
                  <td style="white-space: pre-line" class="<?php echo $atr;?>" ><span class='leidos'><?php echo $usua_actu;?></span></td>
                  <td style="white-space: pre-line" class="<?php echo $atr;?>" ><input type="radio" name="modifenv" value="<?php echo $subdata;?>" id="<?php echo "chk$i";?>"></td>
                  <td></td>
              </tr>
            <?php              }?>
                    </table>
                <!--/div>
                    </tr>
                </table-->
        </form>
        </div>
        <?php             }
        }else{?>
            </header>
            <?php             if (!isset($confmodif)){
            $datos =  preg_split("/,/", $modifenv);
            $copia=$datos[0];
            $radcons=$datos[1];
            $pesocons=$datos[2];
            $medcons=$datos[3];
            ?>
            <form action="modifEnvio.php" method="post">
            <table style="width:100% ; border-spacing: 1 " class="tabS" >
                <tr class="titulos31">
                    <th>Medio de Envio</th>
                    <td>
                        <select name="medenvionew"  class="select">
                            <?php                             $recordME=$envios->consultarMedioEnvio();
                            for($i=0; $i < count($recordME); $i++ ){
                            ?>
                            <option value="<?php echo $recordME[$i]['codenvio']; ?>"<?php if($recordME[$i]['codenvio']==$medcons){echo "selected='selected'";} ?>>
                                <?php echo $recordME[$i]['codenvio']."-".$recordME[$i]['medenvio'];?></option>
                                <?php                                 }
                                ?>
                        </select>
                    </td>
                    <th>Peso (gramos)</th>
                    <td><input type="text" name="newpeso" value="<?php echo $pesocons;?>"></td>
                     <td style="width: 50%"></td>
                </tr>
            </table>
                <br>
        <table style="width:100% ; border-spacing: 1 " class="tabS">
                <tr class="titulo1">
                    <td>N&uacute;mero de radicado</td>
                    <td>Radicado padre</td>
                    <td>Destinatario</td>
                    <td>Direcci&oacute;n</td>
                    <td>Municipio</td>
                    <td>Depto.</td>
                    <td>Pa&iacute;s</td>
                </tr>
                <?php                 $envios->setCopia($copia);
                $envios->setRadiNumeRadi($radcons);
                $recordIA=$envios->infoxModif();


                ?>
                <tr class="listado2">
                    <td><span class='leidos'><?php echo $recordIA['numrad'];?></span></td>
                    <td><span class='leidos'><?php echo $recordIA['radi_padre'];?></span></td>
                    <td><textarea cols="30" rows="2" readonly><?php echo $recordIA['destinatario'];?></textarea></td>
                    <td><textarea cols="20" rows="2" readonly><?php echo $recordIA['direccion'];?></textarea></td>
                    <td><input type="text" readonly="true" value="<?php echo $recordIA['municipio'];?>"/></td>
                    <td><input type="text" readonly="true" value="<?php echo $recordIA['depto'];?>"></td>
                    <td><input type="text" readonly="true" value="<?php echo $recordIA['pais'];?>"></td>
                </tr>
                <tr class="titulos2">
                    <td colspan="2">Asunto  </td><td colspan="5"><input type="text" readonly="true" size="40" value="<?php echo $recordIA['asunto'];?>"></td>
                </tr>
                <tr class="titulos2">
                    <td colspan="2">Observaciones o Desc. de Anexos </td><td colspan="5"><input type="text" size="30" name="comen" value="<?php echo $recordIA['radi_desc']?>"></td>
                </tr>
             </table>
                <input type="hidden" name="modifenv" value="sigue">
                <input type="hidden" name="copia" value="<?php echo $copia;?>">
                <input type="hidden" name="radconf" value="<?php echo $radcons;?>">
            <center><input type="submit" name="confmodif" class="botones_largo" value="Confirmar modificaci&oacute;n"></center>
            </form>

        <?php
            }else{
                ?>
            <table style="width:100% ; border-spacing: 1;" class="tabS" >
                <tr class="titulos31">
                    <th>Medio de Envio</th>
                    <td>
                        <select disabled  class="select">
                            <?php                             $recordME=$envios->consultarMedioEnvio();
                            for($i=0; $i < count($recordME); $i++ ){
                            ?>
                            <option value="<?php echo $recordME[$i]['codenvio']; ?>"<?php if($recordME[$i]['codenvio']==$medenvionew){echo "selected='selected'";} ?>>
                                <?php echo $recordME[$i]['codenvio']."-".$recordME[$i]['medenvio'];?></option>
                                <?php                                 }
                                ?>
                        </select>
                    </td>
                    <th>Peso</th>
                    <td><input type="text" name="newpeso" readonly value="<?php echo $newpeso;?>"></td>
                     <td style="width: 50%"></td>
                </tr>
            </table>
            <br>
        <table style="width:100% ; border-spacing: 1 " class="tabS">
                <tr class="titulo1">
                    <td>N&uacute;mero de radicado</td>
                    <td>Radicado padre</td>
                    <td>Destinatario</td>
                    <td>Direcci&oacute;n</td>
                    <td>Municipio</td>
                    <td>Depto.</td>
                    <td>Pa&iacute;s</td>
                </tr>
                <?php                 //$comen="Envio exitoso y modificado";
                //echo "<b>$medenvionew , $newpeso , $idenvio</b>\n";
                $envios->setRadiNumeRadi($radconf);
                $envios->setCopia($copia);
                $envios->setPesoEnvio($newpeso);
                $envios->setMedioEnvio($medenvionew);
                $envios->setDescripcionEnvio($comen);
                $recordIA=$envios->infoxModif();
                $log->setUsuaCodi($codusuario);
                $log->setDepeCodi($dependencia);
                $log->setRolId($id_rol);
                $log->setDenomDoc($doc='Radicado');
                $log->setAction('document_deliver_modification');
                $log->setOpera('Modificacion de envio documento');
                $log->setNumDocu($radconf);
                $log->registroEvento();
                $envios->modificarEnvio();

                ?>
                <tr class="listado2">
                    <td><span class='leidos'><?php echo $recordIA['numrad'];?></span></td>
                    <td><span class='leidos'><?php echo $recordIA['radi_padre'];?></span></td>
                    <td><textarea cols="30" rows="2" readonly><?php echo $recordIA['destinatario'];?></textarea></td>
                    <td><textarea cols="20" rows="2" readonly><?php echo $recordIA['direccion'];?></textarea></td>
                    <td><input type="text" readonly="true" value="<?php echo $recordIA['municipio'];?>"/></td>
                    <td><input type="text" readonly="true" value="<?php echo $recordIA['depto'];?>"></td>
                    <td><input type="text" readonly="true" value="<?php echo $recordIA['pais'];?>"></td>
                </tr>
                <tr class="titulos2">
                    <td colspan="2">Asunto</td><td colspan="5">  <input type="text" readonly="true" size="40" value="<?php echo $recordIA['asunto'];?>"></td>
                </tr>
                <tr class="titulos2">
                    <td colspan="2">Observaciones o Desc. de Anexos </td><td colspan="5"><input type="text" readonly="true" size="30" value="<?php echo $comen;?>"></td>
                </tr>
             </table>
             <hr/>
             <b>Modificaci&oacute;n del envio realizada con exito</b>
             <hr/>
            <?php             }?>
          <!--  <center><a href="<?php echo $PHP_SELF;?>"><pre>Devolver al listado</pre></a></center>-->
        <?php         }

        ?>
    </body>
</html>
