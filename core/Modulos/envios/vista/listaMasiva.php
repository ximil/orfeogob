<?php session_start(); date_default_timezone_set('America/Bogota'); date_default_timezone_set('America/Bogota');
//error_reporting(E_ALL);
//ini_set('display_errors','On');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
foreach ($_GET as $key => $valor)   $$key = $valor;
foreach ($_POST as $key => $valor)   $$key = $valor;
foreach ($_SESSION as $key => $valor)   $$key = $valor;
foreach ($_SERVER as $key => $valor)   $$key = $valor;
$ruta_raiz='../../../..';
include_once "$ruta_raiz/core/Modulos/envios/clases/masivas.php";
$masivas= new masivas($ruta_raiz);
//Validando session
include_once "$ruta_raiz/core/vista/validadte.php";
//Cargando las dependencias de la entidad
include_once $ruta_raiz . '/core/clases/dependencia.php';
if(isset($depsel)){
    $dependencia=$depsel;
}
//Inicializando las dependencias
$depe = new dependencia($ruta_raiz);
$depe->setDepe_codi($dependencia);
$dependenciasX= $depe->consultarTodo();
$numdep=  count($dependenciasX);
$optionDepe="";
//Carga en una variable para imprimir las dependencias
for($i=0; $i<$numdep;$i++){
    $nomDepe = $dependenciasX [$i] ['depe_nomb'];
    $codDepe = $dependenciasX [$i] ['depe_codi'];
    $optionDepe .="\t \t \t<option ";
    if($codDepe == $dependencia){
        $optionDepe .="selected='selected'";
    }
    $optionDepe .=" value='$codDepe'>$codDepe - $nomDepe</option>\n";
}
if(!isset($nomcarpeta)){
    $nomcarpeta="masivas";
}else{
    $nomcarpeta="masivas";
}
//$encabezado=session_name() . "=" . session_id() . "&krd=$krd";
?>
<html>
    <head>
        <meta http-equiv="Cache-Control" content="cache">
        <meta http-equiv="Pragma" content="public">
        <link rel="stylesheet" href="<?php echo $ruta_raiz;?>/estilos/default/orfeo.css">
        <link rel="stylesheet" href="<?php echo $ruta_raiz; ?>/estilos/Style.css" type="text/css">
        <title>Prueba de listado de masivas</title>
        <script language="javascript">
            function valRadio(){
                var chk=document.forms["envMas"].elements["infomasiva"];
                var marcado=false;
                for(var i=0; i<chk.length;i++){
                    if(chk[i].checked){
                        marcado=true;
                        break;
                    }
                }
                if(marcado==false){
                    alert("Marque grupo de masivas a Enviar");
                    return false;
                }
            return true;
        }
        function valCB(){
            var chk=document.forms["remrad"].elements["listaGM[]"];
            var marcado=false;
            for(var i=0; i<chk.length;i++){
                if(chk[i].checked){
                    marcado=true;
                    break;
                }
            }
            if(marcado==false){
                alert("Marque radicado a excluir de la lista");
                return false;
                }
            return true;
   }
        </script>
    </head>
    <body bgcolor="#FFFFFF">
        <header id="page_header" style="width: 100%">
            <?php include $ruta_raiz.'/core/Modulos/envios/vista/miniMenu.php' ?>
            <h1>Lista de Radicados en Masivas</h1>
	    <form action="listaMasiva.php" method="post">
            <table  style="width:100% ; border-spacing: 1 " class="tabS">
                <tr >
                    <td class="titulo1" style="width:25%">LISTADO DE <?php echo strtoupper($nomcarpeta); ?></td>
                    <td class="titulo1" style="width:25%"><center><?php echo $usua_nomb;?></center></td>
                    <td class="titulo1" style="width:25%">Dependencia del sistema<select class="select" name="depsel" onchange='this.form.submit()'><?php echo $optionDepe;?></select></td>
                </tr>
            </table>
	    </form>
        </header>
        <?php         if(!isset($listaGM))
            unset($submasdel);
        $masivas->setDepeRadi($dependencia);
        if(!isset($gruporad) && !isset($submasdel)){
        //$envios->setEstaCodi($estarad);
        $recordMas=$masivas->listarMasivas($dependencia);
            if ($recordMas['error']!= ""){
                print($recordMas['error']);
            }else{
        ?>
        <!--table width="98%" align="center" cellspacing="0" cellpadding="0" class='borde_tab'-->
        <table style="width:100% ; border-spacing: 1 " class="tabS">
            <tr><td><center><input type="submit" class="botones_largo" name="sendM" value="Envio de documentos" form="envMas"></center></td></tr>
        </table>
        <div style=" padding : 4px; width :100%; height : 400px; overflow : auto;">
        <form action="envioMasivas.php" method="post" id="envMas" onsubmit="return valRadio();">
            <input type="radio" name="infomasiva" value="" id="chk000" style="display:none">
            <input type="hidden" name="depsel" value="<?php echo $dependencia?>">
        <!--table style="width:100%">
            <tr class="grisCCCCCC">
                <!--div style="padding : 4px; width :100%; height : 400px; overflow : auto;" -->
                    <table style="width:100% ; border-spacing: 1 ">
                        <tr class="titulos4">
                            <th align="center">Grupo</th>
                            <th align="center">Radicado Inicial</th>
                            <th align="center">Radicado Final</th>
                            <th align="center">Fecha de radicaci&oacute;n</th>
                            <th align="center">Documentos</th>
                            <th align="center">Excluidos del<br/>grupo</th>
                            <th align="center">Generado por</th>
                            <th align="center">Tipo</th>
                            <th align="center">Enviar</th>
                        </tr>
                        
            <?php  
                for ($i = 0; $i < (count($recordMas)-1); $i++) {
                    $grupo = $recordMas[$i]['grupo'];
                    $rad_ini = $recordMas[$i]['rad_ini'];
                    $rad_fin = $recordMas[$i]['rad_fin'];
                    $fecha   = $recordMas[$i]['fecha'];
                    $eliminados  = $recordMas[$i]['eliminados'];
                    $documentos  = $recordMas[$i]['documentos'];
                    $usua_nomb   = $recordMas[$i]['usua_nomb'];
                    $tipdocum    = $recordMas[$i]['tipdocum'];
                    if($i%2 == 0)
                        $atr="listado1";
                    else{
                        $atr="listado2";
                    }
                ?>
              <tr>
                  <td class="<?php echo $atr?>" align="center"><span class='leidos'><?php echo "<a href='listaMasiva.php?gruporad=$grupo&depsel=$dependencia'>$grupo</a>"; ?></span></td>
                  <td class="<?php echo $atr;?>" align="center"><span class='leidos'><?php echo $rad_ini; ?></span></td>
                  <td class="<?php echo $atr;?>" align="center"><span class='leidos'><?php echo $rad_fin; ?></span></td>
                  <td class="<?php echo $atr;?>" align="center"><span class='leidos'><?php echo $fecha; ?></span></td>
                  <td class="<?php echo $atr;?>" align="center"><span class='leidos'><?php echo $documentos;?></span></td>
                  <td class="<?php echo $atr;?>" align="center"><span class='leidos'><?php echo $eliminados; ?></span></td>
                  <td class="<?php echo $atr;?>" align="center"><span class='leidos'><?php echo $usua_nomb; ?></span></td>
                  <td class="<?php echo $atr;?>" align="center"><span class='leidos'><?php echo $tipdocum; ?></span></td>
                  <td class="<?php echo $atr;?>" align="center"><input type="radio" name="infomasiva" value="<?php echo "$grupo,$rad_ini,$rad_fin";?>" id="<?php echo "chk$i";?>"></td>
              </tr>
            <?php              }
        }
 
            ?>
                    </table>
        </form>
        </div>
    <?php     }  elseif (isset ($gruporad) && !isset ($submasdel)) {
        $masivas->setRadiGrupo($gruporad);
        $recordGM=$masivas->listarGrupoMasivas($gruporad,$dependencia);
        if ($recordGM['error']!= ""){
                print($recordGM['error']);
            }else{
        ?>
        <div style=" padding : 4px; width :100%; height : 400px; overflow : auto;">
            <form action="listaMasiva.php" method="post" id="remrad" onsubmit="return valCB();">
                <input type="hidden" name="depsel" value="<?php echo $dependencia?>">
                <input type="checkbox" name="listaGM[]" value="" id="chk000" style="display:none">
        <!--table style="width:100%">
            <tr class="grisCCCCCC">
                <!--div style="padding : 4px; width :100%; height : 400px; overflow : auto;" -->
                <input type="hidden" name="gruporad" value="<?php echo $gruporad;?>"><input type="submit" name="submasdel" value="Actualizar">
                    <table width="100%"  border="0"  cellpadding="0" cellspacing="1">
                        <tr class="titulos4">
                            <th align="center">N&uacute;mero de radicado</th>
                            <th align="center">Fecha de radicaci&oacute;n</th>
                            <th align="center">Nombre del Destinatario</th>
                            <th align="center">Departamento</th>
                            <th align="center">Municipio</th>
                            <th align="center">Direcci&oacute;n</th>
                            <th align="center">Excluidos</th>
                        </tr>
                        
            <?php                 
                for ($i = 0; $i < (count($recordGM)-1); $i++) {
                    $numrad = $recordGM[$i]['numrad'];
                    $codenvio = $recordGM[$i]['codenvio'];
                    $fecharad = $recordGM[$i]['fecharad'];
                    $dpto_nomb   = $recordGM[$i]['dpto_nomb'];
                    $muni_nomb  = $recordGM[$i]['muni_nomb'];
                    $destinatario  = $recordGM[$i]['destinatario'];
                    $direccion   = $recordGM[$i]['direccion'];
                    if($i%2 == 0)
                        $atr="listado2_center";
                    else{
                        $atr="titulos2";
                    }
                ?>
              <tr>
                  <td class="<?php echo $atr;?>" align="center"><?php echo $numrad; ?></td>
                  <td class="<?php echo $atr;?>" align="center"><?php echo $fecharad; ?></td>
                  <td class="<?php echo $atr;?>" align="center"><?php echo $destinatario; ?></td>
                  <td class="<?php echo $atr;?>" align="center"><?php echo $dpto_nomb;?></td>
                  <td class="<?php echo $atr;?>" align="center"><?php echo $muni_nomb; ?></td>
                  <td class="<?php echo $atr;?>" align="center"><?php echo $direccion; ?></td>
                  <td class="<?php echo $atr;?>" align="center"><input type="hidden" name="gruporad" value="<?php echo $gruporad;?>"><input type="checkbox" name="listaGM[]" value="<?php echo $numrad;?>"></td>
              </tr>
        
    <?php                 }?>
                    </table>
                </form>
            <center><a href="listaMasiva.php"><pre>Devolver a listado masiva</pre></a></center>
       <?php         }
    }elseif(isset ($gruporad) && isset ($submasdel) && !isset($subconfdel)){
        date_default_timezone_set("America/Bogota");
        $fecha=date('d/m/Y h:i:s A');
        $numelem=count($listaGM);
        $passdata="";
        $pdata="";
        $i=0;
        while($i<$numelem){
            $passdata .="\t\t\t<input type=\"hidden\" name=\"listaGM[]\" value=\"$listaGM[$i]\">\n";
            $pdata .="$listaGM[$i]";
            $i++;
            if($i<$numelem)
                $pdata .=",";
        }
    ?>
            <div style=" padding : 4px; width :100%; height : 400px; overflow : auto;">
                <form action="listaMasiva.php" id="regresar" method="post">
                    <input type="hidden" name="gruporad" value="<?php echo $gruporad; ?>">
                    <input type="hidden" name="depsel" value="<?php echo $dependencia?>">
                </form>
                <form action="listaMasiva.php" id="frcfdel">
                    <table width="40%"  border="0"  cellpadding="0" cellspacing="1">
                        <tr class="titulos4">
                            <th colspan="2">ACCION REQUERIDA COMPLETADA</th>
                        </tr>
                        <tr>
                            <td class="titulos2" align="center">ACCION REQUERIDA</td>
                            <td class="listado2">EDICION DE GRUPO DE MASIVA</td>
                        </tr>
                        <tr>
                            <td class="titulos2" align="center">GRUPO</td>
                            <td class="listado2"><?php echo $gruporad;?></td>
                        </tr>
                        <tr>
                            <td class="titulos2" align="center">USUARIO</td>
                            <td class="listado2"><?php echo $usua_nomb;?></td>
                        </tr>
			<tr>
			    <td class="titulos2" align="center">FECHA</td>
			    <td class="listado2"><?php echo "\t".$fecha;?></td>
			</tr>
			<tr>
			    <td class="titulos2" align="center">RADICADOS EXCLUIDOS</td>
			    <td class="listado2"><?php echo $pdata;?></td>
			</tr>
                    </table>
                    <input type="hidden" name="gruporad" value="<?php echo $gruporad;?>"><?php echo "\n".$passdata;?>
			<input type="hidden" name="submasdel" value="Algo">
                </form>    
                <table width="40%"><tr><td align="center"><input type="submit" name="subconfdel" form="frcfdel" value="Aceptar"></td>
                    <td align="center"><input type="submit" value="Regresar" form="regresar"></td></tr></table>
            </div>    
    <?php    
    }else{
        $masivas->setRadiGrupo($gruporad);
        $masivas->setRadiNume($listaGM);
        $cond=$masivas->estadoExcMasiva($gruporad,$listaGM);
        if($cond==TRUE){
            $numelem=count($listaGM);
            $masivas->setDepeRadi($_SESSION['dependencia']);
            $masivas->setUsuaCodi($codusuario);
            $masivas->setUsuaDoc($usua_doc);
            for($i=0; $i<$numelem;$i++){
                $radtmp=$listaGM[$i];
                $masivas->setRadiNume($radtmp);
                $masivas->retirarUsuaMasiva();
            }
            $status="ok Registro el retiro de usuarios y radicados de la masiva";
        }else{
            $status="error Este registro ya fue excluido de la masiva";
        }?>
        <table width="40%"  border="0"  cellpadding="0" cellspacing="1">
            <tr class="titulos4">
                <th>Estado de la operaci&oacute;n</th>
            </tr>
            <tr class="titulos2">
                <td><?php echo $status;?></td>
            </tr>
        </table>
       <p><pre><a href="listaMasiva.php?gruporad=<?php echo $gruporad?>&depsel=<?php echo $dependencia?>">Devolver al listado</a></pre></p>     
    <?php     }
    ?>
    </body>
</html>
