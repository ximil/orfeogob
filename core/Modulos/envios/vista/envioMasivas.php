<?php session_start(); date_default_timezone_set('America/Bogota'); date_default_timezone_set('America/Bogota');
//error_reporting(E_ALL);
//ini_set('display_errors','On');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//foreach ($_GET as $key => $valor)   $$key = $valor;
foreach ($_POST as $key => $valor)   $$key = $valor;
foreach ($_SESSION as $key => $valor)   $$key = $valor;
foreach ($_SERVER as $key => $valor)   $$key = $valor;
$ruta_raiz='../../../..';
//Cargando la clase de masivas
include_once "$ruta_raiz/core/Modulos/envios/clases/masivas.php";
$masivas= new masivas($ruta_raiz);
$masivas->setDepeRadi($depsel);
//Validando session
include_once "$ruta_raiz/core/vista/validadte.php";
//Cargando la clase envios
include_once "$ruta_raiz/core/Modulos/envios/clases/envios.php";
$envios= new envios($ruta_raiz);
include_once $ruta_raiz.'/core/config/config-inc.php';
//Cargando variables de log
if(isset($_SERVER['HTTP_X_FORWARD_FOR'])){
    $proxy=$_SERVER['HTTP_X_FORWARD_FOR'];
}else
    $proxy=$_SERVER['REMOTE_ADDR'];
include_once "$ruta_raiz/core/clases/log.php";
$log = new log($ruta_raiz);
$log->setAddrC($REMOTE_ADDR);
$log->setProxyAd($proxy);
//Cargando la clases planillas
include_once "$ruta_raiz/core/Modulos/envios/clases/planillas.php";
$planillas=new planillas($ruta_raiz);
$planillas->setDepeNume($dependencia);
$recordME=$envios->consultarMedioEnvio();
$nummden=count($recordME);
$optMDenvio="\n<option value='0'>--Seleccione Medio de Envio--</option>\n";
for ($i=0; $i<$nummden;$i++){
    $codenvio=$recordME[$i]['codenvio'];
    $medenvio=$recordME[$i]['medenvio'];
    $optMDenvio .="\t\t\t<option value='$codenvio'>$codenvio - $medenvio</option>\n";
}
if(isset($numplan)){
    $planillas->setNumPlanilla($numplan);
    $val=$planillas->valNumPlanilla();
    $sendM=1;
    if($val==1){
        unset($subenmas);
        $planerr="<font color='red'>N&uacute;mero de la planilla fue usado ya, fue generado otro c&oacute;digo para hacer el envio</font></b>";
    }
}
?>
<html>
    <head>
        <meta http-equiv="Cache-Control" content="cache">
        <meta http-equiv="Pragma" content="public">
        <link rel="stylesheet" href="<?php echo $ruta_raiz;?>/estilos/default/orfeo.css">
        <link rel="stylesheet" href="<?php echo $ruta_raiz; ?>/estilos/Style.css" type="text/css">
        <title>Envio de documentos masivas</title>
	<script>
	    function isNumberKey(evt)
            {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode >31 &&(charCode < 48 || charCode > 57))
                    return false;
                return true;
            }
            function validar(){
                var val=document.forms['formmas'].elements['idenvio'];
                if(document.forms['formmas'].elements['peso'].value == "" || document.forms['formmas'].elements['peso'].value == 0){
                    alert("El peso no puede ir vacio, ni puede ser 0");
                    document.getElementById("formmas").focus();
                    return false;
                }
                if(val.options[val.selectedIndex].value == 0){
                    alert("Seleccione un medio de envio");
                    document.getElementById("formmas").focus();
                    return false;
                }
                document.getElementById("formmas").submit();
            }
	</script>
    </head>
    <body>
<?php if(isset($sendM)){
    if(!isset($subenmas)){
    $masinfo=  preg_split("/,/", $infomasiva);
    $gruporad=$masinfo[0];
    $rad_ini=$masinfo[1];
    $rad_fin=$masinfo[2];
    $masivas->setRadiGrupo($gruporad);
        $recordGM=$masivas->listarGrupoMasivas();
        $listades=$masivas->dataMasEnviar();
        $noplanilla=$planillas->genNumPlanilla();
        if ($recordGM['error']!= ""){
                echo $recordGM['error'];
            }else{
                ?>
        <form id="formmas" action="envioMasivas.php" method="post" onsubmit="return(validar());">
            <input type="hidden" name="depsel" value="<?php echo $depsel?>">
          <center>
              <table width="40%" class="tabS" border="0" cellpadding="1" cellspacing="0">
                  <tr class="titulos4">
                      <th>Grupo</th>
                      <th>Intervalo de radicados</th>
                      <th>Empresa de envio</th>
                      <th>Peso</th>
                  </tr>
                  <tr class="titulos2">
                      <td><?php echo $gruporad;?><input type="hidden" name="grupoenv" value="<?php echo $numrad;?>"></td>
                      <td><?php echo "$rad_ini<br/>$rad_fin";?></td>
                      <td><select name="idenvio" id="idenvio" class="select"><?php echo $optMDenvio;?></select></td>
                      <td><input type="text" name="peso" id="peso" autocomplete="off" onkeypress="return isNumberKey(event)"></td>
                  </tr>
              </table>
              <br/><br/>
              <table width="25%" class="tabS" border="0" cellpadding="1" cellspacing="0">
                  <tr class="titulos4">
                      <th>Destino</th>
                      <th>N&uacute;mero de radicados</th>
                  </tr>
                  <tr class="titulos2">
                      <td>Local</td>
                      <td><?php echo $listades[0];?></td>
                  </tr>
                  <tr class="titulos2">
                      <td>Nacional</td>
                      <td><?php echo $listades[1];?></td>
                  </tr>
                  <tr class="titulos2">
                      <td>Internacional</td>
                      <td><?php echo $listades[2];?></td>
                  </tr>
                  <tr class="titulos2">
                      <td>Intercontinental</td>
                      <td><?php echo $listades[3];?></td>
                  </tr>
              </table>
              <br/><br/>
              <table width="33%" class="tabS" border="0" cellpadding="1" cellspacing="0">
                  <tr class="listado2">
                      <td width="33%">Observaciones o <br>Desc. Anexos</td>
                      <td><input type="text" name="observaenv" size="50"></td>
                  </tr>
                  <tr class="listado2">
                      <td>No. de Planilla</td>
                      <td><input type="text" name="numplan" value="<?php echo $noplanilla;?>"></td>
                  </tr>
              </table>
          </center>
            <?php             if(isset($planerr))
                echo $planerr;
            ?>
            <input type="hidden" name="gruporad" value="<?php echo $gruporad;?>">
            <center><input type="submit" name="subenmas" value="Generar Registro de Envio"></center>
        </form>
            <?php             }
    }
    else{
        $masivas->setMedEnvio($idenvio);
        $masivas->setRadiGrupo($gruporad);
        $listades=$masivas->dataMasEnviar();
        //Cambio para que tome la dependencia del la persona que hace el envio
        $masivas->setDepeRadi($dependencia);
        $masivas->setDestinosRad($listades);
        $masivas->setNumPlanilla($numplan);
        $masivas->setPeso($peso);
        $masivas->marcarEnvioMasiva();
        $masivas->setKrd($krd);
        $masivas->accionExcluidos();
        $masivas->setObservaEnv($observaenv);
        $masivas->setPeso($peso);
        $masivas->setUsuaDoc($usua_doc);
        $log->setUsuaCodi($codusuario);
        $log->setDepeCodi($dependencia);
        $log->setRolId($id_rol);
        $log->setDenomDoc($doc='Radicado');
        $log->setAction('document_deliver_batch');
        $log->setOpera("Envio de masiva grupo $gruporad");
        $log->setNumDocu($gruporad);
        $log->registroEvento();
        $masivas->enviarMasiva();
        $planillas->planillaVerificada();
        ?>
        <center>
            <table width="25%" class="tabS" border="0" cellpadding="1" cellspacing="0">
                  <tr class="titulos4">
                      <th>Destino</th>
                      <th>N&uacute;mero de radicados</th>
                  </tr>
                  <tr class="titulos2">
                      <td>Local</td>
                      <td><?php echo $listades[0];?></td>
                  </tr>
                  <tr class="titulos2">
                      <td>Nacional</td>
                      <td><?php echo $listades[1];?></td>
                  </tr>
                  <tr class="titulos2">
                      <td>Internacional</td>
                      <td><?php echo $listades[2];?></td>
                  </tr>
                  <tr class="titulos2">
                      <td>Intercontinental</td>
                      <td><?php echo $listades[3];?></td>
                  </tr>
                  <tr class="titulos2">
                      <td>Planilla de Env&iacute;o No.</td>
                      <td><?php echo $numplan?></td>
                  </tr>
                  <tr>
                      <td colspan="2" class="titulos4">Envio de radicados hecho con exito</td>
                  </tr>
              </table>
        </center>
        <?php     }
    echo "\t\t<center>\n";
    echo "\t\t<a href='listaMasiva.php'><pre>Regresar al listado de masivas por enviar</pre></a>\n";
    echo "\t\t</center>\n";
}
else{
    echo "<b><font color='red'>Fallo</font></b>";
    header('Location:listaMasiva.php');
}

?>
    </body>
</html>
