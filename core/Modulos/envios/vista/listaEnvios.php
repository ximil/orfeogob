<?php session_start();
date_default_timezone_set('America/Bogota');
date_default_timezone_set('America/Bogota');
//error_reporting(E_ALL);
//ini_set('display_errors', 'On');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
foreach ($_GET as $key => $valor)
    ${$key} = $valor;
foreach ($_POST as $key => $valor)
    ${$key} = $valor;
foreach ($_SESSION as $key => $valor)
    ${$key} = $valor;
foreach ($_SERVER as $key => $valor)
    ${$key} = $valor;
$ruta_raiz = '../../../..';
include_once "$ruta_raiz/core/Modulos/envios/clases/envios.php";
$envios = new envios($ruta_raiz);
//Validacion de session
include_once "$ruta_raiz/core/vista/validadte.php";
//Cargando las dependencias de la entidad
include_once $ruta_raiz . '/core/clases/dependencia.php';
if (isset($depsel)) {
    $dependencia = $depsel;
}
//Inicializando las dependencias
$depe = new dependencia($ruta_raiz);
$depe->setDepe_codi($dependencia);
$dependenciasX = $depe->consultarTodo();
$numdep = count($dependenciasX);
$optionDepe = "";
//Carga en una variable para imprimir las dependencias
for ($i = 0; $i < $numdep; $i++) {
    $nomDepe = $dependenciasX [$i] ['depe_nomb'];
    $codDepe = $dependenciasX [$i] ['depe_codi'];
    $optionDepe .="\t \t \t<option ";
    if ($codDepe == $dependencia) {
        $optionDepe .="selected='selected'";
    }
    $optionDepe .=" value='$codDepe'>$codDepe - $nomDepe</option>\n";
}
if (!isset($nomcarpeta)) {
    $nomcarpeta = "Envios Normales";
    $estarad = 3;
} else {
    $nomcarpeta = "Envios Normales";
    $estarad = 3;
}
//$encabezado=session_name() . "=" . session_id() . "&krd=$krd";
?>
<html>
    <head>
        <meta http-equiv="Cache-Control" content="cache">
        <meta http-equiv="Pragma" content="public">
        <!--link rel="stylesheet" href="<?php echo $ruta_raiz; ?>/old/estilos/table.css" type="text/css"-->
        <link rel="stylesheet" href="<?php echo $ruta_raiz; ?>/estilos/default/orfeo.css">
        <link rel="stylesheet" href="<?php echo $ruta_raiz; ?>/estilos/Style.css" type="text/css">
        <title>Prueba de Envios</title>
        <script type="text/javascript">
           /*function isNumberKey(evt)
            {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode > 44 && (charCode < 48 || charCode > 57))
                    return false;
                return true;
            }*/
            function valCB() {
                var chk = document.forms["marcarEnv"].elements["listaMarca[]"];
                var marcado = false;
                var cant = chk.length;
                for (var i = 0; i < chk.length; i++) {
                    if (chk[i].checked) {
                        marcado = true;
                        break;
                    }
                }
                if (marcado == false) {
                    alert("Marque documento(s) a enviar " + cant);
                    return false;
                }
                return true;
            }
        </script>
    </head>
    <body bgcolor="#FFFFFF">
        <header id="page_header" style="width: 98%">
            <?php include $ruta_raiz . '/core/Modulos/envios/vista/miniMenu.php' ?>
            <h1>Envios normales</h1>  
            <form style="margin:0" action="listaEnvios.php" method="post">
                <table style="width:100% ; border-spacing: 1 " class="tabS">
                    <tr>
                        <td class="titulo1" style="width:33% ">CARPETA DE <?php echo strtoupper($nomcarpeta); ?></td>
                        <td class="titulo1" style="width:33%"><center><?php echo $usua_nomb; ?></center></td>
                    <td class="titulo1" style="width:33%">Dependencia del sistema
                        <select class="select" name="depsel" onchange='this.form.submit()'><?php echo $optionDepe; ?></select></td>
                    </tr>
                </table>
                <table style="width:100% ; border-collapse: collapse "  class="tabS">
                    <tr class="tablas">
                        <td  style="width:25%">N&uacute;mero de radicado</td>
                        <td colspan="2"><center><input type="text" name="numrad_sol" size="100" id="txtCra" autocomplete="off" onkeypress="return isNumberKey(event)"/></center></td>
                    <td style="width: 10%" ><input type="hidden" name="nomcarpeta" value="<?php echo $nomcarpeta; ?>"><input type="submit" name="submitrad" value="Buscar" class="botones"></td>
                    </tr>
                </table>
            </form>
            <table style='width:100%'><tr class="titulos2"><td align="center"><input type="submit" name="marcaenv" class="botones_largo" form="marcarEnv" value="Envio de documentos"></td></tr></table>
        </header>
        <?php         $correrad=0;
        if(isset($numrad_sol)){
            $trimnumrad=preg_replace('/[\s]+/', '', $numrad_sol );
            if(is_numeric(preg_replace('/,/','', $trimnumrad))){
                    $multirad=preg_split("/,/",$trimnumrad);
                    $j=0;
                    for($h=0;$h<count($multirad);$h++){
                        $lenum=strlen($multirad[$h]);
                        if($lenum>=6 && $lenum<=14){
                            $radxeva[$j] = $multirad[$h];
                            $j++;
                        }
                    }
                    if($j>0){
                        $correrad=2;
                    }
                
                   
            } if(is_numeric($trimnumrad) && strlen($trimnumrad)<=14 && strlen($trimnumrad)>=6){
                        $correrad=1;
                    
                }
            }
        
        $envios->setRadiDepeRadi($dependencia);
        $envios->setEstaCodi($estarad);
            //
            if(isset($submitrad) && $correrad!=0){
                if($correrad==1){
                    $envios->setRadiNumeRadi($numrad_sol);
                    $recordEnv=$envios->consultarxRadicado($numrad_sol,$estarad);
                }else{
                        $envios->setRadiNumeRadi($radxeva);
                        $recordEnv=$envios->consultarxRadicado($radxeva,$estarad);
                }
                if($recordEnv['error']!=''){
                    echo $recordEnv['error'];
                    unset($recordEnv);
                    $recordEnv=$envios->consultarxEstado();
                }
            }
         else{
            $recordEnv = $envios->consultarxEstado($dependencia, $estarad);
        }
        if ($recordEnv['error'] != "") {
            echo $recordEnv['error'];
        } else {
            ?>
        <form action="formularioEnvios.php" id="marcarEnv" method="post" onsubmit="return valCB();">
            <div style=" padding : 4px; width :95%; height : 400px; overflow : auto;">
                
                    <input type="checkbox" name="listaMarca[]" value="" id="chk000" style="display:none">
                    <table style="width:100% ; border-spacing: 1 " class="tabS">
                        <tr class="titulos4">
                            <td width="10%"><img src='<?php echo $ruta_raiz; ?>/old/imagenes/estadoDoc.gif' width="130px"	border=0></img></td>
                            <th width="10%">Radicado</th>
                            <th width="2%">cp</th>
                            <th width="10%">Radicado Padre</th>
                            <th >Descripci&oacute;n</th>
                            <th width="15%">Fecha de<br/>radicaci&oacute;n</th>
                            <th width="15%">Generado por</th>
                            <th width="5%"></th>
                        </tr>
                        <?php                         for ($i = 0; $i < (count($recordEnv) - 1); $i++) {
                            $numrad = $recordEnv[$i]['numrad'];
                            $radicado_padre = $recordEnv[$i]['radicado_padre'];
                            $descri_anex = $recordEnv[$i]['descri_anex'];
                            $fecharadi = $recordEnv[$i]['fecharadi'];
                            $usua_login = $recordEnv[$i]['usua_login'];
                            $copia = $recordEnv[$i]['copia'];
                            $valcop = $recordEnv[$i]['valcop'];
                            if ($i % 2 == 0)
                                $atr = "listado1";//"listado2_center";
                            else {
                                $atr = "listado2";
                            }
                            ?>
                            <tr>
                                <td align="center">
        <?php echo "<img src=$ruta_raiz/old/imagenes/docImpreso.gif  border=0>"; ?></td>
                                <td class="<?php echo $atr; ?>" ><span class='leidos'><?php echo $numrad;  ?></span></td>
                                 <td class="<?php echo $atr; ?>" ><span class='leidos'><?php  if ($valcop != '') echo "cp. $valcop"; else echo "-"; ?></span></td>
                                <td class="<?php echo $atr; ?>" ><span class='leidos'><?php echo $radicado_padre; ?></span></td>
                                <td class="<?php echo $atr; ?>" style="white-space: pre-line"><span class='leidos'><?php echo $descri_anex; ?></span></td>
                                <td class="<?php echo $atr; ?>" ><span class='leidos'><?php echo $fecharadi; ?></span></td>
                                <td class="<?php echo $atr; ?>" ><span class='leidos'><?php echo $usua_login; ?></span></td>
                                <td class="<?php echo $atr; ?>" ><input type="checkbox" name="listaMarca[]" value="<?php echo $numrad . ',' . $copia; ?>" id="<?php echo "chk$i"; ?>"></td>
                            </tr>
        <?php }
    ?>
                    </table>
                
            </div>
        </form>
<?php }
?>
    </body>
</html>
