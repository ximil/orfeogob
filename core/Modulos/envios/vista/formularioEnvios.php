<?php session_start(); date_default_timezone_set('America/Bogota'); date_default_timezone_set('America/Bogota');
//error_reporting(E_ALL);
//ini_set('display_errors','On');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//foreach ($_GET as $key => $valor)   $$key = $valor;
foreach ($_POST as $key => $valor)   $$key = $valor;
foreach ($_SESSION as $key => $valor)   $$key = $valor;
foreach ($_SERVER as $key => $valor)  $$key = $valor;
$ruta_raiz='../../../..';
include_once "$ruta_raiz/core/Modulos/envios/clases/envios.php";
$envios= new envios($ruta_raiz);
//Validando session
include_once "$ruta_raiz/core/vista/validadte.php";
//Cargando el Log
if(isset($_SERVER['HTTP_X_FORWARD_FOR'])){
    $proxy=$_SERVER['HTTP_X_FORWARD_FOR'];
}else
    $proxy=$_SERVER['REMOTE_ADDR'];
include_once "$ruta_raiz/core/clases/log.php";
$log = new log($ruta_raiz);
$log->setAddrC($REMOTE_ADDR);
$log->setProxyAd($proxy);
$con=0;
?>
<html>
    <head>
        <link rel="stylesheet" href="<?php echo $ruta_raiz; ?>/estilos/default/orfeo.css">
        <link rel="stylesheet" href="<?php echo $ruta_raiz; ?>/estilos/Style.css" type="text/css">
        <title>Test Formulario para envios</title>
        <script language="javascript">
            function isNumberKey(evt)
            {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode >31 &&(charCode < 48 || charCode > 57))
                    return false;
                return true;
            }
            function validar(){
                var val=document.forms['formenv2'].elements['codmedenvio'];
                if(document.forms['formenv2'].elements['peso'].value == "" || document.forms['formenv2'].elements['peso'].value == 0){
                    alert("El peso no puede ir vacio, ni puede ser 0");
                    document.getElementById("formenv2").focus();
                    return false;
                }
                if(val.options[val.selectedIndex].value == 0){
                    alert("Seleccione un medio de envio");
                    document.getElementById("formenv2").focus();
                    return false;
                }
                document.getElementById("formenv2").submit();
            }
        </script>
    </head>
    <body>
        <header id="page_header" style="width: 99%">

                <?php include $ruta_raiz . '/core/Modulos/envios/vista/miniMenu.php' ?>
                    <h1>Envio de documentos</h1>
        </header>
        <!--<center><pre><a href="listaEnvios.php">Devolver al listado</a></pre></center>-->
        <?php         if(isset($codmedenvio)){
           if($codmedenvio!=0){
               $con=1;
           }
        }
        $recordME=$envios->consultarMedioEnvio();
           // if($recordME['error']!="")
             //   print_r($recordME);
        //print_r($listaMarca);
        if(!isset($submitbutton)){
        //if (!isset($numrad)|| $con==0){
            //echo "<b>$con</b>";

          ?>
        <form id="formenv2" action="formularioEnvios.php" method="post" onsubmit="return(validar());">
            <table style='width: 99% ; border-spacing: 1' class="borde_tab">
                <tr class="titulos31">
                    <th>Seleccione Medio de Envio</th>
                      <td>
                        <select  class="select" name="codmedenvio" id="codmedenvio">
                            <option value="0">&lt;&lt; Seleccione  &gt;&gt;</option>
                            <?php                             for($i=0; $i < count($recordME); $i++ ){
                            ?>
                            <option value="<?php echo $recordME[$i]['codenvio']; ?>"><?php echo $recordME[$i]['codenvio']."-".$recordME[$i]['medenvio'];?></option>
                                <?php                                 }
                                ?>
                        </select>
                    </td>
                    <th>Peso(gramos)</th>
                    <td><input type="text" name="peso" id="peso" autocomplete="off" onkeypress="return isNumberKey(event)"> </td>
                    <td style="width: 50%"></td>
                </tr>
            </table>
            <br>
            <table style='width: 99% ; border-spacing: 1' class="borde_tab" >
                <tr class="titulo1">
                    <td>N&uacute;mero de radicado</td>
                    <td>Radicado padre</td>
                    <td>Destinatario</td>
                    <td>Direcci&oacute;n</td>
                    <td>Municipio</td>
                    <td>Depto.</td>
                    <td>Pa&iacute;s</td>
                </tr>
                <?php                 $numelem=count($listaMarca);
		//print_r($listaMarca);
                for($i=0; $i<$numelem;$i++){
                    list($radtmp,$copia)=explode(",",$listaMarca[$i]);
                    $envios->setRadiNumeRadi($radtmp);
                    $envios->setCopia($copia);
                    $recordIA=$envios->infoxAccion();

                ?>
                <tr class="listado2">
                    <td><span class='leidos'><?php echo $recordIA['numrad'];?></span>
                            <input type="hidden" name="listaEnv[]" value="<?php echo $listaMarca[$i];?>"></td>
                    <td><span class='leidos'><?php echo $recordIA['radi_padre'];?></span></td>
                    <td><textarea cols="30" rows="2" ><?php echo $recordIA['destinatario'];?></textarea></td>
                    <td><textarea cols="20" rows="2"><?php echo $recordIA['direccion'];?></textarea></td>
                    <td><input type="text" readonly="true" value="<?php echo $recordIA['municipio'];?>"/></td>
                    <td><input type="text" readonly="true" value="<?php echo $recordIA['depto'];?>"></td>
                    <td><input type="text" readonly="true" value="<?php echo $recordIA['pais'];?>"></td>
                </tr>
                <tr class="titulos2">
                    <td colspan="2">Asunto </td><td colspan="5"> <input type="text" readonly="true" size="40" value="<?php echo $recordIA['asunto'];?>"></td>
                </tr>
                <tr class="titulos2">
                    <td colspan="2">Observaciones o Desc. de Anexos </td><td colspan="5"><input type="text" name="comen[]" size="30"></td>
                </tr>
                <?php                 }
                ?>
            </table>
	    <center><input type="submit" class="botones_largo"  value="Enviar documentos" name="submitbutton"></center>
        </form>
        <?php

        }
        else{?>
            <table style='width: 99% ; border-spacing: 1' >
                <tr class="titulos31">
                    <th>Medio de Envio</th>
                    <td>
                        <select disabled>
                            <?php                             for($i=0; $i < count($recordME); $i++ ){
                            ?>
                            <option value="<?php echo $recordME[$i]['codenvio']; ?>"<?php if($recordME[$i]['codenvio']==$codmedenvio){echo "selected='selected'";} ?>>
                                <?php echo $recordME[$i]['codenvio']."-".$recordME[$i]['medenvio'];?></option>
                                <?php                                 }
                                ?>
                        </select>
                    </td>
                    <th>Peso</th>
                    <td><input type="text" readonly="true" value="<?php echo $peso;?>"></td>
                    <td style="width: 50%"></td>
                </tr>
            </table>
        <table style='width: 99% ; border-spacing: 1' class="borde_tab">
                <tr class="titulo1">
                    <td>N&uacute;mero de radicado</td>
                    <td>Radicado padre</td>
                    <td>Destinatario</td>
                    <td>Direcci&oacute;n</td>
                    <td>Municipio</td>
                    <td>Depto.</td>
                    <td>Pa&iacute;s</td>
                </tr>
                <?php                 $numelem=count($listaEnv);
                //print_r($listaEnv);
                $envios->setRadiDepeRadi($dependencia);
                $envios->setMedioEnvio($codmedenvio);
                $envios->setUsuaCodi($usua_doc);
                $envios->setRolUsua($id_rol);
                $envios->setUsuaCodi($codusuario);
                $log->setUsuaCodi($codusuario);
                $log->setDepeCodi($dependencia);
                $log->setRolId($id_rol);
                $log->setDenomDoc($doc='Radicado');
                $log->setAction('document_deliver');
                $log->setOpera('Envio de documento');
                $envios->setEstaCodi(4);
                $envios->setPesoEnvio($peso);
                for($i=0; $i<$numelem;$i++){
                    list($radtmp,$copia)=explode(",",$listaEnv[$i]);
                    $tmpcmt=$comen[$i];
                    $envios->setDescripcionEnvio($tmpcmt);
                    $envios->setRadiNumeRadi($radtmp);
                    $envios->setCopia($copia);
                    $log->setNumDocu($radtmp);
                    $recordIA=$envios->infoxAccion();
                    $status=$envios->accionxEstado();
                    if($status=='ok'){
                        $log->registroEvento();
                        $envios->generarEnvio();


                ?>
                <tr class="listado2">
                    <td><span class='leidos'><?php echo $recordIA['numrad'];?></span></td>
                    <td><span class='leidos'><?php echo $recordIA['radi_padre'];?></span></td>
                    <td><textarea cols="30" rows="2" ><?php echo $recordIA['destinatario'];?></textarea></td>
                    <td><textarea cols="20" rows="2"><?php echo $recordIA['direccion'];?></textarea></td>
                    <td><input type="text" readonly="true" value="<?php echo $recordIA['municipio'];?>"/></td>
                    <td><input type="text" readonly="true" value="<?php echo $recordIA['depto'];?>"></td>
                    <td><input type="text" readonly="true" value="<?php echo $recordIA['pais'];?>"></td>
                </tr>
                <tr class="titulos2">
                    <td colspan="7">Asunto  <input type="text" readonly="true" size="40" value="<?php echo $recordIA['asunto'];?>"></td>
                </tr>
                <tr class="titulos2">
                    <td colspan="7">Observaciones o Desc. de Anexos <input type="text" readonly="true" size="30" value="<?php echo $tmpcmt?>"></td>
                </tr>
                <?php                     }else{
                        echo "<b>Error con el envio del radicado n&uacute;mero $radtmp</b>";
                    }
                }
                ?>
            </table>
        <?php         }
        ?>
        <!--<center><pre><a href="listaEnvios.php">Devolver al listado</a></pre></center>-->
    </body>
</html>
