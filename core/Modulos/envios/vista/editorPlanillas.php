<?php session_start(); date_default_timezone_set('America/Bogota'); date_default_timezone_set('America/Bogota');
error_reporting(E_ALL);
ini_set('display_errors','On');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
foreach ($_GET as $key => $valor)  $$key = $valor;
foreach ($_POST as $key => $valor)  $$key = $valor;
foreach ($_SESSION as $key => $valor)  $$key = $valor;
foreach ($_SERVER as $key => $valor)  $$key = $valor;
$ruta_raiz='../../../..';
include_once "$ruta_raiz/core/Modulos/envios/clases/plantillas.php";
$plantillas= new plantillas($ruta_raiz);
//Cargando la clase envios
include_once "$ruta_raiz/core/Modulos/envios/clases/envios.php";
$envios= new envios($ruta_raiz);
//Generando la lista del medio de envio
$recordME=$envios->consultarMedioEnvio();
$nummden=count($recordME)-1;
if(!isset($idmedenvio))
    $idmedenvio=0;
    $optMDenvio="\n<option value='0'>--Seleccione Medio de Envio--</option>\n";
    for ($i=0; $i<$nummden;$i++){
        $codenvio=$recordME[$i]['codenvio'];
        $medenvio=$recordME[$i]['medenvio'];
        $optMDenvio .="\t\t\t<option value='$codenvio'";
        if($codenvio==$idmedenvio)
            $optMDenvio .=" selected";
        $optMDenvio .=">$codenvio - $medenvio</option>\n";
    }

if(!isset($nomcarpeta)){
    $nomcarpeta="planillas";
}else{
    $nomcarpeta="planillas";
}
if(isset($addfield)){
    $plantillas->setMedEnvio($idmedenvio);
    $plantillas->setMetodo($cmetodo);
    $plantillas->setAlias($alias);
    $plantillas->agregarCampo($idmedenvio,$cmetodo,$alias);
}
if(isset($subedit)){
    print_r($_POST);
            $plantillas->setAlias($alias);
            $plantillas->setEvento($idfmenv);
            $plantillas->editarCampo();
}
if(isset($subdesac)){
    $plantillas->setEvento($midfmenv);
    $plantillas->desactCampo();
}
if(isset($subacti)){
    $plantillas->setEvento($midfmenv);
    $plantillas->activarCampo();
}

//$encabezado=session_name() . "=" . session_id() . "&krd=$krd";
?>
<html>
    <head>
        <meta http-equiv="Cache-Control" content="cache">
        <meta http-equiv="Pragma" content="public">
        <link rel="stylesheet" href="<?php echo $ruta_raiz;?>/estilos/default/orfeo.css">
        <link rel="stylesheet" href="<?php echo $ruta_raiz; ?>/estilos/Style.css" type="text/css">
        <title>.::Editor de planillas::.</title>
    </head>
    <body bgcolor="#FFFFFF">
        <header id="page_header" style="width: 100%">
            <h1>Editor de Planillas</h1>
        </header>
        <br>
        <center>
            <form action="<?php echo $PHP_SELF;?>" method="post">
                <table class="tabS" width="40%" border="0" cellpadding="0" cellspacing="0">
                    <tr class="titulos4">
                        <th>Consultar Plantillas existentes</th>
                    </tr>
                    <tr class="titulos2">
                        <td><select name="idmedenvio" onchange="this.form.submit()"><?php echo $optMDenvio;?></select></td>
                    </tr>
                </table>
            </form>
        </center>
    <?php     if($idmedenvio!=0){
        $plantillas->setMedEnvio($idmedenvio);
        $record=$plantillas->cargarCPlantilla($idmedenvio);
        if($record['error']==""){
        ?>
        <center>
        <table width="40%" border="0" cellpadding="0" cellspacing="0" class="tabS">
            <tr class="titulos4">
                <th>Campo a mostrar</th>
                <th>Operaciones</th>
            </tr>
            <?php             for($i=0;$i<count($record)-1;$i++){
                $alias=$record[$i]['alias'];
                $idfmenv=$record[$i]['sgd_fmenv_codigo'];
                $estado=$record[$i]['estado'];
                if($estado==0){
                    $tagest="<input type='submit' name='subdesac' value='Desactivar'>";
                }else{
                    $tagest="<input type='submit' name='subacti' value='Activar'>";
                }
            ?>
            <tr class="titulos2">
            <form action="<?php echo $PHP_SELF;?>" method="post">
                <td><input type="hidden" name="midfmenv" value="<?php echo $idfmenv;?>"><input type="hidden" name="idmedenvio" value="<?php echo $idmedenvio;?>"><input type="hidden" name="malias" value="<?php echo $alias;?>"><?php echo $alias;?></td>
                <td align="center"><input type="submit" value="Editar" name="editfield"><?php echo $tagest;?></td>
            </form>
            </tr>
            <?php             }
            ?>
            
        </table>
        </center>
        <?php             
        }else{
            echo $record['error'];
        }
        if(isset($subadd)){
            $recordC=$plantillas->agregarCPlantilla($idmedenvio);
            $optC="<option value='0'>Opciones a precargadas</option>\n";
            if($recordC['error']==""){
                for ($i=0;$i< count($recordC)-1;$i++){
                    $codmetodo=$recordC[$i]['codmetodo'];
                    $tipo_metodo=$recordC[$i]['tipo_metodo'];
                    $optC .="\t\t\t<option value='$codmetodo'>$tipo_metodo</option>\n";
                }
            }else{
                $optC=$recordC['error'];
            }
            ?>
    <form action="<?php echo $PHP_SELF;?>" method="post">
            <center>
            <table>
                <tr>
                    <td>Alias del Nuevo campo</td>
                    <td>Campos precargados</td>
                </tr>
                <tr>
                    <td><input type="text" name="alias"><input type="hidden" name="idmedenvio" value="<?php echo $idmedenvio;?>"></td>
                    <td><select name="cmetodo" class="select">
                        <?php echo $optC;?>                     </select>
                    </td>
                </tr>
                <tr colspan="2">
                    <td>
                        <input type="submit" name="addfield" value="Agregar">
                    </td>
                </tr>
            </table>
            </center>
    </form>
        <?php         }
        if(isset($addfield)){
            echo "<font color='red'>Campo agregado con exito</font>";
        }
        if(isset($editfield)){
            ?>
            <form method="post">
                <input type="hidden" name="idmedenvio" value="<?php echo $idmedenvio;?>">
                <center><table>
                    <tr><td>Nombre del Alias</td></tr>
                    <tr><td><input type="text" name="alias" value="<?php echo $malias;?>"></td></tr>
                </table></center>
                <input type="hidden" name="idfmenv" value="<?php echo $midfmenv;?>">
                <center><input type="submit" name="subedit" value="Modificar"></center>
            </form>
            <?php         }
        if(isset($subedit)){
            echo "<p align='center'>Campo modificado con exito</p>";
        }
            ?>
        <form method="post">
            <input type="hidden" name="idmedenvio" value="<?php echo $idmedenvio;?>">
            <center><input type="submit" name="subadd" value="Agregar Campo"></center>
        </form>
    <?php     }
    ?>
    </body>
</html>