<?php session_start();
date_default_timezone_set('America/Bogota');
/**
 * Modificacion Variables Globales Infometika 2009
 */
$krd = $_SESSION["krd"];
$dependencia = $_SESSION["dependencia"];
$usua_doc = $_SESSION["usua_doc"];
$codusuario = $_SESSION["codusuario"];
$ruta_raiz = "../../../..";
include_once $ruta_raiz . '/validadte.php';
include $ruta_raiz.'/old/config.php';

?>
<html>
<head>
    <link href="<?php echo $ruta_raiz ?>/estilos/default/orfeo.css" rel="stylesheet" type="text/css">
</head>
<body>
<div style='text-align: center ;width: 100%;margin: 0 auto;'>
    <table class="borde_tab" width="100%" cellspacing="2">
        <tbody>
        <tr>
            <td align="center" class="titulos4">
                Modulo de envios
            </td>
        </tr>
        </tbody>
    </table>
    <br>
    <center>
        <table style='width: 90%' class="borde_tab">
            <tr class='titulo1'>
                <td colspan="5">
                    <img src='<?php echo $ruta_raiz ?>/imagenes/correo.gif'> Env&iacute;o de Correspondencia &nbsp;
                </td>
            </tr>
            <tr align="center" class='listado2'>
                <td class='listado2'>
                    <a href='listaEnvios.php' class='vinculos'>Envio Normal
                    </a>
                </td>
                <td class='listado2'><a href='modifEnvio.php' class='vinculos'>Modificaci&oacute;n Registro de Env&iacute;o
                    </a></td>
                <td class='listado2'><a href='listaMasiva.php' class='vinculos'>Masiva
                    </a></td>
                <td class='listado2'><b><a href='generarEnvios.php' class='vinculos'>Generaci&oacute;n de Planillas y
                            Guias</a><br>
                        <a href='<?php echo $url_orfeo_power_tools."/document/batch_sent" ?>' class='vinculos'>Consultar planillas existentes</a>
                </td>
                <td class='listado2'><b><a href='cargarPlansipos.php' class='vinculos'>Cargar Planilla SIPOST
                        </a></td>
            </tr>
        </table>
        <table>
            <tr>
                <td><p></p></td>
            </tr>
        </table>
        <table style='width: 90%' border="0" class="borde_tab">
            <tr class='titulo1'>
                <td colspan="4">
                    <img src='<?php echo $ruta_raiz ?>/imagenes/devoluciones.gif'> Devoluciones
                </td>
            </tr>
        </table>
        <table style='width: 90%' class="borde_tab">
            <tr style="background-color:#D7D7F3">
                <td class='listado2' height="25">
                    <a href='devolucionEspera.php' class='vinculos'>
                        Por exceder tiempo de espera
                    </a>
                </td>
                <td class='listado2' height="25">
                    <a href='otrasDevoluciones.php' class='vinculos'>
                        Otras Devoluciones
                    </a>
                </td>
                <td class='listado2' height="25"><a href=''>
                    </a>
                </td>
            </tr>
        </table>
        <table style='width: 90%' class="borde_tab">
            <tr class='titulo1'>
                <td colspan="4">
                    <img src='<?php echo $ruta_raiz ?>/imagenes/estadisticas_icono.gif'> Reportes
                </td>
            </tr>
            <tr style="background-color:#D7D7F3">
                <td class='listado2' height="25"><a href='estadisticaEnvios.php' class='vinculos'>
                        Envio de Correo
                    </a>
                </td>
                <td class='listado2' height="25">
                    <a href='estadisticaDevoluciones.php' class='vinculos'>
                        Devoluciones
                    </a></td>
            </tr>
        </table>
</div>
</body>
</html>