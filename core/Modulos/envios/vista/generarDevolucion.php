<?php session_start(); date_default_timezone_set('America/Bogota');
//error_reporting(E_ALL);
//ini_set('display_errors','On');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//foreach ($_GET as $key => $valor)   $$key = $valor;
foreach ($_POST as $key => $valor)   $$key = $valor;
foreach ($_SESSION as $key => $valor)   $$key = $valor;
foreach ($_SERVER as $key => $valor)   $$key = $valor;
$ruta_raiz='../../../..';
include_once "$ruta_raiz/core/Modulos/envios/clases/devoluciones.php";
$devoluciones= new devoluciones($ruta_raiz);
//Validando session
include_once "$ruta_raiz/core/vista/validadte.php";
//Cargando variables de log
if(isset($_SERVER['HTTP_X_FORWARD_FOR'])){
    $proxy=$_SERVER['HTTP_X_FORWARD_FOR'];
}else
    $proxy=$_SERVER['REMOTE_ADDR'];
include_once "$ruta_raiz/core/clases/log.php";
$log = new log($ruta_raiz);
$log->setAddrC($REMOTE_ADDR);
$log->setProxyAd($proxy);
//Cargando clase transaccion para desarchivar radicado en caso de devolución
include_once $ruta_raiz . '/core/Modulos/radicacion/clases/tx.php';
$tx = new tx ( $ruta_raiz );
//Cargando Nombre de carpeta
$motivoDev=$devoluciones->generarMotivoDev();
$nummot=  count($motivoDev);
$optionDevo="<option value='0'>--- Escoja un motivo  ---</option>";
for($i=0; $i<$nummot;$i++){
    $deve_codigo=$motivoDev[$i]['deve_codigo'];
    $deve_motivo=$motivoDev[$i]['deve_motivo'];
    $optionDevo .="\t\t\t<option value='$deve_codigo'>$deve_codigo - $deve_motivo</option>\n";
}

if(!isset($nomcarpeta)){
    $nomcarpeta="generar devolucion";
}else{
    $nomcarpeta="generar devolucion";
}

?>
<html>
    <head>
        <meta http-equiv="Cache-Control" content="cache">
        <meta http-equiv="Pragma" content="public">
        <link rel="stylesheet" href="<?php echo $ruta_raiz;?>/estilos/default/orfeo.css">
        <link rel="stylesheet" href="<?php echo $ruta_raiz;?>/estilos/Style.css" type="text/css">
        <title>.:Fase de construcci&oacute;n de generaci&oacute;n de Envio:.</title>
    </head>
    <body bgcolor="#FFFFFF">
        <header id="page_header" style="width: 95%">
            <h1>Documento Por Generar Devoluci&oacute;n</h1>
        </header>
        <?php         if(!isset($confdev) && !isset($commentdev) && !isset($motdev)){
        ?>
        <table><tr><td></td</tr></table>
        <div id="contenido">
            <form style="margin:0" action="generarDevolucion.php" method="post">
                <center><table style="width:60% ; border-spacing: 1 " class='borde_tab'>
                    <tr>
                        <td class="titulos2" width="25%">Tipo de devoluci&oacute;n</td>
                        <td class="listado2" width="75%"><select class="select" name="motdev"><?php echo $optionDevo;?></select></td>
                    </tr>
                    <tr>
                        <td class="titulos2" width="25%">Comentario</td>
                        <td class="listado2" width="75%"><input type="text" name="commentdev" size="60"></td>
                    </tr>
                    <tr>
                        <td class="titulos2" valign="top" colspan="2"><center><input type="submit" value="Confirmar devoluci&oacute;n" name="confdev" class="botones_largo"></center></td>
                    </tr>
                </table></center><br>
                <table width="95%" border="0" cellpadding="0" cellspacing="1" class="tabS">
                    <tr class="titulos4">
                        <th>N&uacute;mero del radicado</th>
                        <th>Planilla No.</th>
                        <th>Fecha Envio</th>
                        <th>Destinatario</th>
                        <th>Direcci&oacute;n</th>
                        <th>Departamento</th>
                        <th>Municipio</th>
                        <th>Documentos</th>
                        <th></th>
                    </tr>
                    <?php                     $numelem=  count($listaDev);
                    for($i=0; $i<$numelem;$i++){
                        list($radtmp,$cptmp)=explode(",",$listaDev[$i]);
                        $devoluciones->setCopia($cptmp);
                        $devoluciones->setNumeRadi($radtmp);
                        $recordDev=$devoluciones->consulxDevolver();
                        $listadoar=$tx->consultar($radtmp, $dependencia);
                        /*if(count($listadoar)!=0){
                            echo "<font color='red'>Dentro los documentos devueltos hay uno o m&aacute;s documentos por devolver</font>";
                        }*/
                        ?>
                        <tr class="listado2">
                            <td><?php echo $recordDev[0]['numradi'];?></td>
                            <td><?php echo $recordDev[0]['planilla'];?></td>
                            <td><?php echo $recordDev[0]['fechenvio'];?></td>
                            <td><?php echo $recordDev[0]['destinatario'];?></td>
                            <td><?php echo $recordDev[0]['direccion'];?></td>
                            <td><?php echo $recordDev[0]['departamento'];?></td>
                            <td><?php echo $recordDev[0]['municipio'];?></td>
                            <td><?php echo $recordDev[0]['documentos'];?></td>
                            <td><input type="checkbox" name="listaDev[]" checked="checked" value="<?php echo $listaDev[$i];?>"></td>
                        </tr>
                        <?php                     }
                    ?>
                </table>
            </form>
        </div>
        <?php         }else{
            ?>
        <div id="contenido">
            <hr/>
            <table width="95%" border="0" cellpadding="0" cellspacing="1" class="tabS">
                <tr class="titulos2"><th>La operaci&oacute;n de devoluci&oacute;n ha sido satisfactoria de los siguientes radicados</th></tr>
            </table>
            <table width="95%" border="0" cellpadding="0" cellspacing="1" class="tabS">
                <tr class="titulos4">
                    <th>N&uacute;mero del radicado</th>
                    <th>Planilla No.</th>
                    <th>Fecha Envio</th>
                    <th>Destinatario</th>
                    <th>Direcci&oacute;n</th>
                    <th>Departamento</th>
                    <th>Municipio</th>
                    <th>Documentos</th>
                </tr>
                <?php                 $numelem=count($listaDev);
                //print_r($listaDev);
                $devoluciones->setMotiDevol($motdev);
                $devoluciones->setNumeDepe($dependencia);
                $devoluciones->setUsuaCodi($codusuario);
                $devoluciones->setRolId($id_rol);
                $devoluciones->setObserDevol($commentdev);
                $devoluciones->setDocUsuario($usua_doc);
                $log->setUsuaCodi($codusuario);
                $log->setDepeCodi($dependencia);
                $log->setRolId($id_rol);
                $log->setDenomDoc($doc='Radicado');
                $log->setAction('document_delver_returned');
                $log->setOpera('Devolucion de radicado enviado');
                for($i=0;$i<$numelem;$i++){
                    list($radtmp,$cptmp)=explode(",",$listaDev[$i]);
                    $listadoar=$tx->consultar($radtmp, $dependencia);
                    if(count($listadoar)!=0){
                        $depe_ante=$listadoar[0]["USUA_DEPE_ANTE"];
                        include_once $ruta_raiz . '/core/clases/usuarioOrfeo.php';
                        $usuarioDes = new usuarioOrfeo ( $ruta_raiz );
                        $usuarioDes->setDepe_codi ( $dependencia );
                        $usuarioDes->setRols ( 1 );
                        $usuarioDes->consultar_usuario_depe_rol ();
                        $nombre_usuario = $usuarioDes->getLogin () . ' - ' . $usuarioDes->getNombre ();
                        $LoginOrigen = 'DSALIDA';
                        $depOrigen = 999;
                        $depDestino = $dependencia;
                        $codUsDestino = $usuarioDes->getUsua_codi ();
                        $codUsOrigen = 2;
                        $tomarNivel = "no";
                        $motivo=1;
                        $observa = "Por devolucion Usuario Solicitante " . $usua_nomb. ' - ' . $motivo;
                        $codTx = 66;
                        $carp_codi = 0;
                        $depe_nomb = 'Salida';
                        $id_rol = 1;
                        $usuaDocOrigen = '999';
                        $tmpdsar[0]=$radtmp;
                        $tx->reasignar ( $tmpdsar, $LoginOrigen, $depDestino, $depOrigen, $codUsDestino, $codUsOrigen, $tomarNivel, $observa, $codTx, $carp_codi, $id_rol, $usuaDocOrigen );
                        //echo "<font color='red'>Documento Des-Archivado</font>";
                    }
                    $devoluciones->setNumeRadi($radtmp);
                    $devoluciones->setCopia($cptmp);
                    $log->setNumDocu($radtmp);
                    $log->registroEvento();
                    $devoluciones->reportarDevolucion();
                    $recordDev=$devoluciones->consulxDevolver();
                    ?>
                        <tr class="listado2">
                            <td><?php echo $recordDev[0]['numradi'];?></td>
                            <td><?php echo $recordDev[0]['planilla'];?></td>
                            <td><?php echo $recordDev[0]['fechenvio'];?></td>
                            <td><?php echo $recordDev[0]['destinatario'];?></td>
                            <td><?php echo $recordDev[0]['direccion'];?></td>
                            <td><?php echo $recordDev[0]['departamento'];?></td>
                            <td><?php echo $recordDev[0]['municipio'];?></td>
                            <td><?php echo $recordDev[0]['documentos'];?></td>
                        </tr>
                <?php                 }
                ?>
            </table>
            <hr/>
        </div>
        <?php         }
        ?>
    </body>
</html>
