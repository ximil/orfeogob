<?php session_start(); date_default_timezone_set('America/Bogota'); date_default_timezone_set('America/Bogota');
//error_reporting(E_ALL);
//ini_set('display_errors','On');
ini_set('memory_limit','600M');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
foreach ($_GET as $key => $valor)   ${$key} = $valor;
foreach ($_POST as $key => $valor)   ${$key} = $valor;
foreach ($_SESSION as $key => $valor)   ${$key} = $valor;
foreach ($_SERVER as $key => $valor)   ${$key} = $valor;
$ruta_raiz='../../../..';
date_default_timezone_set("America/Bogota");
//Validando session
include_once "$ruta_raiz/core/vista/validadte.php";
//Cargando el medio de envio
include_once "$ruta_raiz/core/Modulos/envios/clases/envios.php";
$envios= new envios($ruta_raiz);
$recordME=$envios->consultarMedioEnvio();
$nummden=count($recordME);
$optMDenvio="\n<option value='0'>--Seleccione Medio de Envio--</option>\n";
for ($i=0; $i<$nummden;$i++){
    $codenvio=$recordME[$i]['codenvio'];
    $medenvio=$recordME[$i]['medenvio'];
    $optMDenvio .="\t\t\t<option value='$codenvio'>$codenvio - $medenvio</option>\n";
}
//Cargando las dependencias de la entidad
include_once $ruta_raiz . '/core/clases/dependencia.php';
//Inicializando las dependencias
$depe = new dependencia($ruta_raiz);
$depe->setDepe_codi($dependencia);
$dependenciasX= $depe->consultarTodo();
$numdep=  count($dependenciasX);
$optionDepe="\n<option value='0-Todas las dependencias'>--Seleccione Dependencia--</option>\n";
//Carga en una variable para imprimir las dependencias
for($i=0; $i<$numdep;$i++){
    $nomDepe = $dependenciasX [$i] ['depe_nomb'];
    $codDepe = $dependenciasX [$i] ['depe_codi'];
    $optionDepe .="\t \t \t<option value='$codDepe-$nomDepe'>$codDepe - $nomDepe</option>\n";
}
$script="$ruta_raiz/core/Modulos/envios/vista/operReporte.php";
?>
<html>
    <head>
        <title>Pruebas de estadisticas de Devoluciones</title>
        <link rel="stylesheet" href="<?php echo $ruta_raiz; ?>/estilos/default/orfeo.css">
        <link rel="stylesheet" href="<?php echo $ruta_raiz; ?>/estilos/Style.css" type="text/css">
        <link rel="stylesheet" type="text/  css" 	href="<?php echo $ruta_raiz; ?>/js/calendario/calendar.css">
        <script language="JavaScript" type="text/javascript"	src="<?php echo $ruta_raiz; ?>/js/calendario/calendar_eu.js"></script>
        <script language="JavaScript" src="<?php echo $ruta_raiz; ?>/js/common.js"></script>
        <script>
        function repDevol(){
            var fechaI=document.formesta.fecha_ini.value;
            var fechaF=document.formesta.fecha_fin.value;
            var depen=document.formesta.depsel.value;
            var menvio=document.formesta.tipo_envio.value;
            var parameter='action=RepDev&fecha_ini='+fechaI+'&fecha_fin='+fechaF+'&depsel='+depen+'&tipo_envio='+menvio;
            var getpara='';
            partes('<?php echo $script?>','repor_dev',parameter,getpara);
        }
        </script>
    </head>
    <body>
        <header id="page_header" style="width: 95%">
            <h1>LISTADO DE DOCUMENTOS EN DEVOLUCI&Oacute;N POR AGENCIA DE CORREO</h1>
        </header>
        <table><tr><td></td></tr></table>
        <form name="formesta" action="estadisticaDevoluciones.php" style="margin: 0" method="post">
            <center>
            <table width="550" border="0" cellpadding="0" cellspacing="2"  class="tabS">
                <tr>
                    <td width="415" class="titulos2"><b>Desde fecha (aaaa/mm/dd)</b></td>
                    <td width="415" class="listado2">
                        &nbsp;<?php $mesante = date("d/m/Y", mktime(0, 0, 0, date("m") - 1, date("d"), date("Y"))); ?>
                                    <input type="text" class="tex_area" name="fecha_ini"	id="fecha_ini" size="10" value="<?php echo $mesante; ?>" /> 
                                    <script	language="javascript">
            var A_CALTPL = {'imgpath': '<?php echo $ruta_raiz; ?>/js/calendario/img/',
                'months': ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                'weekdays': ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'], 'yearscroll': true, 'weekstart': 1, 'centyear': 70}
            new tcal({'formname': 'formesta', 'controlname': 'fecha_ini'}, A_CALTPL);</script>
                    </td>
                </tr>
                <tr>
                    <td width="415" class="titulos2"><b>Hasta fecha(aaaa/mm/dd)</b></td>
                    <td width="415" class="listado2">
                        &nbsp;
                                    <input type="text" class="tex_area" name="fecha_fin"	id="fecha_fin" size="10" value="<?php echo date('d/m/Y'); ?>" /> 
                                    <script	language="javascript">
            var A_CALTPL = {'imgpath': '<?php echo $ruta_raiz; ?>/js/calendario/img/',
                'months': ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                'weekdays': ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'], 'yearscroll': true, 'weekstart': 1, 'centyear': 70}
            new tcal({'formname': 'formesta', 'controlname': 'fecha_fin'}, A_CALTPL);</script>

                    </td>
                </tr>
                <tr>
                    <td height="26" class="titulos2"><b>Tipo de envio</b></td>
                    <td valign="top" class="listado2"><select name="tipo_envio" class="select"><?php echo $optMDenvio;?>
                        </select></td>
                </tr>
                <tr>
                    <td height="26" class="titulos2"><b>Dependencia</b></td>
                    <td height="26" class="listado2"><select name="depsel" class="select"><?php echo $optionDepe;?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="listado2">
                        <center><input type="button" name="submitesta" value="Generar informe" class="botones_mediano2" onclick="repDevol();"></center>
                    </td>
                </tr>
            </table>
            </center>
        </form>
        <div id='repor_dev' name='repor_dev'></div>
    </body>
</html>
