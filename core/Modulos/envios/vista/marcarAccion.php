<?php session_start(); date_default_timezone_set('America/Bogota'); date_default_timezone_set('America/Bogota');
error_reporting(E_ALL);
ini_set('display_errors','On');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//foreach ($_GET as $key => $valor)  $$key = $valor;
foreach ($_POST as $key => $valor)  $$key = $valor;
foreach ($_SESSION as $key => $valor)  $$key = $valor;
//foreach ($_SERVER as $key => $valor)  $$key = $valor;
$ruta_raiz='../../../..';
include_once "$ruta_raiz/core/Modulos/envios/clases/envios.php";
$envios= new envios($ruta_raiz);
if(isset($marcaimp)){
        $val=3;
        $accion="impresos";
}else{
    header('Location:listaImpresos.php');
}
?>
<html>
    <head>
        <link rel="stylesheet" href="<?php echo $ruta_raiz; ?>/estilos/default/orfeo.css">
        <link rel="stylesheet" href="<?php echo $ruta_raiz; ?>/estilos/Style.css" type="text/css">
        <title>Marcado de documentos como<?php echo $accion;?></title>
    </head>
    <body>
        <header id="page_header" style="width: 95%">
            <h1>Listado de documentos marcados como <?php echo $accion; ?></h1>
        </header>
        <a href="listaImpresos.php"><pre>Devolver al listado</pre></a>
        <table width="95%" border="0"  cellpadding="0" cellspacing="1" class="tabS">
            <tr class="titulos4">
                <th>Estado</th>
                <th>Radicado</th>
                <th>Radicado Padre</th>
                <th>Copia No.</th>
                <th>Destinatario</th>
                <th>Direcci&oacute;n</th>
                <th>Pa&iacute;s</th>
                <th>Depto.</th>
                <th>Municipio</th>
                <th>Asunto</th>
            </tr>
    <?php     $numelem=count($listaMarca);
    $envios->setRadiDepeRadi($dependencia);
    $envios->setRolUsua($id_rol);
    $envios->setEstaCodi($val);
    $envios->setUsuaCodi($codusuario);
    for($i=0; $i<$numelem; $i++){
        $tmprad=$listaMarca[$i];
        $envios->setRadiNumeRadi($tmprad);
        $status=$envios->accionxEstado($tmprad,$val,$dependencia,$codusuario,$id_rol);
        $recordAC=$envios->infoxAccion($tmprad);
        ?>
            <tr  class="listado2">
                <td><?php echo $status;?></td>
                <td><?php echo $recordAC['numrad'];?></td>
                <td><?php echo $recordAC['radi_padre'];?></td>
                <td><?php echo $recordAC['copia'];?></td>
                <td><?php echo $recordAC['destinatario'];?></td>
                <td><?php echo $recordAC['direccion'];?></td>
                <td><?php echo $recordAC['pais'];?></td>
                <td><?php echo $recordAC['depto'];?></td>
                <td><?php echo $recordAC['municipio'];?></td>
                <td><?php echo $recordAC['asunto'];?></td>
            </tr>
        <?php     }

?>
        </table>
        <a href="listaImpresos.php"><pre>Devolver al listado</pre></a>
    </body>
</html>