
<?php session_start(); date_default_timezone_set('America/Bogota'); date_default_timezone_set('America/Bogota');
//error_reporting(E_ALL);
//ini_set('display_errors','On');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
foreach ($_GET as $key => $valor)  $$key = $valor;
foreach ($_POST as $key => $valor)  $$key = $valor;
foreach ($_SESSION as $key => $valor)  $$key = $valor;
foreach ($_SERVER as $key => $valor)  $$key = $valor;
$ruta_raiz='../../../..';
//Validando session
include_once "$ruta_raiz/core/vista/validadte.php";
//Cargando las dependencias de la entidad
include_once $ruta_raiz . '/core/clases/dependencia.php';
//Inicializando las dependencias
$depe = new dependencia($ruta_raiz);
$depe->setDepe_codi($dependencia);
$dependenciasX= $depe->consultarTodo();
$numdep=  count($dependenciasX);
$optionDepe="\n<option value='0'>--Seleccione Dependencia--</option>\n";
//Carga en una variable para imprimir las dependencias
for($i=0; $i<$numdep;$i++){
    $nomDepe = $dependenciasX [$i] ['depe_nomb'];
    $codDepe = $dependenciasX [$i] ['depe_codi'];
    $optionDepe .="\t \t \t<option value='$codDepe'>$codDepe - $nomDepe</option>\n";
}
$script="$ruta_raiz/core/Modulos/envios/vista/operDevoluciones.php";
date_default_timezone_set('America/Bogota');
$diante = date("d/m/Y", mktime(0, 0, 0, date("m"), date("d")-1, date("Y")));
$fecha_hoy=date('d/m/Y');
$hor=date('H');
$min=date('i');
?>
<html>
    <head>
        <title>Pruebas de estadisticas de Envios</title>
        <link rel="stylesheet" href="<?php echo $ruta_raiz; ?>/estilos/default/orfeo.css">
        <link rel="stylesheet" href="<?php echo $ruta_raiz; ?>/estilos/Style.css" type="text/css">
        <link rel="stylesheet" type="text/  css" 	href="<?php echo $ruta_raiz; ?>/js/calendario/calendar.css">
        <script language="JavaScript" type="text/javascript"	src="<?php echo $ruta_raiz; ?>/js/calendario/calendar_eu.js"></script>
        <script language="JavaScript" src="<?php echo $ruta_raiz; ?>/js/common.js"></script>
        <script>
        function erroFech(){
            alert('Fecha no valida');
        }
        function genListado(){
            var fechaI=document.formdev.fecha_ini.value;
            var fechaF=document.formdev.fecha_fin.value;
            var hora_actu=document.formdev.hora_actu.value;
            var hora=document.formdev.hora.value;
            var minuto=document.formdev.minuto.value;
            var depen=document.formdev.depsel.value;
            var parameter='action=ListGen&fecha_ini='+fechaI+'+'+hora+':'+minuto+'&fecha_fin='+fechaF+'+'+hora_actu+'&depsel='+depen;
            var getpara='';
            partes('<?php echo $script?>','lista_dev',parameter,getpara);
        }
        function devolDocs(){
            var fechaI=document.hadev.fecha_ini.value;
            var fechaF=document.hadev.fecha_fin.value;
            var depsel=document.hadev.depsel.value;
            var parameter='action=Devol&fecha_ini='+fechaI+'&fecha_fin='+fechaF+'&depsel='+depsel;
            var getpara='';
            partes('<?php echo $script?>','lista_dev',parameter,getpara);
        }
        </script>
    </head>
    <body>
        <header id="page_header" style="width: 95%">
            <h1>LISTADO DE DOCUMENTOS EN DEVOLUCI&Oacute;N POR AGENCIA DE CORREO</h1>
        </header>
        <table><tr><td></td></tr></table>
        <form action="devolucionEspera.php" style="margin: 0" method="post" name="formdev" id="formdev">
            <input type='hidden' id='fecha_fin' name='fecha_fin' value='<?php echo $fecha_hoy;?>'>
            <input type='hidden' id='hora_actu' name='hora_actu' value='<?php echo $hor.':'.$min?>'>
            <center>
            <table width="550" border="0" cellpadding="0" cellspacing="2"  class="tabS">
                <tr>
                    <td width="415" class="titulo2">Fecha de Espera(aaaa/mm/dd)</td>
                    <td width="415" class="listado2">
                        &nbsp;
                                    <input type="text" class="tex_area" name="fecha" id="fecha_ini" size="10" value="<?php echo $diante; ?>"/> 
                                    <script	language="javascript">
            var A_CALTPL = {'imgpath': '<?php echo $ruta_raiz; ?>/js/calendario/img/',
                'months': ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                'weekdays': ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'], 'yearscroll': true, 'weekstart': 1, 'centyear': 70}
            new tcal({'formname': 'formdev', 'controlname': 'fecha'}, A_CALTPL);</script>

                    </td>
                </tr>
                <tr>
                    <td class="titulo3">Desde la hora</td>
                    <td class="listado2">
                        <select id="hora" name="hora" class="select">
                            <?php                             for($i=0;$i<24;$i++){
                                if($i<10){
                                    $h='0'.$i;
                                }else
                                    $h=$i;
                                echo "<option";
                                if($i==$hor)
                                    echo " selected='selected'";
                                echo ">$h</option>\n";
                            }
                            ?>
                        </select> : <select id="minuto" name="minuto" class="select">
                            <?php                             for($i=0;$i<60;$i++){
                                if($i<10){
                                    $m='0'.$i;
                                }else
                                    $m=$i;
                                echo "<option";
                                if($i==$min)
                                    echo " selected='selected'";
                                echo ">$m</option>\n";
                            }
                            ?>
                            </select>
                    </td>
                </tr>
                <tr>
                    <td height="26" class="titulo2">Dependencia</td>
                    <td height="26" class="listado2"><select name="depsel" class="select"><?php echo $optionDepe;?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="listado2">
                        <center><input type="button" name="submitesta" value="Generar informe" class="botones_mediano2" onclick="genListado();"></center>
                    </td>
                </tr>
            </table>
            </center>
        </form>
        <div id='lista_dev' name='lista_dev'></div>
    </body>
</html>
