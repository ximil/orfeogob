<?php error_reporting(E_ALL);
ini_set('display_errors','On');
ini_set('memory_limit','2048M');
ini_set('max_execution_time', 600);
/*
 * name operDevoluciones
 * Realiza interactua con ajax
 */
session_start(); date_default_timezone_set('America/Bogota'); date_default_timezone_set('America/Bogota');
foreach ($_SESSION as $key => $valor)
    $$key = $valor;
foreach ($_POST as $key => $valor)
    $$key = $valor;
$krd = $_SESSION ["krd"];
$dependencia = $_SESSION ["dependencia"];
$usua_doc = $_SESSION ["usua_doc"];
$codusuario = $_SESSION ["codusuario"];
$id_rol=$_SESSION["id_rol"];
$ruta_raiz = "../../../..";    
$accion = $_POST ['action'];
include_once $ruta_raiz.'/core/clases/file-class.php';
$encrypt=new file();
include_once $ruta_raiz.'/core/config/config-inc.php';
include($ruta_raiz.'/core/clases/timecount.php');
$timerRespuesta= new timecount();
//Cargando variables de log
/*if(isset($_SERVER['HTTP_X_FORWARD_FOR'])){
    $proxy=$_SERVER['HTTP_X_FORWARD_FOR'];
}else
    $proxy=$_SERVER['REMOTE_ADDR'];
include_once "$ruta_raiz/core/clases/log.php";
$log = new log($ruta_raiz);
$log->setAddrC($_SERVER['REMOTE_ADDR']);
$log->setProxyAd($proxy);
$log->setDepeCodi($dependencia);
$log->setUsuaCodi($codusuario);
$log->setRolId($id_rol);
$log->setDenomDoc('Radicado');*/
//print_r($_POST);
switch ($accion) {
    case 'RepEnv':
	include "$ruta_raiz/core/Modulos/envios/clases/envios.php";
	$envios= new envios($ruta_raiz);
            $envios->setRadiDepeRadi($depsel);
            $envios->setMedioEnvio($tipo_envio);
            $envios->setFechaIni($fecha_ini);
            $envios->setFechaFin($fecha_fin);
	    $timerRespuesta->intertime('Antes de consulta');
            $recordEE=$envios->estadisticaEnvios();
            $contador=count($recordEE)-1;
            $envios->setNumReporte($contador);
            $envios->setReporte($recordEE);
                $envios->setUsuaCodi($codusuario);
		$timerRespuesta->intertime('Antes de Render');
                $data=$envios->generarReporte();
                $flpdf=$encrypt->encriptar($data['pdf']);
                $flcsv=$encrypt->encriptar($data['csv']);
                echo "<br><table class=borde_tab width='100%'><tr><td class=listado2><CENTER>Archivos generados  </td>
                   <td><a href='$ruta_raiz/core/vista/image.php?nombArchivo=$flpdf' target='image' >Reporte en PDF</a></tD>
                   <td><a href='$ruta_raiz/core/vista/image.php?nombArchivo=$flcsv' target='image' >Reporte en CSV</a></td></tr></table>";
                echo $data['html'];
		echo "<div id='timer' name='timer' style='alignment-adjust: auto;text-align: right;font-size: 10'>";
		$timerRespuesta->endtime();
      		$timerRespuesta->printToIntervalo();
		echo "</div>\n";
		echo "\t\t</body>\n</html>";
        break;
    case 'RepDev':
	include_once "$ruta_raiz/core/Modulos/envios/clases/devoluciones.php";
	$devoluciones= new devoluciones($ruta_raiz);
	$deparr=explode('-',$depsel);
	$depecod=$deparr[0];
	$depselNomb=$deparr[1];
	$devoluciones->setNumeDepe($depecod);
        $devoluciones->setMedEnvio($tipo_envio);
        $devoluciones->setUsuaCodi($codusuario);
        $devoluciones->setFechaIni($fecha_ini);
        $devoluciones->setFechaFin($fecha_fin);
	$timerRespuesta->intertime('Antes de consulta');
        $recordED=$devoluciones->estadisticaDevolucion();
        $devoluciones->setReporte($recordED);
        $contador=count($recordED)-1;
        $devoluciones->setNumReporte($contador);
	$timerRespuesta->intertime('Antes de Render');
        $data=$devoluciones->generarReporte($usua_nomb,$depselNomb);
        $flpdf=$encrypt->encriptar($data['pdf']);
        $flcsv=$encrypt->encriptar($data['csv']);
        echo "<br><table class=borde_tab width='100%'><tr><td class=listado2><CENTER>Archivos generados  </td>
                <td><a href='$ruta_raiz/core/vista/image.php?nombArchivo=$flpdf' target='image' >Reporte en PDF</a></tD>
                <td><a href='$ruta_raiz/core/vista/image.php?nombArchivo=$flcsv' target='image' >Reporte en CSV</a></td></tr></table>";
        echo $data['html'];
	echo "<div id='timer' name='timer' style='alignment-adjust: auto;text-align: right;font-size: 10'>";
        $timerRespuesta->endtime();
        $timerRespuesta->printToIntervalo();
        echo "</div>";
        break;
        
}

	?>
