<?php //error_reporting(E_ALL);
//ini_set('display_errors','On');
/*
 * name operDevoluciones
 * Realiza interactua con ajax
 */
session_start(); date_default_timezone_set('America/Bogota'); date_default_timezone_set('America/Bogota');
foreach ($_SESSION as $key => $valor)
    $$key = $valor;
foreach ($_POST as $key => $valor)
    $$key = $valor;
$krd = $_SESSION ["krd"];
$dependencia = $_SESSION ["dependencia"];
$usua_doc = $_SESSION ["usua_doc"];
$codusuario = $_SESSION ["codusuario"];
$id_rol=$_SESSION["id_rol"];
$ruta_raiz = "../../../..";
$accion = $_POST ['action'];
include_once "$ruta_raiz/core/Modulos/envios/clases/devoluciones.php";
$devoluciones = new devoluciones($ruta_raiz);
//Cargando el historico
include_once "$ruta_raiz/core/Modulos/radicacion/clases/tx.php";
$tx= new tx($ruta_raiz);
//Cargando variables de log
if(isset($_SERVER['HTTP_X_FORWARD_FOR'])){
    $proxy=$_SERVER['HTTP_X_FORWARD_FOR'];
}else
    $proxy=$_SERVER['REMOTE_ADDR'];
include_once "$ruta_raiz/core/clases/log.php";
$log = new log($ruta_raiz);
$log->setAddrC($_SERVER['REMOTE_ADDR']);
$log->setProxyAd($proxy);
$log->setDepeCodi($dependencia);
$log->setUsuaCodi($codusuario);
$log->setRolId($id_rol);
$log->setDenomDoc('Radicado');
//print_r($_POST);
switch ($accion) {
    case 'ListGen':
        $diante = date("d/m/Y", mktime(0,0,0, date("m"), date("d")-1, date("Y")));
        $date1= strtotime(str_replace('/','-',$diante));
        $date2= strtotime(str_replace('/','-',substr($fecha_ini,0,10)));
        if($date1 >=  $date2){
        $devoluciones->setNumeDepe($depsel);
        $devoluciones->setFechaIni($fecha_ini);
        $devoluciones->setFechaFin($fecha_fin);
        $cont=$devoluciones->devolCTEspe();
        if($cont!=0){
            $resul=$devoluciones->devolTEspe();
            ?>
            <form method="post" name="hadev" id="hadev">
                <!--input type="hidden" name="action" value="Devol"-->
                <input type="hidden" name="fecha_fin" value="<?php echo $fecha_fin?>">
                <input type="hidden" name="fecha_ini" value="<?php echo $fecha_ini?>">
                <input type="hidden" name="depsel" value="<?php echo $depsel?>">
                <center>
                <table class='tabS'>
                    <tr class='titulos2'>
                        <td>N&uacute;mero del radicado</td>
                        <td>Dependencia</td>
                        <td>Fecha de devolucion</td>
                        <td>Tiempo de espera</td>
                    </tr>
                <?php                 for($i=0;$i<count($resul);$i++){
                    $numrad=$resul[$i]['numrad'];
                    $depen=$resul[$i]['dependencia'];
                    $tespera=$resul[$i]['t-espera'];
                    ?>
                    <tr class='listado2'>
                        <td><?php echo $numrad;?></td>
                        <td><?php echo $depen?></td>
                        <td><?php echo $fecha_fin;?></td>
                        <td><?php echo $tespera;?></td>
                    </tr>
                <?php                     }
                ?>
                </table>
                </center>
                <center><input type='button' value='Devolver' onclick='devolDocs();' class="botones_mediano2"></center>
            </form>
            <?php         }  else {
            echo "<font color='red'>No hay datos para hacer esta operaci&oacute;n</script>";
        }

    }else{
        echo "<font color='green'>La fecha debe ser menor de 24 horas</font>";
    }
        break;
    case 'Devol':
        $observa='Devuelto por tener mas de 24 horas marcado como impreso';
        $devoluciones->setNumeDepe($depsel);
        $devoluciones->setFechaIni($fecha_ini);
        $devoluciones->setFechaFin($fecha_fin);
        $resul=$devoluciones->devolTEspe();
        ?>
            <center>
                <table class='tabS'>
                    <tr class='titulos2'>
                        <td>Status Acci&oacute;n</td>
                        <td>N&uacute;mero del radicado</td>
                        <td>Dependencia</td>
                        <td>Fecha de devolucion</td>
                        <td>Tiempo de espera</td>
                    </tr>
                <?php                 for($i=0;$i<count($resul);$i++){
                    $numrad=$resul[$i]['numrad'];
                    $radSel[$i]=$resul[$i]['numrad'];
                    $depen=$resul[$i]['dependencia'];
                    $tespera=$resul[$i]['t-espera'];
                    ?>
                    <tr class='listado2'>
                        <td>OK</td>
                        <td><?php echo $numrad;?></td>
                        <td><?php echo $depen?></td>
                        <td><?php echo $fecha_fin;?></td>
                        <td><?php echo $tespera;?></td>
                    </tr>
                <?php
                    $log->setAction('document_delver_returned');
                    $log->setOpera('Devuelto por tiempo de espera');
                    $log->setNumDocu($numrad);
                    $log->registroEvento();
                    }
                ?>
                </table>
            </center>
                <?php                 $tx=69;
                $restant=$tx->insertaHistorico($radSel, $dependencia,$dependencia,$usua_doc,$usua_doc,$codusuario,$codusuario,$id_rol,$id_rol,$observa, $tx);
                $devoluciones->devolxTEspe();
        break;

}

?>
