<?php session_start(); date_default_timezone_set('America/Bogota'); date_default_timezone_set('America/Bogota');
error_reporting(E_ALL);
ini_set('display_errors','On');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
foreach ($_GET as $key => $valor)  $$key = $valor;
foreach ($_POST as $key => $valor)  $$key = $valor;
foreach ($_SESSION as $key => $valor)  $$key = $valor;
foreach ($_SERVER as $key => $valor)  $$key = $valor;
$ruta_raiz='../../../..';
include_once "$ruta_raiz/core/Modulos/envios/clases/envios.php";
$envios= new envios($ruta_raiz);
//Cargando las dependencias de la entidad
include_once $ruta_raiz . '/core/clases/dependencia.php';
if(isset($depsel)){
    $dependencia=$depsel;
}
//Inicializando las dependencias
$depe = new dependencia($ruta_raiz);
$depe->setDepe_codi($dependencia);
$dependenciasX= $depe->consultarTodo();
$numdep=  count($dependenciasX);
$optionDepe="";
//Carga en una variable para imprimir las dependencias
for($i=0; $i<$numdep;$i++){
    $nomDepe = $dependenciasX [$i] ['depe_nomb'];
    $codDepe = $dependenciasX [$i] ['depe_codi'];
    $optionDepe .="\t \t \t<option ";
    if($codDepe == $dependencia){
        $optionDepe .="selected='selected'";
    }
    $optionDepe .=" value='$codDepe'>$codDepe - $nomDepe</option>\n";
}
if(!isset($nomcarpeta)){
    $nomcarpeta="imprimir";
    $estarad=2;
}else{
    if($nomcarpeta=="impresos"){
        $estarad=3;
    }
    elseif($nomcarpeta=="imprimir"){
        $estarad=2;
    }else{
        $nomcarpeta="imprimir";
        $estarad=2;
    }
}
//$encabezado=session_name() . "=" . session_id() . "&krd=$krd";
?>
<html>
    <head>
        <meta http-equiv="Cache-Control" content="cache">
        <meta http-equiv="Pragma" content="public">
        <!--link rel="stylesheet" href="<?php echo $ruta_raiz;?>/old/estilos/table.css" type="text/css"-->
        <link rel="stylesheet" href="<?php echo $ruta_raiz;?>/estilos/default/orfeo.css">
        <link rel="stylesheet" href="<?php echo $ruta_raiz; ?>/estilos/Style.css" type="text/css">
        <title>Prueba de impresos</title>
        <script type="text/javascript">
   function isNumberKey(evt)
   {
   var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 44 && (charCode < 48 || charCode > 57))
        return false;
 
        return true;
    }
   function valCB(){
       var chk=document.forms["marcarEnv"].elements["listaMarca[]"];
       var marcado=false;
       for(var i=0; i<chk.length;i++){
           if(chk[i].checked){
               marcado=true;
               break;
           }
       }
       if(marcado==false){
           alert("Marque documento(s) a enviar");
           return false;
       }
       return true;
   }
        </script>
    </head>
    <body bgcolor="#FFFFFF">
        <header id="page_header" style="width: 95%">
            <h1>Documentos Impresos y por imprimir</h1>  
            <form style="margin:0" action="<?php echo $PHP_SELF; ?>" method="post">
                <table width="100%"  border="0" cellpadding="0" cellspacing="2" class="tabS">
                    <tr>
                        <td class="titulos4" style="width:33%">CARPETA DE <?php echo strtoupper($nomcarpeta); ?></td>
                        <td class="titulos4" style="width:33%"><center><?php echo $usua_nomb;?></center></td>
                        <td class="titulos4" style="width:33%">Dependencia del sistema
                        <select class="select" name="depsel" onchange='this.form.submit()'><?php echo $optionDepe;?></select></td>
                    </tr>
                </table>
                <table width="100%" border="0" cellpadding="0" cellspacing="2" class="tabS">
                    <tr>
                        <td class="titulos2" style="width:25%">N&uacute;mero de radicado</td>
                        <td colspan="2" class="listado2"><center><input type="text" name="numrad_sol" size="100" id="txtCra" autocomplete="off" onkeypress="return isNumberKey(event)"/></center></td>
                        <td style="width: 10%" class="listado2"><input type="hidden" name="nomcarpeta" value="<?php echo $nomcarpeta;?>"><input type="submit" name="submitrad" value="Buscar" class="botones_funcion"></td>
                    </tr>
                </table>
            </form>
            <nav>
                <ul>
                    <li><a href="<?php echo $ruta_raiz; ?>/core/Modulos/envios/vista/listaImpresos.php?nomcarpeta=impresos" >Impresos</a></li>
                    <li><a href="<?php echo $ruta_raiz; ?>/core/Modulos/envios/vista/listaImpresos.php?nomcarpeta=imprimir">Por Imprimir</a></li>
                    <li><input type="submit" name="marcaimp" value="Marcar como impreso" form="marcarImp"></li>
                    <li><input type="submit" name="genlistent" value="Generar listado de entrega"></li>
                </ul>
            </nav>
        </header>
        <?php         $correrad=0;
        if(isset($numrad_sol)){    
            $trimnumrad=trim($numrad_sol,' ');
            //echo "<p>Llegue 1</p>";
            if(preg_match("/[[:digit:]]/",$trimnumrad) && preg_match("/,/",$trimnumrad)){
                //echo "<p>Llegue 2</p>";
                    $multirad=preg_split("/,/",$trimnumrad);
                    $j=0;
                    for($h=0;$h<count($multirad);$h++){
                        if(strlen($multirad[$h])==14){
                            $radxeva[$j] = $multirad[$h];
                            $j++;
                        }
                    }
                    if($j>0){
                        $correrad=2;
                        //print_r($multirad);
                    }
                
                   
            } if(is_numeric($trimnumrad) && strlen($trimnumrad)==14){
                        $correrad=1;
                    
                }
        }
        $envios->setRadiDepeRadi($dependencia);
            $envios->setEstaCodi($estarad);
            //
            if(isset($submitrad) && $correrad!=0){
                if($correrad==1){
                    $envios->setRadiNumeRadi($numrad_sol);
                    $recordImp=$envios->consultarxRadicado($numrad_sol,$estarad);
                }else{
                        $envios->setRadiNumeRadi($radxeva);
                        $recordImp=$envios->consultarxRadicado($radxeva,$estarad);
                }
            }
            else{
                $recordImp=$envios->consultarxEstado($dependencia,$estarad);
            }
            if ($recordImp['error']!= ""){
                echo $recordImp['error'];
            }else{
        ?>
        <!--table width="98%" align="center" cellspacing="0" cellpadding="0" class='borde_tab'-->
        <div style=" padding : 4px; width :100%; height : 400px; overflow : auto;">
            <form action="marcarAccion.php" id="marcarImp" method="post" onsubmit="return valCB();">
                <input type="checkbox" name="listaMarca[]" value="" id="chk000" style="display:none">
        <!--table style="width:95%">
            <tr class="grisCCCCCC">
                <!--div style="padding : 4px; width :100%; height : 400px; overflow : auto;" -->
                    <table width="95%"  border="0"  cellpadding="0" cellspacing="1">
                        <tr class="titulos4">
                            <th width='130px'><img src='<?php echo $ruta_raiz; ?>/old/imagenes/estadoDoc.gif' width="130px" border=0></img></th>
                            <th align="center">Radicado</th>
                            <th align="center">Radicado Padre</th>
                            <th align="center">Descripci&oacute;n</th>
                            <th align="center">Fecha de radicaci&oacute;n</th>
                            <th align="center">Generado por</th>
                            <th></th>
                        </tr>
                        
            <?php  
                for ($i = 0; $i < (count($recordImp)-1); $i++) {
                    $numrad      = $recordImp[$i]['numrad'];
                    $radicado_padre = $recordImp[$i]['radicado_padre'];
                    $descri_anex = $recordImp[$i]['descri_anex'];
                    $fecharadi   = $recordImp[$i]['fecharadi'];
                    $usua_login  = $recordImp[$i]['usua_login'];
                    if($i%2 == 0)
                        $atr="listado2_center";
                    else{
                        $atr="titulos2";
                    }
                ?>
              <tr>
                  <td align="center">
                      <?php 
                      if($estarad==2){
                        echo "<img src=$ruta_raiz/old/imagenes/docRadicado.gif  border=0/>";
                      }  else {
                        echo "<img src=$ruta_raiz/old/imagenes/docImpreso.gif  border=0/>";
                      }
                      ?>
                  </td>
                  <td class="<?php echo $atr?>" align="center"><?php echo $numrad; ?></td>
                  <td class="<?php echo $atr;?>" align="center"><?php echo $radicado_padre; ?></td>
                  <td class="<?php echo $atr;?>" align="center" style="white-space: pre-line"><?php echo $descri_anex; ?></td>
                  <td class="<?php echo $atr;?>" align="center"><?php echo $fecharadi; ?></td>
                  <td class="<?php echo $atr;?>" align="center"><?php echo $usua_login;?></td>
                  <td class="<?php echo $atr;?>" align="center"><input type="checkbox" name="listaMarca[]" value="<?php echo $numrad;?>" id="<?php echo "chk$i";?>"></td>
              </tr>
            <?php              }?>
                    </table>
                <!--/div>
                    </tr>
                </table-->
        </form>
        </div>
        <?php             }?>
    </body>
</html>
