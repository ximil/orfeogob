<?php session_start();
date_default_timezone_set('America/Bogota');
//error_reporting(E_ALL);
//ini_set('display_errors', 'On');
ini_set('auto_detect_line_endings', true);
foreach ($_GET as $key => $valor)  $$key = $valor;
foreach ($_POST as $key => $valor)  $$key = $valor;
foreach ($_SESSION as $key => $valor)  $$key = $valor;
$ruta_raiz="../../../..";
//Carga archivo de configuracion del orfeo
include_once "$ruta_raiz/core/config/config-inc.php";
//Carga del timer
include($ruta_raiz.'/core/clases/timecount.php');
$timerRespuesta= new timecount();
//Carga la clase masiva
include_once "$ruta_raiz/core/Modulos/masiva/clases/masiva.php";
$masiva=new masiva($ruta_raiz);
$masiva->setGrupoRadi($gruposel);
$tipoRad=substr($gruposel,-1);
$timerRespuesta->intertime('Antes de consulta');
$recordGM=$masiva->grupoMasiva();
//Cargando la funciones de expediente
include_once "$ruta_raiz/core/Modulos/Expediente/clases/expediente.php";
$expediente=new expediente($ruta_raiz);
//Carga funciones del log
include_once "$ruta_raiz/core/clases/log.php";
$log= new log($ruta_raiz);
//Cargando tx del historico
include_once "$ruta_raiz/core/Modulos/radicacion/clases/tx.php";
$tx= new tx($ruta_raiz);
function return_bytes($val)
{       $val = trim($val);
        $ultimo = strtolower($val{strlen($val)-1});
        switch($ultimo)
        {       // El modificador 'G' se encuentra disponible desde PHP 5.1.0
                case 'g':       $val *= 1024;
                case 'm':       $val *= 1024;
                case 'k':       $val *= 1024;
        }
        return $val;
}
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<html>
    <head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Masiva a modificar</title>
        <link rel="stylesheet" href="<?php echo $ruta_raiz; ?>/estilos/default/orfeo.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $ruta_raiz?>/estilos/desvanecido.css">
	<script src="<?php echo $ruta_raiz?>/js/jquery-1.10.2.js"></script>
	<script language="javascript">
function validar()
{
    if (document.masad.upload.value.length == 0)
        {
            alert('Seleccione archivo con datos precargados');
            document.masad.upload.focus();
            return false;
        }
        else return true;
}
function validar_forma_arch(field_name, allowed_ext){
    obj1=document.txImg;
    var temp_field= 'obj1.'+field_name+'.value';
    field_value=eval(temp_field);
    if(field_value!=""){
        var file_ext= (field_value.substring((field_value.lastIndexOf('.')+1)).toLowerCase());
        ext=allowed_ext.split(',');
        var allow=0;
        for ( var i=0; i < ext.length; i++) {
                if(ext[i]==file_ext){
                    allow=1;
                }
        }
        if(!allow){
            alert('Formato invalidado. Por favor verique sea formato '+allowed_ext);
            return false;
            }
    }
    return false;
}
function   vehtml(mostrar){
      var est=document.getElementById('estadoC').value;
      if(est==1){
        document.getElementById(mostrar).style.display='block'; //.style.visibility='visible';
        document.getElementById('estadoC').value=2;
      }
      if(est==2){
        document.getElementById(mostrar).style.display='none';
        document.getElementById('estadoC').value=1;
      }

    }
    function generarDoc(){
	bloqScreen();
        var numrad=$("#gruposel").val();
	var tiporad='<?php echo strtolower($tpDescRad[$tipoRad])?>';
        //var respuesta;
        var formData="gruporad="+numrad;
        $.ajax({
            type: "POST",
            url: "<?php echo $ruta_raiz?>/core/Modulos/radicacion/vista/crearJson.php",
            data: formData,
            success: function(data){
                alert($(data).filter("#response").text());
		window.location='http://servprodoc.imprenta.gov.co/app.php/plantilla/new/'+tiporad+'?documento='+numrad+'&masiva=1';
            },
            error: function(e){
                console.log(e.message);
            }
        });
    }
    function bloqScreen(){
         if($("#lightbox-shadow").size()==0){
            var theShadow = $('<div id="lightbox-shadow"><br><center><img src="<?php echo $ruta_raiz?>/imagenes/nuevo.gif"></center><br></div>');
                $('body').append(theShadow);
            }
         $("#lightbox-shadow").show();
    }
	</script>
    </head>
    <body>
        <div style='text-align: center ;width: 100%;margin: 0 auto;'>
            <table class="borde_tab" width="100%" cellspacing="2"><tbody><tr><td align="center" class="titulos4">
                Masiva especial
            </td></tr></tbody></table>
            
<?php 
	if(!isset($submas)){
?>	    
            <table>
                <tr class="titulo1">
                    <td>
                       Campos de combinaci&oacute;n
                    </td>
                </tr>
                <tr class="listado1">
                    <td>
                        <a href='#' onclick='vehtml("campcomb");'>Ver listado</a><input type='hidden' value='1' id='estadoC'> <div id="campcomb">*RAD_S* *ASUNTO* *NUM_EXPEDIENTE* *EMPRESA* *NOM_R* *DIR_R* *MPIO_R* *DEPTO_R* *PAIS_R* *CUENTAI* *DIGNATARIO* *TEL_R* *MAIL_R* *DOC_R* </div>
                    </td>
                </tr>
            </table>
            <form action="modificarMasiva.php" id="masad" name="masad" method="post" enctype="multipart/form-data">
	    <input type="hidden" name="gruposel" value="<?php echo $gruposel?>" id='gruposel'>
	    <input type="hidden" name="MAX_FILE_SIZE" value="<?php echo return_bytes(ini_get('upload_max_filesize')); ?>">
            <table style="width:100%;border-spacing:1" class="borde_tab">
                <tr align="center" class="titulos4">
                    <th style="width:33%">MODIDIFICACI&Oacute;N DE MASIVA</th>
                    <th style="width:33%"><?php echo $_SESSION['usua_nomb']?></th>
                    <th style="width:33%"><?php echo $_SESSION['depe_nomb']?></th>
                </tr>
                <tr>
			    <td colspan="7" align="center"><span class="leidos">Cargue su archivo CSV</span><input type="file" name="upload" size="50" class=tex_area onchange="validar_forma_arch('upload','csv')"></td>
                </tr>
                <tr>
                    <td colspan="7" align="center"><input type="submit" name="submas" value="Realizar cambios" class="botones_mediano" onclick="return validar();"><input class="botones_mediano" type="button" value="Elaborar documentos" onclick="return generarDoc();" id="gendoc"></td>
                </tr>
		<tr>
		    <td colspan="7"><font color='red'><b>Nota</b>: Esta modificaci&oacute;n solo aplica a datos del radicado, por tanto la informaci&oacute;n b&aacute;sica de los terceros hecha por medio de listados no se puede modificar en los campos de nombre y n&uacute;mero de documento, son solo campos de verificaci&oacute;n.</font></td>
		</tr>
            </table>
            </form>
        <table style="width:100%;border-spacing:1" class="borde_tab">
            <tr class="titulos31" style="font-size: 15px">
                <th>N&uacute;mero del radicado</th>
                <th>Asunto</th>
                <th>Destinario</th>
                <th>Direcci&oacute;n</th>
                <th>Ciudad</th>
                <th>Departamento</th>
                <th>Pais</th>
            </tr>
            <?php 	    $timerRespuesta->intertime('Antes de Render');
            $numelem=count($recordGM);
            for($i=0;$i<$numelem;$i++){
                if ($i % 2 == 0)
                    $atr = "listado1";
                else {
                    $atr = "listado2";
                }
                ?>
            <tr>
                <td class="<?php echo $atr?>"><?php echo $recordGM[$i]['numrad']?></td>
                <td class="<?php echo $atr?>"><?php echo $recordGM[$i]['asunto']?></td>
                <td class="<?php echo $atr?>"><?php echo $recordGM[$i]['destinatario']?></td>
                <td class="<?php echo $atr?>"><?php echo $recordGM[$i]['direccion']?></td>
                <td class="<?php echo $atr?>"><?php echo $recordGM[$i]['municipio']?></td>
                <td class="<?php echo $atr?>"><?php echo $recordGM[$i]['departamento']?></td>
                <td class="<?php echo $atr?>"><?php echo $recordGM[$i]['pais']?></td>
            </tr>
                <?php             }
            ?>
        </table>
        </form>
	<?php 	}else{
	//print_r($_FILES);
        if($_FILES['upload']['error']>0){
            echo "Error:".$_FILES['upload']['error'];
        }else{
	    if(!file_exists(RUTA_BODEGA.'masiva/modif'))
	        mkdir(RUTA_BODEGA.'masiva/modif');
            $fecha=date('Y/m/d');
            $hora=date('24H:i:s');
            $newFile=preg_replace('/\//','',$fecha).preg_replace('/:/','',$hora);
            $filename=RUTA_BODEGA.'masiva/modif/'.$newFile.'.csv';
            move_uploaded_file($_FILES['upload']['tmp_name'],$filename);
	    //Revisa primera liena del archivo
	    $chksep=shell_exec("head -n1 $filename");
            if(substr_count($chksep,';')>=3){
		$sep=';';
	    }
	    else{
		$sep=',';
	    }
            //Carga CSV guardado a un arreglo de datos
            $lines=file($filename,FILE_IGNORE_NEW_LINES);
            foreach ($lines as $key => $value){
		//$csv[$key] = str_getcsv(iconv("UTF-8","ISO-8859-1",$value),$sep);
                $csv[$key] = str_getcsv(iconv('iso-8859-1','utf-8',$value),$sep);
            }
	    //print_r($csv);
            $masiva->setCabezaMas($csv[0]);
	    $timerRespuesta->intertime('Antes de consultas de validacion');
            $cabeza=$masiva->validarCabezas();
            //print_r($cabeza);
            if($cabeza['error']!="")
                die($cabeza['error']);
            $masiva->setCabezaMas($cabeza);
            $masiva->setCargaData($csv);
            $resul=$masiva->validarMasiva();
            $statub=$masiva->validarUbicacion();
	    $h=0;
            $numelem=count($csv);
            for($i=1;$i<$numelem;$i++){
	        if($resul[$h]['valid']=='valido'){
		    $observa="Modificado por masiva con listado";
		    $codtx=11;
                    $radSel[0]=$resul[$h]['numrad'];
		    $restant=$tx->insertaHistorico($radSel, $dependencia,$dependencia,$usua_doc,$usua_doc,$codusuario,$codusuario,$id_rol,$id_rol,$observa, $codtx);
		    if(isset($cabeza['exp'])){
                        $j=$cabeza['exp'];
                        $l=$cabeza['colrad'];
                        $observae="Incluido por medio de masiva";
                        $num_expediente=$csv[$i][$j];
                        $numrad=$csv[$i][$l];
                        $tnumrad=  trim($numrad);
                        $num_texpediente=trim($num_expediente);
                        if(strlen($num_texpediente)==17 && substr($num_texpediente,-1)=='E' && is_numeric(substr($num_texpediente,0,-1))){
                            $statexp=$masiva->verifExped($num_texpediente);
                            if(strlen($tnumrad)==14 && is_numeric($tnumrad) && $statexp==1){
                                $masiva->setNumRadi($numrad);
                                $radicados[0]=$numrad;
				echo "$numrad incluido en expediente $num_expediente";
                                $expediente->setNumExp($num_expediente);
                                $expediente->setNumRadicado($numrad);
                                $expediente->incluirExp($codusuario,$usua_doc,$dependencia);
                                $expediente->insertarHistoricoExp($radicados,$dependencia,$codusuario,$observae,53,0);
                            }elseif($statexp==0){
				echo "$num_expediente N&uacute;mero de expediente invalido $num_expediente";
			    }
                        }else{echo "<p>N&uacute;mero de expediente invalido $num_expediente</p>";}
		    }
		}
	        $h++;
            }
            
                
            
	    
            //print_r($resul);
            
	    ?>
        <center>
	<table style="border-collapse: collapse; margin:1 ;  width: 60%; padding: 0 ; border-top: 0; border-spacing: 0">
	    <tr class="titulo1">
		<th>N&uacute;mero de radicado</th>
		<th>Estado radicado</th>
                <th>Actualizando Informaci&oacute;n</th>
	    </tr>
	    <?php              $timerRespuesta->intertime('Antes de Render');
	     for($i=0;$i<count($resul);$i++){?>
	    <tr class="listado1">
		<td><?php echo $resul[$i]['numrad'];?></td>
                <td><?php echo $resul[$i]['valid'];?></td>
                <td><?php if($resul[$i]['valid']=="valido"){ echo $statub[$i]; }else{ echo $resul[$i]['valid']; }?></td>
	    </tr>	
	    <?php 		}
	    ?>
	</table>
        </center>
	    <?php         }
    }
	echo "<div id='timer' name='timer' style='alignment-adjust: auto;text-align: right;font-size: 10'>";
        $timerRespuesta->endtime();
        $timerRespuesta->printToIntervalo();
        echo "</div>\n";
	?>
    </body>
</html>
