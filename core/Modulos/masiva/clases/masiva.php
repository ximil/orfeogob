<?php 
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of masiva
 *
 * @author Diego Enrique Rodriguez Delgado
 */
if (! $ruta_raiz)
	$ruta_raiz = '../../../..';

    include_once "$ruta_raiz/core/Modulos/masiva/modelo/modeloMasiva.php";

class masiva {
    private $GrupoRadi;
    private $DepeNume;
    private $CargaData;
    private $CabezaMas;
    private $NumRadi;
    private $modelo;
    //put your code here
    public function __construct($ruta_raiz) {
        $this->modelo=new modeloMasiva($ruta_raiz);
    }
    
    public function getGrupoRadi() {
        return $this->GrupoRadi;
    }

    public function setGrupoRadi($GrupoRadi) {
        $this->GrupoRadi = $GrupoRadi;
    }

    public function getDepeNume() {
        return $this->DepeNume;
    }

    public function setDepeNume($DepeNume) {
        $this->DepeNume = $DepeNume;
    }

    public function getCargaData(){
	return $this->CargaData;
    }

    public function setCargaData($CargaData){
	$this->CargaData=$CargaData;
    }
    
    public function getCabezaMas() {
        return $this->CabezaMas;
    }

    public function setCabezaMas($CabezaMas) {
        $this->CabezaMas = $CabezaMas;
    }
    
    public function getNumRadi() {
        return $this->NumRadi;
    }

    public function setNumRadi($NumRadi) {
        $this->NumRadi = $NumRadi;
    }
    
    function grupoMasiva(){
        return $this->modelo->grupoMasiva($this->GrupoRadi);
    }
    
    function gruposMasiva(){
        return $this->modelo->gruposMasiva($this->DepeNume);
    }

    function validarMasiva(){
	return $this->modelo->validarMasiva($this->GrupoRadi,$this->CargaData,  $this->CabezaMas);
    }
    
    function validarCabezas(){
        $encabezados=array_map('trim',$this->CabezaMas);
        $cabeza['error']="";
        if(in_array('*RAD_S*',$encabezados)){
            $cabeza['colrad']=  array_search('*RAD_S*',$encabezados);
        }else{
            $cabeza['error'].="<br><font color='red'>Error columna de radicado no se encuentra</font>";
        }
        if(in_array('*ASUNTO*',$encabezados)){
            $cabeza['colasu']=  array_search('*ASUNTO*',$encabezados);
        }else{
            $cabeza['error'].="<br><font color='red'>Error columna del asunto no encontrada</font>";
        }
        if(in_array('*NUM_EXPEDIENTE*',$encabezados)){
            $cabeza['exp']=  array_search('*NUM_EXPEDIENTE*',$encabezados);
        }
        if(in_array('*EMPRESA*',$encabezados) || in_array('*NOM_R*',$encabezados)){
            if(in_array('*EMPRESA*',$encabezados))
                    $cabeza['colempresa']=  array_search($encabezados,'*EMPRESA*');
            if(in_array('*NOM_R*',$encabezados))
                    $cabeza['colnombre']=  array_search('*NOM_R*',$encabezados);
        }else{
            $cabeza['error'].="<br><font color='red'>Error columna de un ciudadano o empresa no encontrados</font>";
        }
        if(in_array('*DIR_R*',$encabezados)){
            $cabeza['coldir']=  array_search('*DIR_R*',$encabezados);
        }else{
            $cabeza['error'].="<br><font color='red'>Error columna de la direcci&oacute;n no encontrada</font>";
        }
        if(in_array('*MPIO_R*',$encabezados)){
            $cabeza['colmnom']=  array_search('*MPIO_R*',$encabezados);
        }else{
            $cabeza['error'].="<br><font color='red'>Error columna del municipio no encontrada</font>";
        }
        if(in_array('*DEPTO_R*',$encabezados)){
            $cabeza['coldnom']=  array_search('*DEPTO_R*',$encabezados);
        }else{
            $cabeza['error'].="<br><font color='red'>Error columna del departamento no encontrada</font>";
        }
        if(in_array('*PAIS_R*',$encabezados)){
            $cabeza['colpais']=  array_search('*PAIS_R*',$encabezados);
        }
        if(in_array('*CUENTAI*',$encabezados)){
            $cabeza['colctai']=  array_search('*CUENTAI*',$encabezados);
        }if(in_array('*DIGNATARIO*',$encabezados)){
            $cabeza['coldig']=  array_search('*DIGNATARIO*',$encabezados);
        }
        if(in_array('*TEL_R*',$encabezados)){
            $cabeza['coltel']=  array_search('*TEL_R*',$encabezados);
        }
        if(in_array('*MAIL_R*',$encabezados)){
            $cabeza['colmail']=  array_search('*MAIL_R*',$encabezados);
        }if(in_array('*DOC_R*',$encabezados)){
            $cabeza['coldocr']=  array_search('*DOC_R*',$encabezados);
        }
        return $cabeza;
    }
    
    function validarUbicacion(){
        return $this->modelo->validarUbicacion($this->CargaData, $this->CabezaMas);
    }
    
    function validarRadicado(){
        return $this->modelo->validarRadicado($this->NumRadi);
    }
    
    function verifExped($num_expediente){
        $depen=substr($num_expediente,4,3);
        $numserie=  substr($num_expediente,7,2);
        $numsubser=  substr($num_expediente,9,2);
        $status=$this->modelo->verifExped($depen,$numserie,$numsubser);
        return $status;
    }
    
}

?>
