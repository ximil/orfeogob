<?php 
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of newPHPClass
 *
 * @author derodriguez
 */
if (!$ruta_raiz)
    $ruta_raiz = '../../..';

include_once "$ruta_raiz/core/clases/ConnectionHandler.php";

class modeloMasiva {
    private $link;
    //put your code here
    public function __construct($ruta_raiz) {
        $db = new ConnectionHandler ( "$ruta_raiz" );
        $db->conn->SetFetchMode ( ADODB_FETCH_NUM );
        $db->conn->SetFetchMode ( ADODB_FETCH_ASSOC );
        $this->link = $db;
        //$this->link->conn->debug=true;
    }
    
    function grupoMasiva($gruporad){
        $ssql="select re.radi_nume_sal as numrad, ra.ra_asun as asunto, sgd_renv_nombre as destinatario, sgd_renv_dir as direccion
            ,sgd_renv_mpio as municipio,sgd_renv_depto as departamento, sgd_renv_pais as pais from radicado ra
            ,sgd_renv_regenvio re where ra.radi_nume_radi=re.radi_nume_sal and re.radi_nume_grupo=$gruporad
	    order by ra.radi_nume_radi";
        $rs=  $this->link->conn->Execute($ssql);
        $i=0;
        while(!$rs->EOF){
            $recordGM[$i]['numrad']=trim($rs->fields['NUMRAD']);
            $recordGM[$i]['asunto']=trim($rs->fields['ASUNTO']);
            $recordGM[$i]['destinatario']=trim($rs->fields['DESTINATARIO']);
            $recordGM[$i]['direccion']=trim($rs->fields['DIRECCION']);
            $recordGM[$i]['municipio']=trim($rs->fields['MUNICIPIO']);
            $recordGM[$i]['departamento']=trim($rs->fields['DEPARTAMENTO']);
            $recordGM[$i]['pais']=trim($rs->fields['PAIS']);
            $rs->MoveNext();
            $i++; 
        }
        return $recordGM;
    }
    function gruposMasiva($depus){
        $ssql="select r.RADI_NUME_GRUPO , count(*) as DOCUMENTOS , min(r.RADI_NUME_SAL) as RAD_INI 
            , MAX(r.RADI_NUME_SAL) as RAD_FIN , TO_CHAR(r.SGD_RENV_FECH, 'YYYY/MM/DD') AS FECHA , r.USUA_DOC 
            , rd.TDOC_CODI from sgd_renv_regenvio r, radicado rd WHERE r.sgd_renv_planilla = '00' and r.sgd_renv_tipo = 1
            and rd.RADI_NUME_RADI= r.RADI_NUME_GRUPO and sgd_depe_genera = '$depus' 
	    group by r.radi_nume_grupo, TO_CHAR(r.SGD_RENV_FECH, 'YYYY/MM/DD')
            , r.usua_doc, rd.TDOC_CODI";
        $rs=  $this->link->conn->Execute($ssql);
        $resul['error']="";
        $i=0;
        if(!$rs->EOF){
            while (!$rs->EOF){
                $resul[$i]['grupo']=$rs->fields['RADI_NUME_GRUPO'];
                $resul[$i]['documentos']=$rs->fields['DOCUMENTOS'];
                $resul[$i]['rad_ini']=$rs->fields['RAD_INI'];
                $resul[$i]['rad_fin']=$rs->fields['RAD_FIN'];
                $tdoc_codi=$rs ->fields['TDOC_CODI'];
                $sqlTmp="select sgd_tpr_descrip as tipdocum from sgd_tpr_tpdcumento where sgd_tpr_codigo=$tdoc_codi";
                $rstmp=  $this->link->conn->Execute($sqlTmp);
                $resul[$i]['tipdocum']=$rstmp->fields['TIPDOCUM'];
                unset($rstmp);
                $i++;
                $rs->MoveNext();
            }
        }else{
            $resul['error']="<font color='red'>No hay radicaci&oacute;n masiva reciente</font>";
        }
        return $resul;
    }

    function validarMasiva($gruporad,$data,$data_head){
	$j=$data_head['colrad'];
        $h=0;
        //print_r($data);
	for($i=1;$i<=count($data);$i++){
	    $radtmp=$data[$i][$j];
	    $ssql="select case when radi_nume_grupo='$gruporad' then 'valido' else 'invalido' end as valid from sgd_renv_regenvio, radicado where
		 radi_nume_sal='$radtmp' and radi_nume_sal=radi_nume_radi";
	    $rstmp=$this->link->conn->Execute($ssql);
	    if(!$rstmp->EOF){
		$resul[$h]['valid']=$rstmp->fields['VALID'];
                $resul[$h]['numrad']=$radtmp;
                if($resul[$h]['valid']=="invalido")
                    $resul[$h]['valid']="<font color='red'>Documento mal radicado o no existe en el grupo</font>";
	    }else{
                $resul[$h]['valid']="<font color='red'>Documento mal radicado o no existe en el grupo</font>";
                $resul[$h]['numrad']=$radtmp;
	    }
	    unset($rstmp);
	    $h++;
	}
        return $resul;
    }
    function validarRadicado($numrad){
        $ssql="select nume_radi_nume from radicado where radi_nume_radi='$numrad'";
        $rs=  $this->link->conn->Execute($ssql);
        if(!$rs->EOF){
            $status=0;
        }else{
            $status=1;
        }
        return $status;
    }
    
  /* function validarExpediente($numExpediente){
        $sql="select sgd_exp_numero from sgd_sexp_secexpedientes where sgd_exp_numero=$numExpediente";
        $rs=$this->link->conn->Execute($sql);
        if(!$rs->EOF){
            $status=0;
        }else{
            $status=1;
        }
        return $status;
    }*/
    
    function validarUbicacion($data,$data_head){
        //Cuenta N&uacute;mero de filas en el arreglo
        $numelem=count($data);
        //Inicializada variable para formar arreglo
        $h=0;
        //Comienza a contar a partir de 1 ya que 0 es los encabezados
        for($i=1;$i<$numelem;$i++){
            //Se evalua si tiene ambos encabezados de empresa y nombre de ciudadano
                $err4=0;
                $err5=0;
            if(isset($data_head['colempresa']) && isset($data_head['colnombre'])){
                $j=$data_head['colempresa'];
                $l=$data_head['colnombre'];
                $tmpemp=$data[$i][$j];
                $tmpnomb=$data[$i][$l];
                //Si ambos estan vacios
                if(trim($tmpemp)=="" && trim($tmpnomb)==""){
                    $err4=1;
                }
                //Si ambos tienen algo
                if(trim($tmpemp)!="" && trim($tmpnomb)!=""){
                    $err5=1;
                }
            }
            //Si no estan los errores de vacios y duplicidad
            if($err4==0 && $err5==0){
                //Se vuelve a evaluar si existe unicamente la empresa
                if(isset($data_head['colempresa'])){
                    $j=$data_head['colempresa'];
                    if(trim($data[$i][$j])!=""){
                        $nombre=$data[$i][$j];
                        $col=0;
                    }
                }
                //Se vuelve a evaluar si existe unicamente el ciudadano
                if(isset($data_head['colnombre'])){
                    $j=$data_head['colnombre'];
                    if(trim($data[$i][$j])!=""){
                        $nombre=$data[$i][$j];
                        $col=1;
                    }
                }
            }
            //Si esta la columna pais la cuan no es obligatoria
            if(isset($data_head['colpais'])){
                $j=$data_head['colpais'];
                $paissql="select id_pais, nombre_pais from sgd_def_paises where nombre_pais = '".$data[$i][$j]."'";
                $rsPais=  $this->link->conn->Execute($paissql);
                if(!$rsPais->EOF){
                    $id_pais=$rsPais->fields['ID_PAIS'];
                    $nomb_pais=$rsPais->fields['NOMBRE_PAIS'];
                    $err1=0;
                }  else {
                    $err1=1;
                }
            } else{
                //Toma por omision COLOMBIA si el usuario no da este campo
               $err1=0;
               $id_pais=170;
               $nomb_pais='COLOMBIA';
            }
            if($err1==0){
                $j=$data_head['coldnom'];
                $m==$data_head['colmnom'];
          /*      $dptosql="select dpto_codi, dpto_nomb from departamento d where id_pais=$id_pais and dpto_nomb like '%".$data[$i][$j]."%' 
                    and exists(select muni_codi from municipio m where m.dpto_codi=d.dpto_codi and and muni_nomb like '%".$data[$i][$m]."%')";*/
                      $dptosql="select dpto_codi, dpto_nomb from departamento d where id_pais=$id_pais and dpto_nomb = '".$data[$i][$j]."' ";
                $rsDpto=  $this->link->conn->Execute($dptosql);
                if(!$rsDpto->EOF){
                    $dpto_codi=$rsDpto->fields['DPTO_CODI'];
                    $dpto_nomb=$rsDpto->fields['DPTO_NOMB'];
                    $err2=0;
                }else
                    $err2=1;
                
                if($err2==0){
                    $j=$data_head['colmnom'];
                    $munsql="select muni_codi, muni_nomb from municipio where dpto_codi=$dpto_codi and muni_nomb = '{$data[$i][$j]}'";
                    $rsMun=  $this->link->conn->Execute($munsql);
                    if(!$rsMun->EOF){
                        $muni_codi=$rsMun->fields['MUNI_CODI'];
                        $muni_nomb=$rsMun->fields['MUNI_NOMB'];
                        $err3=0;
                    }else
                        $err3=1;
                }
            }
            if(isset($data_head['coldig'])){
                $j=$data_head['coldig'];
                $dignatario=",sgd_dir_nombre='".$data[$i][$j]."'";
            }else{
                $dignatario=",sgd_dir_nombre='".substr(trim($nombre),0,150)."'";
            }
            if(isset($data_head['coltel'])){
                $j=$data_head['coltel'];
                $telefonod=",sgd_dir_telefono='".$data[$i][$j]."'";
                $telefonor=",sgd_renv_telefono='".$data[$i][$j]."',sgd_renv_tel='".$data[$i][$j]."'";
            }else{
                $telefonod="";
                $telefonor="";
            }
            if(isset($data_head['colmail'])){
                $j=$data_head['colmail'];
                $maild=",sgd_dir_mail='".$data[$i][$j]."'";
                $mailr=",sgd_renv_mail='".$data[$i][$j]."'";
            }else{
                $maild="";
                $mailr="";
            }
            if(isset($data_head['colctai'])){
                $j=$data_head['colctai'];
                $ctai=",radi_cuentai='".$data[$i][$j]."'";
            }else{
                $ctai="";
            }
            if(isset($data_head['coldocr'])){
                $j=$data_head['coldocr'];
                $document=$data[$i][$j];
            }else{
                $document=="";
            }
	    $j=$data_head['colrad'];
            $numrad=$data[$i][$j];
            $j=$data_head['coldir'];
            $direccion=$data[$i][$j];
            $j=$data_head['colasu'];
            $asunto=$data[$i][$j];
            if($err1==0 && $err2==0 && $err3==0 && $err4==0 && $err5==0){
                $usql1="update sgd_dir_drecciones set sgd_dir_nomremdes='$nombre', id_pais=$id_pais, muni_codi=$muni_codi, dpto_codi=$dpto_codi,
                    sgd_dir_doc='$document', sgd_dir_direccion='$direccion' $dignatario $telefonod $maild where radi_nume_radi='$numrad'";
                $this->link->conn->query($usql1);
                $usql2="update sgd_renv_regenvio set sgd_renv_nombre='$nombre', sgd_renv_pais='$nomb_pais', sgd_renv_depto='$dpto_nomb', sgd_renv_mpio='$muni_nomb',
                     sgd_renv_dir='$direccion' $telefonor $mailr where radi_nume_sal='$numrad'";
                $this->link->conn->query($usql2);
                $usql3="update radicado set radi_nomb='".substr(trim($nombre),0,250)."',ra_asun='".substr(trim($asunto),0,350)."' $ctai where radi_nume_radi='$numrad'";
                $this->link->conn->query($usql3);
                $retor[$h]="Cambio fue exitoso";
            }elseif($err4==0 && $err5==0 && ($err1==1 || $err2==1 || $err3==1)){
		$usql1="update sgd_dir_drecciones set sgd_dir_nomremdes='$nombre',
                    sgd_dir_doc='$document', sgd_dir_direccion='$direccion' $dignatario $telefonod $maild where radi_nume_radi='$numrad'";
                $this->link->conn->query($usql1);
                $usql2="update sgd_renv_regenvio set sgd_renv_nombre='$nombre',
                     sgd_renv_dir='$direccion' $telefonor $mailr where radi_nume_sal='$numrad'";
                $this->link->conn->query($usql2);
                $usql3="update radicado set radi_nomb='".substr(trim($nombre),0,250)."', ra_asun='".substr(trim($asunto),0,350)."' $ctai where radi_nume_radi='$numrad'";
                $this->link->conn->query($usql3);
	    }
                if($err1==1)
                    $retor[$h]="Error pais no encontrado en la base de datos";
                if($err2==1)
                    $retor[$h]="Error departamento no encontrado en la base de datos";
                if($err3==1)
                    $retor[$h]="Error municipio no encontrado en la base de datos";
                if($err4==1)
                    $retor[$h]="Datos de nombre empresa o ciudadanos no hay datos";
                if($err5==1)
                    $retor[$h]="Duplicidad de datos de nombre empresa o ciudadanos no hay datos";
            
            $h++;
        }
        return $retor;
    }

    /*function actualizarDatos($data,$data_head){
	$j=0;
        for($i=1;$i<count($data);$i++){
	    $radtmp=$data[$i][10];
	    $asuntmp=$data[$i][11];
	    $usql="update radicado set ra_asun='$asuntmp' where radi_nume_radi='$radtmp'";
	    $this->link->conn->Execute($usql);
    	}
    }*/
    
    function verifExped($depen,$numserie,$numsubser){
        $ssql="select 1 from sgd_mrd_matrird where depe_codi=$depen and sgd_srd_codigo=$numserie and sgd_sbrd_codigo=$numsubser";
        $rs=  $this->link->conn->Execute($ssql);
        if(!$rs->EOF){
            $status=1;
        }else{
            $status=0;
        }
        return $status;
    }

}

?>
