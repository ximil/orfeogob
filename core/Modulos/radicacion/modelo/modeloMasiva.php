<?php
/**
 * modeloMasiva es la clase encargada de gestionar las operaciones y los datos basicos referentes a un Radicado
 * @author Hardy Deimont Niño  Velasquez
 * @name modeloMasiva
 * @version	1.0
 */
if (!$ruta_raiz)
    $ruta_raiz = '../../../..';

//include_once "$ruta_raiz/include/db/ConnectionHandler.php";
include_once "$ruta_raiz/core/clases/ConnectionHandler.php";

class modeloMasiva {

    //put your code here
    public $link;

    function __construct($ruta_raiz) {

        $db = new ConnectionHandler("$ruta_raiz");
        $db->conn->SetFetchMode(ADODB_FETCH_NUM);
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        if ($_SESSION['test'] == 1)
            $db->conn->debug = true;
        $this->link = $db;
    }

    function consultar($radicado, $depe) {
        $select = "select DISTINCT b.RADI_NUME_RADI,
					b.radi_usu_ante,b.ra_asun, b.SGD_EANU_CODIGO ,usua_depe_ante
    			from radicado b where b.radi_nume_radi is not null
		and b.radi_depe_actu=999
		and b.usua_depe_ante=$depe
		and ((b.SGD_EANU_CODIGO<>2  and b.SGD_EANU_CODIGO<>1) or b.SGD_EANU_CODIGO is null or b.SGD_EANU_CODIGO=3 )
		and RADI_NUME_RADI=$radicado";
        $rs = $this->link->conn->Execute($select);
        $i = 0;
        while (!$rs->EOF) {
            $combi [$i] ["NRADI"] = $rs->fields ['RADI_NUME_RADI'];
            $combi [$i] ["RADI_USU_ANTE"] = $rs->fields ['RADI_USU_ANTE'];
            $combi [$i] ["USUA_DEPE_ANTE"] = $rs->fields ['USUA_DEPE_ANTE'];
            $combi [$i] ["SGD_EANU_CODIGO"] = $rs->fields ['SGD_EANU_CODIGO'];
            $combi [$i] ["RA_ASUN"] = $rs->fields ['RA_ASUN'];

            $i++;
            $rs->MoveNext();
        }
        return $combi;
    }

    function listar($idlista) {
        $sql = "SELECT   ciu.sgd_ciu_nombre as nomb,   ciu.sgd_ciu_cedula as cc,   d.dpto_nomb as nombdepto,
                p.nombre_pais as nombpais, m.muni_nomb as nombmuni,   ciu.sgd_ciu_apell1 as apell,   ciu.sgd_ciu_apell2 as apell2,
                mas.mas_dat_id as id,   mas.mas_dat_idcsv as lista,   mas.mas_dat_tprem as tprem,   ciu.sgd_ciu_codigo as codrem,
                mas.mas_dat_expediente as exp,ciu.sgd_ciu_direccion as dir
                FROM   sgd_ciu_ciudadano ciu,   mas_dat_csvdata mas,   municipio m,   departamento d,   sgd_def_paises p
                WHERE ciu.sgd_ciu_codigo = mas.mas_dat_codrem AND  ciu.dpto_codi = m.dpto_codi
                AND ciu.id_pais = m.id_pais
                AND m.dpto_codi = d.dpto_codi AND   m.id_pais = p.id_pais AND  m.muni_codi = ciu.muni_codi AND
                p.id_pais = d.id_pais AND  p.id_cont = m.id_cont AND  mas.mas_dat_tprem = 0 AND
                mas.mas_dat_idcsv = $idlista order by id";

        $rs = $this->link->query($sql);
        $i = 0;
        while (!$rs->EOF) {
            $data[$i]['id'] = $rs->fields ["ID"];
            $data[$i]['nombre'] = $rs->fields ["NOMB"];
            $data[$i]['apellido'] = $rs->fields ["APELL"];
            $data[$i]['apellido2'] = $rs->fields ["APELL2"];
            $data[$i]['doc'] = $rs->fields ["CC"];
            $data[$i]['dir'] = $rs->fields ["DIR"];
            $data[$i]['depto'] = $rs->fields ["NOMBDEPTO"];
            $data[$i]['muni'] = $rs->fields ["NOMBMUNI"];
            $data[$i]['pais'] = $rs->fields ["NOMBPAIS"];
            $data[$i]['tpr'] = $rs->fields ["TPREM"];
            $data[$i]['crem'] = $rs->fields ["CODREM"];
            $data[$i]['exp'] = $rs->fields ["EXP"];
            $i++;
            $rs->MoveNext();
        }

        $sql = "SELECT   d.dpto_nomb as nombdepto, p.nombre_pais as nombpais, m.muni_nomb as nombmuni,
                         mas.mas_dat_expediente as exp,mas.mas_dat_id as id,   mas.mas_dat_idcsv as lista,
                         mas.mas_dat_tprem as tprem, oem.sgd_oem_oempresa as nomb, oem.sgd_oem_rep_legal as apell,
                         oem.sgd_oem_nit as cc, oem.sgd_oem_sigla as apell2,oem.sgd_oem_direccion as dir
                FROM mas_dat_csvdata mas,municipio m, departamento d, sgd_def_paises p, sgd_oem_oempresas oem
                WHERE
                     m.dpto_codi = d.dpto_codi AND m.id_pais = p.id_pais AND m.muni_codi = oem.muni_codi AND
                     m.dpto_codi = oem.dpto_codi AND p.id_pais = d.id_pais AND  p.id_cont = m.id_cont AND
                     oem.sgd_oem_codigo = mas.mas_dat_codrem AND mas.mas_dat_tprem = 2 AND mas.mas_dat_idcsv = $idlista order by id";

        $rs = $this->link->query($sql);
//
        while (!$rs->EOF) {
            $data[$i]['id'] = $rs->fields ["ID"];
            $data[$i]['nombre'] = $rs->fields ["NOMB"];
            $data[$i]['apellido'] = $rs->fields ["APELL"];
            $data[$i]['apellido2'] = $rs->fields ["APELL2"];
            $data[$i]['doc'] = $rs->fields ["CC"];
            $data[$i]['depto'] = $rs->fields ["NOMBDEPTO"];
            $data[$i]['muni'] = $rs->fields ["NOMBMUNI"];
            $data[$i]['pais'] = $rs->fields ["NOMBPAIS"];
            $data[$i]['tpr'] = $rs->fields ["TPREM"];
            $data[$i]['crem'] = $rs->fields ["CODREM"];
            $data[$i]['exp'] = $rs->fields ["EXP"];
            $data[$i]['dir'] = $rs->fields ["DIR"];
            $i++;
            $rs->MoveNext();
        }


        return $data;
    }

    function insLista($idlista, $tpemp, $doc, $exp) {
        $sql = "SELECT max( mas_dat_id) as ids FROM mas_dat_csvdata";
        $rs = $this->link->query($sql);
        $id = $rs->fields ["IDS"] + 1;
        $sql2 = "insert into mas_dat_csvdata (mas_dat_id, mas_dat_idcsv, mas_dat_tprem, mas_dat_codrem, mas_dat_expediente)
         values ($id,$idlista,$tpemp,$doc,'$exp')";
        $rs = $this->link->query($sql2);
    }

    function delitem($idlista, $id) {
        $sql = "delete from mas_dat_csvdata where mas_dat_id= $id  and mas_dat_idcsv= $idlista";
        $rs = $this->link->query($sql);
    }

    function consultar3($tbusqueda, $nombre_essp, $no_documento) {
        //   echo "$tbusqueda, $nombre_essp, $no_documento";
        if ($tbusqueda == 0) {
            //$array_nombre =  split ( " ", $nombre_essp . "    " );
            $array_nombre = explode(" ", $nombre_essp . "    ");
            $isql = "select * from SGD_CIU_CIUDADANO where ";
            if ($nombre_essp) {
                if ($array_nombre [0]) {
                    $where_split = $this->link->conn->Concat("UPPER(sgd_ciu_nombre)", "UPPER(sgd_ciu_apell1)", "UPPER(sgd_ciu_apell2)") . " LIKE UPPER('%" . $array_nombre [0] . "%') ";
                }
                if ($array_nombre [1]) {
                    $where_split .= " and " . $this->link->conn->Concat("UPPER(sgd_ciu_nombre)", "UPPER(sgd_ciu_apell1)", "UPPER(sgd_ciu_apell2)") . " LIKE UPPER('%" . $array_nombre [1] . "%') ";
                }
                if ($array_nombre [2]) {
                    $where_split .= " and " . $this->link->conn->Concat("UPPER(sgd_ciu_nombre)", "UPPER(sgd_ciu_apell1)", "UPPER(sgd_ciu_apell2)") . " LIKE UPPER('%" . $array_nombre [2] . "%') ";
                }
                if ($array_nombre [3]) {
                    $where_split .= " and " . $this->link->conn->Concat("UPPER(sgd_ciu_nombre)", "UPPER(sgd_ciu_apell1)", "UPPER(sgd_ciu_apell2)") . " LIKE UPPER('%" . $array_nombre [3] . "%') ";
                }
                $isql .= "  $where_split ";
            }
            if ($no_documento) {
                if ($where_split)
                    $isql .= " and ";

                $isql .= " SGD_CIU_CEDULA='$no_documento'   ";
            }
            $isql .= "  and (sgd_ciu_estado=0 or sgd_ciu_estado is null) order by sgd_ciu_nombre,sgd_ciu_apell1,sgd_ciu_apell2 ";
        }
        if ($tbusqueda == 2) {
            $isql = "select SGD_OEM_NIT AS SGD_CIU_CEDULA,SGD_OEM_OEMPRESA as 	SGD_CIU_NOMBRE,SGD_OEM_REP_LEGAL as SGD_CIU_APELL2
	 ,SGD_OEM_CODIGO AS SGD_CIU_CODIGO,SGD_OEM_DIRECCION as SGD_CIU_DIRECCION,SGD_OEM_TELEFONO AS SGD_CIU_TELEFONO,SGD_OEM_SIGLA AS SGD_CIU_APELL1
	 ,MUNI_CODI,DPTO_CODI,ID_PAIS,ID_CONT 	from SGD_OEM_OEMPRESAS where ";
            if ($nombre_essp) {
                $isql .= " UPPER(SGD_OEM_OEMPRESA) LIKE UPPER('%$nombre_essp%')
	 OR UPPER(SGD_OEM_SIGLA) LIKE UPPER('%$nombre_essp%')";
            }

            if ($no_documento) {
                if ($nombre_essp)
                    $isql .= " and ";
                $isql .= "  SGD_OEM_NIT = '$no_documento'   ";
            }
            $isql .= " order by sgd_oem_oempresa";
        }
        if ($tbusqueda == 1) {
            $isql = "select NIT_DE_LA_EMPRESA AS SGD_CIU_CEDULA,NOMBRE_DE_LA_EMPRESA as SGD_CIU_NOMBRE
	,SIGLA_DE_LA_EMPRESA as SGD_CIU_APELL1, " . "IDENTIFICADOR_EMPRESA AS SGD_CIU_CODIGO
	,DIRECCION as SGD_CIU_DIRECCION
	,TELEFONO_1 AS SGD_CIU_TELEFONO
	,NOMBRE_REP_LEGAL as SGD_CIU_APELL2
	,SIGLA_DE_LA_EMPRESA as SGD_CIU_APELL1
	,CODIGO_DEL_DEPARTAMENTO as DPTO_CODI
	,CODIGO_DEL_MUNICIPIO as MUNI_CODI
	,ID_PAIS, ID_CONT
	from BODEGA_EMPRESAS
	WHERE 
	(UPPER(SIGLA_DE_LA_EMPRESA) LIKE UPPER('%$nombre_essp%')
	OR UPPER(NOMBRE_DE_LA_EMPRESA) LIKE UPPER('%$nombre_essp%')) ";
            if ($db->driver == "mssql") {
                $isql = str_replace("UPPER(SIGLA_DE_LA_EMPRESA)", "SIGLA_DE_LA_EMPRESA", $isql);
                $isql = str_replace("UPPER(NOMBRE_DE_LA_EMPRESA)", "NOMBRE_DE_LA_EMPRESA", $isql);
            }
            //Si incluye ESP desactivas
            if (!isset($_POST ['chk_desact']))
                $isql .= " and ACTIVA = 1 ";
            if (strlen(trim($no_documento)) > 0) {
                $isql .= " and NIT_DE_LA_EMPRESA like '%$no_documento%'";
                $isql .= " order by NOMBRE_DE_LA_EMPRESA ";
            }
        }
        if ($tbusqueda == 6) {
            $array_nombre = split(" ", $nombre_essp . "    ");
            //Query que busca funcionario
            $isql = "select usua_doc AS SGD_CIU_CEDULA
	,usua_nomb as SGD_CIU_NOMBRE
	,'' as SGD_CIU_APELL1
	,USUA_DOC AS SGD_CIU_CODIGO
	,dependencia.depe_nomb as SGD_CIU_DIRECCION
	,USUARIO.USUA_EXT  AS SGD_CIU_TELEFONO
	,USUARIO.USUA_LOGIN as SGD_CIU_APELL2
	,'' as SGD_CIU_APELL1
	,dependencia.ID_CONT
	, dependencia.ID_PAIS
	, dependencia.DPTO_CODI as DPTO_CODI
	,dependencia.MUNI_CODI as MUNI_CODI
	,USUARIO.usua_email as SGD_CIU_EMAIL
	from USUARIO,dependencia , sgd_urd_usuaroldep urd
    where USUA_ESTA='1' AND  urd.depe_codi= dependencia.depe_codi  and
 urd.usua_codi=USUARIO.usua_codi
    ";
            if ($nombre_essp) {
                if ($array_nombre [0]) {
                    $where_split = "  UPPER(USUA_NOMB) LIKE UPPER('%" . $array_nombre [0] . "%') ";
                }
                if ($array_nombre [1]) {
                    $where_split .= " AND UPPER(USUA_NOMB) LIKE UPPER('%" . $array_nombre [1] . "%') ";
                }
                if ($array_nombre [2]) {
                    $where_split .= " AND UPPER(USUA_NOMB) LIKE UPPER('%" . $array_nombre [2] . "%') ";
                }
                if ($array_nombre [3]) {
                    $where_split .= " AND UPPER(USUA_NOMB) LIKE UPPER('%" . $array_nombre [3] . "%') ";
                }
                $isql .= " and $where_split ";
            }
            if ($no_documento) {
                if ($nombre_eesp)
                    $isql .= " and ";
                else
                    $isql .= " and ";
                $isql .= " usua_doc='$no_documento' ";
            }
            $isql .= " order by usua_nomb ";
        }

        // echo $isql;
        $i = 0;

        $rs = $this->link->query($isql);
        while (!$rs->EOF) {
            $data[$i]['codigo'] = trim($rs->fields ["SGD_CIU_CODIGO"]);
            $data[$i]['email'] = trim(str_replace('"', ' ', $rs->fields ["SGD_CIU_EMAIL"]));
            $data[$i]['telefono'] = trim(str_replace('"', ' ', $rs->fields ["SGD_CIU_TELEFONO"]));
            $data[$i]['direccion'] = trim(str_replace('"', ' ', $rs->fields ["SGD_CIU_DIRECCION"]));
            $data[$i]['apell2'] = trim(str_replace('"', ' ', $rs->fields ["SGD_CIU_APELL2"]));
            $data[$i]['apell1'] = trim(str_replace('"', ' ', $rs->fields ["SGD_CIU_APELL1"]));
            $data[$i]['nombre'] = trim(str_replace('"', ' ', $rs->fields ["SGD_CIU_NOMBRE"]));
            $data[$i]['codigo_cont'] = $rs->fields ["ID_CONT"];
            $data[$i]['codigo_pais'] = $rs->fields ["ID_PAIS"];
            $data[$i]['codigo_dpto'] = $rs->fields ["ID_PAIS"] . "-" . $rs->fields ["DPTO_CODI"];
            $data[$i]['codigo_muni'] = $rs->fields ["ID_PAIS"] . "-" . $rs->fields ["DPTO_CODI"] . "-" . $rs->fields ["MUNI_CODI"];
            $data[$i]['coddpto'] = $rs->fields ["DPTO_CODI"];
            $data[$i]['codmuni'] = $rs->fields ["MUNI_CODI"];
            $data[$i]['cc_documento'] = trim($rs->fields ["SGD_CIU_CEDULA"]);
            $i++;
            $rs->MoveNext();
        }
        return $data;
    }

    function ciudad() {
        $isql = "select MUNI_NOMB ,muni_codi,dpto_codi from municipio ";
        $rs = $this->link->query($isql);
        if (!$rs->EOF) {
            $munic[$rs->fields ["MUNI_CODI"]][$rs->fields ["MUNI_NOMB"]] = $rs->fields ["MUNI_NOMB"];
        }
        return $munic;
    }

}

?>
