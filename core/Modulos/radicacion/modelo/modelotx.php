<?php 
/** 
 * modeloRadicado es la clase encargada de gestionar las operaciones y los datos basicos referentes a un Radicado
 * @author Hardy Deimont Niño  Velasquez
 * @name modeloRadicado
 * @version	1.0
 */
if (! $ruta_raiz)
	$ruta_raiz = '../../../..';

	//include_once "$ruta_raiz/include/db/ConnectionHandler.php";
include_once "$ruta_raiz/core/clases/ConnectionHandler.php";
class modeloTx {
	//put your code here
	public $link;
	
	function __construct($ruta_raiz) {
		
		$db = new ConnectionHandler ( "$ruta_raiz" );
		$db->conn->SetFetchMode ( ADODB_FETCH_NUM );
		$db->conn->SetFetchMode ( ADODB_FETCH_ASSOC );
		if ($_SESSION['test'] == 1)
                    $db->conn->debug = true;
		$this->link=$db;
	}
	
	function consultar($radicado, $depe) {
		$select = "select DISTINCT b.RADI_NUME_RADI, 
					b.radi_usu_ante,b.ra_asun, b.SGD_EANU_CODIGO ,usua_depe_ante
    			from radicado b where b.radi_nume_radi is not null 
		and b.radi_depe_actu=999 
		and b.usua_depe_ante=$depe 
		and ((b.SGD_EANU_CODIGO<>2  and b.SGD_EANU_CODIGO<>1) or b.SGD_EANU_CODIGO is null or b.SGD_EANU_CODIGO=3 ) 
		and RADI_NUME_RADI=$radicado"; 
		$rs = $this->link->conn->Execute ( $select );
		$i = 0;

		if($rs){
			$combi=array();
			while ( ! $rs->EOF ) {
				$combi [$i] ["NRADI"] = $rs->fields ['RADI_NUME_RADI'];
				$combi [$i] ["RADI_USU_ANTE"] = $rs->fields ['RADI_USU_ANTE'];
				$combi [$i] ["USUA_DEPE_ANTE"] = $rs->fields ['USUA_DEPE_ANTE'];
				$combi [$i] ["SGD_EANU_CODIGO"] = $rs->fields ['SGD_EANU_CODIGO'];
				$combi [$i] ["RA_ASUN"] = $rs->fields ['RA_ASUN'];
				$i ++;
				$rs->MoveNext ();
			}
		}
		else{
		   $combi=false;
		}
		return $combi;
	}
	
function consultarSolAnu($radi) {
		$select = "select DISTINCT b.RADI_NUME_RADI, u.usua_nomb,b.ra_asun, b.SGD_EANU_CODIGO ,b.radi_depe_actu 
			from radicado b,usuario u 
			where RADI_NUME_RADI in ($radi) and sgd_eanu_codigo=1 and b.radi_usua_actu=u.usua_codi";
		$rs = $this->link->conn->Execute ( $select );
		$i = 0;
		while ( ! $rs->EOF ) {
			$combi [$i] ["NRADI"] = $rs->fields ['RADI_NUME_RADI'];
			$combi [$i] ["USUA_NOMB"] = $rs->fields ['USUA_NOMB'];
			$combi [$i] ["RADI_DEPE_ACTU"] = $rs->fields ['RADI_DEPE_ACTU'];
			$combi [$i] ["SGD_EANU_CODIGO"] = $rs->fields ['SGD_EANU_CODIGO'];
			$combi [$i] ["RA_ASUN"] = $rs->fields ['RA_ASUN'];
			
			$i ++;
			$rs->MoveNext ();
		}
		return $combi;
	}
	function consultarMotivos($aplica) {
		$select = "select sgd_mot_id as codigo, sgd_mot_descrp as descrp from sgd_mot_motivos where sgd_mot_tpaplica  =$aplica";
		$rs = $this->link->conn->Execute ( $select );
		$i = 0;
		while ( ! $rs->EOF ) {
			$combi [$i] ["CODIGO"] = $rs->fields ['CODIGO'];
			$combi [$i] ["DESCRP"] = $rs->fields ['DESCRP'];
			
			$i ++;
			$rs->MoveNext ();
		}
		return $combi;
	}
		/**
	 *
	 * @nomb    	nombre de motivo
	 * @aplica 		tipo de motivo
	 * @return void 
	 * 
	 */
	function crearMotivos($nomb,$aplica){
	 	$select = "insert into  sgd_mot_motivos ( sgd_mot_descrp, sgd_mot_tpaplica)values ('$nomb',$aplica)";
		$rs = $this->link->conn->Execute ( $select );
		
	}
		/**
	 *
	 * @nomb    	nombre de motivo
	 * @aplica 		tipo de motivo
	 * @codigo   	codigo de motivo
	 * @return void 
	 * 
	 */
function modMotivos($nomb,$codigo){
		$select = "update sgd_mot_motivos set sgd_mot_descrp='$nomb' where sgd_mot_id=$codigo";
		$rs = $this->link->conn->Execute ( $select );
		
	}
	/**
	 *
	 * @nomb    	nombre de motivo
	 * @aplica 		tipo de motivo
	 * @codigo   	codigo de motivo
	 * @return void 
	 * 
	 */
function eliMotivos($codigo){
	 	$select="delete from sgd_mot_motivos where sgd_mot_id=$codigo";
		$rs = $this->link->conn->Execute ( $select );
	}
	
	/**
	 * 
	 *
	 * @radicados      Arreglo de radicados
	 * @depeOrigen int Dependencia que realiza la transaccion
	 * @depeDest int   Dependencia destino
	 * @usDocOrigen    Documento del usuario que realiza la transacci�n
	 * @usDocDest      Documento del usuario destino
	 * @tipoTx         Tipo de Transacci�n
	 * @return void 
	 * 
	 */
	function insertarHistorico($radicados, $depeOrigen,$depeDest, $usDocOrigen, $usDocDest,$usCodOrigen, $usCodDest,$rol_orign, $rol_dest, $observacion, $tipoTx) {
		//Arreglo que almacena los nombres de columna
		#==========================
		//$fechahist = date('Y-m-d H:i:s');
		foreach ( $radicados as $noRadicado ) {
		 	$sqlinser="INSERT INTO HIST_EVENTOS ( DEPE_CODI, HIST_FECH, USUA_CODI, RADI_NUME_RADI, HIST_OBSE, USUA_CODI_DEST, USUA_DOC,HIST_DOC_DEST, SGD_TTR_CODIGO, DEPE_CODI_DEST,id_rol_dest,id_rol ) 
			VALUES ( $depeOrigen, current_timestamp, $usCodOrigen, $noRadicado, '$observacion',$usCodDest , '$usDocOrigen',$usDocDest, $tipoTx, $depeDest,$rol_dest,$rol_orign )";
			//echo $insertSQL;
			$this->link->query (  $sqlinser ); # Inserta el registro en la base de datos
		}
		$this->link->conn->Close ();
		return ($radicados);
	}
	
	function reasignar($radicados, $loginOrigen, $depDestino, $depOrigen, $codUsDestino, $codUsOrigen, $tomarNivel, $observa, $codTx, $carp_codi, $id_rol, $usuaDocOrigen) {
		$whereNivel = "";
		
		$this->link->conn->SetFetchMode ( ADODB_FETCH_ASSOC );
	 $sql = "SELECT   usuario.usua_login, usuario.usua_email ,sgd_mod_modules.sgd_mod_modulo,  usuario.usua_doc,  usuario.usua_nomb, 
  sgd_drm_dep_mod_rol.sgd_drm_valor,  sgd_urd_usuaroldep.depe_codi,  sgd_urd_usuaroldep.rol_codi 
  FROM   usuario,   sgd_urd_usuaroldep,   sgd_mod_modules,  sgd_drm_dep_mod_rol WHERE 
  usuario.usua_codi = sgd_urd_usuaroldep.usua_codi AND
  sgd_mod_modules.sgd_mod_id = sgd_drm_dep_mod_rol.sgd_drm_modecodi AND
  sgd_drm_dep_mod_rol.sgd_drm_rolcodi = sgd_urd_usuaroldep.rol_codi AND
  sgd_drm_dep_mod_rol.sgd_drm_depecodi = sgd_urd_usuaroldep.depe_codi AND
  sgd_mod_modules.sgd_mod_estado = 1 AND 
  usuario.usua_codi = $codUsDestino AND 
  sgd_mod_modules.sgd_mod_modulo = 'codi_nivel'";
		# Busca el usuairo Origen para luego traer sus datos.
		//$this->db->conn->debug=true;
		

		$rs = $this->link->query ( $sql );
		//$usNivel = $rs->fields["CODI_NIVEL"];
		//$nombreUsuario = $rs->fields["USUA_NOMB"];
		$usNivel = $rs->fields ['SGD_DRM_VALOR'];
		$nombreUsuario = $rs->fields ['USUA_NOMB'];
		$docUsuaDest = $rs->fields ['USUA_DOC'];
		$depeUsuaDest = $rs->fields ['DEPE_CODI'];
	        $loginDest=$rs->fields['USUA_LOGIN'];
		$rollUsuaDest = $rs->fields ['ROL_CODI'];
		$email=$rs->fields['USUA_EMAIL'];
		if ($tomarNivel == "si") {
			$whereNivel = ",CODI_NIVEL=$usNivel";
		}
		
		$radicadosIn = join ( ",", $radicados );
		$proccarp = "Reasignar";
		$carp_per = 0;
		$isql = "update radicado
				set
				  RADI_USU_ANTE='$loginOrigen'
                                  ,USUA_DOC_ANTE=$usuaDocOrigen
                                  ,USUA_DEPE_ANTE='$depOrigen'
                                  ,USUA_ROL_ANTE='$id_rol'
				  ,RADI_DEPE_ACTU=$depDestino
				  ,RADI_USUA_ACTU=$codUsDestino
                                   ,RADI_USUA_DOC_ACTU=$docUsuaDest
                                   ,ID_ROL=$rollUsuaDest
				  ,CARP_CODI=$carp_codi
				  ,CARP_PER=$carp_per
				  ,RADI_LEIDO=0
				  , radi_fech_agend=null
				  ,radi_agend=null
				  $whereNivel
			 where radi_depe_actu=$depOrigen
			 	   AND radi_usua_actu=$codUsOrigen
				   AND RADI_NUME_RADI in($radicadosIn)";
		//$this->conn->Execute($isql);
		

		// MODIFICADO PARA GENERAR ALERTAS
		// JUNIO DE 2009
		/*foreach ( $radicados as $rad ) {
			$this->registrarNovedad ( 'NOV_REASIG', $docUsuaDest, $rad );
		}*/
		//////////////////////////////////
		$this->link->conn->Execute ( $isql ); # Ejecuta la busqueda
		//$this->insertarHistorico($radicados, $depOrigen, $codUsOrigen, $id_rol, $depDestino, $codUsDestino, $rollUsuaDest, $observa, $codTx );
		$this->insertarHistorico($radicados, $depOrigen, $codUsOrigen, $usuaDocOrigen,$id_rol,$depDestino, $docUsuaDest,  $codUsDestino, $rollUsuaDest, $observa, $codTx);
		$this->alertaEmail($radicados,$email,$loginOrigen,$loginDest);
		return $nombreUsuario;
	}
	
	
	/**
	 * 
	 * Funcion de desanular radicado.
	 * @param unknown_type $radicados
	 */
	
function desanular($radicados,$observa) {
	 $isql = "update radicado  set sgd_eanu_codigo=3 where RADI_NUME_RADI in ($radicados)";
		$this->link->conn->Execute ( $isql ); # Ejecuta la busqueda
		$isql2 = "delete from sgd_anu_anulados  where RADI_NUME_RADI in ($radicados)";
		$this->link->conn->Execute ( $isql2 ); # Ejecuta la busqueda
		$depOrigen=$_SESSION['dependencia']; 
		$codUsOrigen=$_SESSION['codusuario']; 
		$usuaDocOrigen=$_SESSION['usua_doc'];
		$id_rol=$_SESSION['id_rol'];
		$depDestino=$_SESSION['dependencia']; 
		$docUsuaDest=$_SESSION['usua_doc'];  
		$codUsDestino=$_SESSION['codusuario'];
		$rollUsuaDest=$_SESSION['id_rol'];
		$observa="Desanular $observa ";
		$codTx=27;
                /*
                 insertarHistorico(
                 * $radicados, 
                 * $depeOrigen, 
                 * $usCodOrigen, 
                 * $usDocOrigen,
                 * $rol_orign,
                 * $depeDest, 
                 * $usDocDest,  
                 * $usCodDest, 
                 * $rol_dest, 
                 * $observacion, 
                 * $tipoTx
                 */
		//$this->insertarHistorico(explode(',',$radicados), $depOrigen, $codUsOrigen, $usuaDocOrigen,$id_rol,$depDestino, $docUsuaDest,  $codUsDestino, $rollUsuaDest, $observa, $codTx);
		$this->insertarHistorico(explode(',',$radicados), $depOrigen,$depDestino, $usuaDocOrigen,$docUsuaDest,$codUsOrigen,$codUsDestino,$id_rol,$rollUsuaDest, $observa, $codTx);
		
		return $nombreUsuario;
	}
	
	function alertaEmail($radicados,$email,$loginOrigen,$loginDestino){
	    require_once "$ruta_raiz/core/vista/correo.php";
            $subject="Documento(s) asignado(s) a su cuenta de ORFEO -- $loginDestino";
            $mensaje = "Cordial Saludo: <br>\nUsted ha recibido en su cuenta de Orfeo lo siguiente:<br>\n<table border=1><tr><th>No.</th><th>Radicado</th><th>Asunto</th><th>Usuario anterior</th><th>Estado</th></tr>\n";
	    $l=1;
	    for($i=0;$i<count($radicados);$i++){
		$rad=$radicados[$i];
		$ssql="select tdoc_codi,ra_asun from radicado, usuario where radi_nume_radi=$rad";
		$rs=$this->link->conn->Execute($ssql);
		$tdoc=$rs->fields['TDOC_CODI'];
		$asunto=$rs->fields['RA_ASUN'];
		if($tdoc==0){
		    $estado="Sin tipificar";
		}
		else{
		    $estado="Tipificado";
		}
		$mensaje.= "<tr><td>$l</td><td>$rad</td><td>".substr($asunto,0,50)."</td><td>$loginOrigen</td><td>$estado</td></tr>\n";
		$l++;
	    }
	    $mensaje.="</table>\n";
	    mailOrfeo('orfeo@imprenta.gov.co', $email, $subject, $mensaje);
	}

	//INFORMADOS PARA NUEVA RADICACION
	//SEPTIEMBRE 2014

	function informar($radicados, $loginOrigen, $loginDest , $depOrigen, $depDest, $codUsOrigen, $codUsDest,$docUsOrigen,$docUsDest,$observa, $id_rol,$rolDest){
	    $fechainfo = date('Y-m-d H:i:s.u');
	    $observa="A: $loginDest $observa";
	    foreach($radicados as $rad){
		/*$record["RADI_NUME_RADI"]=$rad;
		$record["DEPE_CODI"]=$depDest;
		$record["USUA_CODI"]=$codUsOrigen;
		$record["INFO_DESC"]="'$observa'";
		$record["USUA_DOC"]="'$docUsDest'";
		$record["INFO_CODI"]="NULL";
		$record["ID_ROL"]=$rolDest;
		$record["INFO_FECH"]="'$fechainfo'";*/
		$sql="insert into informados(radi_nume_radi,depe_codi,usua_codi,info_desc,usua_doc,id_rol,info_fech) 
			values($rad,$depDest,$codUsOrigen,'$observa','$docUsDest',$rolDest,'$fechainfo')";
		//$resul=$this->link->conn->Replace("INFORMADOS",$record,array('RADI_NUME_RADI', 'INFO_CODI', 'USUA_DOC'), false);
		$this->link->conn->query($sql);
		//$this->registrarNovedad('NOV_INFOR',$docUsDest,$rad);
	    }
	    $tx=8;
	    $this->insertarHistorico($radicados,$depOrigen,$depDest,$docUsOrigen,$docUsDest,$codUsOrigen,$codUsDest,$id_rol,$rolDest,$observa,$tx);
        }

	function verInformados($numrad){
	    $sql="select info_desc,depe_codi,info_fech from informados where 
		radi_nume_radi=$numrad order by info_fech desc";
	    $rs=$this->link->conn->Execute($sql);
	    print_r($rs);
	    $retorno['error']="";
	    $i=0;
	    if(!$rs->EOF){
		while(!$rs->EOF){
		    //$retorno[$i]['depe_nomb']=$rs->fields['DEPE_NOMB'];
		    $retorno[$i]['depe_codi']=$rs->fields['DEPE_CODI'];
		    $retorno[$i]['data1']=$rs->fields['INFO_DESC'];
		    $retorno[$i]['data2']=$rs->fields['INFO_FECH'];
		    /*$retorno[$i]['data3']=date( "d-m-Y", $this->link->conn->UnixTimeStamp( $rs->fields['INFO_FECH'] ) );
		    $retorno[$i]['data4']=$this->link->conn->UnixTimeStamp( $rs->fields['INFO_DESC'] );*/
		    $i++;
		    $rs->MoveNext();
		}
	    }
	    else{
		$retorno['error']="No se encontro datos de informados";
	    }
	    return $retorno;
	}

	function consultarTRD($numrad){
	    $sql="select sr.sgd_srd_descrip,sbr.sgd_sbrd_descrip, tpr.sgd_tpr_descrip from sgd_rdf_retdocf rt, sgd_mrd_matrird mrd,sgd_srd_seriesrd sr,
		sgd_sbrd_subserierd sbr, sgd_tpr_tpdcumento tpr where rt.radi_nume_radi=$numrad and rt.sgd_mrd_codigo=mrd.sgd_mrd_codigo 
		and mrd.sgd_srd_codigo=sr.SGD_SRD_CODIGO and sr.SGD_SRD_CODIGO = sbr.SGD_SRD_CODIGO and sbr.sgd_sbrd_codigo=mrd.SGD_SBRD_CODIGO
		and tpr.SGD_TPR_CODIGO = mrd.SGD_TPR_CODIGO";
	    $rs=$this->link->conn->Execute($sql);
	    $serie=$rs->fields['SGD_SRD_DESCRIP'];
	    $subserie=$rs->fields['SGD_SBRD_DESCRIP'];
	    $tipo_doc=$rs->fields['SGD_TPR_DESCRIP'];
	    return array($serie,$subserie,$tipo_doc);
	}

	function consultarSeries($dependencia){
	    $sql="select distinct(sr.sgd_srd_descrip), sr.sgd_srd_codigo from sgd_srd_seriesrd sr, sgd_mrd_matrird mrd 
		where sr.sgd_srd_codigo=mrd.sgd_srd_codigo and mrd.depe_codi=$dependencia";
	    $rs=$this->link->conn->Execute($sql);
	    $retorno['error']="";
	    $i=0;
	    if(!$rs->EOF){
		while(!$rs->EOF){
		    $retorno[$i]['serie']=$rs->fields['SGD_SRD_DESCRIP'];
		    $retorno[$i]['codigo']=$rs->fields['SGD_SRD_CODIGO'];
		    $i++;
		    $rs->MoveNext();
		}
	    }
	    else{
		$retorno['error']="No encontraron datos";
	    }
	    return $retorno;
	}

	function consultarSubSeries($dependencia,$serie){
            $sql="select distinct(sbr.sgd_sbrd_descrip), sbr.sgd_sbrd_codigo from sgd_sbrd_subserierd sbr, sgd_mrd_matrird mrd
                where sbr.sgd_srd_codigo=mrd.sgd_srd_codigo and mrd.sgd_sbrd_codigo=sbr.sgd_sbrd_codigo and mrd.depe_codi=$dependencia and
		mrd.sgd_srd_codigo=$serie";
            $rs=$this->link->conn->Execute($sql);
            $retorno['error']="";
            $i=0;
            if(!$rs->EOF){
                while(!$rs->EOF){
                    $retorno[$i]['subserie']=$rs->fields['SGD_SBRD_DESCRIP'];
                    $retorno[$i]['codigo']=$rs->fields['SGD_SBRD_CODIGO'];
                    $i++;
                    $rs->MoveNext();
                }
            }
            else{
                $retorno['error']="No encontraron datos";
            }
            return $retorno;
        }

	function consultarTDoc($dependencia,$serie,$subserie,$trad){
            $sql="select distinct(tpr.sgd_tpr_descrip), tpr.sgd_tpr_codigo from sgd_sbrd_subserierd tpr, sgd_mrd_matrird mrd, sgd_tpr_tpdcumento tpr
                where mrd.sgd_tpr_codigo=tpr.sgd_tpr_codigo and mrd.depe_codi=$dependencia and tpr.sgd_tpr_tp$trad=1 and
                mrd.sgd_srd_codigo=$serie and mrd.sgd_sbrd_codigo=$subserie";
            $rs=$this->link->conn->Execute($sql);
            $retorno['error']="";
            $i=0;
            if(!$rs->EOF){
                while(!$rs->EOF){
                    $retorno[$i]['tipodoc']=$rs->fields['SGD_TPR_DESCRIP'];
                    $retorno[$i]['codigo']=$rs->fields['SGD_TPR_CODIGO'];
                    $i++;
                    $rs->MoveNext();
                }
            }
            else{
                $retorno['error']="No encontraron datos";
            }
            return $retorno;
        }

	function aplicarTRD($numrad,$codus,$docus,$dependencia,$serie,$subserie,$tdoc){
	    $isql="insert into sgd_rdf_retdocf select mrd.sgd_mrd_codigo,
		r.radi_nume_radi,mrd.depe_codi,$codus,'$docus',current_timestamp
		from radicado r, sgd_mrd_matrird mrd where r.radi_nume_radi=$numrad and mrd.depe_codi=$dependencia and mrd.sgd_srd_codigo=$serie and 
		mrd.sgd_sbrd_codigo=$subserie and mrd.sgd_tpr_codigo=$tdoc";
	    $this->link->conn->Execute($isql);
	    $usql="update radicado set tdoc_codi=$tdoc where radi_nume_radi=$numrad";
	    $this->link->conn->Execute($usql);
	}

	function tipoDocumental(){
	    $sql="select sgd_tpr_codigo,sgd_tpr_descrip from sgd_tpr_tpdcumento where sgd_tpr_codigo<>0 order by sgd_tpr_descrip";
	    $rs=$this->link->conn->Execute($sql);
	    $i=0;
	    while(!$rs->EOF){
		$retorno[$i]['id']=$rs->fields['SGD_TPR_CODIGO'];
		$retorno[$i]['tpr']=$rs->fields['SGD_TPR_DESCRIP'];
		$i++;
		$rs->MoveNext();
	    }
	    return $retorno;
	}
	
	// MODIFICADO PARA GENERAR ALERTAS
	// JUNIO  DE 2011 
	function registrarNovedad($tipo, $docUsuarioDest, $numRad, $ruta_raiz = "") {
		// busco la información de radicados informados pendientes de alerta
		// Busco info del campo NOV_INFOR de la tabla SGD_NOVEDAD_USUARIO	
		//include_once ("$ruta_raiz/class_control/Param_admin.php");
		$param = Param_admin::getObject ( $this->link, '%', 'ALERT_FUNCTION' );
		$this->link->conn->SetFetchMode ( ADODB_FETCH_ASSOC );
		if ($param->PARAM_VALOR == "1") {
			
			$rads = $this->getRadicados ( $tipo, $docUsuarioDest );
			
			if ($rads != "") {
				$rads .= ",";
			}
			$rads .= $numRad;
			
			$con = $this->link->driver;
			
			switch ($con) {
				case 'oci8' :
					$xarray ['USUA_DOC'] = $docUsuarioDest;
					$xarray ["$tipo"] = $rads;
					
					$tipo1 = $tipo;
					$valor = $xarray ["$tipo"];
					
					$qs = "Select count(*) as contador from SGD_NOVEDAD_USUARIO where USUA_DOC=$docUsuarioDest";
					$rs = $this->db->conn->query ( $qs );
					
					if ($rs->fields ['CONTADOR'] == 0) {
						$qu = "INSERT INTO SGD_NOVEDAD_USUARIO (USUA_DOC,$tipo1) values ($docUsuarioDest,$valor)";
						$this->link->conn->query ( $qu );
					
					} else {
						$this->link->conn->query ( "UPDATE SGD_NOVEDAD_USUARIO SET $tipo1 = $valor where USUA_DOC'$docUsuarioDest'" );
					}
					
					break;
				
				case 'postgres' :
					
					$xarray ['USUA_DOC'] .= '"';
					$xarray ['USUA_DOC'] .= $docUsuarioDest;
					$xarray ['USUA_DOC'] .= '"';
					
					$tipo1 = '"';
					$tipo1 .= $tipo;
					$tipo1 .= '"';
					
					$xarray ["$tipo"] .= "'";
					$xarray ["$tipo"] .= $rads;
					$xarray ["$tipo"] .= "'";
					
					$valor = $xarray ["$tipo"];
					
					$campo = '"USUA_DOC"';
					$qs = "Select count(*) as contador from SGD_NOVEDAD_USUARIO where $campo='$docUsuarioDest'";
					$rs = $this->link->conn->query ( $qs );
					
					if ($rs->fields ['CONTADOR'] == 0) {
						$qu = "INSERT INTO SGD_NOVEDAD_USUARIO ($campo,$tipo1) values ('$docUsuarioDest',$valor)";
						$this->link->conn->query ( $qu );
					
					} else {
						$this->link->conn->query ( "UPDATE SGD_NOVEDAD_USUARIO SET $tipo1 = $valor where $campo='$docUsuarioDest'" );
					}
					
					break;
			}
		
		}
	}

}

class Param_admin {
	public $con;
	public $PARAM_CODIGO;
	public $PARAM_NOMBRE;
	public $PARAM_VALOR;
	public $PARAM_DESC;
	
	public function Param_admin($db){
		$this->con = & $db;
	}
	
	public static function getObject($con, $PARAM_CODIGO = '%', $PARAM_NOMBRE = '%'){
		$query = "SELECT * FROM SGD_PARAM_ADMIN 
			WHERE PARAM_CODIGO LIKE '$PARAM_CODIGO' AND PARAM_NOMBRE LIKE '$PARAM_NOMBRE' ";
		$rs = $con->query ( $query );
		
		if (count ( $rs ) > 0) {
			while ( ! $rs->EOF ) {
				$param = new Param_admin ( $con );
				$param->PARAM_CODIGO = $rs->fields ['PARAM_CODIGO'];
				$param->PARAM_NOMBRE = $rs->fields ['PARAM_NOMBRE'];
				$param->PARAM_VALOR = $rs->fields ['PARAM_VALOR'];
				$param->PARAM_DESC = $rs->fields ['PARAM_DESC'];
				return $param;
			}	
		} else {
			return "";
		}
	}
	
	public function getPARAM_CODIGO() {
		return $this->PARAM_CODIGO;
	}
	
	public function setPARAM_CODIGO($PARAM_CODIGO) {
		$this->PARAM_CODIGO = $PARAM_CODIGO;
	}
	
	
	public function getPARAM_DESC() {
		return $this->PARAM_DESC;
	}
	
	public function setPARAM_DESC($PARAM_DESC) {
		$this->PARAM_DESC = $PARAM_DESC;
	}
	
	public function getPARAM_NOMBRE() {
		return $this->PARAM_NOMBRE;
	}
	
	public function setPARAM_NOMBRE($PARAM_NOMBRE) {
		$this->PARAM_NOMBRE = $PARAM_NOMBRE;
	}
		
 	public function setPARAM_VALOR($PARAM_VALOR) {
		$this->PARAM_VALOR = $PARAM_VALOR;
	}
	public function getPARAM_VALOR() {
		return $this->PARAM_VALOR;
	}
}

?>
