<?php //echo "ESTOY EN  MODELO PRESTAMOS"."<BR>";


if (! $ruta_raiz)
	$ruta_raiz = '../../../../';
include_once "$ruta_raiz/core/clases/ConnectionHandler.php";


class modeloPrestamos {
	//TODO - Insert your code here


	function __construct($ruta_raiz) {
		$db = new ConnectionHandler ( "$ruta_raiz" );
		$db->conn->SetFetchMode ( ADODB_FETCH_NUM );
		$db->conn->SetFetchMode ( ADODB_FETCH_ASSOC );
		//$db->conn->debug=true;
		$this->link = $db;
	}

	function buscar($exp, $rad, $depe, $usu,$tipo) {
		//echo "$exp,$rad,$depe,,$tipo";
		//print_r("<BR>"."modelo Prestamos "."-".$exp."-".$rad."-".$depe."-".$usu."<br>");
		$expSql ='';
		$radSql='';
		$depeSql='';
		if ($exp != '') {

			if ( strlen ( $exp ) == 18) {
				$expSql = "and e.sgd_exp_numero = '$exp'";
			} else
				$expSql = "and e.sgd_exp_numero like '$exp%'";
		}
		if ($rad != '') {
			$radSql = "and p.radi_nume_radi = $rad ";
		}
		if ($depe > 0) {
			$depeSql = "and p.depe_codi = $depe";
		}
		if($tipo==0){
		$ini=" param_nomb = 'PRESTAMO_REQUERIMIENTO' ";
		  $estado='and p.pres_estado=1';
		}
		  if($tipo==1){
		  $ini=" param_nomb = 'PRESTAMO_ESTADO' ";
		  $estado='and p.pres_estado in (2,5)';
		  }
		  if($tipo==2){
		  	$ini=" param_nomb = 'PRESTAMO_ESTADO' ";
		  	$estado='and p.pres_estado= 3';
		  }
		  if($tipo==3){
		  	$ini=" param_nomb = 'PRESTAMO_ESTADO' ";
		  	$estado='and p.pres_estado=4';
		  }

		//if ($usu != 'X') {
		//	$usuSql = "and p.usua_login_actu = '$usu' ";
		//}


		  $sql = "select p.pres_id,p.dev_desc,p.pres_desc , p.radi_nume_radi,e.sgd_exp_numero,p.pres_estado,s.param_valor,u.usua_nomb,p.usua_login_actu,p.depe_codi,d.depe_nomb,
			p.usua_login_actu,r.radi_depe_actu,p.usua_login_pres,p.usua_login_canc,r.radi_nume_hoja,
			to_char(p.pres_fech_pedi, 'yyyy-mm-dd hh:mi:ss') as  f_pedido,to_char(p.pres_fech_pres, 'yyyy-mm-dd hh:mi:ss am') as f_prestado,
			to_char(p.pres_fech_canc, 'yyyy-mm-dd hh:mi:ss am') as f_cancelado,to_char(p.pres_fech_venc, 'yyyy-mm-dd hh:mi:ss am') as f_vencido,
			to_char(p.pres_fech_devo, 'yyyy-mm-dd hh:mi:ss am') as f_devuelto,r.radi_path,r.radi_nume_hoja,r.radi_depe_actu,
			''  as t_espera
			from prestamo  p
			left join radicado  r on (p.radi_nume_radi=r.radi_nume_radi)
			left join sgd_parametro  s on (p.pres_estado = s.param_codi)
			left join usuario  u on (u.usua_login=p.usua_login_actu)
			left join dependencia  d on (d.depe_codi=p.depe_codi)
			left join sgd_exp_expediente  e on (p.radi_nume_radi = e.radi_nume_radi)
			where $ini $expSql $radSql $depeSql $estado";

		$rs = $this->link->query ( $sql );
		$i = 0;

		while ( ! $rs->EOF ) {
			//echo $rs." "."TERMINA"."<br>";usua_login_actu
			$combi [$i] ["PRES_ID"] = $rs->fields ['PRES_ID'];
			$combi [$i] ["DESCPRES"] = $rs->fields ['PRES_DESC'];
			$combi [$i] ["DESCDEV"] = $rs->fields ['DEV_DESC'];
			$combi [$i] ["USUAPRES"] = $rs->fields ['USUA_LOGIN_ACTU'];
			$combi [$i] ["RADI_NUME_RADI"] = $rs->fields ['RADI_NUME_RADI'];
			$combi [$i] ["SGD_EXP_NUMERO"] = $rs->fields ['SGD_EXP_NUMERO'];
			$combi [$i] ["DEPE_NOMB"] = $rs->fields ['DEPE_NOMB'];
			$combi [$i] ["F_PEDIDO"] = $rs->fields ['F_PEDIDO'];
			$combi [$i] ["RADI_NUME_HOJA"] = $rs->fields ['RADI_NUME_HOJA'];
			$combi [$i] ["PARAM_VALOR"] = $rs->fields ['PARAM_VALOR'];
			$combi [$i] ["RADI_DEPE_ACTU"] = $rs->fields ['RADI_DEPE_ACTU'];
			$combi [$i] ["F_PRESTADO"] = $rs->fields ['F_PRESTADO'];
			$combi [$i] ["F_VENCIDO"] = $rs->fields ['F_VENCIDO'];
			$combi [$i] ["F_DEVUELTO"] = $rs->fields ['F_DEVUELTO'];
			$combi [$i] ["F_CANCELADO"] = $rs->fields ['F_CANCELADO'];
			$combi [$i] ["T_ESPERA"] = $rs->fields ['T_ESPERA'];
			$i ++;
			$rs->MoveNext ();
		}
		return $combi;

	}

	function buscarUsuario( $depe,$usuario) {

		$sql = "select
 p.pres_id, p.radi_nume_radi,p.pres_estado,p.usua_login_actu,p.depe_codi,
 p.usua_login_actu,p.usua_login_pres,p.usua_login_canc,
to_char(p.pres_fech_pedi, 'yyyy-mm-dd hh:mi:ss')  f_pedido,to_char(p.pres_fech_pres, 'yyyy-mm-dd hh:mi:ss am')  f_prestado,
to_char(p.pres_fech_canc, 'yyyy-mm-dd hh:mi:ss am')  f_cancelado,to_char(p.pres_fech_venc, 'yyyy-mm-dd hh:mi:ss am')  f_vencido,
 to_char(p.pres_fech_devo, 'yyyy-mm-dd hh:mi:ss am')  f_devuelto
 from prestamo p where p.usua_login_actu   ='$usuario' and p.depe_codi=$depe";
		//print_r("SQL".$sql);
		$rs = $this->link->query ( $sql );
		$i = 0;

		while ( ! $rs->EOF ) {
		//echo $rs." "."TERMINA"."<br>";usua_login_actu
			$combi [$i] ["PRES_ID"] = $rs->fields ['PRES_ID'];
			$combi [$i] ["USUAPRES"] = $rs->fields ['USUA_LOGIN_ACTU'];
			$combi [$i] ["RADI_NUME_RADI"] = $rs->fields ['RADI_NUME_RADI'];
			$combi [$i] ["SGD_EXP_NUMERO"] = $rs->fields ['SGD_EXP_NUMERO'];
			$combi [$i] ["DEPE_NOMB"] = $rs->fields ['DEPE_NOMB'];
			$combi [$i] ["F_PEDIDO"] = $rs->fields ['F_PEDIDO'];
			$combi [$i] ["RADI_NUME_HOJA"] = $rs->fields ['RADI_NUME_HOJA'];
			$combi [$i] ["PARAM_VALOR"] = $rs->fields ['PARAM_VALOR'];
			$combi [$i] ["RADI_DEPE_ACTU"] = $rs->fields ['RADI_DEPE_ACTU'];
			$combi [$i] ["F_PRESTADO"] = $rs->fields ['F_PRESTADO'];
			$combi [$i] ["F_VENCIDO"] = $rs->fields ['F_VENCIDO'];
			$combi [$i] ["F_DEVUELTO"] = $rs->fields ['F_DEVUELTO'];
			$combi [$i] ["F_CANCELADO"] = $rs->fields ['F_CANCELADO'];
			$i ++;
			$rs->MoveNext ();
		}
		return $combi;

		}

	function prestar($rad, $login,$idp, $estado, $descrip, $fechaVencimiento) {
		// 5 indefinido
		// 2 definido

		if ($estado == 2) {

			$sqlFechaVenc = $this->link->conn->DBDate ( $fechaVencimiento );
			$setFecha .= ",PRES_FECH_VENC=" . $sqlFechaVenc . " ";

		}
		 $sSQL = "update PRESTAMO set PRES_FECH_PRES=current_timestamp, PRES_DESC='$descrip', USUA_LOGIN_PRES='$login' $setFecha
	         ,PRES_ESTADO=$estado where radi_nume_radi='$rad' and PRES_ID=$idp";
		$rs = $this->link->query ( $sSQL );

	}
	function devolver($rad, $login,$idp,  $descrip) {
		// 3 devolver
		 $sSQL = "update PRESTAMO set PRES_FECH_DEVO=current_timestamp, DEV_DESC='$descrip', USUA_LOGIN_RX='$login'
	         ,PRES_ESTADO=3 where radi_nume_radi='$rad' and PRES_ID=$idp";
		$rs = $this->link->query ( $sSQL );

	}
	function canSol($rad, $login,$idp) {
		// 4 cancelar
         $descrip="Cancelar Solicitud de Pr&eacute;stamo";
		  $sSQL = "update PRESTAMO set PRES_FECH_CANC=current_timestamp, DEV_DESC='$descrip',PRES_DESC='$descrip', USUA_LOGIN_CANC='$login'
	         ,PRES_ESTADO=4 where radi_nume_radi='$rad' and PRES_ID=$idp";
		$rs = $this->link->query ( $sSQL );

	}

	function solPrestamo($rad, $depe, $login, $fechPed, $PRES_DEPE_ARCH, $PRES_REQUERIMIENTO, $estado, $descrip, $estado) {
		$sec = $this->link->conn->nextId ( 'SEC_PRESTAMO' );
		$sqlFechaPed = $this->link->conn->DBDate ( $fechPed );
		$sSQL = "insert into PRESTAMO(
                  PRES_ID,
                  RADI_NUME_RADI,
		   	      USUA_LOGIN_ACTU,
			      DEPE_CODI,
                  PRES_FECH_PEDI,
                  PRES_DEPE_ARCH,
                  PRES_ESTADO,
                  PRES_REQUERIMIENTO)
               values ('$sec','$rad','$login','$depe',$sqlFechPed,'$PRES_DEPE_ARCH',1,'$PRES_REQUERIMIENTO')";
		$rs = $this->link->query ( $sql );

	}


} // class


//echo "fin prestamo-class"."<BR>";


?>
