<?php /** 
 * modeloBandejas es la clase encargada de gestionar las operaciones de las bandejas
 * @author Hardy Deimont Niño  Velasquez
 * @name modeloBandejas
 * @version	1.0
 */
//inclu
if (! $ruta_raiz)
	$ruta_raiz = '../../../..';
include_once "$ruta_raiz/core/clases/ConnectionHandler.php";
class modeloBandejas {
	public $link;
	
	function __construct($ruta_raiz) {
		
		$db = new ConnectionHandler ( "$ruta_raiz" );
		$db->conn->SetFetchMode ( ADODB_FETCH_NUM );
		$db->conn->SetFetchMode ( ADODB_FETCH_ASSOC );
		$this->link = $db;
	}
    	/**
	 * @return consulta la bandeja.
	 */
	function consultarBandeja($dependencia,$codusuario,$carpeta,$agendado=0) {
            	$fecha_hoy = Date("Y-m-d");
	$sqlFechaHoy=$this->link->conn->DBDate($fecha_hoy);

	//Filtra el query para documentos agendados
	 if ($agendado==1){
	 	$sqlAgendado=" and (radi_agend=1 and radi_fech_agend > $sqlFechaHoy) "; // No vencidos

	 }
	 else  if ($agendado==2){
	   	$sqlAgendado=" and (radi_agend=1 and radi_fech_agend <= $sqlFechaHoy)  "; // vencidos
	 }else{
            $sqlAgendado='';    
         }
            $whereUsuario = " and b.radi_usua_actu='$codusuario' ";
                $whereCarpeta = " and b.carp_codi=$carpeta ";
		$sqlFecha = $this->link->conn->SQLDate("Y-m-d H:i A","b.RADI_FECH_RADI");
                $whereFiltro = str_replace("b.radi_nume_radi","cast(b.radi_nume_radi as varchar(15))",$whereFiltro);
		$redondeo="date_part('days', radi_fech_radi-".$this->link->conn->sysTimeStamp.")+floor(c.sgd_tpr_termino * 7/5)+(select count(*) from sgd_noh_nohabiles where NOH_FECHA between radi_fech_radi and ".$this->link->conn->sysTimeStamp.")";
                $query = 'select
				b.RADI_NUME_RADI as "IDT_Numero Radicado"
				,b.RADI_PATH as "HID_RADI_PATH"
				,'.$sqlFecha.' as "DAT_Fecha Radicado"
				, b.RADI_NUME_RADI as "HID_RADI_NUME_RADI"
				,b.RA_ASUN  as "Asunto"'.
				$colAgendado.
				',d.SGD_DIR_NOMREMDES  as "Remitente"
				,c.SGD_TPR_DESCRIP as "Tipo Documento" 
				,'.$redondeo.' as "Dias Restantes"
				,b.RADI_USU_ANTE as "Enviado Por"
				,b.RADI_NUME_RADI as "CHK_CHKANULAR"
				,b.RADI_LEIDO as "HID_RADI_LEIDO"
				,b.RADI_NUME_HOJA as "HID_RADI_NUME_HOJA"
				,b.CARP_PER as "HID_CARP_PER"
				,b.CARP_CODI as "HID_CARP_CODI"
				,b.SGD_EANU_CODIGO as "HID_EANU_CODIGO"
				,b.RADI_NUME_DERI as "HID_RADI_NUME_DERI"
				,b.RADI_TIPO_DERI as "HID_RADI_TIPO_DERI"
                        	 from
                		 radicado b
                                left outer join SGD_TPR_TPDCUMENTO c
                            	on b.tdoc_codi=c.sgd_tpr_codigo
                    	left outer join SGD_DIR_DRECCIONES d
                on b.radi_nume_radi=d.radi_nume_radi
                   where 
		b.radi_nume_radi is not null
		and b.radi_depe_actu='.$dependencia.
		$whereUsuario.$whereFiltro.'
		'.$whereCarpeta.'
		'.$sqlAgendado.'
	  order by '.$order .' ' .$orderTipo;
		$rs = $this->link->query ( $query );
		$i = 0;
		while ( ! $rs->EOF ) {
			$combi [$i] ["CODIGO"] = $rs->fields ['SGD_TRAD_CODIGO'];
			$combi [$i] ["DESCRIP"] = $rs->fields ['SGD_TRAD_DESCR'];
			$combi [$i] ["GENRADSAL"] = $rs->fields ['SGD_TRAD_GENRADSAL'];
			$combi [$i] ["ICONO"] = $rs->fields ['SGD_TRAD_ICONO'];
			$i ++;
			$rs->MoveNext ();
		}
		return $combi;
	
	}
}

?>
