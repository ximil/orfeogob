<?php 
/**
 * modeloRadicado es la clase encargada de gestionar las operaciones y los datos basicos referentes a un Radicado
 * @author Hardy Deimont Niño  Velasquez
 * @name modeloRadicado
 * @version	1.0
 */
if (!$ruta_raiz)
    $ruta_raiz = '../../../..';
//include_once "$ruta_raiz/include/db/ConnectionHandler.php";
include_once "$ruta_raiz/core/clases/ConnectionHandler.php";
class modeloRadicado {
    //link de conexion con la base de datos
    public $link;

    function __construct($ruta_raiz) {

        $db = new ConnectionHandler("$ruta_raiz");
        $db->conn->SetFetchMode(ADODB_FETCH_NUM);
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        $this->link = $db;
        //if ($_SESSION['test'] == 1)
        //    $this->link->conn->debug = true;
    }

    /**
     * Funtion numRadicado crea el  numero de radicado.
     * @param type $tpRad
     * @param type $tpDepeRad
     * @param type $noDigitosRad
     * @return type 
     */
    function numRadicado($tpRad, $tpDepeRad, $noDigitosRad, $tpDepeCons) {
        # Busca el usuairo Origen para luego traer sus datos.
        $SecName = "SECR_TP$tpRad" . "_" . $tpDepeCons;
        $secNew = $this->link->conn->nextId($SecName);
        if ($secNew == 0) {
            $this->Link->conn->RollbackTrans();
            $secNew = $this->link->conn->nextId($SecName);
            if ($secNew == 0)
                throw new Exception("Error no genero un Numero de Secuencia");
        }
        return $newRadicado = date("Y") . $tpDepeRad . str_pad($secNew, $noDigitosRad, "0", STR_PAD_LEFT) . $tpRad;
    }
    /* Funcion de generar el numero del consecutivo de la dependencia
     * 
     * @return number depeconse
     */
    function valConsecutivo($tpDepeRad, $tpRad) {
        $select = "select depe_rad_tp$tpRad cons from dependencia where depe_codi={$tpDepeRad}";

        $rs = $this->link->conn->Execute($select);
        $depeconse = $rs->fields["CONS"];
        return $depeconse;
    }

    function consNivelUser($radiDepeActu, $radiUsuaActu, $radiUsuaidrol) {
        $select = "SELECT drm.sgd_drm_valor as CODI_NIVEL
                    FROM     sgd_urd_usuaroldep urd,   sgd_mod_modules mod,   sgd_drm_dep_mod_rol drm
                    WHERE 
                      mod.sgd_mod_id = drm.sgd_drm_modecodi AND drm.sgd_drm_rolcodi = urd.rol_codi AND
                      drm.sgd_drm_depecodi = urd.depe_codi AND mod.sgd_mod_estado = 1 AND urd.rol_codi=$radiUsuaidrol and
                      urd.depe_codi = $radiDepeActu AND mod.sgd_mod_modulo = 'codi_nivel'";
        $rs = $this->link->conn->Execute($select);
        $nivel = $rs->fields["CODI_NIVEL"];
        return $nivel;
    }

    /**
     * Crea radicado.
     * @param type $radiDepeRadi
     * @param type $noDigitosRad
     * @param type $tpRad
     * @param type $tipo
     * @param type $codigo_orfeo
     * @param type $cedula
     * @param type $nombre_remitente
     * @param type $apellidos_remitente
     * @param type $muni
     * @param type $depto
     * @param type $direccion_remitente
     * @param type $telefono_remitente
     * @param type $usuaradi
     * @param type $asunto
     * @param type $trteCodi
     * @param type $tdidCodi
     * @param type $radiDepeActu
     * @param type $radiUsuaActuDoc
     * @param type $radiCuentai
     * @param type $radiUsuaActu
     * @param type $radirolradi
     * @param type $radiUsuaidrol
     * @param type $nivelRad
     * @param type $sgd_apli_codi
     * @param type $radiTipoDeri
     * @param type $descAnex
     * @param type $mrecCodi
     * @param type $codiNivel
     * @param type $radiNumeDeri
     * @param type $radiPath
     * @param type $radiPais
     * @param type $carpCodi
     * @param type $folio
     * @return type
     */
    function radicar($radiDepeRadi, $noDigitosRad, $tpRad, $tipo, $codigo_orfeo, $cedula, $nombre_remitente, $apellidos_remitente, $muni, $depto, $direccion_remitente, $telefono_remitente, $usuaradi, $asunto, $trteCodi, $tdidCodi, $radiDepeActu, $usuaDoc,$radiUsuaActuDoc, $radiCuentai, $radiUsuaActu, $radirolradi, $radiUsuaidrol, $nivelRad, $sgd_apli_codi, $radiTipoDeri, $descAnex, $mrecCodi, $codiNivel, $radiNumeDeri = null, $radiPath = null, $radiPais = 'COLOMBIA', $carpCodi = 0, $folio = 0,$fechaOfic,$radiGuia=null,$dependencia=null,$codi_verifica=null) {
        try {
            //Numero de radicado
            $tpDepeCons = $this->valConsecutivo($radiDepeRadi, $tpRad);
            $numeroRadicado = $this->numRadicado($tpRad, $radiDepeRadi, $noDigitosRad, $tpDepeCons);
            $recordR["RADI_NUME_RADI"] = $numeroRadicado;
            $recordR["RADI_FECH_RADI"] = $this->link->conn->OffsetDate(0, $this->link->conn->sysTimeStamp);
            $recordR["TDOC_CODI"] = $tipo;
	    $recordR["SGD_TRAD_CODIGO"]=$tpRad;
            $recordR["MREC_CODI"] = $mrecCodi;
			if($dependencia)
				$recordR["DEPE_CODI"]= $dependencia;
            $recordR["EESP_CODI"] = $codigo_orfeo;
	    if($fechaOfic==null || $fechaOfic=='')
		$fechaOfic=date('d/m/Y');
            $recordR["radi_fech_ofic"] = "to_timestamp('$fechaOfic 0:00:00','dd/mm/yyyy hh24:mi:ss')";
	    if($cedula!=null || $cedula!='')
            	$recordR["radi_nume_iden"] = $cedula;
            $recordR["radi_nomb"] = "'" . strtoupper(substr($nombre_remitente, 0, 90)) . "'";
            $recordR["radi_segu_apel"] = "'" . strtoupper(substr($apellidos_remitente,0,49)) . "'";
            $recordR["RADI_PAIS"] = "'" . $radiPais . "'";
            $recordR["muni_codi"] = $muni;
            $recordR["carp_codi"] = $carpCodi;
            $recordR["dpto_codi"] = "'" . $depto . "'";
            $recordR["radi_dire_corr"] = "'" . strtoupper($direccion_remitente) . "'";
            if ($telefono_remitente)
            $recordR["radi_tele_cont"] = $telefono_remitente;
            if($folio<=0 || $folio==''){
		$recordR["radi_nume_hoja"] = 'NULL';
            	$recordR["radi_nume_folio"]= 'NULL';
	    }else{
	        $recordR["radi_nume_hoja"] = $folio;
	        $recordR["radi_nume_folio"]=$folio;
	    }
            $recordR["radi_desc_anex"] = $this->link->conn->qstr($descAnex);
	    if($codi_verifica)
		$recordR["sgd_rad_codigoverificacion"] = $this->link->conn->qstr($codi_verifica);
	    if($radiGuia)
		$recordR["RADI_NUME_GUIA"] = $radiGuia;
            if ($radiNumeDeri)
                $recordR["RADI_NUME_DERI"] = $radiNumeDeri;
            $recordR["RADI_PATH"] = "$radiPath";
            $recordR["RADI_DEPE_ACTU"] = $radiDepeActu;
            $recordR["RA_ASUN"] = $this->link->conn->qstr($asunto);
            $recordR["RADI_DEPE_RADI"] = $radiDepeRadi;
            $recordR["RADI_USUA_RADI"] = $usuaradi;
            $recordR["CODI_NIVEL"] = $this->consNivelUser($radiDepeActu, $radiUsuaActu, $radiUsuaidrol);
            //$recordR["flag_nivel"] = $radiUsuaActu;  
            $recordR["CARP_PER"] = 0;
            $recordR["RADI_LEIDO"] = 0;
            $recordR["radi_tipo_deri"] = $radiTipoDeri;
            $recordR["sgd_fld_codigo"] = 0;
            $recordR["SGD_APLI_CODI"] = $sgd_apli_codi;
            if ($nivelRad)
                $recordR["SGD_SPUB_CODIGO"] = $nivelRad;
            else
                $recordR["SGD_SPUB_CODIGO"] = 0;
            $recordR["ID_ROL"] = $radiUsuaidrol;
            $recordR["RADI_ROL_RADI"] = $radirolradi;
            $recordR["RADI_USUA_DOC_ACTU"] = $radiUsuaActuDoc;

            $recordR["RADI_CUENTAI"] = $this->link->conn->qstr($radiCuentai);
            $recordR["RADI_USUA_ACTU"] = $radiUsuaActu;
            $recordR["TRTE_CODI"] = $trteCodi;
            $recordR["TDID_CODI"] = $tdidCodi;

            /* foreach ($recordR as $fieldName => $field) {
              $fieldsnames[] = $fieldName;
              $temp[] = $field;
              }
              $sql = "insert into RADICADO (" . join(",", $fieldsnames) . ") values (" . join(",", $temp) . ")";
              //$radicar = $this->link->conn->Execute($sql);
             */

            $insertSQL = $this->link->insert("RADICADO", $recordR, "true");
	    if (!$insertSQL) {
            	echo "<hr><b><font color=red>Error no se inserto sobre radicado<br>SQL: " . $this->link->querySql . "</font></b><hr>";
            }


            //inserta en radicado
            //Inserta historico
            $ins_his = "insert into hist_eventos (depe_codi,hist_fech,usua_codi,radi_nume_radi,hist_obse,usua_codi_dest,usua_doc,sgd_ttr_codigo,hist_doc_dest,depe_codi_dest)
		values($radiDepeRadi,current_timestamp
                    ,$usuaradi,$numeroRadicado,'RADICACION'," . $radiUsuaActu . ",'$usuaDoc',2,'" . $radiUsuaActuDoc . "'," . $radiDepeActu . ")";
            //echo $ins_his;
            $rs_ins_his = $this->link->conn->Execute($ins_his);
            $dataRad = $numeroRadicado;


            return $dataRad;
        } catch (Exception $e) {

            return "ERROR: " . $e->getMessage();
        }
    }

    function consultarUsuarioActual($codusuario, $codDep, $rol) {
        $select = '';
        $rs = $this->link->conn->Execute($select);
    }

    /**
     * 
     * @param type $num_ciu
     * @param type $numeroRadicado
     * @param type $muni
     * @param type $depto
     * @param type $direccion_remitente
     * @param type $telefono_remitente
     * @param type $nombre_remitente
     * @param type $apellidos_remitente
     * @param type $sigla
     * @param type $nit
     * @param type $tpRem
     * @param int $codOEM
     */
    function dir_drecciones($num_ciu, $numeroRadicado, $muni, $depto, $direccion_remitente, $telefono_remitente, $nombre_remitente, $apellidos_remitente, $nit = 1, $codOEM = 0,$pais=170,$cont=1,$dignatario='',$docfun=null) {
        $num_dir = $this->link->conn->GenID('SEC_DIR_DIRECCIONES');
        IF ($codOEM == '' OR $codOEM == NULL)
            $codOEM = 0;
	if($docfun){
	    $funcionario="'$docfun'";
	}else
	    $funcionario="NULL";
        if(empty($nit)){
	    $nit="NULL";
	}
        //inserta en sgd_dir_direcciones
        $ins_dir = "insert into sgd_dir_drecciones(sgd_dir_codigo,sgd_dir_tipo,sgd_oem_codigo,sgd_ciu_codigo,radi_nume_radi,sgd_esp_codi,muni_codi,dpto_codi,sgd_dir_direccion,sgd_dir_telefono,sgd_sec_codigo,sgd_dir_nombre,sgd_dir_nomremdes,sgd_trd_codigo,sgd_dir_doc,id_pais,id_cont,sgd_doc_fun)
		values(" . $num_dir . ",1,$codOEM," . $num_ciu . ",$numeroRadicado,0," . $muni . "," . $depto . ",'" . $direccion_remitente . "','" . $telefono_remitente . "',0,'" . strtoupper($dignatario) . "','" . strtoupper($nombre_remitente) . " " . strtoupper($apellidos_remitente) . "',3," . $nit . ",$pais, $cont,$funcionario)";
        /*     $ins2="insert into SGD_DIR_DRECCIONES(SGD_TRD_CODIGO,SGD_DIR_NOMREMDES,SGD_DIR_DOC,MUNI_CODI,DPTO_CODI,ID_PAIS,ID_CONT,SGD_DOC_FUN,SGD_OEM_CODIGO,SGD_CIU_CODIGO,SGD_ESP_CODI,RADI_NUME_RADI,SGD_SEC_CODIGO,SGD_DIR_DIRECCION,SGD_DIR_TELEFONO,SGD_DIR_MAIL,SGD_DIR_TIPO,SGD_DIR_NOMBRE,SGD_DIR_CODIGO)
          values ($tpRem,'".strtoupper($nombre_remitente)." ".strtoupper($apellidos_remitente).",".$nit.",".$muni.",".$depto.",170,1,0,0,4556,0,$numeroRadicado,0,'".$direccion_remitente."','".$telefono_remitente."','',".$num_dir.",'',".$num_dir.")"; */
        //$ins_dir2 = utf8_encode($ins_dir);
        $rs_ins_ciu = $this->link->conn->Execute($ins_dir);

	if (!$rs_ins_ciu) {
            echo "<hr><b><font color=red>Error no se inserto sobre radicado<br>SQL: " .$ins_dir . "</font></b><hr>";
        }
    }

    /**
     * 
     * @param type $nombre_remitente
     * @param type $apellidos_remitente
     * @param type $direccion_remitente
     * @param type $telefono_remitente
     * @param type $email
     * @param type $muni
     * @param type $depto
     * @param type $cedula
     * @return type
     */
    function ciudano($nombre_remitente, $apellidos1, $apellidos2, $direccion_remitente, $telefono_remitente, $email, $muni, $depto, $cedula) {

        $num_ciu = $this->link->conn->GenID('SEC_CIU_CIUDADANO');
	$data="";
	$cabeza="";
//inserta ciudadano
        //
       if ($cedula) {
            $data.=",'$cedula'";
            $cabeza.=",sgd_ciu_cedula";
        }
        if ($telefono_remitente) {
            $data.=",'$telefono_remitente'";
            $cabeza.=",sgd_ciu_telefono";
        }
        if ($apellidos2) {
            $data.=",'" . mb_strtoupper($apellidos2) . "'";
            $cabeza.=",sgd_ciu_apell2";
        }
        if ($email) {
            $data.=",'" . $email . "'";
            $cabeza.=",sgd_ciu_email";
        }

        $ins_ciu = "insert into sgd_ciu_ciudadano "
                . "(tdid_codi,sgd_ciu_codigo,sgd_ciu_nombre,sgd_ciu_direccion,sgd_ciu_apell1,muni_codi,dpto_codi $cabeza)"
                . "values(2," . $num_ciu . ",'" . mb_strtoupper($nombre_remitente) . "','" . mb_strtoupper($direccion_remitente) . "','" . mb_strtoupper($apellidos1) . "'," . $muni . "," . $depto . " $data)";
        //$ins_ciu2 = utf8_encode($ins_ciu);
        $this->link->conn->Execute($ins_ciu);
        return $num_ciu;
    }

	function permTRad($depus,$rolus){
	    $sql="select sgd_trad_codigo,sgd_trad_descr from sgd_drm_dep_mod_rol, sgd_mod_modules,sgd_trad_tiporad where sgd_drm_depecodi=$depus 
		and sgd_drm_rolcodi=$rolus and sgd_drm_modecodi=sgd_mod_id and sgd_mod_modulo like 'usua_prad_tp%' and 
		cast(sgd_trad_codigo as char(1)) = substr(sgd_mod_modulo,13,1) and sgd_drm_valor not in (0,2)";
	    $rs= $this->link->conn->Execute($sql);
	    if(!$rs->EOF){
		$i=0;
		while(!$rs->EOF){
		    $trad_desc[$i]=$rs->fields['SGD_TRAD_DESCR'];
		    $trad_cod[$i]=$rs->fields['SGD_TRAD_CODIGO'];
		    $i++;
		    $rs->MoveNext();
		}
	    }
	    else{
		$trad_desc[0]='error';
                $trad_cod[0]=0;
	    }
	    return array($trad_desc,$trad_cod);
	}

	function permTRad2($depus,$rolus){
            $sql="select sgd_trad_codigo,sgd_trad_descr from sgd_drm_dep_mod_rol, sgd_mod_modules,sgd_trad_tiporad where sgd_drm_depecodi=$depus
                and sgd_drm_rolcodi=$rolus and sgd_drm_modecodi=sgd_mod_id and sgd_mod_modulo like 'usua_prad_tp%' and
                cast(sgd_trad_codigo as char(1)) = substr(sgd_mod_modulo,13,1) and sgd_drm_valor not in (0,2) and sgd_trad_codigo<>2";
            $rs= $this->link->conn->Execute($sql);
            if(!$rs->EOF){
                $i=0;
                while(!$rs->EOF){
                    $trad_desc[$i]=$rs->fields['SGD_TRAD_DESCR'];
                    $trad_cod[$i]=$rs->fields['SGD_TRAD_CODIGO'];
                    $i++;
                    $rs->MoveNext();
                }
            }
            else{
                $trad_desc[0]='error';
                $trad_cod[0]=0;
            }
            return array($trad_desc,$trad_cod);
        }
	/* Funcion encargada de consultar radicados 
         *  @param numeric radicado numero referencia del radicado
         *  @param date fecha_ini fecha de rango inicial
         *  @param date fecha_ini fecha de rango final
         * 
         *  @return array Resultado de la consulta hecha a la base de datos
         */
	function consultarRad($radicado, $fecha_ini,$fecha_fin){
	    if(strlen(trim($radicado))<12){
		$where="and r.radi_fech_radi between to_timestamp('$fecha_ini 0:00:00','dd/mm/yyyy hh24:mi:ss') and to_timestamp('$fecha_fin 23:59:59','dd/mm/yyyy hh24:mi:ss') ";
	    }
	    else
		$where="";
	    $sql="select r.radi_nume_radi as numrad, to_char(r.radi_fech_radi,'YYYY-MM-DD HH24:II:SS') fecha,r.ra_asun as asunto, d.sgd_dir_nomremdes destinatario from radicado r, sgd_dir_drecciones d 
		where d.radi_nume_radi=r.radi_nume_radi and d.sgd_dir_tipo=1 and cast(r.radi_nume_radi as char(18)) like '$radicado%' $where order by fecha desc";
	    $rs=$this->link->conn->Execute($sql);
	    $retorno['error']=""; 
	    if(!$rs->EOF){
		$i=0;
		while(!$rs->EOF){
		    $retorno[$i]["numrad"]=$rs->fields['NUMRAD'];
		    $retorno[$i]["fecha"]=$rs->fields['FECHA'];
		    $retorno[$i]["asunto"]=$rs->fields['ASUNTO'];
		    $retorno[$i]["destinatario"]=$rs->fields['DESTINATARIO'];
		    $i++;
		    $rs->MoveNext();
		}
	    }
	    else{
		$retorno['error']="Error datos no encontrados";
	    }
	    return $retorno;
	}

	/* Funcion encargada de buscar por nombre en la tabla de ciudadano
         * @param parametro parte del nombre del ciudadano
         * 
         * @return array Resultado de la busqueda 
         */

	function consultarCiudadano($parametro){
	    $pieces=explode(" ",$parametro);
	    $sentinel=0;
	    $where="";
	    $retorno['error']="";
	    for($i=0;$i<count($pieces);$i++){
		if($sentinel==0 && strlen($pieces[$i])>2){
		    $where.="(upper(sgd_ciu_nombre) like '%".strtoupper($pieces[$i])."%' or upper(sgd_ciu_apell1) like '%".strtoupper($pieces[$i])."%' or upper(sgd_ciu_apell2) like '%".strtoupper($pieces[$i])."%')";
		    $sentinel=1;
		}elseif(strlen($pieces[$i])>2){
		    $where.=" and (upper(sgd_ciu_nombre) like '%".strtoupper($pieces[$i])."%' or upper(sgd_ciu_apell1) like '%".strtoupper($pieces[$i])."%' or upper(sgd_ciu_apell2) like '%".strtoupper($pieces[$i])."%')";
		}
	    }
	    if($where!=""){
		$sql="select sgd_ciu_codigo,tdid_codi, sgd_ciu_nombre, sgd_ciu_apell1, sgd_ciu_apell2, sgd_ciu_cedula, sgd_ciu_direccion, sgd_ciu_telefono,
		    c.id_cont,c.id_pais,c.muni_codi, c.dpto_codi,sgd_ciu_email, muni_nomb from
            	    sgd_ciu_ciudadano c, municipio m where $where and m.dpto_codi=c.dpto_codi and m.muni_codi=c.muni_codi order by sgd_ciu_nombre";
		$rs=$this->link->conn->Execute($sql);
		if(!$rs->EOF){
		    $i=0;
                    while(!$rs->EOF){
                    	$retorno[$i]['id']=$rs->fields['SGD_CIU_CODIGO'];
                    	$retorno[$i]['tdid_codi']=$rs->fields['TDID_CODI'];
                    	$retorno[$i]['nombre']=$rs->fields['SGD_CIU_NOMBRE'];
                    	$retorno[$i]['apell1']=$rs->fields['SGD_CIU_APELL1'];
                    	$retorno[$i]['apell2']=$rs->fields['SGD_CIU_APELL2'];
                    	$retorno[$i]['cedula']=$rs->fields['SGD_CIU_CEDULA'];
                    	$retorno[$i]['direccion']=$rs->fields['SGD_CIU_DIRECCION'];
			$retorno[$i]['telefono']=$rs->fields['SGD_CIU_TELEFONO'];
			$retorno[$i]['email']=$rs->fields['SGD_CIU_EMAIL'];
                        $retorno[$i]['muni_codi']=$rs->fields['MUNI_CODI'];
                        $retorno[$i]['dpto_codi']=$rs->fields['DPTO_CODI'];
                        $retorno[$i]['id_cont']=$rs->fields['ID_CONT'];
                        $retorno[$i]['id_pais']=$rs->fields['ID_PAIS'];
                        $retorno[$i]['municipio']=$rs->fields['MUNI_NOMB'];
                    	$i++;
                    	$rs->MoveNext();
                    }
            	}
		else
            	{
                    $retorno['error']="No se encontraron resultado en ciudadano";
            	}
	    }
	    else{
		$retorno['error']="No se encontraron resultado en ciudadano";
	    }
	    return $retorno;
	}

        /* Funcion encargada de buscar por nombre en la tabla de OEM
         * @param parametro parte del nombre del OEM
         * 
         * @return array Resultado de la busqueda 
         */
		 
	function empresa($nombre_nus1, $prim_apell_nus1, $seg_apell_nus1, $direccion_nus1, $telefono_nus1, $email, $muni, $depto, $no_documento) {
        $nextval = $this->link->nextId("sec_oem_oempresas");
        if ($no_documento) {
            $data.=",'$no_documento'";
            $cabeza.=",SGD_OEM_NIT";
        }
        if ($telefono_nus1) {
            $data.=",'$telefono_nus1'";
            $cabeza.=",SGD_OEM_TELEFONO";
        }

        if ($email) {
            $data.=",'" . $email . "'";
            $cabeza.=",sgd_oem_email";
        }


        $isql = "INSERT INTO SGD_OEM_OEMPRESAS( tdid_codi, SGD_OEM_codigo, SGD_OEM_OEMPRESA, SGD_OEM_DIRECCION, 
				SGD_OEM_REP_LEGAL, SGD_OEM_SIGLA,  ID_CONT, ID_PAIS, DPTO_CODI, MUNI_CODI,sgd_oem_estado) 
				values (4, $nextval, '$nombre_nus1', '$direccion_nus1', '$seg_apell_nus1', 
						'$prim_apell_nus1',1, 170, $depto, $muni,1)";
        $ins_ciu2 = $isql; //utf8_encode($isql);
        $rs_ins_ciu = $this->link->conn->Execute($ins_ciu2);
        return $nextval;


        if ($nextval == -1) {
            die("<span class='etextomenu'>No se encontr&oacute; la secuencia sec_oem_oempresas ");
        }
    }
     

/**
     * Retorna el valor correspondiente al 
     * resultado de la validacion
     * @numrad  Numero del Radicado a validar
     * @return   array  $vecRads resultado de la operacion de validacion
     */
    function consultarPermRad($numradi) {
        $radi = '';
        $isql = "SELECT r.RADI_PATH, r.SGD_SPUB_CODIGO,drm.sgd_drm_valor CODI_NIVEL,  u.usua_nomb,   u.usua_doc,  r.RADI_USU_ANTE, r.RADI_DEPE_ACTU
          FROM   RADICADO r,  usuario u,   sgd_urd_usuaroldep urd,   sgd_mod_modules mod,   sgd_drm_dep_mod_rol drm  
          WHERE r.RADI_NUME_RADI='$numradi' and
          u.usua_codi = urd.usua_codi   and r.RADI_USUA_ACTU= u.USUA_CODI AND
          mod.sgd_mod_id = drm.sgd_drm_modecodi AND  drm.sgd_drm_rolcodi = urd.rol_codi AND
          drm.sgd_drm_depecodi = urd.depe_codi   and r.RADI_DEPE_ACTU= urd.depe_codi AND  mod.sgd_mod_modulo = 'codi_nivel'";
        $rs = $this->link->conn->Execute($isql);
        $radi['seguridadRadicado'] = $rs->fields["SGD_SPUB_CODIGO"];
        $radi['nivelRadicado'] = $rs->fields["CODI_NIVEL"];
        $radi['usua_actu'] = $rs->fields["USUA_DOC"];
        $radi['usua_ante'] = $rs->fields["RADI_USU_ANTE"];
        $radi['depe_Actu'] = $rs->fields["RADI_DEPE_ACTU"];
        $radi['pathImagen'] = $rs->fields['RADI_PATH'];
        return $radi;
    }

    function consPermRadInfo($numradi, $usua_doc) {
        $usuaInformado = '';
        $isql = "select USUA_DOC from INFORMADOS where RADI_NUME_RADI='$numradi'and USUA_DOC= '$usua_doc'";

        $rs = $this->link->conn->Execute($isql);
        $usuaInformado = $rs->fields["USUA_DOC"];

        return $usuaInformado;
    }

    function consulPermExp($numradi, $usuadoc) {
        $res = '';
        $consultaExpediente = "SELECT EXP.SGD_EXP_NUMERO,  EXP.SGD_EXP_PRIVADO AS PRIVEXP, USUA_DOC_RESPONSABLE AS RESPONSABLE
				  FROM 
					  RADICADO R, SGD_SEXP_SECEXPEDIENTES SEXP, SGD_EXP_EXPEDIENTE EXP
				  WHERE 
					  R.RADI_NUME_RADI=$numradi 
					  AND R.RADI_NUME_RADI = EXP.RADI_NUME_RADI 
					  AND EXP.SGD_EXP_NUMERO = SEXP.SGD_EXP_NUMERO
					  AND EXP.sgd_exp_estado<>2
					  AND USUA_DOC_RESPONSABLE = '$usuadoc'";
        $rs = $this->link->conn->Execute($consultaExpediente);
        //validar exp esto se debe pasar a la clase expediente

        $res['numExp'] = $rs->fields["SGD_EXP_NUMERO"];
        $res['Estado'] = $rs->fields["PRIVEXP"];
        return $res;
    }

    function Consultar($radiNume, $fechI = null, $fechF = null, $tipoDoc = null) {
        /* $consultaRad = "SELECT radi_nume_radi, radi_depe_actu, radi_usua_actu, radi_path,radi_nume_deri
          FROM RADICADO WHERE radi_nume_radi = '$radiNume'"; */
        $consultaRad = "SELECT   t.sgd_tpr_descrip descp,   r.radi_nume_radi,r.ra_asun,   r.radi_path,   r.radi_usua_actu,   r.radi_depe_actu,   r.radi_nume_deri
                        FROM   sgd_tpr_tpdcumento t,   radicado r
                        WHERE   r.tdoc_codi = t.sgd_tpr_codigo and r.radi_nume_radi = '$radiNume'";
        $rsRad = $this->link->conn->Execute($consultaRad);
        $rsRada['RADI_DEPE_ACTU'] = $rsRad->fields['RADI_DEPE_ACTU'];
        $rsRada['RADI_NUME_RADI'] = $rsRad->fields['RADI_NUME_RADI'];
        $rsRada['RADI_USUA_ACTU'] = $rsRad->fields['RADI_USUA_ACTU'];
        $rsRada['RADI_PATH'] = $rsRad->fields['RADI_PATH'];
        $rsRada['RA_ASUN'] = $rsRad->fields['RA_ASUN'];
        $rsRada['tpDoc'] = $rsRad->fields['DESCP'];
        return $rsRada;
    }

    function crearAnexo($radiNume, $nombreAnexo, $tipoAnexo, $tamanoAnexo, $usuLogin, $descripcion, $numAnexo, $nombreAnexo, $extension, $usuDep, $namefileAnexo = null) {
        $fechaAnexado = 'CURRENT_TIMESTAMP'; //$this->link->conn->OffsetDate(0, $db->conn->sysTimeStamp);
        $consulta = "INSERT INTO ANEXOS (ANEX_RADI_NUME, ANEX_CODIGO, ANEX_TIPO, ANEX_TAMANO, ANEX_SOLO_LECT, ANEX_CREADOR, ANEX_DESC, 
						ANEX_NUMERO, ANEX_NOMB_ARCHIVO, ANEX_BORRADO, ANEX_ESTADO, SGD_REM_DESTINO, SGD_DIR_TIPO, ANEX_DEPE_CREADOR, ANEX_FECH_ANEX) 
						VALUES ('$radiNume', '$nombreAnexo', $tipoAnexo, $tamanoAnexo, 'N', '$usuLogin', '$descripcion', $numAnexo, 
						'$namefileAnexo', 'N', 1, 1, 1, $usuDep, $fechaAnexado)";

        $rsInsert = $this->link->conn->Execute($consulta);
    }

    function validarAnexo($nombreAnexo) {
        $consultaVerificacion = "SELECT ANEX_CODIGO FROM ANEXOS WHERE ANEX_CODIGO = '$nombreAnexo'";
        $rs = $this->link->conn->Execute($consultaVerificacion);
        return $rs->fields['ANEX_CODIGO'];
    }

    /**
     * funcion que calculcula el numero de anexos que tiene un radicado
     *
     * @param int  $radiNume radicado al cual se realiza se adiciona el anexo
     * @param ConectionHandler $db
     * @return int numero de anexos del radicado
     */
    function numeroAnexos($radiNume) {
        $consulta = "SELECT COUNT(1) AS NUM_ANEX FROM ANEXOS WHERE ANEX_RADI_NUME={$radiNume}";
        $salida = 0;
        $rs = & $this->link->query($consulta);
        if ($rs && !$rs->EOF)
            $salida = $rs->fields ['NUM_ANEX'];
        return $salida;
    }

    /**
     * funcioncion que rescata el maxido del anexo de los radicados 
     *
     * @param int $radiNume numero del radicado
     * @param ConnectionHandler $db conexion con la db
     * @return int maximo
     */
    function maxRadicados($radiNume) {
        $consulta = "SELECT max(ANEX_NUMERO) AS NUM_ANEX FROM ANEXOS WHERE ANEX_RADI_NUME={$radiNume}";
        $rs = & $this->link->query($consulta);
        if ($rs && !$rs->EOF)
            $salida = $rs->fields ['NUM_ANEX'];
        return $salida;
    }

    /**
     * funcion encargada regenerar un archivo enviado en base64
     *
     * @param string $ruta ruta donde se almacenara el archivo 
     * @param base64 $archivo archivo codificado en base64
     * @param string $nombre nombre del archivo
     * @return boolean retorna si se pudo decodificar el archivo
     */
    function subirArchivo($ruta, $archivo, $nombre) {
        try {
            //$archivo=  str_replace('"','', $archivo);
            //  return $archivo;
            //direccion donde se quiere guardar los archivos
            $fp = @fopen("{$ruta}", "w");
            ///$bytes = base64_decode($archivo);
            $bytes = $archivo;
            $salida = true;
            if (is_array($bytes)) {
                foreach ($bytes as $k => $v) {
                    $salida = ($salida && fwrite($fp, $bytes));
                }
            } else {
                $salida = fwrite($fp, $bytes);
            }
            fclose($fp);
        } catch (Exception $e) {
            return "error";
        }
        return $salida;
    }

    function ActualizarRadicado($radiNume, $filename) {
        $isql = "update radicado set  RADI_PATH='$filename' where RADI_NUME_RADI='$radiNume'";
        $rs = $this->link->conn->Execute($isql);
        $radicadosSel[] = $rad;
    }

    function vActRadicado($radiNume, $filename) {
        $isql = "select RADI_NUME_RADI from radicado where  RADI_PATH='$filename' and RADI_NUME_RADI='$radiNume'";
        $rs = $this->link->conn->Execute($isql);
        $radicado = $rs->fields["RADI_NUME_RADI"];
        return $radicado;
    }

    /**
     * funcion que consulta el tipo de anexo que se esta generando
     * 
     *
     * @param sting $extension extencion del archivo
     * @param ConnectionHandler $db conexion con la DB
     * @return int
     */
    function tipoAnexo($extension) {
        $consulta = "SELECT ANEX_TIPO_CODI FROM ANEXOS_TIPO WHERE ANEX_TIPO_EXT='" . strtolower($extension) . "'";
        $salida = null;
        $rs = & $this->link->query($consulta);
        if ($rs && !$rs->EOF)
            $salida = $rs->fields ['ANEX_TIPO_CODI'];
        return $salida;
    }

    function consultarDatosAnexos($radicado) {
        $consulta = "SELECT sgd_tpr_codigo tp,anex_tipo,anex_desc,anex_codigo,anex_nomb_archivo,radi_nume_salida salida FROM anexos WHERE anex_radi_nume=$radicado order  by anex_codigo asc";
        $rs = $this->link->query($consulta);
        $i = 1;
        $datos = array();
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                $datos [$i] ['anex_codigo'] = $rs->fields ['ANEX_CODIGO'];
		$datos [$i] ['anex_tipo'] = $rs->fields ['ANEX_TIPO'];
                $datos [$i] ['path'] = $rs->fields ['ANEX_NOMB_ARCHIVO'];
                $datos [$i] ['salida'] = $rs->fields ['SALIDA'];
                $datos [$i] ['codiTP'] = $rs->fields ['TP'];
                $datos [$i] ['anex_desc'] = $rs->fields ['ANEX_DESC'];
                $i = $i + 1;
                $rs->MoveNext();
            }
        }
        return $datos;
    }

    /**
     * 
     * @return type
     */
    function consultartpDoc() {

        $consulta = "SELECT   t.sgd_tpr_descrip descp,t.sgd_tpr_codigo tp
          FROM  sgd_tpr_tpdcumento t ";

        $rs = $this->link->query($consulta);
        //print_r($rs);

        $datos = array();
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                $datos [$rs->fields ['TP']] = $rs->fields ['DESCP'];
                $rs->MoveNext();
            }
        }
        return $datos;
    }

    function consultarHistorico($radicado) {
        $consulta = "select a.HIST_FECH 
                        , a.DEPE_CODI
			, a.USUA_CODI
			,a.RADI_NUME_RADI
			,a.HIST_OBSE 
			,a.USUA_CODI_DEST
			,a.USUA_DOC
			,a.HIST_OBSE
                          , a.depe_codi_dest
			,t.sgd_ttr_descrip 
			from hist_eventos a,sgd_ttr_transaccion t
		 where 
			a.radi_nume_radi =$radicado  and t.SGD_TTR_CODIGO=a.SGD_TTR_CODIGO
			order by hist_fech desc";
        $rs = $this->link->query($consulta);
        $i = 1;
        $datos = array();
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                foreach ($rs->fields as $key => $value) {
                    $datos [$i] [$key] = $value;
                }
                $i = $i + 1;
                $rs->MoveNext();
            }
        }
        return $datos;
    }

    function consultarDependencia($radicado) {
        $consulta = "select * from dependencia";
        $rs = $this->link->query($consulta);
        $i = 1;
        $datos = array();
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                //foreach ($rs->fields as $key => $value) {
                $datos [$rs->fields['DEPE_CODI']] = $rs->fields['DEPE_NOMB'];
                //}
                $i = $i + 1;
                $rs->MoveNext();
            }
        }
        return $datos;
    }

    function consultarusuarios() {
        $consulta = "select * from usuario";
        $rs = $this->link->query($consulta);
        $i = 1;
        $datos = array();
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                // foreach ($rs->fields as $key => $value) {
                $datos [$rs->fields['USUA_CODI']] = $rs->fields['USUA_NOMB'];
                //}
                $i = $i + 1;
                $rs->MoveNext();
            }
        }
        return $datos;
    }

    function valubi($depto, $muni) {
        $consulta = "select * from municipio where muni_codi=$muni  and dpto_codi=$depto";
        $rs = $this->link->query($consulta);
        $i = 1;
        $datos = 'fail';
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                // foreach ($rs->fields as $key => $value) {
                $datos ='ok';
                //}
                $i = $i + 1;
                $rs->MoveNext();
            }
        }
      //  echo $datos;
        return $datos;
    }
	 
	function consultarOem($parametro){
	    $pieces=explode(" ",$parametro);
            $sentinel=0;
            $where="";
            $retorno['error']="";
            for($i=0;$i<count($pieces);$i++){
                if($sentinel==0 && strlen($pieces[$i])>2){
                    $where.="(upper(sgd_oem_oempresa) like '%".strtoupper($pieces[$i])."%' or upper(sgd_oem_sigla) like '%".strtoupper($pieces[$i])."%' or upper(sgd_oem_rep_legal) like '%".strtoupper($pieces[$i])."%')";
                    $sentinel=1;
                }elseif(strlen($pieces[$i])>2){
                    $where.=" and (upper(sgd_oem_oempresa) like '%".strtoupper($pieces[$i])."%' or upper(sgd_oem_sigla) like '%".strtoupper($pieces[$i])."%' or upper(sgd_oem_rep_legal) like '%".strtoupper($pieces[$i])."%')";
                }
            }
            if($where!=""){
		$sql="select sgd_oem_codigo,tdid_codi, sgd_oem_oempresa,sgd_oem_nit ,sgd_oem_rep_legal, sgd_oem_sigla, sgd_oem_sigla, sgd_oem_direccion, 
		    sgd_oem_telefono, o.muni_codi, o.dpto_codi, o.id_cont, o.id_pais, sgd_oem_email, muni_nomb
		    from sgd_oem_oempresas o,municipio m where $where and m.muni_codi=o.muni_codi and m.dpto_codi=o.dpto_codi order by sgd_oem_oempresa";
                $rs=$this->link->conn->Execute($sql);
                if(!$rs->EOF){
		    $i=0;
                    while(!$rs->EOF){
			$retorno[$i]['id']=$rs->fields['SGD_OEM_CODIGO'];
                    	$retorno[$i]['tdid_codi']=$rs->fields['TDID_CODI'];
                    	$retorno[$i]['nombre']=$rs->fields['SGD_OEM_OEMPRESA'];
                    	$retorno[$i]['rep_legal']=$rs->fields['SGD_OEM_REP_LEGAL'];
                    	$retorno[$i]['sigla']=$rs->fields['SGD_OEM_SIGLA'];
                    	$retorno[$i]['nit']=$rs->fields['SGD_OEM_NIT'];
                    	$retorno[$i]['direccion']=$rs->fields['SGD_OEM_DIRECCION'];;
			$retorno[$i]['telefono']=$rs->fields['SGD_OEM_TELEFONO'];
			$retorno[$i]['email']=$rs->fields['SGD_OEM_EMAIL'];
                        $retorno[$i]['muni_codi']=$rs->fields['MUNI_CODI'];
                        $retorno[$i]['dpto_codi']=$rs->fields['DPTO_CODI'];
                        $retorno[$i]['id_cont']=$rs->fields['ID_CONT'];
                        $retorno[$i]['id_pais']=$rs->fields['ID_PAIS'];
                        $retorno[$i]['municipio']=$rs->fields['MUNI_NOMB'];
                        $i++;
                        $rs->MoveNext();
                    }
                }
                else
                {
                    $retorno['error']="No se encontraron resultado en OEM";
                }
            }
            else{
                $retorno['error']="No se encontraron resultado en OEM";
            }
	    return $retorno;
	}
        /* Funcion encargada de buscar por nombre en la tabla de usuarios del sistema
         * @param parametro parte del nombre del Funcionario
         * 
         * @return array Resultado de la busqueda 
         */
	function consultarFuncionario($parametro){
	    $pieces=explode(" ",$parametro);
            $sentinel=0;
            $where="";
            $retorno['error']="";
            for($i=0;$i<count($pieces);$i++){
                if($sentinel==0 && strlen($pieces[$i])>2){
                    $where.="(upper(usua_nomb) like '%".strtoupper($pieces[$i])."%' or upper(usua_login) like '%".strtoupper($pieces[$i])."%')";
                    $sentinel=1;
                }elseif(strlen($pieces[$i])>2){
                    $where.=" and (upper(usua_nomb) like '%".strtoupper($pieces[$i])."%' or upper(usua_login) like '%".strtoupper($pieces[$i])."%')";
                }
            }
            if($where!=""){
                $sql="select usua_doc AS SGD_CIU_CEDULA , usua_nomb as SGD_CIU_NOMBRE , USUA_DOC AS SGD_CIU_CODIGO , 
 		    U.USUA_EXT AS SGD_CIU_TELEFONO , U.USUA_LOGIN as SGD_CIU_APELL2, sgd_rol_nombre rolnomb,
  		    U.usua_email as SGD_CIU_EMAIL , d.depe_nomb as sgd_ciu_direccion, d.id_cont,d.id_pais,d.dpto_codi,d.muni_codi,m.muni_nomb
		    from usuario u,dependencia d, sgd_urd_usuaroldep urd,sgd_rol_roles r,municipio m
  		    where usua_esta='1' and urd.usua_codi=u.usua_codi and urd.depe_codi=d.depe_codi and r.sgd_rol_id=urd.rol_codi and
                    m.muni_codi=d.muni_codi and m.dpto_codi=d.dpto_codi and $where order by usua_nomb ";
                $rs=$this->link->conn->Execute($sql);
                if(!$rs->EOF){
                    $i=0;
                    while(!$rs->EOF){
                        $retorno[$i]['id']=$rs->fields['SGD_CIU_CODIGO'];
                        $retorno[$i]['tdid_codi']=0;
                        $retorno[$i]['nombre']=$rs->fields['SGD_CIU_NOMBRE'];
                        $retorno[$i]['apell1']='';
			$retorno[$i]['rol']=$rs->fields['ROLNOMB'];
                        $retorno[$i]['apell2']=$rs->fields['SGD_CIU_APELL2'];
                        $retorno[$i]['cedula']=$rs->fields['SGD_CIU_CEDULA'];
                        $retorno[$i]['direccion']=$rs->fields['SGD_CIU_DIRECCION'];
			$retorno[$i]['telefono']=$rs->fields['SGD_CIU_TELEFONO'];
			$retorno[$i]['email']=$rs->fields['SGD_CIU_EMAIL'];
                        $retorno[$i]['muni_codi']=$rs->fields['MUNI_CODI'];
                        $retorno[$i]['dpto_codi']=$rs->fields['DPTO_CODI'];
                        $retorno[$i]['id_cont']=$rs->fields['ID_CONT'];
                        $retorno[$i]['id_pais']=$rs->fields['ID_PAIS'];
                        $retorno[$i]['municipio']=$rs->fields['MUNI_NOMB'];
                        $i++;
                        $rs->MoveNext();
                    }
                }
                else
                {
                    $retorno['error']="No se encontraron resultado en ciudadano";
                }
            }
            else{
                $retorno['error']="No se encontraron resultado en ciudadano";
            }
            return $retorno;
	}
        
	/* Funcion encargada de buscar por documento en la tabla de ciudadano
         * @param parametro parte del nombre del ciudadano
         * 
         * @return array Resultado de la busqueda 
         */
	function consultarDocCiu($nodoc){
	    $sql="select sgd_ciu_codigo,tdid_codi, sgd_ciu_nombre, sgd_ciu_apell1, sgd_ciu_apell2, sgd_ciu_cedula, sgd_ciu_direccion, sgd_ciu_telefono,muni_nomb,
		c.id_pais,c.id_cont,c.dpto_codi,c.muni_codi,c.sgd_ciu_email from sgd_ciu_ciudadano c, municipio m where 
		m.dpto_codi=c.dpto_codi and c.muni_codi=m.muni_codi and sgd_ciu_cedula like '%$nodoc%'";
	    $rs=$this->link->conn->Execute($sql);
	    $retorno['error']="";
	    $i=0;
	    if(!$rs->EOF){
		while(!$rs->EOF){
		    $retorno[$i]['id']=$rs->fields['SGD_CIU_CODIGO'];
                    $retorno[$i]['tdid_codi']=$rs->fields['TDID_CODI'];
                    $retorno[$i]['nombre']=$rs->fields['SGD_CIU_NOMBRE'];
                    $retorno[$i]['apell1']=$rs->fields['SGD_CIU_APELL1'];
                    $retorno[$i]['apell2']=$rs->fields['SGD_CIU_APELL2'];
                    $retorno[$i]['cedula']=$rs->fields['SGD_CIU_CEDULA'];
		    $retorno[$i]['telefono']=$rs->fields['SGD_CIU_TELEFONO'];
		    $retorno[$i]['email']=$rs->fields['SGD_CIU_EMAIL'];
		    $retorno[$i]['municipio']=$rs->fields['MUNI_NOMB'];
                    $retorno[$i]['direccion']=$rs->fields['SGD_CIU_DIRECCION'];
		    $retorno[$i]['muni_codi']=$rs->fields['MUNI_CODI'];
                    $retorno[$i]['dpto_codi']=$rs->fields['DPTO_CODI'];
                    $retorno[$i]['id_cont']=$rs->fields['ID_CONT'];
                    $retorno[$i]['id_pais']=$rs->fields['ID_PAIS'];
		    $i++;
		    $rs->MoveNext();
		}
	    }
	    else
	    {
		$retorno['error']="No se encontraron resultado en ciudadano";
	    }
	    return $retorno;
	}

        /* Funcion encargadad de buscar por nombre en la tabla de OEM
         * @param parametro parte del nit de oem 
         * 
         * @return array Resultado de la busqueda 
         */
	function consultarDocOem($nodoc){
	    $sql="select sgd_oem_codigo,tdid_codi, sgd_oem_oempresa,sgd_oem_nit ,sgd_oem_rep_legal, sgd_oem_sigla, sgd_oem_sigla, sgd_oem_direccion, sgd_oem_telefono,
		muni_nomb, oem.dpto_codi, oem.muni_codi, oem.id_pais, oem.id_cont, oem.sgd_oem_email from sgd_oem_oempresas oem, 
		municipio m where m.dpto_codi=oem.dpto_codi and oem.muni_codi=m.muni_codi and sgd_oem_nit like '%$nodoc%'";
            $rs=$this->link->conn->Execute($sql);
            $retorno['error']="";
            $i=0;
            if(!$rs->EOF){
                while(!$rs->EOF){
                    $retorno[$i]['id']=$rs->fields['SGD_OEM_CODIGO'];
                    $retorno[$i]['tdid_codi']=$rs->fields['TDID_CODI'];
                    $retorno[$i]['nombre']=$rs->fields['SGD_OEM_OEMPRESA'];
                    $retorno[$i]['rep_legal']=$rs->fields['SGD_OEM_REP_LEGAL'];
                    $retorno[$i]['sigla']=$rs->fields['SGD_OEM_SIGLA'];
                    $retorno[$i]['nit']=$rs->fields['SGD_OEM_NIT'];
		    $retorno[$i]['municipio']=$rs->fields['MUNI_NOMB'];
                    $retorno[$i]['direccion']=$rs->fields['SGD_OEM_DIRECCION'];
		    $retorno[$i]['telefono']=$rs->fields['SGD_OEM_TELEFONO'];
		    $retorno[$i]['email']=$rs->fields['SGD_OEM_EMAIL'];
                    $retorno[$i]['muni_codi']=$rs->fields['MUNI_CODI'];
                    $retorno[$i]['dpto_codi']=$rs->fields['DPTO_CODI'];
                    $retorno[$i]['id_cont']=$rs->fields['ID_CONT'];
                    $retorno[$i]['id_pais']=$rs->fields['ID_PAIS'];
                    $i++;
                    $rs->MoveNext();
                }
            }
            else
            {
                $retorno['error']="No se encontraron resultado en empresas";
            }
            return $retorno;
	}
	/*Funcion encargada de traer datos de la tabla de usuario
         * @param parametro parte del documento del usuario del sistema
         * 
         * @return array Resultado de la busqueda 
         */
	function consultarDocFun($nodoc){
	    $sql="select usua_doc AS SGD_CIU_CEDULA , usua_nomb as SGD_CIU_NOMBRE , USUA_DOC AS SGD_CIU_CODIGO , 
 		U.USUA_EXT AS SGD_CIU_TELEFONO , r.sgd_rol_nombre as rolnomb,U.USUA_LOGIN as SGD_CIU_APELL2, d.ID_CONT , d.ID_PAIS , 
 		MUNI_NOMB, U.usua_email as SGD_CIU_EMAIL , d.depe_nomb as sgd_ciu_direccion, d.muni_codi,d.id_cont,d.id_pais,d.dpto_codi
		from sgd_rol_roles r, usuario u,dependencia d, 
		sgd_urd_usuaroldep urd, municipio m where usua_esta='1' and urd.usua_codi=u.usua_codi and urd.depe_codi=d.depe_codi and 
		urd.rol_codi=r.sgd_rol_id and usua_doc like '%$nodoc%' and m.muni_codi=d.muni_codi and d.dpto_codi=m.dpto_codi";
	    $rs=$this->link->conn->Execute($sql);
            $retorno['error']="";
            $i=0;
            if(!$rs->EOF){
                while(!$rs->EOF){
                    $retorno[$i]['id']=$rs->fields['SGD_CIU_CODIGO'];
                    $retorno[$i]['tdid_codi']=$rs->fields['TDID_CODI'];
                    $retorno[$i]['nombre']=$rs->fields['SGD_CIU_NOMBRE'];
                    $retorno[$i]['rol']=$rs->fields['ROLNOMB'];
		    $retorno[$i]['apell1']='';
                    $retorno[$i]['apell2']=$rs->fields['SGD_CIU_APELL2'];
                    $retorno[$i]['cedula']=$rs->fields['SGD_CIU_CEDULA'];
		    $retorno[$i]['telefono']=$rs->fields['SGD_CIU_TELEFONO'];
		    $retorno[$i]['email']=$rs->fields['SGD_CIU_EMAIL'];
		    $retorno[$i]['muni_codi']=$rs->fields['MUNI_CODI'];
                    $retorno[$i]['dpto_codi']=$rs->fields['DPTO_CODI'];
                    $retorno[$i]['id_cont']=$rs->fields['ID_CONT'];
                    $retorno[$i]['id_pais']=$rs->fields['ID_PAIS'];
                    $retorno[$i]['municipio']=$rs->fields['MUNI_NOMB'];
                    $retorno[$i]['direccion']=$rs->fields['SGD_CIU_DIRECCION'];
                    $i++;
                    $rs->MoveNext();
                }
            }
            else
            {
                $retorno['error']="No se encontraron resultado en ciudadano";
            }
            return $retorno;
	}

        /* Funcion encargada de buscar por el tipo de tercero los radicados asociados en un rango de tiempo
         * @param parametro parte del nombre del ciudadano
         * @param id Id del tercero a consultar
         * @param tercero Tipo del tercero a consultar del radicado
         * @param fecha_ini Fecha inicial de intervalo a buscar
         * @param fecha_fin Fecha final de intervalo a buscar
         * 
         * @return array Resultado de la busqueda 
         */
	function dataTerceroRad($id,$tercero,$fecha_ini,$fecha_fin){
	    if($tercero==0){
		$where="d.sgd_ciu_codigo=$id";
	    }
	    elseif($tercero==1)
	    {
		$where="d.sgd_oem_codigo=$id";	
	    }
	    else{
		$where="d.sgd_doc_fun='$id'";
	    }
	    $sql="select r.radi_nume_radi as numrad, to_char(r.radi_fech_radi,'YYYY-MM-DD HH:II:SS') as fecha, r.ra_asun from radicado r, sgd_dir_drecciones d where 
		d.sgd_dir_tipo=1 and $where and d.radi_nume_radi=r.radi_nume_radi and r.radi_fech_radi between to_timestamp('$fecha_ini 0:00:00','dd/mm/yyyy hh24:mi:ss') and 
		to_timestamp('$fecha_fin 23:59:59','dd/mm/yyyy hh24:mi:ss') order by fecha desc";
	    $rs=$this->link->conn->Execute($sql);
	    $retorno['error']="";
	    $i=0;
	    if(!$rs->EOF){
		while(!$rs->EOF){
		    $retorno[$i]['numrad']=$rs->fields['NUMRAD'];
                    $retorno[$i]['fecha']=$rs->fields['FECHA'];
                    $retorno[$i]['asunto']=$rs->fields['RA_ASUN'];
		    $i++;
		    $rs->MoveNext();
		}
	    }
	    else{
		$retorno['error']='No se encontraron radicados';
	    }
	    return $retorno;
	}

        /* Funcion encargada consultar en base de datos de paises relacionados con un continente
         * @param continente El id del continente 
         * 
         * @return array Resultado de la busqueda 
         */
	function listarPaises($continente){
	    $sql="select id_pais, nombre_pais from sgd_def_paises where id_cont=$continente order by nombre_pais";
	    $rs=$this->link->conn->Execute($sql);
	    $i=0;
	    $retorno['error']="";
            if(!$rs->EOF){
		while(!$rs->EOF){
		    $retorno[$i]['id']=$rs->fields['ID_PAIS'];
		    $retorno[$i]['pais']=$rs->fields['NOMBRE_PAIS'];
		    $i++;
		    $rs->MoveNext();
		}
	    }
	    else{
		$retorno['error']="No tiene paises este continente";
	    }
	    return $retorno;
	}

        /* Funcion encargada de listar Continentes en la base de datos
         * 
         * @return array Resultado de la busqueda 
         */
	function listarContinentes(){
	    $sql="select id_cont, nombre_cont from sgd_def_continentes order by nombre_cont";
            $rs=$this->link->conn->Execute($sql);
            $i=0;
            while(!$rs->EOF){
                $retorno[$i]['id']=$rs->fields['ID_CONT'];
                $retorno[$i]['continente']=$rs->fields['NOMBRE_CONT'];
                $i++;
                $rs->MoveNext();
            }
            return $retorno;
	}
        /* Funcion encargada consultar en base de datos de departamentos relacionados con un pais
         * @param continente El id del continente 
         * 
         * @return array Resultado de la busqueda 
         */
	function listarDpto($pais){
            $sql="select dpto_codi, dpto_nomb from departamento where id_pais=$pais order by dpto_nomb";
            $rs=$this->link->conn->Execute($sql);
            $i=0;
            $retorno['error']="";
            if(!$rs->EOF){
                while(!$rs->EOF){
                    $retorno[$i]['id']=$rs->fields['DPTO_CODI'];
                    $retorno[$i]['dpto']=$rs->fields['DPTO_NOMB'];
                    $i++;
                    $rs->MoveNext();
                }
            }
            else{
                $retorno['error']="No tiene departamentos este pais";
            }
            return $retorno;
        }
        /* Funcion encargada consultar en base de datos de municipio relacionados con un departamento y pais
         * @param pais El id del pais
         * @param dpto El codigo del departamento
         * 
         * @return array Resultado de la busqueda 
         */
	function listarMunicipio($pais,$departamento){
            $sql="select muni_codi, muni_nomb from municipio where id_pais=$pais and dpto_codi=$departamento order by muni_nomb";
            $rs=$this->link->conn->Execute($sql);
            $i=0;
            $retorno['error']="";
            if(!$rs->EOF){
                while(!$rs->EOF){
                    $retorno[$i]['id']=$rs->fields['MUNI_CODI'];
                    $retorno[$i]['muni']=$rs->fields['MUNI_NOMB'];
                    $i++;
                    $rs->MoveNext();
                }
            }
            else{
                $retorno['error']="No tiene municipios este departamento";
            }
            return $retorno;
        }

        /*Funcion encargada de insertar datos de un ciudadano nuevo
         * 
         * @return int ciu El codigo unico de ciudadano del nuevo registro
         */
	function insertarCiu($param1,$param2,$param3,$param4,$param5,$param6,$param7,$continente,$pais,$dpto,$muni){
		$data="";
		$cabeza="";
		//inserta ciudadano
       if ($param1) {
            $data.=",'$param1'";
            $cabeza.=",sgd_ciu_cedula";
        }
        if ($param6) {
            $data.=",'$param6'";
            $cabeza.=",sgd_ciu_telefono";
        }
        if ($param4) {
            $data.=",'" . mb_strtoupper($param4) . "'";
            $cabeza.=",sgd_ciu_apell2";
        }
        if ($param7) {
            $data.=",'" . $param7 . "'";
            $cabeza.=",sgd_ciu_email";
        }
		$num_ciu = $this->link->conn->GenID('SEC_CIU_CIUDADANO');
        $ins_ciu = "insert into sgd_ciu_ciudadano "
                . "(tdid_codi,sgd_ciu_codigo,sgd_ciu_nombre,sgd_ciu_direccion,sgd_ciu_apell1,muni_codi,dpto_codi,id_cont,id_pais $cabeza)"
                . "values(1," . $num_ciu . ",'" . mb_strtoupper($param2) . "','" . mb_strtoupper($param5) . "','" . mb_strtoupper($param3) . "'," . $muni . "," . $dpto . ", $continente,$pais $data)";
        //$ins_ciu2 = utf8_encode($ins_ciu);
        $this->link->conn->Execute($ins_ciu);
        return $num_ciu;
	}
        /*Funcion encargada de insertar datos de un oem nuevo
         * 
         * @return int oem El codigo unico de oem del nuevo registro
         */
	function insertarOem($param1,$param2,$param3,$param4,$param5,$param6,$param7,$continente,$pais,$dpto,$muni){
		$data="";
		$cabeza="";
        if ($param1) {
            $data.=",'$param1'";
            $cabeza.=",SGD_OEM_NIT";
        }
        if ($param6) {
            $data.=",'$param6'";
            $cabeza.=",SGD_OEM_TELEFONO";
        }
		if ($param4) {
            $data.=",'" . strtoupper($param4) . "'";
            $cabeza.=",sgd_oem_rep_legal";
        }
        if ($param7) {
            $data.=",'" . $param7 . "'";
            $cabeza.=",sgd_oem_email";
        }
		$nextval = $this->link->nextId("sec_oem_oempresas");
        $isql = "INSERT INTO SGD_OEM_OEMPRESAS( tdid_codi, SGD_OEM_codigo, SGD_OEM_OEMPRESA, SGD_OEM_DIRECCION, 
				SGD_OEM_SIGLA,  ID_CONT, ID_PAIS, DPTO_CODI, MUNI_CODI $cabeza) 
				values (4, $nextval, '".mb_strtoupper($param2)."', '".mb_strtoupper($param5)."', '".mb_strtoupper($param3)."', 
						$continente, $pais, $dpto, $muni $data)";
        //$ins_ciu2 = utf8_encode($isql);
        $rs_ins_ciu = $this->link->conn->Execute($isql);
        return $nextval;
        }

 	/*function traerDCiu($param1,$param2,$param5,$continente,$pais,$dpto,$muni){
	    $sql="select sgd_ciu_codigo,tdid_codi,sgd_ciu_cedula,sgd_ciu_nombre,sgd_ciu_apell1,sgd_ciu_apell2,sgd_ciu_direccion,sgd_ciu_telefono,sgd_ciu_email,id_cont,
		id_pais,muni_codi,dpto_codi from sgd_ciu_ciudadano where sgd_ciu_cedula='$param1' and sgd_ciu_nombre='$param2' and sgd_ciu_direccion='$param5'
		and id_cont=$continente and id_pais=$pais and dpto_codi=$dpto and muni_codi=$muni";
	    $rs=$this->link->conn->Execute($sql);
            $retorno['error']="";
            if(!$rs->EOF){
		$retorno['tipo']=0;
                $retorno['id']=$rs->fields['SGD_CIU_CODIGO'];
                $retorno['tdid_codi']=$rs->fields['TDID_CODI'];
                $retorno['param1']=$rs->fields['SGD_CIU_CEDULA'];
                $retorno['param2']=$rs->fields['SGD_CIU_NOMBRE'];
                $retorno['param3']=$rs->fields['SGD_CIU_APELL1'];
                $retorno['param4']=$rs->fields['SGD_CIU_APELL2'];
                $retorno['param5']=$rs->fields['SGD_CIU_DIRECCION'];
                $retorno['param6']=$rs->fields['SGD_CIU_TELEFONO'];
                $retorno['param7']=$rs->fields['SGD_CIU_EMAIL'];
		$retorno['dpto']=$rs->fields['DPTO_CODI'];
		$retorno['muni']=$rs->fields['MUNI_CODI'];
		$retorno['pais']=$rs->fields['ID_PAIS'];
		$retorno['conti']=$rs->fields['ID_CONT'];
            }
            return $retorno;
	}*/
        /*Funcion encargada de consultar datos de un ciudadano con datos a usar en un radicado
         * 
         * @param int ciu El codigo unico de ciudadano del nuevo registro
         * @return array El resultado de ciudadano buscado
         */
	function traerDCiuA($id){
            $sql="select sgd_ciu_codigo,tdid_codi,sgd_ciu_cedula,sgd_ciu_nombre,sgd_ciu_apell1,sgd_ciu_apell2,sgd_ciu_direccion,sgd_ciu_telefono,sgd_ciu_email,id_cont,
                id_pais,muni_codi,dpto_codi from sgd_ciu_ciudadano where sgd_ciu_codigo=$id";
            $rs=$this->link->conn->Execute($sql);
            $retorno['error']="";
            if(!$rs->EOF){
                $retorno['tipo']=0;
                $retorno['id']=$rs->fields['SGD_CIU_CODIGO'];
                $retorno['tdid_codi']=$rs->fields['TDID_CODI'];
                $retorno['param1']=$rs->fields['SGD_CIU_CEDULA'];
                $retorno['param2']=$rs->fields['SGD_CIU_NOMBRE'];
                $retorno['param3']=$rs->fields['SGD_CIU_APELL1'];
                $retorno['param4']=$rs->fields['SGD_CIU_APELL2'];
                $retorno['param5']=$rs->fields['SGD_CIU_DIRECCION'];
                $retorno['param6']=$rs->fields['SGD_CIU_TELEFONO'];
                $retorno['param7']=$rs->fields['SGD_CIU_EMAIL'];
                $retorno['dpto']=$rs->fields['DPTO_CODI'];
                $retorno['muni']=$rs->fields['MUNI_CODI'];
                $retorno['pais']=$rs->fields['ID_PAIS'];
                $retorno['conti']=$rs->fields['ID_CONT'];
            }
            return $retorno;
        }

	/*function traerDOem($param1,$param2,$param5,$continente,$pais,$dpto,$muni){
	    $sql="select sgd_oem_codigo,tdid_codi,sgd_oem_nit,sgd_oem_oempresa,sgd_oem_sigla,sgd_oem_rep_legal,sgd_oem_direccion,
	    sgd_oem_telefono,sgd_oem_email,id_cont,id_pais ,dpto_codi,muni_codi from sgd_oem_oempresas where sgd_oem_nit='$param1' and sgd_oem_oempresa='$param2'
	    and id_pais=$pais and id_cont=$continente and muni_codi=$muni and dpto_codi=$dpto";
	    $rs=$this->link->conn->Execute($sql);
	    $retorno['error']="";
	    if(!$rs->EOF){
		$retorno['tipo']=1;
		$retorno['id']=$rs->fields['SGD_OEM_CODIGO'];
		$retorno['tdid_codi']=$rs->fields['TDID_CODI'];
		$retorno['param1']=$rs->fields['SGD_OEM_NIT'];
		$retorno['param2']=$rs->fields['SGD_OEM_OEMPRESA'];
                $retorno['param3']=$rs->fields['SGD_OEM_SIGLA'];
                $retorno['param4']=$rs->fields['SGD_OEM_REP_LEGAL'];
                $retorno['param5']=$rs->fields['SGD_OEM_DIRECCION'];
                $retorno['param6']=$rs->fields['SGD_OEM_TELEFONO'];
                $retorno['param7']=$rs->fields['SGD_OEM_EMAIL'];
		$retorno['dpto']=$rs->fields['DPTO_CODI'];
                $retorno['muni']=$rs->fields['MUNI_CODI'];
                $retorno['pais']=$rs->fields['ID_PAIS'];
                $retorno['conti']=$rs->fields['ID_CONT'];
	    }
	    return $retorno;
	}*/
        /*Funcion encargada de consultar datos de un OEM con datos a usar en un radicado
         * 
         * @param int oem El codigo unico de OEM del nuevo registro
         * 
         * @return array Los datos de busqueda
         */
        
	function traerDOemA($id){
            $sql="select sgd_oem_codigo,tdid_codi,sgd_oem_nit,sgd_oem_oempresa,sgd_oem_sigla,sgd_oem_rep_legal,sgd_oem_direccion,
            sgd_oem_telefono,sgd_oem_email,id_cont,id_pais ,dpto_codi,muni_codi from sgd_oem_oempresas where sgd_oem_codigo=$id";
            $rs=$this->link->conn->Execute($sql);
            $retorno['error']="";
            if(!$rs->EOF){
                $retorno['tipo']=1;
                $retorno['id']=$rs->fields['SGD_OEM_CODIGO'];
                $retorno['tdid_codi']=$rs->fields['TDID_CODI'];
                $retorno['param1']=$rs->fields['SGD_OEM_NIT'];
                $retorno['param2']=$rs->fields['SGD_OEM_OEMPRESA'];
                $retorno['param3']=$rs->fields['SGD_OEM_SIGLA'];
                $retorno['param4']=$rs->fields['SGD_OEM_REP_LEGAL'];
                $retorno['param5']=$rs->fields['SGD_OEM_DIRECCION'];
                $retorno['param6']=$rs->fields['SGD_OEM_TELEFONO'];
                $retorno['param7']=$rs->fields['SGD_OEM_EMAIL'];
                $retorno['dpto']=$rs->fields['DPTO_CODI'];
                $retorno['muni']=$rs->fields['MUNI_CODI'];
                $retorno['pais']=$rs->fields['ID_PAIS'];
                $retorno['conti']=$rs->fields['ID_CONT'];
            }
            return $retorno;
        }
        /*Funcion encargada de consultar datos de un funcionario con datos a usar en un radicado
         * 
         * @return int ciu El codigo unico de usuario del sistema
         */
	function traerDFunA($id){
            $sql="select usua_doc AS SGD_CIU_CEDULA , usua_nomb as SGD_CIU_NOMBRE , USUA_DOC AS SGD_CIU_CODIGO , 
		U.USUA_EXT AS SGD_CIU_TELEFONO , U.USUA_LOGIN as SGD_CIU_APELL2, d.ID_CONT , d.ID_PAIS , 
 		d.DPTO_CODI as DPTO_CODI , d.MUNI_CODI as MUNI_CODI , U.usua_email as SGD_CIU_EMAIL , d.depe_nomb as DEPENOMB 
 		from usuario u,dependencia d, sgd_urd_usuaroldep urd where usua_esta='1' and urd.usua_codi=u.usua_codi and urd.depe_codi=d.depe_codi and 
		usua_doc='$id'";
            $rs=$this->link->conn->Execute($sql);
            $retorno['error']="";
            if(!$rs->EOF){
                $retorno['tipo']=2;
                $retorno['id']=$id;
                $retorno['tdid_codi']=0;
                $retorno['param1']=$rs->fields['SGD_CIU_CEDULA'];
                $retorno['param2']=$rs->fields['SGD_CIU_NOMBRE'];
                $retorno['param3']='';
                $retorno['param4']=$rs->fields['SGD_CIU_APELL2'];
                $retorno['param5']=$rs->fields['DEPENOMB'];
                $retorno['param6']=$rs->fields['SGD_CIU_TELEFONO'];
                $retorno['param7']=$rs->fields['SGD_CIU_EMAIL'];
                $retorno['dpto']=$rs->fields['DPTO_CODI'];
                $retorno['muni']=$rs->fields['MUNI_CODI'];
                $retorno['pais']=$rs->fields['ID_PAIS'];
                $retorno['conti']=$rs->fields['ID_CONT'];
            }
	    else{
		$retorno['error']="Este usuario no existe o esta inactivo";
	    }
            return $retorno;
        }
        /*Funcion encargada de consultar datos de un ciudadano con datos a usar en un radicado
         * 
         * @return int ciu El codigo unico de ciudadano del nuevo registro
         */
	function traerDatosRad($numrad){
	    $sql="select r.tdid_codi,d.sgd_ciu_codigo,d.sgd_oem_codigo,to_char(r.radi_fech_radi,'dd-mm-yyyy') fechar,d.sgd_dir_doc,d.sgd_doc_fun,sgd_dir_direccion,
		r.radi_desc_anex,r.mrec_codi,d.sgd_dir_nomremdes,r.tdoc_codi,d.sgd_dir_nombre,r.ra_asun,d.id_cont,d.id_pais,d.dpto_codi,d.muni_codi,r.radi_depe_radi from radicado r,
		sgd_dir_drecciones d where r.radi_nume_radi=d.radi_nume_radi and r.radi_nume_radi=$numrad";
	    $rs=$this->link->conn->Execute($sql);
	    $ciu_codi=$rs->fields['SGD_CIU_CODIGO'];
	    $oem_codi=$rs->fields['SGD_OEM_CODIGO'];
	    $fun=$rs->fields['SGD_DOC_FUN'];
	    if($ciu_codi==0 && $oem_codi==0 && ($fun!='' || $fun!=0)){
		$retorno['id']=$fun;
		$dataFun=$this->traerDFunA($fun);
		$retorno['param1']=$dataFun['param1'];
		$nom_r=$dataFun['param2'];
                $retorno['param2']=$dataFun['param2'];
                $retorno['param3']=$dataFun['param3'];
                $retorno['param4']=$dataFun['param4'];
                $retorno['param5']=$dataFun['param5'];
                $retorno['param6']=$dataFun['param6'];
                $retorno['param7']=$dataFun['param7'];
                $retorno['tipo']=2;
            }
	    elseif($oem_codi!=0){
		$dataOem=$this->traerDOemA($oem_codi);
		$retorno['param1']=$dataOem['param1'];
                $retorno['param2']=$dataOem['param2'];
                $retorno['param3']=$dataOem['param3'];
                $retorno['param4']=$dataOem['param4'];
                $retorno['param5']=$dataOem['param5'];
                $retorno['param6']=$dataOem['param6'];
                $retorno['param7']=$dataOem['param7'];
		$nom_r=$dataOem['param2'];
		$retorno['id']=$oem_codi;
		$retorno['tipo']=1;
	    }
	    elseif($ciu_codi!=0){
		$retorno['id']=$ciu_codi;
                $retorno['tipo']=0;
		$dataCiu=$this->traerDCiuA($ciu_codi);
		$retorno['param1']=$dataCiu['param1'];
                $retorno['param2']=$dataCiu['param2'];
                $retorno['param3']=$dataCiu['param3'];
                $retorno['param4']=$dataCiu['param4'];
                $retorno['param5']=$dataCiu['param5'];
                $retorno['param6']=$dataCiu['param6'];
                $retorno['param7']=$dataCiu['param7'];
	    }
	    else{
		$retorno['id']=0;
                $retorno['tipo']=0;
		$retorno['param1']='';
                $retorno['param2']='';
                $retorno['param3']='';
                $retorno['param4']='';
                $retorno['param5']='';
                $retorno['param6']='';
                $retorno['param7']='';
	    }
	    
	    $retorno['tdid_codi']=$rs->fields['TDID_CODI'];
	    if(isset($nom_r)){
		$retorno['nom_r']=$nom_r;
	    }
	    else{
            	$retorno['nom_r']=$rs->fields['SGD_DIR_NOMREMDES'];
	    }
	    $retorno['param5']=$rs->fields['SGD_DIR_DIRECCION'];
            $retorno['param8']=$rs->fields['SGD_DIR_NOMBRE'];
	    $retorno['param9']=$rs->fields['RA_ASUN'];
	    $retorno['mrec']=$rs->fields['MREC_CODI'];
            $retorno['descanex']=$rs->fields['RADI_DESC_ANEX'];
	    $retorno['dpto']=$rs->fields['DPTO_CODI'];
            $retorno['muni']=$rs->fields['MUNI_CODI'];
            $retorno['pais']=$rs->fields['ID_PAIS'];
            $retorno['conti']=$rs->fields['ID_CONT'];
	    $retorno['trd']=$rs->fields['TDOC_CODI'];
	    $retorno['fechar']=$rs->fields['FECHAR'];
        $retorno['radi_depe_radi']=$rs->fields['RADI_DEPE_RADI'];
	    return $retorno;
	}
        /* Funcion que lista de la base de datos los medios de recepcion
         * 
         * @return array Listado de los medios de recepcion activos
         */
	function getMediorec(){
	    $sql="Select MREC_DESC as \"desc\", MREC_CODI as \"codi\" from MEDIO_RECEPCION";
	    $rs=$this->link->conn->Execute($sql);
	    $i=0;
	    while(!$rs->EOF){
		$resultado[$i]['codi']=$rs->fields['codi'];
		$resultado[$i]['desc']=$rs->fields['desc'];
		$i++;
		$rs->MoveNext();
	    }
	    //print_r($resultado);
	    return $resultado;
	}

        /* Funcion encargada de listar las dependencias de recepcion para entrada
         * 
         * @return array Listados de dependencias activas y con Jefes asignados activos
         * 
         */
	function depeRec(){
	    $sql="SELECT d.depe_codi||' - '||d.depe_nomb as depe, d.depe_codi, case when drm.sgd_drm_valor is null then 0 else 1 end as nivseg FROM dependencia d, 
	        sgd_urd_usuaroldep ur left outer join sgd_drm_dep_mod_rol drm on (ur.rol_codi=drm.sgd_drm_rolcodi and ur.depe_codi=drm.sgd_drm_depecodi and 
	        sgd_drm_modecodi=40) WHERE d.depe_codi = ur.depe_codi AND ur.rol_codi = 1 and ur.depe_codi!=999 order by d.depe_codi";
	    $rs=$this->link->conn->Execute($sql);
	    //$retorno['error']="";
	    $i=0;
	    while(!$rs->EOF){
		$retorno[$i]['depe']=$rs->fields['DEPE'];
		$retorno[$i]['depe_codi']=$rs->fields['DEPE_CODI'];
                $retorno[$i]['nivseg']=$rs->fields['NIVSEG'];
		$i++;
	        $rs->MoveNext();
	    }
	    return $retorno;
	}
        /* Funcion encargada de buscar la dependencia de usuario funcional para destinar documentos
         * 
         * @return array Listado de dependencia de usuario
         */
	function depeDes($depus,$rolus){
            $sql="SELECT d.depe_codi||' - '||d.depe_nomb as depe, d.depe_codi, case when drm.sgd_drm_valor is null then 0 else 1 end as nivseg FROM dependencia d,
                sgd_urd_usuaroldep ur left outer join sgd_drm_dep_mod_rol drm on (drm.sgd_drm_rolcodi=$rolus and ur.depe_codi=drm.sgd_drm_depecodi and
                sgd_drm_modecodi=40) WHERE d.depe_codi = ur.depe_codi AND ur.rol_codi = 1 and ur.depe_codi=$depus and ur.depe_codi!=999 order by d.depe_codi";
            $rs=$this->link->conn->Execute($sql);
            //$retorno['error']="";
            $i=0;
            while(!$rs->EOF){
                $retorno[$i]['depe']=$rs->fields['DEPE'];
                $retorno[$i]['depe_codi']=$rs->fields['DEPE_CODI'];
                $retorno[$i]['nivseg']=$rs->fields['NIVSEG'];
                $i++;
                $rs->MoveNext();
            }
            return $retorno;
        }
        /* Funcion encargada de consultar datos de usuario jefe de una dependencia
         * 
         * @param numeric Dependencia a consultar jefe
         * 
         * @return array Informacion retorno consultada
         */
	function jefeRad($depus){
	    $sql="select ur.usua_codi,u.usua_doc, u.usua_email,usua_login,depe_nomb from sgd_urd_usuaroldep ur, usuario u, dependencia d where ur.depe_codi=$depus 
		and ur.depe_codi=d.depe_codi and ur.rol_codi=1 and u.usua_codi=ur.usua_codi";
	    $rs=$this->link->conn->Execute($sql);
	    $usua_codi=$rs->fields['USUA_CODI'];
	    $usua_doc=$rs->fields['USUA_DOC'];
	    $usua_login=$rs->fields['USUA_LOGIN'];
	    $depe_nomb=$rs->fields['DEPE_NOMB'];
	    $usua_email=$rs->fields['USUA_EMAIL'];
	    $retorno=array($usua_codi,$usua_doc,$usua_login,$depe_nomb,$usua_email);
	    return $retorno;
	}
        /* funcion encargada de mostrar los informados en radicacion
         * 
         * @param numeric Numero de radicado a consultar informados
         * 
         * @return array Datos de informados sobre radicado
         */
	function verInformados($numrad){
            $sql="select info_desc,informados.depe_codi,informados.info_fech as fecha,depe_nomb from informados,dependencia 
		where radi_nume_radi=$numrad and dependencia.depe_codi=informados.depe_codi order by info_fech desc";
            $rs=$this->link->conn->Execute($sql);
            $retorno['error']="";
            $i=0;
            if(!$rs->EOF){
                while(!$rs->EOF){
                    $retorno[$i]['depe_codi']=$rs->fields['DEPE_CODI'];
                    $retorno[$i]['depe_nomb']=$rs->fields['DEPE_NOMB'];
                    $retorno[$i]['data1']=$rs->fields['INFO_DESC'];
                    $retorno[$i]['data2']=$rs->fields['FECHA'];
                    $retorno[$i]['data3']=date("dMy",$rs->fields['FECHA']);
                    $retorno[$i]['data4']=$this->link->conn->UnixTimeStamp( $rs->fields['INFO_DESC'] );
                    $retorno[$i]['data5']=date( "d-m-Y", $this->link->conn->UnixTimeStamp( $rs->fields['INFO_FECH'] ) );
                    $i++;
                    $rs->MoveNext();
                }
            }
            else{
               $retorno['error']="No se encontro datos de informados";
            }
            return $retorno;
        }
        /*
         * Funcion encargada de insertar los informados a la base de datos
         * 
         * @param array radicados Los radicados a informar
         * @param string loginOrigen El login de usuario que realiza la accion
         * @param string loginDest El login de usuario sobre el que recae la accion
         * @param numeric depOrigen La dependencia del usuario origen
         * @param numeric depDest La dependencia del usuario destino
         * @param int codUsOrigen El codigo de usuario origen
         * @param int codUsDest El codigo del usuario destino
         * @param string docUsOrigen El documento del usuario origen
         * @param string docUsDest El documento del usuario destino
         * @param string observa La observacion hecha sobre el radicado informado
         * @param int id_rol El id rol de usuario origen
         * @param int rol_dest El id rol del usuario destino
         * 
         * 
         */
	function informar($radicados, $loginOrigen, $loginDest , $depOrigen, $depDest, $codUsOrigen, $codUsDest,$docUsOrigen,$docUsDest,$observa, $id_rol,$rolDest){
            $fechainfo = date('Y-m-d H:i:s');
            $observa="A: $loginDest $observa";
            foreach($radicados as $rad){
                $record["RADI_NUME_RADI"]=$rad;
                $record["DEPE_CODI"]=$depDest;
                $record["USUA_CODI"]=$codUsOrigen;
                $record["INFO_DESC"]="'$observa'";
                $record["USUA_DOC"]="'$docUsDest'";
                $record["INFO_CODI"]="NULL";
                $record["ID_ROL"]=$rolDest;
                $record["INFO_FECH"]="to_timestamp('$fechainfo','yyyy-mm-dd hh24:mi:ss')";
                $resul=$this->link->conn->Replace("INFORMADOS",$record,array('RADI_NUME_RADI', 'INFO_CODI', 'USUA_DOC'), false);
                //$this->registrarNovedad('NOV_INFOR',$docUsDest,$rad);
		$tx=8;
            $ins_his = "insert into hist_eventos (depe_codi,hist_fech,usua_codi,radi_nume_radi,hist_obse,usua_codi_dest,usua_doc,sgd_ttr_codigo,hist_doc_dest,
                depe_codi_dest,id_rol,id_rol_dest)
                values($depOrigen,current_timestamp,$codUsOrigen,$rad,'$observa'," . $codUsDest . ",'$docUsOrigen',$tx,'" .$docUsDest. "',".$depDest.",$id_rol,$rolDest)";
                $rs_ins_his = $this->link->conn->Execute($ins_his);
            }
        }
        /*
         * Funcion encargada de borrar los informados en radicacion
         * 
         * @param array radicados Los radicados a informar
         * @param string loginOrigen El login de usuario que realiza la accion
         * @param string loginDest El login de usuario sobre el que recae la accion
         * @param numeric depOrigen La dependencia del usuario origen
         * @param numeric depDest La dependencia del usuario destino
         * @param int codUsOrigen El codigo de usuario origen
         * @param int codUsDest El codigo del usuario destino
         * @param string docUsOrigen El documento del usuario origen
         * @param string docUsDest El documento del usuario destino
         * @param string observa La observacion hecha sobre el informado borrado
         * @param int id_rol El id rol de usuario origen
         * @param int rol_dest El id rol del usuario destino
         * 
         * 
         */
	function borrarInformado($rad, $depOrigen, $depDest, $codUsOrigen, $codUsDest,$docUsOrigen,$docUsDest,$observa, $id_rol,$rolDest){
	    $sql="delete from informados where radi_nume_radi=$rad and depe_codi=$depDest";
	    $this->link->conn->Execute($sql);
	    $tx=8;
            $ins_his = "insert into hist_eventos (depe_codi,hist_fech,usua_codi,radi_nume_radi,hist_obse,usua_codi_dest,usua_doc,sgd_ttr_codigo,hist_doc_dest,
                depe_codi_dest,id_rol,id_rol_dest)
                values($depOrigen,current_timestamp,$codUsOrigen,$rad,'$observa'," . $codUsDest . ",'$docUsOrigen',$tx,'" .$docUsDest. "',".$depDest.",$id_rol,$rolDest)";
            $rs_ins_his = $this->link->conn->Execute($ins_his);
	}

	function genCodiVerifica($trad){
	    $sql="select sgd_trad_gencodverif from sgd_trad_tiporad where sgd_trad_codigo=$trad";
	    $rs=$this->link->conn->Execute($sql);
	    if(!$rs->EOF){
		$retorno=$rs->fields['SGD_TRAD_GENCODVERIF'];
	    }
	    else{
		$retorno=0;
	    }
	    return $retorno;
	}

	function agregFactura($numerad,$tipofac,$remfact,$descfact){
	    $sql="insert into radi_factura (radi_nume_radi,no_factura,factu_prefijo,factu_valor) values ($numerad,'$tipofac','$remfact','$descfact')";
	    $this->link->conn->Execute($sql);
	}

	function anexosTipo(){
	    $sql="select * from anexos_tipo order by anex_tipo_codi";
	    $rs=$this->link->conn->Execute($sql);
	    $i=0;
	    while(!$rs->EOF){
	        $retorno[$i]['id']=$rs->fields['ANEX_TIPO_CODI'];
		$retorno[$i]['ext']=$rs->fields['ANEX_TIPO_EXT'];
                $retorno[$i]['tipo']=$rs->fields['ANEX_TIPO_DESC'];
		$i++;
		$rs->MoveNext();
	    }
	    return $retorno;
	}
	function dataUbicados($cont,$pais,$depto,$muni){
	    $sql="select dpto_nomb,muni_nomb from SGD_DEF_CONTINENTES c,sgd_def_paises p,departamento d, municipio m where
		d.id_pais=p.id_pais and c.id_cont=p.id_cont and d.dpto_codi=m.dpto_codi and c.id_cont=$cont and p.id_pais=$pais 
		and d.dpto_codi=$depto and m.muni_codi=$muni";
	    $rs=$this->link->conn->Execute($sql);
	    $depton=$rs->fields['DPTO_NOMB'];
	    $munin=$rs->fields['MUNI_NOMB'];
	    return array($depton,$munin);
	}

	function sandboxAnexo($radisal,$radipadre,$login,$depus,$desc){
	    if($radipadre==0){
		$radipadre=$radisal;
	    }
	    $maxAnexos=$this->maxRadicados($radipadre)+1;
	    $numAnexos=$this->numeroAnexos($radipadre)+1;
	    $numAnexo = ($numAnexos > $maxAnexos) ? $numAnexos : $maxAnexos;
	    $anexCodigo = $radipadre.str_pad($numAnexo, 5, "0", STR_PAD_LEFT);
	    $sql="insert into anexos (anex_radi_nume,anex_codigo,anex_solo_lect,anex_creador,anex_desc,anex_numero,anex_borrado,anex_origen,radi_nume_salida,
		sgd_trad_codigo,anex_depe_creador,anex_fech_anex,sgd_dir_tipo,anex_salida,anex_estado,anex_tipo,sgd_rem_destino,anex_nomb_archivo) values 
		($radipadre,'$anexCodigo','N','$login','$desc',$numAnexo,'N',1,$radisal,".substr($radisal,-1).",$depus,current_timestamp,1,1,0,7,1,'sinimagen')";
	    $ins=$this->link->conn->Execute($sql);
	    if($ins){
		return $anexCodigo;
	    }
	    else{
		$error="ERROR:Anexo no generado $anex_codigo";
		return $error;
	    }
	}

	function insertarAnexo($radiNume,$anexNomb,$anexArch,$depus,$anexTipo,$desc,$login,$numAnexo,$size,$tpr){
	    $tpr=($tpr==0)?"NULL":$tpr;
	    $sql="insert into anexos (anex_radi_nume,anex_codigo,anex_solo_lect,anex_creador,anex_desc,anex_numero,anex_borrado,anex_origen,
                anex_depe_creador,anex_fech_anex,sgd_dir_tipo,anex_salida,anex_estado,anex_tipo, sgd_rem_destino,anex_nomb_archivo,anex_tamano,sgd_tpr_codigo) values
                ($radiNume,'$anexNomb','N','$desc','$login',$numAnexo,'N',0,$depus,current_timestamp,0,0,1,$anexTipo,1,'$anexArch',$size,$tpr)";
	    $ins=$this->link->conn->Execute($sql);
	    if($ins){
		$retorno=0;
	    }
	    else{
		$retorno=1;
	    }
	    return $retorno;
	}

	function traerDescAnexo($anexCodigo){
	    $sql="select anex_desc, sgd_tpr_codigo tpr from anexos where anex_codigo='$anexCodigo'";
	    $rs=$this->link->conn->Execute($sql);
	    $retorno['desc']=$rs->fields['ANEX_DESC'];
	    $retorno['tpr']=$rs->fields['TPR'];
	    return $retorno;
	}

	function traerFile($anexCodigo){
	    $sql="select anex_nomb_archivo from anexos where anex_codigo='$anexCodigo'";
	    $rs=$this->link->conn->Execute($sql);
            $retorno=$rs->fields['ANEX_DESC'];
            return $retorno;
	}

	function actualFileAnex($anexNomb,$anexArch,$desc,$anexTipo,$size,$tpr){
	    $tpr=($tpr==0)?"NULL":$tpr;
	    $usql="update anexos set anex_nomb_archivo='$anexArch',anex_tipo=$anexTipo,anex_tamano=$size,anex_desc='$desc',sgd_tpr_codigo=$tpr
		where anex_codigo='$anexNomb'";
	    $upd=$this->link->conn->Execute($usql);
	    if($upd){
                $retorno=0;
            }
            else{
                $retorno=1;
            }
            return $retorno;
	}

	function modAnexDesc($anexNomb,$desc,$tpr){
	    $tpr=($tpr==0)?"NULL":$tpr;
	    $usql="update anexos set anex_desc='$desc',sgd_tpr_codigo=$tpr where anex_codigo='$anexNomb'";
            $upd=$this->link->conn->Execute($usql);
            if($upd){
                $retorno=$anexNomb;
            }
            else{
                $retorno="ERROR: No pudo actualizarse la informacion del anexo $anexNomb";
            }
            return $retorno;
	}

	function borrarAnexo($anexNomb){
            $usql="update anexos set anex_borrado='S' where anex_codigo='$anexNomb'";
            $upd=$this->link->conn->Execute($usql);
            if($upd){
                $retorno=$anexNomb;
            }
            else{
                $retorno="ERROR: No pudo actualizarse la informacion del anexo $anexNomb";
            }
            return $retorno;
        }

	function verRadicadoAnexo($numrad){
	    $sql="select t.sgd_trad_codigo, t.sgd_trad_descr, a.anex_radi_nume, a.anex_codigo, a.anex_estado, case when radi_path not like '%.pdf' or radi_path is null
		then 'sinimagen' else radi_path end path from radicado r, sgd_trad_tiporad t,anexos a
		where r.sgd_trad_codigo=t.sgd_trad_codigo and r.radi_nume_radi=a.anex_radi_nume and a.radi_nume_salida=$numrad";
	    $rs=$this->link->conn->Execute($sql);
	    $retorno['error']="";
	    if(!$rs->EOF){
		$retorno['trad']=$rs->fields['SGD_TRAD_CODIGO'];
		$retorno['desc']=$rs->fields['SGD_TRAD_DESCR'];
		$retorno['radi_padre']=$rs->fields['ANEX_RADI_NUME'];
		$retorno['path']=$rs->fields['PATH'];
		$retorno['anex_codigo']=$rs->fields['ANEX_CODIGO'];
		$retorno['anex_estado']=$rs->fields['ANEX_ESTADO'];
	    }
	    else{
		$retorno['error']="ERROR: no se encontro radicado";
	    }
	    return $retorno;
	}

	function asociarImagenAnexo($numrad,$ruta,$filename,$tamano,$pages){
	    $usql="update radicado set radi_path='".$ruta.$filename."', radi_nume_folios=$pages where radi_nume_radi=$numrad";
	    $this->link->conn->Execute($usql);
	    $usql2="update anexos set anex_nomb_archivo='$filename', anex_estado=2,anex_tamano=$tamano where radi_nume_salida=$numrad";
	    $this->link->conn->Execute($usql2);
	}

	function datosradCarpeta($radi){
	    $sql="select radi_nume_radi, case when r.carp_per=0 then car.carp_desc else per.nomb_carp||'(Personal)' end carpeta_nombre, r.carp_per, r.carp_codi
		from radicado r
		left join carpeta car on (r.carp_per=0 and r.carp_codi=car.carp_codi) left join carpeta_per per on (r.carp_per=1 and r.carp_codi=per.codi_carp 
		and per.id_rol=r.id_rol and per.usua_codi=r.radi_usua_actu)  where radi_nume_radi=$radi";
	    $rs=$this->link->conn->Execute($sql);
	    if(!$rs->EOF){
                $retorno['numrad']=$rs->fields['RADI_NUME_RADI'];
                $retorno['carpeta_nombre']=$rs->fields['CARPETA_NOMBRE'];
		$retorno['carp_per']=$rs->fields['CARP_PER'];
		$retorno['carp_codi']=$rs->fields['CARP_CODI'];
	    }
	    return $retorno;
	}

	function verAnexoFirmado($anexCodigo){
            $sql="select anex_radi_nume, anex_codigo, anex_estado, anex_nomb_archivo, radi_nume_salida from anexos
                where anex_codigo='$anexCodigo'";
            $rs=$this->link->conn->Execute($sql);
            if(!$rs->EOF){
                $retorno['radi_padre']=$rs->fields['ANEX_RADI_NUME'];
                $retorno['archivo']=$rs->fields['ANEX_NOMB_ARCHIVO'];
                $retorno['anex_codigo']=$rs->fields['ANEX_CODIGO'];
                $retorno['anex_estado']=$rs->fields['ANEX_ESTADO'];
		$retorno['radi_salida']=$rs->fields['RADI_NUME_SALIDA'];
            }
            return $retorno;
        }

	function anexoFirmado($anexCodigo,$size){
	    $usql="update anexos set anex_tamano=$size where anex_codigo='$anexCodigo'";
	    $rs=$this->link->conn->Execute($usql);
	}

	function actuRadiPath($numrad,$docuOrigen){
	    $ext=end(explode('.',$docuOrigen));
	    $ruta='/'.substr($numrad,0,4).'/'.substr($numrad,4,3).'/'.$numrad.'.'.$ext;
	    if(copy($docuOrigen,RUTA_BODEGA.$ruta)){
		$script="pdfinfo $docuOrigen| grep Pages|awk '{print $2}'";
                exec($script,$data);
	    	$usql="update radicado set radi_path='$ruta',radi_nume_folios={$data[0]} where radi_nume_radi=$numrad";
		$rs=$this->link->conn->Execute($usql);
		if($rs){
		    $retorno=true;
		}
		else{
		    $retorno=false;
		}
	    }
	    else{
		$retorno=false;
	    }
	    return $retorno;
	}

}
?>
