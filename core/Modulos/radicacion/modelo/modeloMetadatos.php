<?php 
/**
 * modeloRadicado es la clase encargada de gestionar las operaciones y los datos basicos referentes a un Radicado
 * @author Hardy Deimont Niño  Velasquez
 * @name modeloRadicado
 * @version     1.0
 */
if (!$ruta_raiz)
    $ruta_raiz = '../../../..';
//include_once "$ruta_raiz/include/db/ConnectionHandler.php";
include_once "$ruta_raiz/core/clases/ConnectionHandler.php";
class modeloMetadatos {
    //link de conexion con la base de datos
    public $link;

    function __construct($ruta_raiz) {
        $db = new ConnectionHandler("$ruta_raiz");
        $db->conn->SetFetchMode(ADODB_FETCH_NUM);
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        $this->link = $db;
	//$this->link->conn->debug = true;
    }

    function crearMetadatos($nombData,$metaDatos,$trad,$depe){
	$this->link->conn->autoCommit=false;
        $this->link->conn->SetTransactionMode("SERIALIZABLE");
        $this->link->conn->StartTrans();
	$err=0;
	$id=$this->link->conn->GenID('seq_radi_datainfo');
	$sql="insert into radi_data_metainfo (radi_data_id,radi_data_nomb,radi_data_busq,radi_data_estado,sgd_trad_codigo,depe_codi) values ($id,'$nombData',1,1,$trad,$depe)";
	$this->link->conn->Execute($sql);
	$m=count($metaDatos);
	//print_r($metaDatos);
	$col="radi_nume_radi N(18)";
	for($i=0;$i<$m;$i++){
	    $tipo=$metaDatos[$i]['tipo'];
	    $nombre=$metaDatos[$i]['nombre'];
	    $busq=$metaDatos[$i]['busq'];
	    $oblig=$metaDatos[$i]['oblig'];
	    $desc=$metaDatos[$i]['desc'];
	    $col.=", $nombre $tipo";
	    $sql2="insert into radi_cam_metacampos (radi_data_id,radi_cam_nombre,radi_cam_tipo,radi_cam_oblig,radi_cam_busq,radi_cam_desc,radi_cam_estado) values 
		($id,'$nombre','$tipo',$oblig,$busq,'$desc',1)";
	    $this->link->conn->Execute($sql2);
	}
	//Sentencia que modifica el modelo de datos
	$val=$this->link->dict->CreateTableSQL('radi_'.$nombData,$col,$res=false);
	$this->link->dict->ExecuteSQLArray($val);
	if(!$this->link->conn->CompleteTrans()){
	    $err=1;
	}
	return $err;
	//$val=$this->link->dict->MetaTables();
    }

    function modificarMetadatos($id,$campo,$valor){
	$sql="update radi_data_metainfo set radi_data_$campo=$valor where radi_data_id=$id";
	$this->link->conn->Execute($sql);
    }

    function agregarTag($id,$nombData,$dataTag){
	$this->link->conn->autoCommit=false;
    $this->link->conn->SetTransactionMode("SERIALIZABLE");
    $this->link->conn->StartTrans();
	$tipo=$dataTag['tipo'];
        $nombre=$dataTag['nombre'];
        $busq=$dataTag['busq'];
        $oblig=$dataTag['oblig'];
	$desc=$dataTag['desc'];
	$estado=$dataTag['estado'];
	$sql="insert into radi_cam_metacampos (radi_data_id,radi_cam_nombre,radi_cam_tipo,radi_cam_oblig,radi_cam_desc,radi_cam_busq,radi_cam_estado) values 
	    ($id,'$nombre','$tipo',$oblig,'$desc',$busq,$estado)";
	$this->link->conn->Execute($sql);
	$col="$nombre $tipo";
	$val=$this->link->dict->AddColumnSQL('radi_'.$nombData,$col);
	$this->link->dict->ExecuteSQLArray($val);
	if($this->link->conn->CompleteTrans()){
        echo "Tag creado con exito";		
	}
	else{
        echo "Fallo creacion de Tag";
	}
    }

    function modificarTag($id,$dataTag){
        $busq=$dataTag['busq'];
        $oblig=$dataTag['oblig'];
        $desc=$dataTag['desc'];
	$estado=$dataTag['estado'];
	$usql="update radi_cam_metacampos set radi_cam_estado=$estado,radi_cam_oblig=$oblig,radi_cam_desc='$desc', radi_cam_busq=$busq
	    where radi_cam_id=$id";
	$this->link->conn->Execute($usql);
    }

    function existeMetadatos($trad,$depe){
	$sql="select count(1) as meta from radi_data_metainfo where sgd_trad_codigo=$trad and radi_data_estado=1 and depe_codi in (0,$depe)";
	$rs=$this->link->conn->Execute($sql);
	$resultado=$rs->fields['META'];
	return $resultado;
    }

    function listarMetadatos($trad=false,$meta,$depe=false){
	$warray=array();
	if($trad){
	    $warray[]="sgd_trad_codigo=$trad";
        }
	if($meta){
	    $warray[]="radi_data_estado=1";
	}
	if($depe){
	    $warray[]="depe_codi in (0,$depe)";
	}
	if(count($warray)!=0){
	    $where="where ".implode(" and ",$warray);
	}
	else{
	    $where="";
	}
	$sql="select radi_data_id,radi_data_nomb,radi_data_busq,radi_data_estado,sgd_trad_codigo from radi_data_metainfo $where";
	$rs=$this->link->conn->Execute($sql);
	$retorno['error']="";
	if(!$rs->EOF){
	    $i=0;
	    while(!$rs->EOF){
	        $retorno[$i]['radi_data_id']=$rs->fields['RADI_DATA_ID'];
		$retorno[$i]['radi_data_nomb']=$rs->fields['RADI_DATA_NOMB'];
		$retorno[$i]['radi_data_busq']=$rs->fields['RADI_DATA_BUSQ'];
		$retorno[$i]['radi_data_estado']=$rs->fields['RADI_DATA_ESTADO'];
		$retorno[$i]['sgd_trad_codigo']=$rs->fields['SGD_TRAD_CODIGO'];
		$i++;
		$rs->MoveNext();
	    }
	}
	else{
	    $retorno['error']="No existen metadatos para este tipo de radicacion";
	}
	return $retorno;
    }

    function infoMetadatos($id){
	$sql="select radi_data_id,radi_data_nomb,radi_data_busq,radi_data_estado,sgd_trad_codigo from radi_data_metainfo where radi_data_id=$id";
        $rs=$this->link->conn->Execute($sql);
            $retorno['radi_data_id']=$rs->fields['RADI_DATA_ID'];
            $retorno['radi_data_nomb']=$rs->fields['RADI_DATA_NOMB'];
            $retorno['radi_data_busq']=$rs->fields['RADI_DATA_BUSQ'];
            $retorno['radi_data_estado']=$rs->fields['RADI_DATA_ESTADO'];
            $retorno['sgd_trad_codigo']=$rs->fields['SGD_TRAD_CODIGO'];
        return $retorno;
    }

    function listarTags($id_meta,$meta){
	if($meta){
	    $where=" and radi_cam_estado=1";
	}
	else{
	    $where="";
	}
	$sql="select radi_cam_id,radi_cam_nombre,radi_cam_tipo,radi_cam_oblig,radi_cam_busq,radi_cam_desc,radi_cam_estado from radi_cam_metacampos
	    where radi_data_id=$id_meta $where";
	$rs=$this->link->conn->Execute($sql);
        $retorno['error']="";
        if(!$rs->EOF){
            $i=0;
            while(!$rs->EOF){
		$retorno[$i]['radi_cam_id']=$rs->fields['RADI_CAM_ID'];
		$retorno[$i]['radi_cam_nombre']=$rs->fields['RADI_CAM_NOMBRE'];
		$retorno[$i]['radi_cam_tipo']=$rs->fields['RADI_CAM_TIPO'];
		$retorno[$i]['radi_cam_oblig']=$rs->fields['RADI_CAM_OBLIG'];
		$retorno[$i]['radi_cam_busq']=$rs->fields['RADI_CAM_BUSQ'];
		$retorno[$i]['radi_cam_desc']=$rs->fields['RADI_CAM_DESC'];
		$retorno[$i]['radi_cam_estado']=$rs->fields['RADI_CAM_ESTADO'];
		$i++;
                $rs->MoveNext();
	    }
	}
	else{
	    $retorno['error']="No existen metadatos para este tipo de radicacion";
	}
	return $retorno;
    }

    function insertarInfoMeta($id,$dataTag,$numrad){
	$sql="select radi_data_nomb from radi_data_metainfo where radi_data_id=$id";
	$rs=$this->link->conn->Execute($sql);
	$nombtable=$rs->fields['RADI_DATA_NOMB'];
	$ins="insert into radi_$nombtable ";
	$col="radi_nume_radi";
	$val="$numrad";
	foreach($dataTag as $key=>$value){
	    $col.=",$key";
	    if(is_numeric($value) && $value!=''){
	        $val.=",$value";
	    }
	    elseif(trim($value)==''){
		$val.=",NULL";
	    }
	    else{
		$val.=",'$value'";
	    }
	}
	$ins.="($col) values ($val)";
	$this->link->conn->Execute($ins);
	$usql="update radicado set radi_mdata_metadatos=$id where radi_nume_radi=$numrad";
	$this->link->conn->Execute($usql);
    }

    function metaXbusq(){
	$sql="select d.radi_data_id,d.RADI_DATA_NOMB, count(c.radi_cam_id) from radi_data_metainfo d left outer join radi_cam_metacampos c on 
	    (c.radi_data_id=d.radi_data_id and radi_cam_busq=1) where d.radi_data_busq=1 group by d.radi_data_id, 
	    radi_data_nomb having count(c.radi_cam_id)>0 order by radi_data_id";
	$rs=$this->link->conn->Execute($sql);
	$retorno=array();
	if(!$rs->EOF){
	    $i=0;
	    while(!$rs->EOF){
		$retorno[$i]['id']=$rs->fields['RADI_DATA_ID'];
		$retorno[$i]['nomb']=$rs->fields['RADI_DATA_NOMB'];
		$i++;
		$rs->MoveNext();
            }
	}
	return $retorno;
    }
}
