<?php 
/**
 * modeloGenlistado es la clase encargada de gestionar las operaciones de las Listaod de terceros
 * 
 * @author Hardy Deimont Niño  Velasquez
 * @name modeloGenlistado
 * @version	1.0
 */
if (!$ruta_raiz)
    $ruta_raiz = '../../../..';
include_once "$ruta_raiz/core/clases/ConnectionHandler.php";
class modeloGenlistado {
    public $link;

    function __construct($ruta_raiz) {

        $db = new ConnectionHandler("$ruta_raiz");
        $db->conn->SetFetchMode(ADODB_FETCH_NUM);
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        $this->link = $db;
         if($_SESSION['usua_debug']==1)
        $this->link->conn->debug = true;
    }

    function CosultarListadoMasiva($depe) {
        $q = "SELECT  mas_csv_id as id,  mas_csv_nombre as nomb,  mas_csv_estado as est,  mas_csv_depe as depe,  mas_csv_extra as extra FROM 
                mas_csv_base where  mas_csv_depe=$depe or mas_csv_estado=2";
        $rs = $this->link->query($q);
        $retorno = array();
        //print_r($rs);
        if (!$rs->EOF) {

            $i = 0;
            while (!$rs->EOF) {
                $retorno [$i] ['id'] = $rs->fields ['ID'];
                $retorno [$i] ['nombre'] = $rs->fields ['NOMB'];
                $retorno [$i] ['est'] = $rs->fields ['EST'];
                $retorno [$i] ['depe'] = $rs->fields ['DEPE'];
                $retorno [$i] ['extra'] = $rs->fields ['EXTRA'];
                $i ++;
                $rs->MoveNext();
            }
        } else {
            $retono ['Error'] = 'No se contienen datos';
        }
        return $retorno;
    }

    //--------------------------------------------

    function valListadoMasiva($nomb, $esta, $depe, $extra) {

        $q1 = "SELECT  mas_csv_nombre,mas_csv_depe,mas_csv_id FROM  mas_csv_base WHERE mas_csv_nombre='$nomb' and mas_csv_depe=$depe ";
        $rs = $this->link->query($q1);
        $retorno = array();
        //print_r($rs);
        if (!$rs->EOF) {
            $retorno = 'El listado ya esta creado en el sistema';
        } else {
          
                $retorno = 'ok';
            
        }
        return $retorno;
    }

    function valListadoMasivaMod($id, $nomb, $esta, $depe, $extra) {
        $q1 = "SELECT  mas_csv_nombre,mas_csv_depe,mas_csv_id FROM  mas_csv_base WHERE  mas_csv_id=$id ";

        $rs = $this->link->query($q1);
        $retorno = array();
        //print_r($rs);

            if ($rs->fields ['MAS_CSV_DEPE'] == $depe)
                $retorno = 'ok';
            else
                $retorno = 'El listado No corresponde a la dependencia';
        
        return $retorno;
    }

    //--------------------------------------------

    function addListadoMasiva($nomb, $esta, $depe, $extra) {

        $q1 = "SELECT  max(mas_csv_id) as id FROM  mas_csv_base ";
        $rs2 = $this->link->query($q1);
        $id = $rs2->fields ['ID'] + 1;
        $q = "insert into mas_csv_base   (mas_csv_id,  mas_csv_nombre,  mas_csv_estado,  mas_csv_depe,  mas_csv_extra) values 
                 ($id,'$nomb',$esta,$depe,'$extra')";
        $rs = $this->link->query($q);
        $retorno = array();
        //print_r($rs);
        if ($rs->EOF) {
            $retono = 'ok';
        } else {
            $retono = 'No se contienen datos';
        }
        return $retorno;
    }

    function modListadoMasiva($id, $nomb, $esta, $depe, $extra) {

        $q = "update  mas_csv_base  set  mas_csv_nombre='$nomb', mas_csv_estado=$esta,mas_csv_depe=$depe,  mas_csv_extra='$extra '
                 where mas_csv_id=$id";
        $rs = $this->link->query($q);
        $retorno = array();
        //print_r($rs);
        if ($rs->EOF) {
            $retono = 'ok';
        } else {
            $retono = 'No se contienen datos';
        }
        return $retorno;
    }

    function ListarMasiva($id) {

        $q = "SELECT   mas_dat_id as id,   mas_dat_idcsv as idlista,   mas_dat_tprem as tpŕem,   mas_dat_codrem as codrem,   mas_dat_extras as campextra,   mas_dat_expediente as exp
                FROM   mas_dat_csvdata  where mas_dat_idcsv=$id";
        $rs = $this->link->query($q);
        $retorno = array();
        //print_r($rs);
        if (!$rs->EOF) {

            $i = 0;
            while (!$rs->EOF) {
                $retorno [$i] ['id'] = $rs->fields ['ID'];
                $retorno [$i] ['idlista'] = $rs->fields ['IDLISTA'];
                $retorno [$i] ['tprem'] = $rs->fields ['|TPREM'];
                $retorno [$i] ['codrem'] = $rs->fields ['CODREM'];
                $retorno [$i] ['extra'] = $rs->fields ['EXTRA'];
                $retorno [$i] ['exp'] = $rs->fields ['EXP'];
                $i ++;
                $rs->MoveNext();
            }
        } else {
            $retono ['Error'] = 'No se contienen datos';
        }
        return $retorno;
    }

}

?>
