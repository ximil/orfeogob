<?php /** 
 * modeloTpradi es la clase encargada de gestionar las operaciones y los datos basicos referentes a un series
 * @author Hardy Deimont Niño  Velasquez
 * @name modeloTpradi
 * @version	1.0
 */
if (! $ruta_raiz)
	$ruta_raiz = '../../../..';
	//include_once "$ruta_raiz/include/db/ConnectionHandler.php";
include_once "$ruta_raiz/core/clases/ConnectionHandler.php";
class modeloTpradi {
	
	public $link;
	
	function __construct($ruta_raiz) {
		
		$db = new ConnectionHandler ( "$ruta_raiz" );
		$db->conn->SetFetchMode ( ADODB_FETCH_NUM );
		$db->conn->SetFetchMode ( ADODB_FETCH_ASSOC );
               // $db->conn->debug=true;
		$this->link = $db;
	}
	
	/**
	 * @return consulta los datos de serie.
	 */
	function consultar() {
		$query = "SELECT SGD_TRAD_CODIGO,SGD_TRAD_DESCR,sgd_trad_genradsal,SGD_trad_icono FROM SGD_TRAD_TIPORAD ORDER BY SGD_TRAD_CODIGO";
		$rs = $this->link->query ( $query );
		$i = 0;
		while ( ! $rs->EOF ) {
			$combi [$i] ["CODIGO"] = $rs->fields ['SGD_TRAD_CODIGO'];
			$combi [$i] ["DESCRIP"] = $rs->fields ['SGD_TRAD_DESCR'];
			$combi [$i] ["GENRADSAL"] = $rs->fields ['SGD_TRAD_GENRADSAL'];
			$combi [$i] ["ICONO"] = $rs->fields ['SGD_TRAD_ICONO'];
			$i ++;
			$rs->MoveNext ();
		}
		return $combi;
	
	}
        
        function crear($codigo,$desc,$salRad) {
            $ok1 = $this->link->conn->Replace('SGD_TRAD_TIPORAD',array('SGD_TRAD_CODIGO'=>$codigo,'SGD_TRAD_DESCR'=>$desc,'SGD_TRAD_GENRADSAL'=>$salRad),'SGD_TRAD_CODIGO',true);            
            $ok7 = $this->link->conn->Replace('SGD_TIP3_TIPOTERCERO',array('SGD_TIP3_CODIGO'=>1,'SGD_TPR_TP'.$codigo=>0),'SGD_TIP3_CODIGO',true);
            $ok5 = $this->link->conn->Replace('CARPETA',array('CARP_CODI'=>$codigo,'CARP_DESC'=>$desc),'CARP_CODI',true);
            	$query = "UPDATE sgd_mod_modules SET sgd_mod_menu= 'Rad. $desc', sgd_mod_estado = '1' WHERE sgd_mod_modulo = 'usua_prad_tp$codigo'";
	 	$rs=$this->link->conn->Execute($query);
            return $ok7.'.'.$ok5;
        }
        
        function eliminar($codigo) {
            $ok1 = $this->link->conn->Execute("DELETE FROM SGD_TRAD_TIPORAD WHERE SGD_TRAD_CODIGO=".$codigo);
            $ok5 = $this->link->conn->Execute("DELETE FROM CARPETA WHERE CARP_CODI=".$codigo);
            $query = "UPDATE sgd_mod_modules SET sgd_mod_estado = '0' WHERE sgd_mod_modulo = 'usua_prad_tp$codigo'";
	 	$rs=$this->link->conn->Execute($query);
            return $ok1.'.'.$ok5;
        }
        
        function modificar($codigo,$descrip,$salRad){	
	//$flag = $this->link->conn->Replace('CARPETA',array('CARP_CODI'=>$codigo,'CARP_DESC'=>$descrip),"CARP_CODI");
       //  $flag = $this->link->conn->Replace('SGD_TRAD_TIPORAD',array('SGD_TRAD_CODIGO'=>$codigo,'SGD_TRAD_DESCR'=>$descrip,'SGD_TRAD_GENRADSAL'=>$salRad),'SGD_TRAD_CODIGO');
        $query="UPDATE CARPETA SET CARP_DESC='$descrip' WHERE CARP_CODI=$codigo";
        $rs = $this->link->query ( $query );
        $query="UPDATE SGD_TRAD_TIPORAD SET SGD_TRAD_DESCR='$descrip',SGD_TRAD_GENRADSAL=$salRad WHERE SGD_TRAD_CODIGO=$codigo";
        $rs = $this->link->query ( $query );
        $query = "UPDATE sgd_mod_modules SET sgd_mod_menu= 'Rad. $descrip' WHERE sgd_mod_modulo = 'usua_prad_tp$codigo'";
	$rs=$this->link->conn->Execute($query);
	return $rs;
}
	/**
	 * @return consulta los datos de los Roles.
	 */
/*	function Buscar($id, $detaserie, $like = 0) {
		//echo $id,$detaserie;
		if ($id != 0)
			$where = "where sgd_srd_codigo = '$id'";
		else if (strlen ( $detaserie ) > 0) {
			if ($like == 0)
				$where = "where upper(sgd_srd_descrip) = upper('$detaserie')";
			else
				$where = "where upper(sgd_srd_descrip) like upper('%$detaserie%')";
		}
		$query = "select SGD_SRD_CODIGO,SGD_SRD_DESCRIP,to_char(SGD_SRD_FECHFIN,'DD/MM/YYYY') SGD_SRD_FECHFIN ,to_char(SGD_SRD_FECHINI,'DD/MM/YYYY') SGD_SRD_FECHINI  from sgd_srd_seriesrd  $where ";
		$rs = $this->link->query ( $query );
		
		$i = 0;
		if (! $rs->EOF) {
			while ( ! $rs->EOF ) {
				$combi [$i] ["CODIGO"] = $rs->fields ['SGD_SRD_CODIGO'];
				$combi [$i] ["DESCRIP"] = $rs->fields ['SGD_SRD_DESCRIP'];
				$combi [$i] ["FECHINI"] = $rs->fields ['SGD_SRD_FECHINI'];
				$combi [$i] ["FECHFIN"] = $rs->fields ['SGD_SRD_FECHFIN'];
				$i ++;
				$combi ['ERROR'] = 'OK';
				$rs->MoveNext ();
			}
			
			return $combi;
		} else {
			return $combi ['ERROR'] = 'No se encontro  dato Buscado.';
		}
	
	}
	/**
	 * @return crea un Rol.
	 */
/*	function crear($codserieI, $detaserie, $sqlFechaD, $sqlFechaH) {
		$query = "insert into SGD_SRD_SERIESRD(SGD_SRD_CODIGO   , SGD_SRD_DESCRIP,SGD_SRD_FECHINI,SGD_SRD_FECHFIN )
					VALUES ($codserieI,upper('$detaserie')    ,'" . $sqlFechaD . "','" . $sqlFechaH . "')";
		$rs = $this->link->conn->Execute ( $query );
		if (! $rs) {
			return 'ERROR: No se realizo la Transacción';
		}
		return 'Transacción realizada con exito';
	}
	
	/**
	 * @return resultado de la operacion de actualizacion un Rol.
	 * @param $nombre
	 * @param $tipo
	 * @param $id
	 */
/*	function actualizar($codserieI, $detaserie, $sqlFechaD, $sqlFechaH) {
		
		$isqlUp = "update sgd_srd_seriesrd 
						set SGD_SRD_DESCRIP= '$detaserie',
						SGD_SRD_FECHINI='$sqlFechaD',
						SGD_SRD_FECHFIN ='$sqlFechaH'
						where sgd_srd_codigo = $codserieI";
		
		$rs = $this->link->conn->Execute ( $isqlUp );
		if (! $rs) {
			return 'ERROR';
		}
		return "SE MODIFIC&Oacute; LA SERIE $detaserie";
	
	}*/
}
?>
