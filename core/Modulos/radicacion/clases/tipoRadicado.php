<?php 
/**
 * tipoRadicado es la clase encargada de gestionar las operaciones tipoRadicado 
 * @author Hardy Deimont Niño  Velasquez
 * @name tipoRadicado
 * @version	1.0
 */
if (!$ruta_raiz)
    $ruta_raiz = '../../../..';
include_once "$ruta_raiz/core/Modulos/radicacion/modelo/modeloTpradi.php";
//==============================================================================================	
// CLASS tipoRadicado
//==============================================================================================	

/**
 *  Objecto tipoRadicado 
 */
class tipoRadicado {

    private $modelo;
    private $trad_genradsal; //login de  usuario
    private $trad_descr; //descripcion
    private $trad_icono; //icono
    private $codigo; //codigo 
    private $ocupados = array("0");
    private $ruta_raiz;
    
    
    public function getOcupados() {
        return $this->ocupados;
    }

        function __construct($ruta_raiz) {


        $this->modelo = new modeloTpradi($ruta_raiz);
        $this->ruta_raiz;
    }

    /**
     * Consulta los datos de las tipoRadicado 
     * @return   void
     */
    function consultar() {
        $rs = $this->modelo->consultar();
        return $rs;
    }

    function consultaHtml() {

         $tparray = $this->modelo->consultar();
        for ($index = 0; $index < count($tparray); $index++) {
            $TPRadSAl = 'No';
            if ($tparray[$index]['GENRADSAL'] == 1) {
                $TPRadSAl = 'Si';
            }
            $codigo= $tparray[$index]['CODIGO'];
            $descrip=$tparray[$index]['DESCRIP'];
            $codsal=$tparray[$index]['GENRADSAL'];
            $nombrecampo = $cad . $tparray[$index]['CODIGO'];
            $val.="<tr><td class='listado2'>" .$codigo . "</td>
            <td class='listado2'>" . $descrip . "</td>
            <td class='listado2'>" . $TPRadSAl . "</td><td class='listado2'><a href='#' onclick='editor($codigo,\"$descrip\",$codsal);'><img src='../../../../imagenes/edit.png' width='24'/></a></td></tr>";
            $this->ocupados[$index]=$tparray[$index]['CODIGO'] ;
            /*$valJS.="," . $nombrecampo;
            $documenJs.="document.getElementById('$nombrecampo').value = $nombrecampo;\n";
            $documenJS2.="var $nombrecampo = document.getElementById('$nombrecampo').value;";
            $documenJSend.="+'&$nombrecampo='+$nombrecampo";*/
             
        }
       // print_r($this->ocupados);
        return $val;
    }

    function libres(){
        
        
         for ($index = 1; $index < 10; $index++) {
             if(!$this->ocupados[$index-1])
                $option[$index].=$index;
             
         }
        return $option;
    }
    /* 	function buscar() {
      $combi = $this->modelo->Buscar( 0,$this->descripcion,1);
      if($combi  ["ERROR"]=='OK'){
      return $combi;
      }
      return 'No se encotro la Descripción';

      }

      /**
     * crea usuario
     */
    function crear($codigo,$descripcion,$salRad) {
      
      return $this->modelo->crear($codigo,$descripcion,$salRad);
      
      }
      
    function Modificar($codigo,$descripcion,$salRad) {
      
      return $this->modelo->modificar($codigo,$descripcion,$salRad);
      
    }
    function eliminar($codigo) {
      
      return $this->modelo->eliminar($codigo);
      
    }
      /**
     * actualiza  usuario
     */
    /* 	function actualizar() {

      $combi2 = $this->modelo->Buscar( 0,$this->descripcion);
      if($combi2  ["ERROR"]!='OK'||$combi2  [0]["DESCRIP"]==$this->descripcion){
      return $this->modelo->actualizar($this->codigo,$this->descripcion, $this->fechaInicio, $this->fechaFin);
      }
      else
      return 'Ya existe la Descripción';


      }

      /**
     * Limpia los atributos de la instancia referentes a la informacion del usuario
     * @return   void
     */

    function __destruct() {
        
    }

}
?>