<?php 
/**
 * Radicado es la clase encargada de gestionar las operaciones .Radicado 
 * @author Hardy Deimont Niño  Velasquez
 * @name Radicado
 * @version	1.0
 */
if (!$ruta_raiz)
    $ruta_raiz = '../../../..';
include_once "$ruta_raiz/core/Modulos/radicacion/modelo/modeloMasiva.php";

//==============================================================================================	
// CLASS masivas
//==============================================================================================	

/**
 *  Objecto radicado 
 */
class masivas {

    private $modelo;
    private $idList;

    public function getIdList() {
        return $this->idList;
    }

    public function setIdList($idList) {
        $this->idList = $idList;
    }

    function __construct($ruta_raiz) {
        $this->modelo = new modeloMasiva($ruta_raiz);
    }

    /**
     * Listar una Lista masiva.
     */
    function Listar($mTexp, $aa) {

        $data = $this->modelo->listar($this->idList, $aa);
        $exp = "";
        if ($mTexp == 2) {
            $exp = "<th>Expediente</th>";
        }
        if ($aa == 1)
            $aac = "<th>Accion</th>";
        $resp = "<table> <tr class='titulos31'>
                <th>Nombre</th><th>Apellido</th><th>seg Apellido</th><th>Documento</th>
                <th>Pais</th><th>Departamento</th><th>Municipio</th><th>Direccion</th>$exp $aac </tr>";
        for ($i = 0; $i < count($data); $i++) {
            $g = $i % 2;
            if ($g == 0)
                $style = 'listado51';
            else
                $style = 'listado50';
            $exp2 = "";
            if ($mTexp == 2)
                $exp2 = "<td>" . $data[$i]['exp'] . "</td>";
            $acc = '';
            if ($aa == 1)
                $acc = " <td><a href='#' onclick='delLista(" . $data[$i]['id'] . ")'>eliminar</a></td>";

            $resp.="<tr class='$style'>
             <td>" . $data[$i]['nombre'] . "</td>
             <td>" . $data[$i]['apellido'] . "</td>
             <td>" . $data[$i]['apellido2'] . "</td>
             <td>" . $data[$i]['doc'] . "</td>
             <td>" . $data[$i]['pais'] . "</td>
             <td>" . $data[$i]['depto'] . "</td>
             <td>" . $data[$i]['muni'] . "</td>
             <td>" . $data[$i]['dir'] . "</td>
             $exp2
                 $acc
             </tr> ";
        }
        $resp='<table><tr class="titulo1">
            <td>Cantidad <input type="text" value="'.$i.'" class="select" name="listNum" id="listNum" disabled></td>
            </tr>  <tr><td>'.$resp.'</td></tr></table>';
        return $resp;
    }

    function delitem($id) {
        return $this->modelo->delitem($this->idList, $id);
    }

    function insListainsLista($tpemp, $doc, $exp) {
        return $this->modelo->insLista($this->idList, $tpemp, $doc, $exp);
    }

    function busqueda3($tbusqueda, $nombre_essp, $no_documento) {
        $resp.='<table class=borde_tab width="99%" cellpadding="0" cellspacing="1" bgcolor="#FFFFFF">';
        $data = $this->modelo->consultar3($tbusqueda, $nombre_essp, $no_documento);
        $dataMuni = $this->modelo->ciudad();

        $tipo_emp = $tbusqueda;

        $esty = 1;
        $resp.='<tr class="grisCCCCCC" align="center">
            <td width="10%" CLASS="titulos31">DOCUMENTO</td>
            <td width="10%" CLASS="titulos31">NOMBRE</td>
            <td width="10%" CLASS="titulos31">PRIM. APELLIDO o SIGLA</td>
            <td width="10%" CLASS="titulos31">SEG. APELLIDO o R Legal</td>
            <td width="10%" CLASS="titulos31">DIRECCION</td>
            <td width="5%" CLASS="titulos31">CIUDAD</td>
            <td width="5%" CLASS="titulos31">TELEFONO</td>
            <td width="5%" CLASS="titulos31">EMAIL</td>
            <td colspan="3%" CLASS="titulos31">COLOCAR COMO</td>
            </tr> ';

        for ($i = 0; $i < count($data); $i++) {
            $email = $data[$i]['email'];
            $telefono = $data[$i]['telefono'];
            $direccion = $data[$i]['direccion'];
            $apell2 = $data[$i]['apell2'];
            $apell1 = $data[$i]['apell1'];
            $nombre = $data[$i]['nombre'];
            $codigo = $data[$i]['codigo'];
            $codigo_cont = $data[$i]['codigo_cont'];
            $codigo_pais = $data[$i]['codigo_pais'];
            $codigo_dpto = $data[$i]['codigo_dpto'];
            $codigo_muni = $data[$i]['codigo_muni'];
            $cc_documento = $data[$i]['cc_documento'];
            ($grilla == "timparr") ? $grilla = "timparr" : $grilla = "tparr";
            if ($esty == 1) {
                $estilo = 'listado51';
                $esty = 0;
            } else {
                $estilo = 'listado50';
                $esty = 1;
            }
            $resp.="<tr class='$grilla'>
                     <TD class='$estilo'><font size='-3'>{$cc_documento}</font></TD>
                <TD class='$estilo'><font size='-3'> " . substr($nombre, 0, 120) . "</font></TD>
                <TD class='$estilo'><font size='-3'>" . substr($apell1, 0, 70) . "</font></TD>
                <TD class='$estilo'><font size='-3'>{$apell2} </font></TD>
                <TD class='$estilo'><font size='-3'>{$direccion}</font></TD>
                <TD class='$estilo'><font size='-3'>{$dataMuni[$data[$i]['coddpto']][$data[$i]['codmuni']]} </font></TD>
                <TD class='$estilo'><font size='-3'> {$telefono}</font></TD>
                <TD class='$estilo'><font size='-3'> " . substr($email, 0, 30) . "</font></TD>
                <TD width='6%' align='center' valign='top' class='$estilo'>
                <font size='-3'><a href='#btnpasar' 
                onClick=\"addItem('$codigo', '$cc_documento', '$nombre','$apell1', '$apell2','{$dataMuni[$data[$i]['coddpto']][$data[$i]['codmuni']]}','$direccion', $tbusqueda);\"
                        class='$estilo'>adicionar</a></font></TD></tr>";
        }

        return $resp . "</table>";
    }

    function __destruct() {
        
    }

}
?>