<?php 
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of genListado
 *
 * @author deimont
 */
if (! $ruta_raiz) 
	$ruta_raiz = '../../../../';

include_once "$ruta_raiz/core/Modulos/radicacion/modelo/modeloGenlistado.php";

class genListado {
    //put your code here
    private $modelo;
    private $ruta_raiz;
    
    public function __construct( $ruta_raiz) {
     $this->ruta_raiz = $ruta_raiz;
		$this->modelo = new modeloGenlistado( $this->ruta_raiz );
    }
   
    public function consultarBase($depe) {
        //Array ( [id] => 1 [nombre] => prueba [est] => 1 [depe] => 900 [extra] => dignatario,prueba )
        $res=$this->modelo->CosultarListadoMasiva($depe);
        //print_r($res);
        $data.='ml = new Array();';
        for ($b = 0; $b < count($res); $b++) {
            $a=$res[$b]['id'];
            
            $res[$b]['est'];
            $res[$b]['depe'];
            $res[$b]['extra'];
            $option.="<option value='$a' >".$res[$b]['nombre']." (".$res[$b]['depe'].")</option>";
            $data.= " ml[$a] = new Array();
            ml[$a]['NOMBRE'] = '".$res[$b]['nombre']."';
            ml[$a]['ID'] = ".$res[$b]['id'].";
            ml[$a]['EXTRA'] = '".$res[$b]['extra']."';
            ml[$a]['EST'] = ".$res[$b]['est'].";
            ml[$a]['DEPE'] = ".$res[$b]['depe'].";";
        }
        $rsx['option']=$option;
       $rsx['script']=$data;
        return $rsx;
    }

    public function crearBase($nomb, $esta, $depe, $extra) {
        $resval=$this->modelo->valListadoMasiva($nomb, $esta, $depe, $extra);
        if($resval=='ok'){
            $res=$this->modelo->addListadoMasiva($nomb, $esta, $depe, $extra);
                if($res!='ok')
                    $res2=' Se creo listado ';
                else 
                    $res2='Error al generar nuevo listado';
        }
        else
            $res2='Error el nombre de listado ya se encuentra creado en el sistema ';
        
        return $res2;
    }
    
    public function crearModificar($id, $nomb, $esta, $depe, $extra) {
        $resval=$this->modelo->valListadoMasivaMod($id,$nomb, $esta, $depe, $extra);
        if($resval=='ok'){
            $res=$this->modelo->modListadoMasiva($id, $nomb, $esta, $depe, $extra);
            if($res!='ok')
                $res2=' Se modificado listado ';
            else  $res2='Error no se puede modificar el listado';
        }
        else
            $res2=$resval; //'Error no se puede modificar un listado de otra dependencia ';
        return $res2;
    }
    
        
    public function consultarTercero() {
        ;
    }
    
        
    public function agregarTercero() {
        ;
    }
    
        
    public function quitarTercero() {
        ;
    }
}

?>
