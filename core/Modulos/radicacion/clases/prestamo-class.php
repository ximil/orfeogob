<?php // echo "ESTOY EN  PRESTAMO_CLASS"."<BR>";
if (! $ruta_raiz) 
	$ruta_raiz = '../../../../';

include_once "$ruta_raiz/core/Modulos/radicacion/modelo/modeloPrestamos.php";

//print_r("inicio prestamo-class"."<BR>");

class prestamo {
	public $ruta_raiz;
	private $modelo;
	private $expediente; 
	private $radicado; 
	private $dependencia; 
	private $fecha; 
	private $usuario;
	private $tipo;
	private $usuaLogin;
	
	/**
	 * @param field_type $usuaLogin
	 */
	public function setUsuaLogin($usuaLogin) {
		$this->usuaLogin = $usuaLogin;
	}

	public function getExpediente() {
		return $this->expediente;
	}

	public function getRadicado() {
		return $this->radicado;
	}

	public function getDependencia() {
		return $this->dependencia;
	}
	
	public function getFecha() {
		return $this->fecha;
	}
	
	public function getUsuario() {
		return $this->usuario;
	}
	
	public function setExpediente($expediente) {
		$this->expediente = $expediente;
	}

	public function setRadicado($radicado) {
		$this->radicado = $radicado;
	}

	public function setDependencia($dependencia) {
		$this->dependencia = $dependencia;
	}


	public function setUsuario($usuario) {
		$this->usuario = $usuario;
	}
	public function setTipo($tipo) {
		$this->tipo = $tipo;
	}
	
	function __construct($ruta_raiz) {
		$this->ruta_raiz = $ruta_raiz;
		$this->modelo = new modeloPrestamos( $this->ruta_raiz );
	}

	function buscar() {		
	//	echo $this->tipo;
		return $this->modelo->buscar($this->expediente,$this->radicado,$this->dependencia,$this->usuario,$this->tipo);
		//return $this->modelo->buscar($this->expediente,$this->radicado,$this->dependencia);
	}
	function buscarInfoPrstamo() {
		//	echo $this->tipo;
		return $this->modelo->buscarUsuario($this->dependencia, $this->usuaLogin);
		//return $this->modelo->buscar($this->expediente,$this->radicado,$this->dependencia);
	}
	function prestar( $idp, $estado, $descrip, $fechaVencimiento){		
		return $this->modelo->prestar($this->radicado, $this->usuario,$idp, $estado, $descrip, $fechaVencimiento);
		//return $this->modelo->buscar($this->expediente,$this->radicado,$this->dependencia);
	}
	function cancelarSol($idp){		
		return $this->modelo->canSol($this->radicado, $this->usuario,$idp);
		//return $this->modelo->buscar($this->expediente,$this->radicado,$this->dependencia);
	}
	function devolver($idp, $descrip){		
		return $this->modelo->devolver($this->radicado, $this->usuario, $idp, $descrip);
	}
	function consultarPrestamos(){		
		return $this->modelo->buscar('', $this->radicado,'','','');
	}	
/*	function fechadife($date){
		 //$dd=0, $mm=0; $yy=0; $hh=0; $mn=0; $ss=0;
			$date_r = getdate(strtotime($date));
			$date_result = date("Y/m/d", mktime(($date_r["hours"]+$hh), ($date_r["minutes"]+$mn), ($date_r["seconds"]+$ss), ($date_r["mon"]+$mm),($date_r["mday"]+$dd),($date_r["year"]+$yy)));
			return $date_result;
			
	}*/
	function __destruct() {

	}

}

//print_r("fin prestamo-class"."<BR>");
?>