<?php 
/** 
 * Radicado es la clase encargada de gestionar las operaciones .Radicado 
 * @author Hardy Deimont Niño  Velasquez
 * @name Radicado
 * @version	1.0
 */ 
if (! $ruta_raiz)
	$ruta_raiz = '../../../..';
    include_once "$ruta_raiz/core/Modulos/radicacion/modelo/modelotx.php";
	//==============================================================================================	
	// CLASS radicado
	//==============================================================================================	
	
	/**
	 *  Objecto radicado 
	 */ 

class tx  {

	private $modelo;
	

	
	function __construct($ruta_raiz) {

		
	    $this->modelo = new modeloTx( $ruta_raiz );
	}
	
        
	/**
	 * Consulta los datos de las tipoRadicado 
	 * @return   void
	 */
	function consultar($radicado,$depe) {
			$rs = $this->modelo->consultar ( $radicado,$depe );
			return $rs;
		
	}
	
/**
	 * Consulta los datos de radicaod en solicitud de anulacion. 
	 * @return   void
	 */
	function consultarSolAnu($radicados) {
			$rs = $this->modelo->consultarSolAnu ( $radicados );
			return $rs;
		
	}
	
     /**
	 * Desanular el estado de anulacion. 
	 * @return   void
	 */
	function desanular($radicados,$observa) {
		//validar estado del  radicado  y  usuario  anterior
			$rs = $this->modelo->desanular($radicados,$observa);
			return $rs;
		
	}	
	
    /**
	 * Consulta los datos de motivos 
	 * @return   void
	 */
	function consultarMotivos($aplica) {
			$rs = $this->modelo->consultarMotivos($aplica);
			return $rs;
		
	}
	
    /**
	 * crea motivos 
	 * @return   void
	 */
	function crearMotivos($nomb,$aplica) {
			$this->modelo->crearMotivos($nomb,$aplica);
			return 'Creado con exito';
		
	}            

	/**
	 * modificar motivos 
	 * @return   void
	 */
	function modMotivos($nomb,$codigo) {
			$this->modelo->modMotivos($nomb,$codigo);
			return 'Modificado  Con exito';
		
	}            
	/**
	 * eliminar motivos 
	 * @return   void
	 */
	function eliMotivos($codigo) {
			$this->modelo->eliMotivos($codigo);
			return 'Eliminado  con exito';
		
	}  	
    /**
	 * Reasignar  un radicado 
	 * @return   void
	 */
	function reasignar( $radicados, $loginOrigen,$depDestino,$depOrigen,$codUsDestino, $codUsOrigen,$tomarNivel, $observa,$codTx,$carp_codi,$id_rol,$usuaDocOrigen) {
			$rs = $this->modelo->reasignar($radicados,$loginOrigen,$depDestino,$depOrigen,$codUsDestino, $codUsOrigen,$tomarNivel, $observa,$codTx,$carp_codi,$id_rol,$usuaDocOrigen);
			return $rs;
		
	}

    /**
	 * Consulta los datos de las tipoRadicado 
	 * @return   void
	 */
	function insertaHistorico($radicados,$depeOrigen,$depeDest,$usDocOrigen,$usDocDest,$usCodOrigen,$usCodDest,$rol_orign, $rol_dest,$observacion,$tipoTx) {
			$rs = $this->modelo->insertarHistorico($radicados,$depeOrigen,$depeDest,$usDocOrigen,$usDocDest,$usCodOrigen,$usCodDest,$rol_orign, $rol_dest,$observacion,$tipoTx);
			return $rs;
		
	}

	function informar($radicados, $loginOrigen, $loginDest,$depOrigen, $depDest, $codUsOrigen,$codUsDest,$docUsOrigen,$docUsDest,$observa, $id_rol,$rolDest){
	    $this->modelo->informar($radicados, $loginOrigen, $loginDest , $depOrigen, $depDest, $codUsOrigen, $codUsDest,$docUsOrigen,$docUsDest,$observa, $id_rol,$rolDest);
	}

	function verInformadosRad($numrad){
	    $resultado=$this->modelo->verInformados($numrad);
	    print_r($resultado);
            $cont=count($resultado)-1;
	    if($resultado['error']==""){
		$retorno.="<table>";
	        for($i=0;$i<$cont;$i++){
			$depe_codi=$resultado[$i]['depe_codi'];
			$depe_nomb=$resultado[$i]['depe_nomb'];
			$data1=$resultado[$i]['data1'];
			$data3=$resultado[$i]['data3'];
			$retorno.="<tr  class='listado50'>
                            <td><input type='radio' name='borrarradicado' id='borrarradicado' value='$depe_codi' onclick='borrarInf()'></td>
                            <td class='listado50'><b>$data1</td><td class='listado50'> <center>$depe_codi</td><td class='listado50'>$data3</td></tr>";
	    	}
		$retorno.="</table>";
	    }
	    else{
		$retorno=$resultado['error'];
	    }
	    return $retorno;
	}

	function consultarTRD($numrad){
	    return $this->modelo->consultarTRD($numrad);
	}

	function comboSeries($dependencia){
	    $dataSeries=$this->modelo->consultarSeries($dependencia);
	    $cont=count($dataSeries)-1;
	    $retorno="<select id='seriedocf' onchange='return comboSubseries(this.value);' class='select'>\n<option value=0>Seleccione Serie</option>\n";
	    for($i=0;$i<$cont;$i++){
		$retorno.="\t\t<option>{$dataSeries[$i]['codigo']}-{$dataSeries[$i]['serie']}</option>\n";
	    }
	    $retorno.="</select>\n";
	    return $retorno;
	}

	function comboSubSeries($dependencia,$serie){
	    if($serie!=0){
		$div=explode('-',$serie);
		$serie=$div[0];
	    }
            $datasubSeries=$this->modelo->consultarSubSeries($dependencia,$serie);
            $cont=count($datasubSeries)-1;
            $retorno="<select id='subserdocf' onchange='return comboTipodoc(this.value);' class='select'>\n<option value=0>Seleccione SubSerie</option>\n";
            for($i=0;$i<$cont;$i++){
               $retorno.="\t\t<option>{$datasubSeries[$i]['codigo']}-{$datasubSeries[$i]['subserie']}</option>\n";
            }
            $retorno.="</select>\n";
            return $retorno;
        }
	

	function comboTDoc($dependencia,$serie,$subserie,$trad){
	    if($serie!=0){
                $div=explode('-',$serie);
                $serie=$div[0];
            }
	    if($subserie!=0){
                $div2=explode('-',$subserie);
                $subserie=$div2[0];
            }
            $dataTdoc=$this->modelo->consultarTDoc($dependencia,$serie,$subserie,$trad);
            $cont=count($dataTdoc)-1;
            $retorno="<select id='tdocf' class='select'>\n<option value=0>Seleccione Tipo Documento</option>\n";
            for($i=0;$i<$cont;$i++){
                $retorno.="\t\t<option>{$dataTdoc[$i]['codigo']}-{$dataTdoc[$i]['tipodoc']}</option>\n";
            }
            $retorno.="</select>\n";
            return $retorno;
        }

	function aplicarTRD($numerad,$codus,$docus,$dependencia,$id_rol,$serie,$subserie,$tdoc){
	    $series=explode('-',$serie);
	    $subseries=explode('-',$subserie);
	    $tdocs=explode('-',$tdoc);
	    $this->modelo->aplicarTRD($numerad,$codus,$docus,$dependencia,$series[0],$subseries[0],$tdocs[0]);
	    $observa="*Incluido TRD* {$series[1]}/{$subseries[1]}/{$tdocs[1]}";
	    $rad[]=$numerad;
	    $tx=32;
	    $a= $this->modelo->insertarHistorico($rad,$dependencia,$dependencia,$docus,$docus,$codus,$codus,$id_rol, $id_rol,$observa,$tx);
	}


	function tipoDocumental($tpr_sel){
	    $resultado=$this->modelo->tipoDocumental();
	    $retorno="<select class='select' name='tpr' id='tpr'>\n<option value=0>SIN TIPIFICAR</option>\n";
	    for($i=0;$i<count($resultado);$i++){
		if($tpr_sel==$resultado[$i]['id']){
		   $opt=" selected";
		}
		else{
		   $opt="";
		}
		$retorno.="<option value={$resultado[$i]['id']}$opt>{$resultado[$i]['tpr']}</option>\n";
	    }
	    $retorno.="</select>";
	    return $retorno;
	}
	/** 
	 * Limpia los atributos de la instancia referentes a la informacion del usuario
	 * @return   void
	 */
	function __destruct() {
			
	}
	
}

?>
