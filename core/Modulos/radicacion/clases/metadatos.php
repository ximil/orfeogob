<?php 
/**
 * Radicado es la clase encargada de gestionar las operaciones de metadatos del radicacion
 * @author Diego E. Rodriguez D.
 * @name Radicado
 * @version     1.0
 */
if (!$ruta_raiz)
    $ruta_raiz = '../../../..';
include_once "$ruta_raiz/core/Modulos/radicacion/modelo/modeloMetadatos.php";

//==============================================================================================
// CLASS radicado
//==============================================================================================

/**
 *  Objecto radicado
 */
class metadatos {
    //link para llamar el modelo
    private $modelo;
    //Camps del metadato
    private $DepeCodi;
    //Tipo de radicacion
    private $TipoRad;
    //NOmbre del metadato
    private $NombMD;
    //Data del tag
    private $DataTag;
    //Numero de Radicado
    private $RadiNume;

    function __construct($ruta_raiz){
	$this->modelo = new modeloMetadatos($ruta_raiz);
    }

    public function getDepeCodi(){
	return $this->DepeCodi;
    }

    public function setDepeCodi($DepeCodi){
	$this->DepeCodi=$DepeCodi;
    }

    public function getDataTag(){
	return $this->DataTag;
    }

    public function setDataTag($DataTag){
	$this->DataTag=$DataTag;
    }

    public function getTipoRad(){
	return $this->TipoRad;
    }

    public function setTipoRad($TipoRad){
	$this->TipoRad=$TipoRad;
    }

    public function getNombMD(){
	return $this->NombMD;
    }

    public function setNombMD($NombMD){
	$this->NombMD=$NombMD;
    }

    public function getRadiNume(){
        return $this->RadiNume;
    }

    public function setRadiNume($RadiNume){
        $this->RadiNume=$RadiNume;
    }

    function crearMetadatos(){
	return $this->modelo->crearMetaDatos($this->NombMD,$this->DataTag,$this->TipoRad,$this->DepeCodi);
    }

    function agregarTag($id){
	$this->modelo->agregarTag($id,$this->NombMD,$this->DataTag);
    }

    function modificarTag($id){
	$this->modelo->modificarTag($id,$this->DataTag);
    }

    function existeMetadatos(){
	return $this->modelo->existeMetadatos($this->TipoRad,$this->DepeCodi);
    }

    function listarMetadatos($meta){
	return $this->modelo->listarMetadatos($this->TipoRad,$meta,$this->DepeCodi);
    }

    function listarTags($id,$meta){
	return $this->modelo->listarTags($id,$meta);
    }

    function infoMetadatos($id){
	return $this->modelo->infoMetadatos($id);
    }

    function insertarInfoMeta($id){
	$this->modelo->insertarInfoMeta($id,$this->DataTag,$this->RadiNume);
    }

    function modificarMetadatos($id,$columna,$valor){
	$this->modelo->modificarMetadatos($id,$columna,$valor);
    }

    function metaXbusq(){
	return $this->modelo->metaXbusq();
    }
}
