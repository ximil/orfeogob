<?php 
/**
 * Se encarga de permitir o no  ver la imagen
 * @author Hardy Deimont Niño  Velasquez
 * @name dependencia
 * @version	1.0
 */
if (!$ruta_raiz)
    $ruta_raiz = '../..';
include_once "$ruta_raiz/core/Modulos/radicacion/modelo/modeloRadicado.php";

class LinkFile {

    private $rad; //Object RADICADO
    private $numRad; //numero de radicado validar

    function __construct($ruta_raiz) {
        $this->ruta_raiz = $ruta_raiz;
        $this->rad = new modeloRadicado($this->ruta_raiz);
        ;
    }
    public function getNumRad() {
        return $this->numRad;
    }

    public function setNumRad($numRad) {
        $this->numRad = $numRad;
    }

        /**
     * Retorna el valor correspondiente al 
     * resultado de la validacion
     * @numrad  Numero del Radicado a validar
     * @return   array  $vecRads resultado de la operacion de validacion
     */
    function validarPerm($Login, $usua_doc, $codi_nivel) {
        $numradi = $this->numRad;
        $radi = $this->rad->consultarPermRad($numradi);
        $usuaInformado = $this->rad->consultarPermRad($numradi);
        $rSegRad = $radi['seguridadRadicado'];
        $rNivelRad = $radi['nivelRadicado'];
        $rUsuaActu = $radi['usua_actu'];
        $rUsuaAnte = $radi['usua_ante'];
        $rDepeActu = $radi['depe_Actu'];
        $rPathImagen = $radi['pathImagen'];

        if ($rUsuaActu == $usua_doc || $usuaInformado == $usua_doc) {
            $verImg = "OK";
        } else {
            $exp = $this->rad->consulPermExp($numradi, $usua_doc);
            if ($exp['numExp']) {
                $verImg = "OK";
            } elseif ($rSegRad == 1) {
                if ($rDepeActu == '999' && $rUsuaAnte == $Login) {
                    $verImg = "OK";
                }
            } elseif ($codi_nivel >= $rNivelRad && $rSegRad == 0) {
                $verImg = "OK";
            }
        }
        if ($codi_nivel >= $rNivelRad && $rSegRad == 0) {
            $verImg = "OK";
        }
        $vecRadsD['verImg'] = $verImg;
        $vecRadsD['pathImagen'] = $rPathImagen;
        return $vecRadsD;
    }

}

?>
