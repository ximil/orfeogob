<?php
/**
 * Radicado es la clase encargada de gestionar las operaciones .Radicado
 * @author Hardy Deimont Niño  Velasquez
 * @name Radicado
 * @version	1.0
 */
if (!$ruta_raiz)
    $ruta_raiz = '../../../..';
include_once "$ruta_raiz/core/Modulos/radicacion/modelo/modeloRadicado.php";

//==============================================================================================
// CLASS radicado
//==============================================================================================

/**
 *  Objecto radicado
 */
class radicado {
    //link para llamar el modelo
    private $modelo;
    //tipo de radicacion
    private $tipRad;
    //Que tipo de derivacion es el radicado 0 asociado 1 Copia o Nuevo 2 Anexo
    private $radiTipoDeri;
    //Nivel de seguridad asociado al nivel de seguridad del usuario
    private $nivelRad;
    //La cuenta interna o referencia interna o externa del documento
    private $radiCuentai;
    //Referencia si iba en contra posicion a una ESP (Solo aplicaba en la Super)
    private $eespCodi;
    //Codigo del medio de recepcion
    private $mrecCodi;
    //Fecha de entrega del documento a radicar
    private $radiFechOfic;
    //Si el derivado tiene un numero de radicado
    private $radiNumeDeri;
    //Tipo de Identificacion usada por el tercero
    private $tdidCodi;
    //La descripcion de anexos
    private $descAnex;
    //Numero de hojas o folios del documento
    private $radiNumeHoja;
    //Pais donde se recibe los documentos
    private $radiPais;
    //Asunto del documento
    private $raAsun;
    //Dependencia Radicadora del documento
    private $radiDepeRadi;
    //Usuario actual el cual posee el documento virtual
    private $radiUsuaActu;
    //Numero del documento de la persona que posee el documento virtual
    private $radiUsuaActuDoc;
    //Rol de la persona que posee el documento virtual
    private $radiUsuaActurol;
    //Dependencia actual del documento
    private $radiDepeActu;
    //Rol del usuario radicador del documento
    private $rolid;
    //Carpeta virtual donde esta el documento
    private $carpCodi;
    //Numero de radicado del documento
    private $radiNumeRadi;
    private $trteCodi;
    private $radiNumeIden;
    //Fecha de radicacion del documento
    private $radiFechRadi;
    private $sgd_apli_codi;
    //Tipo documental asociado al documento
    private $tdocCodi;
    private $estaCodi;
    //Ruta del guardado de documento digitalizado
    private $radiPath;
    private $tipo;
    private $codigo_orfeo;
    //Cedula de la persona Remitente/Destinatario del documento
    private $cedula;
    //Nombres de la persona Remitente/Destinatario del documento
    private $nombre_remitente;
    //Apellidos de la persona Remitente/Destinatario del documento
    private $apellidos_remitente;
    //Primer Apellido de la persona Remitente/Destinatario del documento
    private $primer_apellidos;
    //Segundo Apellido de la persona Remitente/Destinatario del documento
    private $seg_apellidos;
    //Continente de Remitente/Destinatario del documento
    private $continente;
    //Pais de Remitente/Destinatario del documento
    private $pais;
    //Municipio de Remitente/Destinatario del documento
    private $muni;
    //Departamento de Remitente/Destinatario del documento
    private $depto;
    //Direccion del remitente  de Remitente/Destinatario del documento
    private $direccion_remitente;
    //Telefono del remitente  de Remitente/Destinatario del documento
    private $telefono_remitente;
    //Radicado asociado a este documento
    private $radi_asocc;
    //Correo electronico de Remitente/Destinatario del documento
    private $email;
    //Sigla de la empresa Remitente/Destinatario del documento
    private $sigla;
    //NIT de la empresa Remitente/Destinatario del documento
    private $nit = 1;
     //Codigo de ciudadano guardado el cual es Remitente/Destinatario del documento
    private $ciu;
    //Codigo de la empresa, organizacion o entidad guardado el cual es Remitente/Destinatario del documento
    private $oem;
    //Persona Encargada de firmar el documento
    private $dignatario;
    //Documento del funcionario el cual es Destinatario del documento
    private $docFun;
    //Fecha de entrega de documento para la radicación
    private $fechaOfic;
    //Guia con la que se Recibio o se Envio el documento
    private $radiGuia;
    /**
     *  VARIABLES DEL USUARIO ACTUAL
     */
    //Dependencia asignada a numeros de secuencia
    private $dependencia;
    //Docuemento del usuario radicador
    private $usuaDoc;
    //Usualogin del usuario
    private $usuaLogin;
    //Codigo de usuario radicador
    private $usuaCodi;
    private $codiNivel = 0;
    private $noDigitosRad = 6;
    private $ruta_raiz = 6;
    //Codigo de verificacion para consulta externa
    private $codi_verifica;
    private $pathAnexo;

    public function getPathAnexo() {
        return $this->pathAnexo;
    }

    public function setPathAnexo($pathAnexo) {
        $this->pathAnexo=$pathAnexo;
    }

    public function getRadiUsuaActuDoc() {
        return $this->radiUsuaActuDoc;
    }

    public function setRadiUsuaActuDoc($radiUsuaActuDoc) {
        $this->radiUsuaActuDoc = $radiUsuaActuDoc;
    }

    public function getRadiUsuaActurol() {
        return $this->radiUsuaActurol;
    }

    public function setRadiUsuaActurol($radiUsuaActurol) {
        $this->radiUsuaActurol = $radiUsuaActurol;
    }

    public function getRolid() {
        return $this->rolid;
    }

    public function setRolid($rolid) {
        $this->rolid = $rolid;
    }

    public function getTipo() {
        return $this->tipo;
    }

    public function setTipo($tipo) {
        $this->tipo = $tipo;
    }

    public function getCodigo_orfeo() {
        return $this->codigo_orfeo;
    }

    public function setCodigo_orfeo($codigo_orfeo) {
        $this->codigo_orfeo = $codigo_orfeo;
    }

    public function getCedula() {
        return $this->cedula;
    }

    public function setCedula($cedula) {
        $this->cedula = $cedula;
    }

    public function getNombre_remitente() {
        return $this->nombre_remitente;
    }

    public function setNombre_remitente($nombre_remitente) {
        $this->nombre_remitente = $nombre_remitente;
    }

    public function getApellidos_remitente() {
        return $this->apellidos_remitente;
    }

    public function setApellidos_remitente($apellidos_remitente) {
        $this->apellidos_remitente = $apellidos_remitente;
    }

    public function getPrimer_apellidos() {
        return $this->primer_apellidos;
    }

    public function setPrimer_apellidos($primer_apellidos) {
        $this->primer_apellidos = $primer_apellidos;
    }

    public function getSeg_apellidos() {
        return $this->seg_apellidos;
    }

    public function setSeg_apellidos($seg_apellidos) {
        $this->seg_apellidos = $seg_apellidos;
    }

    public function getContinente() {
        return $this->continente;
    }

    public function setContinente($continente) {
        $this->continente = $continente;
    }

    public function getPais() {
        return $this->pais;
    }

    public function setPais($pais) {
        $this->pais = $pais;
    }

    public function getMuni() {
        return $this->muni;
    }

    public function setMuni($muni) {
        $this->muni = $muni;
    }

    public function getDepto() {
        return $this->depto;
    }

    public function setDepto($depto) {
        $this->depto = $depto;
    }

    public function getDireccion_remitente() {
        return $this->direccion_remitente;
    }

    public function setDireccion_remitente($direccion_remitente) {
        $this->direccion_remitente = $direccion_remitente;
    }

    public function getTelefono_remitente() {
        return $this->telefono_remitente;
    }

    public function setTelefono_remitente($telefono_remitente) {
        $this->telefono_remitente = $telefono_remitente;
    }

    public function getRadi_asocc() {
        return $this->radi_asocc;
    }

    public function setRadi_asocc($radi_asocc) {
        $this->radi_asocc = $radi_asocc;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function getSigla() {
        return $this->sigla;
    }

    public function setSigla($sigla) {
        $this->sigla = $sigla;
    }

    public function getNit() {
        return $this->nit;
    }

    public function setNit($nit) {
        $this->nit = $nit;
    }

    public function getTipRad() {
        return $this->tipRad;
    }

    public function setTipRad($tipRad) {
        $this->tipRad = $tipRad;
    }

    public function getRadiTipoDeri() {
        return $this->radiTipoDeri;
    }

    public function setRadiTipoDeri($radiTipoDeri) {
        $this->radiTipoDeri = $radiTipoDeri;
    }

    public function getNivelRad() {
        return $this->nivelRad;
    }

    public function setNivelRad($nivelRad) {
        $this->nivelRad = $nivelRad;
    }

    public function getRadiCuentai() {
        return $this->radiCuentai;
    }

    public function setRadiCuentai($radiCuentai) {
        $this->radiCuentai = $radiCuentai;
    }

    public function getEespCodi() {
        return $this->eespCodi;
    }

    public function setEespCodi($eespCodi) {
        $this->eespCodi = $eespCodi;
    }

    public function getMrecCodi() {
        return $this->mrecCodi;
    }

    public function setMrecCodi($mrecCodi) {
        $this->mrecCodi = $mrecCodi;
    }

    public function getRadiFechOfic() {
        return $this->radiFechOfic;
    }

    public function setRadiFechOfic($radiFechOfic) {
        $this->radiFechOfic = $radiFechOfic;
    }

    public function getRadiNumeDeri() {
        return $this->radiNumeDeri;
    }

    public function setRadiNumeDeri($radiNumeDeri) {
        $this->radiNumeDeri = $radiNumeDeri;
    }

    public function getTdidCodi() {
        return $this->tdidCodi;
    }

    public function setTdidCodi($tdidCodi) {
        $this->tdidCodi = $tdidCodi;
    }

    public function getDescAnex() {
        return $this->descAnex;
    }

    public function setDescAnex($descAnex) {
        $this->descAnex = $descAnex;
    }

    public function getRadiNumeHoja() {
        return $this->radiNumeHoja;
    }

    public function setRadiNumeHoja($radiNumeHoja) {
        $this->radiNumeHoja = $radiNumeHoja;
    }

    public function getRadiPais() {
        return $this->radiPais;
    }

    public function setRadiPais($radiPais) {
        $this->radiPais = $radiPais;
    }

    public function getRaAsun() {
        return $this->raAsun;
    }

    public function setRaAsun($raAsun) {
        $this->raAsun = $raAsun;
    }

    public function getRadiDepeRadi() {
        return $this->radiDepeRadi;
    }

    public function setRadiDepeRadi($radiDepeRadi) {
        $this->radiDepeRadi = $radiDepeRadi;
    }

    public function getRadiUsuaActu() {
        return $this->radiUsuaActu;
    }

    public function setRadiUsuaActu($radiUsuaActu) {
        $this->radiUsuaActu = $radiUsuaActu;
    }

    public function getRadiDepeActu() {
        return $this->radiDepeActu;
    }

    public function setRadiDepeActu($radiDepeActu) {
        $this->radiDepeActu = $radiDepeActu;
    }

    public function getCarpCodi() {
        return $this->carpCodi;
    }

    public function setCarpCodi($carpCodi) {
        $this->carpCodi = $carpCodi;
    }

    public function getTrteCodi() {
        return $this->trteCodi;
    }

    public function setTrteCodi($trteCodi) {
        $this->trteCodi = $trteCodi;
    }

    public function getRadiNumeIden() {
        return $this->radiNumeIden;
    }

    public function setRadiNumeIden($radiNumeIden) {
        $this->radiNumeIden = $radiNumeIden;
    }

    public function getRadiFechRadi() {
        return $this->radiFechRadi;
    }

    public function setRadiFechRadi($radiFechRadi) {
        $this->radiFechRadi = $radiFechRadi;
    }

    public function getSgd_apli_codi() {
        return $this->sgd_apli_codi;
    }

    public function setSgd_apli_codi($sgd_apli_codi) {
        $this->sgd_apli_codi = $sgd_apli_codi;
    }

    public function getTdocCodi() {
        return $this->tdocCodi;
    }

    public function setTdocCodi($tdocCodi) {
        $this->tdocCodi = $tdocCodi;
    }

    public function getEstaCodi() {
        return $this->estaCodi;
    }

    public function setEstaCodi($estaCodi) {
        $this->estaCodi = $estaCodi;
    }

    public function getRadiPath() {
        return $this->radiPath;
    }

    public function setRadiPath($radiPath) {
        $this->radiPath = $radiPath;
    }

    public function getDependencia() {
        return $this->dependencia;
    }

    public function setDependencia($dependencia) {
        $this->dependencia = $dependencia;
    }

    public function getUsuaDoc() {
        return $this->usuaDoc;
    }

    public function setUsuaDoc($usuaDoc) {
        $this->usuaDoc = $usuaDoc;
    }

    public function getUsuaLogin() {
        return $this->usuaLogin;
    }

    public function setUsuaLogin($usuaLogin) {
        $this->usuaLogin = $usuaLogin;
    }

    public function getUsuaCodi() {
        return $this->usuaCodi;
    }

    public function setUsuaCodi($usuaCodi) {
        $this->usuaCodi = $usuaCodi;
    }

    public function getCodiNivel() {
        return $this->codiNivel;
    }

    public function setCodiNivel($codiNivel) {
        $this->codiNivel = $codiNivel;
    }

    public function getNoDigitosRad() {
        return $this->noDigitosRad;
    }

    public function setNoDigitosRad($noDigitosRad) {
        $this->noDigitosRad = $noDigitosRad;
    }

    public function getRadiNumeRadi() {
        return $this->radiNumeRadi;
    }

    public function setRadiNumeRadi($radiNumeRadi) {
        $this->radiNumeRadi = $radiNumeRadi;
    }

    public function getCiu(){
	return $this->ciu;
    }

    public function setCiu($ciu){
	$this->ciu=$ciu;
    }

    public function getOem(){
        return $this->oem;
    }

    public function setOem($oem){
        $this->oem=$oem;
    }

    public function getDignatario(){
        return $this->dignatario;
    }

    public function setDignatario($dignatario){
        $this->dignatario=$dignatario;
    }

    public function getDocFun(){
	return $this->docFun;
    }

    public function setDocFun($docFun){
	$this->docFun=$docFun;
    }

    public function getFechaOfic(){
	return $this->fechaOfic;
    }

    public function setFechaOfic($fechaOfic){
	$this->fechaOfic=$fechaOfic;
    }

    public function getRadiGuia(){
	return $this->radiGuia;
    }

    public function setRadiGuia($radiGuia){
	$this->radiGuia=$radiGuia;
    }

    public function getCodi_verifica(){
	return $this->codi_verifica;
    }

    public function setCodi_verifica($codi_verifica){
	$this->codi_verifica=$codi_verifica;
    }
        /* __construct function
         *
         */
	function __construct($ruta_raiz) {
	    $this->modelo = new modeloRadicado( $ruta_raiz );
	}


        /** Funcion encargada de hacer la radicacion
	 * @return numeric Retorna numero de radicado
	 */
    function radicar() {
        //$carpCodi=0;
        /* $numradicado=$this->modelo->radicar(
          $this->radiNumeRadi,  $this->radiNumeIden, $this->radiFechRadi,
          $this->estaCodi,  $this->dependencia, $this->usuaDoc,
          $this->usuaLogin, $this->usuaCodi
          ); */


        if (!$this->carpCodi) {
            if ($this->tipRad == 2)
                $this->carpCodi = 0;
            else
                $this->carpCodi = $this->tipRad;
        }
        $numradicado = $this->modelo->radicar($this->radiDepeRadi, $this->noDigitosRad, $this->tipRad, $this->tdocCodi, $this->eespCodi, $this->cedula, $this->nombre_remitente, $this->apellidos_remitente, $this->muni, $this->depto, $this->direccion_remitente, $this->telefono_remitente, $this->usuaCodi, $this->raAsun, $this->trteCodi, $this->tdidCodi, $this->radiDepeActu, $this->usuaDoc, $this->radiUsuaActuDoc, $this->radiCuentai, $this->radiUsuaActu, $this->rolid, $this->radiUsuaActurol, $this->nivelRad, $this->sgd_apli_codi, $this->radiTipoDeri, $this->descAnex, $this->mrecCodi, $this->codiNivel, $this->radiNumeDeri, $this->radiPath, $this->radiPais, $this->carpCodi, $this->radiNumeHoja,$this->fechaOfic,$this->radiGuia,$this->dependencia, $this->codi_verifica);
        $this->radiNumeRadi = $numradicado;
        return $numradicado;
    }

    /**
     * Funcion encargada de guardar datos del terceros del Remitente Destinatario
     *
     * @return void
     */
    function dir_drecciones() {
        $this->modelo->dir_drecciones($this->ciu, $this->radiNumeRadi, $this->muni, $this->depto, $this->direccion_remitente, $this->telefono_remitente, $this->nombre_remitente, $this->primer_apellidos . " " . $this->seg_apellidos, $this->nit, $this->oem,$this->pais,$this->continente,$this->dignatario,$this->docFun);
    }

	/**
	 * Consulta los datos de las tipoRadicado
	 * @return   void
	 */
	/*function consultar() {
			$rs = $this->modelo->consultar (  );
			return $rs;

	}


/**
     * Operacion ciudadano
     *
     */
    function ciudadano() {
        try {
            if ($this->validaUbi() == 'fail')
                throw new Exception("Los codigos de municipio y departamento son erroneros");
            if ($this->validar($this->nombre_remitente, 150, 1) == 'fail')
                throw new Exception("Nombre Remitente a sobrepasado el tama&ntilde;o caracteres");
            if ($this->validar($this->primer_apellidos, 50, 1) == 'fail')
                throw new Exception("primer apellido a sobrepasado el tama&ntilde;o caracteres");
            if ($this->validar($this->seg_apellidos, 50, 1) == 'fail')
                throw new Exception("segundo apellido a sobrepasado el tama&ntilde;o caracteres");
            if ($this->validar($this->direccion_remitente, 150, 1) == 'fail')
                throw new Exception("La direcion a sobrepasado el tama&ntilde;o caracteres");
            if ($this->validar($this->telefono_remitente, 50, 1) == 'fail')
                throw new Exception("email a sobrepasado el tama&ntilde;o caracteres");
            if ($this->validar($this->email, 50, 1) == 'fail')
                throw new Exception("cedula a sobrepasado el tama&ntilde;o caracteres");
            if ($this->validar($this->cedula, 50, 1) == 'fail')
                throw new Exception("telefono a sobrepasado el tama&ntilde;o caracteres");
            return $this->modelo->ciudano($this->nombre_remitente, $this->primer_apellidos, $this->seg_apellidos, $this->direccion_remitente, $this->telefono_remitente, $this->email, $this->muni, $this->depto, $this->cedula, $this->nit);
        } catch (Exception $e) {

            return $e->getMessage();
        }
    }

    /**
     *
     * @return type
     */
    function empresa() {
        try {
            if ($this->validaUbi() == 'fail')
                throw new Exception("Los codigos de municipio y departamento son erroneros");
            if ($this->validar($this->nombre_remitente, 250, 1) == 'fail')
                throw new Exception("Nombre empresa a sobrepasado el tama&ntilde;o caracteres");
            if ($this->validar($this->primer_apellidos, 250, 1) == 'fail')
                throw new Exception("representante legal a sobrepasado el tama&ntilde;o caracteres");
            if ($this->validar($this->seg_apellidos, 50, 1) == 'fail')
                throw new Exception("sigla a sobrepasado el tama&ntilde;o caracteres");
            if ($this->validar($this->direccion_remitente, 250, 1) == 'fail')
                throw new Exception("La direcion a sobrepasado el tama&ntilde;o caracteres");
            if ($this->validar($this->telefono_remitente, 150, 1) == 'fail')
                throw new Exception("Telefono a sobrepasado el tama&ntilde;o caracteres");
            if ($this->validar($this->email, 250, 1) == 'fail')
                throw new Exception("email a sobrepasado el tama&ntilde;o caracteres");
            if ($this->validar($this->nit, 14, 1) == 'fail')
                throw new Exception("Nit a sobrepasado el tama&ntilde;o caracteres");
            return $this->modelo->empresa($this->nombre_remitente, $this->primer_apellidos, $this->seg_apellidos, $this->direccion_remitente, $this->telefono_remitente, $this->email, $this->muni, $this->depto, $this->nit);
//            $data['ERROR']
        } catch (Exception $e) {

            return $e->getMessage();
        }
    }

    function validaUbi() {
        if ($this->modelo->valubi($this->depto, $this->muni) == 'fail'){
            return 'fail';
        }
        return 'ok';
    }

    function validar($nombre, $tama, $tipo) {
        // 1 para caracteres
        if ($tipo == 1) {
            $a = strlen($nombre);
            if ($a >= $tama) {
                return 'fail';
            }
        }
        // 2 validacion nuerica
        if ($tipo == 2) {
            if ($nombre >= $tama) {
                return 'fail';
            }
        }
        return 'ok';
    }

    /**
     * Consulta los datos de las tipoRadicado
     * @return   void
     */
    function consultar() {
        $rs = $this->modelo->consultar($this->radiNumeRadi);
        return $rs;
    }

    function consultar2($rad) {
        $rs = $this->modelo->consultar($rad);
        return $rs;
    }

    /**
     * Consulta los datos de las tipoRadicado
     * @return   void
     */
    function consultartpDoc() {
        $rs = $this->modelo->consultartpDoc();
        return $rs;
    }

    /**
     * Consulta los datos de las tipoRadicado
     * @return   void
     */
    function consultarUsuarioActual() {
        $rs = $this->modelo->consultarUsuarioActual();
        return $rs;
    }

    /* 	function buscar() {
      $combi = $this->modelo->Buscar( 0,$this->descripcion,1);
      if($combi  ["ERROR"]=='OK'){
      return $combi;
      }
      return 'No se encotro la Descripción';

      }

      /**
     * crea Anexo
     */

    function crearAnexo($radiNume, $filename, $file, $usuLogin, $descripcion, $usuDep) {
        $radiNume = trim($radiNume);
        $ruta = RUTA_BODEGA . substr($radiNume, 0, 4) . "/" . substr($radiNume, 4, 3) . "/docs/";
        $numAnexos = $this->modelo->numeroAnexos($radiNume) + 1;
        $maxAnexos = $this->modelo->maxRadicados($radiNume) + 1;
        $extension = substr($filename, strrpos($filename, ".") + 1);
        $numAnexo = ($numAnexos > $maxAnexos) ? $numAnexos : $maxAnexos;
        $nombreAnexo = $radiNume . substr("00000" . $numAnexo, - 5);
        if ($filename)
            $nombreAnexo2 = '1' . $radiNume . '_' . substr("00000" . $numAnexo, - 5) . "." . $extension;
        else
            $nombreAnexo2 = '';
        $subirArchivo = $this->modelo->subirArchivo($ruta . $nombreAnexo2, $file, '');
        if ($subirArchivo) {

            $tamanoAnexo = $subirArchivo / 1024; //tamano en kilobytes
            $tipoAnexo = $this->modelo->tipoAnexo($extension);
            $tipoAnexo = ($tipoAnexo) ? $tipoAnexo : "NULL";
            $combi = $this->modelo->crearAnexo($radiNume, $nombreAnexo, $tipoAnexo, $tamanoAnexo, $usuLogin, $descripcion, $numAnexo, $nombreAnexo, $extension, $usuDep, $nombreAnexo2);

            $codAnexo = $this->modelo->validarAnexo($nombreAnexo);

            if ($codAnexo) {
                return $codAnexo;
            } else {

                return "ERROR: Ocurrio un error al ingresar el anexo.";
            }
        } else {

            return "ERROR: Ocurrio un error al momento de subir el archivo asociado al anexo.";
        }
    }

    /**
     * Subir Imagen
     */
    function SubirImageDoc($radiNume, $filename, $file, $usuLogin, $usuDep) {

        $ruta = RUTA_BODEGA . $filename;
        $subirArchivo = $this->modelo->subirArchivo($ruta, $file, '');

        if ($subirArchivo) {
            $this->modelo->ActualizarRadicado($radiNume, $filename);
            $radicado = $this->modelo->vActRadicado($radiNume, $filename);

            if ($radicado) {
                return 'ok';
            } else {

                return "ERROR: Ocurrio un error al actualizar la imagen.";
            }
        } else {

            return "ERROR: Ocurrio un error al momento de subir el Documento.";
        }
    }

    function consultarDatosAnexos($tipo) {

        $datos = $this->modelo->consultarDatosAnexos($this->radiNumeRadi, $tipo);
        // print_r($datos);
        for ($index = 1; $index <= count($datos); $index++) {
            $rs [$index]['anex_codigo'] = $datos [$index] ['anex_codigo'];
	    $rs [$index]['anex_tipo'] = $datos [$index] ['anex_tipo'];
            $rs [$index]['path'] = $datos [$index] ['path'];
            $rs [$index]['salida'] = $datos [$index] ['salida'];
            $rs [$index]['codiTP'] = $datos [$index] ['codiTP'];
            $rs [$index]['anex_desc'] = $datos [$index] ['anex_desc'];
            $rs [$index]['anex_desc']=$datos [$index] ['anex_desc'];
            $radi = $datos [$index] ['salida'];
            if ($radi) {
                $rsRad = $this->consultar2($radi);
                $rs [$index] ['rad'] = $rsRad['RADI_NUME_RADI'];
                $rs [$index] ['pathr'] = $rsRad['RADI_PATH'];
                $rs [$index]['codiTP'] = $rsRad['tpDoc'];
                $rs [$index]['anex_desc'] = $rsRad['ANEX_DESC'];
            }
        }
//print_r($rs);

        return $rs;
    }

    function consultarHistorico() {

        $datos = $this->modelo->consultarHistorico($this->radiNumeRadi);
        $depe = $this->modelo->consultarDependencia();
        $usua = $this->modelo->consultarusuarios();
        for ($index = 1; $index <= count($datos); $index++) {
            $rs [$index]['fecha'] = $datos [$index] ['HIST_FECH'];
            $rs [$index]['depe'] = $depe[$datos [$index] ['DEPE_CODI']];
            $rs [$index]['usua'] = $usua[$datos [$index] ['USUA_CODI']];
            $rs [$index]['obs'] = $datos [$index] ['HIST_OBSE'];
            $rs [$index]['usuadest'] = $usua[$datos [$index] ['USUA_CODI_DEST']];
            $rs [$index]['depedest'] = $depe[$datos [$index] ['DEPE_CODI_DEST']];
            $rs [$index]['tx'] = $datos [$index] ['SGD_TTR_DESCRIP'];
        }
//print_r($rs);

        return $rs;
    }

        /**
         * Funcion encargada de consultar y entregar los permisos de radicacion
         *
         * @return string Menu de radicacion
         *
         */
	function permTRad($depus,$rolus){
	    list($trad_desc,$trad_cod)=$this->modelo->permTRad($depus,$rolus);
	    if($trad_cod[0]==0){
		echo "error";
	    }
	    $cont=count($trad_desc);
	    $retorno="<table><tr>";
	    for($i=0;$i<$cont;$i++){
		    $retorno.="<td><input type='radio' id='trad' name='trad' value={$trad_cod[$i]}></td>
			<td>{$trad_desc[$i]}<input type=\"hidden\" id=\"t{$trad_cod[$i]}\" name=\"t{$trad_cod[$i]}\" value=\"{$trad_desc[$i]}\"></td>\n";
	    }
	    $retorno.="</tr></table>";
	    return $retorno;
	}

	/**
         * Funcion encargada de consultar y entregar los permisos de radicacion para radicados anexos de respuesta o tramite
         *
         * @return string Menu de radicacion
         *
         */
	function permTRad2($depus,$rolus){
            list($trad_desc,$trad_cod)=$this->modelo->permTRad2($depus,$rolus);
            if($trad_cod[0]==0){
                echo "error";
            }
            $cont=count($trad_desc);
            $retorno="<table><tr>";
            for($i=0;$i<$cont;$i++){
                    $retorno.="<td><input type='radio' id='trad' name='trad' value={$trad_cod[$i]}></td>
                        <td>{$trad_desc[$i]}<input type=\"hidden\" id=\"t{$trad_cod[$i]}\" name=\"t{$trad_cod[$i]}\" value=\"{$trad_desc[$i]}\"></td>\n";
            }
            $retorno.="</tr></table>";
            return $retorno;
        }
        /* Funcion encargada de buscar y mostrar resultados relacionados con un numero de radicado en especifico
         *
         * @return string Resultados de la busqueda
         */
	function consultarRad($radicado,$fecha_ini,$fecha_fin){
	    $resultado=$this->modelo->consultarRad($radicado,$fecha_ini,$fecha_fin);
	    if($resultado['error']==""){
		$cont=count($resultado)-1;
                $retorno="<table class='borde_tab' style=\"border:1;border-collapse;width:90%;\">\n";
                $retorno.="<thead><tr class='titulos4'><td colspan=5>RESULTADOS DE LA BUSQUEDA</td></tr>";
                $retorno.="<tr class=\"titulo1\"><th>Radicado</th><th>Fecha de radicaci&oacute;n</th><th>Asunto</th><th>Remitente/Destinatario</th><th>Relaci&oacute;n</th></tr></thead><tbody>\n";
		for($i=0;$i<$cont;$i++){
		    if($i%2==0){
                       $atr="class=\"listado1\"";
               	    }else{
                       $atr="class=\"listado2\"";
               	    }
		    $retorno.="<tr $atr>
				<td rowspan=2>{$resultado[$i]['numrad']}</td>
				<td rowspan=2>{$resultado[$i]['fecha']}</td>
				<td rowspan=2>{$resultado[$i]['asunto']}</td>
				<td rowspan=2>{$resultado[$i]['destinatario']}</td>
				<td><a href='#' onclick='relacionar(2,{$resultado[$i]['numrad']})'>Asociado</a></td>
			      </tr>
			      <tr $atr><td><a href='#' onclick='relacionar(0,{$resultado[$i]['numrad']})'>Anexo</a></td></tr>
				\n";
		}
	    	$retorno.="</tbody></table>";
	    }
	    else{
		$retorno=$resultado['error'];
	    }
	    return $retorno;
	}
        /* Funcion encargada de consultar el por el nombre del tercero ciudadano o empresa con los numeros de radicados asociados a este
         *
         * @return string Resultados de la busqueda realizada
         */
	function consultarNombre($parametro, $fecha_ini,$fecha_fin){
	    $resultado1=$this->modelo->consultarCiudadano($parametro);
	    $retorno="<table style=\"border:1;border-collapse;width:90%;\" id='tabresultado'><thead>";
            $retorno.="<tr class='titulos4'><td colspan=5>RESULTADOS DE LA BUSQUEDA</td></tr></thead>";
            $retorno.="<tr class='titulo1'><th>Documento</th><th>Nombre</th><th>Direcci&oacute;n</th><th>Ciudad</th><th>Acci&oacute;n</th></tr>\n";
            if($resultado1['error']==""){
                $cont=count($resultado1)-1;
                for($i=0;$i<$cont;$i++){
                    $datarads=$this->modelo->dataTerceroRad($resultado1[$i]['id'],0,$fecha_ini,$fecha_fin);
                    if($datarads['error']==""){
			$cedula=($resultado1[$i]['cedula']=="")?"(sin_documento)":$resultado1[$i]['cedula'];
		        $retorno.="<tr class='titulos5'><td><a href='#' onclick='mostrarAsocc(0,{$resultado1[$i]['id']})'>$cedula</a></td><td>{$resultado1[$i]['nombre']} {$resultado1[$i]['apell1']} {$resultado1[$i]['apell2']}</td><td>{$resultado1[$i]['direccion']}</td><td>{$resultado1[$i]['municipio']}</td><td><a href='#' onclick='radicarNuevo(0,{$resultado1[$i]['id']})'>Nuevo</a></td></tr>\n";
                        $cont2=count($datarads)-1;
                        $retorno.="<tr id='ciu{$resultado1[$i]['id']}' style='display:none;'><td colspan=5><table style='width:100%;border:1px solid black;'>\n";
                        $retorno.="<tr class='titulos2'><td>No. Radicado</td><td>Fecha de<br>radicaci&oacute;n</td><td>Asunto</td><td>Relaci&oacute;n</td></tr>";
                        for($j=0;$j<$cont2;$j++){
			    $retorno.="<tr class='listado2'><td rowspan=2>{$datarads[$j]['numrad']}</td>
                                <td rowspan=2>{$datarads[$j]['fecha']}</td>
                                <td rowspan=2>{$datarads[$j]['asunto']}</td>
                                <td><a href='#' onclick='relacionar(2,{$datarads[$j]['numrad']})'>Asociado</a></td>
                                </tr>
                                <tr class='listado2'>
                                    <td><a href='#' onclick='relacionar(0,{$datarads[$j]['numrad']})'>Anexo</a></td>
                                </tr>\n";
                        }
                        $retorno.="</table></td></tr>\n";
                    }
		    else{
			$retorno.="<tr class='titulos5'><td>{$resultado1[$i]['cedula']}</td><td>{$resultado1[$i]['nombre']} {$resultado1[$i]['apell1']} {$resultado1[$i]['apell2']}</td><td>{$resultado1[$i]['direccion']}</td><td>{$resultado1[$i]['municipio']}</td><td><a href='#' onclick='radicarNuevo(0,{$resultado1[$i]['id']})'>Nuevo</a></td></tr>\n";
		    }
                }
            }
            $resultado2=$this->modelo->consultarOem($parametro);
	    if($resultado2['error']==""){
                $cont=count($resultado2)-1;
                for($i=0;$i<$cont;$i++){
                    $datarads=$this->modelo->dataTerceroRad($resultado2[$i]['id'],1,$fecha_ini,$fecha_fin);
                    if($datarads['error']==""){
			$nit=($resultado2[$i]['nit']=="")?"(sin_documento)":$resultado2[$i]['nit'];
			$retorno.="<tr class='titulos5'><td><a href='#' onclick='mostrarAsocc(1,{$resultado2[$i]['id']})'>$nit</a></td><td>{$resultado2[$i]['nombre']} {$resultado2[$i]['rep_legal']} {$resultado2[$i]['sigla']}</td><td>{$resultado2[$i]['direccion']}</td><td>{$resultado2[$i]['municipio']}</td><td><a href='#' onclick='radicarNuevo(1,{$resultado2[$i]['id']})'>Nuevo</a></td></tr>\n";
                        $cont2=count($datarads)-1;
                        $retorno.="<tr id='oem{$resultado2[$i]['id']}' style='display:none;'><td colspan=5><table style='width:100%;border:1px solid black;'>\n";
                        $retorno.="<tr class='titulos2'><td>No. Radicado</td><td>Fecha de<br>radicaci&oacute;n</td><td>Asunto</td><td>Relaci&oacute;n</td></tr>";
                        for($j=0;$j<$cont2;$j++){
                            $retorno.="<tr class='listado2'><td rowspan=2>{$datarads[$j]['numrad']}</td>
                                        <td rowspan=2>{$datarads[$j]['fecha']}</td>
                                        <td rowspan=2>{$datarads[$j]['asunto']}</td>
                                        <td><a href='#' onclick='relacionar(2,{$datarads[$j]['numrad']})'>Asociado</a></td>
                                        </tr>
                                        <tr class='listado2'>
                                            <td><a href='#' onclick='relacionar(0,{$datarads[$j]['numrad']})'>Anexo</a></td>
                                        </tr>\n";
                        }
                        $retorno.="</table></td></tr>\n";
                    }
		    else{
			$retorno.="<tr class='titulos5'><td>{$resultado2[$i]['nit']}</td><td>{$resultado2[$i]['nombre']} {$resultado2[$i]['rep_legal']} {$resultado2[$i]['sigla']}</td><td>{$resultado2[$i]['direccion']}</td><td>{$resultado2[$i]['municipio']}</td><td><a href='#' onclick='radicarNuevo(1,{$resultado2[$i]['id']})'>Nuevo</a></td></tr>\n";
		    }
                }
            }
            $retorno.="</table>";
            if($resultado1['error']!="" && $resultado2['error']!=""){
                $retorno="No se encontraron resultados";
            }
            return $retorno;
	}

        /*Funcion Encargada de buscar el tercero por numero de identificacion y los radicados asociados
         *
         * @return string Resultados de la busqueda
         */
	function consultarDoc($nodoc,$fecha_ini,$fecha_fin){
	    $resultado1=$this->modelo->consultarDocCiu($nodoc);
	    $retorno="<table style=\"border:1;border-collapse;width:90%;\" id='tabresultado'><thead>";
            $retorno.="<tr class='titulos4'><td colspan=5>RESULTADOS DE LA BUSQUEDA</td></tr></thead>";
	    $retorno.="<tr class='titulo1'><th>Documento</th><th>Nombre</th><th>Direcci&oacute;n</th><th>Ciudad</th><th>Acci&oacute;n</th></tr>";
	    if($resultado1['error']==""){
		$cont=count($resultado1)-1;
		for($i=0;$i<$cont;$i++){
		    $datarads=$this->modelo->dataTerceroRad($resultado1[$i]['id'],0,$fecha_ini,$fecha_fin);
		    if($datarads['error']==""){
			$retorno.="<tr class='titulos5'><td><a href='#' onclick='mostrarAsocc(0,{$resultado1[$i]['id']})'>{$resultado1[$i]['cedula']}</a></td><td>{$resultado1[$i]['nombre']} {$resultado1[$i]['apell1']} {$resultado1[$i]['apell2']}</td><td>{$resultado1[$i]['direccion']}</td><td>{$resultado1[$i]['municipio']}</td><td><a href='#' onclick='radicarNuevo(0,{$resultado1[$i]['id']})'>Nuevo</a></td></tr>\n";
			$cont2=count($datarads)-1;
                    	$retorno.="<tr id='ciu{$resultado1[$i]['id']}' style='display:none;'><td colspan=5><table style='width:100%;border:1px solid black'>\n";
                    	$retorno.="<tr class='titulos2'><td>No. Radicado</td><td>Fecha de<br>radicaci&oacute;n</td><td>Asunto</td><td>Relaci&oacute;n</td></tr>";
			for($j=0;$j<$cont2;$j++){
			    $retorno.="<tr class='listado2'><td rowspan=2>{$datarads[$j]['numrad']}</td>
				<td rowspan=2>{$datarads[$j]['fecha']}</td>
				<td rowspan=2>{$datarads[$j]['asunto']}</td>
				<td><a href='#' onclick='relacionar(2,{$datarads[$j]['numrad']})'>Asociado</a></td>
				</tr>
                                <tr class='listado2'>
                                    <td><a href='#' onclick='relacionar(0,{$datarads[$j]['numrad']})'>Anexo</a></td>
                                </tr>\n";
			}
			$retorno.="</table></td></tr>\n";
			//$retorno.="<tr class='titulo1'><td>Documento</td><td>Nombre</td><td>Direcci&oacute;n</td><td>Departamento</td><td>Ciudad</td><td>Acci&oacute;n</td></tr>";
		    }
		    else{
			$retorno.="<tr class='titulos5'><td>{$resultado1[$i]['cedula']}</td><td>{$resultado1[$i]['nombre']} {$resultado1[$i]['apell1']} {$resultado1[$i]['apell2']}</td><td>{$resultado1[$i]['direccion']}</td><td>{$resultado1[$i]['municipio']}</td><td><a href='#' onclick='radicarNuevo(0,{$resultado1[$i]['id']})'>Nuevo</a></td></tr>\n";
		    }
		}
	    }
	    $resultado2=$this->modelo->consultarDocOem($nodoc);
	    if($resultado2['error']==""){
                $cont=count($resultado2)-1;
                for($i=0;$i<$cont;$i++){
                    $datarads=$this->modelo->dataTerceroRad($resultado2[$i]['id'],1,$fecha_ini,$fecha_fin);
                    if($datarads['error']==""){
			$retorno.="<tr class='titulos5'><td><a href='#' onclick='mostrarAsocc(1,{$resultado2[$i]['id']})'>{$resultado2[$i]['nit']}</a></td><td>{$resultado2[$i]['nombre']} {$resultado2[$i]['rep_legal']} {$resultado2[$i]['sigla']}</td><td>{$resultado2[$i]['direccion']}</td><td>{$resultado2[$i]['municipio']}</td><td><a href='#' onclick='radicarNuevo(1,{$resultado2[$i]['id']})'>Nuevo</a></td></tr>\n";
                        $cont2=count($datarads)-1;
                        $retorno.="<tr id='oem{$resultado2[$i]['id']}' style='display:none;'><td colspan=5><table style='width:100%;border:1px solid black;'>\n";
                        $retorno.="<tr class='titulos2'><td>No. Radicado</td><td>Fecha de<br>radicaci&oacute;n</td><td>Asunto</td><td>Relaci&oacute;n</td></tr>";
                        for($j=0;$j<$cont2;$j++){
                            $retorno.="<tr class='listado2'><td rowspan=2>{$datarads[$j]['numrad']}</td>
					<td rowspan=2>{$datarads[$j]['fecha']}</td>
					<td rowspan=2>{$datarads[$j]['asunto']}</td>
					<td><a href='#' onclick='relacionar(2,{$datarads[$j]['numrad']})'>Asociado</a></td>
					</tr>
					<tr class='listado2'>
					    <td><a href='#' onclick='relacionar(0,{$datarads[$j]['numrad']})'>Anexo</a></td>
					</tr>\n";
                        }
                        $retorno.="</table></td></tr>\n";
                    }
		    else{
			$retorno.="<tr class='titulos5'><td>{$resultado2[$i]['nit']}</td><td>{$resultado2[$i]['nombre']} {$resultado2[$i]['rep_legal']} {$resultado2[$i]['sigla']}</td><td>{$resultado2[$i]['direccion']}</td><td>{$resultado2[$i]['municipio']}</td><td><a href='#' onclick='radicarNuevo(1,{$resultado2[$i]['id']})'>Nuevo</a></td></tr>\n";
		    }
                }
            }
	    $retorno.="</table>";
	    if($resultado1['error']!="" && $resultado2['error']!=""){
		$retorno="No se encontraron resultados";
	    }
	    return $retorno;
	}
        /* Funcion de listar el menu de continentes
         *
         * @return string El menu de continentes
         */
	function listarContinentes(){
	    return $this->modelo->listarContinentes();
	}
        /* Funcion encargada de listar el menu de paises asociado a un continente
         *
         * @return string El menu de paises
         */
	function listarPaises($continente){
            return $this->modelo->listarPaises($continente);
	}
        /**Funcion Encargada de listar el menu de departamentos asociados a un pais
         *
         * @return string El menu de departamentos
         */

	function listarDpto($pais){
            return $this->modelo->listarDpto($pais);
        }

        /**Funcion encargada de listar el menu de municipios asociados a un pais y departamento
         *
         * @return string El menu de municipios
         */

	function listarMunicipio($pais,$dpto){
            return $this->modelo->listarMunicipio($pais,$dpto);
        }

        /* Funcion encargada de crear un tercero en la base de datos
         *
         *
         * @return int Id del tercero creado
         */
	function crearTercero($tipo,$param1,$param2,$param3,$param4,$param5,$param6,$param7,$continente,$pais,$dpto,$muni){
	    if($tipo==0){
	    	$id=$this->modelo->insertarCiu($param1,$param2,$param3,$param4,$param5,$param6,$param7,$continente,$pais,$dpto,$muni);
	    }
	    else{
	    	$id=$this->modelo->insertarOem($param1,$param2,$param3,$param4,$param5,$param6,$param7,$continente,$pais,$dpto,$muni);
	    }
		return $id;
	}

        /* Funcion que consulta datos de un tercero al cual se va asignar un radicado nuevo
         *
         * @return array Datos del tercero
         */
	function traerDatosNuevo($tipo,$id){
            if($tipo==0){
                $retorno=$this->modelo->traerDCiuA($id);
            }
            elseif($tipo==1){
                $retorno=$this->modelo->traerDOemA($id);
            }
	    else{
		$retorno=$this->modelo->traerDFunA($id);
	    }
            return $retorno;
        }
	/* Trae datos de un radicado asociado a uno nuevo
         *
         * @return array Datos de radicado asociado
         *
         */

	function traerDatosRad($numrad){
	    return $this->modelo->traerDatosRad($numrad);
	}

        /*Funcion encargada de construir automaticamente una ubicacion topografica en combo
         *
         * @return string Menu de combo de localizacion
         */

	function constCombo($conti,$pais,$dpto,$muni){
	    if($conti=='' or $conti==null)
		$conti=0;
	    if($pais=='' or $pais==null)
                $pais=0;
	    if($dpto=='' or $dpto==null)
                $dpto=0;
	    if($muni=='' or $muni==null)
                $muni=0;
	    //Construye combo continente
	    $resultado=$this->listarContinentes();
            $comboCont="<select id='comboCont' class='select' onchange='cambioPais(this.value)'><option value=0>--Selecciones un Continente--</option>";
            $cont=count($resultado);
            for($i=0;$i<$cont;$i++){
            //Opcion para quemar dato en formulario
            	if($resultado[$i]['id']==$conti){
            	    $select=' selected';
                }else
            	    $select='';
        	$comboCont.="<option value={$resultado[$i]['id']}$select>{$resultado[$i]['continente']}</option>";
            }
            $comboCont.="</select>";
            unset($resultado);
	    //Combo pais
            $comboPais="<select id='comboPais' onchange='cambioDpto(this.value)' class='select'><option value=0>--Selecciones un Pais--</option>";
            $resultado=$this->listarPaises($conti);
            if($resultado['error']==""){
        	$cont=count($resultado)-1;
            	for($i=0;$i<$cont;$i++){
                    if($resultado[$i]['id']==$pais){
                   	$select=" selected";
                    }
                    else{
                    	$select="";}
                    $comboPais.="<option value={$resultado[$i]['id']}$select>{$resultado[$i]['pais']}</option>\n";
            	}
            }
            $comboPais.="</select>\n";
            unset($resultado);
	    //Combo Dpto
            $comboDpto="<select id='comboDpto' onchange='cambioMuni(this.value)' class='select'><option value=0>--Selecciones un Departamento--</option>";
            $resultado=$this->listarDpto($pais);
            if($resultado['error']==""){
            	$cont=count($resultado)-1;
            	for($i=0;$i<$cont;$i++){
                    if($resultado[$i]['id']==$dpto){
                    	$select=" selected='selected'";
                    }else
                    	$select="";
                    $comboDpto.="<option value={$resultado[$i]['id']}$select>{$resultado[$i]['dpto']}</option>\n";
            	}
            }
            $comboDpto.="</select>\n";
            unset($resultado);
	    //Combo municipio
            $comboMuni="<select id='comboMuni' class='select'><option value=0>--Selecciones un Municipio--</option>";
            $resultado=$this->listarMunicipio($pais,$dpto);
            if($resultado['error']==""){
            	$cont=count($resultado)-1;
            	for($i=0;$i<$cont;$i++){
                    if($resultado[$i]['id']==$muni){
                    	$select=" selected='selected'";
                    }else
                    	$select="";
                    $comboMuni.="<option value={$resultado[$i]['id']}$select>{$resultado[$i]['muni']}</option>\n";
            	}
            }
            $comboMuni.="</select>\n";
            unset($resultado);
	    return array($comboCont,$comboPais,$comboDpto,$comboMuni);
	}
	/* Funcion encargada de lista los medios de recepcion
         *
         * return string Menu de medio de recepcion
         */
	function comboMediorec(){
	    $resultado=$this->modelo->getMediorec();
	    $retorno="<select class='select' id='medrec'>\n";
	    $cont=count($resultado);
	    for($i=0;$i<$cont;$i++){
		$retorno.="<option value={$resultado[$i]['codi']}>{$resultado[$i]['desc']}</option>\n";
	    }
	    $retorno.="</select>";
	    return $retorno;
	}
        /* Funcion encargada de listar las dependencia de recepcion para documentos de entrada
         *
         * return string Menu de medio de dependencia
         */
	function depeRec(){
	    $resultado=$this->modelo->depeRec();
	    $retorno="<select class='select' id='depsel'>\n";
	    $cont=count($resultado);
	    $retorno.="<option value=0>--Seleccione una Dependencia--</option>";
            for($i=0;$i<$cont;$i++){
		if($resultado[$i]['nivseg']==1){
                    $retorno.="<option value={$resultado[$i]['depe_codi']}>{$resultado[$i]['depe']}</option>\n";
		}
		else{
		    $retorno.="<option value='-1'>{$resultado[$i]['depe']}</option>\n";
		}
            }
	    $retorno.="</select>";
	    return $retorno;
	}

        /* Funcion encargada de listar la dependencia de remitente para documento generados
         *
         * @return string Menu de dependencia
         */
	function depeDes($depus,$rolus){
            $resultado=$this->modelo->depeDes($depus,$rolus);
            $retorno="<select class='select' id='depsel'>\n";
            $cont=count($resultado);
	    $retorno.="<option value=0>--Seleccione una Dependencia--</option>";
            for($i=0;$i<$cont;$i++){
                if($resultado[$i]['nivseg']==1){
                    $retorno.="<option value={$resultado[$i]['depe_codi']} selected>{$resultado[$i]['depe']}</option>\n";
                }
                else{
                    $retorno.="<option value='-1' selected>{$resultado[$i]['depe']}</option>\n";
                }
            }
            $retorno.="</select>";
            return $retorno;
        }

        /* Funcion encargada de listar de consultar datos de un jefe de una dependencia especifica
         *
         * @return array Datos del jefe de la dependencia
         */
	function jefeRad($depus){
	    return $this->modelo->jefeRad($depus);
	}

        /* Funcion encargada de buscar el documento de un funcionario para radicacion de interna de documentos y radicados asociados
         *
         * @return string Resultado de la busqueda de funcionario por documento
         */

	function consultarDocFun($nodoc,$fecha_ini,$fecha_fin){
	    $resultado1=$this->modelo->consultarDocFun($nodoc);
	    $retorno="<table style=\"border:1;border-collapse;width:90%;\">";
            $retorno.="<tr class='titulos4'><td colspan=6>RESULTADOS DE LA BUSQUEDA</td></tr>";
            $retorno.="<tr class='titulo1'><td>Documento</td><td>Nombre</td><td>Dependencia</td><td>Rol</td><td>Ciudad</td><td>Acci&oacute;n</td></tr>";
	    if($resultado1['error']==""){
                $cont=count($resultado1)-1;
                for($i=0;$i<$cont;$i++){
                    $datarads=$this->modelo->dataTerceroRad($resultado1[$i]['cedula'],2,$fecha_ini,$fecha_fin);
                    if($datarads['error']==""){
                        $retorno.="<tr class='titulos5'><td><a href='#' onclick='mostrarAsocc(2,{$resultado1[$i]['cedula']});'>{$resultado1[$i]['cedula']}</a></td><td>{$resultado1[$i]['nombre']} {$resultado1[$i]['apell1']} {$resultado1[$i]['apell2']}</td><td>{$resultado1[$i]['direccion']}</td><td>{$resultado1[$i]['rol']}</td><td>{$resultado1[$i]['municipio']}</td><td><a href='#' onclick='radicarNuevo(2,{$resultado1[$i]['cedula']})'>Nuevo</a></td></tr>\n";
                        $cont2=count($datarads)-1;
                        $retorno.="<tr id='fun{$resultado1[$i]['cedula']}' style='display:none;'><td colspan=6><table style='width:100%;border:1px solid black;'>\n";
                        $retorno.="<tr class='titulos2'><td>No. Radicado</td><td>Fecha de<br>radicaci&oacute;n</td><td>Asunto</td><td>Relaci&oacute;n</td></tr>";
                        for($j=0;$j<$cont2;$j++){
                            $retorno.="<tr class='listado2'><td rowspan=2>{$datarads[$j]['numrad']}</td>
                                <td rowspan=2>{$datarads[$j]['fecha']}</td>
                                <td rowspan=2>{$datarads[$j]['asunto']}</td>
                                <td><a href='#' onclick='relacionar(2,{$datarads[$j]['numrad']})'>Asociado</a></td>
                                </tr>
                                <tr class='listado2'>
                                    <td><a href='#' onclick='relacionar(0,{$datarads[$j]['numrad']})'>Anexo</a></td>
                                </tr>\n";
                        }
                        $retorno.="</table></td></tr>\n";
                    }
                    else{
                        $retorno.="<tr class='titulos5'><td>{$resultado1[$i]['cedula']}</td><td>{$resultado1[$i]['nombre']} {$resultado1[$i]['apell1']} {$resultado1[$i]['apell2']}</td><td>{$resultado1[$i]['direccion']}</td><td>{$resultado1[$i]['rol']}</td><td>{$resultado1[$i]['municipio']}</td><td><a href='#' onclick='radicarNuevo(2,{$resultado1[$i]['cedula']})'>Nuevo</a></td></tr>\n";
                    }
                }
            }
	    else{
		$retorno="No se encontraron resultados";
	    }
	    return $retorno;
	}
        /* Funcion encargada de buscar el nombre de un funcionario para radicacion de interna de documentos y radicados asociados
         *
         * @return string Resultado de la busqueda de funcionario por nombre
         */
	function consultarFuncionario($parametro,$fecha_ini,$fecha_fin){
	    $resultado1=$this->modelo->consultarFuncionario($parametro);
	    $retorno="<table style=\"border:1;border-collapse;width:90%;\">";
            $retorno.="<tr class='titulos4'><td colspan=6>RESULTADOS DE LA BUSQUEDA</td></tr>";
            $retorno.="<tr class='titulo1'><td>Documento</td><td>Nombre</td><td>Dependencia</td><td>Rol</td><td>Ciudad</td><td>Acci&oacute;n</td></tr>";
            if($resultado1['error']==""){
                $cont=count($resultado1)-1;
                for($i=0;$i<$cont;$i++){
                    $datarads=$this->modelo->dataTerceroRad($resultado1[$i]['cedula'],2,$fecha_ini,$fecha_fin);
                    if($datarads['error']==""){
                        $retorno.="<tr class='titulos5'><td><a href='#' onclick='mostrarAsocc(2,{$resultado1[$i]['cedula']})'>{$resultado1[$i]['cedula']}</a></td><td>{$resultado1[$i]['nombre']} {$resultado1[$i]['apell1']} {$resultado1[$i]['apell2']}</td><td>{$resultado1[$i]['direccion']}</td><td>{$resultado1[$i]['rol']}</td><td>{$resultado1[$i]['municipio']}</td><td><a href='#' onclick='radicarNuevo(2,{$resultado1[$i]['cedula']})'>Nuevo</a></td></tr>\n";
                        $cont2=count($datarads)-1;
                        $retorno.="<tr id='fun{$resultado1[$i]['cedula']}' style='display:none;'><td colspan=6><table style='width:100%;border:1px solid black;'>\n";
                        $retorno.="<tr class='titulos2'><td>No. Radicado</td><td>Fecha de<br>radicaci&oacute;n</td><td>Asunto</td><td>Relaci&oacute;n</td></tr>";
                        for($j=0;$j<$cont2;$j++){
                            $retorno.="<tr class='listado2'><td rowspan=2>{$datarads[$j]['numrad']}</td>
                                <td rowspan=2>{$datarads[$j]['fecha']}</td>
                                <td rowspan=2>{$datarads[$j]['asunto']}</td>
                                <td><a href='#' onclick='relacionar(2,{$datarads[$j]['numrad']})'>Asociado</a></td>
                                </tr>
                                <tr class='listado2'>
                                    <td><a href='#' onclick='relacionar(0,{$datarads[$j]['numrad']})'>Anexo</a></td>
                                </tr>\n";
                        }
                        $retorno.="</table></td></tr>\n";
                    }
                    else{
                        $retorno.="<tr class='titulos5'><td>{$resultado1[$i]['cedula']}</td><td>{$resultado1[$i]['nombre']} {$resultado1[$i]['apell1']} {$resultado1[$i]['apell2']}</td><td>{$resultado1[$i]['direccion']}</td><td>{$resultado1[$i]['rol']}</td><td>{$resultado1[$i]['municipio']}</td><td><a href='#' onclick='radicarNuevo(2,{$resultado1[$i]['cedula']})'>Nuevo</a></td></tr>\n";
                    }
                }
            }
            else{
                $retorno="No se encontraron resultados";
            }
            return $retorno;
	}
        /* Menu de listado de dependencias a Informar desde radicacion
         *
         * @return string Menu de dependencias a informar
         */
	function depeXInformar(){
	    $resultado=$this->modelo->depeRec();
            $retorno="<select id='depeXinformar' name='depeXinformar' class='select' multiple='' size='5'>";
	    $cont=count($resultado);
	    for($i=0;$i<$cont;$i++){
                $retorno.="<option value={$resultado[$i]['depe_codi']}>{$resultado[$i]['depe']}</option>\n";
            }
            $retorno.="</select>";
            return $retorno;
	}

        /* Funcion encargada de hacer la transaccion de informar en el menu radicacion
         *
         *
         */
	function informar($radicados, $loginOrigen, $loginDest,$depOrigen, $depDest, $codUsOrigen,$codUsDest,$docUsOrigen,$docUsDest,$observa, $id_rol,$rolDest){
            $this->modelo->informar($radicados, $loginOrigen, $loginDest , $depOrigen, $depDest, $codUsOrigen, $codUsDest,$docUsOrigen,$docUsDest,$observa, $id_rol,$rolDest);
        }
	/* Funcion que visualiza los usuarios informados de la radicacion de documento
         *
         * return string Menu de las dependencias y usuarios informados
         */
	function verInformadosRad($numrad){
            $resultado=$this->modelo->verInformados($numrad);
            //print_r($resultado);
            $cont=count($resultado)-1;
            if($resultado['error']==""){
                $retorno.="<table>";
                for($i=0;$i<$cont;$i++){
                        $depe_codi=$resultado[$i]['depe_codi'];
                        $depe_nomb=$resultado[$i]['depe_nomb'];
                        $data1=$resultado[$i]['data1'];
                        $data5=$resultado[$i]['data5'];
                        $retorno.="<tr  class='listado50'>
                            <td><input type='radio' name='borrarradicado' id='borrarradicado' value='$depe_codi' onclick='borrarInf(this)'></td>
                            <td class='listado50'><b>$data1</td><td class='listado50'> <center>$depe_nomb</td><td class='listado50'>$data5</td></tr>";
                }
                $retorno.="</table>";
            }
            else{
                $retorno=$resultado['error'];
            }
            return $retorno;
        }

        /* Funcion encargada de borrar un informado en menu
         *
         *
         */
	function borrarInformado($radicados, $loginDest,$depOrigen, $depDest, $codUsOrigen,$codUsDest,$docUsOrigen,$docUsDest, $id_rol,$rolDest){
            $observa="Se borro Info. ($loginDest)";
	    $this->modelo->borrarInformado($radicados,$depOrigen, $depDest, $codUsOrigen,$codUsDest,$docUsOrigen,$docUsDest,$observa, $id_rol,$rolDest);
	}
        /*Funcion encargada de buscar otro tercero diferente a un radicado asociado a otro radicado
         *
         * @return busqueda
         */
	function asoNuevoTer($tipo,$busq,$parametro){
	    $retorno="<table style=\"border:1;border-collapse;width:90%;\" id='tabresultado'>";
	    $retorno.="<thead><tr class='titulos4'><td colspan=4>RESULTADOS DE LA BUSQUEDA</td></tr>";
            $retorno.="<tr class='titulo1'><td>Documento</td><td>Nombre</td><td>Direcci&oacute;n</td><td>Acci&oacute;n</td></tr></thead><tbody>";
            if($tipo==0 && $busq==0){
                $resultado=$this->modelo->consultarDocCiu($parametro);
	        if($resultado['error']!=""){
		    $conta=count($resultado)-1;
		    for($i=0;$i<$conta;$i++){
			$id=$resultado[$i]['id'];
			$tdid_codi=$resultado[$i]['tdid_codi'];
			$param1=$resultado[$i]['cedula'];
			$param2=$resultado[$i]['nombre'];
			$param3=$resultado[$i]['apell1'];
			$param4=$resultado[$i]['apell2'];
			$param5=$resultado[$i]['direccion'];
			$param6=$resultado[$i]['telefono'];
			$param7=$resultado[$i]['email'];
			$cont=$resultado[$i]['id_cont'];
			$pais=$resultado[$i]['id_pais'];
			$dpto=$resultado[$i]['dpto_codi'];
			$muni=$resultado[$i]['muni_codi'];
			$retorno.="<tr class='listado2'><td>$param1</td><td>$param2 $param3 $param4</td><td>$param5</td><td><a href='#' onclick=\"return asoccOtro('$id','$tdid_codi','$tipo','$param1','$param2','$param3','$param4','$param5','$param6','$param7','$cont','$pais','$dpto','$muni');\">USAR</a></td></tr>";
		    }
		}
            }
            elseif($tipo==1 && $busq==0){
                $resultado=$this->modelo->consultarDocOem($parametro);
		if($resultado['error']!=""){
                    $conta=count($resultado)-1;
                    for($i=0;$i<$conta;$i++){
                        $id=$resultado[$i]['id'];
                        $tdid_codi=$resultado[$i]['tdid_codi'];
                        $param1=$resultado[$i]['nit'];
                        $param2=$resultado[$i]['nombre'];
                        $param3=$resultado[$i]['rep_legal'];
                        $param4=$resultado[$i]['sigla'];
                        $param5=$resultado[$i]['direccion'];
                        $param6=$resultado[$i]['telefono'];
                        $param7=$resultado[$i]['email'];
                        $cont=$resultado[$i]['id_cont'];
                        $pais=$resultado[$i]['id_pais'];
                        $dpto=$resultado[$i]['dpto_codi'];
                        $muni=$resultado[$i]['muni_codi'];
			$retorno.="<tr class='listado2'><td>$param1</td><td>$param2 $param3 $param4</td><td>$param5</td><td><a href='#' onclick=\"return asoccOtro('$id','$tdid_codi','$tipo','$param1','$param2','$param3','$param4','$param5','$param6','$param7','$cont','$pais','$dpto','$muni');\">USAR</a></td></tr>";
                    }
                }
            }
            elseif($tipo==2 && $busq==0){
                $resultado=$this->modelo->busqDocFun($parametro);
                if($resultado['error']==""){
                    $conta=count($resultado)-1;
                    for($i=0;$i<$conta;$i++){
                        $id=$resultado[$i]['id'];
                        $tdid_codi=$resultado[$i]['tdid_codi'];
                        $param1=$resultado[$i]['cedula'];
                        $param2=$resultado[$i]['nombre'];
                        $param3=$resultado[$i]['apell1'];
                        $param4=$resultado[$i]['apell2'];
                        $param5=$resultado[$i]['direccion'];
                        $param6=$resultado[$i]['telefono'];
                        $param7=$resultado[$i]['email'];
                        $cont=$resultado[$i]['id_cont'];
                        $pais=$resultado[$i]['id_pais'];
                        $dpto=$resultado[$i]['dpto_codi'];
                        $muni=$resultado[$i]['muni_codi'];
			$retorno.="<tr class='listado2'><td>$param1</td><td>$param2 $param3 $param4</td><td>$param5</td><td><a href='#' onclick=\"return asoccOtro('$id','$tdid_codi','$tipo','$param1','$param2','$param3','$param4','$param5','$param6','$param7','$cont','$pais','$dpto','$muni');\">USAR</a></td></tr>";
                    }
		}
            }
	    elseif($tipo==0 && $busq==1){
		$resultado=$this->modelo->consultarCiudadano($parametro);
                if($resultado['error']==""){
                    $conta=count($resultado)-1;
                    for($i=0;$i<$conta;$i++){
                        $id=$resultado[$i]['id'];
                        $tdid_codi=$resultado[$i]['tdid_codi'];
                        $param1=$resultado[$i]['cedula'];
                        $param2=$resultado[$i]['nombre'];
                        $param3=$resultado[$i]['apell1'];
                        $param4=$resultado[$i]['apell2'];
                        $param5=$resultado[$i]['direccion'];
                        $param6=$resultado[$i]['telefono'];
                        $param7=$resultado[$i]['email'];
                        $cont=$resultado[$i]['id_cont'];
                        $pais=$resultado[$i]['id_pais'];
                        $dpto=$resultado[$i]['dpto_codi'];
                        $muni=$resultado[$i]['muni_codi'];
			$retorno.="<tr class='listado2'><td>$param1</td><td>$param2 $param3 $param4</td><td>$param5</td><td><a href='#' onclick=\"return asoccOtro('$id','$tdid_codi','$tipo','$param1','$param2','$param3','$param4','$param5','$param6','$param7','$cont','$pais','$dpto','$muni');\">USAR</a></td></tr>";
                    }
		}
            }
            elseif($tipo==1 && $busq==1){
                $resultado=$this->modelo->consultarOem($parametro);
		if($resultado['error']==""){
                    $conta=count($resultado)-1;
                    for($i=0;$i<$conta;$i++){
                        $id=$resultado[$i]['id'];
                        $tdid_codi=$resultado[$i]['tdid_codi'];
                        $param1=$resultado[$i]['nit'];
                        $param2=$resultado[$i]['nombre'];
                        $param3=$resultado[$i]['rep_legal'];
                        $param4=$resultado[$i]['sigla'];
                        $param5=$resultado[$i]['direccion'];
                        $param6=$resultado[$i]['telefono'];
                        $param7=$resultado[$i]['email'];
                        $cont=$resultado[$i]['id_cont'];
                        $pais=$resultado[$i]['id_pais'];
                        $dpto=$resultado[$i]['dpto_codi'];
                        $muni=$resultado[$i]['muni_codi'];
			$retorno.="<tr class='listado2'><td>$param1</td><td>$param2 $param3 $param4</td><td>$param5</td><td><a href='#' onclick=\"return asoccOtro('$id','$tdid_codi','$tipo','$param1','$param2','$param3','$param4','$param5','$param6','$param7','$cont','$pais','$dpto','$muni');\">USAR</a></td></tr>";
                    }
                }
            }
            elseif($tipo==2 && $busq==1){
                $resultado=$this->modelo->consultarFuncionario($parametro);
                if($resultado['error']==""){
                    $conta=count($resultado)-1;
                    for($i=0;$i<$conta;$i++){
                        $id=$resultado[$i]['id'];
                        $tdid_codi=$resultado[$i]['tdid_codi'];
                        $param1=$resultado[$i]['cedula'];
                        $param2=$resultado[$i]['nombre'];
                        $param3=$resultado[$i]['apell1'];
                        $param4=$resultado[$i]['apell2'];
                        $param5=$resultado[$i]['direccion'];
                        $param6=$resultado[$i]['telefono'];
                        $param7=$resultado[$i]['email'];
                        $cont=$resultado[$i]['id_cont'];
                        $pais=$resultado[$i]['id_pais'];
                        $dpto=$resultado[$i]['dpto_codi'];
                        $muni=$resultado[$i]['muni_codi'];
                        $retorno.="<tr class='listado2'><td>$param1</td><td>$param2 $param3 $param4</td><td>$param5</td><td><a href='#' onclick=\"return asoccOtro('$id','$tdid_codi','$tipo','$param1','$param2','$param3','$param4','$param5','$param6','$param7','$cont','$pais','$dpto','$muni');\">USAR</a></td></tr>";
                    }
		}
            }
	    elseif($tipo==3 && $busq==0){
                $res1=$this->modelo->consultarDocCiu($parametro);
                if($res1['error']==""){
                    $conta=count($res1)-1;
                    for($i=0;$i<$conta;$i++){
                        $id=$res1[$i]['id'];
                        $tdid_codi=$res1[$i]['tdid_codi'];
                        $param1=$res1[$i]['cedula'];
                        $param2=$res1[$i]['nombre'];
                        $param3=$res1[$i]['apell1'];
                        $param4=$res1[$i]['apell2'];
                        $param5=$res1[$i]['direccion'];
                        $param6=$res1[$i]['telefono'];
                        $param7=$res1[$i]['email'];
                        $cont=$res1[$i]['id_cont'];
                        $pais=$res1[$i]['id_pais'];
                        $dpto=$res1[$i]['dpto_codi'];
                        $muni=$res1[$i]['muni_codi'];
                        $retorno.="<tr class='listado2'><td>$param1</td><td>$param2 $param3 $param4</td><td>$param5</td><td><a href='#' onclick=\"return asoccOtro('$id','$tdid_codi','0','$param1','$param2','$param3','$param4','$param5','$param6','$param7','$cont','$pais','$dpto','$muni');\">USAR</a></td></tr>";
                    }
                }
		$res2=$this->modelo->consultarDocOem($parametro);
		if($res2['error']==""){
                    $conta=count($res2)-1;
                    for($i=0;$i<$conta;$i++){
                        $id=$res2[$i]['id'];
                        $tdid_codi=$res2[$i]['tdid_codi'];
                        $param1=$res2[$i]['cedula'];
                        $param2=$res2[$i]['nombre'];
                        $param3=$res2[$i]['apell1'];
                        $param4=$res2[$i]['apell2'];
                        $param5=$res2[$i]['direccion'];
                        $param6=$res2[$i]['telefono'];
                        $param7=$res2[$i]['email'];
                        $cont=$res2[$i]['id_cont'];
                        $pais=$res2[$i]['id_pais'];
                        $dpto=$res2[$i]['dpto_codi'];
                        $muni=$res2[$i]['muni_codi'];
                        $retorno.="<tr class='listado2'><td>$param1</td><td>$param2 $param3 $param4</td><td>$param5</td><td><a href='#' onclick=\"return asoccOtro('$id','$tdid_codi','1','$param1','$param2','$param3','$param4','$param5','$param6','$param7','$cont','$pais','$dpto','$muni');\">USAR</a></td></tr>";
                    }
                }
		if($res2['error']!="" && $res1['error']!=""){
		    $resultado['error']="Error no encontro ningun";
		}
		else{
		    $resultado['error']="";
		}
            }
	    elseif($tipo==3 && $busq==1){
                $res1=$this->modelo->consultarCiudadano($parametro);
                if($res1['error']==""){
                    $conta=count($res1)-1;
                    for($i=0;$i<$conta;$i++){
                        $id=$res1[$i]['id'];
                        $tdid_codi=$res1[$i]['tdid_codi'];
                        $param1=$res1[$i]['cedula'];
                        $param2=$res1[$i]['nombre'];
                        $param3=$res1[$i]['apell1'];
                        $param4=$res1[$i]['apell2'];
                        $param5=$res1[$i]['direccion'];
                        $param6=$res1[$i]['telefono'];
                        $param7=$res1[$i]['email'];
                        $cont=$res1[$i]['id_cont'];
                        $pais=$res1[$i]['id_pais'];
                        $dpto=$res1[$i]['dpto_codi'];
                        $muni=$res1[$i]['muni_codi'];
                        $retorno.="<tr class='listado2'><td>$param1</td><td>$param2 $param3 $param4</td><td>$param5</td><td><a href='#' onclick=\"return asoccOtro('$id','$tdid_codi','0','$param1','$param2','$param3','$param4','$param5','$param6','$param7','$cont','$pais','$dpto','$muni');\">USAR</a></td></tr>";
                    }
                }
		$res2=$this->modelo->consultarOem($parametro);
		if($res2['error']==""){
                    $conta=count($res2)-1;
                    for($i=0;$i<$conta;$i++){
                        $id=$res2[$i]['id'];
                        $tdid_codi=$res2[$i]['tdid_codi'];
                        $param1=$res2[$i]['cedula'];
                        $param2=$res2[$i]['nombre'];
                        $param3=$res2[$i]['apell1'];
                        $param4=$res2[$i]['apell2'];
                        $param5=$res2[$i]['direccion'];
                        $param6=$res2[$i]['telefono'];
                        $param7=$res2[$i]['email'];
                        $cont=$res2[$i]['id_cont'];
                        $pais=$res2[$i]['id_pais'];
                        $dpto=$res2[$i]['dpto_codi'];
                        $muni=$res2[$i]['muni_codi'];
                        $retorno.="<tr class='listado2'><td>$param1</td><td>$param2 $param3 $param4</td><td>$param5</td><td><a href='#' onclick=\"return asoccOtro('$id','$tdid_codi','1','$param1','$param2','$param3','$param4','$param5','$param6','$param7','$cont','$pais','$dpto','$muni');\">USAR</a></td></tr>";
                    }
                }
		if($res2['error']!="" && $res1['error']!=""){
                    $resultado['error']="Error no encontro ningun";
                }
                else{
                    $resultado['error']="";
                }
            }
	    $retorno.="</tbody></table>";
	    if($resultado['error']!=""){
		$retorno="No se encontraron resultados";
	    }
	    return $retorno;
        }


/*	function buscar() {
		$combi = $this->modelo->Buscar( 0,$this->descripcion,1);
			if($combi  ["ERROR"]=='OK'){
				return $combi;
			}
		return 'No se encotro la Descripción';

	}

	/**
	 * crea usuario
	 */
/*	function crear() {
		$combi = $this->modelo->Buscar( $this->codigo,'',0);
		if($combi  ["ERROR"]!='OK'){
			$combi2 = $this->modelo->Buscar( 0,$this->descripcion,0);
			if($combi2  ["ERROR"]!='OK'){
		 	return $this->modelo->crear($this->codigo,$this->descripcion, $this->fechaInicio, $this->fechaFin);
			}
			else
			return 'Ya existe la Descripción';
		}
		else
		return 'Ya existe el codigo';
	}
	/**
	 * actualiza  usuario
	 */
/*	function actualizar() {

			$combi2 = $this->modelo->Buscar( 0,$this->descripcion);
			if($combi2  ["ERROR"]!='OK'||$combi2  [0]["DESCRIP"]==$this->descripcion){
		 	return $this->modelo->actualizar($this->codigo,$this->descripcion, $this->fechaInicio, $this->fechaFin);
			}
			else
			return 'Ya existe la Descripción';


	}

	/**
	 * Limpia los atributos de la instancia referentes a la informacion del usuario
	 * @return   void
	 */
	function __destruct() {

	}


	function agregFactura($tipofac,$remfact,$descfact){
	    $this->modelo->agregFactura($this->radiNumeRadi,$tipofac,$remfact,$descfact);
	}

	function genCodiVerifica($len=6){
	    $perm=$this->modelo->genCodiVerifica($this->tipRad);
	    if($perm==1){
	    $an = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdfeghijklmopqrstuvwxyz-)!=(+#@&?";
    	    $su = strlen($an) - 1;
            $patmay="[A-Z]";
            $patmin="[a-z]";
            $patnum="[0-9]";
            $patschr="[\!\@\#\&\(\)\-\+\=\?\/\\\\]";
            $patbusq="/^($patmay+$patmin+$patnum+$patschr+)|($patmay+$patmin+$patschr+$patnum+)|($patmay+$patschr+$patmin+$patnum+)|($patmay+$patschr+$patnum+$patmin+)|($patmay+$patnum+$patmin+$patschr+)|($patmay+$patnum+$patschr+$patmin+)|($patmin+$patmay+$patschr+$patnum+)|($patmin+$patmay+$patnum+$patschr+)|($patmin+$patschr+$patnum+$patmay+)|($patmin+$patschr+$patmay+$patnum+)|($patmin+$patnum+$patschr+$patmay+)|($patmin+$patnum+$patmay+$patschr+)|($patnum+$patmin+$patmay+$patschr+)|($patnum+$patmin+$patschr+$patmay+)|($patnum+$patmay+$patmin+$patschr+)|($patnum+$patmay+$patschr+$patmin+)|($patnum+$patschr+$patmay+$patmin+)|($patnum+$patschr+$patmin+$patmay+)|($patschr+$patmin+$patmay+$patnum+)|($patschr+$patmin+$patnum+$patmay+)|($patschr+$patmay+$patnum+$patmin+)|($patschr+$patmay+$patmin+$patnum+)|($patschr+$patnum+$patmay+$patmin+)|($patschr+$patnum+$patmin+$patmay+)/";
            $j=0;
            do{
    		$res="";
        	for($i=0;$i<$len;$i++){
            	    $res.=substr($an, rand(0, $su), 1);
        	}
        	$j++;
            }while(!(preg_match($patbusq,$res)) && $j<50);
	    }
	    else{
		$res='';
	    }
	    return $res;
        }

	function anexosTipo(){
	    return $this->modelo->anexosTipo();
	}

	function dataUbicados(){
	    return $this->modelo->dataUbicados($this->continente,$this->pais,$this->depto,$this->muni);
	}

	function sandboxAnexo(){
	    return $this->modelo->sandboxAnexo($this->radiNumeRadi,$this->radiNumeDeri,$this->usuaLogin,$this->radiDepeRadi,$this->raAsun);
	}

	/*
	 *
	 */
	function crearAnexo2($numrad,$file,$filename,$desc,$login,$depus,$anexTipo,$size,$tpr){
	    include_once "../../../../core/config/config-inc.php";
	    $radiNume = trim($numrad);

        /* Obtiene la dependencia de radicacion del documento para identificar correctamente la ruta de la carpeta de documentos */
        $datos_radicado = $this->traerDatosRad($radiNume);
        $dependencia_radicacion = $datos_radicado['radi_depe_radi'];
        /* Fin de la obtención de la dependencia de radicacion */

            $ruta = RUTA_BODEGA . substr($radiNume, 0, 4) . "/" . $dependencia_radicacion . "/docs/";
            $numAnexos = $this->modelo->numeroAnexos($radiNume) + 1;
            $maxAnexos = $this->modelo->maxRadicados($radiNume) + 1;
            $extension = strtolower(end(explode(".",$filename)));
            $numAnexo = ($numAnexos > $maxAnexos) ? $numAnexos : $maxAnexos;
	    $anexArch = $radiNume.'_'.str_pad($numAnexo, 5, "0", STR_PAD_LEFT).'.'.$extension;
	    $anexNomb = $radiNume.str_pad($numAnexo, 5, "0", STR_PAD_LEFT);

	    /* Se verifica si no existe la carpeta y se intenta hacer su creación */
	    if(!file_exists($ruta)){
            mkdir($ruta,0777,true);
	    }
	    if(move_uploaded_file($file,$ruta.$anexArch)){
    		$val=$this->modelo->insertarAnexo($radiNume,$anexNomb,$anexArch,$depus,$anexTipo,$desc,$login,$numAnexo,$size,$tpr);
    		if($val==1){
    		    //RollBack del archivo copiado
    		    system("rm -f $anexArch");
    		    $retorno='ERROR: No se realizo el registro a la base de datos.';
    		}
    		else{
    		    $retorno=$anexNomb;
    		}
	    }
	    else{
		    $retorno='ERROR: No se pudo copiar a la bodega';
	    }
	    return $retorno;
	}

	function traerDescAnexo($anexCodigo){
	    return $this->modelo->traerDescAnexo($anexCodigo);
	}

	function modAnexFile($anexCodigo,$file,$filename,$desc,$anexTipo,$size,$tpr){
	    include_once "../../../../core/config/config-inc.php";
            $radiNume = trim($anexCodigo);
	    $oldfile=$this->modelo->traerFile($anexCodigo);
            $ruta = RUTA_BODEGA . substr($radiNume, 0, 4) . "/" . substr($radiNume, 4, 3) . "/docs/";
	    $bak=date("Ymdhh24iiss");
	    $oldfilePath=$ruta.$oldfile;
	    $oldfileNew=$ruta.$bak.'-'.$oldfile;
	    //El archivo anexado a reemplazar no se borra se renombra, por trazabilidad
	    system("mv $oldfilePath $oldfileNew");
            $extension = strtolower(end(explode(".",$filename)));
            $anexArch = substr($anexCodigo,0,14).'_'.substr($anexCodigo,14).'.'.$extension;
            $anexNomb = $anexCodigo;
            if(move_uploaded_file($file,$ruta.$anexArch)){
                $val=$this->modelo->actualFileAnex($anexNomb,$anexArch,$desc,$anexTipo,$size,$tpr);
                if($val==1){
                    //RollBack del archivo copiado
                    system("rm -f $anexArch");
		    //Si el nuevo archivo no se pudo mover a la bodega y el registro no se hizo en la base de datos se revierte el backup de archivo
                    system("mv $oldfileNew $oldfilePath");
                    $retorno='ERROR: No se realizo el registro a la base de datos.';
                }
                else{
                    $retorno=$anexNomb;
                }
            }
            else{
		//Si el nuevo archivo no se pudo mover a la bodega y el registro no se hizo en la base de datos se revierte el backup de archivo
		system("mv $oldfileNew $oldfilePath");
                $retorno='ERROR: No se pudo copiar a la bodega';
            }
            return $retorno;
	}

	function modAnexDesc($anexCodigo,$desc,$tpr){
	    return $this->modelo->modAnexDesc($anexCodigo,$desc,$tpr);
	}

	function borrarAnexo($anexNume){
	    return $this->modelo->borrarAnexo($anexNume);
	}

	function genDocumentoAccion(){
	    include_once "../../../../core/config/config-inc.php";
	    $resultado=$this->modelo->verRadicadoAnexo($this->radiNumeRadi);
	    $numrad=$this->radiNumeRadi;
	    $archivo=$this->pathAnexo;
	    $radi_padre=$resultado['radi_padre'];
	    $anexCodigo=$resultado['anex_codigo'];
	    $path=$resultado['path'];
	    if($path!='sinimagen'){
		//Si existe un pdf se hace backup
		$tmp=RUTA_BODEGA.substr($numrad,0,4).'/'.substr($numrad,4,3).'/'.date("Ymdhh24iiss").'_'.$numrad.'.pdf';
		$tmp2=RUTA_BODEGA.$path;
		system("mv $tmp2 $tmp");
	    }
	    $ruta=substr($radi_padre, 0, 4) . "/" . substr($radi_padre, 4, 3) . "/docs/";
	    $filename='1'.substr($anexCodigo,0,14).'_'.substr($anexCodigo,14).'.pdf';
	    if(file_exists(RUTA_BODEGA.$ruta)){
	        $archivo2=RUTA_BODEGA.$ruta.$filename;
	        system("mv $archivo $archivo2");
		$tamano=filesize($archivo2)/1024;
		$script="pdfinfo $archivo2| grep Pages|awk '{print $2}'";
		exec($script,$data);
		$this->modelo->asociarImagenAnexo($this->radiNumeRadi,'/'.$ruta,$filename,$tamano,$data[0]);
	    }
	    else{
		$resultado['error']='ERROR: Ruta en bodega no existe '.RUTA_BODEGA.$ruta;
	    }
	    return $resultado;
	}

	function datosradCarpeta(){
	   return $this->modelo->datosradCarpeta($this->radiNumeRadi);
	}

	function firmarDocumento($codAnexo,$new_arch){
	    include_once "../../../../core/config/config-inc.php";
	    $infoAnexo=$this->modelo->verAnexoFirmado($codAnexo);
	    $radi_padre=$infoAnexo['radi_padre'];
	    $archivo=$infoAnexo['archivo'];
	    $ruta=RUTA_BODEGA.substr($radi_padre,0,4).'/'.substr($radi_padre, 4, 3).'/docs/';
	    if($infoAnexo['radi_salida']!=''){
		$archivo_bak=RUTA_BODEGA.substr($infoAnexo['radi_salida'],0,4).'/'.substr($infoAnexo['radi_salida'], 4, 3).'/'.date("Ymdhh24iiss").'_'.$infoAnexo['radi_salida'].'.pdf';
	    }
	    else{
		$archivo_bak=$ruta.date("Ymdhh24iiss").'_'.$archivo;
	    }
	    exec("mv $ruta$archivo $archivo_bak");
	    if(file_exists($archivo_bak)){
		$size=filesize($new_arch)/1024;
		$this->modelo->anexoFirmado($anexCodigo,$size);
		exec("cp $new_arch $ruta$archivo");
		if(file_exists($ruta.$archivo)){
		   $this->modelo->anexoFirmado($anexCodigo,$size);
		}
		else{
		    $infoAnexo['error']='No se pudo mover el archivo';
		}
	    }
	    else{
		$infoAnexo['error']="No se pudo mover el archivo";
	    }
	    return $infoAnexo;
	}

	function actuRadiPath($docuOrigen){
	    return $this->modelo->actuRadiPath($this->radiNumeRadi,$docuOrigen);
	}

}
?>
