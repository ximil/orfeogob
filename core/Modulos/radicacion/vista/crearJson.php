<?php //ini_set('display_errors','On');
//error_reporting(-1);
session_start();
date_default_timezone_set('America/Bogota');
//error_reporting(E_ALL);
//ini_set('display_errors', 'On');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
foreach ($_GET as $key => $valor)
    $$key = $valor;
foreach ($_POST as $key => $valor)
    $$key = $valor;
foreach ($_SESSION as $key => $valor)
    $$key = $valor;
$ruta_raiz = '../../../..';
include_once "$ruta_raiz/core/Modulos/radicacion/clases/radicado.php";
$rad=new radicado($ruta_raiz);
include_once "$ruta_raiz/core/config/config-inc.php";
include_once "$ruta_raiz/core/Modulos/radicacion/clases/tx.php";
$tx=new tx($ruta_raiz);
if(!isset($numrad) && !isset($gruporad)){
    die("Error datos no encontrados");
}
if(isset($numrad)){
$archivo=RUTA_GENDOC.$numrad.'.json';
if(file_exists($archivo)){
    system("rm -f $archivo");
}
$datosrad=$rad->traerDatosRad($numrad);
$nom_r=$datosrad['nom_r'];
$dir_r=$datosrad['param5'];
$rad_r=$numrad;
$dignatario=$datosrad['param8'];
$asunto=$datosrad['param9'];
$rad->setContinente($datosrad['conti']);
$rad->setPais($datosrad['pais']);
$rad->setDepto($datosrad['dpto']);
$rad->setMuni($datosrad['muni']);
list($depto_r,$muni_r)=$rad->dataUbicados($datosrad['dpto'],$datosrad['muni']);
$f_rad_s=$datosrad['fechar'];
if($datosrad['trd']==0){
    $trd="SIN TIPIFICAR";
}
else{
    list($serie,$subserie,$tipo_doc)=$tx->consultarTRD($numrad);
    $trd="$serie/$subserie/$tipo_doc";
}
$salida=array(array("llave"=>"DEPENDENCIA_NOMBRE","valor"=>"$depe_nomb","barras"=>false),
array("llave"=>"FUNCIONARIO","valor"=>"$usua_nomb","barras"=>false),
array("llave"=>"RAD_S_BARRA","valor"=>"$rad_r","barras"=>true),
array("llave"=>"DIGNATARIO","valor"=>"$dignatario","barras"=>false),
array("llave"=>"DEPE_CODI","valor"=>"$dependencia","barras"=>false),
array("llave"=>"DEP_SIGLA","valor"=>"$dep_sigla","barras"=>false),
array("llave"=>"F_RAD_S","valor"=>"$f_rad_s","barras"=>false),
array("llave"=>"DEPTO_R","valor"=>"$muni_r, $depto_r","barras"=>false),
array("llave"=>"ASUNTO","valor"=>"$asunto","barras"=>false),
array("llave"=>"NOM_R","valor"=>"$nom_r","barras"=>false),
array("llave"=>"RAD_S","valor"=>"$rad_r","barras"=>false),
array("llave"=>"DIR_R","valor"=>"$dir_r","barras"=>false),
array("llave"=>"TRD","valor"=>"$trd","barras"=>false));
//print_r($salida);
    if(($fp=@fopen($archivo,"w"))){
    	fwrite($fp, stripcslashes(json_encode($salida,JSON_UNESCAPED_UNICODE)));
    	fclose($fp);
    	echo "<div id='response'>Archivo cargado con exito</div>";
    }
    else{
    	echo "<div id='response'>ERROR: no pudo abrir la ruta $archivo</div>";
    }
}
else{
    $carp=RUTA_GENDOC.$gruporad;
    if(file_exists($carp)){
	system("rm -rf $carp");
    }
    if(mkdir($carp,0755,true)){
	$status=0;
	$err="";
    }
    else{
	$status=1;
	$err="ERROR: No se pudo la ruta para cargar la masiva";
    }
    include_once "$ruta_raiz/core/Modulos/masiva/clases/masiva.php";
     $masiva=new masiva($ruta_raiz);
     $masiva->setGrupoRadi($gruporad);
     $datos=$masiva->grupoMasiva();
     list($serie,$subserie,$tipo_doc)=$tx->consultarTRD($gruporad);
     $trd="$serie/$subserie/$tipo_doc";
     if($status==0){
     $salida="";
     for($i=0;$i<count($datos);$i++){
	$numrad=$datos[$i]['numrad'];
	$archivo=$carp.'/'.$numrad.'.json';
	$datosrad=$rad->traerDatosRad($numrad);
	$nom_r=$datosrad['nom_r'];
	$dir_r=$datosrad['param5'];
	$rad_r=$numrad;
	$dignatario=$datosrad['param8'];
	$asunto=$datosrad['param9'];
	$rad->setContinente($datosrad['conti']);
	$rad->setPais($datosrad['pais']);
	$rad->setDepto($datosrad['dpto']);
	$rad->setMuni($datosrad['muni']);
	list($depto_r,$muni_r)=$rad->dataUbicados($datosrad['dpto'],$datosrad['muni']);
	$f_rad_s=$datosrad['fechar'];
	if(isset($salida))
	    unset($salida);
	$salida=array(array("llave"=>"DEPENDENCIA_NOMBRE","valor"=>"$depe_nomb","barras"=>false),
array("llave"=>"FUNCIONARIO","valor"=>"$usua_nomb","barras"=>false),
array("llave"=>"RAD_S_BARRA","valor"=>"$rad_r","barras"=>true),
array("llave"=>"DIGNATARIO","valor"=>"$dignatario","barras"=>false),
array("llave"=>"DEPE_CODI","valor"=>"$dependencia","barras"=>false),
array("llave"=>"DEP_SIGLA","valor"=>"$dep_sigla","barras"=>false),
array("llave"=>"F_RAD_S","valor"=>"$f_rad_s","barras"=>false),
array("llave"=>"DEPTO_R","valor"=>"$depto_r - $muni_r","barras"=>false),
array("llave"=>"ASUNTO","valor"=>"$asunto","barras"=>false),
array("llave"=>"NOM_R","valor"=>"$nom_r","barras"=>false),
array("llave"=>"RAD_S","valor"=>"$rad_r","barras"=>false),
array("llave"=>"DIR_R","valor"=>"$dir_r","barras"=>false),
array("llave"=>"TRD","valor"=>"$trd","barras"=>false));
	if(($fp=@fopen($archivo,"w"))){
    	    fwrite($fp, stripslashes(json_encode($salida,JSON_UNESCAPED_UNICODE)));
    	    fclose($fp);
	}
	else{
	    $status=1;
	    $err.="$numrad.json,";
	}
     }
     if($status==1){
	$err="ERROR: Archivo no pudieron ser creados $err";
     }
     }

     if($status==0){
	echo "<div id='response'>Archivo cargado con exito</div>";
     }
     else{
	echo "<div id='response'>$err</div>";
     }

     //print_r($salida);
}
?>
