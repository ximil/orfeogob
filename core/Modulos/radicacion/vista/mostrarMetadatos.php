<?php session_start(); date_default_timezone_set('America/Bogota'); date_default_timezone_set('America/Bogota');
foreach ($_GET as $key => $valor)  $$key = $valor;
foreach ($_POST as $key => $valor)  $$key = $valor;
$krd = $_SESSION["krd"];
$dependencia = $_SESSION["dependencia"];
$usua_doc = $_SESSION["usua_doc"];
$codusuario = $_SESSION["codusuario"];
$id_rol = $_SESSION["id_rol"];
$tpDescRad = $_SESSION["tpDescRad"];
$ruta_raiz="../../../..";
$url="$ruta_raiz/core/Modulos/radicacion/vista/operMetadatos.php";
//Validando session
include_once "$ruta_raiz/core/vista/validadte.php";
include_once "$ruta_raiz/core/Modulos/radicacion/clases/metadatos.php";
$met= new metadatos($ruta_raiz);
?>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Formulario de radicaci&oacute;n</title>
        <link rel="stylesheet" href="<?php echo $ruta_raiz?>/estilos/default/orfeo.css">
	<link type="text/css" href="<?php echo $ruta_raiz?>/estilos/Style.css" rel="stylesheet">
	<style>
	.cosa
            {
                margin-top: 0;
                margin: 0;
                padding: 0;
                color: #ffffff;
                font-size: 14px;
                text-align: center;
                background-color: #006699;
                text-transform: inherit;
                text-shadow: 0.1em 0.1em 0.2em black;
            }
	</style>
	<script src="<?php echo $ruta_raiz?>/js/jquery-1.10.2.js"></script>
	<script>
	function modifMeta(id,vista){
            var formData="action=cargaMeta&id="+id+"&vista="+vista;
            $.ajax({
                url : "<?php echo $url?>",
                type: "POST",
                data: formData
            })
            .done(function(data){
                $("#response").html(data);
            })
            .fail(function(){
                alert("Fallo el ajax");
            });
        }

	function replaceVista(dato){
	    if(dato==1){
		var x=$("#botomodi").attr('onclick').replace('replaceVista(1);','replaceVista(0);');
        	$("#botomodi").attr('onclick',x);
		$("#botomodi").val("Modificar Tags");
		$("#t1").show();
		$("#t2").hide();
	    }
	    else{
		var x=$("#botomodi").attr('onclick').replace('replaceVista(0);','replaceVista(1);');
                $("#botomodi").attr('onclick',x);
		$("#botomodi").val("Modificar Metadato");
                $("#t1").hide();
                $("#t2").show();
	    }
	}

	function addMeta(){
	    var formData="action=addMetadato";
            $.ajax({
                url : "<?php echo $url?>",
                type: "POST",
                data: formData
            })
            .done(function(data){
                $("#response").html(data);
            })
            .fail(function(){
                alert("Fallo el ajax");
            });
	}

	function cargaTags(num){
	    var formData="action=addTags&cont="+num;
            $.ajax({
                url : "<?php echo $url?>",
                type: "POST",
                data: formData
            })
            .done(function(data){
                $("#listaTag").html(data);
            })
            .fail(function(){
                alert("Fallo el ajax");
            });
	}

	function crearMetadato(){
	    var notags=$("#no_tags").val();
	    var datatags='';
	    var formData;
	    var s1;
	    var s2;
	    var j;
	    var nomb_data=$.trim($("#nomb_data").val());
	    var depe_sel=$("#depe_sel").val();
	    if(nomb_data==''){
		alert('El nombre de metadato a crear');
		return false;
	    }
	    for(var i=0;i<notags;i++){
		if(i==0){
		    datatags+='[';
		}
		else{
		    datatags+=',';
		}
		datatags+='{"nomb":"'+$("#nomb"+i).val()+'",'+
		'"tipo":"'+$("#tipo"+i).val()+'",'+
		'"oblig":'+$("#oblig"+i).val()+','+
		'"busq":'+$("#busq"+i).val()+','+
		'"desc":"'+$("#desc"+i).val()+'"}';
		s1=$.trim($("#nomb"+i).val());
		s2=$.trim($("#desc"+i).val());
		if(s1==''){
	            j=i+1;
		    alert('El nombre del tag No. '+j+' no es valido');
		    return false;
		}
		if(s2==''){
                    j=i+1;
                    alert('La descripcion del tag No. '+j+' no es valido');
                    return false;
                }
	    }
	    if(datatags!=''){
		datatags+=']';
	    }
	    //alert(datatags);
	    var obj=JSON.stringify(datatags);
	    formData="action=insertarMetadato&datatags="+obj+"&nomb_data="+nomb_data+"&trad="+$("#tipo_rad").val()+"&meta_estado="+$("#meta_estado").val()+"&meta_busq="+$("#meta_busq").val()+"&depe_sel="+depe_sel;
	    $.ajax({
		url : "<?php echo $url?>",
                type: "POST",
                data: formData
	    })
	    .done(function(data){
		//$("#response").html(data);
		alert(data);
		location.reload();
	    })
	    .fail(function(){
                alert("Fallo el ajax");
            });
	}

	function modificaInfo(valor,columna,id){
	    var formData="action=modificarMeta&val="+valor+"&columna="+columna+"&id="+id;
	    $.ajax({
                url : "<?php echo $url?>",
                type: "POST",
                data: formData
            })
            .done(function(data){
                $("#div_"+columna).html(data);
            })
            .fail(function(){
                alert("Fallo el ajax");
            });
	}

	function modifTag(id,nombre,desc,oblig,tipo,busq,estado){
	    $("#variable").text("Tag a Modificar");
	    $("#ten").html("<a href='#' alt='Modificar' onclick='cambiarTag("+id+");'><img src='<?php echo $ruta_raiz?>/imagenes/edit.png' alt='Modificar' height=24 width=24></a>");
	    $("#desc").val(desc);
	    $("#nomb").val(nombre);
	    $("#nomb").prop('readonly',true);
	    $("#esta").val(tipo);
	    $("#esta").prop('disabled','disabled');
	    $("#estado").val(estado);
            $("#busq").val(busq);
            $("#oblig").val(oblig);
	}

	function cambiarTag(id){
	    var formData="action=cambiarTag&data[desc]="+$("#desc").val()+"&data[busq]="+$("#busq").val()+"&data[oblig]="+$("#oblig").val()+"&data[estado]="+$("#estado").val()+"&id="+id;
	    $.ajax({
                url : "<?php echo $url?>",
                type: "POST",
                data: formData
            })
            .done(function(data){
                alert(data);
                modifMeta($("#id_meta").val(),1);
            })
            .fail(function(){
                alert("Fallo el ajax");
            });
	}

	function agregarTag(){
	    var desc=$.trim($("#desc").val());
	    var nomb=$.trim($("#nomb").val());
	    if(desc.length==0){
		alert('La etiqueta del tag no puede ir vacia');
		return false;
	    }
	    if(nomb.length==0){
                alert('El nombre del tag no puede ir vacio');
                return false;
            }
	    var formData="action=addTag&data[nombre]="+nomb+"&data[desc]="+desc+"&data[tipo]="+$("#esta").val()+"&data[estado]="+$("#estado").val()+"&data[busq]="+$("#busq").val()+"&data[oblig]="+$("#oblig").val()+"&meta_nomb="+$("#meta_nomb").val()+'&id='+$("#id_meta").val();
	    $.ajax({
                url : "<?php echo $url?>",
                type: "POST",
                data: formData
            })
            .done(function(data){
                alert(data);
		modifMeta($("#id_meta").val(),1);
            })
            .fail(function(){
                alert("Fallo el ajax");
            });
	}
	</script>
    </head>
    <body>
	<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center" class="BORDE_TAB">
            <tr align="center" >
        <td class="titulos4">
        <center>Editor de metadatos</center></td></tr>
        </table>
	<?php 	//$met->setTipoRad(2);
	$data=$met->listarMetadatos(false);
	$cont=count($data)-1;
	//print_r($data);
	?>
	<div id='response'>
	<table class='borde_tab' cellspacing=2 cellpadding=0 align='center' width='70%'>
	    <tr><td class='info' colspan=5>Agregar un Metadato implica generar un campo de opciones de ingreso de información obligatoria o no, que implica a un radicado y una dependencia. Después de creado el Metadato <b>no hay forma de realizar</b> ajustes, la única opción es desactivarlo.
                <br><br>
<b>Nota:</b> Los metadatos son datos altamente estructurados que describen información,
describen el contenido, la calidad, la condición y otras características de los datos. Es "Información sobre información" o "datos sobre los datos".</td></tr>
	    <tr>
		<td class='cosa' colspan=5>Listado de metadatos disponibles</td>
	    </tr>
	    <tr class='titulos3'>
		<td>Nombre de Metadato</td>
		<td>Tipo de Radicacion</td>
		<td>Visible para busqueda</td>
		<td>Estado</td>
		<td>Acci&oacute;n</td>
	    </tr>
	    <?php 	    for($i=0;$i<$cont;$i++){
	    ?>
	    <tr class='listado2'>
		<td><?php echo $data[$i]['radi_data_nomb']?></td>
	        <td><?php echo $tpDescRad[$data[$i]['sgd_trad_codigo']]?></td>
                <td><?php echo ($data[$i]['radi_data_busq']==1)?'Activo':'Inactivo'?></td>
                <td><?php echo ($data[$i]['radi_data_estado']==1)?'Activo':'Inactivo'?></td>
                <td><input type='button' value='Modificar' class='botones' onclick="modifMeta(<?php echo $data[$i]['radi_data_id']?>,0)"></td>
	    </tr>
	    <?php 	    }
	    ?>
	    <tr><td class='info' align='center' colspan=5><input type='button' class='botones_mediano' value='Agregar Metadato' onclick='addMeta();'></td></tr>
	</table>
	</div>
    </body>
</html>
