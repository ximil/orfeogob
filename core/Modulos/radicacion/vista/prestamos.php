<?php 
	session_start(); date_default_timezone_set('America/Bogota'); date_default_timezone_set('America/Bogota'); 
	foreach ($_GET as $key => $valor)  $$key = $valor;
	foreach ($_POST as $key => $valor)  $$key = $valor;
	$krd = $_SESSION["krd"];
	$dependencia = $_SESSION["dependencia"];
	$usua_doc = $_SESSION["usua_doc"];
	$codusuario = $_SESSION["codusuario"];
	$id_rol = $_SESSION["id_rol"];
	$ruta_raiz="../../../..";
	include($ruta_raiz.'/core/vista/validadte.php');
	//path para pintar el recorset en la vista.
	$scripturl= $ruta_raiz.'/core/Modulos/radicacion/vista/operPrestamos.php';
	//inicializa  dependecias
	include_once $ruta_raiz.'/core/clases/dependencia.php';
        $depe= new dependencia($ruta_raiz);
        
	$dependecias=$depe->consultarTodo();
	$numndep=count($dependecias);
	$optionDepe="<option value='0'>Todas Las dependencias</option>";
	for ($i = 0; $i < $numndep; $i++) {
		$nomDepe = $dependecias[$i]["depe_nomb"];
		$codDepe = $dependecias[$i]["depe_codi"];
		if($codDepe!='999')
			$optionDepe.="<option value='$codDepe'>$codDepe - $nomDepe</option>";				
	}	
	include_once $ruta_raiz.'/core/clases/usuarioOrfeo.php';
?>

<html>
	<head>
		<link rel="stylesheet" type="text/css" 	href="<?php echo $ruta_raiz?>/js/calendario/calendar.css">
		<script src="<?php echo $ruta_raiz?>/old/js/popcalendar.js"></script>
		<link rel="stylesheet" href="../../../../estilos/caprecom/orfeo.css">
		<script language="JavaScript" src="<?php echo $ruta_raiz?>/js/common.js"></script>
		<script language="JavaScript" type="text/javascript"	src="<?php echo $ruta_raiz?>/js/calendario/calendar_eu.js"></script>
		<script type="text/javascript">
		function calCV(fddd,variable){
		var A_CALTPL ={'imgpath' : '../../../../js/calendario/img/',
				'months' : ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
				'weekdays' : ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],'yearscroll': true,	'weekstart': 1,	'centyear'  : 70}
			return tcal ({'formname': fddd,'controlname': variable},A_CALTPL);
		}
			function buscar(g){
				//alert(g);
				var exp = document.getElementById('numexp').value;
				var rad = document.getElementById('numRad').value;
				var depe = document.getElementById('depe').value;
				var tipoX = document.getElementById('tipoX').value;
				var pagina = document.forms.busquedaExp;
				vistaFormUnitid('listados',1);
				vistaFormUnitid('respuesta',2);
				vistaFormUnitid('Resp',2);
				if(g=='e'){
				if( exp.length==0 && rad.length==0 && depe==0){
					alert('Debe llenar alguno de los campos Expediente o Radicado o combinados');
					return false;
				}
				}
					//partes(url,div,parameter,getpara)
					var poststr = "action=buscar&exp="+exp+"&rad="+rad+"&depe="+depe+"&tipo="+tipoX; 
					//alert(poststr);
					partes('<?php  echo $scripturl; ?>','listados',poststr,'');
				
			}
			function accion(accion,numRa,idp){
					//partes(url,div,parameter,getpara)
					var poststr = "action="+accion+"&rad="+numRa+"&idp="+idp; 
					//alert(poststr);
					if(confirm("Esta seguro de cancelar la solicitud de prestamo del radicado "+numRa+"? ")){
					vistaFormUnitid('respuesta',1);
					vistaFormUnitid('listados',2);
					partes('<?php  echo $scripturl; ?>','respuesta',poststr,'');
					}
				
			}
			function devolver(){
				var rad = document.getElementById('HnumRad').value;
				var idp = document.getElementById('Hidp').value;
				var obse = document.getElementById('descpmotivo').value;
				var poststr = "action=devolver&rad="+rad+"&idp="+idp+"&observar="+obse; 
				if(obse.length< 10 ){
					alert('Debe Colocar una observacion con un minimo de 10 caracteres');
					return false;
				}							
				vistaFormUnitid('respuesta',1);
				vistaFormUnitid('listados',2);
				vistaFormUnitid('Resp',2);
				partes('<?php  echo $scripturl; ?>','respuesta',poststr,'');
				
			
		}
		function prestar(){
				var rad = document.getElementById('HnumRad').value;
				var idp = document.getElementById('Hidp').value;
				var esta = document.getElementById('estado').value;
				var fech = document.getElementById('fecha_busq').value;
				var obse = document.getElementById('descpmotivo').value;
				if(fech=='' && esta == 2){
					alert('Debe seleccionar fecha de vencimiento');
					return false;
				}
				if(obse.length< 10 ){
					alert('Debe Colocar una observacion con un minimo de 10 caracteres');
					return false;
				}
				if(confirm("Esta seguro de prestar el radicado "+rad+"? ")) {
				var poststr = "action=prestar&rad="+rad+"&idp="+idp+"&observar="+obse+"&fecha="+fech+"&estado="+esta; 
				//alert(poststr);
				vistaFormUnitid('respuesta',1);
				vistaFormUnitid('listados',2);
				vistaFormUnitid('Resp',2);
				partes('<?php  echo $scripturl; ?>','respuesta',poststr,'');
				}
			
		}
			function formprestar(numRa,idp,tp){
				
				
				document.getElementById('idRad').innerHTML = numRa;
				document.getElementById('HnumRad').value=numRa;
				document.getElementById('Hidp').value=idp;
				if(tp==1){
					vistaFormUnitid('delvom',2);
					vistaFormUnitid('btprestamo',2);
					vistaFormUnitid('btdevolucion',1);
					document.getElementById('mti').innerHTML = "Devolver Radicado";
					}
				else{
					vistaFormUnitid('delvom',1);
					vistaFormUnitid('btprestamo',1);
					vistaFormUnitid('btdevolucion',2);
					document.getElementById('mti').innerHTML = "Prestar Radicado";
					}
				vistaFormUnitid('respuesta',2);
				vistaFormUnitid('listados',2);
				vistaFormUnitid('Resp',1);
				}
			function entsub(event,ourform) {
				if (event && event.which == 13)
				buscar();
			else
				return true;
			}
			
			function busqusu() {
				var codepen = document.getElementById('depe').value;
				var pagina = document.forms.busquedaExp;
				var postusu = "actionusu=busqusu&depe="+codepen; 
				//alert(postusu);
			}
			function cancelar(){
				vistaFormUnitid('respuesta',2);
				vistaFormUnitid('listados',1);
				vistaFormUnitid('Resp',2);
			}
			function val(){
				var esta = document.getElementById('estado').value;
				if(esta==5){
					vistaFormUnitid('datafech',2);
					}
				else{
				vistaFormUnitid('datafech',1);
				}

			}

		</script>
		<style type="text/css">
		dataPres .td {
		font-family: Verdana, Arial, Helvetica, sans-serif;
         font-size: 10px;
         font-weight: bolder;
        color: #069;
        text-decoration: none;
		}
		</style>
	</head>

	<body bgcolor="#FFFFFF">
	<div id="spiffycalendar" class="text"></div>
		<?php 
			//pinta el menu de la barra general
			//include 'admPrestamosTab.php';
		?>
		<table class=borde_tab width='100%' cellspacing="2">
			<tr>
				<td  align='center' class="titulos4">
		  		B&uacute;squeda Documentos Solicitados Para Prestar 
				</td>
			</tr>
		</table>
		<form method="post" action="#" name="busquedaExp" style="margin:0"> 	
			<table  style="border-collapse: collapse; margin:0 ;  width: 100%; padding: 0 ; border-top: 0; border-spacing: 0">
		  	<tr>
		  		<td width="5">&nbsp;</td>
			    <td  valign="top">
			    	<table width="100%"  cellspacing="0" class="borde_tab">
							<tr class="titulo1">
				 				<td colspan="6">Datos de B&uacute;squeda</td>
				 			</tr>
				        <tr>
			        	<td class="titulos2" rowspan=4 width="200">
			          	<select name="tipoX" id="tipoX" size="4" class="select" onchange='buscar("sin");' style="width:200px">
			           		<option value='0' selected>Prestamo</option>
			           		<option value='1'>Devoluci&oacute;n</option>
			           		<option value='2'>Hist. Prestados</option>
			           		<option value='3'>Hist. Cancelados</option>
			          	</select>
			        	</td>
			      	   <td class="titulos2"> Expediente </td>
			        	<td class="titulos2">
			          	<input type=text id="numexp" name='numexp'  onkeypress='return entsub(event,this.form)' class='tex_area' size="20" maxlength="20" >
			        	</td>
			        	<td height="26" class="titulos2" colspan="2">Dependencias</td>
			        	
			        	<td height="26" colspan=2 class="listado2" rowspan=2>
			        			<div id="busIns" align="center">
			        				<input type="button" name="buscar_serie2" Value='Buscar' onclick='buscar("e");' class=botones >
			            		<input type="reset"  name=aceptar2 class=botones id=aceptar onClick="vistaFormUnitid('busIns',1);vistaFormUnitid('busModi',2);document.UsuarioOrfeo.login.disabled = false;" value='Limpiar'>
			        			</div>
			            	<div id="busModi">&nbsp;</div>      
			          </td>
			      	</tr>
			      	<tr>
			        	<td height="26" class="titulos2"> Radicado </td>
			        	<td height="26" class="titulos2">
			        		<input type=text id="numRad" name='numRad'  onkeypress='return entsub(event,this.form)' class='tex_area' size=20 maxlength="18" >
			        	</td>
			        	<td height="26" class="listado2" colspan="2">
			        		<select name="depe" id="depe" class="select" style="width:200px" onChange='busqusu()'>
			           		<?php echo $optionDepe;?>
			          	</select>
			        	</td>
			      	</tr>   
			      	           
			      	<tr>
					 		</tr>
				    </table>
			    </td>
			    </tr><tr>
			    <td width="5" >&nbsp;</td>
<!-- --------------------------------------------------------------------------- -->
			    <td valign="top" >
			   		<table width="100%" cellpadding="0" cellspacing="0"  class="borde_tab">
			   			<tr class='titulo1'>
				 				<td colspan='2' >Resultado  de la B&uacute;squeda</td>
				 			</tr>
				   		<tr>
				   			<td>
			    				<div id="listados" style="vertical-align : top; padding : 0px; width : 100%; height : 400px; overflow : auto; "  class='listado2' ></div>
			    				
								<div id="Resp" style="vertical-align : top; padding : 0px; width : 100%; height : 400px; overflow : auto; display : none"  class='listado2' >
								
		<br>
		<br>
		<table width="624" border="0" align="center" cellpadding="0" cellspacing="0">
			<tr class='titulo1'>
				<td colspan='4'><div id='mti'>Prestar radicado</div></td>
			</tr>
			<tr>
				<td class='titulos3' align="left">Radicado No.</td>
				<td class='titulos3' colspan=3 valign="middle"><div id='idRad'></div><input type="hidden" id='HnumRad' ><input type="hidden" id='Hidp' ></td>
			</tr>
			<tr bgcolor="White">
 	            <td class='titulos3' colspan=4 >
 	            <div id='delvom'>
 	            <table>
			<tr bgcolor="White">
 	            <td class='titulos3' align="right" class="titulosError2">Estado:</td>
				<td class='titulos3'>
					<select class="select" name="estado" id="estado" onchange='val()'>
 					<option selected="" value="2" >PRESTADO</option>  
 					<option value="5">PRESTAMO INDEFINIDO</option>
	                  </select></td>
 	            <td class='titulos3' align="right" class="titulosError2">Fecha de Vencimiento:</td>
				<td class='titulos3'>
     	<div id="datafech">	<input readonly="true" type="text" class="tex_area" name="fecha_busq" id="fecha_busq" size="10" value=""> 
       <script language="javascript">
       tcal=calCV('busquedaExp','fecha_busq');
			</script></div>  
        </td>
					     </tr>
					     </table>
					     </div>
					     </td>
					     </tr>
			<tr>
				<td class='titulos3' valign="middle" align="left">Obsevaci&oacute;n</td>
				<td class='titulos3' colspan=3 valign="middle">
					<textarea class='select' name="descpmotivo" cols="70" rows="6" id="descpmotivo"></textarea>
				</td>
				
				
			</tr>
			<tr>
				<td colspan="4" class='titulos3'><br>
					<table align="center"><tr>
					<td><div id='btprestamo'><input type="button" class="botones" onclick="prestar();" value="Prestamo" /></div></td>
					<td><div id='btdevolucion' style="display: none"><input type="button" class="botones" onclick="devolver();" value="Devolver" /></div></td>
					<td><input type="button" onclick='cancelar()' class="botones" value="Cancelar" /></td>
					</tr></table>
					
					 
				</td>
			</tr>
			<tr>
				<td colspan="4" class='titulos3'>&nbsp;</td>
			</tr>
		</table>
							</div>
<div id="respuesta" style="vertical-align : top; padding : 0px; width : 100%; height : 400px; overflow : auto; "  class='listado2' ></div>
								
			
			    			</td>
			    		</tr>
			    	</table>
			    </td>
<!-- --------------------------------------------------------------------------- -->
					<td width="5">&nbsp;</td>
		  	</tr>
			</table>
		</form>
		<!--
		<form  action="depuracion.php" name='formdepu' id='formdepu' method="post">
			<input type="hidden" id='passRad' name='passRad'>
		  <input type="hidden" id='passExp' name='passExp'>
		  <input type="submit" value="Depurar">
		</form> -->
		<script>
		buscar('sin');
		</script>
	</body>
</html>