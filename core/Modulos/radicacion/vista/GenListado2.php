<?php date_default_timezone_set('America/Bogota');
session_start();
foreach ($_GET as $key => $valor)
    ${$key} = $valor;
foreach ($_POST as $key => $valor)
    ${$key} = $valor;
$krd = $_SESSION["krd"];
$dependencia = $_SESSION["dependencia"];
$usua_doc = $_SESSION["usua_doc"];
$codusuario = $_SESSION["codusuario"];
$id_rol = $_SESSION["id_rol"];
$ruta_raiz = "../../../..";
include($ruta_raiz . '/core/config/config-inc.php');
//path para pintar el recorset en la vista.
$scripturl = $ruta_raiz . '/core/Modulos/radicacion/vista/operlisMas.php';
$scripturl2 = $ruta_raiz . '/core/Modulos/radicacion/vista/operBuscarUso.php';
//inicializa  dependecias
$tituloPage = 'Listados masivas';
include_once $ruta_raiz . '/core/Modulos/radicacion/clases/genListado.php';
$lisMas = new genListado($ruta_raiz);
$rsx = $lisMas->consultarBase($_SESSION['dependencia']);
if (MAX_MASIVA)
    $masMax = MAX_MASIVA;
else
    $masMax = 200;
include_once $ruta_raiz . '/core/clases/usuarioOrfeo.php';
?>

<html>
    <head>
        <link rel="stylesheet" type="text/css" 	href="<?php echo  $ruta_raiz ?>/js/calendario/calendar.css">
        <script src="<?php echo  $ruta_raiz ?>/old/js/popcalendar.js"></script>
        <link rel="stylesheet" href="../../../../estilos/caprecom/orfeo.css">
        <script language="JavaScript" src="<?php echo  $ruta_raiz ?>/js/common.js"></script>
        <script language="JavaScript" type="text/javascript"	src="<?php echo  $ruta_raiz ?>/js/calendario/calendar_eu.js"></script>
        <script type="text/javascript">
<?php echo $rsx['script']; ?>

            function nuevo() {
                vistaFormUnitid('formlista', 1);
                vistaFormUnitid('accionlista', 2);
                vistaFormUnitid('Ladd', 1);
                vistaFormUnitid('Lmod', 2);
                document.getElementById('idmas').value = "";
                document.getElementById('masnomb').value = "";
                document.getElementById('campextra').value = "";
                document.getElementById('masest').value = "";
            }

            function cancelar() {
                vistaFormUnitid('formlista', 2);
                vistaFormUnitid('accionlista', 2);
            }
            function delLista(id) {

                var idlist = document.getElementById('lista').value;
                var pstvar = 'action=delItem&idList=' + idlist + '&id=' + id;
                partes('<?php echo $scripturl2; ?>', 'listados', pstvar, '');
                var f = document.getElementById('listNum').value;
                document.getElementById('listNum').value = f - 1;

            }
            function    listar(selTag) {
                //document.getElementById('NumListado').innerHTML = '';
                var x = selTag.options[selTag.selectedIndex].text;
                var id = selTag.options[selTag.selectedIndex].value;
                document.getElementById('tituloListado').innerHTML = x;
                var ida = document.getElementById('lista').text;
                var poststr = "action=Listar3&idList=" + id;
                partes('<?php  echo $scripturl2; ?>', 'listados', poststr, '');
            }
            function modificar() {
                // 	    vistaFormUnitid('Ladd', 2);
                //	    vistaFormUnitid('Lmod', 1);
                var id = document.getElementById('lista').value;
                var nomb = document.getElementById('masnomb').value = ml[id]['NOMBRE'];
                Start('buscar_usuario.php?&lista=' + id + '&nombLista=' + nomb, 1024, 400);
            }

            function Start(URL, WIDTH, HEIGHT) {
                windowprops = "top=0,left=0,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=1100,height=550";
                preview = window.open(URL, 'preview', windowprops);
            }

            function        modNomb() {
                vistaFormUnitid('Ladd', 2);
                vistaFormUnitid('Lmod', 1);
                var lista = document.getElementById('lista').value;
                if (lista > 0) {
                    vistaFormUnitid('formlista', 1);
                    document.getElementById('idmas').value = ml[lista]['ID'];
                    document.getElementById('masnomb').value = ml[lista]['NOMBRE'];
                    document.getElementById('campextra').value = ml[lista]['EXTRA'];
                    document.getElementById('masest').value = ml[lista]['EST'];
                }
                else {
                    alert('Debe seleccionar una lista');
                }
            }

            function        modLista() {
                vistaFormUnitid('accionlista', 2);
                var id = document.getElementById('idmas').value;
                var nomb = document.getElementById('masnomb').value;
                var extra = document.getElementById('campextra').value;
                var esta = document.getElementById('masest').value;
                if (confirm("Esta seguro de modificar la lista Masiva " + nomb + "? ")) {
                    var poststr = "action=modlista&idlista=" + id + "&nomb=" + nomb + "&extra=" + extra + "&estado=" + esta;
                    partes('<?php echo $scripturl; ?>', 'accionlista', poststr, '');
                    vistaFormUnitid('accionlista', 1);
                    auLista('listadoMas');
                }
                else {
                    return false;
                }
            }
            function auLista(div) {

                var poststr = "action=listado";
                partes('<?php echo $scripturl; ?>', div, poststr, '');
            }
            function addLista() {
                vistaFormUnitid('accionlista', 2);
                var nomb = document.getElementById('masnomb').value;
                var extra = document.getElementById('campextra').value;
                var esta = document.getElementById('masest').value;
                var poststr = "action=addList&nomb=" + nomb + "&extra=" + extra + "&estado=" + esta;
                partes('<?php echo $scripturl; ?>', 'accionlista', poststr, '');
                vistaFormUnitid('accionlista', 1);
                auLista('listadoMas');

            }
            function addItem(idcod, cc, nomb, ap, ap2, muni, dir, tp) {
                var f = document.getElementById('listNum').value;
                x = <?php echo  $masMax; ?>;
                if (f >= x) {
                    alert('El maximo del listado de masivas es ' + x);
                    return false;
                }
                var lista = document.getElementById('lista').value;
                //alert( nomb+' '+ap+' '+ap2+' '+muni+' '+dir);
                var poststr = "action=addListado&idList=" + lista + "&tpemp=" + tp + "&no_documento1=" + cc + "&idcod=" + idcod;
                partes('<?php echo $scripturl2; ?>', 'listados', poststr, '');

                document.getElementById('listNum').value = f + 1

                //document.getElementById('NumListado').innerHTML = f+1;

            }
            function buscarUsuario() {
                vistaFormUnitid('opeR', 1);
                var doc = document.getElementById('no_documento').value;
                var nomb = document.getElementById('nombre_essp').value;
                var tbusqueda = document.getElementById('tbusqueda').value;

                var poststr = "action=consultarBusqueda&nomb=" + nomb + "&doc=" + doc + "&tbusqueda=" + tbusqueda;
                partes('<?php echo $scripturl2; ?>', 'opeR', poststr, '');
                //vistaFormUnitid('accionlista', 1);
                //auLista('listadoMas');  

            }
        </script>
        <style type="text/css">
            dataPres .td {
                font-family: Verdana, Arial, Helvetica, sans-serif;
                font-size: 10px;
                font-weight: bolder;
                color: #069;
                text-decoration: none;
            }
            #lista
            {
                width: 200px;
            }
        </style>
    </head>

    <body bgcolor="#FFFFFF" style="margin: 0;margin-top: 0;margin-left: 0;margin-right: 0">
        <div id="spiffycalendar" class="text"></div>
        <?php         //pinta el menu de la barra general
        //include 'admPrestamosTab.php';
        ?>
        <table class='borde_tab'  style='width: 100%' >
            <tr>
                <td  align='center' class="titulos4">
                    <?php echo $tituloPage; ?>
                </td>
            </tr>
        </table>
        <table  style="border-collapse: collapse; margin:0 ;height: 500px;  width: 100%; padding: 0 ; border-top: 0; border-spacing: 0">
            <tr >
                <td  class="titulos2" style="width: 100px;vertical-align: top; padding: 0px;margin: 0;margin-top: 0;margin-left: 0;margin-right: 0" >
                    <table style="width: 100%;padding: 0px;vertical-align: top;border-collapse: collapse" class="borde_tab">
                        <tr class="titulo1">
                            <td colspan="6">Listados</td>
                        </tr>
                        <tr>
                            <td >
                                <div id='listadoMas'>  <select size="10" class='select' onchange='listar(this);' id='lista' id="lista"><?php echo $rsx['option']; ?></select></div>
                            </td>
                        </tr>
                        <tr class="titulo1">
                            <td colspan="6">Operaciones</td>
                        </tr>
                        <tr>
                            <td >
                                <table><tr>
                                        <td><input type="button" name="buscar_serie2" Value='Nuevo' onclick='nuevo();' class=botones ></td>
                                        <td ><input type="button" name="buscar_serie2" Value='Modificar' onclick='modNomb();' class=botones ></td></tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="titulos2"> 
                                <table width="100%"  cellspacing="0" >
                                    <tr>
                                        <td>
                                            <div id="formlista" style='display: none'>
                                                <table width="100%"  cellspacing="0" class="borde_tab">
                                                    <tr class="titulo1">
                                                        <td>Nombre</td>
                                                    <tr>   <td><input type="hidden" id="idmas" name="idmas" >
                                                            <input type="text" id="masnomb" name="masnomb" ></td>
                                                        <td><input type="hidden" id="campextra" name="campextra" ></td></tr> 
                                                    <tr class="titulo1">
                                                        <td>Privacidad</td>
                                                    </tr><tr>                                                        <td>
                                                            <select id="masest" name="masest">
                                                                <option value="1" selected="selected" >Dependencia</option>
                                                                <option value="2">Publico</option>
                                                            </select>
                                                        </td></tr>
                                                    <tr class="titulo1">
                                                        <td>Accion</td>
                                                    </tr>
                                                    <tr>
                                                        <td rowspan="2" class="titulos2">
                                                            <table><tr>
                                                                    <td><div id='Ladd'> <input type="button" name="buscar_serie2" Value='Adicionar' onclick='addLista();' class=botones ></div></td>
                                                                    <td><div id='Lmod'> <input type="button" name="buscar_serie2" Value='Modificar' onclick='modLista();' class=botones ></div></td>
                                                                </tr><tr><td colspan='2'><input type="button" name="buscar_serie2" Value='cancelar' onclick='cancelar();' class=botones ></td></tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>


                                                    </tr>
                                                </table>
                                                <div id="accionlista"></div>
                                            </div>
                                        </td></tr></table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="vertical-align: top"><table style="border-collapse: collapse; margin:0 ;  width: 100%; padding: 0 ; border-top: 0; border-spacing: 0; display : block">
                        <tr>
                            <td  align='center' >
                                <table width="100%"  cellspacing="0" class="borde_tab">
                                    <tr class="titulo1">
                                        <td colspan="6">Listado <label id='tituloListado'> </label></td>
                                    </tr>
                                    <tr>
                                        <td class=""> 
                                           <!-- <input type="button" name="buscar_serie2" Value='Modificar Listado' onclick='modificar();' class=botones >
                                            <input type="button" name="buscar_serie2" Value='Agrear al listado' onclick='addBusqueda();' class=botones >-->
                                            <div id='crbus'>
                                                <table border=0 width=100%" align="center" class="borde_tab" cellpadding="0" cellspacing="1">
                                                    <tr> 
                                                        <td  class="titulos31"><font class="tituloListado">BUSCAR POR </font></td>
                                                        <td  class="titulos31">
                                                            <select name='tbusqueda' id='tbusqueda' class='select' onChange="activa_chk(this.form);
                visual();">
                                                                        <?php 
                                                                        if ($tbusqueda == 0) {
                                                                            $datos = "selected";
                                                                            $tbusqueda = 0;
                                                                        } else {
                                                                            $datos = "";
                                                                        }
                                                                        ?> 
                                                                <option value=0 <?php echo  $datos ?>>USUARIO</option>
                                                                <?php 
                                                                //		if($tbusqueda==1){$datos = "selected";$tbusqueda=1;}else{$datos= "";}
                                                                //if (strlen($nombreTp3) > 0) echo "<option value=1 $datos>$nombreTp3</option>";

                                                                if ($tbusqueda == 2) {
                                                                    $datos = "selected";
                                                                    $tbusqueda = 2;
                                                                } else {
                                                                    $datos = "";
                                                                }
                                                                ?> 	
                                                                <option value=2 <?php echo  $datos ?>>ENTIDAD</option>
                                                                <?php 
                                                                if ($tbusqueda == 6) {
                                                                    $datos = " selected ";
                                                                    $tbusqueda = 6;
                                                                } else {
                                                                    $datos = "";
                                                                }
                                                                ?>
                                                <!--  	<option value=6 <?php echo  $datos ?>>USUARIO ORFEO</option>-->
                                                            </select>
                                                        </td>
                                                        <td class="titulos31" valign="middle">
                                                            <span class="titulos31">Documento</span> 
                                                            <input type=text name='no_documento' id='no_documento' onkeypress='return entsub(event, this.form,<?php echo '"' . $label_us . '","' . $label_pred . '","' . $label_emp . '"'; ?>)' value='<?php echo  $_POST["no_documento"] ?>' class="tex_area">
                                                            </font>
                                                        </td>
                                                        <td class="titulos31" valign="middle">
                                                            <span class="titulos31">Nombre</span> 
                                                            <input type=text name='nombre_essp' id='nombre_essp' onkeypress='return entsub(event, this.form,<?php echo '"' . $label_us . '","' . $label_pred . '","' . $label_emp . '"'; ?>)' value='<?php echo  $_POST["nombre_essp"] ?>' class="tex_area">
                                                            <div id='vigen' style="display: none"><input type="checkbox" name="chk_desact" id="chk_desact" <?php ($_POST['tbusqueda'] != 1) ? print "disabled"  : print ""; ?>>Incluir no vigentes</div>  
                                                        </td>
                                                        <td width="40%"  align="center" class="titulos31" > 
                                                        <!--<input type=submit name=buscar value='BUSCAR' class="botones">-->
                                                            <input type="button" onclick='buscarUsuario()' name=buscar value='BUSCAR' class="botones">
                                                            <input type="button" onclick="vistaFormUnitid('opeR', 2);" name=oc value='ocultar' class="botones">
                                                            <input type="button" onclick="vistaFormUnitid('opeR', 1);" name=oc value='mostrar' class="botones">
                                                        </td>

                                                    </tr>
                                                </table></div>



                                            <div id="opeR" style="vertical-align : top; padding : 0px; width : 100%; height : 150px; overflow : auto; display : none"  class='listado' ></div>
                                        </td>

                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <table width="100%" cellpadding="0" cellspacing="0"  class="borde_tab">
                        <tr class='titulo1'>
                            <td colspan='2' >Datos de Lista </td>
                        </tr>
                        <tr>
                            <td>
                                <div id="listados" style="vertical-align : top; padding : 0px; width : 100%; height : 300px; overflow : auto; "  class='listado2' > </div>
                                <div id="Resp" style="vertical-align : top; padding : 0px; width : 100%; height : 300px; overflow : auto; display : none"  class='listado2' ></div>

                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" class='titulos3'>&nbsp;</td>
                        </tr>
                    </table>


                </td>
            </tr>
        </table>
        <script>
            //	buscar('sin');
        </script>
    </body>
</html>
