<?php session_start();
date_default_timezone_set('America/Bogota');
//error_reporting(E_ALL);
//ini_set('display_errors', 'On');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
foreach ($_GET as $key => $valor)
    $$key = $valor;
foreach ($_POST as $key => $valor)
    $$key = $valor;
foreach ($_SESSION as $key => $valor)
    $$key = $valor;
foreach ($_SERVER as $key => $valor)
    $$key = $valor;
$ruta_raiz = '../../../..';
include_once "$ruta_raiz/core/Modulos/radicacion/clases/radicado.php";
$rad=new radicado($ruta_raiz);
$anex_tipo=$rad->anexosTipo();
/*$option="";
for($i=0;$i<count($anex_tipo);$i++){
    $option.="\t\t\t<option value='{$anex_tipo[$i]['id']}'>{$anex_tipo[$i]['tipo']}</option>\n";
}*/
$url="../core/Modulos/radicacion/vista/operNuevoArchivo.php";
$url2="../core/Modulos/radicacion/vista/crearJson.php";
?>
<script>
var anex_tipo=<?php echo json_encode($anex_tipo)?>;
var timeout;
var dev=0;
var isLoading=false;
var tpDepeRad=<?php echo json_encode($tpDepeRad);?>;
function noEnter (field, event) {
    var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
    if (keyCode == 13) {
        var i;
        for (i = 0; i < field.form.elements.length; i++)
	    if (field == field.form.elements[i])
	    break;
	i = (i + 1) % field.form.elements.length;
	field.form.elements[i].focus();
	return false;
    }
    else
	return true;
}
function ismaxlength(obj) {
        var mlength = obj.getAttribute ? parseInt(obj.getAttribute("maxlength")) : ""
        if (obj.getAttribute && obj.value.length > mlength)
            obj.value = obj.value.substring(0, mlength)
    }
function validarTipoArch(arch){
    var nombArch=arch.files[0].name;
    var sizeF=arch.files[0].size;
    var arrNomb=nombArch.split('.');
    var last=arrNomb.pop();
    var valido=0;
    var tipo_anexo=$("input[name=tipo_anexo]:hidden");
    for(var i=0;i<anex_tipo.length;i++){
	if(anex_tipo[i].ext==last.toLowerCase()){
	    tipo_anexo.val(anex_tipo[i].id);
	    //alert(anex_tipo[i].id);
	    valido=1;
	    break;
	}
    }
    if(valido==0){
	alert("AVISO: Tipo de archivo no valido, comuniquese con el administrador para informarse sobre los tipos de archivo");
	arch.val('');
	return false;
    }
    else{
	return true;
    }

}
function actualizar(){
    var formData=new FormData($('#formanexos')[0]);
    var userfile=$("#userfile").val();
    var desc=$.trim($("#descr").val()).length;
    if(userfile==""){
	alert('Cargue Un archivo');
	return false;
    }
    if(desc<10){
	alert("En la descripción del Asunto digite un texto relacionado y acorde al documento anexo, por ejemplo:\n- Informe de pasivos y activos\n- Relación de los asistentes al evento de capacitacion\n- Anexo de pruebas y estadísticas del mes de febrero");
	return false;
    }
    $("#bactual").hide();
    $("#bcerrar").hide();
    $("#descr").prop('disabled','disabled');
    //$("#bcerrar").val('Cerrar');
    //var formData=new FormData(form);
    //var file=$('#userfile');
    //formData.append('file',file);
    //console.log(file);
    $.ajax({
	url: "<?php echo $url?>",
	type: "POST",
	data: formData,
	processData: false,
	contentType: false
    })
    .done(function(data){
	$("#response").html(data);
	setTimeout(cerrar2,2500);
    })
    .fail(function(){
	alert("Fallo el ajax");
    });
}
function reloadSearch(param1,param2){
            if(!isLoading){
                var searchTerm=$(param1).val();
                var trad=$('#trad:checked').val();
                //var fecha_ini=$('#fecha_ini').val();
                //var fecha_fin=$('#fecha_fin').val();
		var control;
		if(param2==2){
		    control=2;
		}
		else{
		    control=4;
		}
                if(searchTerm.length>control){
                    //timeout=setTimeout(function(){
                    isLoading=true;
                    //log("ajax call - " + searchTerm, true);

                // Simulate a real ajax call
                setTimeout(function() { isLoading = false; }, 1000);
            //}, delay);
                    //if(param2==0)
                        //var formData="action=busqRad&norad="+searchTerm+"&fecha_ini="+fecha_ini+"&fecha_fin="+fecha_fin;
                    if(param2==1)
                        var formData="action=NuevoTercero&parametro="+searchTerm+"&busq=0&trad="+trad;
                    if(param2==2)
                        var formData="action=NuevoTercero&parametro="+searchTerm+"&busq=1&trad="+trad;
                    //alert(formData);
                    $.ajax({
                        url : "<?php echo $url?>",
                        type: "POST",
                        data: formData,
                    })
                    .done(function(data){
                        $('#response').html(data);
                    })
                    .fail(function(){
                        alert("Fallo el ajax");
                    });
                }
		else{
		    $('#response').html("");
		}
            }
                    //log("No hay suficientes caracteres");
            else{
                //alert("Falla");
                //log("Sigue haciendo la busqueda anterior");
            }
        }
$(function(){
            $("#norad").keyup(function(){
                if(timeout){
                    clearTimeout(timeout);
                }
                reloadSearch("#norad",0);
            });
            $("#nombre").keyup(function(){
                if(timeout){
                    clearTimeout(timeout);
                }
                reloadSearch("#nombre",2);
            });
            $("#nodoc").keyup(function(){
                if(timeout){
                    clearTimeout(timeout);
                }
                reloadSearch("#nodoc",1);
            });
            $("input[name=ra1]:radio").change(function(){
                switch($(this).val()){
                    case '1':
                        $('#p1').hide('fast');
                        $('#p2').hide('fast');
                        $('#p3').hide('fast');
                        $('#p4').show('fast');
                    break;
                    case '2':
                        $('#p1').hide('fast');
                        $('#p2').hide('fast');
                        $('#p3').hide('fast');
                        $('#p5').show('fast');
                    break;
                    case '3':
                        $('#p1').hide('fast');
                        $('#p2').hide('fast');
                        $('#p3').hide('fast');
                        $('#p6').show('fast');
                    break;
                }
            })
	    $("input[name=trad]:radio").change(function(){
                var trad=$("input[name=trad]:checked").val();
                var descrad=$("#t"+trad).val();
                var formdata="action=cargarTRad&trad="+trad+"&descrad="+descrad;
                //alert("Ha seleccionado radicado de "+descrad);
                if(tpDepeRad[trad]==null){
                    alert("El tipo de radicacion "+descrad+" no tiene consecutivos para esta dependencia.\nFavor comuniquese con el administrador del sistema");
                    $("input[name=trad]").prop('checked',false);
                }
                else{
                    //alert(formdata);
                    $("#rtrad").hide('fast');
                    $('#p1').show('fast');
                    $('#p2').show('fast');
                    $('#p3').show('fast');
                    $('#p7').show('fast');
                $.ajax({
                    url:  "<?php echo $url?>",
                    type: "POST",
                    data: formdata,
                })
                .done(function(data){
                    $("#defitrad").html(data);
                })
                .fail(function(){
                    alert("Fallo el ajax");
                })
                }
            });
        });
function asoccOtro(id,tdid_codi,tipo,param1,param2,param3,param4,param5,param6,param7,cont,pais2,dpto,muni){
            var trad=$('#trad:checked').val();
            var descrad=$('#descrad').val();
	    var radi=$('#radi').val();
            var formdata="action=cargaTercero&id="+id+"&tdid_codi="+tdid_codi+"&tipo="+tipo+"&param1="+param1+"&param2="+param2+"&param3="+param3+"&param4="+param4+"&param5="+param5+"&param6="+param6+"&param7="+param7+"&cont="+cont+"&pais2="+pais2+"&dpto="+dpto+"&muni="+muni+"&trad="+trad+"&descrad="+descrad+"&radi="+radi;
            $('#main-tab').hide('fast');
            $.ajax({
                url:  "<?php echo $url?>",
                type: "POST",
                data: formdata,
            })
            .done(function(data){
                    $("#response").html(data);
            })
            .fail(function(){
                alert("Fallo el ajax");
            })
        }
    function radicar(){
	var param1=$.trim( $("#param1").val() );
        var param2=$.trim( $("#param2").val() );
        var param3=$.trim( $("#param3").val() );
        var param4=$.trim( $("#param4").val() );
        var param5=$.trim( $("#param5").val() );
        var param6=$.trim( $("#param6").val() );
        var param7=$.trim( $("#param7").val() );
        var param8=$.trim( $("#param8").val() );
        var param9=$.trim( $("#descr").val() );
        //var param10=$.trim( $("#param10").val() );
        var conti=$("#cont").val();
        var pais2=$("#pais2").val();
        var dpto=$("#dpto").val();
        var muni=$("#muni").val();
	var id=$("#id").val();
        //var tipoDeri=$("#tipoDeri").val();
        var radi=$("#radi").val();
        var trad=$("input[name=trad]:checked").val();
        var descrad=$("#t"+trad).val();
	var tipo=$("#tipo").val();
	var mrec=$("#mrec").val();
	var formData="action=radicarAnexo&id="+id+"&param1="+param1+"&param2="+param2+"&param3="+param3+"&param4="+param4+"&param5="+param5+"&param6="+param6+"&param7="+param7+"&param8="+param8+"&param9="+param9+"&conti="+conti+"&pais2="+pais2+"&dpto="+dpto+"&muni="+muni+"&trad="+trad+"&mrec="+mrec+"&tipoDeri=0&radiDeri="+radi+"&descrad="+descrad+"&tipo="+tipo;
	if(param9.length==0){
                alert("El Campo Descripcion es obligatorio");
                return false;
        }
	$("#descr").prop('disabled','disabled');
	$("#gendoc").show();
	$("#radicarb").hide();
	$("#cerrar").hide();
	//$("#gendoc").show();
	$.ajax({
            url : "<?php echo $url?>",
            type: "POST",
            data: formData
        })
        .done(function(data){
            $("#radicados").html(data);
            })
        .fail(function(){
            alert("Fallo el ajax");
        });
    }
    /*function generarDoc(){
	var postData = 
         {
           "bid":"1",
           "location1":"1","quantity1":"hola","price1":"hola",
           "location2":"2","quantity2":"mundo","price2":"hola",
           "location3":"3","quantity3":"hola","price3":"hola"
        }
    	var dataString = JSON.stringify(postData);
	$.ajax({
            type: "POST",
            //dataType: "json",
            url: "<?php echo $url?>",
            data: {myData:dataString,'action':'json'},
            //contentType: "application/json; charset=utf-8",
            success: function(data){
                //alert('Items added');
		$("#gendocd").html(data);

            },
            error: function(e){
                console.log(e.message);
            }
    	});
    }*/


    function modificAnex(){
	var formData=new FormData($('#formanexos')[0]);
    	var userfile=$("#userfile").val();
    	var desc=$.trim($("#descr").val());
	var descold=$("#descr2").val();
	var tpr=$("#tpr").val();
    	if(userfile.length==0 && desc==descold && typeof tpr === typeof undefined){
            alert('No ha ingresado informacion a modificar');
            return false;
    	}
    	if(desc.length<10){
            alert("En la descripción del Asunto digite un texto relacionado y acorde al documento anexo, por ejemplo:\n- Informe de pasivos y activos\n- Relación de los asistentes al evento de capacitacion\n- Anexo de pruebas y estadísticas del mes de febrero");
            return false;
    	}
    	$("#bactual").hide();
    	$("#descr").prop('disabled','disabled');
    	$("#bcerrar").hide();
    //var formData=new FormData(form);
    //var file=$('#userfile');
    //formData.append('file',file);
    //console.log(file);
    $.ajax({
        url: "<?php echo $url?>",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false
    })
    .done(function(data){
        $("#response").html(data);
	setTimeout(cerrar2,2500);
    })
    .fail(function(){
        alert("Fallo el ajax");
    });

    }
    function borrarAnexo(){
	var anexCodigo=$("#anexCodigo").val();
	var formData="action=borrarAnex&anexNume="+anexCodigo;
	$("#tabmain").hide();
	$.ajax({
            url : "<?php echo $url?>",
            type: "POST",
            data: formData
        })
        .done(function(data){
            $("#boresp").html(data);
	    setTimeout(cerrar2,2500);
        })
        .fail(function(){
            alert("Fallo el ajax");
        });
    }
</script>
<div id='gendocd'>
<?php switch($action){
    case 0:
?>
<br>
<form id='formanexos' method='post'>
<input type='hidden' value='' id='tipo_anexo' name='tipo_anexo'>
<input type='hidden' value='crearAnexo' id='action' name='action'>
<input type='hidden' value='<?php echo $radi?>' id='radi' name='radi'>
<table class="borde_tab" width='90%' style='border-collapse:collapse;border-spacing:0px;border-spacing:0px' align='center'>
    <tr>
        <th class='titulos4'>CREAR ANEXO A DOCUMENTO <?php echo $radi?></th>
    </tr>
    <!--<tr>
	<td>
	    <table class="borde_tab" width="100%" border="0">
		<tr><td class="titulos2" height="25" align="left" colspan="2">ATRIBUTOS</td></tr>
		<tr>
                    <td width="50%" class="listado2"><input type="checkbox" id="sololect" name="sololect" class="select"> Solo lectura</td>
                    <td align="left" class="listado2">Tipo de Anexo: <select id="tipo_clase" class="select" name="tipo"></select></td>
		</tr>
	    </table>
	</td>
    </tr>-->
    <tr>
        <td>
           <table width="100%" cellspacing="2" cellpadding="0" border="0" class="borde_tab">
                <tr align="center">
                    <td class="titulos2" width="100%" height="25" colspan="2">
                        ADJUNTAR ARCHIVO
                    </td>
                </tr>
                <tr class="listado2" align="center">
                    <td align='center'><div id='response'>Esta función permite anexar soportes directamente relacionados al radicado diligenciado.<br><br><input id="userfile" class="tex_area" type="file" onchange="return validarTipoArch(this);" name="userfile1"><br>
			<b>SUGERENCIA:</b> Anexe el soporte documental preferiblemente en extensión PDF, cualquier otra extencion de archivo office es permitido.</td></tr>
           </table>
        </td>
    </tr>
    <?php     if($perm_tipif_anexo==1){
    include_once "$ruta_raiz/core/Modulos/radicacion/clases/tx.php";
    $tx=new tx($ruta_raiz);
    ?>
    <tr>
        <td>
           <table width="100%" cellspacing="2" cellpadding="0" border="0" class="borde_tab">
                <tr align="center">
                    <td class="titulos2" width="100%" height="25" colspan="2">
                        TIPO DOCUMENTAL
                    </td>
                </tr>
                <tr class="listado2" align="center">
                    <td align='left'>Tipo de documento </td><td align='right'><?php echo $tx->tipoDocumental(0)?></td></tr>
           </table>
        </td>
    </tr>
    <?php     }
    ?>
    <tr>
	<td>
	<table width="100%" class="borde_tab">
                                    <tr valign="top">
                                        <td valign="top" colspan="2" class="titulos2">Descripcion</td>
                                    </tr>
                                    <tr valign="top">
                                        <td valign="top" height="66" align="center" class="listado2" colspan="2">
                                    <center>Ingrese el texto del asunto y/o descripción del Anexo, máximo 550 caracteres.<br>
                                        <textarea onkeypress="return noEnter(this, event)" onkeyup="return ismaxlength(this)" maxlength="595" id="descr" class="tex_area" rows="3" cols="70" name="descr"></textarea><br></center> 
                            </td>
                        </tr>
			<tr class="listado2">
			    <td align="center">
                                <input type="button" value="Anexar" onclick="return actualizar()" class="botones_largo" name="button" id='bactual'> 
                       		<input type="button" onclick="cerrar2();" value="Cancelar" class="botones" id='bcerrar'> 
                            </td>
                        </tr>
                    </table>
	</td>
    </tr>
</table>
</form>
<?php 	break;
    case 1:
	$tipoRad=$rad->permTRad2($dependencia,$id_rol);
	?>
	<table style="width:90%;border-spacing:0;border-padding:0;" border="0" align="center" class="BORDE_TAB" id='main-tab'>
	    <tr align="center">
        	<td class="titulos4" colspan=2>
        	<center>RADICADO DE RESPUESTA O TR&Aacute;MITE DE <?php echo $radi?><input type='hidden' value='<?php echo $radi?>' id='radi'>&nbsp;<div id='defitrad'></div></center></td></tr>
		<tr id='rtrad' style="display:table-row">
                    <td class="titulos2" width="25%">Seleccione un tipo de radicaci&oacute;n</td>
                    <td class="listado2" width="25%"><?php echo $tipoRad?></td>
                </tr>
		<tr id='p1' style="display:none"><td class="titulos2" width="25%" colspan="2"><input id='r1' type='radio' name='ra1' value=1>Nombre</td></tr>
                <tr id='p2' style="display:none"><td class="titulos2" width="25%" colspan="2"><input id='r2' type='radio' name='ra1' value=2>Documento</td></tr>
		<tr id='p4' style="display:none"><td class="titulos2" width="15%">Nombre</td>
		    <td class="titulos2" width="25%"> <input type='text' id='nombre'></td></tr>
                <tr id='p5' style="display:none"><td class="titulos2" width="15%">Documento</td>
		    <td class="titulos2" width="25%"> <input type='text' id='nodoc'></td></tr>
		<tr>
                    <td class="info" colspan=4 id='cancelar'><center><b>RECUERDE:</b> Esta funci&oacute;n genera un nuevo radicado de respuesta o tr&aacute;mite relacionado directamente con el radicado <b><?php echo $radi?></b> que est&aacute; en su bandeja de carpetas.</td>
                </tr>
		<tr>
                    <td class="info" colspan=4 id='cancelar'><center><input class='botones' type='button' value='Cancelar' onclick="cerrar2()"></center></td>
                </tr>
	</table>
	<div id='response'></div>
	<?php 	break;
    case 2:
	$resultado=$rad->traerDescAnexo($anexCodigo);
	$tpr=$resultado['tpr'];
	$descr=$resultado['desc'];
	?>
	<br>
<form id='formanexos' method='post'>
<input type='hidden' value='' id='tipo_anexo' name='tipo_anexo'>
<input type='hidden' value='modifAnex' id='action' name='action'>
<input type='hidden' value='<?php echo $radi?>' id='radi' name='radi'>
<input type='hidden' value='<?php echo $descr?>' id='descr2'>
<input type='hidden' value='<?php echo $anexCodigo?>' id='anexCodigo' name='anexCodigo'>
<table class="borde_tab" width='90%' style='border-collapse:collapse;border-spacing:0px;border-spacing:0px' align='center'>
    <tr>
        <th class='titulos4'>MODIFICAR ANEXO <?php echo $anexCodigo?></th>
    </tr>
    <tr>
        <td>
           <table width="100%" cellspacing="2" cellpadding="0" border="0" class="borde_tab">
                <tr align="center">
                    <td class="titulos2" width="100%" height="25" colspan="2">
                        ADJUNTAR ARCHIVO
                    </td>
                </tr>
                <tr class="listado2" align="center">
                    <td align='center'><div id='response'>
			<input id="userfile" class="tex_area" type="file" onchange="return validarTipoArch(this);" name="userfile1"><br>
			<b>SUGERENCIA:</b> Anexe el soporte documental preferiblemente en extensi&oacute;n PDF, cualquier otra extension de archivo office es permitido.</td></tr>
           </table>
        </td>
    </tr>
    <?php     if($perm_tipif_anexo==1){
    include_once "$ruta_raiz/core/Modulos/radicacion/clases/tx.php";
    $tx=new tx($ruta_raiz);
    ?>
    <tr>
        <td>
           <table width="100%" cellspacing="2" cellpadding="0" border="0" class="borde_tab">
                <tr align="center">
                    <td class="titulos2" width="100%" height="25" colspan="2">
                        TIPO DOCUMENTAL
                    </td>
                </tr>
                <tr class="listado2" align="center">
                    <td align='left'>Tipo de documento </td><td align='right'><?php echo $tx->tipoDocumental(($tpr=='')?0:$tpr)?></td></tr>
           </table>
        </td>
    </tr>
    <?php     }
    ?>
    <tr>
        <td>
        <table width="100%" class="borde_tab">
                                    <tr valign="top">
                                        <td valign="top" colspan="2" class="titulos2">Descripcion</td>
                                    </tr>
                                    <tr valign="top">
                                        <td valign="top" height="66" align="center" class="listado2" colspan="2">
                                    <center>Ingrese el texto del asunto y/o descripción del Anexo, máximo 550 caracteres.<br>
                                        <textarea onkeypress="return noEnter(this, event)" onkeyup="return ismaxlength(this)" maxlength="595" id="descr" class="tex_area" rows="3" cols="70" name="descr"><?php echo $descr?></textarea><br></center>
                            </td>
                        </tr>
			<tr class="listado2">
			    <td align="center"><span class='leidos'><b>RECUERDE:</b> Esta función permite modificar anexos relacionados al radicado 
			diligenciado anteriormente.</span></td>
			</tr>
                        <tr class="listado2">
                            <td align="center">
                                <input type="button" value="Modificar Anexo" onclick="return modificAnex()" class="botones_largo" name="button" id='bactual'>
                                <input type="button" onclick="cerrar2();" value="Cancelar" class="botones" id='bcerrar'>
                            </td>
                        </tr>
                    </table>
        </td>
    </tr>
</table>
</form>
	<?php 	break;
    case 3:
	?>
	<table width="100%" class="borde_tab" cellpadding='0' cellspacing='0'>
	    <tr>
		<td class='listado2' colspan='2' style="text-align:center;"><b><div id='boresp'>Esta seguro de borrar Anexo N&uacute;mero <?php echo $anexCodigo?>?</div></b><input id='anexCodigo' type='hidden' value='<?php echo $anexCodigo?>'></td>
	    </tr>
	    <tr style="display:table-row" id='tabmain'>
		<td class='listado2' style="text-align:right;"><input class='botones' value='Si' type='button' onclick="borrarAnexo();"></td>
		<td class='listado2' style="text-align:left;"><input class='botones' value='No' type='button' onclick="cerrar2();"></td>
	    </tr>
	</table>
	<?php 	break;
}
?>
</div>
