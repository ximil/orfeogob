<?php date_default_timezone_set('America/Bogota');
session_start();
foreach ($_GET as $key => $valor)
    ${$key} = $valor;
foreach ($_POST as $key => $valor)
    ${$key} = $valor;
$krd = $_SESSION["krd"];
$dependencia = $_SESSION["dependencia"];
$usua_doc = $_SESSION["usua_doc"];
$codusuario = $_SESSION["codusuario"];
$id_rol = $_SESSION["id_rol"];
$ruta_raiz = "../../../..";
//include($ruta_raiz . '/core/vista/validadte.php');
//path para pintar el recorset en la vista.
$scripturl = $ruta_raiz . '/core/Modulos/radicacion/vista/operlisMas.php';
$scripturl2 = $ruta_raiz . '/core/Modulos/radicacion/vista/operBuscarUso.php';
//inicializa  dependecias
$tituloPage = 'Radicación Masivas con Listados';
include_once $ruta_raiz . '/core/Modulos/radicacion/clases/genListado.php';
$lisMas = new genListado($ruta_raiz);
$rsx = $lisMas->consultarBase($_SESSION['dependencia']);

include_once $ruta_raiz . '/core/clases/usuarioOrfeo.php';
$krd = $_SESSION["krd"];
$dependencia = $_SESSION["dependencia"];
$usua_doc = $_SESSION["usua_doc"];
$codusuario = $_SESSION["codusuario"];
$ruta_raiz = "..";

$ruta_raiz = "../../../..";
include($ruta_raiz . '/core/vista/validadte.php');
include_once $ruta_raiz . '/core/Modulos/trd/clases/series.php';
$script3 = "$ruta_raiz/core/Modulos/trd/vista/operinfoTRD.php";
include_once "$ruta_raiz/core/Modulos/trd/clases/tpDocumento.php";

//inicializa tipos documentales
$tpdoc = new tpDocumento($ruta_raiz);
//consulta


$tdocumentales = $tpdoc->consultar();
//inicializa  series
$serie = new series($ruta_raiz);
$Listado = $serie->consultar();
$numn = count($Listado);
//include_once $ruta_raiz.'/core/language/'.$_SESSION ["lang"].'/data.inc';
$optionSSd = "";
for ($i = 0; $i < $numn; $i++) {
    $codserie = $Listado[$i]["CODIGO"];
    $dserie = $Listado[$i]["DESCRIP"];
    $optionSSd.="<option value='$codserie'>$codserie - $dserie</option>";
}

$optionTp = "";
$numtp = count($tdocumentales);
for ($i = 0; $i < $numtp; $i++) {
    $nomTP = $tdocumentales[$i]["DESCRIP"];
    $codTP = $tdocumentales[$i]["CODIGO"];
    $optionTp.="<option value='$codTP'>$nomTP</option>";
}
?>

<html>
    <head>
        <link rel="stylesheet" type="text/css" 	href="<?php echo  $ruta_raiz ?>/js/calendario/calendar.css">
        <script src="<?php echo  $ruta_raiz ?>/old/js/popcalendar.js"></script>
        <link rel="stylesheet" href="../../../../estilos/caprecom/orfeo.css">
        <script language="JavaScript" src="<?php echo  $ruta_raiz ?>/js/common.js"></script>
        <script language="JavaScript" type="text/javascript"	src="<?php echo  $ruta_raiz ?>/js/calendario/calendar_eu.js"></script>
        <script type="text/javascript">
<?php echo $rsx['script']; ?>

            function nuevo() {
                vistaFormUnitid('formlista', 1);
                vistaFormUnitid('accionlista', 2);
                vistaFormUnitid('Ladd', 1);
                vistaFormUnitid('Lmod', 2);
                document.getElementById('idmas').value = "";
                document.getElementById('masnomb').value = "";
                document.getElementById('campextra').value = "";
                document.getElementById('masest').value = "";
            }

            function cancelar() {
                vistaFormUnitid('formlista', 2);
                vistaFormUnitid('accionlista', 2);
            }
            function delLista(id) {

                var idlist = document.getElementById('lista').value;
                var pstvar = 'action=delItem&idList=' + idlist + '&id=' + id;
                partes('<?php echo $scripturl2; ?>', 'listados', pstvar, '');
            }
            function    listar(selTag) {
                var x = selTag.options[selTag.selectedIndex].text;
                var id = selTag.options[selTag.selectedIndex].value;
                document.getElementById('tituloListado').innerHTML = x;
                var ida = document.getElementById('lista').text;
                var poststr = "action=Listar&idList=" + id;
                partes('<?php  echo $scripturl2; ?>', 'listados', poststr, '');
            }
            function modificar() {
                // 	    vistaFormUnitid('Ladd', 2);
                //	    vistaFormUnitid('Lmod', 1);
                var id = document.getElementById('lista').value;
                var nomb = document.getElementById('masnomb').value = ml[id]['NOMBRE'];
                Start('buscar_usuario.php?&lista=' + id + '&nombLista=' + nomb, 1024, 400);
            }

            function Start(URL, WIDTH, HEIGHT) {
                windowprops = "top=0,left=0,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=1100,height=550";
                preview = window.open(URL, 'preview', windowprops);
            }

            function        modNomb() {
                vistaFormUnitid('Ladd', 2);
                vistaFormUnitid('Lmod', 1);
                var lista = document.getElementById('lista').value;
                if (lista > 0) {
                    vistaFormUnitid('formlista', 1);
                    document.getElementById('idmas').value = ml[lista]['ID'];
                    document.getElementById('masnomb').value = ml[lista]['NOMBRE'];
                    document.getElementById('campextra').value = ml[lista]['EXTRA'];
                    document.getElementById('masest').value = ml[lista]['EST'];
                }
                else {
                    alert('Debe seleccionar una lista');
                }
            }

            function        modLista() {
                vistaFormUnitid('accionlista', 2);
                var id = document.getElementById('idmas').value;
                var nomb = document.getElementById('masnomb').value;
                var extra = document.getElementById('campextra').value;
                var esta = document.getElementById('masest').value;
                if (confirm("Esta seguro de modificar la lista Masiva " + nomb + "? ")) {
                    var poststr = "action=modlista&idlista=" + id + "&nomb=" + nomb + "&extra=" + extra + "&estado=" + esta;
                    partes('<?php echo $scripturl; ?>', 'accionlista', poststr, '');
                    vistaFormUnitid('accionlista', 1);
                    auLista('listadoMas');
                }
                else {
                    return false;
                }
            }
            function auLista(div) {

                var poststr = "action=listado";
                partes('<?php echo $scripturl; ?>', div, poststr, '');
            }
            function addLista() {
                vistaFormUnitid('accionlista', 2);
                var nomb = document.getElementById('masnomb').value;
                var extra = document.getElementById('campextra').value;
                var esta = document.getElementById('masest').value;
                var poststr = "action=addList&nomb=" + nomb + "&extra=" + extra + "&estado=" + esta;
                partes('<?php echo $scripturl; ?>', 'accionlista', poststr, '');
                vistaFormUnitid('accionlista', 1);
                auLista('listadoMas');
            }
            function addItem(idcod, cc, nomb, ap, ap2, muni, dir, tp) {
                var lista = document.getElementById('lista').value;
                //alert( nomb+' '+ap+' '+ap2+' '+muni+' '+dir);
                var poststr = "action=addListado&idList=" + lista + "&tpemp=" + tp + "&no_documento1=" + cc + "&idcod=" + idcod;
                partes('<?php echo $scripturl2; ?>', 'listados', poststr, '');
                //            vistaFormUnitid('accionlista', 1);
                //          auLista('listadoMas');
            }
            function buscarUsuario() {
                vistaFormUnitid('opeR', 1);
                var doc = document.getElementById('no_documento').value;
                var nomb = document.getElementById('nombre_essp').value;
                var tbusqueda = document.getElementById('tbusqueda').value;

                var poststr = "action=consultarBusqueda&nomb=" + nomb + "&doc=" + doc + "&tbusqueda=" + tbusqueda;
                partes('<?php echo $scripturl2; ?>', 'opeR', poststr, '');
                //vistaFormUnitid('accionlista', 1);
                //auLista('listadoMas');  

            }
        </script>
        <style type="text/css">
            dataPres .td {
                font-family: Verdana, Arial, Helvetica, sans-serif;
                font-size: 10px;
                font-weight: bolder;
                color: #069;
                text-decoration: none;
            }
            #lista
            {
                width: 200px;
            }
        </style>
    </head>

    <body bgcolor="#FFFFFF" style="margin: 0;margin-top: 0;margin-left: 0;margin-right: 0">
        <div id="spiffycalendar" class="text"></div>
        <?php //pinta el menu de la barra general
//include 'admPrestamosTab.php';
        ?>
        <table class='borde_tab'  style='width: 100%' >
            <tr>
                <td  align='center' class="titulos4">
                    <?php echo $tituloPage; ?>
                </td>
            </tr>
        </table>
        <table  style="border-collapse: collapse; margin:0 ;height: 500px;  width: 100%; padding: 0 ; border-top: 0; border-spacing: 0">
            <tr >
                <td  class="titulos2" style="width: 40%;vertical-align: top; padding: 0px;margin: 0;margin-top: 0;margin-left: 0;margin-right: 0" >
                    <table class=borde_tab style="border-collapse: collapse; margin:0 ;  width: 100%; padding: 0 ; border-top: 0; border-spacing: 0; display : block" >
                        <tr>
                            <td   align='center'  class="titulo1">
                                Aplicaion de Tabla de Retenci&oacute;n Documental</td>
                        </tr>
                        <tr>
                            <td class='titulos2' >Serie</td> </tr>
                        <tr>
                            <td  class="listado2">
                                    <select  id="srd" name='srd' onChange='consultarSub()'  class='select'  style="width:300px; ">
                                        <option value='0' selected>--- Seleccione ---</option>
                                        <?php echo $optionSSd; ?>
                                    </select>
                                </td>
                        </tr>
                        <tr>
                            <td class='titulos2' >Subserie</td> </tr>
                        <tr>
                            <td  class="listado2">
                                <div id='ComboSBRD'><select  id="sbrd" name='sbrd'   class='select'  style="width:300px; ">
                                        <option value='0'>--- Seleccione ---</option>
                                    </select></div>
                            </td>
                        </tr>
                        <tr>  <td class='titulos2'>Tipo de documentos </td> </tr>
                        <tr>
                            <td  class="listado2">	  <select  name='tpdoc' id='tpdoc'  class='select'  style="width:300px; ">
                                    <option value='0' ><font>--- Seleccione ---</font></option>
                                    <?php echo $optionTp; ?>
                                </select></td></tr>
                    </table>
                </td>
                <td style="width: 5px" rowspan="3"></td>
                <td style="vertical-align: top;" rowspan="3">
                    <table style="border-collapse: collapse; margin:0 ;  width: 100%; padding: 0 ; border-top: 0; border-spacing: 0; display : block">

                        <table width="100%" cellpadding="0" cellspacing="0"  class="borde_tab">
                            <tr class='titulo1'>
                                <td colspan='2' >Datos de Lista <label id='tituloListado'> </label></td>
                            </tr>
                            <tr>
                                <td>
                                    <div id="listados" style="vertical-align : top; padding : 0px; width : 100%; height : 500px; overflow : auto; "  class='listado2' ></div>
                                    <div id="Resp" style="vertical-align : top; padding : 0px; width : 100%; height : 300px; overflow : auto; display : none"  class='listado2' ></div>

                                </td>
                            </tr>
                     
                    </table>
                </td>

            </tr>
            <tr >
                <td  class="titulos2" style="vertical-align: top; padding: 0px;margin: 0;margin-top: 0;margin-left: 0;margin-right: 0" >
                    <table style="width: 100%;padding: 0px;vertical-align: top;border-collapse: collapse" class="borde_tab">
                        <tr class="titulo1">
                            <td colspan="2"> Datos</td>
                        </tr>
                                             <tr >
                            <td class="titulos2">Tipo de radicacion</td>
                                <td class="listado2">
                                <select  class='select'  id='tprad' ><option value='0' selected>--- Seleccione ---</option></select>
                            </td>
                        </tr>
    
                        <tr>
                             <td class="titulos2">
                                Listado
                            </td>
                            <td class="listado2">
                                <div id='listadoMas'>  <select  class='select' onchange='listar(this);' id='lista' id="lista"><option value='0' selected>--- Seleccione ---</option><?php echo $rsx['option']; ?></select></div>
                            </td>
                        </tr>
                        <tr>
                             <td class="titulos2">
                                Asunto
                            </td>
                            <td class="listado2" >
                                <textarea name="asunto" cols="40" rows="5"></textarea>
                            </td>
                        </tr>
                    </table>
                </td>

            </tr>
        </table>
        <script>
            //	buscar('sin');
        </script>
    </body>
</html>

