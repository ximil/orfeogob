<?php /**
 * @name :   
 * @desc :   Genera checks de copias y destinatario.
 * @author  :  Hardy  Deimont Niño Velasquez.
 * @version 0.1
 */
$ruta_raiz = "../../../..";
include_once "$ruta_raiz/core/Modulos/radicacion/clases/Masiva-class.php";
//include_once "$ruta_raiz/core/clases/ConnectionHandler.php";

include_once $ruta_raiz . "/core/config/config-inc.php";

//$db->conn->debug=true;
//	print_r ( $_POST );
session_start();
date_default_timezone_set('America/Bogota');

$mas = new masivas($ruta_raiz);

$action = $_POST ['action'];
//print_r($_POST);


if ($action == 'consultarBusqueda') {
   // print_r($_POST);
    $nombre_essp = $_POST ['nomb'];
    $cc=$_POST ['doc'];
    $tp=$_POST ['tbusqueda'];
   echo  $data = $mas->busqueda3($tp,$nombre_essp,$cc);
}
if ($action == 'delItem') {
   // print_r($_POST);
    $idlista = $_POST['idList'];
    $id = $_POST['id'];
    $mas->setIdList($idlista,1);
    $mas->delitem( $id);
    $data = $mas->listar(1,1);
    echo $data;
}
if ($action == 'Listar') {
    $idlista = $_POST['idList'];
    $mas->setIdList($idlista,0);
    $data = $mas->listar(1);
    echo $data;
}
if ($action == 'Listar2') {
    $idlista = $_POST['idList'];
    $mas->setIdList($idlista,0);
    $data = $mas->listar(2);
    echo $data;
}
if ($action == 'Listar3') {
    //print_r($_POST);
    //tpemp 0 ciudadanos 2 empresa
    $idlista = $_POST['idList'];
    $mas->setIdList($idlista,1);
    $data = $mas->listar(1,1);
    echo $data;
}

if ($action == 'addListado') {
     //  print_r($_POST);
    //tpemp 0 ciudadanos 2 empresa

    $idlista = $_POST['idList'];
    $tpemp = $_POST['tpemp'];
    $doc = $_POST['no_documento1'];
    $exp = $_POST['exp'];
    $idcod=$_POST['idcod'];

    $mas->setIdList($idlista);
    $data = $mas->insListainsLista($tpemp, $idcod, $exp);
    $data = $mas->listar(1,1);
    echo $data;
}
                    