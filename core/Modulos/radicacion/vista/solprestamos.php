<?php 
	session_start(); date_default_timezone_set('America/Bogota'); date_default_timezone_set('America/Bogota'); 
	foreach ($_GET as $key => $valor)  $$key = $valor;
	foreach ($_POST as $key => $valor)  $$key = $valor;
	$krd = $_SESSION["krd"];
	$dependencia = $_SESSION["dependencia"];
	$usua_doc = $_SESSION["usua_doc"];
	$codusuario = $_SESSION["codusuario"];
	$id_rol = $_SESSION["id_rol"];
	$ruta_raiz="../../../..";
	include($ruta_raiz.'/core/vista/validadte.php');
	//path para pintar el recorset en la vista.
	$scripturl= $ruta_raiz.'/core/Modulos/radicacion/vista/operPrestamos.php';
	//inicializa  dependecias
	include_once $ruta_raiz.'/core/clases/dependencia.php';
	$depe= new dependencia($ruta_raiz);
	$dependecias=$depe->consultarTodo();
	$numndep=count($dependecias);
	$optionDepe="<option value='0'>Todas Las dependencias</option>";
	for ($i = 0; $i < $numndep; $i++) {
		$nomDepe = $dependecias[$i]["depe_nomb"];
		$codDepe = $dependecias[$i]["depe_codi"];
		if($codDepe!='999')
			$optionDepe.="<option value='$codDepe'>$codDepe - $nomDepe</option>";				
	}	
	include_once $ruta_raiz.'/core/clases/usuarioOrfeo.php';
	//print_r($_SESSION);
?>

<html>
	<head>
		<link rel="stylesheet" href="../../../../estilos/caprecom/orfeo.css">
		<script language="JavaScript" src="<?php echo $ruta_raiz?>/js/common.js"></script>
		<script type="text/javascript">

			function buscar(){
					//partes(url,div,parameter,getpara)
					var poststr = "action=presUsuarios"; 
					//alert(poststr);
					partes('<?php  echo $scripturl; ?>','listados',poststr,'');
				
			}
			function accion(accion,numRa,idp){
					//partes(url,div,parameter,getpara)
					var poststr = "action="+accion+"&rad="+numRa+"&idp="+idp; 
					//alert(poststr);
					if(confirm("Esta seguro de cancelar la solicitud de prestamo del radicado "+numRa+"? ")){
					vistaFormUnitid('respuesta',1);
					vistaFormUnitid('listados',2);
					partes('<?php  echo $scripturl; ?>','respuesta',poststr,'');
					}
				
			}
			function devolver(){
				var rad = document.getElementById('HnumRad').value;
				var idp = document.getElementById('Hidp').value;
				var obse = document.getElementById('descpmotivo').value;
				var poststr = "action=devolver&rad="+rad+"&idp="+idp+"&observar="+obse; 
				if(obse.length< 10 ){
					alert('Debe Colocar una observacion con un minimo de 10 caracteres');
					return false;
				}							
				vistaFormUnitid('respuesta',1);
				vistaFormUnitid('listados',2);
				vistaFormUnitid('Resp',2);
				partes('<?php  echo $scripturl; ?>','respuesta',poststr,'');
				
			
		}
		function prestar(){
				var rad = document.getElementById('HnumRad').value;
				var idp = document.getElementById('Hidp').value;
				var esta = document.getElementById('estado').value;
				var fech = document.getElementById('fecha_busq').value;
				var obse = document.getElementById('descpmotivo').value;
				if(fech=='' && esta == 2){
					alert('Debe seleccionar fecha de vencimiento');
					return false;
				}
				if(obse.length< 10 ){
					alert('Debe Colocar una observacion con un minimo de 10 caracteres');
					return false;
				}
				if(confirm("Esta seguro de prestar el radicado "+rad+"? ")) {
				var poststr = "action=prestar&rad="+rad+"&idp="+idp+"&observar="+obse+"&fecha="+fech+"&estado="+esta; 
				//alert(poststr);
				vistaFormUnitid('respuesta',1);
				vistaFormUnitid('listados',2);
				vistaFormUnitid('Resp',2);
				partes('<?php  echo $scripturl; ?>','respuesta',poststr,'');
				}
			
		}
			function formprestar(numRa,idp,tp){
				
				
				document.getElementById('idRad').innerHTML = numRa;
				document.getElementById('HnumRad').value=numRa;
				document.getElementById('Hidp').value=idp;
				if(tp==1){
					vistaFormUnitid('delvom',2);
					vistaFormUnitid('btprestamo',2);
					vistaFormUnitid('btdevolucion',1);
					document.getElementById('mti').innerHTML = "Devolver Radicado";
					}
				else{
					vistaFormUnitid('delvom',1);
					vistaFormUnitid('btprestamo',1);
					vistaFormUnitid('btdevolucion',2);
					document.getElementById('mti').innerHTML = "Prestar Radicado";
					}
				vistaFormUnitid('respuesta',2);
				vistaFormUnitid('listados',2);
				vistaFormUnitid('Resp',1);
				}
			function entsub(event,ourform) {
				if (event && event.which == 13)
				buscar();
			else
				return true;
			}
			
			function busqusu() {
				var codepen = document.getElementById('depe').value;
				var pagina = document.forms.busquedaExp;
				var postusu = "actionusu=busqusu&depe="+codepen; 
				//alert(postusu);
			}
			function refresh(){
				vistaFormUnitid('respuesta',2);
				vistaFormUnitid('listados',1);
				vistaFormUnitid('Resp',2);
				buscar();
				}
			function cancelar(){
				vistaFormUnitid('respuesta',2);
				vistaFormUnitid('listados',1);
				vistaFormUnitid('Resp',2);
			}
			function val(){
				var esta = document.getElementById('estado').value;
				if(esta==5){
					vistaFormUnitid('datafech',2);
					}
				else{
				vistaFormUnitid('datafech',1);
				}

			}

		</script>
	</head>

	<body bgcolor="#FFFFFF">
	<div id="spiffycalendar" class="text"></div>
		<?php 
			//pinta el menu de la barra general
			//include 'admPrestamosTab.php';
		?>
		<table class=borde_tab width='100%' cellspacing="2">
			<tr>
				<td  align='center' class="titulos4">
		  		 Documentos Solicitados Para Prestamo 		<input type="button" onclick='refresh()' class="botones" value="refrescar" />
				</td>
			</tr>
		</table>
			
		<form method="post" action="#" name="busquedaExp" style="margin:0"> 

			<table width="100%" height='450px' border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse;">
		  	<tr>
		  		<td height="5">&nbsp;</td>
		  	</tr>
		  	<tr>
			    <td width="5" >&nbsp;</td>
<!-- --------------------------------------------------------------------------- -->
			    <td valign="top" >
			   		<table width="100%"  height='400px' cellpadding="0" cellspacing="0"  class="borde_tab">
			   			<tr class='titulo1'>
				 				<td colspan='2' >Resultado  de la B&uacute;squeda</td>
				 			</tr>
				   		<tr>
				   			<td>
			    				<div id="listados" style="vertical-align : top; padding : 0px; width : 100%; height : 400px; overflow : auto; "  class='listado2' ></div>
								<div id="Resp" style="vertical-align : top; padding : 0px; width : 100%; height : 400px; overflow : auto; display : none"  class='listado2' >							
		<br>
		<br>
		<table width="624" border="0" align="center" cellpadding="0" cellspacing="0">
			<tr class='titulo1'>
				<td colspan='4'><div id='mti'>Prestar radicado</div></td>
			</tr>
			<tr>
				<td class='titulos3' align="left">Radicado No.</td>
				<td class='titulos3' colspan=3 valign="middle"><div id='idRad'></div><input type="hidden" id='HnumRad' ><input type="hidden" id='Hidp' ></td>
			</tr>
			<tr bgcolor="White">
 	            <td class='titulos3' colspan=4 >
 	            <div id='delvom'>
 	            <table>
			<tr bgcolor="White">
 	            <td class='titulos3' align="right" class="titulosError2">Estado:</td>
				<td class='titulos3'>
					<select class="select" name="estado" id="estado" onchange='val()'>
 					<option selected="" value="2" >PRESTADO</option>  
 					<option value="5">PRESTAMO INDEFINIDO</option>
	                  </select></td>
 	            <td class='titulos3' align="right" class="titulosError2">Fecha de Vencimiento:</td>
				<td class='titulos3'>
   
        </td>
					     </tr>
					     </table>
					     </div>
					     </td>
					     </tr>
			<tr>
				<td class='titulos3' valign="middle" align="left">Obsevaci&oacute;n</td>
				<td class='titulos3' colspan=3 valign="middle">
					<textarea class='select' name="descpmotivo" cols="70" rows="6" id="descpmotivo"></textarea>
				</td>
				
				
			</tr>
			<tr>
				<td colspan="4" class='titulos3'><br>
					<table align="center"><tr>
					<td><div id='btprestamo'><input type="button" class="botones" onclick="prestar();" value="Prestamo" /></div></td>
					<td><div id='btdevolucion' style="display: none"><input type="button" class="botones" onclick="devolver();" value="Devolver" /></div></td>
					<td><input type="button" onclick='cancelar()' class="botones" value="Cancelar" />
					</td>
					</tr></table>
					
					 
				</td>
			</tr>
			<tr>
				<td colspan="4" class='titulos3'>&nbsp;</td>
			</tr>
		</table>
							</div>
<div id="respuesta" style="vertical-align : top; padding : 0px; width : 100%; height : 400px; overflow : auto; display : none"  class='listado2' ></div>
			    			</td>
			    		</tr>
			    	</table>
			    </td>
					<td width="5">&nbsp;</td>
		  	</tr>
			</table>
		</form>
		
		<script>
		buscar();
		</script>
	</body>
</html>