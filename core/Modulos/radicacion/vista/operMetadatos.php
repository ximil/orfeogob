<?php session_start(); date_default_timezone_set('America/Bogota'); date_default_timezone_set('America/Bogota');
foreach ($_GET as $key => $valor)  $$key = $valor;
foreach ($_POST as $key => $valor)  $$key = $valor;
$krd = $_SESSION["krd"];
$dependencia = $_SESSION["dependencia"];
$usua_doc = $_SESSION["usua_doc"];
$codusuario = $_SESSION["codusuario"];
$id_rol = $_SESSION["id_rol"];
$tpDepeRad=$_SESSION['tpDepeRad'];
$tpDescRad = $_SESSION["tpDescRad"];
$ruta_raiz="../../../..";
include_once "$ruta_raiz/core/Modulos/radicacion/clases/metadatos.php";
$met=new metadatos($ruta_raiz);
include_once "$ruta_raiz/core/Modulos/radicacion/clases/tx.php";
$tx= new tx($ruta_raiz);
//Cargar variables del log
if(isset($_SERVER['HTTP_X_FORWARD_FOR'])){
    $proxy=$_SERVER['HTTP_X_FORWARD_FOR'];
}else
    $proxy=$_SERVER['REMOTE_ADDR'];
$REMOTE_ADDR=$_SERVER['REMOTE_ADDR'];
include_once "$ruta_raiz/core/clases/log.php";
$log=new log($ruta_raiz);
$log->setRolId($id_rol);
$log->setUsuaCodi($codusuario);
$log->setDepeCodi($dependencia);
$log->setDenomDoc('Radicado');
$log->setProxyAd($proxy);
$log->setAddrC($REMOTE_ADDR);
$est=array(0=>'Inactivo',1=>'Activo');
switch ($action){
    case 'cargaMeta':
        $info=$met->infoMetadatos($id);
        $tags=$met->listarTags($id,false);
        $cont=count($tags)-1;
	if($vista==0){
	    $r1="table-row";
            $r2="none";
	    $boton="<input id='botomodi' class='botones_mediano' type='button' value='Modificar Tags' onclick=\"replaceVista(0);\">";
	}
	else{
	    $r1="none";
            $r2="table-row";
	    $boton="<input id='botomodi' class='botones_mediano' type='button' value='Modificar Metadato' onclick=\"replaceVista(1);\">";
	}
	$selarr=array('N'=>'N&uacute;merico','C(50)'=>'Texto','C(250)'=>'Texto Largo');
	$sel_tipo="<select id='esta' class='select'>
                <option value='N'>N&uacute;merico</option>
                <option value='C(50)'>Texto</option>
                <option value='C(250)'>Texto Largo</option>
            </select>";
        ?>
	<input id='id_meta' type='hidden' value='<?php echo $id?>'>
        <table cellspacing=0 cellpadding=0 align='center' class='borde_tab' width='800px'>
            <tr style='display:<?php echo $r1?>' id='t1'><td>
                <table cellspacing=2 cellpadding=0 width='100%'>
                    <tr>
                        <td class='titulos4' colspan=2>Metadato <?php echo $info['radi_data_nomb']?><input type='hidden' id='meta_nomb' value='<?php echo $info['radi_data_nomb']?>'></td>
                    </tr>
                    <tr>
                        <td class='titulos2'>Estado de metadato</td>
                        <td class='listado2'><div id='div_estado'><?php echo crearCombo('meta_estado',$info['radi_data_estado'],$est,"modificaInfo(this.value,'estado','$id');")?></div></td>
                    </tr>
                    <tr>
                        <td class='titulos2'>B&uacute;squeda de metadato</td>
                        <td class='listado2'><div id='div_busq'><?php echo crearCombo('meta_busq',$info['radi_data_busq'],$est,"modificaInfo(this.value,'busq','$id');")?></div></td>
                    </tr>
		    <tr><td class='info' colspan=2>Seleccionar Inactivo o Activo para el Estado y/o Búsqueda del Metadato</td></tr>
                </table>
            </td></tr>
            <tr style='display:<?php echo $r2?>' id='t2'><td>
                <table width='100%' cellspacing=2 cellpadding=0>
                    <tr>
                        <td class='cosa' colspan=7>Tags de metadato</td>
                    </tr>
		    <tr>
			<td class='titulo1' colspan=7><div id='variable'>Datos de Tag a Agregar</div></td>
		    </tr>
		    <tr class='titulos2'>
                        <td>Nombre de Tag</td>
                        <td>Etiqueta</td>
                        <td>Uso de campo</td>
			<td>Tipo de dato</td>
                        <td>Visible para busqueda</td>
                        <td>Estado</td>
                        <td>Acci&oacute;n</td>
                    </tr>
		    <tr class='listado2'>
			<td><input type='text' id='nomb'></td>
			<td><input type='text' id='desc'></td>
			<td><?php echo crearCombo('oblig',1,array('No obligatorio','Obligatorio'))?></td>
			<td><?php echo $sel_tipo?></td>
			<td><?php echo crearCombo('busq',1,$est)?></td>
			<td><?php echo crearCombo('estado',1,$est)?></td>
			<td><div id='ten'><a href='#' alt='Agregar Tag' onclick="agregarTag();"><img src='<?php echo $ruta_raiz?>/imagenes/add1.png' alt='Agregar Tag' height='24' width='24'></a></div></td>
		    </tr>
		    <tr>
                        <td class='titulo1' colspan=7 align='center'>Listado de tags disponible</td>
                    </tr>
                    <tr class='titulos3'>
                        <td>Nombre de Tag</td>
			<td>Etiqueta</td>
                        <td>Uso de campo</td>
			<td>Tipo de dato</td>
                        <td>Visible para busqueda</td>
                        <td>Estado</td>
                        <td>Acci&oacute;n</td>
                    </tr>
            <?php             for($i=0;$i<$cont;$i++){
	    $click="'{$tags[$i]['radi_cam_id']}','{$tags[$i]['radi_cam_nombre']}','{$tags[$i]['radi_cam_desc']}','{$tags[$i]['radi_cam_oblig']}','{$tags[$i]['radi_cam_tipo']}','{$tags[$i]['radi_cam_busq']}','{$tags[$i]['radi_cam_estado']}'";
            ?>
                    <tr class='listado5'>
                        <td><?php echo $tags[$i]['radi_cam_nombre']?></td>
			<td><?php echo $tags[$i]['radi_cam_desc']?></td>
                        <td><?php echo ($tags[$i]['radi_cam_oblig']==1)?'Obligario':'No obligatorio'?></td>
			<td><?php echo $selarr[$tags[$i]['radi_cam_tipo']]?></td>
			<td><?php echo ($tags[$i]['radi_cam_busq']==1)?'Activo':'Inactivo'?></td>
                        <td><?php echo ($tags[$i]['radi_cam_estado']==1)?'Activo':'Inactivo'?></td>
                        <td><input type='button' value='Editar' class='botones' onclick="modifTag(<?php echo $click?>)"></td>
                    </tr>
            <?php             }
            ?>
		    <tr><td class='info' colspan=7>Seleccione EDITAR en la línea a modificar para editar los campos Etiqueta, Uso de Campo, Visible para la búsqueda y Estado.<br>
En caso requerido se permite agregar otro TAG con el botón más (+) diligenciando antes los campos.</td></tr>
                </table>
            </td></tr>
            <tr><td class='info' align='center'><?php echo $boton?>
            </td></tr>
        </table>
        <?php         break;
    case 'addMetadato':
	include_once $ruta_raiz . '/core/clases/dependencia.php';
	$depe=new dependencia($ruta_raiz);
	$tdepe=$depe->consultarTodo();
	$optionDepe = "<select id='depe_sel' class='select'>\n<option value=0>Aplica a todas las dependencias</option>\n";
    	for ($i = 0; $i < count($tdepe)-1; $i++) {
            $nomDepe = $tdepe[$i]["depe_nomb"];
            $codDepe = $tdepe[$i]["depe_codi"];
            $optionDepe.="<option value='$codDepe'>$codDepe - $nomDepe</option>\n";
    	}
	$optionDepe.="</select>\n";
	$sel_tipo="<select id='esta' class='select'>
		<option value='N'>N&uacute;merico</option>
		<option value='C(50)'>Texto</option>
		<option value='C(250)'>Texto Largo</option>
	    </select>";
        ?>
        <table cellspacing=0 cellpadding=0 align='center' class='borde_tab' width='700px'>
            <tr style='display:table-row' id='t1'><td>
                <table cellspacing=2 cellpadding=0 width='100%'>
                    <tr>
                        <td class='titulos4' colspan=2>Agregar Nuevo Metadato</td>
                    </tr>
                    <tr>
                        <td class='titulos2'>Nombre de metadato</td>
                        <td class='listado2'><input id='nomb_data' type='text'></td>
                    </tr>
                    <tr>
                        <td class='titulos2'>Estado de metadato</td>
                        <td class='listado2'><?php echo crearCombo('meta_estado',1,$est)?></td>
                    </tr>
                    <tr>
                        <td class='titulos2'>B&uacute;squeda de metadato</td>
                        <td class='listado2'><?php echo crearCombo('meta_busq',1,$est)?></td>
                    </tr>
                    <tr>
                        <td class='titulos2'>Tipo de radicacion a aplicar</td>
                        <td class='listado2'><?php echo crearCombo('tipo_rad',2,$tpDescRad)?></td>
                    </tr>
                    <tr>
                        <td class='titulos2'>N&uacute;mero Tags</td>
                        <td class='listado2'><?php echo crearCombo('no_tags',2,array(2=>2,3=>3,4=>4,5=>5,6=>6),'cargaTags(this.value);')?></td>
                    </tr>
		    <tr>
                        <td class='titulos2'>Dependencia Base</td>
                        <td class='listado2'><?php echo $optionDepe?></td>
                    </tr>
                </table>
            </td></tr>
	    <tr><td class='info'>Ingrese el nombre del Metadato, selecciones las opciones de las litas desplegables según el requerimiento.<br>
	Nombre de Tag = a datos. Ingrese uno o más separados por guión al piso (ej. información_clave)<br>
	Descripción = Nombre del campo a digitar la información por parte del funcionario.</td></tr>
            <tr><td><div id='listaTag'>
                <table width='100%' cellspacing=2 cellpadding=0>
                    <tr>
                        <td class='cosa' colspan=5>Listado de tags</td>
                    </tr>
                    <tr class='titulos3'>
                        <td>Nombre de Tag</td>
			<td>Descripci&oacute;n</td>
                        <td>Tipo de Dato</td>
                        <td>Uso de campo</td>
                        <td>Visible para busqueda</td>
                    </tr>
                <?php                 for($i=0;$i<2;$i++){
                ?>
                    <tr class='listado2'>
                        <td><input type='text' id='nomb<?php echo $i?>'></td>
			<td><input type='text' id='desc<?php echo $i?>'></td>
			<td><?php echo str_replace("esta","tipo$i",$sel_tipo)?></td>
			<td><?php echo crearCombo("oblig$i",1,array('No Obligatorio','Obligatorio'))?></td>
			<td><?php echo crearCombo("busq$i",1,$est)?></td>
                    </tr>
                <?php                 }
                ?>
                </table>
		</div>
            </td></tr>
	    <tr><td class='info'></td></tr>
	    <tr><td class='info' align='center'><input class='botones' type='button' value='Agregar' onclick="crearMetadato();"></td></tr>
        </table>
<?php 	break;
    case 'addTag':
	$met->setDataTag($data);
	$met->setNombMD($meta_nomb);
	$met->agregarTag($id);
	$log->setAction('tag_creation');
	$log->setOpera('Creado Tag '.$data['nombre'].' Metadato '.$meta_nomb);
    $log->registroEvento();
	break;
    case 'addTags':
	$sel_tipo="<select id='esta' class='select'>
                <option value='N'>N&uacute;merico</option>
                <option value='C(50)'>Texto</option>
                <option value='C(250)'>Texto Largo</option>
            </select>";
	?>
                <table width='100%' cellspacing=2 cellpadding=0>
                    <tr>
                        <td class='cosa' colspan=5>Listado de tags</td>
                    </tr>
                    <tr class='titulos3'>
                        <td>Nombre de Tag</td>
                        <td>Descripci&oacute;n</td>
                        <td>Tipo de Dato</td>
                        <td>Uso de campo</td>
                        <td>Visible para busqueda</td>
                    </tr>
                <?php                 for($i=0;$i<$cont;$i++){
                ?>
                    <tr class='listado2'>
                        <td><input type='text' id='nomb<?php echo $i?>'></td>
                        <td><input type='text' id='desc<?php echo $i?>'></td>
                        <td><?php echo str_replace("esta","tipo$i",$sel_tipo)?></td>
                        <td><?php echo crearCombo("oblig$i",1,array('No Obligatorio','Obligatorio'))?></td>
                        <td><?php echo crearCombo("busq$i",1,$est)?></td>
                    </tr>
                <?php                 }
                ?>
                </table>
	<?php 	break;
    case 'insertarMetadato':
	$temp1=stripslashes($datatags);
	$temp2=str_replace('"[','[',$temp1);
	$fin=str_replace(']"',']',$temp2);
	$data=json_decode($fin);
	for($i=0;$i<count($data);$i++){
	    $dataTag[$i]['nombre']=$data[$i]->nomb;
	    $dataTag[$i]['tipo']=$data[$i]->tipo;
	    $dataTag[$i]['desc']=$data[$i]->desc;
	    $dataTag[$i]['oblig']=$data[$i]->oblig;
	    $dataTag[$i]['busq']=$data[$i]->busq;
	}
	$met->setNombMD($nomb_data);
	$met->setTipoRad($trad);
	$met->setDepeCodi($depe_sel);
	$met->setDataTag($dataTag);
	$err=$met->crearMetadatos();
	if($err==1){
	    echo "Metadato fallo creación";
	}
	else{
	    echo "Metadato creado con exito";
	    $log->setAction('tag_value_added');
		$log->setOpera('Creado Metadato '.$nomb_data);
        $log->registroEvento();
	}
	break;
    case 'modificarMeta':
	$met->modificarMetadatos($id,$columna,$val);
	echo crearCombo('meta_'.$columna,$val,$est,"modificaInfo(this.value,'$columna','$id');");
	$log->setAction('tag_value_modification');
	$log->setOpera('Modificado Metadatos de Id '.$id);
    $log->registroEvento();
	echo "Modificado con exito";
	break;
    case 'cambiarTag':
	$met->setDataTag($data);
	$met->modificarTag($id);
	$log->setAction('tag_modification');
	$log->setOpera('Modificado tag ID '.$id);
    $log->registroEvento();
	break;
}
function crearCombo($nombre,$escogida,$lista,$change=false){
    if($change){
        $retorno="<select class='select' id='$nombre' onchange=\"$change\">\n";
    }
    else{
        $retorno="<select class='select' id='$nombre'>\n";
    }
    foreach($lista as $key=>$value){
        if($escogida==$key){
            $retorno.="<option value=$key selected>$value</option>\n";
        }
        else{
            $retorno.="<option value=$key>$value</option>\n";
        }
    }
    $retorno.="</select>\n";
    return $retorno;
}
?>
