<?php 
date_default_timezone_set('America/Bogota');session_start(); 
foreach ($_GET as $key => $valor)   ${$key} = $valor;
foreach ($_POST as $key => $valor)   ${$key} = $valor;
$krd = $_SESSION["krd"];
$dependencia = $_SESSION["dependencia"];
$usua_doc = $_SESSION["usua_doc"];
$codusuario = $_SESSION["codusuario"];
$id_rol = $_SESSION["id_rol"];
$ruta_raiz="../../../..";
include($ruta_raiz.'/core/vista/validadte.php');
//include_once $ruta_raiz.'/core/language/'.$_SESSION ["lang"].'/data.inc';
include_once $ruta_raiz.'/core/clases/dependencia.php';
$scripturl= $ruta_raiz.'/core/Modulos/radicacion/vista/operTx.php';
//inicializa  dependecias
$depe= new dependencia($ruta_raiz);
//echo 1;
$dependecias=$depe->consultarTodo();
$numndep=count($dependecias);
//include_once $ruta_raiz.'/core/language/'.$_SESSION ["lang"].'/data.inc';
$optionDepe="<option value='0'>Todas Las dependencias</option>";
for ($i = 0; $i < $numndep; $i++) {
	$nomDepe  =$dependecias[$i]["depe_nomb"];
	$codDepe   =$dependecias[$i]["depe_codi"];
	if($codDepe!='999'){
		$optionDepe.="<option value='$codDepe'>$codDepe - $nomDepe</option>";				
	}	
 }
?>

<html>
<head>
	<link rel="stylesheet" type="text/css" 	href="<?php echo $ruta_raiz?>/js/calendario/calendar.css">
	<link rel="stylesheet" href="../../../../estilos/default/orfeo.css">
	<script language="JavaScript" src="<?php echo $ruta_raiz?>/js/common.js"></script>
	<script language="JavaScript" type="text/javascript"	src="<?php echo $ruta_raiz?>/js/calendario/calendar_eu.js"></script>
	<script type="text/javascript">
		function buscar(){
			/**
 			* Modificacion para validar caracteres
 			* @autor Luis Jaime Benavides P. 
 			* @fecha 2011/11
 			*/
			var rad = document.getElementById('numRad').value;
			var poststr = "action=buscarDesanular&numrad="+rad;
			//alert("Funcion Buscar Desanular " + " " + poststr);
			var caract_extra= '';
			var ubicacion;
			var enter = "\n";
			var caracteres = "1234567890," + String.fromCharCode(13) + enter + caract_extra
			var contador = 0;
			if(rad.length< 14 ){
				alert('Debe Colocar un numero de radicado');
				return false;
			}
			else {
				for (var i=0; i < rad.length; i++) {
					ubicacion = rad.substring(i, i + 1)
					//alert("[" + caracteres.indexOf(ubicacion)+"]");
					if (caracteres.indexOf(ubicacion) != -1) {
						contador++
					} 
					else {
						alert("El campo debe ser numerico no se acepta el caracter: '" + ubicacion + "'.")
						return false
					}
				}	// for
				partes('<?php  echo $scripturl; ?>','listados',poststr,'');
			} //else
		}
	
		function desAnula(radi){
			var poststr = "action=desanular&numrad="+radi;
			//alert(poststr); 
			partes('<?php  echo $scripturl; ?>','desanu',poststr,'');
			vistaFormUnitid('desanu',1);
			vistaFormUnitid('respx',2);
			vistaFormUnitid('respOpera',2);
		}
		function cancelar(){
			vistaFormUnitid('desarch',2);
			vistaFormUnitid('respx',1);
			vistaFormUnitid('respOpera',2);
		}
		function realizarTx(numrad,depeante){
			var motivo = document.getElementById('motivo').value;
			if(motivo=='0'){
				alert('Debe Selecionar un motivo');
				return false;
			}
			var descpmotivo = document.getElementById('descpmotivo').value;
			if(descpmotivo.length< 10 ){
				alert('Debe Colocar una observacion con un minimo de 10 caracteres');
				return false;
			}
			if(confirm("Esta seguro de desarchivar el radicado "+numrad+"? ")) {
				var motivo2=motivo+" * Comentario  "+descpmotivo;
				var poststr = "action=realizardeanula&numrad="+numrad+"&motivo="+motivo2+"&depeante="+depeante;
				//alert(poststr); 
				partes('<?php  echo $scripturl; ?>','respOpera',poststr,'');
				vistaFormUnitid('desanu',2);
				//vistaFormUnitid('respx',2);
				vistaFormUnitid('respOpera',1);
			}
			else {
				return false;
			}
		}
		function entsub(event,ourform) {
			if (event && event.which == 13){
				buscar();
			}
			else {
				return false;
		}
		}
	</script>
</head>
<body bgcolor="#FFFFFF">
	<table class=borde_tab width='100%' cellspacing="0">
		<tr>
			<td  align='center' class="titulos4">B&uacute;squeda de Radicados a Desanular</td>
		</tr>
	</table>
	<form method="post" action="#" name="busquedaExp"  style="margin:0"> 
		<center>
		<table width="100%" border="0" cellspacing="0" cellpadding="0" style='border-collapse: collapse;'>
		  <!-- <tr><td height="5">&nbsp;</td></tr> -->
		  <tr>
		  	<td width="5">&nbsp;</td>
		    <td valign="top" width="500">
		   		<table width="500"  cellspacing="2" class="borde_tab">
						<tr class='titulo1'>
			 				<td colspan='8' >Datos de B&uacute;squeda</td>
			 			</tr>
		      	<tr>
		        	<td width="75" height="26"  class='titulos2'> Radicado </td>
		        	<td width="125" class='titulos2'>
		         		<textarea type=text id="numRad" name='numRad' class='select' rows="1" cols="32" ></textarea>
		         	</td>    
		          <td width="100" colspan="2">
		          	<div id='busIns' align="left" style="background-color:#a8bac6">
		          		<!-- <center> -->
		            	<input type="button" name='buscar_serie2' Value='Buscar' onclick='buscar();' class='botones'>
		            	<input type="reset"  name='aceptar2' class='botones' id='aceptar' onClick="vistaFormUnitid('busIns',1);vistaFormUnitid('busModi',2);document.UsuarioOrfeo.login.disabled = false;" value='Limpiar'>
		        		</div>
		            <div id='busModi'></div>
		          </td>
						</tr>
			    </table>
			  </td>
				<td>
					<table cellspacing="0" class="borde_tab" height="36">
						<tr class='titulo1'>
			 				<td colspan='8' >Datos de Usuario</td>
						</tr>
		      	<tr>
					  	<td width="75"  class='titulos2'  colspan="2">Usuario</td>
		      	  <td class='titulos2' colspan="2">
		         		<?php  echo $_SESSION['usua_nomb'];?>
		        	</td>
				     	<td width="75" class='titulos2'  colspan="2">Dependencia</td>
		          <td class='titulos2' colspan="2">
		         		<?php  echo $_SESSION['depe_nomb'];?>
		        	</td>
				 		</tr>
		    	</table>
		    </td>
				<td width="5">&nbsp;</td>
		  </tr>
		  <tr>
		  	<td width="5">&nbsp;</td>
		   	<td valign="top" colspan='2'>
		  		<table width="100%" cellpadding="0" cellspacing="0" class="borde_tab">
		   			<tr class='titulo1'>
			 				<td colspan='2'>Resultado  de la B&uacute;squeda</td>
			 			</tr>
				   	<tr>
				   		<td>
		    				<div id="listados" style="vertical-align : top; padding : 0px; width : 100%; height : 400px; overflow : auto; "  class='listado2' >Solo radicados que pertenecieron a la dependencia  actual cuando  fueron solicitados la anulaci&oacute;n.</div>
		    			</td>
		    		</tr>
		    	</table>
		    </td>
				<td width="5"></td>
		  </tr>
		</table>
		<script language="javascript">
		//vistaFormUnitid('busModi',2);
		//partes('<?php  echo $scripturl ?>','listados','accion=listado','');
		//target='mainFrame' 
		</script>
	</form>
	<form  action="depuracion.php" name='formdepu' id='formdepu' method="post">
		<input type="hidden" id='passRad' name='passRad'>
	  <input type="hidden" id='passExp' name='passExp'>
	  <!--  <input type="submit" value="Depurar">-->
	</form>
</body>
</html>