<?php //ini_set('display_errors','On');
//error_reporting(-1);
session_start();
date_default_timezone_set('America/Bogota');
//error_reporting(E_ALL);
//ini_set('display_errors', 'On');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
foreach ($_GET as $key => $valor)
    $$key = $valor;
foreach ($_POST as $key => $valor)
    $$key = $valor;
foreach ($_SESSION as $key => $valor)
    $$key = $valor;
$ruta_raiz = '../../../..';
$json_res=json_decode($respuesta);
$numrad=substr($json_res->nombre,0,14);
include_once "$ruta_raiz/core/Modulos/radicacion/clases/radicado.php";
$rad= new radicado($ruta_raiz);
include_once "$ruta_raiz/core/Modulos/radicacion/clases/tx.php";
$tx=new tx($ruta_raiz);
if(isset($_SERVER['HTTP_X_FORWARD_FOR'])){
    $proxy=$_SERVER['HTTP_X_FORWARD_FOR'];
}else
    $proxy=$_SERVER['REMOTE_ADDR'];
$REMOTE_ADDR=$_SERVER['REMOTE_ADDR'];
include_once "$ruta_raiz/core/clases/log.php";
$log=new log($ruta_raiz);
$log->setRolId($id_rol);
$log->setUsuaCodi($codusuario);
$log->setDepeCodi($dependencia);
$log->setDenomDoc('Radicado');
$log->setProxyAd($proxy);
$log->setAddrC($REMOTE_ADDR);
echo "<div id='response'>";
if(substr($json_res->nombre,-3)=='pdf'){
    $rad->setRadiNumeRadi($numrad);
    $rad->setPathAnexo($json_res->localizacion.$json_res->nombre);
    $resultado=$rad->genDocumentoAccion();
    $i=0;
    if($resultado['error']==""){
        $trad=$resultado['trad'];
        $descrad=$resultado['desc'];
        $obse="Generado documento $numrad";
        if($resultado['radi_padre']!=$numrad){
	    $rads[$i]=$resultado['radi_padre'];
	    $i++;
	    $obse.=" Radicado Padre {$resultado['radi_padre']}";
        }
        //Tipo de transaccion validando cuando ya hizo el documento
    	if($resultado['anex_estado']==0){
	    $codTx=72;
    	}
    	else{
	    $codTx=73;
    	}
    	$obse.=" Anexo {$resultado['anex_codigo']}";
    	$rads[$i]=$numrad;
    	$tx->insertaHistorico($rads,$dependencia,$dependencia,$usua_doc,$usua_doc,$codusuario,$codusuario,$id_rol,$id_rol,$obse,$codTx);
    	$log->setNumDocu($numrad);
    	$log->setAction('document_generated_from_online_templates_module');
        $log->setOpera('Documento Elaborado por Generador de documentos');
        $log->registroEvento();
    	echo "Generado documento";
	$rad->setRadiNumeRadi($resultado['radi_padre']);
	$retorno=$rad->datosradCarpeta();
	if($retorno['carp_per']==0){
	    $temp="&tipo_carp=&carpeta={$retorno['carp_codi']}";
	}
	else{
	    $temp="&tipo_carp=1&carpeta={$retorno['carp_codi']}";
	}
	$regreso="./old/verradicado.php?verrad=".$resultado['radi_padre']."&".session_name()."=".session_id()."&adodb_next_page=1&depeBuscada=&filtroSelect=&tpAnulacion=".$temp."&nomcarpeta=".$retorno['carpeta_nombre']."&agendado=&orderTipo=DESC&orderNo=16&menu_ver_tmp=2";
    }
    else{
        echo $resultado['error'];
    }
}
else{
    include_once "$ruta_raiz/core/Modulos/masiva/clases/masiva.php";
    $masiva=new masiva($ruta_raiz);
    $masiva->setGrupoRadi($numrad);
    $grupomas=$masiva->grupoMasiva();
    $local=$json_res->localizacion;
    $error=0;
    for($i=0;$i<count($grupomas);$i++){
	$rads[$i]=$grupomas[$i]['numrad'];
	$rad->setPathAnexo($local.$grupomas[$i]['numrad'].'.pdf');
	$rad->setRadiNumeRadi($grupomas[$i]['numrad']);
	$resultado=$rad->genDocumentoAccion();
	$anex_estado=$resultado['anex_estado'];
	if($resultado['error']!=""){
	    $error=1;
	    $msg=$resultado['error'];
	}
    }
    if($error==0){
	if($anex_estado==0){
	    $codTx=72;
	}
	else{
	    $codTx=73;
	}
	$obse="Generado documentos grupo $numrad";
	$tx->insertaHistorico($rads,$dependencia,$dependencia,$usua_doc,$usua_doc,$codusuario,$codusuario,$id_rol,$id_rol,$obse,$codTx);
	echo "Generados documentos para masiva";
	$log->setNumDocu($rads);
	    $log->setAction('document_generated_from_online_templates_module');
        $log->setOpera('Documento Elaborado por Generador de documentos');
        $log->registroEvento();
    }
    else{
	echo $msg;
    }
    $regreso="./old/radsalida/masiva/menu_masiva.php";
}
echo "</div>";
echo "<div id='regreso'>$regreso</div>";
?>
