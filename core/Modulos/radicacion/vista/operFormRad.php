<?php session_start(); date_default_timezone_set('America/Bogota'); date_default_timezone_set('America/Bogota');
foreach ($_GET as $key => $valor)  $$key = $valor;
foreach ($_POST as $key => $valor)  $$key = $valor;
$krd = $_SESSION["krd"];
$dependencia = $_SESSION["dependencia"];
$usua_doc = $_SESSION["usua_doc"];
$codusuario = $_SESSION["codusuario"];
$id_rol = $_SESSION["id_rol"];
$tpDepeRad=$_SESSION['tpDepeRad'];
$ruta_raiz="../../../..";
include_once "$ruta_raiz/core/Modulos/radicacion/clases/radicado.php";
$rad= new radicado($ruta_raiz);
include_once "$ruta_raiz/core/Modulos/radicacion/clases/tx.php";
$tx= new tx($ruta_raiz);
//Cargar variables del log
if(isset($_SERVER['HTTP_X_FORWARD_FOR'])){
    $proxy=$_SERVER['HTTP_X_FORWARD_FOR'];
}else
    $proxy=$_SERVER['REMOTE_ADDR'];
$REMOTE_ADDR=$_SERVER['REMOTE_ADDR'];
include_once "$ruta_raiz/core/clases/log.php";
$log=new log($ruta_raiz);
$log->setRolId($id_rol);
$log->setUsuaCodi($codusuario);
$log->setDepeCodi($dependencia);
$log->setDenomDoc('Radicado');
$log->setProxyAd($proxy);
$log->setAddrC($REMOTE_ADDR);
//print_r($_POST);
switch ($action){
    case 'comboPais':
	echo "<select id='comboPais' onchange='cambioDpto(this.value)' class='select'>
		<option value=0>--Selecciones un Pais--</option>";
        if($continente!=0){
	    if(!isset($sel)){
		$sel=0;
	    }
            $resultado=$rad->listarPaises($continente);
            if($resultado['error']==""){
                $cont=count($resultado)-1;
                for($i=0;$i<$cont;$i++){
		     $id=$resultado[$i]['id'];
		     if($sel==$id){
			$att=" selected";
		     }
		     else{
			$att="";
		     }
                    echo "<option value=$id$att>{$resultado[$i]['pais']}</option>\n";
                }
            }
        }
        echo "</select>\n";
	break;
    case 'comboDpto':
        echo "<select id='comboDpto' onchange='cambioMuni(this.value)' class='select'>
		<option value=0>--Selecciones un Departamento--</option>";
        if($pais2!=0){
	    if(!isset($sel)){
                $sel=0;
            }
            $resultado=$rad->listarDpto($pais2);
	    print_r($resultado);
            if($resultado['error']==""){
                $cont=count($resultado)-1;
                for($i=0;$i<$cont;$i++){
		    $id=$resultado[$i]['id'];
                    if($sel==$id){
                        $att=" selected";
                    }
                    else{
                        $att="";
                    }
                    echo "<option value=$id$att>{$resultado[$i]['dpto']}</option>\n";
                }
            }
        }
        echo "</select>\n";
	break;
    case 'comboMuni':
	echo "<select id='comboMuni' class='select'>
                <option value=0>--Selecciones un Municipio--</option>";
        if($pais2!=0 && $dpto!=0){
	    if(!isset($sel)){
                $sel=0;
            }
            $resultado=$rad->listarMunicipio($pais2,$dpto);
            if($resultado['error']==""){
                $cont=count($resultado)-1;
                for($i=0;$i<$cont;$i++){
		    $id=$resultado[$i]['id'];
                    if($sel==$id){
                        $att=" selected";
                    }
                    else{
                        $att="";
                    }
                    echo "<option value=$id$att>{$resultado[$i]['muni']}</option>\n";
                }
            }
        }
        echo "</select>\n";
        break;
    case 'busqRad':
        echo "<center>";
	$resultado=$rad->consultarRad($norad,$fecha_ini,$fecha_fin);
	echo $resultado;
        echo "</center>";
	break;
    case 'cargarTRad':
        ?>
	<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center" class="BORDE_TAB">
            <tr align="center" >
        <td class="titulos4">
        <center>RADICACI&Oacute;N DE DOCUMENTOS DE <b><?php echo strtoupper($descrad)?></b> <?php echo $dependencia?>-&#62;<?php echo $tpDepeRad[$trad]?><input type="hidden" id="tpDepeRad" value="<?php echo $tpDepeRad[$trad]?>"></center></td></tr>
        </table>
	<?php 	break;
    case 'busqDoc':
        echo "<center>";
	if($trad!=3){
	    $resultado=$rad->consultarDoc($nodoc,$fecha_ini,$fecha_fin);
	    if(substr($resultado,0,2)!='No'){
            	echo $resultado;
	    }
	    else{
	    ?>
	    <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center" class="BORDE_TAB">
            	<tr align="center" >
        	<td class="titulos2">
        	<center><?php echo strtoupper($resultado)?></center></td></tr>
		<tr align="center" >
                <?php                 if($_SESSION['entidad_add']==1){
                ?>
        	<td class="listado2"><center><input type='button' class='botones' value='CREAR NUEVO' onclick='crearTercero()'></center></td></tr>
                <?php                 }
                else{
                ?>
            <td class="listado2"><center><span style="color:red"><b>SI DESEA CREAR UN TERCERO, FAVOR SOLICITAR EL PERMISO CON EL ADMINISTRADOR DEL SISTEMA.</b></span></center></td></tr>
                <?php                 }
                ?>
            </table>
	    <?php 	    }
	}
	else{
	    $resultado=$rad->consultarDocFun($nodoc,$fecha_ini,$fecha_fin);
	    if(substr($resultado,0,2)!='No'){
                echo $resultado;
            }
            else{
        ?>
        <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center" class="BORDE_TAB">
            <tr align="center" >
                <td class="titulos2">
                <center><?php echo strtoupper($resultado)?>. FUNCIONARIO NO EXISTE EN LA BASE DE DATOS</center></td></tr>
        </table>
        <?php 	    }
	}
        echo "</center>";
        break;
    case 'formTercero':
	list($comboCont,$comboPais,$comboDpto,$comboMuni)=$rad->constCombo(1,169,11,1);
	?>
 	    <table width="90%" cellspacing=1 cellpadding=0 align='center' style="border-collapse:collapse;border: 0.5px solid grey;">
                <tr><td class='titulo1' colspan='7'>Agregando nuevo tercero</td></tr>
                <tr id='rowciu' class='titulos31' align='center' style='display:table-row'>
                    <td>DOCUMENTO</td>
                    <td>NOMBRE</td>
                    <td>PRIMER<br>APELLIDO</td>
                    <td>SEGUNDO<br>APELLIDO</td>
                    <td>DIRECCI&Oacute;N</td>
                    <td>TEL&Eacute;FONO</td>
                    <td>EMAIL</td>
                </tr>
                <tr id='rowoem' class='titulos31' align='center' style='display:none'>
                    <td>NIT</td>
                    <td>RAZ&Oacute;N SOCIAL</td>
                    <td>SIGLA</td>
                    <td>REPRESENTANTE<br>LEGAL</td>
                    <td>DIRECCI&Oacute;N</td>
                    <td>TEL&Eacute;FONO</td>
                    <td>EMAIL</td>
                </tr>
                <tr class='listado5'>
                    <td><input type='text' id='param1' value='<?php echo $nodoc?>' onkeypress="return isNumberKey(event);"></td>
                    <td><input type='text' id='param2'></td>
                    <td><input type='text' id='param3'></td>
                    <td><input type='text' id='param4'></td>
                    <td><input type='text' id='param5'></td>
                    <td><input type='text' id='param6'></td>
                    <td><input type='text' id='param7'></td>
                </tr>
		<tr align='center'>
                    <td class='titulos31'>CONTINENTE</td>
                    <td class='titulos31'>PAIS</td>
                    <td class='titulos31'>DEPARTAMENTO</td>
                    <td class='titulos31'>MUNICIPIO</td>
                    <td colspan=3 rowspan=2 class='grisCCCCCC'>
                        <select id="entrada" class='select' onchange="cambioTercero()"><option value=0>Usuario</option><option value=1>Entidad</option></select>
                        <input type="button" class='botones_largo' value='AGREGAR Y USAR DATOS' onclick="validarTercero()">
                        <input class='botones' type='button' value='Devolver' onclick="devolver2()">
                    </td>
                <tr class='listado5'>
                    <td><?php echo $comboCont?></td>
                    <td><div id='pais'><?php echo $comboPais?></div></td>
                    <td><div id='dpto'><?php echo $comboDpto?></div></td>
                    <td><div id='muni'><?php echo $comboMuni?></div></td>
                </tr>
		<tr class='info'>
		    <td colspan='7'><b>ATENCIÓN:</b> El formulario de registro de datos de radicación lo ha redirigido a esta ventana.
		La creación de un tercero (entidad o ciudadano) está activado y solo se digitara los datos del
		tercero en caso que no exista en el listado de selección al iniciar la radicación del documento.
		Verifique nuevamente el nombre y/o número de identificación.
		Al cerciorarse que el perfil del tercero no existe, continúe digitando los datos completos mínimo el
		nombre o razón social, la dirección y la localización geográfica del misma.</td>
		</tr>
            </table>
	<?php 	break;
    case 'busqNombre':
        echo "<center>";
	if($trad!=3){
	    $resultado=$rad->consultarNombre($parametro,$fecha_ini,$fecha_fin);
	    if(substr($resultado,0,2)!='No'){
                echo $resultado;
            }
            else{
            ?>
            <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center" class="BORDE_TAB">
            <tr align="center" >
            	<td class="titulos2">
            	<center><?php echo strtoupper($resultado)?></center></td></tr>
            <tr align="center" >
            <?php                 if($_SESSION['entidad_add']==1){
                ?>
        	<td class="listado2"><center><input type='button' class='botones' value='CREAR NUEVO' onclick='crearTercero()'></center></td></tr>
                <?php                 }
                else{
                ?>
            <td class="listado2"><center><span style="color:red;"><b>SI DESEA CREAR UN TERCERO, FAVOR SOLICITAR EL PERMISO CON EL ADMINISTRADOR DEL SISTEMA.</b></span></center></td></tr>
                <?php                 }
                ?>
            </table>
            <?php             }
	}
	else{
	    $resultado=$rad->consultarFuncionario($parametro,$fecha_ini,$fecha_fin);
	    if(substr($resultado,0,2)!='No'){
                echo $resultado;
            }
            else{
	?>
	<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center" class="BORDE_TAB">
            <tr align="center" >
                <td class="titulos2">
                <center><?php echo strtoupper($resultado)?></center></td></tr>
        </table>
	<?php 	    }
	}
        echo "</center>";
        break;
    case 'crearTercero':
	$id=$rad->crearTercero($tipo,$param1,$param2,$param3,$param4,$param5,$param6,$param7,$conti,$pais2,$dpto,$muni);
	$datosNew=$rad->traerDatosNuevo($tipo,$id);
	//print_r($datosNew);
	//break;
    case 'radicarNuevo':
	if(isset($codi)){
	     $datosNew=$rad->traerDatosNuevo($tipo,$codi);
	}
    case 'relacionarRad':
	include_once "$ruta_raiz/core/Modulos/radicacion/clases/metadatos.php";
	$met=new metadatos($ruta_raiz);
	if(isset($numrad)){
	    $datosNew=$rad->traerDatosRad($numrad);
	}
	//print_r($datosNew);
	foreach($datosNew as $key =>$value)
		$$key=$value;
	?>
	<form name='formulario2' id='formulario2' method='post'>
	<input type='hidden' id='id_meta' value=0>
	<table  width=90% border="0" align="center" cellspacing="1" cellpadding="1" class="borde_tab" >
            <tr valign="middle">
                <td class="titulos5" width="15%" align="right">
                    <font color="" face="Arial, Helvetica, sans-serif">
                    <span class="titulos5">Fecha Radicaci&oacute;n:<br>dd/mm/aaaa</span></font>
                </td>
                <td class="listado50" width="15%"><font color="" face="Arial, Helvetica, sans-serif">
                    <input name="fechproc1" type="text"  readonly="true" id="fechproc13" size="2" maxlength="2" value="<?php echo date('d')?>" class="tex_area">/
                    <input name="fechproc2" type="text"  readonly="true" id="fechproc23" size="2" maxlength="2" value="<?php echo date('m')?>" class="tex_area">/
                    <input name="fechproc3" readonly="true" type="text" id="fechproc33" size="4" maxlength="4" value="<?php echo date('Y')?>" class="tex_area">
                    </font>
        	</td>
                <td width="15%" class="titulos5" align="right"><font color="" face="Arial, Helvetica, sans-serif" class="titulos5">Fecha Documento:<br> dd/mm/aaaa </font>
        	</td>
                <td width="15%" class="listado50"><font color="" face="Arial, Helvetica, sans-serif">
		    <input readonly="true" type="text" class="tex_area" name="fecha_ofic" id="fecha_ofic" size="10"/>
                </td>
                <td width="15%" class="titulos5" align="right">Cuenta Interna, Oficio, Referencia</td>
                <td width="15%" class="listado50">
                    <font face="Arial, Helvetica, sans-serif">
                    <input name="cuentai" type="text"  maxlength="20" class="tex_area" value='' id='cuentai'>
                    </font>
        	</td>
                <td width="15%" class="titulos5" align="right">Guia</td>
                <td width="15%" class="listado50">
            	    <input type="text" name="guia" id='guia' value='' class="tex_area" size="35">
                </td>
            </tr>
	</table>
	<table width="90%" align="center" border="0" cellspacing="0" cellpadding="0">
            <tr valign="bottom">
            	<td height="10">
            	    <table width="99%" align="center" border="0" cellspacing="0" cellpadding="0">
                    	<tr>
                    	<?php                     	if($trad!=2){
			    $img_remitente = "destinatario";
			    $dimen='50%';
			    $serCombo="<td class=\"listado50\" align='right' width='50%'><b>Aplicar TRD</b><div id=\"serdoc\">".$tx->comboSeries($dependencia)."</div></td>";
			    $subserCombo="<td class='listado50' align='right' width='50%'><div id='subsdoc'><select id='subserdocf' onchange='return comboTipodoc(this.value);' class='select'>\n<option value=0>Seleccione SubSerie</option></div></td>";
			    $tdocCombo="<td class='listado50' align='right' width='50%'><div id='tpdoc'><select id='tdocf' class='select'>\n<option value=0>Seleccione Tipo Documento</option></select></div></td>";
			}
			else{
			    $img_remitente = "remitente";
			    $dimen='100%';
			    $serCombo="";
                            $subserCombo="";
                            $tdocCombo="";
			}
			$met->setTipoRad($trad);
			$met->setDepeCodi($dependencia);
			$mtdata=$met->existeMetadatos();
			if($mtdata==0){
			    $otro_form="none";
			}
			else{
			    $otro_form="table-row";
			}
                    	?>
                            <td width="523"  valign="bottom" >
			     <?php
			     if(!isset($tipoDeri)){
				$tipoDeri=0;
				$numrad=0;
				$cambio=" disabled";
				$vis="none";
			     }
			     else{
				$cambio="";
                                $vis="block";
			     }
			     ?>
			    <input type='hidden' id='tipoDeri' value=<?php echo $tipoDeri?>>
			    <input type='hidden' id='radiDeri' value=<?php echo $numrad?>>
        		    </td>
                    	<?php                 	if($trad!=2) $busq_salida="true"; else  $busq_salida=""; ?>
                        </tr>
                    </table>
            	</td>
            </tr>
            <tr valign="top">
            <td class="titulos5">
		<div class="tabs" id="tabs" style="padding: 0px">
                    <ul>
                        <li><a href="#tab-1"><?php echo   strtoupper($img_remitente)?></a></li>
                    </ul>
	    	    <div id="tab-1" style="padding: 0px">
			<table width=100%   class="t_bordeGris" align="center" cellpadding="0" cellspacing="0">
			    <tr class=listado2>
        			<td class="titulos50"  align="right">Documento</td>
        			<td bgcolor="#FFFFFF"  class="listado50">
                		    <input type=text  name='cc_documento' id='param1' value='<?php echo $param1?>' readonly="true" class="tex_area">
        			    <input type=text name='documento_us' id='tdid_codi' value='<?php echo $tdid_codi?>' readonly="true" class="tex_area" size="1">
        		        </td>
        			<td class="titulos5"  align="right"><font face="Arial, Helvetica, sans-serif" class="etextomenu">Tipo</font></td>
        			<td width="33%"  bgcolor="#FFFFFF" class="listado50">
				    <select id='tipo' class="select" onchange="overWindow('addTercero.html');" <?php echo $cambio?>>
				    <?php
			            if($tipo==0){
					echo "<option value=0 selected='selected'>USUARIO</option>
					      <option value=1>ENTIDAD</option>
                                              <option value=2>FUNCIONARIO</option>\n";
					$hide="<input type='hidden' id='oem' value=0>
						<input type='hidden' id='ciu' value=$id>
						<input type='hidden' id='docfun' value=''>\n";
				        $lbl_nombre="Nombre";
					$lbl_apellido="Primer Apellido";
					$lbl_nombre2="Segundo Apellido";
				    }elseif($tipo==1){
					echo "<option value=0>USUARIO</option>
                                              <option value=1 selected='selected'>ENTIDAD</option>
                                              <option value=2>FUNCIONARIO</option>\n";
					 $hide="<input type='hidden' id='oem' value=$id>
                                                <input type='hidden' id='ciu' value=0>
						<input type='hidden' id='docfun' value=''>\n";
					$lbl_nombre="Raz&oacute;n social";
					$lbl_apellido="Sigla";
					$lbl_nombre2="Representante Legal";
				    }else{
					echo "<option value=0>USUARIO</option>
                                              <option value=1>ENTIDAD</option>
                                              <option value=2 selected='selected'>FUNCIONARIO</option>\n";
				        $hide="<input type='hidden' id='oem' value=0>
                                               <input type='hidden' id='ciu' value=0>
					       <input type='hidden' id='docfun' value='$id'>\n";
					$lbl_nombre="Nombre";
                                        $lbl_apellido="Primer Apellido";
                                        $lbl_nombre2="Segundo Apellido";
				    }
				    ?>
				    </select>
                                    &nbsp;&nbsp;<input id='cambiar' class="botones" type="button" value="Cambiar" onclick="overWindow('addTercero.html');" style="display:<?php echo $vis?>;"></td><td>
                                    <input id='devolverRad' class="botones" type="button" value="Devolver" onclick="devolver2()" style="display:block;"></td>
				    <?php echo $hide?>
				</td>
			    </tr>
			    <tr class=e_tablas>
        			<td width="26%" class="titulos5" align="right"> <font face="Arial, Helvetica, sans-serif" class="etextomenu"><?php echo $lbl_nombre ?>
                		</font></td>
        			<td width="22%" bgcolor="#FFFFFF" class="listado50">
                		<INPUT type=text name='param2' id='param2' value='<?php echo $param2?>'  readonly="true"  class="tex_area" size=40>
        			</td>
        			<td width="8%" class="titulos5" align="right">
				    <font face="Arial, Helvetica, sans-serif" class="etextomenu">
 				    <?php echo $lbl_apellido ?></font></td>
        			<td colspan="3" bgcolor="#FFFFFF" class="listado50">
				    <INPUT type=text name='param3' id='param3' value='<?php echo $param3?>' class="tex_area"  readonly="true"  size="35">
				</td>
        		    </tr>
        		    <tr class=e_tablas>
                		<td width="26%" class="titulos5"  align="right"><font face="Arial, Helvetica, sans-serif" class="etextomenu"><?php echo $lbl_nombre2 ?></font></td>
                		<td width="22%" bgcolor="#FFFFFF" class="listado50">
                		<input type=text name='param4'  id='param4' value='<?php echo $param4 ?>'  readonly="true"  class="tex_area" size=40>
        			</td>
 	 			<td width="8%" class="titulos5"  align="right"><font face="Arial, Helvetica, sans-serif" class="etextomenu">Tel&eacute;fono
		                </font></td>
                		<td  colspan="3" bgcolor="#FFFFFF"  class="listado50">
                		<input type=text  name='param6' id='param6' value='<?php echo $param6?>' class="tex_area" size=35>
        			</td>
			    </tr>
			    <tr class=e_tablas>
        			<td width="26%" class="titulos5"  align="right"><font face="Arial, Helvetica, sans-serif" class="etextomenu">Direcci&oacute;n
        				</font> </td>
        			<td width="22%" bgcolor="#FFFFFF"  class="listado50">
                		    <INPUT type=text name='direccion_us' id='param5' value='<?php echo $param5?>' class="tex_area" size=40>
        			</td>
        			<td width="8%" class="titulos5"  align="right"><font face="Arial, Helvetica, sans-serif" class="etextomenu">Mail
                		    </font></td>
        			<td  colspan="3" bgcolor="#FFFFFF"  class="listado50">
                		    <INPUT type=text name='mail_us' id='param7' value='<?php echo $param7?>' class="tex_area" size=35>
        			</td>
			    </tr>
				<tr class=e_tablas>
        			    <td width="26%" class="titulos5"  align="right" ><font face="Arial, Helvetica, sans-serif" class="etextomenu">Dignatario</font></td>
        			    <td bgcolor="#FFFFFF"  class="listado50" colspan="4">
        			    <INPUT type='text' name='param8' id='param8' value="<?php echo (isset($param8))?$param8:''?>" class='tex_area' size='40' maxlength='50'>
        			    </td>
				</tr>
			    <?php 			    list($comboCont,$comboPais,$comboDpto,$comboMuni)=$rad->constCombo($conti,$pais,$dpto,$muni);
			    if(isset($param9)){
				$asu=$param9;
			    }else
				$asu='';
			    ?>
			    <tr class=e_tablas>
        			<td width="26%" class="titulos5"  align="right"><font face="Arial" class="etextomenu">Continente</font></td>
        			<td width="22%" bgcolor="#FFFFFF"  class="listado50">&nbsp;<?php echo $comboCont?></td>
			        <td width="8%" class="titulos5"  align="right"><font face="Arial" class="etextomenu">Pa&iacute;s</font></td>
        			<td  colspan="3" bgcolor="#FFFFFF"  class="listado50">&nbsp;<div id='pais'><?php echo $comboPais?></div></td>
			    </tr>
			    <tr class=e_tablas>
                                <td width="26%" class="titulos5"  align="right"><font face="Arial" class="etextomenu">Departamento</font></td>
                                <td width="22%" bgcolor="#FFFFFF"  class="listado50">&nbsp;<div id='dpto'><?php echo $comboDpto?></div></td>
                                <td width="8%" class="titulos5"  align="right"><font face="Arial" class="etextomenu">Municipio</font></td>
                                <td  colspan="3" bgcolor="#FFFFFF"  class="listado50">&nbsp;<div id='muni'><?php echo $comboMuni?></div></td>
                            </tr>
                            <tr>
                                <td class="titulos5" align="center" colspan="5"><font color="" face="Arial, Helvetica, sans-serif" class="etextomenu">Verifique que los datos cargados al formulario, diligencie los
			campos comprometidos con el radicado que está elaborando.</font></td>
                            </tr>
			    <tr id="rntab" style="display:<?php echo $otro_form?>">
                                <td class="titulos5" align="center" colspan="5"><input type="button" class="botones_mediano2" value="Seleccionar Metadato" onclick="overWindow('formMetadatos.php?trad=<?php echo $trad?>');"></td>
                            </tr>
			</table>
		    </div>
		</div>
		<table width=100% border="0" class="borde_tab" align="center">
        	    <tr>
        		<td  class="titulos5" width="26%" align="right" > <font color="" face="Arial, Helvetica, sans-serif" class="etextomenu">Asunto
                	</font></td>
        		<td width="78%" class="listado50" >
        		    <textarea  name="asu" id="param9" cols="50" class="tex_area" rows="2" onKeyUp="maximo(this,350);" onKeyDown="maximo(this,350);"><?php echo htmlspecialchars(stripcslashes($asu)); ?></textarea>
        	        *maximo 350 caracteres<br>
			<font color="" face="Arial, Helvetica, sans-serif" class="etextomenu"><b>RECUERDE:</b> Se recomienda digitar claro y preciso el concepto del radicado, permitiendo una búsqueda futura precisa y rápida</font>
        		</td>
        	    </tr>
		    <tr>
                	<td  class="titulos5" width="26%" align="right"> <font color="" face="Arial, Helvetica, sans-serif" class="etextomenu">
                	Descripci&oacute;n de Anexos</font>              </td>
                	<td width="78%" class="listado50"><table width='100%'><tr><td class="listado50" align='left' width='<?php echo $dimen?>'><font color="" face="Arial, Helvetica, sans-serif">
                	<input name="ane" id="param10" type="text" size="50" maxlength="100" class="tex_area"><br>* maximo 100 caracteres
                	</font></td><?php echo $serCombo?></tr></table></td>
                    </tr>
	  	    <tr>
        		<td width="26%" class="titulos5" align="right">
                	<font color="" face="Arial, Helvetica, sans-serif" class="etextomenu">
                    <?php                     if($trad==2)
                    {
                        echo "Medio Recepci&oacute;n";
			$comboDep=$rad->depeRec();
			$proce="<input class=\"botones\" type=\"button\" value=\"Nuevo\" onclick=\"return nuevoARadicar();\">";
                    }
                    else
                    {
                        echo "Medio Env&iacute;o";
			$comboDep=$rad->depeDes($dependencia,$id_rol);
			$proce="<input class=\"botones\" type=\"button\" value=\"Continuar\" onclick=\"contFlujo();\">";
                    }
		    ?>
        	    	</font></td>
 		    	<td width="78%" valign="top" class="listado50"><table width='100%'><tr><td class="listado50" align='left' width='<?php echo $dimen?>'><font color="">&nbsp;<?php echo $rad->comboMediorec();?>
			    <input type="hidden" name="tdoc" value='NULL' /></font></td>
				<?php echo $subserCombo?></tr></table></td>
		    </tr>
        	    <tr>
             		<td  class="titulos5"  align="right">
                            <font face="Arial, Helvetica, sans-serif" class="etextomenu">No. Folios</font>
                    	</td>
                    	<td  class="listado50"><table width='100%'><tr><td class='listado50' align='left' width='<?php echo $dimen?>'>
                        <input name="nofolios" id="nofolios" type="number" size="10" class="tex_area" onkeypress="return isNumberKey(event);">
                    	</td><?php echo $tdocCombo?>
			</tr></table></td>
    		    </tr>
		    <tr>
        		<td class="titulos5" width="26%" align="right"><font color="" face="Arial, Helvetica, sans-serif" class="etextomenu">
        			Dependencia</font>      </td>
        		<td colspan="3" width="78%" class="listado50">
    			<font color="" face="Arial, Helvetica, sans-serif">&nbsp;
			<?php echo $comboDep?>
			</font></td>
		    </tr>
		    <tr style="display:table-row" id='tuplarad' align='center'>
                        <td class="titulos5" width="26%" align="center" colspan=3><font color="" face="Arial, Helvetica, sans-serif" class="etextomenu">
                            <center>
			    	<input class="botones_largo" type="button" value="Radicar" name="Submit33" onclick="return radicar();">
			    </center> </font></td>
                    </tr>
	            <tr><td class="titulos5" width="26%" align="center" colspan=2>
			<div id="radicado"></div></td></tr>
		    <tr style="display:none" id='flujo' align='right'>
                        <td class="titulos5" width="26%" align="center" colspan=3><font color="" face="Arial, Helvetica, sans-serif" class="etextomenu">
                                <center><input class="botones" type="button" value="Informar" id='binfo' name='binfo' onclick="return informarA(this.value);">
				<?php echo $proce?></center>
                            </font></td>
                    </tr>
		    <tr style="display:none;" id='informado'>
			<td class="titulos5" width="26%" align="left"><b>Informar a:</b></td>
			<td class="titulos5" width="26%" align="left"><?php echo $rad->depeXInformar()?><input class="botones_largo" type="button" value="Informar" name="Infff" onclick="Informar()"></td>
		    </tr>
		</table>
	    </td>
	    </tr>
	    <tr><td><div id='informado2' style='display:none'>
		<table border=0  width=100% class="borde_tab">
        <tr>
        <td></td><td class="titulos5"><b>SE HA INFORMADO A:<b></td><td class=listado2><div id='InfoInfoc'>
        </div></td></tr>
        </table></div>
	    </td></tr>
    	</table>
	</form>
		<?php 	break;
	case 'radicar':
	    include($ruta_raiz.'/core/clases/timecount.php');
	    $timerRespuesta= new timecount();
	    $remite="$param2 $param3 $param4";
	    $rad->setRadiDepeRadi($dependencia);
	    $rad->setTipRad($trad);
	    $rad->setDependencia($tpDepeRad[$trad]);
	    //Por omision su tipo documental es no definido
	    $rad->setTdocCodi(0);
	    $rad->setEespCodi(0);
	    $rad->setCedula($param1);
	    $rad->setNit($param1);
	    $rad->setNombre_remitente($param2);
	    $rad->setApellidos_remitente($param3.' '.$param4);
	    $rad->setPrimer_apellidos($param3);
            $rad->setSeg_apellidos($param4);
	    $rad->setRaAsun($param9);
	    $rad->setMuni($muni);
	    $rad->setDepto($dpto);
	    $rad->setPais($pais2);
	    $rad->setContinente($conti);
	    $rad->setDireccion_remitente($param5);
	    $rad->setUsuaCodi($codusuario);
	    $rad->setUsuaDoc($usua_doc);
	    if($tdid_codi=='' || $tdid_codi=NULL){
		$rad->setTdidCodi(1);
	    }
	    else
		$rad->setTdidCodi($tdid_codi);
	    if($trad==2){
		list($usuacodides,$usuadocdes,$logindest,$depe_nomb,$usua_email)=$rad->jefeRad($depsel);
		$roldes=1;
                $mail=0;
	    }
	    else{
		$usuacodides=$codusuario;
		$usuadocdes=$usua_doc;
		$roldes=$id_rol;
                $mail=1;
	    }
	    $codverifica=$rad->genCodiVerifica(6);
	    if($codverifica!='')
                $rad->setCodi_verifica($codverifica);
	    $rad->setOem($oem);
	    $rad->setCiu($ciu);
	    if(!isset($docfun))
		$docfun=0;
		$rad->setDocFun($docfun);
	    $rad->setRadiDepeActu($depsel);
	    $rad->setRadiUsuaActu($usuacodides);
	    $rad->setRadiUsuaActuDoc($usuadocdes);
	    $rad->setRolid($id_rol);
	    $rad->setTrteCodi(0);
	    $rad->setRadiUsuaActurol($roldes);
	    $rad->setDescAnex($param10);
	    $rad->setRadiGuia($guia);
	    $rad->setMrecCodi($mrec);
	    $rad->setRadiNumeHoja($nofolios);
	    $rad->setRadiPath('NULL');
	    $rad->setSgd_apli_codi(0);
	    $rad->setRadiTipoDeri($tipoDeri);
	    $rad->setRadiNumeDeri($radiDeri);
  	    $rad->setRadi_asocc($radiDeri);
	    $rad->setFechaOfic($fecha_ofic);
	    $rad->setRadiCuentai($cuentai);
	    $rad->setDignatario($param8);
	    $timerRespuesta->intertime('Antes de consulta');
            $numerad=$rad->radicar();
            $log->setNumDocu($numerad);
            $log->setAction('document_create');
            $log->setOpera('Radicacion de documento de '.$descrad);
            $log->registroEvento();
	    $rad->setRadiNumeRadi($numerad);
	    $timerRespuesta->intertime('Antes de consulta');
	    $rad->dir_drecciones();
	    //Insercion de metadatos parametrizados
	    if(isset($metadato)){
		include_once "$ruta_raiz/core/Modulos/radicacion/clases/metadatos.php";
		$met= new metadatos($ruta_raiz);
		$met->setRadiNume($numerad);
		$met->setDataTag($metadato);
		$met->insertarInfoMeta($id_meta);
	    }
	    $timerRespuesta->intertime('Antes de render');
            //Alerta de correo activada para radicacion de entrada
            if($mail==0){
		//include_once "$ruta_raiz/core/vista/correo.php";
                $subject="Documento(s) asignado(s) a su cuenta de ORFEO -- $logindest";
                $mensaje="Cordial saludo:<br>\nUsted ha recibido en su bandeja de Orfeo los siguientes radicado(s) :<br>\n<table border='1'><tr><th>No.</th><th>Radicado</th><th>Asunto</th><th>Usuario anterior</th><th>Estado</th></tr>";
                $mensaje.="<tr><td>1</td><td>$numerad</td><td>".htmlentities(substr($param9,0,50))."</td><td>".htmlentities($param2.' '.$param3.' '.$param4)."</td><td>Sin tipificar</td></tr></table>";
                if($usua_email!=null && $usua_email!=''){
                    $headers   = array();
                    $headers[] = "MIME-Version: 1.0";
                    $headers[] = "Content-type: text/html; charset=iso-8859-1";
                    $headers[] = "From: ORFEO IMPRENTA NACIONAL DE COLOMBIA<".CORREOENV.">";
                    $headers[] = "To: <".$usua_email.">";
                    $headers[] = "Subject: {$subject}";
                    $headers[] = "X-Mailer: PHP/".phpversion();
                    mail($correo_usuario, $subject, $mensaje, implode("\r\n", $headers));
                }
           }
	   else{
		//Modificacion para generar documento anexo para generacion de documento
		$rad->setRadiNumeDeri($numerad);
		$rad->setUsuaLogin($krd);
                $anexCod=$rad->sandboxAnexo();
	   }
	   $var=session_name()."=".session_id()."&leido=no&krd=".$krd."&verrad=$numerad&nurad=$numerad&remite=$remite+&dependenciaDestino=$depsel";
	   if(isset($tdoc)){
	       if($tdoc!='0'){
		  $tx->aplicarTRD($numerad,$codusuario,$usua_doc,$dependencia,$id_rol,$serie,$subserie,$tdoc);
		  $log->setNumDocu($numerad);
			$log->setAction('document_classification');
	          $log->setOpera('Aplicada TRD en Radicacion');
 	          $log->registroEvento();
	       }
	   }
	   ?>
	   <div id='datarad'>
	     <center>
	        <img src='<?php echo $ruta_raiz?>/imagenes/iconos/img_alerta_2.gif'>
	   	<font face='Arial' size='3'><b>Se ha generado el radicado No.</b></font>
	    	<font face='Arial' size='4' color='red'><b><u><?php echo $numerad?></u></b></font><br>
		<?php 		if($codverifica!=''){
		    //$codverifica=htmlspecialchars($codverifica);
		?>
		<font face='Arial' size='3'>C.W.: <span style="font-size:20px;font-weight:bold;"><?php echo htmlspecialchars($codverifica)?></span></font><br>
		<?php }  ?>
	     <input type='hidden' id='numerad' value='<?php echo $numerad?>'><a href="<?php echo $ruta_raiz?>/old/radicacion/hojaResumenRad.php?<?php echo $var?>" class="vinculos">Hoja de resumen</a>
	     </center>
	    </div>
	    <div id='tiempo'>
	    <?php 	    $timerRespuesta->endtime();
            $timerRespuesta->printToIntervalo();
            ?>
	    </div>
	    <?php 	break;
    case 'borraInfo':
        list($usuacodides,$usuadocdes,$logindest,$depe_nomb,$usua_email)=$rad->jefeRad($depeinfo);
        $rad->borrarInformado($numerad,$logindest,$dependencia,$depeinfo,$codusuario,$usuacodides,$usua_doc,$usuadocdes,$id_rol,1);
        $log->setNumDocu($numerad);
        $log->setOpera('Borrar informado de'.$logindest.' Dependencia '.$depeinfo);
        $log->registroEvento();
        echo "<font color=red>Se ha borrado un Inf. y registrado en eventos<br></font><br>";
    case 'inforJefe':
        if(!isset($depeinfo)){
            $rads[]=$numerad;
            $log->setNumDocu($numerad);
            $depeinfo=explode(',',$depes);
            for($i=0;$i<count($depeinfo);$i++){
                $depinfo=$depeinfo[$i];
                list($usuacodides,$usuadocdes,$logindest,$depe_nomb,$usua_email)=$rad->jefeRad($depinfo);
                /*echo "<tr  class='listado50'>
                            <td><b>$depinfo - $depe_nomb</b></td>
                            <td class='listado50'><center><ul><li>$logindest - $usua_nomb</li></ul></td></tr>";*/
                $observa="INFORMADO DESDE RADICACION";
                $log->setOpera('Informar en Radicacion a '.$logindest.' Dependencia '.$depinfo);
                $log->registroEvento();
                $rad->informar($rads,$krd,$logindest,$dependencia,$depinfo,$codusuario,$usuacodides,$usua_doc,$usuadocdes,$observa,$id_rol,1);
            }
        }
	/*echo "</table>";
   	break;*/
    case 'pruebas':
	$retorno=$rad->verInformadosRad($numerad);
	echo $retorno;
	break;
    case 'asocOtro':
        echo "<center>";
	$retorno=$rad->asoNuevoTer($tipo,$busq,$param);
	echo $retorno;
        echo "</center>";
    break;
    case 'comboSubseries':
	echo $tx->comboSubSeries($dependencia,$serie);
	break;
    case 'comboTpDoc':
	echo $tx->comboTDoc($dependencia,$serie,$subserie,$trad);
}
?>
