<?php session_start(); date_default_timezone_set('America/Bogota'); date_default_timezone_set('America/Bogota');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

$krd = $_SESSION["krd"];
$dependencia = $_SESSION["dependencia"];
$usua_doc = $_SESSION["usua_doc"];
$codusuario = $_SESSION["codusuario"];
$tip3Nombre = $_SESSION["tip3Nombre"];
$tip3desc = $_SESSION["tip3desc"];
$tip3img = $_SESSION["tip3img"];
$permArchi = $_SESSION["perm_archi"];
$ruta_raiz = "../../../..";
include $ruta_raiz.'/core/config/config-inc.php';
include($ruta_raiz.'/core/Modulos/radicacion/clases/tipoRadicado.php');
$tprad= new tipoRadicado($ruta_raiz);
$val=$tprad->consultaHtml();
      // print_r($tparray);
      $cad = "perm_tp";
      $option = "<option value='0'>inactivo</option><option value='1'>Activo</option>";
$ocupados=$tprad->getOcupados();  
$scripturl=$ruta_raiz.'/core/Modulos/radicacion/vista/operTR.php';
//print_r ($ocupados );
?>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Tipo de Radicaci&oacute;n</title>
    <link rel="stylesheet" href="<?php  echo $ruta_raiz ?>/<?php echo $ESTILOS_PATH ?>/orfeo.css">       
    <style type="text/css">
    table{
     border:none;
      }
</style>
<script language="JavaScript" src="<?php echo $ruta_raiz?>/js/common.js"></script>
<script >
function creartp(){
	
	var codigo = document.getElementById('grpRad').value;
	var nomb = document.getElementById('txtnombre').value;
        var radSal = document.getElementById('radSal').value;
	if(codigo==0){
		alert('Debe colocarC odigo');
		return false;
	}
        if(!nomb){
		alert('Debe colocarle un nombre T.R.');
		return false;
	}
        if(radSal== 99){
		alert('Debe Selecionar Si o no  genera radicado de salida');
		return false;
	}
	var poststr = "action=crearTP&codigo="+codigo+"&nomb="+nomb+"&radSal="+radSal;
	partes('<?php  echo $scripturl; ?>','REsult',poststr,'');
        vistaFormUnitid('modfo',2);
        vistaFormUnitid('creafo',2);
}

function modtp(){
	
	var codigo = document.getElementById('grpRad').value;
	var nomb = document.getElementById('txtnombre').value;
        var radSal = document.getElementById('radSal').value;
	if(codigo==0){
		alert('Debe colocar Codigo');
		return false;
	}
        if(!nomb){
		alert('Debe colocarle un nombre T.R.');
		return false;
	}
        if(radSal== 99){
		alert('Debe Selecionar Si o no  genera radicado de salida');
		return false;
	}
	var poststr = "action=modiTP&codigo="+codigo+"&nomb="+nomb+"&radSal="+radSal;
	partes('<?php  echo $scripturl; ?>','REsult',poststr,'');
        vistaFormUnitid('modfo',2);
        vistaFormUnitid('creafo',2);
}

function elitp(){
	
	var codigo = document.getElementById('grpRad').value;
	var nomb = document.getElementById('txtnombre').value;
        var radSal = document.getElementById('radSal').value;
	if(codigo==0){
		alert('Debe colocar Codigo');
		return false;
	}
        if(!confirm(" Esta seguro de Realizar esta Accion? \n Recuerde que la eliminacion de este tipo de radicado \n puede generar inconvenientes a los usuarios")) {return false;} 
	var poststr = "action=eliTP&codigo="+codigo+"&nomb="+nomb+"&radSal="+radSal;
	partes('<?php  echo $scripturl; ?>','REsult',poststr,'');
        vistaFormUnitid('modfo',2);
        vistaFormUnitid('creafo',2);
}


function limpiar(){
        vistaFormUnitid('modfo',2);
        vistaFormUnitid('creafo',1);
        document.getElementById('grpRad').removeAttribute("readonly",false);
        document.getElementById('grpRad').setAttribute("onchange","validarAct()",false);
	document.getElementById('grpRad').value='';
	document.getElementById('txtnombre').value='';
        document.getElementById('radSal').value=99;

}
function editor(codigo,nomb,radasal){
        vistaFormUnitid('modfo',1);
        vistaFormUnitid('creafo',2);
        document.getElementById('grpRad').setAttribute("readonly","readonly",false);
        document.getElementById('grpRad').removeAttribute("onchange",false);
	document.getElementById('grpRad').value=codigo;
	document.getElementById('txtnombre').value=nomb;
        document.getElementById('radSal').value=radasal;

}
function listar(){
        var poststr = "action=Listar";
	partes('<?php  echo $scripturl; ?>','listaD',poststr,'');

}

function validarAct(){
    var codigo=document.getElementById('grpRad').value;
    var eD=0;
    if(codigo<1){ eD=1;  }
    if(codigo>9){ eD=1;  }
    <?php for ($index = 0; $index < count($ocupados); $index++) {
    echo "if(codigo==".$ocupados[$index]."){ eD=1;  } \n";
        } 
         ?>
    if(eD==1){
        alert('El Codigo  no se Puede repetir, \n ser menor o igual a 0 \n o mayor a 9');
        document.getElementById('grpRad').value='';
    }
}
</script>
</head>

<body>
    <div align='center'>     <table border="0" cellpadding="0" class="borde_tab">
 <tbody><tr>
			    <th class="titulos4" colspan="4">ADMINISTRADOR DE TIPOS DE RADICADOS</th>
			    
				
			</tr>
			<tr>
			    <th class="titulos2">Código</th>
			    <th class="titulos2">Nombre del T.R.</th>
			    <th class="titulos2">Genera radicado de salida</th>
			    <th class="titulos2" width="20%">Acción</th>				
			</tr>
			<tr>
                            <td class="titulos2">       <input type='text' onchange="validarAct()" name="grpRad" id="grpRad" size="5" />
                            </td>
			    <td class="titulos2"><input type="text" name="txtnombre" id="txtnombre" size="20" maxlength="30" /></td>
			    <td class="titulos2">	<select size="1" name="radSal" id="radSal">
			<option value="99">&nbsp;</option>
			<option value="1">S I</option>
			<option value="0">N O</option>
		</select></td>
			    <td class="titulos2">
			    <table><tbody><tr><td>
			    <div id="creafo" style="display: block"><img src="<?php echo $ruta_raiz;?>/imagenes/add1.png" alt="Agregar" width="24" height="24" longdesc="Agregrar Modulo" onclick="creartp()"></div>
			    
			    <div id="modfo" style="display: none">
                            <a href="#" onclick="modtp()"><img src="<?php echo $ruta_raiz;?>/imagenes/dialog-apply1.png" alt="Editar" width="24" height="24" longdesc="Modificar"></a>
                            <a href="#" onclick="elitp()"><img src="<?php echo $ruta_raiz;?>/imagenes/borrar.png" alt="Editar" width="24" height="24" longdesc="Eliminar"></a></div>
			    </td><td>
			    <a href="#" onclick="limpiar('')"><img src="<?php echo $ruta_raiz;?>/imagenes/clear.png" alt="Editar" width="24" height="24" longdesc="Editar Edificio"></a>
                            <a href="admTpRad.php" ><img src="<?php echo $ruta_raiz;?>/imagenes/actualizar.png" alt="Actualizar Lista" width="24" height="24" longdesc="actualizar"></a>
			    </td></tr></tbody></table>
			    </td>
				
			</tr>
                        <tr><td colspan="4"><div id="REsult"></div></td></tr><tr><td colspan="4" class="titulos5">
 <div id="listaD"> 
     <table style="width: 100%">
     <tr>
			    <th class="titulos2">Código</th>
			    <th class="titulos2">Nombre del T.R.</th>
			    <th class="titulos2">Genera radicado de salida</th>
			    <th class="titulos2" width="20%">Acción </th>
				
			</tr>
     <?php  echo $val;?></table></div>
                            </td></tr>		 </tbody></table>
   
</body>
</html>
