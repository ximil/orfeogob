
<html>
<head>
<link rel="stylesheet" href="<?php echo $ruta_raiz?>/estilos/default/orfeo.css">
<script language="JavaScript" src="<?php echo $ruta_raiz?>/js/common.js"></script>
<SCRIPT Language="JavaScript" src="<?php  echo  $ruta_raiz; ?>/core/vista/combos_universales.js.php"></SCRIPT>
<SCRIPT Language="JavaScript" src="<?php  echo  $ruta_raiz; ?>/js/crea_combos_2.js"></SCRIPT>
<script language="JavaScript" type="text/javascript"	src="<?php echo $ruta_raiz?>/js/calendario/calendar_eu.js"></script>
<script type="text/javascript">

function deldata(cod,nom){
	valor=<?php echo $item;?>; 
	var poststr = "action=elimot&nomb="+nom+"&codigo="+cod+"&tipo="+valor;

	
	partes('<?php  echo $scripturl; ?>','REsult',poststr,'');
	vistaFormUnitid("REsult",1);
	Listar(valor);
}
function crearmot(){
	//document.getElementById('cjid').value=cod;
	nomb=document.getElementById('motnomb').value;
	//alert("funcion crear motivo"+ " "+ nomb);
	if(nomb.length< 10) {
		alert('El motivo debe ser mayor de diez caracteres');
		return false;
	}
	
		valor=<?php echo $item;?>;
		var poststr = "action=crearmot&nomb="+nomb+"&tipo="+valor;
		//alert(poststr); 
		partes('<?php  echo $scripturl; ?>','REsult',poststr,'');
		vistaFormUnitid("REsult",1);
		Listar(valor);
	
}

function mod(){
	nomb=document.getElementById('motnomb').value;
	cod=document.getElementById('idmot').value;
	//alert("funcion modificar motivo"+ " "+ nomb +" "+cod );
	valor=<?php echo $item;?>;
	var poststr = "action=modmot&nomb="+nomb+"&codigo="+cod+"&tipo="+valor;
	//alert(poststr); 
	partes('<?php  echo $scripturl; ?>','REsult',poststr,'');
	vistaFormUnitid("REsult",1);
	Listar(valor);
}
//funcion que lleva al ajax
function Listar(valor){
	var poststr = "action=listamotivo&valor="+valor;
	partes('<?php  echo $scripturl; ?>','Listado',poststr,'');
	//alert("funcion Listar metodo partes" + " " + partes);
}
function agregar(){
	//alert("funcion agregar");
	document.getElementById('tlmodacc').innerHTML=' Agregar Motivo';
	document.getElementById('motnomb').value='';
	document.getElementById('idmot').value='';
	vistaFormUnitid("formaMov",1);
	vistaFormUnitid("creafo",1);
	vistaFormUnitid("modfo",2);
	vistaFormUnitid("REsult",2);
}
function modificar(idcamp,descrp){
	
	document.getElementById('tlmodacc').innerHTML=' Modificar Motivo';
	document.getElementById('motnomb').value=descrp;
	document.getElementById('idmot').value=idcamp;
	vistaFormUnitid("formaMov",1);
	vistaFormUnitid("creafo",2);
	vistaFormUnitid("modfo",1);
	vistaFormUnitid("REsult",2);
	
}
//variables para contar caracteres del textarea
contenido_textarea = ""
num_caracteres_permitidos = 100
function valida_longitud(){
   num_caracteres = document.forms[0].motnomb.value.length
   if (num_caracteres > num_caracteres_permitidos){
      document.forms[0].motnomb.value = contenido_textarea
   }else{
      contenido_textarea = document.forms[0].motnomb.value
   }
   if (num_caracteres >= num_caracteres_permitidos){
      document.forms[0].caracteres.style.color="#ff0000";
   }else{
      document.forms[0].caracteres.style.color="#000000";
   }
   cuenta()
}
function cuenta(){
   //document.forms[0].caracteres.value=document.forms[0].texto.value.length
   var difer = num_caracteres_permitidos - document.forms[0].motnomb.value.length
   document.forms[0].caracteres.value=difer
}


</script>
</head>
<body bgcolor="#FFFFFF">
<table border="0" class=borde_tab width='100%' cellspacing="2">
	<tr><td  align='center' class="titulos4">Motivo de <?php echo $titulo; ?></td></tr>
</table>
<!-- -------------inicio arreglo--------- -->
<table border="0" cellpadding="1" cellspacing="1">
	<tr>
		<td width="400" valign="top" rowspan='2'>
			<div id='Listado' style="size:auto"></div>
		</td>
		<td valign="top">
			<input type="button" value="Actualizar" onclick='Listar(<?php echo  $item; ?>);' class='botones'> 
			<input type="button" value="Nuevo"  onclick='agregar();' class='botones'>
		</td>
	</tr>
	<tr>
		<td>
			<div id='formaMov' style='display: none'>
				<table border="0" cellpadding="0" cellspacing="0" class="borde_tab">
					<tr>
						<th class="titulo1" colspan="4"><div id='tlmodacc'> Agregar Motivo </div> </th>
					</tr>
					<tr>
						<th class="titulos2" >Motivo</th>
						<th class="titulos2" width="20%" >Acci&oacute;n</th>
					</tr>
					<tr>
						
						<!-- --------contar caracteres-------------- -->
						<form action="#" method="post">
						<td class="listado2" >
							<textarea id='motnomb' name='motnomb' cols="40" rows="5" onKeyDown="valida_longitud()" onKeyUp="valida_longitud()"></textarea>
								<INPUT id='idmot' type="hidden">
						</td>
												
						
						<td class="listado2" valign="top">
								<table border=0  cellpadding="0" cellspacing="0">
								<tr>
									<td>
										<div id='creafo' style='display: block'>
											<input type="button" value="Agregar" onclick="crearmot()" class='botones'>
										</div>
										<div id='modfo' style='display: none'>
											<input type="button" value="Modificar" onclick="mod()" class='botones'>
										</div>
											<input type="button" value="cancelar" onclick="vistaFormUnitid('formaMov',2);" class='botones'>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					
					<!-- --------contar caracteres-------------- -->
					<tr>
  					<td colspan="2" class="listado2">Caracteres escritos 
    					<input type="text" name="caracteres" size="4" style="border: thin solid #e3e8ec">
    				</td>
  				</tr>
  			</form>
					<!-- <tr><td colspan=4 ><a href='#' onclick='vistaFormUnitid("formaMov",2);'>cancelar</a></td></tr> -->
						<?php echo $table1;?>
				</table>
			</div>
			<div id='REsult' style='display: none'></div>
		</td>
	</tr>
</table>
<!-- -------------fin arreglo------------- -->
<script >
Listar(<?php echo  $item; ?>);

</script>
</body>
</html>
