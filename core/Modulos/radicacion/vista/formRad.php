<?php session_start(); date_default_timezone_set('America/Bogota'); date_default_timezone_set('America/Bogota');
foreach ($_GET as $key => $valor)  $$key = $valor;
foreach ($_POST as $key => $valor)  $$key = $valor;
$krd = $_SESSION["krd"];
$dependencia = $_SESSION["dependencia"];
$usua_doc = $_SESSION["usua_doc"];
$codusuario = $_SESSION["codusuario"];
$id_rol = $_SESSION["id_rol"];
$tpDepeRad=$_SESSION["tpDepeRad"];
$ruta_raiz="../../../..";
$url="$ruta_raiz/core/Modulos/radicacion/vista/operFormRad.php";
//Validando session
include_once "$ruta_raiz/core/vista/validadte.php";
include_once "$ruta_raiz/core/Modulos/radicacion/clases/radicado.php";
$rad= new radicado($ruta_raiz);
$tipoRad=$rad->permTRad($dependencia,$id_rol);
$mesante = date("d/m/Y", mktime(0, 0, 0, date("m") - 1, date("d"), date("Y")));
?>
<html>
    <head>
	<meta charset="utf-8" />
	<title>Formulario de radicaci&oacute;n</title>
	<link rel="stylesheet" href="<?php echo $ruta_raiz?>/estilos/default/orfeo.css">
	<link rel="stylesheet" href="<?php echo $ruta_raiz?>/estilos/jquery-ui.css">
	<style type="text/css">
	#lightbox {
	position: absolute;
	top: 0;
        height: 400px;
	left: 24%;
	width: 90%;
	margin-left: -250px;
	background: #fff;
        overflow: auto;
	z-index: 1001;
	display: none;
	}
	#lightbox-shadow{
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background: #000;
	filter: alpha(opacity=75);
	-moz-opacity: 0.75;
	-khtml-opacity: 0.75;
	opacity: 0.75;
	z-index: 1000;
	display: none;
	}
	</style>
	<script language="JavaScript" src="<?php echo $ruta_raiz?>/js/common.js"></script>
	<script src="<?php echo $ruta_raiz?>/js/jquery-1.10.2.js"></script>
	<script src="<?php echo $ruta_raiz?>/js/jquery-ui.js"></script>
        <script src="<?php echo $ruta_raiz?>/js/jquery.floatThead.min.js"></script>
	<script src="<?php echo $ruta_raiz?>/js/shortcut.js"></script>
	<script> 
	var timeout;
	var dev=0;
	//var delay=1000;
	var isLoading=false;
	var tpDepeRad=<?php echo json_encode($tpDepeRad);?>;
	shortcut.add("alt+p",function() {
	var depsel=$("#depsel").val();
	var numerad=$("#numerad").val();
	var param2=$("#param2").val();
	var param3=$("#param3").val();
	var param4=$("#param4").val();
	var remite=param2+" "+param3+" "+param4;
        var data="<?php echo session_name()?>=<?php echo session_id()?>&leido=no&krd=<?php echo $krd?>&verrad="+numerad+"&nurad="+numerad+"&remite="+remite+"&dependenciaDestino="+depsel;
	if(numerad===undefined){
	    return false;
	}
         window.open ("<?php echo $ruta_raiz?>/old/radicacion/stickerWeb/index5dne.php?"+data+"&alineacion=left","mywindow","menubar=1,resizable=1,width=400,height=150");
	});
        
        function tabflotan(){
            $("#tabresultado").floatThead({
                useAbsolutePositioning:false
            });
        }
        
        function borrarInf(depe){
            var depeinfo=$(depe).val();
            var numerad=$("#numerad").val();
            var formData="action=borraInfo&numerad="+numerad+"&depeinfo="+depeinfo;
            $.ajax({
                url : "<?php echo $url?>",
                type: "POST",
                data: formData
            })
            .done(function(data){
                $("#InfoInfoc").html(data);
            })
            .fail(function(){
                alert("Fallo el ajax");
            });
        }

	$(function(){
	    $.datepicker.setDefaults({dateFormat:"dd/mm/yy",
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
                dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
                dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		changeYear: true,
		showOn: "button",
		buttonImage: "<?php echo $ruta_raiz?>/imagenes/calendar.gif",
		buttonImageOnly: true,
		buttonText: "Seleccione fecha",
		constrainInput: true,
                maxDate: "<?php echo date("d/m/Y")?>",
		beforeShow: function(){
		    $(".ui-datepicker").css('font-size', 12)
		}
	    })
	    $("#fecha_ini").datepicker({
                onSelect: function(selected){
                    $("#fecha_fin").datepicker('option','minDate',selected);
                }
            });
            $("#fecha_ini").datepicker("setDate","<?php echo $mesante?>");
	    $("#fecha_fin").datepicker({
                minDate: "<?php echo $mesante?>",
                onSelect: function(selected){
                    $("#fecha_ini").datepicker('option','maxDate',selected);
                }
            });
	    $("#fecha_fin").datepicker("setDate","<?php echo date("d/m/Y")?>");
	});

	function fechaof(){
	    $("#fecha_ofic").datepicker();
            $("#fecha_ofic").datepicker("setDate","<?php echo date("d/m/Y")?>");
	}

        function maximo(campo,limite){
                if(campo.value.length>=limite){
                campo.value=campo.value.substring(0,limite);
                 }
                }

	function reloadSearch(param1,param2){
	    if(!isLoading){
		var searchTerm=$(param1).val();
		var fecha_ini=$('#fecha_ini').val();
		var fecha_fin=$('#fecha_fin').val();
                var trad=$("input[name=trad]:checked").val();
		var response;
		var formData;
		var tipo;
		var control;
		if(dev<=2){
		    response="#response";
		}else{
		    response="#response2";
		}
		if(param2==0 || param2==1){
		    control=5;
		}
                else{
		    control=2;
		}
		if(searchTerm.length>control){
		    //timeout=setTimeout(function(){
		    isLoading=true;
		    //log("ajax call - " + searchTerm, true);
                
                // Simulate a real ajax call
                setTimeout(function() { isLoading = false; }, 1000);
            //}, delay);
		    if(param2==0)
	    	    	formData="action=busqRad&norad="+searchTerm+"&trad="+trad+"&fecha_ini="+fecha_ini+"&fecha_fin="+fecha_fin;
		    if(param2==1)
		    	formData="action=busqDoc&nodoc="+searchTerm+"&trad="+trad+"&fecha_ini="+fecha_ini+"&fecha_fin="+fecha_fin;
		    if(param2==2)
			formData="action=busqNombre&parametro="+searchTerm+"&trad="+trad+"&fecha_ini="+fecha_ini+"&fecha_fin="+fecha_fin;
		    if(dev>2){
			tipo=$("#tipo").val();
			formData="action=asocOtro&tipo="+tipo+"&busq="+param2+"&param="+searchTerm;
		    }
		    //alert(formData);
		    $.ajax({
			url : "<?php echo $url?>",
			type: "POST",
			data: formData
		    })
		    .done(function(data){
			$(response).html(data);
                        tabflotan();
		    })
		    .fail(function(){
			alert("Fallo el ajax");
		    });
	        }
		else{
		    if(dev<=2){
		    	$("#response").html("");
		    }
		    else{
			$("#response2").html("");
		    }
		}
	    }
		    //log("No hay suficientes caracteres");
	    else{
		//alert("Falla");
		//log("Sigue haciendo la busqueda anterior");
	    }
	}


	function cambioPais(cont,sel){
	    var formData;
	    if(typeof sel === typeof undefined){
                select="";
            }
            else{
		select="&sel="+sel;
            }
	    formData="action=comboPais&continente="+cont+select;
	    cambioDpto('0');
	    $.ajax({
		url : "<?php echo $url?>",
		type: "POST",
		data: formData
	    })
	    .done(function(data){
		$("#pais").html(data);
	    })
	    .fail(function(){
		alert("Fallo el ajax");
	    });
	}

	function cambioDpto(pais,sel){
	    var formData;
	    if(typeof sel === typeof undefined){
                select="";
            }
            else{
		select="&sel="+sel;
            }
	    formData="action=comboDpto&pais2="+pais+select;
	    cambioMuni('0');
	    $.ajax({
                url : "<?php echo $url?>",
                type: "POST",
                data: formData
            })
            .done(function(data){
                $("#dpto").html(data);
            })
            .fail(function(){
                alert("Fallo el ajax");
            });
	}

	function cambioMuni(dpto,country,sel){
	    var pais;
            var formData;
	    var select;
	    if(typeof country === typeof undefined){
		pais=$("#comboPais").val();
	    }
	    else{
		pais=country;
	    }
	    if(typeof sel === typeof undefined){
		select="";
	    }
	    else{
		select="&sel="+sel;
	    }
	    formData="action=comboMuni&pais2="+pais+"&dpto="+dpto+select;
            $.ajax({
                url : "<?php echo $url?>",
                type: "POST",
                data: formData
            })
            .done(function(data){
                $("#muni").html(data);
            })
            .fail(function(){
                alert("Fallo el ajax");
            });
        }

	function devolver(){
	    if(dev==1){
		window.location="formRad.php";
	    }
            if(dev==2){
		$("#p1").show('fast');
                $("#p2").show('fast');
                $("#p3").show('fast');
                $("#p4").hide('fast');
                $("#p5").hide('fast');
                $("#p6").hide('fast');
                $("#nombre").val('');
                $("#nodoc").val('');
                $("#norad").val('');
                $("#response").html('');
                $("input[name=ra1]:radio").removeAttr('checked');
	        $("#response").html("");
	        dev--;
	    }
	    if(dev==3){
		cerrar();
		$("#tipo").val($("#tipo").find('option[selected]').val());
            	dev--;
	    }
	    if(dev==4){
                $("#j1").show('fast');
                $("#j2").show('fast');
                $("#j4").hide('fast');
                $("#j5").hide('fast');
                $("#nombre2").val('');
                $("#nodoc2").val('');
                $("#norad2").val('');
                $("input[name=ra2]:radio").removeAttr('checked');
                $("#response2").html("");
                dev--;
            }
	}

        function devolver2(){
            $("#busqueda").show('fast');
            $("#response").html("");
        }
        
        function overWindow(script){
	    if($("#lightbox").size()==0){
	    var theLightbox = $('<div id="lightbox"/>');
	    var theShadow = $('<div id="lightbox-shadow"/>');
	        $('body').append(theShadow);
	        $('body').append(theLightbox);
	    }
	    $("#lightbox").empty();
	    $.get(script, function(data){
                $("#lightbox").append(data);
	    });
	    $('#lightbox').css('top', $(window).scrollTop() + 100 + 'px');
            // display the lightbox
            $('#lightbox').show();
            $('#lightbox-shadow').show();
            dev++;
        }
	
        function formAgr(seleccion,alterdata,id_meta){
            var tabs=$("#tabs").tabs();
            var contenido=$("#tab-1").contents();
            var ul=tabs.find("ul");
	    $("#id_meta").val(id_meta);
            $("<li><a href='#tab-2'>"+seleccion+"</a></li>").appendTo(ul);
            var newtab=$("<div id='tab-2' style='padding:0px'/>");
            newtab.appendTo(tabs);
            if(typeof alterdata === typeof undefined){
                newtab.append(contenido);
            }
            else{
                $.get(alterdata,function(data){
                    newtab.append(data);
                });
            }
            tabs.tabs('refresh');
            $("#rntab").hide('fast');
            cerrar();
            dev--;
        }

	function dataBusq(dato){
	   switch($(dato).val()){
                    case '1':
                        $('#j1').hide('fast');
                        $('#j2').hide('fast');
                        $('#j4').show('fast');
                    break;
                    case '2':
                        $('#j1').hide('fast');
                        $('#j2').hide('fast');
                        $('#j5').show('fast');
                    break;
                }
                dev++; 
	}

	function cerrar(){
	   $('#lightbox').hide();
	   $('#lightbox-shadow').hide();
	   $('#lightbox').empty();
	}

        function nuevoARadicar(){
	    $("#busqueda").show('fast');
	    $("#p1").show('fast');
	    $("#p2").show('fast');
	    $("#p3").show('fast');
            $("#p4").hide('fast');
            $("#p5").hide('fast');
            $("#p6").hide('fast');
	    $("#nombre").val('');
	    $("#nodoc").val('');
	    $("#norad").val('');
	    $("#response").html('');
	    $("input[name=ra1]:radio").removeAttr('checked');
	    dev=1;
        }

	function contFlujo(){
	    var trad=$("input[name=trad]:checked").val();
	    var descrad=$("#t"+trad).val();
	    var numerad=$("#numerad").val();
	    var linkNuevo="<?php echo $ruta_raiz?>/old/verradicado.php?verrad="+numerad+"&<?php echo session_name()?>=<?php echo session_id()?>&adodb_next_page=1&depeBuscada=&filtroSelect=&tpAnulacion=&carpeta="+trad+"&tipo_carp=&nomcarpeta="+descrad+"&agendado=&orderTipo=DESC&orderNo=16&menu_ver_tmp=2";
	    window.location=linkNuevo;
	}

	function isNumberKey(evt)
	{
	    var charCode = (evt.which) ? evt.which : event.keyCode
	    if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	    return true;
	}

	function crearTercero(){
	    var nodoc=$("#nodoc").val();
	    $("#busqueda").hide('fast');
	    var formData="action=formTercero&nodoc="+nodoc;
	    $.ajax({
                url : "<?php echo $url?>",
                type: "POST",
                data: formData
            })
            .done(function(data){
                $("#response").html(data);
            })
            .fail(function(){
                alert("Fallo el ajax");
            });
	}

	function radicarNuevo(tipo,id){
	    var trad=$("input[name=trad]:checked").val();
	    $("#busqueda").hide('fast');
	    var formData="action=radicarNuevo&tipo="+tipo+"&codi="+id+"&trad="+trad;
	    $.ajax({
                url : "<?php echo $url?>",
                type: "POST",
                data: formData
            })
            .done(function(data){
                $("#response").html(data);
		fechaof();
                pestana();
            })
            .fail(function(){
                alert("Fallo el ajax");
            });
	}

        function validarEmail(valor){
        re=/^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*\.[a-z]{2,3}$/
        //   re= /^[a-z][a-z-_0-9.]+@[a-z-_=>0-9.]+.[a-z]{2,3}$/i
            if(!re.exec(valor))    {          
                alert("La direccion de email "+valor+" es incorrecta.");
            return 'fallo';
            }else{
            //alert("La direccin de email " + valor + " es correcta.");
            return true;
            }
        }

	function validarTercero(){
	    var param1=$.trim( $("#param1").val() );
	    var param2=$.trim( $("#param2").val() );
	    var param3=$.trim( $("#param3").val() );
	    var param4=$.trim( $("#param4").val() );
	    var param5=$.trim( $("#param5").val() );
	    var param6=$.trim( $("#param6").val() );
	    var param7=$.trim( $("#param7").val() );
	    var conti=$("#comboCont").val();
            var pais=$("#comboPais").val();
            var dpto=$("#comboDpto").val();
            var muni=$("#comboMuni").val();
	    var tipo=$("#entrada").val();
	    var trad=$("input[name=trad]:checked").val();
	    var formData="action=crearTercero&param1="+param1+"&param2="+param2+"&param3="+param3+"&param4="+param4+"&param5="+param5+"&param6="+param6+"&param7="+param7+"&conti="+conti+"&pais2="+pais+"&dpto="+dpto+"&muni="+muni+"&tipo="+tipo+"&trad="+trad;
            var nomb;
            var apell;
            if(tipo==0){
                nomb="El nombre es obligatorio";
		apell="La persona debe tener el primer apellido";
            }
            else{
                nomb="La razon social es obligatoria";
            }
            if(param2.length<2){
                alert(nomb);
                return false;
            }
            if(param3.length<4 && tipo==0){
                alert(apell);
                return false;
            }
            if(param5.length<5){
                alert("La direccion es obligatoria.\nY debe ser de minimo 5 caracteres");
                return false;
            }
            if(param7.length>0){
                if(validarEmail(param7)=='fallo'){
                    return false ;
                }
            }
	    $.ajax({
                url : "<?php echo $url?>",
                type: "POST",
                data: formData
            })
            .done(function(data){
                $("#response").html(data);
                fechaof();
                pestana();
            })
            .fail(function(){
                alert("Fallo el ajax");
            });
	}

	function radicar(){
            var param1=$.trim( $("#param1").val() );
            var param2=$.trim( $("#param2").val() );
            var param3=$.trim( $("#param3").val() );
            var param4=$.trim( $("#param4").val() );
            var param5=$.trim( $("#param5").val() );
            var param6=$.trim( $("#param6").val() );
            var param7=$.trim( $("#param7").val() );
	    var param8=$.trim( $("#param8").val() );
	    var param9=$.trim( $("#param9").val() );
	    var param10=$.trim( $("#param10").val() );
            var conti=$("#comboCont").val();
            var pais=$("#comboPais").val();
            var dpto=$("#comboDpto").val();
            var muni=$("#comboMuni").val();
	    var tipoDeri=$("#tipoDeri").val();
	    var radiDeri=$("#radiDeri").val();
	    var trad=$("input[name=trad]:checked").val();
            var descrad=$("#t"+trad).val();
            if(typeof descrad === typeof undefined || descrad==""){
                descrad=$("input[name='t"+trad+"']").val();
            }
	    var depsel=$("#depsel").val();
	    var guia=$("#guia").val();
	    var mrec=$("#medrec").val();
	    var nofolios=$("#nofolios").val();
	    var oem=$("#oem").val();
            var ciu=$("#ciu").val();
	    var fecha_ofic=$("#fecha_ofic").val();
	    var cuentai=$("#cuentai").val();
	    var serie=$("#seriedocf").val();
	    var subserie=$("#subserdocf").val();
	    var tdoc=$("#tdocf").val();
	    var datatrd="";
            var datater;
            var sentil=0;
	    var metadato='';
	    var id_meta=$("#id_meta").val();
	    //var tipoFac=$("#tipoFac").val();
	    var extrapar="";
	    /*if (typeof tipoFac !== typeof undefined){
		var RemFact=$("#RemFact").val();
		var DescFact=$("#DescFact").val();
		extrapar="&tipoFac="+tipoFac+"&RemFact="+RemFact+"&DescFact="+DescFact;
	    }*/
	    if(oem==0 && ciu==0){
		var docfun=$("#docfun").val();
		datater="&oem="+oem+"&ciu="+ciu+"&docfun="+docfun;
	    }
	    else{
		datater="&oem="+oem+"&ciu="+ciu;
            }
	    if(oem==0 && ciu==0 && (typeof docfun === typeof undefined || docfun==0 || docfun=='')){
		alert("ATENCION: Respuesta a un formulario web. Seleccione el boton CAMBIAR para ingresar el nombre de la entidad o ciudadano.");
		return false;
	    }else{
		if(oem==0 && ciu==0 && trad!=3){
		    alert("Este tipo de radicacion proviene o es dirigido a un tercero externo de la entidad");
		}
	    }
	    if(typeof tdoc!==typeof undefined){
		datatrd="&serie="+serie+"&subserie="+subserie+"&tdoc="+tdoc;;
	    }
	    var formData="action=radicar&param1="+param1+"&param2="+param2+"&param3="+param3+"&param4="+param4+"&param5="+param5+"&param6="+param6+"&param7="+param7+"&param8="+param8+"&param9="+param9+"&param10="+param10+"&conti="+conti+"&pais2="+pais+"&dpto="+dpto+"&muni="+muni+"&trad="+trad+"&mrec="+mrec+"&depsel="+depsel+"&tipoDeri="+tipoDeri+"&nofolios="+nofolios+"&radiDeri="+radiDeri+datater+"&fecha_ofic="+fecha_ofic+"&cuentai="+cuentai+"&guia="+guia+"&descrad="+descrad+extrapar+datatrd+"&id_meta="+id_meta;
	    if(param5==''){
		alert("La direccion es obligatoria");
		return false;
	    }
	    if(depsel==0){
		alert("La dependencia es obligatoria");
                return false;
	    }
	    if(depsel==-1){
                alert("El usuario en la dependencia no tiene nivel de seguridad");
                return false;
            }
	    if((nofolios.length==0 || nofolios==0) && trad==2){
		alert("El campo de No. folios es obligatorio\ny no puede ser cero");
		return false;
	    }
	    if(muni==0 || dpto==0 || pais==0 || conti==0){
		alert("La ubicacion topografica debe estar bien definida");
		return false;
	    }
            if(param7.length>0){
                if(validarEmail(param7)=='fallo'){
                    return false ;
                }
            }
            if(param9.length==0){
                alert("El asunto es obligatorio");
                return false;
            }
            
            if($("#tab-2").size()>0){
                $("#tab-2").find(':required').each(function (){
                    var input=$.trim($(this).val());
		    if(input.length==0){
                    	alert("El campo "+$(this).attr('name')+" es obligatorio");
                    	sentil=1;
	            }
                });
		$("#tab-2").find('input').each(function (){
		    metadato+='&metadato['+$(this).attr('name')+']='+$(this).val();
                });
		//une metadatos al form de radicacion
		formData=formData+metadato;
            }
            if(sentil==1){
                return false;
            }
            $("#rntab").hide('fast');
	    $("#tuplarad").hide('fast');
	    $("#devolverRad").hide('fast');
            deshabilitarTodo();
	    $("#flujo").show('fast');
	    $.ajax({
                url : "<?php echo $url?>",
                type: "POST",
                data: formData
            })
            .done(function(data){
                $("#radicado").html($(data).filter('#datarad').html());
		$("#timer").html($(data).filter('#tiempo').html());
            })
            .fail(function(){
                alert("Fallo el ajax");
            });
	    $("#cambiar").hide('fast');
	}
        
        function deshabilitarTodo(){
            $("#param1").prop('readonly',true);
            $("#param2").prop('readonly',true);
            $("#param3").prop('readonly',true);
            $("#param4").prop('readonly',true);
            $("#param5").prop('readonly',true);
            $("#param6").prop('readonly',true);
            $("#param7").prop('readonly',true);
            $("#param8").prop('readonly',true);
            $("#param9").prop('readonly',true);
            $("#param10").prop('readonly',true);
            $(".select").prop('disabled','disabled');
            $("#nofolios").prop('readonly',true);
            $("#cuentai").prop('readonly',true);
            $("#guia").prop('readonly',true);
            $("#fecha_ofic").datepicker("option","disabled",true);
        }
        
        function pestana(){
           $("#tabs").tabs(); 
        }

	function cambioTercero(){
	    var tipo=$("#entrada").val();
                if(tipo==1){
                    $("#rowciu").hide('fast');
                    $("#rowoem").show('fast');
                }
                else{
                    $("#rowciu").show('fast');
                    $("#rowoem").hide('fast');
                }
        }

	function informarA(){
	    if($("#informado").is(':hidden')){
		$("#informado").show('fast');
	    }
	    else{
 		$("#informado").hide('fast');
	   }
	   $("#depeXinformar").removeAttr('disabled');
	}

	function Informar(){
	    var radicado=$("#numerad").val();
	    var depes=$("#depeXinformar").val();
            var formData="action=inforJefe&numerad="+radicado+"&depes="+depes;
	    //$(boton).attr('disabled','disabled');
	    $("#informado2").show('fast');
	    if(radicado === undefined){
		alert("ERROR: No se encontro el numero de radicado");
	    }
	    else{
		$("#informado").hide('fast');
		$.ajax({
                    url : "<?php echo $url?>",
                    type: "POST",
                    data: formData
            	})
                .done(function(data){
                    $("#InfoInfoc").html(data);
                })
                .fail(function(){
                    alert("Fallo el ajax");
            	});
	    }
        }

	function asoccOtro(id,tdid_codi,tipo,param1,param2,param3,param4,param5,param6,param7,cont,pais,dpto,muni){
	    if(tipo==0){
		$("#oem").val('0');
		$("#fundoc").val('0');
		$("#ciu").val(id);
	    }
	    if(tipo==1){
                $("#oem").val(id);
                $("#fundoc").val('0');
                $("#ciu").val('0');
            }
	    if(tipo==2){
                $("#oem").val('0');
                $("#fundoc").val(id);
                $("#ciu").val('0');
            }
	    $("#tdid_codi").val(tdid_codi);
	    $("#param1").val(param1);
            $("#param2").val(param2);
            $("#param3").val(param3);
            $("#param4").val(param4);
            $("#param5").val(param5);
            $("#param6").val(param6);
            $("#param7").val(param7);
	    $("#comboCont").val(cont);
	    cambioPais(cont,pais);
	    cambioDpto(pais,dpto);
	    cambioMuni(dpto,pais,muni);
            $("#tipo").prop('disabled','disabled');
            $("#cambiar").hide('fast');
	    cerrar();
	    dev--;
	}

	function mostrarAsocc(tipo,id){
	   var asocc;
           if(tipo==0){
		asocc="#ciu"+id;
	   }
           if(tipo==1){
		asocc="#oem"+id;
	   }
	   if(tipo==2){
		asocc="#fun"+id;
	   }
	   if($(asocc).is(':hidden')){
		$(asocc).show('fast');
	   }
	   else{
		$(asocc).hide('fast');
	   }
	}

	function relacionar(tipoDeri,numrad){
	    var trad=$("input[name=trad]:checked").val();
	    var formData="action=relacionarRad&numrad="+numrad+"&tipoDeri="+tipoDeri+"&trad="+trad;
	    $("#busqueda").hide('fast');
	    $.ajax({
                url : "<?php echo $url?>",
                type: "POST",
                data: formData
            })
            .done(function(data){
                $("#response").html(data);
		fechaof();
                pestana();
            })
            .fail(function(){
                alert("Fallo el ajax");
            });
	}

	$(function(){
	    $("#norad").keyup(function(){
		if(timeout){
		    clearTimeout(timeout);
		}
		reloadSearch("#norad",0);
	    });
	    $("#nombre").keyup(function(){
		if(timeout){
                    clearTimeout(timeout);
                }
                reloadSearch("#nombre",2);
	    });
	    $("#nodoc").keyup(function(){
                if(timeout){
                    clearTimeout(timeout);
                }
                reloadSearch("#nodoc",1);
            });
	    $("input[name=ra1]:radio").change(function(){
		switch($(this).val()){
		    case '1':
			$('#p1').hide('fast');
			$('#p2').hide('fast');
			$('#p3').hide('fast');
			$('#p4').show('fast');
		    break;
		    case '2':
			$('#p1').hide('fast');
			$('#p2').hide('fast');
                        $('#p3').hide('fast');
			$('#p5').show('fast');
		    break;
		    case '3':
			$('#p1').hide('fast');
			$('#p2').hide('fast');
                        $('#p3').hide('fast');
			$('#p6').show('fast');
                    break;
		}
		dev++;
	    })
	    $("input[name=trad]:radio").change(function(){
		var trad=$("input[name=trad]:checked").val();
		var descrad=$("#t"+trad).val();
                if(typeof descrad === typeof undefined || descrad==""){
                    descrad=$("input[name='t"+trad+"']").val();
                }
		//alert("Ha seleccionado radicado de "+descrad);
		var formdata="action=cargarTRad&trad="+trad+"&descrad="+descrad;
		if(tpDepeRad[trad]==null){
                    alert("El tipo de radicacion "+descrad+" no tiene consecutivos para esta dependencia.\nFavor comuniquese con el administrador del sistema");
                    $("input[name=trad]").prop('checked',false);
                }
                else{
                    $("#rtrad").hide('fast');
                    $('#p1').show('fast');
                    $('#p2').show('fast');
                    $('#p3').show('fast');
		    $('#p7').show('fast');
		    dev++;
		if(trad==1 || trad==2 || trad==3){
		    $("#rinfo").show('fast');
		    if(trad==1)
			$("#drinfo").html("Radicado de Salida es todo documento que se elabora para entidades, empresas y/o ciudadanos.")
		    if(trad==2)
			$("#drinfo").html("Radicado de Entrada es todo documento que se recibe o ingresa por la ventanilla de correspondencia.");
		    if(trad==3)
			$("#drinfo").html("Radicado Interno es todo documento de trámite interno de la entidad.");
		}
		$.ajax({
		    url:  "<?php echo $url?>",
		    type: "POST",
		    data: formdata
		})
		.done(function(data){
		    $("#cabezote").html(data);
		})
		.fail(function(){
		    alert("Fallo el ajax");
		})
		}
	    })
	    $("#entrada").change(function(){
		var tipo=$("#entrada").val();
		if(tipo==1){
		    $("#rowciu").hide('fast');
		    $("#rowoem").show('fast');
		}
		else{
		    $("#rowciu").show('fast');
		    $("#rowoem").hide('fast');
		}		
	    })
	});

	function comboSubseries(serie){
	    var formdata="action=comboSubseries&serie="+serie;
	    $.ajax({
                url:  "<?php echo $url?>",
                type: "POST",
                data: formdata
            })
            .done(function(data){
                $("#subsdoc").html(data);
		if(serie==0){
		    comboTipodoc(0);
		}
            })
            .fail(function(){
                alert("Fallo el ajax");
            });
	}

	function comboTipodoc(subserie){
	    var serie=$("#seriedocf").val();
	    var trad=$("#trad:checked").val();
	    var formdata="action=comboTpDoc&serie="+serie+"&subserie="+subserie+"&trad="+trad;
	    $.ajax({
                url:  "<?php echo $url?>",
                type: "POST",
                data: formdata
            })
            .done(function(data){
                $("#tpdoc").html(data);
            })
            .fail(function(){
                alert("Fallo el ajax");
            });
	}
	</script>
    </head>
    <body>
	<div id='contenedor'>
	<form name="formulario" style="margin: 0" method="GET">
	<div id='cabezote'>
	<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center" class="BORDE_TAB">
	    <tr align="center" >
        <td class="titulos4">
        <center>RADICACI&Oacute;N DE DOCUMENTOS</center></td></tr>
	</table>
	</div>
	<div id='busqueda' style='display:block;'>
	<table class="borde_tab" width="90%" align="center" border="0" cellspacing="0" cellpadding="0">
	    <tr class='info'><td><span class='info'>Esta acción le permitirá realizar un radicado base partiendo de una solicitud, respuesta o entrada a la entidad.
		</span></td></tr>
	    <tr class="titulo1"><td height="19">Datos de b&uacute;squeda</td></tr>
	    <tr>
		<td>
		    <table width="100%" align="center" cellspacing="1" cellpadding="1" border="0">
		    <tr id='rtrad' style="display:table-row">
			<td class="titulos2" width="25%" colspan=2>Seleccione un tipo de radicaci&oacute;n</td>
			<td class="listado2" width="25%" colspan=2><?php echo $tipoRad?></td>
		   </tr>
            		<tr id='p1' style="display:none"><td class="titulos2" width="25%" colspan="4"><input id='r1' type='radio' name='ra1' value=1>Nombre</td></tr>
			<tr id='p2' style="display:none"><td class="titulos2" width="25%" colspan="4"><input id='r2' type='radio' name='ra1' value=2>Documento</td></tr>
			<tr id='p3' style="display:none"><td class="titulos2" width="25%" colspan="4"><input id='r3' type='radio' name='ra1' value=3>No. Radicado</td></tr>
                        <tr id='p4' style="display:none"><td class="titulos2" width="15%">Nombre</td><td class="titulos2" width="25%" colspan=3> <input type='text' id='nombre'></td></tr>
            		<tr id='p5' style="display:none"><td class="titulos2" width="15%">Documento</td><td class="titulos2" width="25%" colspan=3> <input type='text' id='nodoc'></td></tr>
            		<tr id='p6' style="display:none"><td class="titulos2" width="15%">No. Radicado</td><td class="titulos2" width="25%" colspan=3><input type='text' id='norad'></td></tr>
			<tr id='p7' style="display:none">
			    <td   class="titulos2" style="width:25%">Desde fecha (dd/mm/aaaa) </td>
			    <td  class="listado2" style="width:25%">
				<input id='fecha_ini' type='text' readonly/>
			    </td>
                            <td   class="titulos2" style="width:25%">Hasta fecha (dd/mm/aaaa) </td>
			    <td  class="listado2" style="width:25%">
				<input id='fecha_fin' type='text' readonly/>
			    </td>
			</tr>
			<tr id='rinfo'>
			    <td class="info" colspan=4><div id='drinfo'></div></td>
			</tr>
			<tr>
			    <td class="info" colspan=4 id='devolver'><center><input class='botones' type='button' value='Devolver' onclick="devolver()"></center></td>
			</tr>
		   </table>
		</td>
	    </tr>
	</table></div>
	</form>
	<div id="response"></div>
	<div id='timer' name='timer' style='alignment-adjust: auto;text-align: right;font-size: 10'></div>
    </body>
</html>
