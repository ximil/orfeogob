<?php session_start(); date_default_timezone_set('America/Bogota'); date_default_timezone_set('America/Bogota');
foreach ($_GET as $key => $valor)  $$key = $valor;
foreach ($_POST as $key => $valor)  $$key = $valor;
$krd = $_SESSION["krd"];
$dependencia = $_SESSION["dependencia"];
$usua_doc = $_SESSION["usua_doc"];
$codusuario = $_SESSION["codusuario"];
$id_rol = $_SESSION["id_rol"];
$tpDepeRad=$_SESSION["tpDepeRad"];
$ruta_raiz="../../../..";
$url="$ruta_raiz/core/Modulos/Expediente/vista/operBusqExp.php";
//Validando session
include_once "$ruta_raiz/core/vista/validadte.php";
?>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Formulario de radicaci&oacute;n</title>
        <link rel="stylesheet" href="<?php echo $ruta_raiz?>/estilos/default/orfeo.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css">
	<script language="JavaScript" src="<?php echo $ruta_raiz?>/js/common.js"></script>
	<script src="<?php echo $ruta_raiz?>/js/jquery-1.10.2.js"></script>
	<script>
	var timeout;
        var dev=0;
        var isLoading=false;
	function noPermiso(){
	    alert("No tiene permiso para acceder a la informacion del Expediente");
	}
	function reloadSearch(){
            if(!isLoading){
		var a=$("#numExp").val();
		var b=$("#numRad").val();
		var c=$("#titExp").val();
		var d=$("#asunExp").val();
		var formData="numExp="+a+"&numRad="+b+"&titExp="+c+"&asunExp="+d;
		if(a.length>=7 || b.length>=6 || c.length>=6 || d.length>=6){
                    isLoading=true;
                    setTimeout(function() { isLoading = false; }, 1000);
		    $.ajax({
                        url : "<?php echo $url?>",
                        type: "POST",
                        data: formData
                    })
                    .done(function(data){
                        $("#response").html(data);
                        //tabflotan();
                    })
                    .fail(function(){
                        alert("Fallo el ajax");
                    });
		}
		else{
		    $("#response").html("");
		}
	    }
	}

	$(function(){
            $("#numExp").keyup(function(){
                if(timeout){
                    clearTimeout(timeout);
                }
                reloadSearch();
            });
            $("#numRad").keyup(function(){
                if(timeout){
                    clearTimeout(timeout);
                }
                reloadSearch();
            })
            $("#titExp").keyup(function(){
                if(timeout){
                    clearTimeout(timeout);
                }
                reloadSearch();
            });
	    $("#asunExp").keyup(function(){
                if(timeout){
                    clearTimeout(timeout);
                }
                reloadSearch();
            })
	});
	</script>
    </head>
    <body>
	<form id='formBExp' method='post'>
	<table class="borde_tab" width="90%" cellspacing="2" cellpadding="0" border="0" align='center'>
	    <tr>
		<td class="titulos4" colspan=4>
		<table class="titulos4" width="100%" cellspacing="0" cellpadding="0" border="0">
		    <tr>
			<td>Busqueda de Expedientes</td>
		    </tr>
		</table>
		</td>
	    </tr>
	    <tr>
		<td class='titulos5' style='width:25%;'>
		N&uacute;mero de expediente
		</td>
		<td class='listado5' style='width:25%'>
		   <input type='text' id='numExp' class='tex_area' name='numExp'>
		</td>
		<td class='titulos5' style='width:25%;'>
                Radicado
                </td>
                <td class='listado5' style='width:25%'>
                   <input type='text' id='numRad' class='tex_area' name='numRad'>
                </td>
	    </tr>
	    <tr>
                <td class='titulos5'>
                    Titulo de Expediente
                </td>
                <td class='listado5' colspan=3>
                   <input type='text' id='titExp' class='tex_area' size='70' maxlength='70' name='titExp'>
                </td>
	    </tr>
	    <tr>
                <td class='titulos5'>
                    Asunto u Objeto
                </td>
                <td class='listado5' colspan=3>
                   <input type='text' id='asunExp' class='tex_area' size='70' maxlength='70' name='asunExp'>
                </td>
            </tr>
	    <tr>
		<td class='info' colspan=4>Digite en el campo requerido el número o descripción del expediente, el sistema despliega un listado con las opciones posibles a la relación.</td>
	    </tr>
	</table>
	</form>
	<div id='response'>
	</div>
    </body>
</html>
