<?php session_start();
$numExpediente=$_POST['numeroExpediente'];
//print_r($_POST);
if(!in_array(substr($numExpediente,4,3),$_SESSION['depeMatrizPer']))
{
  die("No tiene acceso al expediente");
}
$ruta_raiz='../../../..';
include_once "$ruta_raiz/core/vista/validadte.php";
include_once $ruta_raiz . '/core/Modulos/Expediente/clases/expediente.php';
include_once $ruta_raiz . '/core/clases/file-class.php';
include_once $ruta_raiz . '/core/config/config-inc.php';
$encrypt = new file();
$objExpe = new expediente($ruta_raiz);
$objExpe->setNumExp($numExpediente);
$expCierre=$objExpe->solEstadoExp();
if(isset($_POST['subcierre'])){
    if($expCierre==2){
	$adm=$expCierre;
    }
    else{
	$adm=1;
    }
    include_once "$ruta_raiz/core/clases/usuarioOrfeo.php";
    $usua=new usuarioOrfeo($ruta_raiz);
    $usua->setDepe_codi($_SESSION['dependencia']);
    $usua->setRols(1);
    $usua->consultar_usuario_depe_rol();
    $jefe_nomb=$usua->getNombre();
    $tpDepeRad=$_SESSION['tpDepeRad'];
    $tpDescRad=$_SESSION['tpDescRad'];
    if($tpDepeRad[3]==null || $tpDepeRad[3]==""){
	die("<span style='font-color:red;'><b>Su usuario no puede radicar el documento cierre de expediente, por configuraci&oacute;n de consecutivos.</b></span>");
    }
    include_once "$ruta_raiz/core/Modulos/radicacion/clases/radicado.php";
    $rad=new radicado($ruta_raiz);
    $expdata = $objExpe->consultarExpediente();
    // El radicado  Cierre 
    $dataFun=$rad->traerDatosNuevo(2,$expdata['USUA_DOC_RESPONSABLE']);
    if($dataFun['error']!=""){
	die("<span style='font-color:red;'><b>El responsable del expediente esta inactivo, favor cambielo para el proceso de cierre de expediente.</b></span>");
    }
    foreach($dataFun as $key =>$value)
        $$key=$value;
    $pais2=$dataFun['pais'];
    $remite="$param2 $param3 $param4";
    $rad->setRadiDepeRadi($_SESSION['dependencia']);
    $rad->setTipRad(3);
    $rad->setDependencia($tpDepeRad[3]);
    //Por omision su tipo documental es no definido
    $rad->setTdocCodi(0);
    $rad->setEespCodi(0);
    $rad->setCedula($param1);
    $rad->setNit($param1);
    $rad->setNombre_remitente($param2);
    $rad->setApellidos_remitente($param3.' '.$param4);
    $rad->setPrimer_apellidos($param3);
    $rad->setSeg_apellidos($param4);
    if($expCierre==2){
	$rad->setRaAsun("ACTA DE REAPERTURA DE EXPEDIENTE $numExpediente");
    }
    else{
	$rad->setRaAsun("ACTA DE CIERRE DE EXPEDIENTE $numExpediente");
    }
    $rad->setMuni($muni);
    $rad->setDepto($dpto);
    $rad->setPais($pais2);
    $rad->setContinente($conti);
    $rad->setDireccion_remitente($param5);
    $rad->setDignatario($param2);
    $rad->setUsuaCodi($_SESSION['codusuario']);
    $rad->setUsuaDoc($_SESSION['usua_doc']);
    $rad->setTdidCodi(1);
    $rad->setOem(0);
    $rad->setCiu(0);
    $rad->setDocFun($id);
    $rad->setRadiDepeActu(999);
    $rad->setRadiUsuaActu(2);
    $rad->setRadiUsuaActuDoc(999);
    $rad->setRolid($_SESSION['id_rol']);
    $rad->setTrteCodi(0);
    $rad->setRadiUsuaActurol(1);
    $rad->setDescAnex('');
    $rad->setRadiGuia('');
    $rad->setMrecCodi(10);
    $rad->setRadiNumeHoja(1);
    $rad->setRadiPath('NULL');
    $rad->setSgd_apli_codi(0);
    $rad->setRadiTipoDeri(0);
    $rad->setRadiNumeDeri(0);
    $rad->setRadi_asocc(0);
    $rad->setFechaOfic(date('d/m/Y'));
    $rad->setRadiCuentai('');
    $numerad=$rad->radicar();
    /*$log->setNumDocu($numerad);
    $log->setOpera('Radicacion de documento de '.$tpDescRad[2]);
    $log->registroEvento();*/
    $rad->setRadiNumeRadi($numerad);
    $rad->dir_drecciones();
    $objExpe->setNumRadicado($numerad);
    $objExpe->incluirExp($_SESSION['codusuario'], $_SESSION['usua_doc'], $_SESSION['dependencia']);
    $radicados[0]=$numerad;
    $tx=53;
    $observa='Incluir radicado en Expediente';
    $e=$objExpe->insertarHistoricoExp($radicados,$_SESSION['dependencia'],$_SESSION['codusuario'],$observa,$tx,0);
    if($expCierre==2){
	$objExpe->reabrirExp();
	$tx=59;
        $observa='Expediente Ha Sido Reabierto';
        $e=$objExpe->insertarHistoricoExp($radicados,$_SESSION['dependencia'],$_SESSION['codusuario'],$observa,$tx,0);
    }
    else{
	$objExpe->cerrarExp();
        $tx=58;
        $observa='Expediente Ha Sido Cerrado';
        $e=$objExpe->insertarHistoricoExp($radicados,$_SESSION['dependencia'],$_SESSION['codusuario'],$observa,$tx,0);
    }
    $ruta_bodega=$objExpe->actaExpCierre($_POST['observa'],$_SESSION['usua_nomb'],$_SESSION['depe_nomb'],$jefe_nomb,$numerad,$adm);
    $a=$rad->actuRadiPath($ruta_bodega);
    if(!$a){
	echo "Fallo imagen de documento.";
    }
    else{
	$ruta_bodega=$objExpe->actaExpCierre($_POST['observa'],$_SESSION['usua_nomb'],$_SESSION['depe_nomb'],$jefe_nomb,$numerad,$adm);
        $a=$rad->actuRadiPath($ruta_bodega);
    }
    /*$rad->setUsuaLogin($_SESSION['krd']);
    $anexCod=$rad->sandboxAnexo();*/
}
?>
<html>
    <head>
	<link rel="stylesheet" href="<?php echo $ruta_raiz?>/estilos/default/orfeo.css">
	<SCRIPT Language="JavaScript" SRC="<?php echo $ruta_raiz; ?>/js/common.js"></SCRIPT>
	<script>
	contenido_textarea = ""
         num_caracteres_permitidos = 300
         function valida_longitud(){
             num_caracteres = document.forms[0].observa.value.length
             if (num_caracteres > num_caracteres_permitidos){
                 document.forms[0].observa.value = contenido_textarea
             }
             else{
                 contenido_textarea = document.forms[0].observa.value
             }
             if (num_caracteres >= num_caracteres_permitidos){
                 document.forms[0].caracteres.style.color = "#ff0000";
             }
             else{
                 document.forms[0].caracteres.style.color = "#000000";
              }
              cuenta()
          }

	function validaForm(){
	    var obse=document.getElementById('observa').value;
	    var res=trim(obse);
	    if(res<20){
		alert('Falta la observacion '+res);
		document.forms[0].observa.focus;
		return false;
	    }
	}

          function cuenta(){
              var difer = num_caracteres_permitidos - document.forms[0].observa.value.length
                  document.forms[0].caracteres.value = difer
          }
	</script>
    </head>
    <body>
	<form method='post' onsubmit='return validaForm();'>
	<center>
	<table width="98%" cellspacing="5" cellpadding="0" border="0" class="borde_tab">
	    <tr>
		<td class='titulos4' align='center'><?php echo ($expCierre==2)?"Reabrir Expediente":"Cierre de Expediente"?> <?php echo $numExpediente?><input type='hidden' value='<?php echo $numExpediente?>' name='numeroExpediente' id='numeroExpediente'></td>
	    </tr>
	    <?php              if(!isset($_POST['subcierre'])){
             ?>
	    <tr>
		<td>
		    <center>
		    <table width="500" border="0" bgcolor="White" align="center">
			<tr bgcolor="White">
                            <td width="100">
                                <center>
                                    <img title="Tux Transaccion" alt="Tux Transaccion" src="<?php echo $ruta_raiz?>/imagenes/iconos/tuxTx.gif">
                                </center>
                            </td>
			    <td align=''>
				<span class="info">Digite el texto con las observaciones del cierre o reapertura del expediente.</span> 
                                            <textarea onkeyup="valida_longitud()" onkeydown="valida_longitud()" class="ecajasfecha" rows="3" cols="70" id="observa" name="observa"></textarea>
				<span class="info">Caracteres escritos</span>
				<input type="text" style="border: thin solid rgb(227, 232, 236); color: rgb(0, 0, 0);" size="4" name="caracteres" readonly>
			    </td>
			</tr>
		    </table>
		</center>
		</td>
	    </tr>
	    <tr><td align='center'><input type="submit" class="botones_largo" value='<?php echo ($expCierre==2)?"REABRIR EXPEDIENTE":"CERRAR EXPEDIENTE"?>' name='subcierre'></td>
	    </tr>
	    <?php }
            else{?>
                 <tr><td>Acta de <?php echo ($expCierre==2)?"reapertura":"cierre"?> <a href='<?php echo "$ruta_raiz/core/vista/image.php?nombArchivo=" . $encrypt->encriptar($ruta_bodega)?>'><?php echo $numExpediente?></a></td></tr>
	    <?php             }
            ?>
	    <tr><td>
<?php include "listaExpediente.php";
?>
	</td></tr>
	</table>
	</center>
	</form>
    </body>
</html>
