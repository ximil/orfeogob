<?php session_start();
date_default_timezone_set('America/Bogota');
foreach ($_GET as $key => $valor)
    $$key = $valor;
foreach ($_POST as $key => $valor)
    $$key = $valor;
$ruta_raiz = "../../../..";
if (isset($_SERVER['HTTP_X_FORWARD_FOR'])) {
    $proxy = $_SERVER['HTTP_X_FORWARD_FOR'];
} else
    $proxy = $_SERVER['REMOTE_ADDR'];
$REMOTE_ADDR = $_SERVER['REMOTE_ADDR'];
include_once $ruta_raiz . '/core/Modulos/Expediente/clases/expediente.php';
include_once $ruta_raiz . '/core/clases/file-class.php';
include_once $ruta_raiz . '/core/config/config-inc.php';
$encrypt = new file();
//print_r($_GET);
if(!$_GET['eo'])
    die('No tiene datos');
$datareturn=explode('/', $encrypt->desencriptar($_GET['eo']));
if($datareturn[1]!=date('Y-m-d'))
    die('No se encotraron datos');

$objExpe = new expediente($ruta_raiz);
$objExpe->setNumExp($datareturn[0]);
$dataExp = $objExpe->consultarExpediente();
//print_r($dataExp);
$numExp = $dataExp['SGD_EXP_NUMERO'];
$nom_expediente = $dataExp['SGD_EXP_NUMERO'];
//$faro = $dataExp['SGD_SEXP_FARO'];
//$matrix = $dataExp['SGD_SEXP_MATRIX'];
//SGD_SRD_CODIGO SGD_SBRD_CODIGO
$seriessdata = $objExpe->consultarSerieYSubserie($dataExp['SGD_SRD_CODIGO'], $dataExp['SGD_SBRD_CODIGO']);
$trd = "{$dataExp['SGD_SRD_CODIGO']}-{$seriessdata['serie']} / {$dataExp['SGD_SBRD_CODIGO']}-{$seriessdata['subserie']}";
$fechaInicio = $dataExp['SGD_SEXP_FECH'];
$responsable = $objExpe->consultarResponsable($dataExp['USUA_DOC_RESPONSABLE']);
//$objExpe->setNumExp("2014900030100001E");
$dataRad = $objExpe->consultarExpRadicados();
$datosRad = $dataRad['datos'];
//print_r($dataRad);
$dataAnex = $objExpe->consultarExpAnexos($dataRad['radi'], 'N');

/*
  echo "<hr>";
  print_r($dataAnex); */
?><html>
    <head>
        <link rel="stylesheet" href="<?php echo $ruta_raiz; ?>/estilos/cristal//orfeo.css" type="text/css">

    </head>
    <body>
        <table border="0" width="98%" class="borde_tab" align="center" cellspacing="1">
            <tbody><tr>                     
                    <td class="titulos4" colspan="4">EXPEDIENTE <?php echo $numExp; ?></td>
                </tr>
                <tr>
                    <td class="titulos5" > Nombre de Expediente</td>
                    <td class="leidos"><?php echo $nom_expediente; ?></td>
                    <td class="titulos5">Responsable:</td>
                    <td class="leidos"><?php echo $responsable; ?>  </td>
                </tr>
                <tr>
                    <td class="titulos5">TRD:</td>
                    <td class="leidos2"><?php echo $trd; ?></td>
                    <td class="titulos5" nowrap="">Fecha Inicio:</td>
                    <td class="leidos2"><?php echo $fechaInicio; ?></td>
                </tr>
                <tr class="timparr">
                    <td colspan="6" class="titulos5">
                        <br>
                        <table border="0" width="98%" class="borde_tab" id="myTable" class="tablesorter"  >

                            <thead><tr class="listado5">
                                    <td>&nbsp;</td>
                                    <th align="center">
                                        Radicado
                                    </th>
                                    <th align="center">
                                        Fecha Radicación / Doc
                                    </th>
                                    <th align="center">
                                        Tipo Documento
                                    </th>
                                    <th align="center" style="width: 250px">
                                        Asunto
                                    </th>
                                    <th align="center" style="width: 250px">
                                        Remitente
                                    </th>
                                </tr>
                            </thead> 
                            <tbody>
                                <?php                                 // $datosRad['RADI_PATH']
                                $listado = '';
                                $itr = '<tr >';
                                $itd = ' <td valign="baseline" class="listado1" style="white-space: pre-line; text-align: left"><span class="leidos">';
                                $itd2 = ' <td valign="baseline" class="listado5" style="white-space: pre-line; text-align: left">';
                                $ftd = '</span></td>';
                                $ftd2 = '</td>';
                                $ftr = '</tr>';
                                $imgg2 = '<img src="../../../../imagenes/iconos/docs_tree.gif" border="0">';
                                $imgg = '<img src="../../../../imagenes/menu.gif" border="0">';
                                $afrad = '';
                                $afrad3 = '</a>';
                                for ($index = 0; $index < count($datosRad); $index++) {
                                    $airad ='';
                                    if($datosRad[$index]['RADI_PATH']){
                                    $rutaUrl = $ruta_raiz . "/core/vista/image.php?nombArchivo=" . $encrypt->encriptar(RUTA_BODEGA . $datosRad[$index]['RADI_PATH']);
                                    $airad = "<a href='$rutaUrl' target='_blank'>". $datosRad[$index]['RADI_NUME_RADI'];
                                    $afrad ='';
                                    }
                                    else{      $afrad = $datosRad[$index]['RADI_NUME_RADI'];}
                                    $listado.=$tr . $itd . $imgg . $ftd . $itd . $airad .  $afrad . $ftd;
                                    $listado.=$itd . $datosRad[$index]['FECHA_RAD'] . $ftd;
                                    $listado.=$itd . $datosRad[$index]['SGD_TPR_DESCRIP'] . $ftd;
                                    $listado.=$itd . $datosRad[$index]['RA_ASUN'] . $ftd;
                                    $listado.=$itd . $datosRad[$index]['REMITENTE'] . $ftd . $ftr;
                                    $respAnex = $dataAnex[$datosRad[$index]['RADI_NUME_RADI']];
                                    // echo count($respAnex);
                                    for ($aN = 1; $aN <= count($respAnex); $aN++) {
                                        $rutaAnex= substr ( trim ( $datosRad[$index]['RADI_NUME_RADI'] ), 0, 4 ) . "/" . substr ( trim ( $datosRad[$index]['RADI_NUME_RADI'] ), 4, 3 ) . "/docs/";
                                        $rutaUrl2 = $ruta_raiz . "/core/vista/image.php?nombArchivo=" . $encrypt->encriptar(RUTA_BODEGA .$rutaAnex. $respAnex[$aN]['ANEX_NOMB_ARCHIVO']);
                                        $aianex = "<a href='$rutaUrl2' target='_blank'>". substr($respAnex[$aN]['ANEX_CODIGO'], -5).$afrad3;
                                        $listado.=$tr . $itd2 . $ftd2 . $itd2 . $imgg2 .  $aianex . $ftd2;
                                        $listado.=$itd2 . $respAnex[$aN]['ANEX_FECH_ANEX'] . $ftd2;
                                        $listado.=$itd2 . $respAnex[$aN]['SGD_TPR_DESCRIP'] . $ftd2;
                                        $listado.=$itd2 . $respAnex[$aN]['ANEX_DESC'] . $ftd2;
                                        $listado.=$itd2 . $respAnex[$aN]['REMITENTE'] . $ftd2 . $ftr;
                                    }
                                }

                                echo $listado;
                                ?>

                            </tbody></table>
                    </td>
                </tr>

            </tbody></table>
    </body></html>
