<?php session_start(); date_default_timezone_set('America/Bogota'); date_default_timezone_set('America/Bogota');
foreach ($_GET as $key => $valor)  $$key = $valor;
foreach ($_POST as $key => $valor)  $$key = $valor;
$krd = $_SESSION["krd"];
$dependencia = $_SESSION["dependencia"];
$usua_doc = $_SESSION["usua_doc"];
$codusuario = $_SESSION["codusuario"];
$id_rol = $_SESSION["id_rol"];
$tpDepeRad=$_SESSION['tpDepeRad'];
$ruta_raiz="../../../..";
include_once "$ruta_raiz/core/Modulos/Expediente/clases/expediente.php";
$exp=new expediente($ruta_raiz);
$url="$ruta_raiz/core/Modulos/Expediente/vista/operMainExp.php";
?>
<html>
    <head>
	<link rel="stylesheet" href="<?php echo $ruta_raiz?>/estilos/default/orfeo.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $ruta_raiz?>/estilos/desvanecido.css">
	<style>
    #tblExpMasivos
    {
        width: 400px;
        margin-right:auto;
	margin-left:auto;
    }
    .tdspace
    {
        height: 20px;
        background-color: #e0e6e7;
    }
    #tblresultExpMas
    {
        width: 600px;
        margin-right:auto;
	margin-left:auto;
        /*display:none;*/
    }
    .titulos6 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	font-style: normal;
	font-weight: bolder;
	color: #000000;
	background-color: #e0e6e7;
	text-indent: 5pt;
	text-align: left;
    }
</style>
	<script language="JavaScript" src="<?php echo $ruta_raiz?>/js/common.js"></script>
        <script src="<?php echo $ruta_raiz?>/js/jquery-1.10.2.js"></script>
	<script>
	function overWindow(script){
            if($("#lightbox").size()==0){
            var theLightbox = $('<div id="lightbox"></div>');
            var theShadow = $('<div id="lightbox-shadow" onclick="cerrar2();"></div>');
                $('body').append(theShadow);
                $('body').append(theLightbox);
            }
            $("#lightbox").empty();
            $.get(script, function(data){
                $("#lightbox").append(data);
            });
            $('#lightbox').css('top', 100 + 'px');
            // display the lightbox
            $('#lightbox').show();
            $('#lightbox-shadow').show();
        }
	function cerrar2(){
           $('#lightbox').hide();
           $('#lightbox-shadow').hide();
           $('#lightbox').empty();
        }
	function validarNumExpediente(){
	    var numeroExpediente=$.trim($("#numeroExpediente").val());
	    var formData="action=valExpRad&numeroExpediente="+numeroExpediente;
	    if(!isNaN(numeroExpediente.substr(0,16)) && numeroExpediente.substr(-1)=='E'){
		$.ajax({
                    url : "<?php echo $url?>",
                    type: "POST",
                    data: formData
                })
            	.done(function(data){
		    var resp=$(data).filter("#respexp").text();
                    if(resp=='ok'){
			document.formC.submit();
		    }
		    else{
			alert(resp);
		    }
            	})
            	.fail(function(){
                    alert("Fallo el ajax");
            	});
	    }
	    else{
		alert('Numero de expediente no valido');
	    }
	}
	function action(exp){
	    var numeroExpediente=$("#numeroExpediente").val(exp);
	    $("#numeroExpediente").prop("readonly",true);
	    $("#t1").hide();
	    $("#t2").show();
	    cerrar2();
	}
	</script>
    </head>
    <body>
	<form action='cierreExp.php' method='post' id='formC' name='formC'>
	<table class="borde_tab" cellspacing="1" cellpadding="0" id="tblExpMasivos">
            <tr>
		<td height="15" class="titulos2" colspan="2"><?php echo (isset($cierre))?"ADMINISTRAR EXPEDIENTE":"INCLUIR EN  EL EXPEDIENTE"?></td>
            </tr>
	    <tr>
		<td class='info' colspan=2>Si tiene conocimiento ingrese el número del expediente o dar clic en el botón Búsqueda para realizar la acci&oacute;n correspondiente</td>
	    </tr>
            <tr>
            	<td class="titulos5" align="left" nowrap>N&uacute;mero de Expediente </td>
            	<td class="titulos5" align="left">
            	   <input type="text" name="numeroExpediente" id="numeroExpediente" size="30">
                </td>
            </tr>
            <tr>
                <td class="tdspace" colspan="2">&nbsp;</td>
            </tr>
            <tr style='display:table-row;' id='t1'>
                <td class="titulos5" align="center" colspan=2>
                <center><input name="actualizar" type="button" class="botones_funcion" id="envia23" onClick="overWindow('busqExp.php');"value=" Busqueda ">
                    <input name="btnIncluirExp" type="button" class="botones_funcion" id="btnIncluirExp" onClick="validarNumExpediente();" value="Ver Exp.">
                </center></td></tr>
	    <tr style='display:none;' id='t2'>
		<td class="titulos5" align="center" colspan=2><input name="btnIncluirExp" type="button" class="botones_funcion" id="btnIncluirExp" onClick="validarNumExpediente();" value="Cerrar Exp"></td>
            </tr>
        </table>
	</form>
    </body>
</html>
