<?php 
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of modeloExpediente
 *
 * @author hnino
 * 
 * 
 */
if (!$ruta_raiz)
    $ruta_raiz = '../../../../';
include_once "$ruta_raiz/core/clases/ConnectionHandler.php";

//TODO - Insert your code here



class modeloExpediente {

    //put your code here
    private $link;

    function __construct($ruta_raiz) {

        $db = new ConnectionHandler("$ruta_raiz");
        $db->conn->SetFetchMode(ADODB_FETCH_NUM);
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        $this->link = $db;
         if ($_SESSION['test'] == 1)
        $this->link->conn->debug = true;
    }

    /*     * Cambnio de titulo expediente *** */

    function modFaro($numExpediente, $titulo) {
        $sql = "update SGD_SEXP_SECEXPEDIENTES SET SGD_SEXP_faro='$titulo' WHERE SGD_EXP_NUMERO ='$numExpediente' ";
        $rs = $this->link->query($sql);
    }

    function modMatrix($numExpediente, $titulo) {
        $sql = "update SGD_SEXP_SECEXPEDIENTES SET SGD_SEXP_matrix='$titulo' WHERE SGD_EXP_NUMERO ='$numExpediente' ";
        $rs = $this->link->query($sql);
    }

    function modAsunto($numExpediente, $titulo) {
        $sql = "update SGD_EXP_EXPEDIENTE SET SGD_EXP_ASUNTO='$titulo' WHERE SGD_EXP_NUMERO ='$numExpediente' ";
        $rs = $this->link->query($sql);
	return $titulo;
    }

    function crearExpediente($numExpediente, $radicado, $depe_codi, $usua_codi, $usua_doc, $usuaDocExp, $codiSRD, $codiSBRD, $expOld = null, $fechaExp = null, $codiPROC = null, $arrParametro = null, $matrix = null, $faro = null) {
        $p = 1;
        // Valida que $arrParametro contenga un arreglo
        if (is_array($arrParametro)) {
            foreach ($arrParametro as $orden => $datoParametro) {
                $coma = ", ";
                if ($p == count($arrParametro)) {
                    $coma = "";
                }
                $campoParametro .= "SGD_SEXP_PAREXP" . $orden . $coma;
                $valorParametro .= "'" . $datoParametro . "'" . $coma;
                $p++;
            }
        }
        $estado_expediente = 0;
        $query = "select SGD_EXP_NUMERO from SGD_SEXP_SECEXPEDIENTES WHERE SGD_EXP_NUMERO='$numExpediente'";
        // $this->db->conn->debug = true;
        if ($expOld == "false") {
            $rs = $this->Link->conn->Execute($query);
            $trdExp = substr("00" . $codiSRD, -2) . substr("00" . $codiSBRD, -2);
            $anoExp = substr($numExpediente, 0, 4);
            if ($expManual == 1) {
                $secExp = $this->secExpediente($dependencia, $codiSRD, $codiSBRD, $anoExp);
            } else {
                $secExp = substr($numExpediente, 11, 5);
            }
            $consecutivoExp = substr("00000" . $secExp, -5);
            $numeroExpediente = $anoExp . $dependencia . $trdExp . $consecutivoExp;
        } else {
            $secExp = "0";
            $consecutivoExp = "00000";
            $anoExp = substr($numExpediente, 0, 4);
        }
        if ($rs->fields["SGD_EXP_NUMERO"] == $numExpediente) {
            return 0;
        } else {
            $fecha_hoy = Date("Y-m-d");
            //echo $fechaExp;
            if (!$fechaExp)// $fechaExp = $fecha_hoy;
                $sqlFechaHoy = $this->db->conn->OffsetDate(0, $this->db->conn->sysTimeStamp);
            else
                $sqlFechaHoy = $this->db->conn->DBDate($fechaExp);
            if (!$codiPROC)
                $codiPROC = "0";
            if (!$secExp)
                $secExp = 1;
            //$queryDel = "DELETE FROM SGD_SEXP_SECEXPEDIENTES WHERE SGD_EXP_NUMERO='$numExpediente'";
            //$this->db->conn->query($queryDel);

            $query = "insert into SGD_SEXP_SECEXPEDIENTES(SGD_EXP_NUMERO   ,SGD_SEXP_FECH      ,DEPE_CODI   ,USUA_DOC   ,SGD_FEXP_CODIGO,SGD_SRD_CODIGO,SGD_SBRD_CODIGO,SGD_SEXP_SECUENCIA, SGD_SEXP_ANO, USUA_DOC_RESPONSABLE, SGD_PEXP_CODIGO,SGD_SEXP_MATRIX,SGD_SEXP_FARO";
            if ($campoParametro != "") {
                $query .= ", $campoParametro";
            }
            $query .= " )";
            $query .= " VALUES ('$numExpediente'," . $sqlFechaHoy . " ,'$depe_codi','$usua_doc',0              ,$codiSRD     ,$codiSBRD        ,'$secExp' ,$anoExp, $usuaDocExp, $codiPROC,'$matrix','$faro'";
            if ($valorParametro != "") {
                $query .= " , $valorParametro";
            }
            $query .= " )";
            if (!$rs = $this->db->conn->Execute($query)) {
                //echo '<br>Lo siento no pudo agregar el expediente<br>';
                echo "No se ha podido insertar el Expediente";
                return 0;
            } else {
                //echo "<br>Expediente Grabado Correctamente<br>";
                return $numExpediente;
            }
        }
    }

    function consultarExpediente($numExpediente) {
        $sql = "select s.* ,to_char(sgd_sexp_fech,'dd-mm-yyyy hh24:mi') as fecha from SGD_SEXP_SECEXPEDIENTES s WHERE SGD_EXP_NUMERO='$numExpediente' ";
        $rs = $this->link->query($sql);
        $datos['ERROR'] = '';
        if (!$rs->EOF) {
            foreach ($rs->fields as $key => $value) {
                $datos[strtoupper($key)] = $value;
            }
            $datos['expediente'] = $rs->fields["SGD_EXP_NUMERO"];
        } else {
            $datos['ERROR'] = 'No se encontro el Expediente';
        }
        return $datos;
    }

    function consultaExpAdicional($numExpediente){
	$sql ="select max(sgd_exp_titulo) as titulo, max(sgd_exp_asunto) as asunto from sgd_exp_expediente where sgd_exp_numero='$numExpediente'";
	$rs=$this->link->conn->Execute($sql);
	$titulo=$rs->fields['TITULO'];
	$asunto=$rs->fields['ASUNTO'];
	return array($titulo,$asunto);
    }

    function consultarResp($doc) {
        $sql = "select as usua_nomb from usuario WHERE usua_doc='$doc' ";
        $rs = $this->link->query($sql);
        $datos['ERROR'] = '';
        if (!$rs->EOF) {
            $datos = $rs->fields["USUA_NOMB"];
        } else {
            $datos['ERROR'] = 'No se encontro el responsable';
        }
        return $datos;
    }

    function consultarSerieySub($serie, $sub) {
        $sql = "select s.sgd_srd_descrip as nserie,sb.sgd_sbrd_descrip as nsubserie
                from sgd_sbrd_subserierd sb,sgd_srd_seriesrd s 
                where  s.sgd_srd_codigo=sb.sgd_srd_codigo and sb.sgd_sbrd_codigo=$sub and s.sgd_srd_codigo=$serie ";
        $rs = $this->link->query($sql);
        //$datos['ERROR'] = '';
        if (!$rs->EOF) {
            $datos['subserie'] = $rs->fields["NSUBSERIE"];
            $datos['serie'] = $rs->fields["NSERIE"];
        } else {
            $datos['ERROR'] = 'No se encontro la serie';
        }
        return $datos;
    }

    function consultarExpRadicados($numExpediente) {
        $sql = "select r.*, c.sgd_tpr_descrip, TO_CHAR(a.RADI_FECH_RADI, 'DD-MM-YYYY HH24:MI') as FECHA_RAD , a.RADI_CUENTAI, a.RA_ASUN , a.RADI_PATH , a.radi_nume_radi as RADI_NUME_RADI, sgd_dir_nomremdes as remitente from sgd_exp_expediente r, radicado a, SGD_TPR_TPDCUMENTO c, sgd_dir_drecciones g where r.sgd_exp_numero='$numExpediente' and g.radi_nume_radi=a.radi_nume_radi and g.sgd_dir_cpcodi is null and r.radi_nume_radi=a.radi_nume_radi and a.tdoc_codi=c.sgd_tpr_codigo AND r.SGD_EXP_ESTADO <> 2 order by a.radi_fech_radi desc ";
        $rs = $this->link->query($sql);
       // $datos['ERROR'] = '';
        if (!$rs->EOF) {
            $i = 0;
            $x = 0;
            while (!$rs->EOF) {
                foreach ($rs->fields as $key => $value) {
                    $datos[$i][strtoupper($key)] = $value;
                }
                if ($x == 0) {
                    $datosR = $rs->fields["RADI_NUME_RADI"];
                    $x = 2;
                } else {
                    $datosR .= ',' . $rs->fields["RADI_NUME_RADI"];
                }
                $i++;
                $rs->MoveNext();
            }
        } else {
            $datos['ERROR'] = 'No se encontro radidcados en el Expediente';
        }
        $dat['datos']=$datos;
        $dat['radi']=$datosR;
        return $dat;
    }
    function consultarExpAnexos($rad,$estado='N') {
        $sql = "select a.*,to_char(a.anex_fech_anex,'dd-mm-yyyy hh24:mi') as fecha from anexos a where ANEX_RADI_NUME in ($rad) and anex_borrado <> 'S' order by anex_numero  ";
        $rs = $this->link->query($sql);
       // $datos['ERROR'] = '';
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                $radi=$rs->fields["ANEX_RADI_NUME"];
                foreach ($rs->fields as $key => $value) {
                    $datos[$radi][$rs->fields["ANEX_NUMERO"]][strtoupper($key)] = $value;
                }    
                $rs->MoveNext();
            }
        } else {
            $datos['ERROR'] = 'No se encontro radidcados en el Expediente';
        }
        return $datos;
    }
    

    function consultarExpediente2($numExpediente) {
        
        $sql = "select SGD_EXP_NUMERO from SGD_SEXP_SECEXPEDIENTES 
                WHERE  sgd_exp_numero='$numExpediente'";
        $rs = $this->link->query($sql);
        //$datos['ERROR'] = '';
        if (!$rs->EOF) {
            $datos = $rs->fields["SGD_EXP_NUMERO"];
        } else {
            $datos = 'error';
        }
        return $datos;
    }

    function estadoRadicadoExpediente($numRadicado, $numExpediente) {
        global $ruta_raiz;
        $db = new ConnectionHandler($ruta_raiz, 'WS');
        $salida = - 1;
        $consulta = "SELECT SGD_EXP_ESTADO FROM SGD_EXP_EXPEDIENTE WHERE RADI_NUME_RADI={$numRadicado} AND SGD_EXP_NUMERO='{$numExpediente}'";
        $resultado = $this->link->query($consulta);
        if ($resultado && !$resultado->EOF) {
            $salida = $resultado->fields ['SGD_EXP_ESTADO'];
        }
        return $salida;
    }

    function agregarExp($numExpediente, $numRadicado, $usua_codi, $usua_doc, $usua_depe) {
        $fecha = $this->link->conn->OffsetDate(0, $this->link->conn->sysTimeStamp);
        $consulta = "INSERT INTO SGD_EXP_EXPEDIENTE 
            (SGD_EXP_NUMERO,RADI_NUME_RADI,SGD_EXP_FECH,SGD_EXP_ESTADO,USUA_CODI,USUA_DOC,DEPE_CODI)
           VALUES ('{$numExpediente}',{$numRadicado},{$fecha},0,$usua_codi,'$usua_doc',$usua_depe)";
        $resultado = $this->link->query($consulta);
    }

    function validarExp($numExpediente, $numRadicado) {
        $consulta = "select SGD_EXP_NUMERO,RADI_NUME_RADI from SGD_EXP_EXPEDIENTE where
            SGD_EXP_NUMERO='$numExpediente' and RADI_NUME_RADI=$numRadicado";
        $resultado = $this->link->query($consulta);
        $datos['ERROR'] = '';
        if (!$rs->EOF) {
            $datos['expediente'] = $resultado->fields["SGD_EXP_NUMERO"];
            $datos['radicado'] = $resultado->fields["RADI_NUME_RADI"];
        } else {
            $datos['ERROR'] = 'No se encontro el Expediente';
        }
        return $datos;
    }

    /**
     * FUNCION QUE INSERTA HISTORICO DE EXPEDIENTES
     *
     * @radicados   array Arreglo de radicados
     * @dependencia	int   Dependencia que realiza la transaccion
     * @depeDest    int   Dependencia destino
     * @codUsuario  int   Documento del usuario que realiza la transaccion
     * @tipoTx      int   Tipo de Transaccion
     * @return void
     *
     */
    function insertarHistoricoExp($numeroExpediente, $radicados, $dependencia, $codUsuario, $observacion, $tipoTx, $codigoFldExp) {
        //Arreglo que almacena los nombres de columna
        #==========================
        //$this->db->debug = true;
        # Busca el Documento del usuario Origen
        $ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
        $this->link->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        $sql = "SELECT u.USUA_DOC, u.USUA_LOGIN FROM USUARIO u, sgd_urd_usuaroldep urd
				WHERE
					urd.DEPE_CODI=$dependencia and urd.usua_codi=u.USUA_CODI
					AND u.USUA_CODI=$codUsuario";
        # Busca el usuairo Origen para luego traer sus datos.
        $rs = $this->link->conn->Execute($sql);
        $usDoc = $rs->fields["USUA_DOC"];
        $usuLogin = $rs->fields["USUA_LOGIN"];

        $record = array(); # Inicializa el arreglo que contiene los datos a insertar
        if ($radicados) {
            foreach ($radicados as $noRadicado) {
                # Asignar el valor de los campos en el registro
                # Observa que el nombre de los campos pueden ser mayusculas o minusculas

                $record["SGD_EXP_NUMERO"] = "'" . $numeroExpediente . "'";
                $record["SGD_FEXP_CODIGO"] = $codigoFldExp;
                $record["RADI_NUME_RADI"] = $noRadicado;
                $record["DEPE_CODI"] = $dependencia;
                $record["USUA_CODI"] = $codUsuario;
                $record["USUA_DOC"] = $usDoc;
                $record["SGD_TTR_CODIGO"] = $tipoTx;
                $record["SGD_HFLD_OBSERVA"] = "'$observacion'";
                $record["SGD_HFLD_FECH"] = $this->link->conn->OffsetDate(0, $this->link->conn->sysTimeStamp);
                if ($codigoArista) {
                    $record["SGD_FARS_CODIGO"] = $codigoArista;
                }
                # Mandar como parametro el recordset vacio y el arreglo conteniendo los datos a insertar
                # a la funcion GetInsertSQL. Esta procesara los datos y regresara un enunciado SQL
                # para procesar el INSERT.
                $insertSQL = $this->link->insert("SGD_HFLD_HISTFLUJODOC", $record, "true");
            }
            return ($radicados);
        }
    }

    function busqExpediente($numExp=null,$numRadicado=null,$exp_titulo=null,$exp_asunto=null){
	$param="select distinct(x.sgd_exp_numero) from sgd_sexp_secexpedientes x, sgd_exp_expediente y where x.sgd_exp_numero=y.sgd_exp_numero";
	if($numExp)
	    $param.=" and x.sgd_exp_numero like '%$numExp%' ";
	if($numRadicado)
	    $param.=" and cast(y.radi_nume_radi as char(18)) like '%$numRadicado%'";
	if($exp_asunto)
	    $param.=" and upper(y.sgd_exp_asunto) like '%".strtoupper($exp_asunto)."%'";
	if($exp_titulo)
            $param.=" and upper(y.sgd_exp_titulo) like '%".strtoupper($exp_titulo)."%'";
	
	$sql="select s.sgd_exp_numero,max(e.sgd_exp_titulo) as titulo,max(e.sgd_exp_asunto) as asunto, count(radi_nume_radi) as radicados,
	    max(e.sgd_exp_archivo) as estado_cierre from sgd_sexp_secexpedientes s, 
	    sgd_exp_expediente e where e.sgd_exp_numero=s.sgd_exp_numero and s.sgd_exp_numero in ($param) group by s.sgd_exp_numero";
	$rs=$this->link->conn->Execute($sql);
	if(!$rs->EOF){
	    $i=0;
	    while(!$rs->EOF){
		$retorno[$i]['exp']=$rs->fields['SGD_EXP_NUMERO'];
		$retorno[$i]['titulo']=$rs->fields['TITULO'];
		$retorno[$i]['asunto']=$rs->fields['ASUNTO'];
		$retorno[$i]['radicados']=$rs->fields['RADICADOS'];
		$retorno[$i]['estado_cierre']=$rs->fields['ESTADO_CIERRE'];
		$i++;
		$rs->MoveNext();
	    }
	}
	else{
	    $retorno['error']="No hay resultados de busqueda";
	}
	return $retorno;
    }

    function valArchVExp($numexp){
        $sql="select r.radi_nume_radi from radicado r, sgd_exp_expediente e where e.sgd_exp_numero='$numexp' and e.sgd_exp_estado<>2 and
            r.radi_nume_radi=e.radi_nume_radi and r.radi_usua_actu<>2";
        $rs=$this->link->conn->Execute($sql);
        if(!$rs->EOF){
            $i=0;
            while(!$rs->EOF){
                $radicados[$i]=$rs->fields['RADI_NUME_RADI'];
                $i++;
                $rs->MoveNext();
            }
            $retorno='Radicados '.implode(',',$radicados).' aun estan en tramite';
        }
        else{
	    $sql1="select sgd_exp_numero from sgd_sexp_secexpedientes where sgd_exp_numero='$numexp'";
	    $rs1=$this->link->conn->Execute($sql1);
	    if(!$rs1->EOF){
            	$retorno='ok';
	    }
	    else{
		$retorno="ERROR: El expediente $numexp no existe";
	    }
        }
        return $retorno;
    }

    function cerrarExp($numexp){
	$dat=date('Y-m-d');
	$usql="update sgd_exp_expediente set sgd_exp_archivo='2',SGD_EXP_FECHFIN=to_date('$dat','yyyy-mm-dd') where sgd_exp_numero='$numexp'";
	$rs=$this->link->conn->Execute($usql);
	if($rs){
	    return true;
	}
	return false;
    }

    function reabrirExp($numexp){
	$dat=date('Y-m-d');
        $usql="update sgd_exp_expediente set sgd_exp_archivo='1',SGD_EXP_FECHFIN=to_date('$dat','yyyy-mm-dd') where sgd_exp_numero='$numexp'";
        $rs=$this->link->conn->Execute($usql);
        if($rs){
            return true;
        }
        return false;
    }

    function solEstadoExp($numexp){
	$sql="select max(sgd_exp_archivo) as estado from sgd_exp_expediente where sgd_exp_numero='$numexp' and sgd_exp_estado<>2";
	$rs=$this->link->conn->Execute($sql);
	$retorno=$rs->fields['ESTADO'];
	return $retorno;
    }

    function expedienteConsulta2($numexp){
	$sql="select r.radi_nume_radi, r.radi_nume_folios as folios, e.sgd_exp_fech ,d.sgd_dir_nomremdes,ra_asun,srd.sgd_srd_codigo||'-'||srd.sgd_srd_descrip as serie,
        sbrd.sgd_sbrd_codigo||'-'||sbrd.sgd_sbrd_descrip as subserie,count(a.anex_codigo) as no_anexos,sum(a.anex_folios_dig)
        as folios_anexos, max(e.sgd_exp_titulo) as titulo, max(e.sgd_exp_asunto) as asunto from sgd_exp_expediente e, sgd_sexp_secexpedientes s,
        sgd_srd_seriesrd srd, SGD_SBRD_SUBSERIERD sbrd,
        sgd_dir_drecciones d, radicado r
        left outer join anexos a on (r.radi_nume_radi=a.anex_radi_nume and a.anex_borrado='N')
        where e.sgd_exp_estado<>2 and r.radi_nume_radi=e.radi_nume_radi and d.radi_nume_radi=r.radi_nume_radi and e.sgd_exp_numero='$numexp'
        and s.sgd_exp_numero=e.sgd_exp_numero and 
        srd.sgd_srd_codigo=s.sgd_srd_codigo and sbrd.sgd_srd_codigo=srd.sgd_srd_codigo and s.sgd_sbrd_codigo=sbrd.sgd_sbrd_codigo group by r.radi_nume_radi,
	r.radi_nume_folios,d.sgd_dir_nomremdes,e.sgd_exp_fech,r.ra_asun, srd.sgd_srd_codigo||'-'||srd.sgd_srd_descrip,
        sbrd.sgd_sbrd_codigo||'-'||sbrd.sgd_sbrd_descrip order by e.sgd_exp_fech";
	$rs=$this->link->conn->Execute($sql);
	$i=0;
	$asunto='';
	$titulo='';
	$folios_total=0;
	$anexos=0;
	while(!$rs->EOF){
	    if($i==0){
		$serie=$rs->fields['SERIE'];
		$subserie=$rs->fields['SUBSERIE'];
	    }
	    if($rs->fields['ASUNTO']!=""){
		$asunto=$rs->fields['ASUNTO'];
	    }
	    if($rs->fields['TITULO']!=""){
                $titulo=$rs->fields['TITULO'];
            }
	    if($rs->fields['FOLIOS']!=""){
                $retorno[$i]['folios']=$rs->fields['FOLIOS'];
		$folios_total=$folios_total+$rs->fields['FOLIOS'];
            }
	    else{
		$retorno[$i]['folios']=0;
	    }
	    if($rs->fields['FOLIOS_ANEXOS']!=""){
                $retorno[$i]['folios_anexos']=$rs->fields['FOLIOS_ANEXOS'];
                $folios_total=$folios_total+$rs->fields['FOLIOS_ANEXOS'];
            }
            else{
                $retorno[$i]['folios_anexos']=0;
            }
	    $retorno[$i]['radicado']=$rs->fields['RADI_NUME_RADI'];
	    $retorno[$i]['no_anexos']=$rs->fields['NO_ANEXOS'];
	    $retorno[$i]['destinatario']=$rs->fields['SGD_DIR_NOMREMDES'];
	    $retorno[$i]['ra_asun']=$rs->fields['RA_ASUN'];
	    $anexos=$anexos+$rs->fields['NO_ANEXOS'];
	    $i++;
	    $rs->MoveNext();
	}
	return array($retorno,$titulo,$asunto,$folios_total,$anexos,$i,$serie,$subserie);
    }

   function consActaCierre($numexp){
	$sql="select * from (select h.radi_nume_radi,to_char(h.SGD_HFLD_FECH,'yyyy-mm-dd') as fecha,row_number() over(order by sgd_hfld_fech desc) as rowz from
    	    sgd_hfld_histflujodoc h where sgd_ttr_codigo=58 and sgd_exp_numero='$numexp') where rowz=1";
	$rs=$this->link->conn->Execute($sql);
	$retorno['numrad']=$rs->fields['RADI_NUME_RADI'];
	$retorno['fecha']=$rs->fields['FECHA'];
	return $retorno;
   }

}

?>
