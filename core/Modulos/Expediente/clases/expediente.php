<?php 
/**
 * archivo es la clase encargada de gestionar las operaciones de archivo
 * @author Hardy Deimont Niño  Velasquez
 * @name expediente
 * @version	1.0
 */
if (!$ruta_raiz)
    $ruta_raiz = '../../../..';
include_once "$ruta_raiz/core/Modulos/Expediente/modelo/modeloExpediente.php";

/**
 * Description of expediente
 *
 * @author hnino
 */
class expediente {

    public $ruta_raiz;
    private $modelo;
    private $numExp; // Almacena el nume del expediente
    private $numFaro;
    private $numMatrix;
    private $numRadicado;
    private $estado_expediente; // Almacena el estado 0 para organizacion y 1 para indicar ke ya esta clasificado fisicamente en archivo
    private $descSerie;
    private $descSubSerie;
    private $descTipoExp;
    private $descFldExp;
    private $codigoFldExp;
    private $exp_titulo;
    private $exp_asunto;
    private $exp_ufisica;
    private $exp_isla;
    private $exp_caja;
    private $exp_estante;
    private $exp_carpeta;
    private $exp_unicon;
    private $exp_archivo;
    private $exp_fechaIni;
    private $exp_fechaFin;
    private $exp_num_carpetas;
    private $expUsuaDoc;
    private $codiSRD;
    private $codiSBRD;
    private $codigoTipoDoc;
    private $expFechaCrea;
    private $numSubexpediente;
    private $asunto;

    public function getAsunto(){
	return $this->asunto;
    }

    public function setAsunto($asunto){
	$this->asunto = $asunto;
    }

    public function getNumRadicado() {
        return $this->numRadicado;
    }

    public function setNumRadicado($numRadicado) {
        $this->numRadicado = $numRadicado;
    }

    public function getExp_titulo() {
        return $this->exp_titulo;
    }

    public function setExp_titulo($exp_titulo) {
        $this->exp_titulo = $exp_titulo;
    }

    public function getExp_asunto() {
        return $this->exp_asunto;
    }

    public function setExp_asunto($exp_asunto) {
        $this->exp_asunto = $exp_asunto;
    }

    public function getExpUsuaDoc() {
        return $this->expUsuaDoc;
    }

    public function setExpUsuaDoc($expUsuaDoc) {
        $this->expUsuaDoc = $expUsuaDoc;
    }

    public function getCodiSRD() {
        return $this->codiSRD;
    }

    public function setCodiSRD($codiSRD) {
        $this->codiSRD = $codiSRD;
    }

    public function getCodiSBRD() {
        return $this->codiSBRD;
    }

    public function setCodiSBRD($codiSBRD) {
        $this->codiSBRD = $codiSBRD;
    }

    public function getNumExp() {
        return $this->numExp;
    }

    public function setNumExp($numExp) {
        $this->numExp = $numExp;
    }

    public function getNumFaro() {
        return $this->numFaro;
    }

    public function setNumFaro($numFaro) {
        $this->numFaro = $numFaro;
    }

    public function getNumMatrix() {
        return $this->numMatrix;
    }

    public function setNumMatrix($numMatrix) {
        $this->numMatrix = $numMatrix;
    }

    function __construct($ruta_raiz) {
        $this->ruta_raiz = $ruta_raiz;
        $this->modelo = new modeloExpediente($this->ruta_raiz);
    }

    function modFaro() {
        $rs = $this->modelo->modFaro($this->numExp, $this->numFaro);
        return $rs;
    }

    function modMatrix() {
        $rs = $this->modelo->modMatrix($this->numExp, $this->numMatrix);
        return $rs;
    }

    function modAsunto(){
	$rs = $this->modelo->modAsunto($this->numExp, $this->asunto);
        return $rs;
    }

    function crearExpediente($param) {
        
    }

    function consultarExpediente() {
        $rs = $this->modelo->consultarExpediente($this->numExp);
        return $rs;
    }

    function consultarResponsable($doc) {
        $rs = $this->modelo->consultarResp($doc);
        return $rs;
    }

    function consultarSerieYSubserie($serie,$sub) {
        $rs = $this->modelo->consultarSerieySub($serie,$sub);
        return $rs;
    }
    
    function consultarExpRadicados() {
        $rs = $this->modelo->consultarExpRadicados($this->numExp);
        return $rs;
    }
    function consultarExpAnexos($radicado,$tipo){
        $rs = $this->modelo->consultarExpAnexos($radicado,$tipo);
        return $rs;        
    }
    
    function consultaExpAdicional(){
	return $this->modelo->consultaExpAdicional($this->numExp);
    }

    function consultarExpediente2() {
        $rs = $this->modelo->consultarExpediente2($this->numExp);
        return $rs;
    }

    function estadoRadicadoExpediente() {
        $rs = $this->modelo->estadoRadicadoExpediente($this->numRadicado, $this->numExp);
        return $rs;
    }

    function incluirExp($usua_codi, $usua_doc, $usua_depe) {
        $this->modelo->agregarExp($this->numExp, $this->numRadicado, $usua_codi, $usua_doc, $usua_depe);
        $rs = $this->modelo->validarExp($this->numExp, $this->numRadicado);
        return $rs;
    }

    function insertarHistoricoExp($radicados, $dependencia, $codUsuario, $observacion, $tipoTx, $codigoFldExp) {
        $a = $this->modelo->insertarHistoricoExp($this->numExp, $radicados, $dependencia, $codUsuario, $observacion, $tipoTx, $codigoFldExp);
        return $a;
    }

    function busqExpediente($matVisual){
	$a=$this->modelo->busqExpediente($this->numExp,$this->numRadicado,$this->exp_titulo,$this->exp_asunto);
	$resultado="<table class=\"borde_tab\" width=\"100%\" cellspacing=\"1\" cellpadding=\"0\" border=\"0\">\n";
	$resultado.="<tr class='titulos4'><th colspan=5 style='text-align:center;'>Resultado b&uacute;queda</th></tr>\n";
	$resultado.="<tr class='titulo1'><th style='width:25%;'>N&uacute;mero Expediente</th><th style='width:25%;'>Titulo de Expediente</th><th style='width:25%;'>Asunto u Objeto</th><th style='width:25%;'>Radicados Incluidos</th><th>Acci&oacute;n</th></tr>\n";
	if(isset($a['error'])){
	    $resultado=$a['error'];
	}
	else{
	    for($i=0;$i<count($a);$i++){
		if($i%2==0){
		    $style=" class='listado50'";
		}
		else{
		    $style=" class='listado51'";
		}
		$link=($a[$i]['estado_cierre']==2)?"REABRIR":"CERRAR";
		$action=(in_array(substr($a[$i]['exp'],4,3),$matVisual))?"action('{$a[$i]['exp']}');":"noPermiso();";
		$resultado.="<tr$style><td>{$a[$i]['exp']}</td><td>{$a[$i]['titulo']}</td><td>{$a[$i]['asunto']}</td><td>{$a[$i]['radicados']}</td><td><input type='button' class='botones' onclick=\"$action\" value='$link'></td></tr>\n";
	    }
	    $resultado.="</table>";
	}
	return $resultado;
    }

    function valArchVExp($matVisual){
	$retorno="<div id='respexp'>";
	$numExpediente=$this->numExp;
	$a=$this->modelo->valArchVExp($this->numExp);
	if(in_array(substr($numExpediente,4,3),$matVisual) && $a=='ok'){
	    $retorno.=$a;
	}
	elseif($a=='ok'){
	    $retorno.="No tiene permiso para acceder a este expediente";
	}
	else{
	    $retorno.=$a;
	}
	$retorno.="</div>";
	return $retorno;
    }

    function cerrarExp(){
	$this->modelo->cerrarExp($this->numExp);
    }

    function reabrirExp(){
	$this->modelo->reabrirExp($this->numExp);
    }

    function actaExpCierre($observa,$nomus,$depus,$nomb_jefe,$radicado,$adm){
	if($adm==2){
	     $action="REAPERTURA";
	     $action2="Reabrir";
	     $obse="Observaci&oacute;n de reapertura";
             $dataC=$this->modelo->consActaCierre($this->numExp);
	     $trextra="<tr><td style='font-weight: bold;'>Acta de cierre:</td><td>{$dataC['numrad']} {$dataC['fecha']}</td></tr>";
	}
	else{
	    $action="CIERRE";
	    $action2="Cerrar";
            $obse="Observaci&oacute;n de cierre";
	    $trextra="";
	}
	list($resultado,$titulo,$asunto,$folios_total,$anexos,$rads,$serie,$subserie)=$this->modelo->expedienteConsulta2($this->numExp);
	$numExpediente=$this->numExp;
	$mes=date('n');
	$sem=date('w');
	$ano=date('Y');
	$dia=date('d');
	$dsemana=array('Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado');
	switch($mes){
           case 1:
                $nmes='Enero';
                break;
           case 2:
                $nmes='Febrero';
                break;
           case 3:
                $nmes='Marzo';
                break;
           case 4:
                $nmes='Abril';
                break;
           case 5:
                $nmes='Mayo';
                break;
           case 6:
                $nmes='Junio';
                break;
           case 7:
                $nmes='Julio';
                break;
           case 8:
                $nmes='Agosto';
                break;
           case 9:
                $nmes='Septiembre';
                break;
           case 10:
                $nmes='Octubre';
                break;
           case 11:
                $nmes='Noviembre';
                break;
           case 12:
                $nmes='Diciembre';
                break;
        }
        $head="<html lang='es'><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
		<style rel='stylesheet' type='text/css'>
		body {
 		   font-family: Arial, Helvetica, sans-serif;
		   font-size: 13px;
		}
		.cuerpo {
		    text-align:justify;
		}
		.titulo{
		    text-align:center;
		}
		.firma{
		    text-align:justify;
		}
		</style>
		</head>
            <body>
	<div class='titulo'>
	<b>ACTA $action DE EXPEDIENTE</b><br><b>RADICADO No. $radicado</b><br>
	<b>GRUPO DE GESTI&Oacute;N DOCUMENTAL</b>
	</div>
	<div class='cuerpo'>
	<p>En cumplimiento a lo establecido en el Acuerdo No. 002 del 14 de marzo de 2014 expedido por el <b>Archivo General de la Naci&oacute;n</b>, en el cual se establecen los criterios b&aacute;sicos para creaci&oacute;n, conformaci&oacute;n, control y consulta de los expedientes de archivo, par&aacute;grafo de Art&iacute;culo 10° <b>CIERRE DEL EXPEDIENTE</b>, el cual establece que:</p>
	<ol style='list-style-type: lower-alpha;text-align: justify;'>
	<li>Cierre Administrativo: Una vez finalizadas las actualizaciones y resuelto el tr&aacute;mite o procedimiento administrativo que dio origen.</li>
	<li>Cierre Definitivo: Una vez superada la vigencia de las actualizaciones y cumplido el tiempo de prescripci&oacute;n de acciones administrativas, fiscales o legales. Durante esta fase se pueden agregar nuevos documentos.</li>
	</ol>
	<p><b>$obse</b></p>
	<p>$observa</p>
	</div>
	<br>
	<p>1.- Descripción del Expediente a $action2:</p>
	";
	$titulo=($titulo=="")?"(no tiene)":$titulo;
	$asunto=($asunto=="")?"(no tiene)":$asunto;
	$tabla1="<table width='99%' border='0'  cellpadding='0' cellspacing='0' style='border-collapse: collapse;font-style:normal;font-stretch: ultra-expanded;vertical-align:inherit;text-align:left;'>
	<tr><td width='30%' style='font-weight: bold;'>No. Expediente:</td><td width='70%'>$numExpediente</td></tr>
	<tr><td style='font-weight: bold;'>TRD:</td><td>$serie/$subserie</td></tr>
	<tr><td style='font-weight: bold;'>Titulo o Nombre:</td><td>$titulo</td></tr>
        <tr><td style='font-weight: bold;'>Asunto u Objeto:</td><td>$asunto</td></tr>
	<tr><td style='font-weight: bold;'>Funcionario Realizo Cierre:</td><td>$nomus</td></tr>
        <tr><td style='font-weight: bold;'>&Aacute;rea solicitante:</td><td>$depus</td></tr>
	<tr><td style='font-weight: bold;'>No. Radicados:</td><td>$rads</td></tr>
	<tr><td style='font-weight: bold;'>No. Anexos:</td><td>$anexos</td></tr>
	<tr><td style='font-weight: bold;'>Folios digitalizados:</td><td>$folios_total Folios</td></tr>$trextra
	</table><br>";
        $contenidoh2 = "<div class='titulo'><b>Detalles de radicados</b></div>
<table width='99%' border='1'  cellpadding='0' cellspacing='0' style='border-collapse: collapse;font-style:normal;font-size:8;text-align:center;letter-spacing: 0.1em;font-stretch: ultra-expanded;vertical-align:inherit;'>
<tr bgcolor='#999999'><td width='3%'>#</td><td width='15%'>No. Radicado</td><td width='37%'>Asunto</td><td width='15%'>Destinatario</td><td width='15%'>Total<br>Anexos</td><td width='15%'>Total<br>Digitalizado</td></tr>";
	for($i=0;$i<count($resultado);$i++){
	    $o=$i+1;
	    $folios=$resultado[$i]['folios_anexos']+$resultado[$i]['folios'];
	    $contenidoh2.="<tr><td>$o</td><td>{$resultado[$i]['radicado']}</td><td>{$resultado[$i]['ra_asun']}</td><td>{$resultado[$i]['destinatario']}</td><td>{$resultado[$i]['no_anexos']}</td><td>$folios</td></tr>\n";
	}
	 $contenidoh2.="</table>";
	$footer="<p>2.- Se deja copia de la presente acta en el expediente físico del área para el trámite respectivo de la organización física de los archivos.</p>
	   <br>
	   <div class='firma'><p>Se firma la presente el {$dsemana[$sem]}, $dia de $nmes de $ano</p><br>
	   _______________________________________________________________<br>
	   $nomb_jefe<br>
	   Jefe de $depus<br>
	   Imprenta Nacional de Colombia</div>
	   </body></html>";
	$html=$head.$tabla1.$contenidoh2.$footer;
	include_once("../../../../extra/MPDF57/mpdf.php");
        $mpdf=new mPDF('es-ES','Letter',0, '', 18.7, 18.7, 45, 30.7, 8, 8);
	$real_header="<table style='border-collapse:collapse;border-spacing:0;padding:0;width:100%;'>
		<tr><td valign='top' align='left'><img src='http://{$_SERVER['SERVER_NAME']}/orfeo/imagenes/imprenta/cabezote_carta_inc.png'></td>
		<td valign='top' align='right'><img src='http://{$_SERVER['SERVER_NAME']}/orfeo/imagenes/imprenta/cabezote_carta_gob.png'></td></tr></table>";
	$real_footer="<table style='border-collapse:collapse;border-spacing:0;padding:0;width:100%;font-family: Arial, Helvetica, sans-serif;font-size:8'>
                <tr><td align='left' valign='middle'>Imprenta Nacional de Colombia<br>
		Carrera 66 No. 24-09<br>
		Tel: (57 1) 4578000<br>
		<span style='font-weight:bold;'>www.imprenta.gov.co</span><br>
		e-mail: correspondencia@imprenta.gov.co</td>
                <td valign='top' align='right'><img src='http://{$_SERVER['SERVER_NAME']}/orfeo/imagenes/imprenta/piefot.png'></td></tr>
		</table>";
        //$mpdf->SetFooter('PLANILLA DE DEVOLUCION DE ENVIOS A LAS DEPENDENCIAS|Pagina {PAGENO} de {nb}');
	$mpdf->PDFA=true;
	$mpdf->PDFAauto = true;
	$mpdf->SetHTMLHeader($real_header);
	$mpdf->SetHTMLFooter($real_footer);
        $mpdf->WriteHTML($html);
	//$mpdf->showImageErrors = true;
	//$mpdf->debug=true;
        $arpdf_tmp = RUTA_BODEGA . "/pdfs/guias/Cierre$numExpediente.pdf";
        //salida
        //Copiar a la bodega
        $mpdf->Output($arpdf_tmp,'F');
	return $arpdf_tmp;
    }

    function solEstadoExp(){
	return $this->modelo->solEstadoExp($this->numExp);
    }
}

?>
