<?php 
/**
 * modeloTpDoc es la clase encargada de gestionar las operaciones y los datos basicos referentes a los tipos de documento
 * @author Hardy Deimont Niño  Velasquez
 * @name modeloTpDoc
 * @version	1.0
 */
if (!$ruta_raiz)
    $ruta_raiz = '../../../..';
//include_once "$ruta_raiz/include/db/ConnectionHandler.php";
include_once "$ruta_raiz/core/clases/ConnectionHandler.php";
include_once "$ruta_raiz/core/config/config-inc.php";

class modeloEstadisticas {

    public $link;
    public $depeCorresp;

    function __construct($ruta_raiz) {

        $depeCorresp = DPCORRES;
        $db = new ConnectionHandler("$ruta_raiz");
        $db->conn->SetFetchMode(ADODB_FETCH_NUM);
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        //$db->conn->debug = true;
        $this->link = $db;
    }

    /**
     * @return consulta los datos de tpdocumentos.
     */
    function consultar($subserie, $depe, $serieid, $ano) {
        $isqlC = "select distinct (select distinct e.sgd_exp_titulo from sgd_exp_expediente e where sgd_exp_titulo is not null and e.sgd_exp_numero=exp.sgd_exp_numero ) titulo,sec.sgd_exp_numero , sec.sgd_sexp_ano
		from sgd_sexp_secexpedientes  sec,sgd_exp_expediente exp
		where
               
		sec.depe_codi=$depe and
		sec.sgd_srd_codigo=$serieid
		and sec.sgd_sbrd_codigo=$subserie
		and sgd_sexp_ano='$ano' and
		sec.sgd_exp_numero=exp.sgd_exp_numero
		order by sec.sgd_exp_numero asc";
        //and exp.sgd_exp_titulo is not null  sgd_exp_titulo is not null and 
        $rs = $this->link->query($isqlC);
        $i = 0;
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                $combi [$i] ["numero"] = $rs->fields ['SGD_EXP_NUMERO'];
                $combi [$i] ["year"] = $rs->fields ['SGD_SEXP_ANO'];
                $combi [$i] ["titulo"] = $rs->fields ['TITULO'];

                $i++;
                $rs->MoveNext();
            }
        } else {
            $combi['error'] = 'Error. No se encontraron datos';
        }
        return $combi;
    }

    /*     * ***************************************************************
     * *																**
     * *	RADICADOS DE ENTRADA RECIBIDOS DEL AREA DE CORRESPONDENCIA	**
     * *	POR USUARIO ACTUAL											**
     * *																**
     * *************************************************************** */

    function est5($fechIni, $fechFin, $depe, $estado) {
        if ($depe == '99999') {
            $where = '';
        } else
            $where = 'and r.radi_depe_radi=' . $depe;
        $isqlC = "select u.usua_codi ucodi,u.usua_nomb USUARIO, count(9) RADICADOS  
                from radicado r, usuario u 
                where r.radi_fech_radi between '$fechIni' and '$fechFin' and r.radi_usua_radi=u.usua_codi 
                and substr(cast(r.radi_nume_radi as char(18)) , 14 ,1)='2' $where 
                group by u.usua_nomb,u.usua_codi
                order by u.usua_nomb,u.usua_codi,count(9)  desc";

        $rs = $this->link->query($isqlC);
        $i = 0;
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                $combi [$i] ["ucodi"] = $rs->fields ['UCODI'];
                $combi [$i] ["usuario"] = $rs->fields ['USUARIO'];
                $combi [$i] ["radicados"] = $rs->fields ['RADICADOS'];
                $i++;
                $rs->MoveNext();
            }
        } else {
            $combi['error'] = 'No se encontraron datos';
        }
        return $combi;
    }

    function detallesEst5($fechIni, $fechFin, $idusua) {
        if ($depe == '99999') {
            $where = '';
        } else
            $where = 'and r.radi_depe_radi=' . $depe;

        $isqlC = "select u.usua_codi ucodi,u.usua_nomb USUARIO,r.radi_nume_radi radicados,
                     r.ra_asun asun,  TO_CHAR(r.radi_fech_radi, 'YYYY/MM/DD HH24:MI:SS')  fech
                        from radicado r, usuario u 
                        where r.radi_fech_radi between  '$fechIni' and '$fechFin'
                        and substr(cast(r.radi_nume_radi as char(18)), 14, 1)='2'    
                        and r.radi_usua_radi=u.usua_codi and r.radi_usua_radi=$idusua  order by r.radi_nume_radi,r.radi_fech_radi desc  ";

        $rs = $this->link->query($isqlC);
        $i = 0;
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                $combi [$i] ["ucodi"] = $rs->fields ['UCODI'];
                $combi [$i] ["usuario"] = $rs->fields ['USUARIO'];
                $combi [$i] ["radicados"] = $rs->fields ['RADICADOS'];
                $combi [$i] ["fech"] = $rs->fields ['FECH'];
                $combi [$i] ["asunto"] = $rs->fields ['ASUN'];
                $i++;
                $rs->MoveNext();
            }
        } else {
            $combi['error'] = 'No se encontraron datos';
        }
        return $combi;
    }

    function est6($depe, $usua, $estado, $tipoRadicado, $tipoDocumento) {
        //print "$depe, $usua, $estado, $tipoRadicado, $tipoDocumento <br>";
        $where = ' ';
        if ($tipoDocumento != 9999)
            $where .=' and r.tdoc_codi=' . $tipoDocumento;
        if ($tipoRadicado != null)
            $where .=" and substr(cast(r.radi_nume_radi as char(18)), 14 , 1)='$tipoRadicado'";

        if ($usua != 0)
            $where .=' and b.usua_codi=' . $usua;

        if ($estado == 0)
            $where .=" and b.USUA_esta='1' ";

        if ($depe != '99999')
            $where .=' and r.radi_depe_actu=' . $depe;
        $sql = "SELECT b.USUA_CODI ucodi,b.USUA_NOMB as USUARIO,r.radi_depe_actu, count(cast(r.radi_nume_radi as varchar(18)) ) as 
RADICADOS , MIN(b.USUA_CODI) as HID_COD_USUARIO 
FROM RADICADO r, USUARIO b, sgd_urd_usuaroldep urd 
WHERE r.RADI_USUA_ACTU=b.USUA_CODI AND r.RADI_DEPE_ACTU=urd.DEPE_CODI 
and urd.usua_codi=b.USUA_CODI AND r.radi_depe_actu=urd.depe_codi  $where
GROUP BY b.USUA_NOMB,r.radi_depe_actu,b.USUA_CODI ORDER BY 1";
        //print $sql;
        $rs = $this->link->query($sql);
        $i = 0;
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                $combi [$i] ["ucodi"] = $rs->fields ['UCODI'];
                $combi [$i] ["usuario"] = $rs->fields ['USUARIO'];
                $combi [$i] ["radicados"] = $rs->fields ['RADICADOS'];
                $i++;
                $rs->MoveNext();
            }
        } else {
            $combi['error'] = 'No se encontraron datos';
        }
        return $combi;
    }

    /* 	select sgd_exp_numero,sgd_sexp_ano from sgd_sexp_secexpedientes
      where
      depe_codi=$depe and
      sgd_srd_codigo=$serieid
      and sgd_sbrd_codigo=$subserie
      and sgd_sexp_ano='$ano' */

    function detallesEst6($idusua, $tipoRadicado, $tipoDocumento) {
        $where = ' ';
        if ($tipoDocumento != 9999)
            $where .=' and r.tdoc_codi=' . $tipoDocumento;
        if ($tipoRadicado != null)
            $where .=" and substr(cast(r.radi_nume_radi as char(18)) , 14 , 1) ='$tipoRadicado'";

        $sql = "SELECT  b.USUA_CODI ucodi,b.USUA_NOMB as USUARIO,r.radi_depe_actu, r.radi_nume_radi  as RADICADOS,
              r.ra_asun asun, r.radi_usu_ante uante,tp.sgd_tpr_descrip tpdesc, r.radi_fech_radi fech
FROM RADICADO r, USUARIO b, sgd_urd_usuaroldep urd,sgd_tpr_tpdcumento tp
 WHERE r.RADI_USUA_ACTU=$idusua and r.RADI_USUA_ACTU=b.USUA_CODI AND r.RADI_DEPE_ACTU=urd.DEPE_CODI and 
     urd.usua_codi=b.USUA_CODI AND r.radi_depe_actu=urd.depe_codi and b.USUA_esta='1'  and
     r.tdoc_codi=tp.sgd_tpr_codigo $where ORDER BY 1    ";

        $rs = $this->link->query($sql);
        $i = 0;
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                $combi [$i] ["ucodi"] = $rs->fields ['UCODI'];
                $combi [$i] ["usuario"] = $rs->fields ['USUARIO'];
                $combi [$i] ["radicados"] = $rs->fields ['RADICADOS'];
                $combi [$i] ["fech"] = $rs->fields ['FECH'];
                $combi [$i] ["asunto"] = $rs->fields ['ASUN'];
                $combi [$i] ["uante"] = $rs->fields ['UANTE'];
                $combi [$i] ["tpdesc"] = $rs->fields ['TPDESC'];
                $i++;
                $rs->MoveNext();
            }
        } else {
            $combi['error'] = 'No se encontraron datos';
        }
        return $combi;
    }

    /* esta estadistica traer los radicados realizados por un usuario debido a esto se usa radi_usuaraidi. */

    function est1($depe, $fechIni, $fechFin, $usua, $estado, $tipoRadicado, $tipoDocumento,$Inac) {
        $where = ' ';
        if ($tipoDocumento != 9999)
            $where .=' and r.tdoc_codi=' . $tipoDocumento;
        if ($tipoRadicado != null)
            $where .=" and substr(cast(r.radi_nume_radi as char(18)), 14 , 1)='$tipoRadicado'";

        if ($usua != 0)
            $where .=' and b.usua_codi=' . $usua;

        if ($estado == 0)
            $where .=" and b.USUA_esta='1' ";

        if ($depe != '99999'){
            $where .=' and r.radi_depe_radi=' . $depe;
	}else{
	    if($Inac==1)
		$where .=" and r.radi_depe_radi in (select depe_codi from dependencia where depe_estado=0) ";
	}
        $sql = "SELECT b.USUA_NOMB as USUARIO, count(*) as RADICADOS, b.USUA_CODI as UCODI ,usua_esta ESTADO
                FROM RADICADO r , USUARIO b WHERE 
                r.radi_fech_radi between  to_timestamp('$fechIni','dd/mm/yyyy hh24:mi:ss') and to_timestamp('$fechFin','dd/mm/yyyy hh24:mi:ss')
                and r.radi_usua_radi=b.usua_CODI 
                $where
                 GROUP BY b.USUA_NOMB,usua_esta, b.USUA_CODI ORDER BY 1 , 2 ";
        //la linea 217 era and r.radi_usua_radi=b.usua_CODI
        $rs = $this->link->query($sql);
        $i = 0;
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                $combi [$i] ["ucodi"] = $rs->fields ['UCODI'];
                $combi [$i] ["usuario"] = $rs->fields ['USUARIO'];
                $combi [$i] ["radicados"] = $rs->fields ['RADICADOS'];
                $combi [$i] ["estado"] = $rs->fields['ESTADO'];
                $i++;
                $rs->MoveNext();
            }
        } else {
            $combi['error'] = 'No se encontraron datos';
        }
        return $combi;
    }

    /* 	select sgd_exp_numero,sgd_sexp_ano from sgd_sexp_secexpedientes
      where
      depe_codi=$depe and
      sgd_srd_codigo=$serieid
      and sgd_sbrd_codigo=$subserie
      and sgd_sexp_ano='$ano' */

    function detallesEst1($fechIni, $fechFin, $idusua, $tipoRadicado, $tipoDocumento,$depe,$Inac) {
        $where = ' ';
        if ($tipoDocumento != 9999)
            $where .=' and r.tdoc_codi=' . $tipoDocumento;
        if ($tipoRadicado != null)
            $where .=" and substr(cast(r.radi_nume_radi as char(18)) , 14 , 1) ='$tipoRadicado'";
	if($depe=='99999' && $Inac==1){
	    $where .=" and r.radi_depe_radi in (select depe_codi from dependencia where depe_estado=0) ";
	}elseif($depe!='99999'){
	    $where .=" and r.radi_depe_radi=$depe ";
	}
//la linea 254 era WHERE r.RADI_usua_radi=$idusua and r.RADI_USUA_actu=b.USUA_CODI
        $sql = "SELECT dir.sgd_dir_nomremdes rem, b.USUA_CODI ucodi,b.USUA_NOMB as USUARIO,r.radi_depe_actu, r.radi_nume_radi  as RADICADOS,
              r.ra_asun asun, r.radi_usu_ante uante,tp.sgd_tpr_descrip tpdesc, r.radi_fech_radi fech,da.depe_nomb dactu
FROM RADICADO r, USUARIO b, sgd_urd_usuaroldep urd,sgd_tpr_tpdcumento tp,dependencia da,sgd_dir_drecciones dir
 WHERE r.RADI_usua_radi=$idusua and r.RADI_USUA_actu=b.USUA_CODI 
     and da.depe_codi=r.radi_depe_actu and r.radi_nume_radi =dir.radi_nume_radi 
     and      urd.usua_codi=b.USUA_CODI and
     r.radi_fech_radi between  to_timestamp('$fechIni','dd/mm/yyyy hh24:mi:ss') and to_timestamp('$fechFin','dd/mm/yyyy hh24:mi:ss') and
     r.tdoc_codi=tp.sgd_tpr_codigo and dir.sgd_dir_tipo in (1,2) $where ORDER BY radi_fech_radi";

        $rs = $this->link->query($sql);
        $i = 0;
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                $combi [$i] ["ucodi"] = $rs->fields ['UCODI'];
                $combi [$i] ["usuario"] = $rs->fields ['USUARIO'];
                $combi [$i] ["radicados"] = $rs->fields ['RADICADOS'];
                $combi [$i] ["fech"] = $rs->fields ['FECH'];
                $combi [$i] ["asunto"] = $rs->fields ['ASUN'];
                $combi [$i] ["uante"] = $rs->fields ['UANTE'];
                $combi [$i] ["tpdesc"] = $rs->fields ['TPDESC'];
                $combi [$i] ["dactu"] = $rs->fields ['DACTU'];
                $combi [$i] ["dest"] = $rs->fields ['REM'];
                $i++;
                $rs->MoveNext();
            }
        } else {
            $combi['error'] = 'No se encontraron datos';
        }
        return $combi;
    }

    function est2($depe, $fechIni, $fechFin, $usua, $estado, $tipoRadicado,$Inac) {
        $where = ' ';
        if ($tipoRadicado != null)
            $where .=" and substr(cast(r.radi_nume_radi as char(18)) ,14 , 1)='$tipoRadicado'";

        if ($usua != 0)
            $where .=' and b.usua_codi=' . $usua;

        if ($estado == 0)
            $where .=" and b.USUA_esta='1' ";
        if ($idusua != 0)
            $where .=' and r.RADI_usua_radi=' . $idusua;
        if ($depe != '99999'){
            $where .=' and r.radi_depe_radi=' . $depe;
	}else{
	    if($Inac==1)
		$where .=" and r.radi_depe_radi in (select depe_codi from dependencia where depe_estado=0) ";
        }
        /*
          if ($depe != '99999')
          $where .=' and r.radi_depe_actu=' . $depe; */
        $sql = "SELECT c.mrec_desc AS MEDIO_RECEPCION, COUNT(*) AS Radicados, max(c.MREC_CODI) AS HID_MREC_CODI 
                 FROM RADICADO r, MEDIO_RECEPCION c, USUARIO b 
                 WHERE r.radi_usua_radi=b.usua_CODI AND r.mrec_codi=c.mrec_codi and 
                 r.radi_fech_radi between  to_timestamp('$fechIni','dd/mm/yyyy hh24:mi:ss') and to_timestamp('$fechFin','dd/mm/yyyy hh24:mi:ss')
                 $where
                 GROUP BY c.mrec_desc ORDER BY 1 , 2 ";

        $rs = $this->link->query($sql);
        $i = 0;
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                $combi [$i] ["ucodi"] = $rs->fields ['HID_MREC_CODI'];
                $combi [$i] ["usuario"] = $rs->fields ['MEDIO_RECEPCION'];
                $combi [$i] ["radicados"] = $rs->fields ['RADICADOS'];
                $i++;
                $rs->MoveNext();
            }
        } else {
            $combi['error'] = 'No se encontraron datos';
        }
        return $combi;
    }

    function detallesEst2($fechIni, $fechFin, $idusua, $tipoRadicado, $tipoDocumento, $tpenvio, $depe, $estado,$Inac) {
        $where = ' ';
        if ($estado == 0)
            $where .=" and b.USUA_esta='1' ";
        if ($tipoDocumento != 9999)
            $where .=' and r.tdoc_codi=' . $tipoDocumento;
        if ($idusua != 0)
            $where .=' and r.RADI_usua_radi=' . $idusua;
        if ($depe != '99999'){
            $where .=' and r.radi_depe_radi=' . $depe;
	}else{
	    if($Inac==1)
	    $where .=' and r.radi_depe_radi in (select depe_codi from dependencia where depe_estado=0)';
	}
        if ($tipoRadicado != null)
            $where .=" and substr(cast(r.radi_nume_radi as char(18)), 14, 1) ='$tipoRadicado'";
        $sql = "SELECT c.mrec_desc AS MEDIO_RECEPCION, b.USUA_CODI ucodi,b.USUA_NOMB as USUARIO,r.radi_depe_actu, r.radi_nume_radi  as RADICADOS,
              r.ra_asun asun, r.radi_usu_ante uante,tp.sgd_tpr_descrip tpdesc, r.radi_fech_radi fech
FROM RADICADO r, USUARIO b, sgd_tpr_tpdcumento tp, MEDIO_RECEPCION c
 WHERE r.RADI_USUA_RADI=b.USUA_CODI 
     and r.mrec_codi=$tpenvio and  r.mrec_codi=c.mrec_codi and
     r.radi_fech_radi between  to_timestamp('$fechIni','dd/mm/yyyy hh24:mi:ss') and to_timestamp('$fechFin','dd/mm/yyyy hh24:mi:ss') and
     r.tdoc_codi=tp.sgd_tpr_codigo $where ORDER BY MEDIO_RECEPCION, USUA_NOMB, RADICADOS  ";

        $rs = $this->link->query($sql);
        $i = 0;
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                $combi [$i] ["ucodi"] = $rs->fields ['UCODI'];
                $combi [$i] ["usuario"] = $rs->fields ['USUARIO'];
                $combi [$i] ["radicados"] = $rs->fields ['RADICADOS'];
                $combi [$i] ["fech"] = $rs->fields ['FECH'];
                $combi [$i] ["asunto"] = $rs->fields ['ASUN'];
                $combi [$i] ["uante"] = $rs->fields ['UANTE'];
                $combi [$i] ["tpdesc"] = $rs->fields ['TPDESC'];
                $combi [$i] ["mrec"] = $rs->fields ['MEDIO_RECEPCION'];
                $i++;
                $rs->MoveNext();
            }
        } else {
            $combi['error'] = 'No se encontraron datos';
        }
        return $combi;
    }

    /*     * ***********************************************************
     * *															**
     * *	ESTADISTICAS DE MEDIO DE ENVIO FINAL DE DOCUMENTOS		**
     * *															**
     * *********************************************************** */

    function est3($depe, $fechIni, $fechFin,$Inac) {
        $where = ' ';

        if ($depe != '99999'){
            $where .=' and c.depe_codi=' . $depe;
	}else{
	    if($Inac==1)
		$where .=" and r.radi_depe_radi in (select depe_codi from dependencia where depe_estado=0) ";
	}
        $sql = "select d.sgd_fenv_descrip,d.sgd_fenv_codigo, COUNT(*) as RADICADOS
				from SGD_RENV_REGENVIO a, dependencia c, SGD_FENV_FRMENVIO d 
				WHERE a.sgd_renv_fech BETWEEN to_timestamp('$fechIni','dd/mm/yyyy hh24:mi:ss') and to_timestamp('$fechFin','dd/mm/yyyy hh24:mi:ss')
				and substr(cast(a.radi_nume_sal as varchar(18)), 5, 3)=cast(c.depe_codi as varchar(6)) and a.sgd_fenv_codigo=d.sgd_fenv_codigo
				and a.sgd_renv_planilla != '' and  a.sgd_renv_planilla != '00' $where
				GROUP BY d.sgd_fenv_descrip,d.sgd_fenv_codigo";

        //print $sql;

        $rs = $this->link->query($sql);
        $i = 0;
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                $combi [$i] ["radicados"] = $rs->fields ['RADICADOS'];
                $combi [$i] ["idmedio"] = $rs->fields ['SGD_FENV_CODIGO'];
                $combi [$i] ["nombremed"] = $rs->fields ['SGD_FENV_DESCRIP'];
                $i++;
                $rs->MoveNext();
            }
        } else {
            $combi['error'] = 'No se encontraron datos';
        }
        return $combi;
    }

    /*     * ***********************************************************
     * *  		DETALLES DE LA ESTADISTICA NUMERO 3	            **
     * *	ESTADISTICAS DE MEDIO DE ENVIO FINAL DE DOCUMENTOS		**
     * *********************************************************** */

    function detallesEst3($depe, $fechIni, $fechFin, $idmedio,$Inac) {
        $where = ' ';
        if ($depe != '99999'){
            $where .=' and c.depe_codi=' . $depe;
	}else{
	    if($Inac==1)
		$where .=" and r.radi_depe_radi in (select depe_codi from dependencia where depe_estado=0) ";
	}
        $sql = "select c.depe_nomb, a.radi_nume_sal, a.sgd_renv_nombre, a.sgd_renv_dir , a.sgd_renv_mpio, a.sgd_renv_depto, a.sgd_renv_fech, a.sgd_renv_planilla, a.sgd_renv_cantidad, a.sgd_renv_valor, a.sgd_deve_fech, a.sgd_renv_observa, d.sgd_fenv_descrip 
		from SGD_RENV_REGENVIO a, dependencia c, SGD_FENV_FRMENVIO d
		WHERE a.sgd_renv_fech BETWEEN to_timestamp('$fechIni','dd/mm/yyyy hh24:mi:ss') and to_timestamp('$fechFin','dd/mm/yyyy hh24:mi:ss') 
		and substr(cast(a.radi_nume_sal as varchar(18)), 5, 3)=cast(c.depe_codi as varchar(6)) 
		and a.sgd_fenv_codigo=d.sgd_fenv_codigo and a.sgd_fenv_codigo=$idmedio and a.sgd_renv_planilla != '' and  a.sgd_renv_planilla != '00'
		$where
		ORDER BY substr(cast(a.radi_nume_sal as varchar(18)), 5, 3), a.SGD_RENV_FECH DESC,a.SGD_RENV_PLANILLA DESC ";

        //print $sql;

        $rs = $this->link->query($sql);
        $i = 0;
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                $combi [$i] ["radicado"] = $rs->fields ['RADI_NUME_SAL'];
                $combi [$i] ["rnombre"] = $rs->fields ['SGD_RENV_NOMBRE'];
                $combi [$i] ["rdireccion"] = $rs->fields ['SGD_RENV_DIR'];
                $combi [$i] ["rmunicipio"] = $rs->fields ['SGD_RENV_MPIO'];
                $combi [$i] ["rdepartamento"] = $rs->fields ['SGD_RENV_DEPTO'];
                $combi [$i] ["rfecha"] = $rs->fields ['SGD_RENV_FECH'];
                $combi [$i] ["planilla"] = $rs->fields ['SGD_RENV_PLANILLA'];
                $combi [$i] ["sgd_renv_observa"] = $rs->fields ['SGD_RENV_OBSERVA'];
                $i++;
                $rs->MoveNext();
            }
        } else {
            $combi['error'] = 'No se encontraron datos';
        }
        return $combi;
    }

    /*     * *******************************************
     * *											**
     * *	ESTADISTICA 10 RADICADOS POR BANDEJA	**
     * *											**
     * ******************************************* */

    function est10($depe, $usua, $estado, $tipoDocumento, $fechIni, $fechFin,$Inac) {
        $where = ' ';

        if ($tipoDocumento != 9999) {
            $where = ' and r.tdoc_codi=' . $tipoDocumento;
        }
        if ($usua != 0)
            $where .=' and u.usua_codi=' . $usua;
        if ($estado == 0)
            $where .=" and u.USUA_esta='1' ";
        if ($depe != '99999') {
            $where .=' and d.DEPE_CODI =' . $depe;
            $wherePer = ' where DEPE_CODI =' . $depe;
        }else{
	    if($Inac==1){
		$where .=' and d.DEPE_CODI in (select depe_codi from dependencia where depe_estado=0) ';
                $wherePer = ' where DEPE_CODI in (select depe_codi from dependencia where depe_estado=0) ';
	    }
	}
        $sqlcarp = " select u.usua_nomb,d.depe_nomb, count(1) cuenta,r.carp_codi,r.carp_per,u.usua_codi, r.carp_codi,d.depe_codi 
 from radicado r, usuario u,dependencia d 
 where  r.radi_usua_actu=u.usua_codi and r.radi_depe_actu=d.depe_codi  $where
 group by r.carp_codi,r.carp_per,u.usua_nomb,r.radi_depe_actu,d.depe_nomb,u.usua_codi, r.carp_codi,d.depe_codi 
 order by r.radi_depe_actu ,u.usua_nomb,d.depe_nomb   ";
        $rs = $this->link->query($sqlcarp);
        $sqlnombcarp = ' select carp_codi,carp_desc from carpeta';
        $rs2 = $this->link->query($sqlnombcarp);
        $sqlnombcarpPer = "select usua_codi,depe_codi,nomb_carp,codi_carp from carpeta_per $wherePer";
        $rs3 = $this->link->query($sqlnombcarpPer);
        while (!$rs2->EOF) {
            $dataCarps[$rs2->fields ['CARP_CODI']] = $rs2->fields ['CARP_DESC'];
            $rs2->MoveNext();
        }
        while (!$rs3->EOF) {
            $dataCarpPers[$rs3->fields ['USUA_CODI'] . "-" . $rs3->fields ['DEPE_CODI']][$rs3->fields ['CODI_CARP']] = $rs3->fields ['NOMB_CARP'];
            $rs3->MoveNext();
        }
        // print_r($dataCarpPers);
        $usuaDAta = '';
        $combi = '';
        $combi2 = '';
        if (!$rs->EOF) {
            $i = 0;
            $a = 0;
            $i3 = 0;
            $g = 0;
            while (!$rs->EOF) {
                if ($g != $rs->fields ['USUA_CODI']) {
                    $g = $rs->fields ['USUA_CODI'];
                    $usuaDAta [$i3] ["usuacodi"] = $rs->fields ['USUA_CODI'];
                    $usuaDAta [$i3] ["depenomb"] = $rs->fields ['DEPE_NOMB'];
                    $usuaDAta [$i3] ["depecodi"] = $rs->fields ['DEPE_CODI'];
                    $usuaDAta [$i3] ["usuanomb"] = $rs->fields ['USUA_NOMB'];
                    $i3++;
                    $i = 0;
                    $a = 0;
                }
                if ($rs->fields ['CARP_PER'] == 0) {
                    $combi[$g] [$i] ["namecarp"] = $dataCarps[$rs->fields ['CARP_CODI']];
                    $combi[$g] [$i] ["carpcodi"] = $rs->fields ['CARP_CODI'];
                    $combi[$g] [$i] ["totalcarp"] = $rs->fields ['CUENTA'];

                    $i++;
                }
                if ($rs->fields ['CARP_PER'] == 1) {
                    $combi2 [$g] [$a]["namecarp"] = $dataCarpPers[$rs->fields ['USUA_CODI'] . "-" . $rs->fields ['DEPE_CODI']][$rs->fields ['CARP_CODI']];
                    $combi2 [$g] [$a]["carpper"] = $rs->fields ['CARP_CODI'];
                    $combi2 [$g] [$a]["totalcarp"] = $rs->fields ['CUENTA'];
                    $a++;
                }
                $rs->MoveNext();
            }
        }

        $resultado['usuario'] = $usuaDAta;
        $resultado['carp'] = $combi;
        $resultado['per'] = $combi2;
        // print_r($combi2);
        return $resultado;
    }

    /*
      function est101($depe, $usua, $estado, $tipoDocumento, $fechIni, $fechFin) {
      //print "$depe,$usua,$estado,$tipoDocumento,$fechIni,$fechFin";
      $where = ' ';
      $where2 = ' ';
      $where3 = ' ';
      if ($tipoDocumento != 9999) {
      $where2 = ' and r.tdoc_codi=' . $tipoDocumento;
      $where3 = ' and tdoc_codi=' . $tipoDocumento;
      }

      if ($usua != 0)
      $where .=' and u.usua_codi=' . $usua;
      echo $estado;
      if ($estado == 0)
      $where .=" and u.USUA_esta='1' ";

      if ($depe != '99999')
      $where .=' and d.DEPE_CODI =' . $depe;

      $sql = "select u.USUA_NOMB as USUARIO,usua_esta ESTADO, MIN(u.USUA_CODI) as UCODI, u.USUA_DOC as DOCUMENTO, d.DEPE_CODI as DEPENDENCIA, d.ROL_CODI as ROL
      FROM USUARIO u, sgd_urd_usuaroldep d
      where u.USUA_CODI=d.USUA_CODI
      $where
      GROUP BY u.USUA_NOMB,u.USUA_DOC,usua_esta,d.DEPE_CODI, d.ROL_CODI ORDER BY d.DEPE_CODI , u.USUA_NOMB ";
      //print $sql;
      $rs = $this->link->query($sql);

      $i = 0;
      if (!$rs->EOF) {
      while (!$rs->EOF) {
      $codigous = $rs->fields ['UCODI'];
      $documento = $rs->fields ['DOCUMENTO'];
      $rol = $rs->fields ['ROL'];
      $dependencia = $rs->fields ['DEPENDENCIA'];

      $sql1 = "select  DISTINCT ON  ( nomb_carp ) CODI_CARP,DESC_CARP,NOMB_CARP,(SELECT count(1) CUENTA  FROM   radicado r
      WHERE  r.carp_per=1 and carp_codi=CODI_CARP AND  r.id_rol=$rol AND   r.radi_usua_actu =$codigous AND
      r.radi_depe_actu =$dependencia and r.radi_fech_radi BETWEEN '$fechIni' and '$fechFin' $where2) cuenta from carpeta_per where usua_codi=$codigous and id_rol =$rol and depe_codi=$dependencia order by nomb_carp";
      //print $sql1;
      $rs2 = $this->link->query($sql1);
      $a = 0;
      if (!$rs2->EOF) {
      while (!$rs2->EOF) {

      $combi [$i] ["personales"] [$a] ["namecarp"] = $rs2->fields ['NOMB_CARP'];
      $combi [$i] ["personales"] [$a] ["idcarpeta"] = $rs2->fields ['CODI_CARP'];
      $combi [$i] ["personales"] [$a] ["totalcarp"] = $rs2->fields ['CUENTA'];
      $a++;
      $rs2->MoveNext();
      }
      } else {

      }

      $sql2 = "select c.carp_codi CODI_CARP, carp_desc CARP_DESC,count(carp_desc) CUENTA from carpeta c, radicado r where c.carp_codi<>11 and
      r.carp_codi=c.carp_codi and  r.radi_depe_actu=$dependencia
      and r.radi_usua_actu=$codigous and id_rol=$rol and r.carp_per=0 and r.radi_usua_doc_actu='$documento'  and r.radi_fech_radi BETWEEN '$fechIni' and '$fechFin' $where2 group by c.carp_codi,c.carp_desc";
      //print $sql2;
      $rs3 = $this->link->query($sql2);
      $c = 0;
      if (!$rs3->EOF) {
      while (!$rs3->EOF) {
      $combi [$i] ["carpetas"] [$c] ["namecarp"] = $rs3->fields ['CARP_DESC'];
      $combi [$i] ["carpetas"] [$c] ["idcarpeta"] = $rs3->fields ['CODI_CARP'];
      $combi [$i] ["carpetas"] [$c] ["totalcarp"] = $rs3->fields ['CUENTA'];
      $c++;
      $rs3->MoveNext();
      }
      } else {

      }
      if ($rol == 1) {
      $sql3 = "select count(1) as CONTADOR from radicado
      where carp_per=0 and carp_codi=11
      and  radi_depe_actu=$dependencia
      and radi_usua_actu=$codigous $where3";
      //print $sql3;
      $rs4 = $this->link->query($sql3);

      if (!$rs4->EOF) {
      while (!$rs4->EOF) {
      $combi [$i] ["carpetas"] [$c] ["namecarp"] = "Vo.Bo.";
      $combi [$i] ["carpetas"] [$c] ["idcarpeta"] = 11;
      $combi [$i] ["carpetas"] [$c] ["totalcarp"] = $rs4->fields ['CONTADOR'];
      $c++;
      $rs4->MoveNext();
      }
      } else {

      }
      }

      $combi [$i] ["ucodi"] = $rs->fields ['UCODI'];
      $combi [$i] ["usuario"] = $rs->fields ['USUARIO'];
      $combi [$i] ["dependencia"] = $rs->fields ['DEPENDENCIA'];
      $i++;
      $rs->MoveNext();
      }
      } else {
      $combi['error'] = 'No se encontraron datos';
      }
      //print_r ($combi);
      return $combi;
      } */

    /*     * **********************************************************
     * *														   **
     * *  		DETALLES DE LA ESTADISTICA NUMERO 10           **
     * *														   **
     * ********************************************************** */

    function detallesEst10($idusuaban, $dependencia_ban, $tipocarpetaban, $carpetanumero, $tipoDocumento, $fechIni, $fechFin) {
        //print "$idusuaban, $dependencia_ban, $tipocarpetaban, $carpetanumero, $tipoDocumento,$fechIni,$fechFin <br>";
        $where = ' ';
        if ($tipoDocumento != 9999) {
            $where = ' and b.tdoc_codi=' . $tipoDocumento;
        }
        $sql = "select distinct b.RADI_NUME_RADI as RADICADOS ,TO_CHAR(b.RADI_FECH_RADI,'YYYY-MM-DD HH24:MI AM') as FECHA_RADICADO ,
		b.RA_ASUN as ASUNTO,d.SGD_DIR_NOMREMDES as REMITENTE, c.SGD_TPR_DESCRIP as TIPO_DOCUMENTO ,
		0 as DIAS_RESTANTES ,
		b.RADI_NUME_DERI as HID_RADI_NUME_DERI ,b.RADI_TIPO_DERI as HID_RADI_TIPO_DERI ,b.radi_usu_ante as USUANTE 
		from radicado b left outer join SGD_TPR_TPDCUMENTO c on b.tdoc_codi=c.sgd_tpr_codigo 
		left outer join SGD_DIR_DRECCIONES d on b.radi_nume_radi=d.radi_nume_radi 
		where b.radi_nume_radi is not null and (d.sgd_dir_tipo<2 or d.sgd_dir_tipo is null) 
		and b.radi_depe_actu=" . $dependencia_ban . " and b.radi_usua_actu='" . $idusuaban . "' and b.carp_codi=" . $carpetanumero . " and b.carp_per=" . $tipocarpetaban . " $where  order by 3 DESC";
        //and b.radi_fech_radi BETWEEN '$fechIni' and '$fechFin'
        //echo $sql;
        $rs = $this->link->query($sql);
        $i = 0;
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                $combi [$i] ["radicados"] = $rs->fields ['RADICADOS'];
                $combi [$i] ["fecha_radicado"] = $rs->fields ['FECHA_RADICADO'];
                $combi [$i] ["asunto"] = $rs->fields ['ASUNTO'];
                $combi [$i] ["remitente"] = $rs->fields ['REMITENTE'];
                $combi [$i] ["tipo_documento"] = $rs->fields ['TIPO_DOCUMENTO'];
                $combi [$i] ["dias_restantes"] = $rs->fields ['DIAS_RESTANTES'];
                $combi [$i] ["radi_nume_deri"] = $rs->fields ['RADI_NUME_DERI'];
                $combi [$i] ["hid_radi_tipo_deri"] = $rs->fields ['HID_RADI_TIPO_DERI'];
                $combi [$i] ["usuante"] = $rs->fields ['USUANTE'];
                $i++;
                $rs->MoveNext();
            }
            /* if ($tipocarpetaban == 0) {
              $sqlminic = "select carp_desc BANDEJA from carpeta where carp_codi=$carpetanumero";
              $rsminic = $this->link->query($sqlminic);
              $combi [0] ["bandeja"] = $rsminic->fields ['BANDEJA'];
              } else {
              $sqlminip = "select  DISTINCT ON  ( nomb_carp ) CODI_CARP,nomb_carp BANDEJA from carpeta_per where codi_carp=$carpetanumero";
              $rsminip = $this->link->query($sqlminip);
              $combi [0] ["bandeja"] = $rsminip->fields ['BANDEJA'];
              } */
        } else {
            $combi['error'] = 'No se encontraron datos';
        }
        return $combi;
    }

    function est11($depe, $fechIni, $fechFin, $usua, $estado, $tipoRadicado, $tipoDocumento) {
        //print "$depe, $fechIni, $fechFin, $usua, $estado, $tipoRadicado, $tipoDocumento <br>";
        $where = ' ';
        if ($tipoRadicado != null)
            $where .=" and substr(cast(r.radi_nume_radi as char(18)) ,14, 1)='$tipoRadicado'";

        if ($usua != 0)
            $where .=' and b.usua_codi=' . $usua;

        if ($estado == 0)
            $where .=" and b.USUA_esta='1' ";
        //if ($idusua != 0)
        //$where .=' and b.usua_DOC=' . $idusua;
        if ($depe != '99999' && $depe != '0')
            $whereh .=' and h.DEPE_CODI=' . $depe;
        //$where .=' and h.DEPE_CODI_DEST=' . $depe; //$where .=' and h.DEPE_CODI=' . $depe;

        $sql = "SELECT  b.USUA_NOMB USUARIO
					, count(distinct r.radi_nume_radi) RADICADOS
					, SUM(select sum(rr.RADI_NUME_folios) from radicado rr  where rr.radi_nume_radi=r.RADI_NUME_RADI) 	HOJAS_DIGITALIZADAS
                                        , sum((select sum(anex_folios_dig) from anexos where anex_radi_nume=r.RADI_NUME_RADI)) anexos
					, MIN(b.USUA_CODI) HID_COD_USUARIO,h.depe_codi
					FROM RADICADO r, USUARIO b, HIST_EVENTOS h
				WHERE
					h.USUA_DOC=b.usua_DOC
					AND h.RADI_NUME_RADI=r.RADI_NUME_RADI
					AND h.SGD_TTR_CODIGO in (22,42)
					AND h.hist_fech BETWEEN  to_timestamp('$fechIni','dd/mm/yyyy hh24:mm:ss') and timestamp('$fechFin','dd/mm/yyyy hh24:mi:ss')
                                            $where
				GROUP BY b.USUA_NOMB,h.depe_codi order BY b.USUA_NOMB asc";
        $sql = "SELECT b.USUA_NOMB USUARIO
, count( r.radi_nume_radi) RADICADOS
  , sum((select sum(anex_folios_dig) from anexos where anex_radi_nume=r.RADI_NUME_RADI)) anexos
, SUM(r.RADI_NUME_folios) 	HOJAS_DIGITALIZADAS
, MIN(b.USUA_CODI) HID_COD_USUARIO,max( th.depe) depe_codi
FROM RADICADO r, USUARIO b, (select distinct h.radi_nume_radi as radi,h.usua_codi as ucodi,h.depe_codi as depe from  hist_eventos h where h.SGD_TTR_CODIGO in (22,42) AND h.hist_fech BETWEEN  to_timestamp('$fechIni','dd/mm/yyyy hh24:mi:ss') and to_timestamp('$fechFin','dd/mm/yyyy hh24:mi:ss') $whereh ) th
 WHERE r.RADI_NUME_RADI=th.radi  $where and  b.usua_codi=th.ucodi
GROUP BY b.USUA_NOMB order BY b.USUA_NOMB asc"; //substring(cast(r.radi_nume_radi as char(18)) from 14 for 1)='2'
        $rs = $this->link->query($sql);
        $i = 0;
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                $combi [$i] ["ucodi"] = $rs->fields ['HID_COD_USUARIO'];
                $combi [$i] ["usuario"] = $rs->fields ['USUARIO'];
                $combi [$i] ["radicados"] = $rs->fields ['RADICADOS'];
                $combi [$i] ["depe"] = $rs->fields ['DEPE_CODI'];
                $combi [$i] ["folios"] = $rs->fields ['HOJAS_DIGITALIZADAS'];
                $combi [$i] ["anexos"] = $rs->fields ['ANEXOS'];
                $i++;
                $rs->MoveNext();
            }
        } else {
            $combi['error'] = 'No se encontraron datos';
        }
        return $combi;
    }

    function detallesEst11($fechIni, $fechFin, $idusua, $tipoRadicado, $depe) {
        $where = ' ';
        //       echo "$fechIni, $fechFin, $idusua, $tipoRadicado,  $depe";
        if ($tipoRadicado != null)
            $where .=" and substr(cast(r.radi_nume_radi as char(18)) ,14 ,1)='$tipoRadicado'";

        $sql = "SELECT distinct r.radi_nume_radi,to_char(h.hist_fech, 'DD-MM-YYYY') fhist,r.ra_asun asun ,
               to_char(r.radi_fech_radi, 'DD-MM-YYYY HH24:II:SS') frad, r.RADI_NUME_folios HOJAS_DIGITALIZADAS , b.USUA_DOC HID_COD_USUARIO,b.usua_nomb usuario ,r.radi_nume_Radi radicados
                ,(select sum(anex_folios_dig) from anexos where anex_radi_nume=r.RADI_NUME_RADI) num_anex
              FROM RADICADO r, USUARIO b, HIST_EVENTOS h 
                WHERE h.USUA_DOC=b.usua_DOC 
             AND h.RADI_NUME_RADI=r.RADI_NUME_RADI 
            AND h.SGD_TTR_CODIGO in (22,42) 
                 AND h.hist_fech BETWEEN to_timestamp('$fechIni','dd/mm/yyyy hh24:mi:ss') and to_timestamp('$fechFin','dd/mm/yyyy hh24:mi:ss') 
                and h.DEPE_CODI=$depe 
             and b.usua_codi=$idusua
                 $where
             order BY r.radi_nume_radi asc        
            "; //order BY to_char(r.radi_fech_radi , 'DD-MM-YYYY') asc

        $rs = $this->link->query($sql);
        $i = 0;
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                $combi [$i] ["hdigitalizadas"] = $rs->fields ['HOJAS_DIGITALIZADAS'];
                $combi [$i] ["ahdigitalizadas"] = $rs->fields ['NUM_ANEX'];
                $combi [$i] ["radicados"] = $rs->fields ['RADICADOS'];
                $combi [$i] ["fech"] = $rs->fields ['FHIST'];
                $combi [$i] ["fechRad"] = $rs->fields ['FRAD'];
                $combi [$i] ["asunto"] = $rs->fields ['ASUN'];
                $combi [$i] ["usuario"] = $rs->fields ['USUARIO'];

                $i++;
                $rs->MoveNext();
            }
        } else {
            $combi['error'] = 'No se encontraron datos';
        }
        return $combi;
    }

    /*     * *******************************************************************************
     * *																				**
     * *	RADICADOS DE ENTRADA RECIBIDOS DEL AREA DE CORRESPONDENCIA (DEPENDENCIA)	**
     * *																				**
     * ******************************************************************************* */

    function est15($depe, $fechIni, $fechFin, $tipoDocumento, $tipoRadicado,$Inac) {
        //print "$depe, $fechIni, $fechFin, $tipoRadicado <br>";

        $where = ' ';
        if ($tipoDocumento != 9999)
            $where .=" AND r.TDOC_CODI='$tipoRadicado'";
        if ($tipoRadicado != null)
            $where .=" and substr(cast(r.radi_nume_radi as char(18)) ,14, 1)='$tipoRadicado'";

        if ($depe != '99999'){
            $where .=' and h.DEPE_CODI_DEST =' . $depe;
	}else{
	    if($Inac==1)
		$where .=' and h.DEPE_CODI_DEST in (select depe_codi from dependencia where depe_estado=0)';
	}
        $DPCORRES = DPCORRES;
        $sql = "SELECT d.DEPE_NOMB AS DEPENDENCIA, count(*) AS RADICADOS, d.DEPE_CODI as CODEPE
					FROM DEPENDENCIA d, HIST_EVENTOS h, RADICADO r
					WHERE h.SGD_TTR_CODIGO=2
					and r.radi_depe_radi = $DPCORRES
					AND h.DEPE_CODI_DEST = d.DEPE_CODI
					and h.radi_nume_radi = r.radi_nume_radi
					AND r.radi_fech_radi BETWEEN to_timestamp('$fechIni','dd/mm/yyyy hh24:mi:ss') AND to_timestamp('$fechFin','dd/mm/yyyy hh24:mi:ss')
					$where
					GROUP BY d.DEPE_NOMB,d.DEPE_CODI
					ORDER BY DEPENDENCIA ASC
		";

        //print_r($sql);

        $rs = $this->link->query($sql);
        $i = 0;
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                $combi [$i] ["dependencia"] = $rs->fields ['DEPENDENCIA'];
                $combi [$i] ["radicados"] = $rs->fields ['RADICADOS'];
                $combi [$i] ["codepe"] = $rs->fields ['CODEPE'];
                $i++;
                $rs->MoveNext();
            }
        } else {
            $combi['error'] = 'No se encontraron datos';
        }
        return $combi;
    }

    /*     * **********************************************************
     * *														   **
     * *  		DETALLES DE LA ESTADISTICA NUMERO 15           **
     * *														   **
     * ********************************************************** */

    function detallesEst15($depe, $tipoDocumento, $fechIni, $fechFin, $tipoRadicado) {
        //print "$depe, $tipoDocumento, $fechIni, $fechFin <br>";
        $DPCORRES = DPCORRES;
        $where = ' ';
        if ($tipoDocumento != 9999)
            $where .=" AND r.TDOC_CODI='$tipoRadicado' ";
        if ($tipoRadicado != null)
            $where .=" and substr(cast(r.radi_nume_radi as char(18)) ,14, 1)='$tipoRadicado'";


        $sql = "
			SELECT dir.sgd_dir_nomremdes rem,d.DEPE_NOMB AS DEPENDENCIA, r.RADI_NUME_RADI AS RADICADOS, RADI_FECH_RADI, r.RA_ASUN as ASUNTO, r.RADI_DESC_ANEX as ANEXOS  
			FROM DEPENDENCIA d, HIST_EVENTOS h, RADICADO r ,sgd_dir_drecciones dir
			WHERE h.SGD_TTR_CODIGO=2 and r.radi_depe_radi =$DPCORRES AND h.DEPE_CODI_DEST = d.DEPE_CODI
			and h.radi_nume_radi = r.radi_nume_radi and r.radi_nume_radi =dir.radi_nume_radi
			AND r.radi_fech_radi BETWEEN to_timestamp('$fechIni','dd/mm/yyyy hh24:mi:ss') AND to_timestamp('$fechFin','dd/mm/yyyy hh24:mi:ss')
			and h.DEPE_CODI_DEST = $depe $where
			ORDER BY RADICADOS ASC
		";

        //print_r($sql);

        $rs = $this->link->query($sql);

        $i = 0;
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                $combi [$i] ["dependencia"] = $rs->fields ['DEPENDENCIA'];
                $combi [$i] ["radicados"] = $rs->fields ['RADICADOS'];
                $combi [$i] ["fecha"] = $rs->fields ['RADI_FECH_RADI'];
                $combi [$i] ["asunto"] = $rs->fields ['ASUNTO'];
                $combi [$i] ["anexos"] = $rs->fields ['ANEXOS'];
                $combi [$i] ["rem"] = $rs->fields ['REM'];
                $i++;
                $rs->MoveNext();
            }
        } else {
            $combi['error'] = 'No se encontraron datos';
        }
        return $combi;
    }

    function est17($depe, $fechIni, $fechFin, $usua, $estado, $tipoRadicado, $tipoDocumento) {
        //print "$depe, $fechIni, $fechFin, $usua, $estado, $tipoRadicado, $tipoDocumento <br>";

        $where = ' ';
        if ($tipoRadicado != null)
            $where .=" and substr(cast(r.radi_nume_radi as char(18)), 14 , 1)='$tipoRadicado'";
        if ($tipoDocumento != 9999)
            $where .=" AND r.TDOC_CODI='$tipoDocumento' ";
        if ($depe != '99999')
            $where .=' and h.DEPE_CODI_DEST =' . $depe;
        if ($usua != 0)
            $where .=' and b.usua_codi=' . $usua;
        if ($estado == 0)
            $where .=" and b.USUA_esta='1' ";

        $sql = "
			SELECT MIN(b.USUA_NOMB) USUARIO
          , count(r.RADI_NUME_RADI) RADICADOS
	  , count(a.anex_radi_nume) TRAMITADOS
          , b.USUA_CODI HID_COD_USUARIO
          , b.depe_codi HID_DEPE_USUA
        FROM RADICADO r LEFT JOIN (select distinct anex_radi_nume
       		from ANEXOS where anex_estado>=2) a ON r.RADI_NUME_RADI=a.ANEX_RADI_NUME
	, USUARIO b, HIST_EVENTOS h, SGD_TPR_TPDCUMENTO t
        WHERE 
          h.HIST_DOC_DEST=b.usua_doc
          AND r.tdoc_codi=t.sgd_tpr_codigo
          AND h.RADI_NUME_RADI=r.RADI_NUME_RADI
          AND h.SGD_TTR_CODIGO in(2,9,12,16)
		  AND r.radi_fech_radi BETWEEN to_timestamp('$fechIni','dd/mm/yyyy hh24:mi:ss') AND to_timestamp('$fechFin','dd/mm/yyyy hh24:mi:ss')
          $where
        group by b.usua_codi,b.depe_codi
		";

        //print_r($sql);

        $rs = $this->link->query($sql);

        $i = 0;
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                $combi [$i] ["usuario"] = $rs->fields ['USUARIO'];
                $combi [$i] ["radicados"] = $rs->fields ['RADICADOS'];
                $combi [$i] ["tramitados"] = $rs->fields ['TRAMITADOS'];
                $combi [$i] ["usuacodi"] = $rs->fields ['HID_COD_USUARIO'];
                $combi [$i] ["depeusua"] = $rs->fields ['HID_DEPE_USUA'];
                $i++;
                $rs->MoveNext();
            }
        } else {
            $combi['error'] = 'No se encontraron datos';
        }
        return $combi;
    }

    /**
     * Detalles Estadistica 17 ESTADISTICA POR RADICADOS Y SUS RESPUESTAS
     * 
     */
    function detallesEst17($depe, $fechIni, $fechFin, $usua, $tipoRadicado, $tipoDocumento) {
        //print "$depe, $fechIni, $fechFin, $usua, $tipoRadicado, $tipoDocumento <br>";

        $where = ' ';
        if ($tipoRadicado != null)
            $where .=" and substr(cast(r.radi_nume_radi as char(18)) , 14 , 1)='$tipoRadicado'";
        if ($tipoDocumento != 9999)
            $where .=" AND r.TDOC_CODI='$tipoDocumento' ";
        if ($depe != '99999') {
            $where .=' and h.DEPE_CODI_DEST =' . $depe;
        } else {
            $sqlbas = "
				select depe_codi from SGD_URD_USUAROLDEP where USUA_CODI = $usua
			";

            $rs1 = $this->link->query($sqlbas);

            $depe = $rs1->fields ['DEPE_CODI'];
        }
        /* if ($depe != '99999')
          $where .=' and h.DEPE_CODI_DEST =' . $depe;
          if ($usua != 0)
          $where .=' and b.usua_codi=' . $usua;
          if ($estado == 0)
          $where .=" and b.USUA_esta='1' "; */

        $sql = " SELECT r.RADI_NUME_RADI RADICADO
			, b.USUA_NOMB USUARIO
                        , b2.USUA_NOMB USUACTU
			, r.RA_ASUN ASUNTO
			,  to_char(r.RADI_FECH_RADI, 'DD-MM-YYYY HH:II:SS') FECHA_RADICACION
			, an.RADI_NUME_SALIDA
                        ,  to_char(an.ANEX_RADI_FECH , 'DD-MM-YYYY HH:II:SS') ANEX_RADI_FECH 
                        ,  to_char(an.ANEX_FECH_ENVIO , 'DD-MM-YYYY HH:II:SS') ANEX_FECH_ENVIO 
			, t.SGD_TPR_TERMINO
			, t.SGD_TPR_DESCRIP
			, d1.SGD_DIR_NOMREMDES DATO_1
			, m1.muni_nomb MUNICIPIO_1
			, (Select d.sgd_dir_nomremdes from sgd_dir_drecciones d where d.radi_nume_radi=r.radi_nume_radi AND d.SGD_DIR_TIPO=2) AS DATO_2
			, an.ANEX_CREADOR,r.radi_nume_deri
			FROM sgd_dir_drecciones d1,MUNICIPIO m1, USUARIO b,USUARIO b2, HIST_EVENTOS h, SGD_TPR_TPDCUMENTO t
			, RADICADO r left outer join anexos an 
			ON (R.RADI_NUME_RADI=an.ANEX_RADI_NUME ANd an.anex_estado>=2) 
			WHERE 
			r.tdoc_codi=t.sgd_tpr_codigo 
			AND d1.radi_nume_radi=r.radi_nume_radi
			AND d1.sgd_dir_tipo=1
                        and r.radi_usua_actu=b2.usua_codi
			AND d1.muni_codi=m1.muni_codi AND d1.dpto_codi=m1.dpto_codi
			AND h.HIST_DOC_DEST=b.usua_doc
			and h.DEPE_CODI_DEST = $depe
			and b.usua_codi=$usua
			AND h.RADI_NUME_RADI=r.RADI_NUME_RADI
			AND h.SGD_TTR_CODIGO in(2,9,12,16)
			AND r.radi_fech_radi BETWEEN to_timestamp('$fechIni','dd/mm/yyyy hh24:mi:ss')  AND to_timestamp('$fechFin','dd/mm/yyyy hh24:mi:ss') 
			$where
			order by radicado";
        //print_r($sql);
        $rs = $this->link->query($sql);

        $i = 0;
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                $combi [$i] ["radicado"] = $rs->fields ['RADICADO'];
                $combi [$i] ["usuario"] = $rs->fields ['USUARIO'];
                $combi [$i] ["asunto"] = $rs->fields ['ASUNTO'];
                $combi [$i] ["fecha_radicacion"] = $rs->fields ['FECHA_RADICACION'];
                $combi [$i] ["radi_nume_salida"] = $rs->fields ['RADI_NUME_SALIDA'];
                $combi [$i] ["anex_radi_fech"] = $rs->fields ['ANEX_RADI_FECH'];
                $combi [$i] ["anex_fech_envio"] = $rs->fields ['ANEX_FECH_ENVIO'];
                $combi [$i] ["sgd_tpr_termino"] = $rs->fields ['SGD_TPR_TERMINO'];
                $combi [$i] ["sgd_tpr_descrip"] = $rs->fields ['SGD_TPR_DESCRIP'];
                $combi [$i] ["dato_1"] = $rs->fields ['DATO_1'];
                $combi [$i] ["municipio_1"] = $rs->fields ['MUNICIPIO_1'];
                $combi [$i] ["dato_2"] = $rs->fields ['DATO_2'];
                $combi [$i] ["anex_creador"] = $rs->fields ['ANEX_CREADOR'];
                $combi [$i] ["radi_nume_deri"] = $rs->fields ['RADI_NUME_DERI'];
                $combi [$i] ["usua_actu"] = $rs->fields ['USUACTU'];
                if ($rs->fields ['RADI_NUME_DERI'])
                    $combi [$i] ["radi_nume_deri"] = $rs->fields ['RADI_NUME_DERI'];
                else
                    $combi [$i] ["radi_nume_deri"] = '';
                $i++;
                $rs->MoveNext();
            }
        } else {
            $combi['error'] = 'No se encontraron datos';
        }
        return $combi;
    }

    /**
     * Consulta Estadistica 18 (PQRS) PETICIONES, QUEJAS, RECLAMOS, SOLICITUDES
     * 
     */
    function est18($depe, $fechIni, $fechFin, $serie, $tipoRadicado) {
        $where = ' ';
        if ($depe != '99999')
            $where .=' and rd.DEPE_CODI =' . $depe;
        if ($tipoRadicado != null)
            $where .=" and substr(cast(r.radi_nume_radi as char(18)) ,14 ,1)='$tipoRadicado'";
        $sql = "SELECT m.sgd_srd_codigo cser, m.sgd_sbrd_codigo csub, s.sgd_srd_descrip nser,  sb.sgd_sbrd_descrip nsub,count(1)  valor
            FROM radicado r, sgd_rdf_retdocf rd, sgd_mrd_matrird m,  sgd_sbrd_subserierd sb,   sgd_srd_seriesrd s
            WHERE   r.radi_nume_radi = rd.radi_nume_radi AND
                    rd.sgd_mrd_codigo = m.sgd_mrd_codigo AND
                    m.sgd_srd_codigo = s.sgd_srd_codigo AND
                    m.sgd_sbrd_codigo = sb.sgd_sbrd_codigo AND
                    s.sgd_srd_codigo = sb.sgd_srd_codigo AND
                    s.sgd_srd_codigo = $serie and rd.sgd_rdf_fech between to_timestamp('$fechIni','dd/mm/yyyy hh24:mi:ss') AND to_timestamp('$fechFin','dd/mm/yyyy hh24:mi:ss') $where
            group by   m.sgd_srd_codigo,   m.sgd_sbrd_codigo,   s.sgd_srd_descrip,   sb.sgd_sbrd_descrip   order by sb.sgd_sbrd_descrip  ";

        $rs = $this->link->query($sql);

        $i = 0;
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                $combi [$i] ["valor"] = $rs->fields ['VALOR'];
                $combi [$i] ["nomSubserie"] = $rs->fields ['NSUB'];
                $combi [$i] ["nomSerie"] = $rs->fields ['NSER'];
                $combi [$i] ["codiSerie"] = $rs->fields ['CSER'];
                $combi [$i] ["codSubserie"] = $rs->fields ['CSUB'];
                $i++;
                $rs->MoveNext();
            }
        } else {
            $combi['error'] = 'No se encontraron datos';
        }
        return $combi;
    }

    /**
     * Detalles Estadistica 18 (PQRS) PETICIONES, QUEJAS, RECLAMOS, SOLICITUDES
     * 
     */
    function detallesEst18($depe, $fechIni, $fechFin, $Serie, $subseriec, $tipoRadicado) {
        $where = ' ';
        if ($depe != '99999')
            $where .=' and rd.DEPE_CODI =' . $depe;
        if ($tipoRadicado != null)
            $where .=" and substr(cast(r.radi_nume_radi as char(18)), 14, 1)='$tipoRadicado'";
        //print "$fechIni, $fechFin, $usua <br>";
        $sql = "SELECT   u.usua_nomb usua,r.radi_nume_radi radi,  r.ra_asun asunto,d.depe_nomb depen,
             TO_CHAR(r.radi_fech_radi, 'YYYY/MM/DD')  AS infech
            FROM   radicado r,   sgd_rdf_retdocf rd,   sgd_mrd_matrird m,   sgd_sbrd_subserierd sb,   sgd_srd_seriesrd s,  dependencia d, usuario u
            WHERE 
            r.radi_usua_actu=u.usua_codi and
            r.radi_depe_actu=d.depe_codi and
            r.radi_nume_radi = rd.radi_nume_radi AND
            rd.sgd_mrd_codigo = m.sgd_mrd_codigo AND
            m.sgd_srd_codigo = s.sgd_srd_codigo AND
            m.sgd_sbrd_codigo = sb.sgd_sbrd_codigo AND
            sb.sgd_sbrd_codigo=$subseriec and
            s.sgd_srd_codigo = sb.sgd_srd_codigo AND
            s.sgd_srd_codigo = $Serie and rd.sgd_rdf_fech between to_timestamp('$fechIni','dd/mm/yyyy hh24:mi:ss') and to_timestamp('$fechFin','dd/mm/yyyy hh24:mi:ss') $where
            order by r.radi_nume_radi";

        $rs = $this->link->query($sql);
        $combi = '';
        $radic = null;
        $i = 0;
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                $combi[$i]['radicado'] = $rs->fields ['RADI'];
                $combi[$i]['asunto'] = $rs->fields ['ASUNTO'];
                $combi[$i]['dependencia'] = $rs->fields ['DEPEN'];
                $combi[$i]['usuario'] = $rs->fields ['USUA'];
                $combi[$i]['radiFech'] = $rs->fields ['INFECH'];

                if ($radic == NUll)
                    $radic = $rs->fields ['RADI'];
                else
                    $radic.=',' . $rs->fields ['RADI'];
                $i++;
                $rs->MoveNext();
            }
        }else {
            $combi['error'] = 'No se encontraron datos';
        }

        $sqln = "select radi_nume_radi radicado,radi_nume_deri deri,radi_fech_radi fech from radicado where radi_nume_deri in ($radic)";
        $rs2 = $this->link->query($sqln);
        $combia = '';
        $i = 0;
        if (!$rs2->EOF) {
            while (!$rs2->EOF) {
                $combia[$rs2->fields ['DERI']][$i]['numeradi'] = $rs2->fields ['RADICADO'];
                $combia[$rs2->fields ['DERI']][$i]['fecha'] = $rs2->fields ['FECH'];
                $i++;
                $rs2->MoveNext();
            }
        } else {
            //  $combi['error'] = 'No se encontraron datos';
        }
        //print_r($combia);
        $combie['resp'] = $combia;
        $combie['data'] = $combi;
        return $combie;
    }

    /**
     * Consulta Estadistica 19 RADICADOS REASIGNADOS
     * 
     */
    function est19($depe, $fechIni, $fechFin, $usua, $tipoRadicado, $tipoDocumento) {
        $where = ' ';
        if ($tipoRadicado != null)
            $where .=" and substr(cast(h.radi_nume_radi as char(18)), 14, 1)='$tipoRadicado'";
        if ($tipoDocumento != 9999)
            $where .=" AND r.TDOC_CODI='$tipoDocumento' ";
        if ($depe != '99999')
            $where .=' and h.DEPE_CODI =' . $depe;
        if ($usua != 0)
            $where .=' and h.USUA_CODI=' . $usua;


        $sql = "
			SELECT 
				UPPER(u.usua_nomb) as USUARIO, 
				count (h.radi_nume_radi) as RADICADOS,
				u.USUA_CODI, 
				h.DEPE_CODI 
			FROM  hist_eventos h
				inner join radicado r on r.radi_nume_radi=h.radi_nume_radi  
				inner join dependencia d on d.depe_codi=h.depe_codi
				inner join usuario u on u.usua_codi=h.usua_codi 
			WHERE 
				h.hist_fech BETWEEN to_timestamp('$fechIni','dd/mm/yyyy hh24:mi:ss') AND to_timestamp('$fechFin','dd/mm/yyyy hh24:mi:ss') 
				and h.sgd_ttr_codigo in (9,12,8,16,13)
				$where
			GROUP BY u.usua_nomb, u.usua_codi,h.depe_codi
			ORDER BY u.usua_nomb ASC

		";

        $rs = $this->link->query($sql);

        $i = 0;
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                $combi [$i] ["usuario"] = $rs->fields ['USUARIO'];
                $combi [$i] ["radicados"] = $rs->fields ['RADICADOS'];
                $combi [$i] ["usuacodi"] = $rs->fields ['USUA_CODI'];
                $combi [$i] ["usuadepe"] = $rs->fields ['DEPE_CODI'];
                $i++;
                $rs->MoveNext();
            }
        } else {
            $combi['error'] = 'No se encontraron datos';
        }
        return $combi;
    }

    /**
     * Detalles Estadistica 19 RADICADOS REASIGNADOS
     * 
     */
    function detallesEst19($depe, $fechIni, $fechFin, $usua, $tipoRadicado, $tipoDocumento) {
        $where = ' ';
        if ($tipoRadicado != null)
            $where .=" and substr(cast(h.radi_nume_radi as char(18)), 14 ,1)='$tipoRadicado'";
        if ($tipoDocumento != 9999)
            $where .=" AND r.TDOC_CODI='$tipoDocumento' ";
        $sql = "SELECT   t.sgd_ttr_descrip ntra,   r2.sgd_rol_nombre rold, 
                r1.sgd_rol_nombre rolt,   to_char(h.hist_fech, 'DD-MM-YYYY HH24:II:SS') fhist, 
                h.radi_nume_radi radicado,   h.hist_obse hosb,   r.ra_asun asunto,
                u1.usua_nomb usuat,   u2.usua_nomb usuad,   d1.depe_nomb depet,  
                d2.depe_nomb deped,   to_char(r.radi_fech_radi, 'DD-MM-YYYY HH:II:SS')  fecharad
                FROM 
                    dependencia d1,   radicado r,   dependencia d2,   usuario u1, 
                    usuario u2,   hist_eventos h,   sgd_ttr_transaccion t,   sgd_rol_roles r1, 
                    sgd_rol_roles r2
                WHERE 
                    h.hist_fech BETWEEN to_timestamp('$fechIni','dd/mm/yyyy hh24:mi:ss') AND to_timestamp('$fechFin','dd/mm/yyyy hh24:mi:ss') and 
                    d1.depe_codi = h.depe_codi AND
                    r.radi_nume_radi = h.radi_nume_radi AND
                    h.usua_codi = u1.usua_codi AND h.usua_codi_dest = u2.usua_codi AND
                    h.depe_codi_dest = d2.depe_codi AND h.id_rol = r1.sgd_rol_id AND
                    h.id_rol_dest = r2.sgd_rol_id AND t.sgd_ttr_codigo = h.sgd_ttr_codigo
                    and h.sgd_ttr_codigo in (9,12,8,16,13)
                    and h.depe_codi=$depe and h.usua_codi = $usua $where order by to_char(h.hist_fech, 'DD-MM-YYYY HH24:II:SS')";
        $rs = $this->link->query($sql);

        $i = 0;
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                $combi [$i] ["radicado"] = $rs->fields ['RADICADO'];
                $combi [$i] ["asunto"] = $rs->fields ['ASUNTO'];
                $combi [$i] ["fecharad"] = $rs->fields ['FECHARAD'];
                $combi [$i] ["fechareasig"] = $rs->fields ['FHIST'];
                $combi [$i] ["detalle"] = $rs->fields ['HOSB'];
                $combi [$i] ["trans"] = $rs->fields ['NTRA'];
                $combi [$i] ["dependencia_t"] = $rs->fields ['DEPET'];
                $combi [$i] ["dependencia_d"] = $rs->fields ['DEPED'];
                $combi [$i] ["usuario_d"] = $rs->fields ['USUAD'];
                $combi [$i] ["usuario_t"] = $rs->fields ['USUAT'];
                $combi [$i] ["rol_d"] = $rs->fields ['ROLD'];
                $combi [$i] ["rol_t"] = $rs->fields ['ROLT'];
                $i++;
                $rs->MoveNext();
            }
        } else {
            $combi['error'] = 'No se encontraron datos';
        }
        return $combi;
    }

    function Total1($depe, $fechIni, $fechFin, $usua, $estado, $tipoRadicado, $tipoDocumento) {

        $where = ' ';
        if ($tipoDocumento != 9999)
            $where .=' and r.tdoc_codi=' . $tipoDocumento;
        if ($tipoRadicado != null)
            $where .=" and substr(cast(r.radi_nume_radi as char(18)) ,14, 1)='$tipoRadicado'";

        if ($usua != 0)
            $where .=' and b.usua_codi=' . $usua;

        if ($estado == 0)
            $where .=" and b.USUA_esta='1' ";

        if ($depe != '99999')
            $where .=' and r.radi_depe_radi=' . $depe;
        /* if ($tipoDocumento != 9999)
          $where .=' and r.tdoc_codi=' . $tipoDocumento; */
        if ($tipoRadicado != null)
            $where .=" and substr(cast(r.radi_nume_radi as char(18)) ,14 ,1) ='$tipoRadicado'";
//la linea 254 era WHERE r.RADI_usua_radi=$idusua and r.RADI_USUA_actu=b.USUA_CODI
        $sql = "SELECT  distinct r.radi_nume_radi as RADICADO,
                        d.depe_nomb ndepeA,
                        b.USUA_NOMB as USUARIO, 
                        r.ra_asun asun,
                        r.radi_fech_radi as fech,
                        g.sgd_dir_nomremdes remitente
                        FROM RADICADO r , USUARIO b , dependencia d,sgd_dir_drecciones g
                            WHERE 
                          r.radi_fech_radi between  '$fechIni' and '$fechFin'  and 
                              r.radi_usua_actu=b.usua_CODI 
                              and g.radi_nume_radi=r.radi_nume_radi
                              and d.depe_codi=r.radi_depe_actu and g.sgd_dir_tipo=1
                              $where ORDER BY r.radi_nume_radi asc  ";
//to_char(r.radi_fech_radi, 'DD-MM-YYYY HH24:II:SS') as fech,
        $rs = $this->link->query($sql);
        $i = 0;
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                $combi [$i] ["depeA"] = $rs->fields ['NDEPEA'];
                $combi [$i] ["usuario"] = $rs->fields ['USUARIO'];
                $combi [$i] ["radicado"] = $rs->fields ['RADICADO'];
                $combi [$i] ["fech"] = $rs->fields ['FECH'];
                $combi [$i] ["asunto"] = $rs->fields ['ASUN'];
                $combi [$i] ["remitente"] = $rs->fields ['REMITENTE'];
                $i++;
                $rs->MoveNext();
            }
        } else {
            $combi['error'] = 'No se encontraron datos';
        }
        return $combi;
    }

    /**
     * Consulta Estadistica 19 RADICADOS REASIGNADOS
     * 
     */
    function est20($depe, $fechIni, $fechFin, $tipoRadicado) {
        $where = ' ';
        if ($tipoRadicado != '')
            $where .=" and t.sgd_trad_codigo='$tipoRadicado'";
        if ($depe != '99999')
            $where .=' and r.radi_depe_radi =' . $depe;
        $sql = " select r.radi_depe_radi depe,t.sgd_trad_descr tiporadq,count(1) num,t.sgd_trad_codigo tpr 
                 from radicado r ,sgd_trad_tiporad t where r.radi_depe_radi!=998 and r.radi_depe_radi!=999 and
                 r.radi_fech_radi between to_timestamp('$fechIni','dd/mm/yyyy hh24:mi:ss') and to_timestamp('$fechFin','dd/mm/yyyy hh24:mi:ss')
                and substr(cast(r.radi_nume_radi as char(18)) , 14 , 1)=cast(t.sgd_trad_codigo as char(1))
                 $where
                 group   by r.radi_depe_radi,t.sgd_trad_descr,t.sgd_trad_codigo order by r.radi_depe_radi asc";

       $rs = $this->link->query($sql);

        $i = 0;
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                foreach ($rs->fields as $key => $valor)
                    $combi [$i][strtoupper($key)] = $valor;

                $i++;
                $rs->MoveNext();
            }
        } else {
            $combi['error'] = 'No se encontraron datos';
        }
        return $combi;
    }
function detallesEst20($depe, $fechIni, $fechFin, $tipoRadicado) {
        $where = ' ';
       // if ($tipoRadicado != '')
            //$where .=" and t.sgd_trad_codigo='$tipoRadicado'";
        if ($depe != '99999')
            $where .=' and r.radi_depe_radi =' . $depe;
        $sql = " select r.*,d.*, substr(cast(r.radi_nume_radi as char(18)) ,8 ,6) as conse
                 from radicado r ,sgd_dir_drecciones d where r.radi_depe_radi!=998 and r.radi_depe_radi!=999 and
                 r.radi_fech_radi between to_timestamp('$fechIni','dd/mm/yyyy hh24:mi:ss') and to_timestamp('$fechFin','dd/mm/yyyy hh24:mi:ss')  and r.radi_nume_radi =d.radi_nume_radi 
                and substr(cast(r.radi_nume_radi as char(18)) , 14 , 1)='$tipoRadicado' and sgd_dir_tipo=1
                 $where
                 order by conse asc";

       $rs = $this->link->query($sql);

        $i = 0;
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                foreach ($rs->fields as $key => $valor)
                    $combi [$i][strtoupper($key)] = $valor;

                $i++;
                $rs->MoveNext();
            }
        } else {
            $combi['error'] = 'No se encontraron datos';
        }
        return $combi;
    }
    function consultarDepe() {
         $q = "select * from dependencia where depe_estado=1 order by depe_codi asc ";
        //where depe_codi =$codigo
        $rs = $this->link->query($q);
        $retorno = array();
        if (!$rs->EOF) {
            $i = 0;
            while (!$rs->EOF) {
                foreach ($rs->fields as $key => $valor) {
                    $combi [$rs->fields ['DEPE_CODI']][strtolower($key)] = $valor;
                }
                      $rs->MoveNext();
            }
        } else {
            $combi ['Error'] = 'No se contienen datos';
        }
        return $combi;
    }
        function consultarUsu() {
         $q = "select * from usuario  order by usua_codi asc ";
        //where depe_codi =$codigo
        $rs = $this->link->query($q);
        $retorno = array();
        if (!$rs->EOF) {
            $i = 0;
            while (!$rs->EOF) {
                foreach ($rs->fields as $key => $valor) {
                    $combi [$rs->fields ['USUA_CODI']][strtolower($key)] = $valor;
                }
                      $rs->MoveNext();
            }
        } else {
            $combi ['Error'] = 'No se contienen datos';
        }
        return $combi;
    }
   function consultarMuni() {
         $q = "select * from municipio";
        //where depe_codi =$codigo
        $rs = $this->link->query($q);
        $retorno = array();
        if (!$rs->EOF) {
            $i = 0;
            while (!$rs->EOF) {
                foreach ($rs->fields as $key => $valor) {
                    $da=$rs->fields ['ID_PAIS'].'-'.$rs->fields ['DPTO_CODI'].'-'.$rs->fields ['MUNI_CODI'];
                    $combi [$da][strtoupper($key)] = $valor;
                }
                      $rs->MoveNext();
            }
        } else {
            $combi ['Error'] = 'No se contienen datos';
        }
        return $combi;
    }
       function consultarDepto() {
         $q = "select * from departamento ";
        //where depe_codi =$codigo
        $rs = $this->link->query($q);
        $retorno = array();
        if (!$rs->EOF) {
            $i = 0;
            while (!$rs->EOF) {
                foreach ($rs->fields as $key => $valor) {
                    $da=$rs->fields ['ID_PAIS'].'-'.$rs->fields ['DPTO_CODI'];
                    $combi [$da][strtoupper($key)] = $valor;
                }
                      $rs->MoveNext();
            }
        } else {
            $combi ['Error'] = 'No se contienen datos';
        }
        return $combi;
    }
    function consultarTP() {
         $q = "select * from sgd_tpr_tpdcumento ";
        //where depe_codi =$codigo
        $rs = $this->link->query($q);
        $retorno = array();
        if (!$rs->EOF) {
            $i = 0;
            while (!$rs->EOF) {
                foreach ($rs->fields as $key => $valor) {
                    $combi [$rs->fields ['SGD_TPR_CODIGO']][strtolower($key)] = $valor;
                }
                      $rs->MoveNext();
            }
        } else {
            $combi ['Error'] = 'No se contienen datos';
        }
        return $combi;
    }
/*
Funcion de estadistica de creacion de expedientes
*/
function consultarDepExp($depe,$fecha_ini,$fecha_fin){
    $where="";
    if($depe!='99999'){
	$where.=" and s.depe_codi=$depe "; 
    }
    $sql="select d.depe_nomb, count(1) as expedientes from sgd_sexp_secexpedientes s, dependencia d, sgd_hfld_histflujodoc h 
	where h.sgd_exp_numero=s.sgd_exp_numero and  
	s.depe_codi=d.depe_codi and h.sgd_hfld_fech between to_timestamp('$fecha_ini 0:00:00','dd/mm/yyyy hh24:mi:ss') and to_timestamp('$fecha_fin 23:59:59','dd/mm/yyyy hh24:mi:ss') $where
	and h.sgd_ttr_codigo=51 group by  d.depe_nomb";
    $rs=$this->link->conn->Execute($sql);
    if(!$rs->EOF){
	$i=0;
	while(!$rs->EOF){
	    $retorno[$i]['depe_nomb']=$rs->fields['DEPE_NOMB'];
	    $retorno[$i]['expedientes']=$rs->fields['EXPEDIENTES'];
	    $i++;
	    $rs->MoveNext();
	}
    }else{
	$retorno="Error no se encontraron datos";
    }
    return $retorno;
}

function consultarCreacion($depe,$fecha_ini,$fecha_fin){
    $where="";
    if($depe!='99999'){
        $where.=" and s.depe_codi=$depe ";
    }
    $sql="select s.sgd_exp_numero, to_char(sgd_hfld_fech,'YYYY-MM-DD HH24:II:SS') as fecha_crea, d.depe_nomb, u1.usua_nomb as usua_crea, u2.usua_nomb as usua_responsable, 
	h.radi_nume_radi ,count(e.radi_nume_radi) as radicados, max(e.sgd_exp_titulo) as titulo, max(e.sgd_exp_asunto) as asunto
	from sgd_exp_expediente e, sgd_sexp_secexpedientes s,
	dependencia d,usuario u1, usuario u2, sgd_hfld_histflujodoc h where s.sgd_exp_numero=e.sgd_exp_numero and h.sgd_exp_numero=e.sgd_exp_numero and  
	u1.usua_doc=s.usua_doc and u2.usua_doc=s.usua_doc_responsable and s.depe_codi=d.depe_codi and h.sgd_hfld_fech 
	between to_timestamp('$fecha_ini 0:00:00','dd/mm/yyyy hh24:mi:ss') and to_timestamp('$fecha_fin 23:59:59','dd/mm/yyyy hh24:mi:ss') $where
	and h.sgd_ttr_codigo=51 and e.sgd_exp_estado<>2 group by s.sgd_exp_numero, sgd_hfld_fech, d.depe_nomb, u1.usua_nomb, u2.usua_nomb, 
	h.radi_nume_radi order by d.depe_nomb, sgd_hfld_fech";
    $rs=$this->link->conn->Execute($sql);
    $i=0;
    while(!$rs->EOF){
	$retorno[$i]['expediente']=$rs->fields['SGD_EXP_NUMERO'];
	$retorno[$i]['fecha_crea']=$rs->fields['FECHA_CREA'];
	$retorno[$i]['depe_nomb']=$rs->fields['DEPE_NOMB'];
        $retorno[$i]['usua_crea']=$rs->fields['USUA_CREA'];
        $retorno[$i]['usua_responsable']=$rs->fields['USUA_RESPONSABLE'];
	$retorno[$i]['radicados']=$rs->fields['RADICADOS'];
        $retorno[$i]['numradi']=$rs->fields['RADI_NUME_RADI'];
        $retorno[$i]['faro']=$rs->fields['SGD_SEXP_FARO'];
        $retorno[$i]['matrix']=$rs->fields['SGD_SEXP_MATRIX'];
	$retorno[$i]['titulo']=$rs->fields['TITULO'];
	$retorno[$i]['asunto']=$rs->fields['ASUNTO'];
	$i++;
	$rs->MoveNext();
    }
    return $retorno;
}

}

?>
