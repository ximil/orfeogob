<?php 
/**
 * modeloIndicadores es la clase encargada de gestionar las operaciones y los datos basicos referentes a los indicadores del sistema
 * @author Diego Enrique Rodriguez
 * @name modeloIndicadores
 * @version     1.0
 */
if (!$ruta_raiz)
    $ruta_raiz = '../../../..';
include_once "$ruta_raiz/core/clases/ConnectionHandler.php";

class modeloIndicadores {
    //link de conexion con la base de datos
    public $link;

    function __construct($ruta_raiz) {
        $db = new ConnectionHandler("$ruta_raiz");
        $db->conn->SetFetchMode(ADODB_FETCH_NUM);
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        $this->link = $db;
        if ($_SESSION['usua_debug'] == 1)
            $this->link->conn->debug = true;
    }

	function infoGestionDocumentos($tprad,$depe,$fecha_ini,$fecha_fin){
		if($depe==99999){
			$dependencia="";
		}
		else{
			$dependencia="and depe_codi_dest=$depe";
		}
		$sql="select case when r.radi_usua_actu=2 and r.tdoc_codi<>0 then 'Archivado' else 'Tramite' end as indicador, count(1) as radicados from radicado r, hist_eventos h
			where r.radi_nume_radi=h.radi_nume_radi and h.sgd_ttr_codigo=2 $dependencia and radi_fech_radi between to_date('$fecha_ini 0:00:00','dd/mm/yyyy hh24:mi:ss')
			and to_date('$fecha_fin 23:59:59','dd/mm/yyyy hh24:mi:ss') and substr(cast(r.radi_nume_radi as char(18)),14,1)='$tprad' and r.radi_depe_radi<>998
			group by case when r.radi_usua_actu=2 and r.tdoc_codi<>0 then 'Archivado' else 'Tramite' end";
		$rs=$this->link->conn->Execute($sql);
		$resultado=array();
		$total=0;
		$i=0;
		if(!$rs->EOF){
		    while(!$rs->EOF){
				$resultado[$i]['indicador']=$rs->fields['INDICADOR'];
				$resultado[$i]['radicados']=$rs->fields['RADICADOS'];
				$total=$total+$resultado[$i]['radicados'];
				$i++;
				$rs->MoveNext();
			}
		}
		return array($resultado,$total);
	}
	
	function infoSinImagen($tprad,$depe,$fecha_ini,$fecha_fin){
		if($depe==99999){
			$dependencia="";
		}
		else{
			$dependencia="and depe_codi_dest=$depe";
		}
		$sql="select case when radi_path is null or trim(radi_path)='' then 'Sin Imagen' else 'Con Imagen' end as indicador, count(1) as radicados from radicado r,
			hist_eventos h where h.radi_nume_radi=r.radi_nume_radi and h.sgd_ttr_codigo=2 $dependencia and radi_fech_radi between 
			to_date('$fecha_ini 0:00:00','dd/mm/yyyy hh24:mi:ss')and to_date('$fecha_fin 23:59:59','dd/mm/yyyy hh24:mi:ss') 
			and substr(cast(r.radi_nume_radi as char(18)),14,1)='$tprad' and r.radi_depe_radi<>998
			group by case when radi_path is null or trim(radi_path)='' then 'Sin Imagen' else 'Con Imagen' end";
		$rs=$this->link->conn->Execute($sql);
		$resultado=array();
		$total=0;
		$i=0;
		if(!$rs->EOF){
		    while(!$rs->EOF){
				$resultado[$i]['indicador']=$rs->fields['INDICADOR'];
				$resultado[$i]['radicados']=$rs->fields['RADICADOS'];
				$total=$total+$resultado[$i]['radicados'];
				$i++;
				$rs->MoveNext();
			}
		}
		return array($resultado,$total);
	}
	
	function infoAnulados($tprad,$depe,$fecha_ini,$fecha_fin){
		if($depe==99999){
			$dependencia="";
		}
		else{
			$dependencia="and radi_depe_radi=$depe";
		}
		$sql="select case when sgd_eanu_codigo is not null and sgd_eanu_codigo in (1,2) then 'Solicitado/Anulado' else 'Normal' end indicador, count(1) radicados 
			from radicado where radi_fech_radi between to_date('$fecha_ini 0:00:00','dd/mm/yyyy hh24:mi:ss') and 
			to_date('$fecha_fin 23:59:59','dd/mm/yyyy hh24:mi:ss') $dependencia and substr(cast(radi_nume_radi as char(18)),14,1)='$tprad' and radi_depe_radi<>998
			group by case when sgd_eanu_codigo is not null and sgd_eanu_codigo in (1,2) then 'Solicitado/Anulado' else 'Normal' end";
		$rs=$this->link->conn->Execute($sql);
		$resultado=array();
		$total=0;
		$i=0;
		if(!$rs->EOF){
		    while(!$rs->EOF){
				if($rs->fields['INDICADOR']!='Normal'){
					$resultado[$i]['indicador']=$rs->fields['INDICADOR'];
					$resultado[$i]['radicados']=$rs->fields['RADICADOS'];
					$i++;
				}
				$total=$total+$rs->fields['RADICADOS'];
				$rs->MoveNext();
			}
		}
		if(empty($resultado) && $total!=0){
		    $resultado[$i]['indicador']='Solicitado/Anulado';
		    $resultado[$i]['radicados']=0;
		}
		return array($resultado,$total);
	}
	
	function infoOrigenTx($tprad,$depe,$fecha_ini,$fecha_fin){
		if($depe==99999){
			$dependencia="";
		}
		else{
			$dependencia="and radi_depe_radi=$depe";
		}
		$sql="select case when radi_nume_deri=0 or radi_nume_deri is null then 'Principal' else 'Anexo y/o respuesta' end indicador, count(1) radicados
			from radicado where radi_fech_radi between to_date('$fecha_ini 0:00:00','dd/mm/yyyy hh24:mi:ss') and 
			to_date('$fecha_fin 23:59:59','dd/mm/yyyy hh24:mi:ss') $dependencia and substr(cast(radi_nume_radi as char(18)),14,1)='$tprad' and radi_depe_radi<>998
			group by case when radi_nume_deri=0 or radi_nume_deri is null then 'Principal' else 'Anexo y/o respuesta' end";
		$rs=$this->link->conn->Execute($sql);
		$resultado=array();
		$total=0;
		$i=0;
		if(!$rs->EOF){
		    while(!$rs->EOF){
				$resultado[$i]['indicador']=$rs->fields['INDICADOR'];
				$resultado[$i]['radicados']=$rs->fields['RADICADOS'];
				$total=$total+$resultado[$i]['radicados'];
				$i++;
				$rs->MoveNext();
			}
		}
		return array($resultado,$total);
	}
	
	function infoCorresExt($tprad,$depe,$fecha_ini,$fecha_fin){
		if($depe==99999){
			$dependencia="";
		}
		else{
			$dependencia="and radi_depe_radi=$depe";
		}
		$sql="select count(radi_nume_radi) radicadon, count(re.radicados) enviados,count(devolucion) devueltos from radicado r
			left outer join (select radi_nume_sal radicados, count(sgd_renv_codigo) envio, min(sgd_deve_codigo) devolucion from sgd_renv_regenvio
			where sgd_renv_planilla is null or sgd_renv_planilla<>'00' and sgd_renv_fech>=to_date('$fecha_ini 0:00:00','dd/mm/yyyy hh24:mi:ss') 
			group by radi_nume_sal) re on re.radicados=r.radi_nume_radi where radi_fech_radi between to_date('$fecha_ini 0:00:00','dd/mm/yyyy hh24:mi:ss') 
			and to_date('$fecha_fin 23:59:59','dd/mm/yyyy hh24:mi:ss') $dependencia and substr(cast(radi_nume_radi as char(18)),14,1)='$tprad' and r.radi_depe_radi<>998";
		$rs=$this->link->conn->Execute($sql);
		$resultado=array();
		$total=0;
		$i=0;
		if(!$rs->EOF){
		    while(!$rs->EOF){
				$resultado[0]['indicador']='Enviados';
				$resultado[0]['radicados']=$rs->fields['ENVIADOS'];
				$resultado[1]['indicador']='Devueltos';
				$resultado[1]['radicados']=$rs->fields['DEVUELTOS'];
				$total=$rs->fields['RADICADON'];
				$rs->MoveNext();
			}
		}
		return array($resultado,$total);
	}
	
	function infoOficioTr($tprad,$depe,$fecha_ini,$fecha_fin){
		if($depe==99999){
			$dependencia="";
		}
		else{
			$dependencia="and radi_depe_radi=$depe";
		}
		$sql="select case when radi_nume_salida is null then 'Sin Documento' when anex_estado=4 then 'Enviado' 
			when sgd_deve_codigo is not null then 'Devueltos' when anex_estado=3 then 'Impreso sin envio'
			else 'Radicados sin Impresion' end indicador,count(radi_nume_radi) radicados from radicado r left outer join anexos a 
			on radi_nume_radi=radi_nume_salida
			where substr(cast(radi_nume_radi as char(18)),14,1)='$tprad' and radi_fech_radi between 
			to_date('$fecha_ini 0:00:00','dd/mm/yyyy hh24:mi:ss') and to_date('$fecha_fin 23:59:59','dd/mm/yyyy hh24:mi:ss') $dependencia
			and substr(cast(radi_nume_radi as char(18)),14,1)='$tprad' and r.radi_depe_radi<>998 group by case when radi_nume_salida is null then 'Sin Documento'
			when anex_estado=4 then 'Enviado' when sgd_deve_codigo is not null 
			then 'Devueltos' when anex_estado=3 then 'Impreso sin envio' else 'Radicados sin Impresion' end";
		$rs=$this->link->conn->Execute($sql);
		$resultado=array();
		$total=0;
		$i=0;
		if(!$rs->EOF){
		    while(!$rs->EOF){
				$resultado[$i]['indicador']=$rs->fields['INDICADOR'];
				$resultado[$i]['radicados']=$rs->fields['RADICADOS'];
				$total=$total+$resultado[$i]['radicados'];
				$i++;
				$rs->MoveNext();
			}
		}
		return array($resultado,$total);
	}
	
	function infoOrigenTr($tprad,$fecha_ini,$fecha_fin){
		$sql="select 'Ventanilla' as indicador, count(radi_nume_radi) as radicados from radicado where mrec_codi<>12 and 
			substr(cast(radi_nume_radi as char(18)),14,1)='$tprad' and radi_fech_radi between to_date('$fecha_ini 0:00:00','dd/mm/yyyy hh24:mi:ss') and
			to_date('$fecha_fin 23:59:59','dd/mm/yyyy hh24:mi:ss') and radi_depe_radi<>998
		union
		select 'Formularios' as indicador, count(r.radi_nume_radi) as radicados from sgd_rdf_retdocf rdf left outer join fow_base_baseinfo fow on 
			fow.sgd_mrd_codigo=rdf.sgd_mrd_codigo, radicado r where r.radi_nume_radi=rdf.radi_nume_radi and r.mrec_codi=12 and 
			substr(cast(r.radi_nume_radi as char(18)),14,1)='$tprad' and radi_fech_radi between to_date('$fecha_ini 0:00:00','dd/mm/yyyy hh24:mi:ss') and
			to_date('$fecha_fin 23:59:59','dd/mm/yyyy hh24:mi:ss') and r.radi_depe_radi<>998";

		$rs=$this->link->conn->Execute($sql);
		$resultado=array();
		$total=0;
		$i=0;
		if(!$rs->EOF){
		    while(!$rs->EOF){
				$resultado[$i]['indicador']=$rs->fields['INDICADOR'];
				$resultado[$i]['radicados']=$rs->fields['RADICADOS'];
				$total=$total+$resultado[$i]['radicados'];
				$i++;
				$rs->MoveNext();
			}
		}
		return array($resultado,$total);
	}

	function totalRadicados($trad,$fecha_ini,$fecha_fin){
	    $ssql="select count(1) as total from radicado where  substr(cast(radi_nume_radi as char(18)),14,1)='$trad' and 
		radi_fech_radi between to_date('$fecha_ini 0:00:00','dd/mm/yyyy hh24:mi:ss') and to_date('$fecha_fin 23:59:59','dd/mm/yyyy hh24:mi:ss') and radi_depe_radi<>998";
	    $rs=$this->link->conn->Execute($ssql);
	    if(!$rs->EOF){
		$total=$rs->fields['TOTAL'];
            }
	    return $total;
	}
}
