<?php 
/**
 * modeloTpDoc es la clase encargada de gestionar las operaciones y los datos basicos referentes a los tipos de documento
 * @author Hardy Deimont Niño  Velasquez
 * @name modeloTpDoc
 * @version	1.0
 */
if (!$ruta_raiz)
    $ruta_raiz = '../../../..';
//include_once "$ruta_raiz/include/db/ConnectionHandler.php";
include_once "$ruta_raiz/core/clases/ConnectionHandler.php";

class modeloEstadisticas {

    public $link;

    function __construct($ruta_raiz) {

        $db = new ConnectionHandler("$ruta_raiz");
        $db->conn->SetFetchMode(ADODB_FETCH_NUM);
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
       // $db->conn->debug = true;
        $this->link = $db;
    }

    /**
     * @return consulta los datos de tpdocumentos.
     */
    function consultar($subserie, $depe, $serieid, $ano) {
      $isqlC = "select distinct (select distinct e.sgd_exp_titulo from sgd_exp_expediente e where sgd_exp_titulo is not null and e.sgd_exp_numero=exp.sgd_exp_numero ) titulo,sec.sgd_exp_numero , sec.sgd_sexp_ano
		from sgd_sexp_secexpedientes  sec,sgd_exp_expediente exp
		where
		sec.depe_codi=$depe and
		sec.sgd_srd_codigo=$serieid
		and sec.sgd_sbrd_codigo=$subserie
		and sgd_sexp_ano='$ano' and
		sec.sgd_exp_numero=exp.sgd_exp_numero
		order by sec.sgd_exp_numero asc";
        //and exp.sgd_exp_titulo is not null  sgd_exp_titulo is not null and 
        $rs = $this->link->query($isqlC);
        $i = 0;
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                $combi [$i] ["numero"] = $rs->fields ['SGD_EXP_NUMERO'];
                $combi [$i] ["year"] = $rs->fields ['SGD_SEXP_ANO'];
                $combi [$i] ["titulo"] = $rs->fields ['TITULO'];

                $i++;
                $rs->MoveNext();
            }
        } else {
            $combi['error'] = 'No se encontraron datos';
        }
        return $combi;
    }

    function est5($fechIni, $fechFin, $depe, $estado) {
        if ($depe == '99999') {
            $where = '';
        }
        else
            $where = 'and r.radi_depe_radi=' . $depe;
        $isqlC = "select u.usua_codi ucodi,u.usua_nomb USUARIO, count(9) RADICADOS  
                from radicado r, usuario u 
                where r.radi_fech_radi between '$fechIni' and '$fechFin' and r.radi_usua_radi=u.usua_codi 
                and substring(cast(r.radi_nume_radi as char(18)) from 14 for 1)='2' $where 
                group by u.usua_nomb,u.usua_codi
                order by u.usua_nomb,u.usua_codi,count(9)  desc";

        $rs = $this->link->query($isqlC);
        $i = 0;
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                $combi [$i] ["ucodi"] = $rs->fields ['UCODI'];
                $combi [$i] ["usuario"] = $rs->fields ['USUARIO'];
                $combi [$i] ["radicados"] = $rs->fields ['RADICADOS'];
                $i++;
                $rs->MoveNext();
            }
        } else {
            $combi['error'] = 'No se encontraron datos';
        }
        return $combi;
    }

    function detallesEst5($fechIni, $fechFin, $idusua) {
        if ($depe == '99999') {
            $where = '';
        }
        else
            $where = 'and r.radi_depe_radi=' . $depe;
          
        $isqlC = "select u.usua_codi ucodi,u.usua_nomb USUARIO,r.radi_nume_radi radicados,
                     r.ra_asun asun,  TO_CHAR(r.radi_fech_radi, 'YYYY/MM/DD HH24:MI:SS')  fech
                        from radicado r, usuario u 
                        where r.radi_fech_radi between  '$fechIni' and '$fechFin'
                        and substring(cast(r.radi_nume_radi as char(18)) from 14 for 1)='2'    
                        and r.radi_usua_radi=u.usua_codi and r.radi_usua_radi=$idusua  order by r.radi_nume_radi,r.radi_fech_radi desc  ";

        $rs = $this->link->query($isqlC);
        $i = 0;
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                $combi [$i] ["ucodi"] = $rs->fields ['UCODI'];
                $combi [$i] ["usuario"] = $rs->fields ['USUARIO'];
                $combi [$i] ["radicados"] = $rs->fields ['RADICADOS'];
                $combi [$i] ["fech"] = $rs->fields ['FECH'];
                $combi [$i] ["asunto"] = $rs->fields ['ASUN'];
                $i++;
                $rs->MoveNext();
            }
        } else {
            $combi['error'] = 'No se encontraron datos';
        }
        return $combi;
    }

    function est6($depe, $usua, $estado, $tipoRadicado, $tipoDocumento) {
        $where = ' ';
        if ($tipoDocumento != 9999)
            $where .=' and r.tdoc_codi=' . $tipoDocumento;
        if ( $tipoRadicado!= null)
            $where .=" and substring(cast(r.radi_nume_radi as char(18)) from 14 for 1)='$tipoRadicado'";

        if ($usua != 0)
            $where .=' and b.usua_codi=' . $usua;

        if ($estado == 0)
            $where .=" and b.USUA_esta='1' ";

        if ($depe != '99999')
            $where .=' and r.radi_depe_actu=' . $depe;
        $sql = "SELECT b.USUA_CODI ucodi,b.USUA_NOMB as USUARIO,r.radi_depe_actu, count(cast(r.radi_nume_radi as varchar(18)) ) as 
RADICADOS , MIN(b.USUA_CODI) as HID_COD_USUARIO 
FROM RADICADO r, USUARIO b, sgd_urd_usuaroldep urd 
WHERE r.RADI_USUA_ACTU=b.USUA_CODI AND r.RADI_DEPE_ACTU=urd.DEPE_CODI 
and urd.usua_codi=b.USUA_CODI AND r.radi_depe_actu=urd.depe_codi  $where
GROUP BY b.USUA_NOMB,r.radi_depe_actu,b.USUA_CODI ORDER BY 1";

        $rs = $this->link->query($sql);
        $i = 0;
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                $combi [$i] ["ucodi"] = $rs->fields ['UCODI'];
                $combi [$i] ["usuario"] = $rs->fields ['USUARIO'];
                $combi [$i] ["radicados"] = $rs->fields ['RADICADOS'];
                $i++;
                $rs->MoveNext();
            }
        } else {
            $combi['error'] = 'No se encontraron datos';
        }
        return $combi;
    }

    /* 	select sgd_exp_numero,sgd_sexp_ano from sgd_sexp_secexpedientes
      where
      depe_codi=$depe and
      sgd_srd_codigo=$serieid
      and sgd_sbrd_codigo=$subserie
      and sgd_sexp_ano='$ano' */

    function detallesEst6($idusua, $tipoRadicado, $tipoDocumento) {
        $where = ' ';
        if ($tipoDocumento != 9999)
            $where .=' and r.tdoc_codi=' . $tipoDocumento;
        if ( $tipoRadicado!= null)
            $where .=" and substring(cast(r.radi_nume_radi as char(18)) from 14 for 1) ='$tipoRadicado'";

        $sql = "SELECT  b.USUA_CODI ucodi,b.USUA_NOMB as USUARIO,r.radi_depe_actu, r.radi_nume_radi  as RADICADOS,
              r.ra_asun asun, r.radi_usu_ante uante,tp.sgd_tpr_descrip tpdesc, r.radi_fech_radi fech
FROM RADICADO r, USUARIO b, sgd_urd_usuaroldep urd,sgd_tpr_tpdcumento tp
 WHERE r.RADI_USUA_ACTU=$idusua and r.RADI_USUA_ACTU=b.USUA_CODI AND r.RADI_DEPE_ACTU=urd.DEPE_CODI and 
     urd.usua_codi=b.USUA_CODI AND r.radi_depe_actu=urd.depe_codi and b.USUA_esta='1'  and
     r.tdoc_codi=tp.sgd_tpr_codigo $where ORDER BY 1    ";

        $rs = $this->link->query($sql);
        $i = 0;
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                $combi [$i] ["ucodi"] = $rs->fields ['UCODI'];
                $combi [$i] ["usuario"] = $rs->fields ['USUARIO'];
                $combi [$i] ["radicados"] = $rs->fields ['RADICADOS'];
                $combi [$i] ["fech"] = $rs->fields ['FECH'];
                $combi [$i] ["asunto"] = $rs->fields ['ASUN'];
                $combi [$i] ["uante"] = $rs->fields ['UANTE'];
                $combi [$i] ["tpdesc"] = $rs->fields ['TPDESC'];
                $i++;
                $rs->MoveNext();
            }
        } else {
            $combi['error'] = 'No se encontraron datos';
        }
        return $combi;
    }

    
      function est1($depe,$fechIni,$fechFin, $usua, $estado, $tipoRadicado, $tipoDocumento) {
        $where = ' ';
        if ($tipoDocumento != 9999)
            $where .=' and r.tdoc_codi=' . $tipoDocumento;
        if ( $tipoRadicado!= null)
            $where .=" and substring(cast(r.radi_nume_radi as char(18)) from 14 for 1)='$tipoRadicado'";

        if ($usua != 0)
            $where .=' and b.usua_codi=' . $usua;

        if ($estado == 0)
            $where .=" and b.USUA_esta='1' ";

        if ($depe != '99999')
            $where .=' and r.radi_depe_actu=' . $depe;
         $sql = "SELECT b.USUA_NOMB as USUARIO, count(*) as RADICADOS, MIN(b.USUA_CODI) as UCODI 
                FROM RADICADO r , USUARIO b WHERE 
                r.radi_fech_radi between  '$fechIni' and '$fechFin'
                and r.radi_usua_actu=b.usua_CODI 
                $where
                 GROUP BY b.USUA_NOMB ORDER BY 1 , 2 ";

        $rs = $this->link->query($sql);
        $i = 0;
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                $combi [$i] ["ucodi"] = $rs->fields ['UCODI'];
                $combi [$i] ["usuario"] = $rs->fields ['USUARIO'];
                $combi [$i] ["radicados"] = $rs->fields ['RADICADOS'];
                $i++;
                $rs->MoveNext();
            }
        } else {
            $combi['error'] = 'No se encontraron datos';
        }
        return $combi;
    }

    /* 	select sgd_exp_numero,sgd_sexp_ano from sgd_sexp_secexpedientes
      where
      depe_codi=$depe and
      sgd_srd_codigo=$serieid
      and sgd_sbrd_codigo=$subserie
      and sgd_sexp_ano='$ano' */

    function detallesEst1($fechIni,$fechFin, $idusua, $tipoRadicado, $tipoDocumento) {
        $where = ' ';
        if ($tipoDocumento != 9999)
            $where .=' and r.tdoc_codi=' . $tipoDocumento;
        if ( $tipoRadicado!= null)
            $where .=" and substring(cast(r.radi_nume_radi as char(18)) from 14 for 1) ='$tipoRadicado'";

         $sql = "SELECT  b.USUA_CODI ucodi,b.USUA_NOMB as USUARIO,r.radi_depe_actu, r.radi_nume_radi  as RADICADOS,
              r.ra_asun asun, r.radi_usu_ante uante,tp.sgd_tpr_descrip tpdesc, r.radi_fech_radi fech
FROM RADICADO r, USUARIO b, sgd_urd_usuaroldep urd,sgd_tpr_tpdcumento tp
 WHERE r.RADI_usua_actu=$idusua and r.RADI_USUA_actu=b.USUA_CODI 
     and      urd.usua_codi=b.USUA_CODI and
     r.radi_fech_radi between  '$fechIni' and '$fechFin' and
     r.tdoc_codi=tp.sgd_tpr_codigo $where ORDER BY 1  ";

        $rs = $this->link->query($sql);
        $i = 0;
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                $combi [$i] ["ucodi"] = $rs->fields ['UCODI'];
                $combi [$i] ["usuario"] = $rs->fields ['USUARIO'];
                $combi [$i] ["radicados"] = $rs->fields ['RADICADOS'];
                $combi [$i] ["fech"] = $rs->fields ['FECH'];
                $combi [$i] ["asunto"] = $rs->fields ['ASUN'];
                $combi [$i] ["uante"] = $rs->fields ['UANTE'];
                $combi [$i] ["tpdesc"] = $rs->fields ['TPDESC'];
                $i++;
                $rs->MoveNext();
            }
        } else {
            $combi['error'] = 'No se encontraron datos';
        }
        return $combi;
    }
    
     function est2($depe,$fechIni,$fechFin, $usua, $estado, $tipoRadicado) {
        $where = ' ';
        if ( $tipoRadicado!= null)
            $where .=" and substring(cast(r.radi_nume_radi as char(18)) from 14 for 1)='$tipoRadicado'";

        if ($usua != 0)
            $where .=' and b.usua_codi=' . $usua;

        if ($estado == 0)
            $where .=" and b.USUA_esta='1' ";
        if ($idusua != 0)
            $where .=' and r.RADI_usua_radi=' . $idusua;
         if ($depe != '99999')
            $where .=' and r.radi_depe_radi=' . $depe;
/*
        if ($depe != '99999')
            $where .=' and r.radi_depe_actu=' . $depe;*/
          $sql = "SELECT c.mrec_desc AS MEDIO_RECEPCION, COUNT(*) AS Radicados, max(c.MREC_CODI) AS HID_MREC_CODI 
                 FROM RADICADO r, MEDIO_RECEPCION c, USUARIO b, sgd_urd_usuaroldep urd 
                 WHERE r.radi_usua_radi=b.usua_CODI AND r.mrec_codi=c.mrec_codi and 
                 urd.usua_codi=b.USUA_CODI AND r.radi_depe_radi=urd.DEPE_CODI and
                 r.radi_fech_radi between  '$fechIni' and '$fechFin'
                 $where
                 GROUP BY c.mrec_desc ORDER BY 1 , 2 ";

        $rs = $this->link->query($sql);
        $i = 0;
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                $combi [$i] ["ucodi"] = $rs->fields ['HID_MREC_CODI'];
                $combi [$i] ["usuario"] = $rs->fields ['MEDIO_RECEPCION'];
                $combi [$i] ["radicados"] = $rs->fields ['RADICADOS'];
                $i++;
                $rs->MoveNext();
            }
        } else {
            $combi['error'] = 'No se encontraron datos';
        }
        return $combi;
    }
    
     function detallesEst2($fechIni,$fechFin, $idusua, $tipoRadicado, $tipoDocumento,$tpenvio,$depe,$estado) {
        $where = ' ';
        if ($estado == 0)
            $where .=" and b.USUA_esta='1' ";
        if ($tipoDocumento != 9999)
            $where .=' and r.tdoc_codi=' . $tipoDocumento;
        if ($idusua != 0)
            $where .=' and r.RADI_usua_radi=' . $idusua;
        if ($depe != '99999')
            $where .=' and r.radi_depe_radi=' . $depe;
        if ( $tipoRadicado!= null)
            $where .=" and substring(cast(r.radi_nume_radi as char(18)) from 14 for 1) ='$tipoRadicado'";
           $sql = "SELECT c.mrec_desc AS MEDIO_RECEPCION, b.USUA_CODI ucodi,b.USUA_NOMB as USUARIO,r.radi_depe_actu, r.radi_nume_radi  as RADICADOS,
              r.ra_asun asun, r.radi_usu_ante uante,tp.sgd_tpr_descrip tpdesc, r.radi_fech_radi fech
FROM RADICADO r, USUARIO b, sgd_urd_usuaroldep urd,sgd_tpr_tpdcumento tp, MEDIO_RECEPCION c
 WHERE r.RADI_USUA_RADI=b.USUA_CODI 
     and      urd.usua_codi=b.USUA_CODI and  r.mrec_codi=$tpenvio and  r.mrec_codi=c.mrec_codi and
     r.radi_fech_radi between  '$fechIni' and '$fechFin' and
     r.tdoc_codi=tp.sgd_tpr_codigo $where ORDER BY 1  ";

        $rs = $this->link->query($sql);
        $i = 0;
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                $combi [$i] ["ucodi"] = $rs->fields ['UCODI'];
                $combi [$i] ["usuario"] = $rs->fields ['USUARIO'];
                $combi [$i] ["radicados"] = $rs->fields ['RADICADOS'];
                $combi [$i] ["fech"] = $rs->fields ['FECH'];
                $combi [$i] ["asunto"] = $rs->fields ['ASUN'];
                $combi [$i] ["uante"] = $rs->fields ['UANTE'];
                $combi [$i] ["tpdesc"] = $rs->fields ['TPDESC'];
                $combi [$i] ["mrec"] = $rs->fields ['MEDIO_RECEPCION'];
                $i++;
                $rs->MoveNext();
            }
        } else {
            $combi['error'] = 'No se encontraron datos';
        }
        return $combi;
    }

        /*************************************************************
	**                                                          **
	**  ESTADISTICAS DE MEDIO DE ENVIO FINAL DE DOCUMENTOS      **
	**                                                          **
	*************************************************************/
	function est3($depe,$fechIni,$fechFin) {
        $where = ' ';
 
         if ($depe != '99999')
            $where .=' and c.depe_codi=' . $depe;

		$sql = "select d.sgd_fenv_descrip,d.sgd_fenv_codigo, COUNT(*) as RADICADOS
				from SGD_RENV_REGENVIO a, dependencia c, SGD_FENV_FRMENVIO d 
				WHERE a.sgd_renv_fech BETWEEN '$fechIni' and '08/07/2013 23:59:59' and substr(cast(a.radi_nume_sal as varchar(18)), 5, 3)=cast(c.depe_codi as varchar(6)) and a.sgd_fenv_codigo=d.sgd_fenv_codigo
				and a.sgd_renv_planilla != '' and  a.sgd_renv_planilla != '00' $where
				GROUP BY d.sgd_fenv_descrip,d.sgd_fenv_codigo";
		
		//print $sql;

        $rs = $this->link->query($sql);
        $i = 0;
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                $combi [$i] ["radicados"] = $rs->fields ['RADICADOS'];
                $combi [$i] ["idmedio"] = $rs->fields ['SGD_FENV_CODIGO'];
                $combi [$i] ["nombremed"] = $rs->fields ['SGD_FENV_DESCRIP'];
                $i++;
                $rs->MoveNext();
            }
        } else {
            $combi['error'] = 'No se encontraron datos';
        }
        return $combi;
    }
	
	/*************************************************************
	**  		DETALLES DE LA ESTADISTICA NUMERO 3	    **
	**	ESTADISTICAS DE MEDIO DE ENVIO FINAL DE DOCUMENTOS  **
	*************************************************************/
	
	function detallesEst3($depe,$fechIni,$fechFin,$idmedio) {
        $where = ' ';
         if ($depe != '99999')
            $where .=' and c.depe_codi=' . $depe;
		$sql = "select c.depe_nomb, a.radi_nume_sal, a.sgd_renv_nombre, a.sgd_renv_dir , a.sgd_renv_mpio, a.sgd_renv_depto, a.sgd_renv_fech, a.sgd_renv_planilla, a.sgd_renv_cantidad, a.sgd_renv_valor, a.sgd_deve_fech, d.sgd_fenv_descrip 
		from SGD_RENV_REGENVIO a, dependencia c, SGD_FENV_FRMENVIO d
		WHERE a.sgd_renv_fech BETWEEN '$fechIni' and '$fechFin' and substr(cast(a.radi_nume_sal as varchar(18)), 5, 3)=cast(c.depe_codi as varchar(6)) 
		and a.sgd_fenv_codigo=d.sgd_fenv_codigo and a.sgd_fenv_codigo=$idmedio
		$where
		ORDER BY substr(cast(a.radi_nume_sal as varchar(18)), 5, 3), a.SGD_RENV_FECH DESC,a.SGD_RENV_PLANILLA DESC ";
		
		//print $sql;

        $rs = $this->link->query($sql);
        $i = 0;
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                $combi [$i] ["radicado"] = $rs->fields ['RADI_NUME_SAL'];
                $combi [$i] ["rnombre"] = $rs->fields ['SGD_RENV_NOMBRE'];
                $combi [$i] ["rdireccion"] = $rs->fields ['SGD_RENV_DIR'];
				$combi [$i] ["rmunicipio"] = $rs->fields ['SGD_RENV_MPIO'];
				$combi [$i] ["rdepartamento"] = $rs->fields ['SGD_RENV_DEPTO'];
				$combi [$i] ["rfecha"] = $rs->fields ['SGD_RENV_FECH'];
				$combi [$i] ["planilla"] = $rs->fields ['SGD_RENV_PLANILLA'];
                $i++;
                $rs->MoveNext();
            }
        } else {
            $combi['error'] = 'No se encontraron datos';
        }
        return $combi;
    }
	
        /*********************************************
	**                                          **
	**  ESTADISTICA 10 RADICADOS POR BANDEJA    **
	**                                          **
	*********************************************/
	function est10($depe,$usua,$estado,$tipoDocumento) {
        $where = ' ';
		$where2 = ' ';
		$where3 = ' ';
        if ($tipoDocumento != 9999)
		{
            $where2 =' and r.tdoc_codi='.$tipoDocumento;
			$where3 =' and tdoc_codi='.$tipoDocumento;
		}

        if ($usua != 0)
            $where .=' and u.usua_codi=' . $usua;

        if ($estado == 0)
            $where .=" and u.USUA_esta='1' ";

        if ($depe != '99999')
            $where .=' and d.DEPE_CODI =' . $depe;
			
         $sql = "select u.USUA_NOMB as USUARIO, MIN(u.USUA_CODI) as UCODI, u.USUA_DOC as DOCUMENTO, d.DEPE_CODI as DEPENDENCIA, d.ROL_CODI as ROL FROM USUARIO u, sgd_urd_usuaroldep d
where u.USUA_CODI=d.USUA_CODI 
                $where
                 GROUP BY u.USUA_NOMB,u.USUA_DOC,d.DEPE_CODI, d.ROL_CODI ORDER BY 1 , 2 ";
		//print $sql;
        $rs = $this->link->query($sql);
		
        $i = 0;
        if (!$rs->EOF) {
            while (!$rs->EOF) {
				$codigous = $rs->fields ['UCODI'];
				$documento = $rs->fields ['DOCUMENTO'];
				$rol = $rs->fields ['ROL'];
				$dependencia = $rs->fields ['DEPENDENCIA'];
				
				$sql1 = "select  DISTINCT ON  ( nomb_carp ) CODI_CARP,DESC_CARP,NOMB_CARP,(SELECT count(1) CUENTA  FROM   radicado r
            WHERE  r.carp_per=1 and carp_codi=CODI_CARP AND  r.id_rol=$rol AND   r.radi_usua_actu =$codigous AND
            r.radi_depe_actu =$dependencia $where2) cuenta from carpeta_per where usua_codi=$codigous and id_rol =$rol and depe_codi=$dependencia order by nomb_carp";
				//print $sql1;
				$rs2 = $this->link->query($sql1);
				$a = 0;
				if (!$rs2->EOF) {
					while (!$rs2->EOF) {
						
						$combi [$i] ["personales"] [$a] ["namecarp"] = $rs2->fields ['NOMB_CARP'];
						$combi [$i] ["personales"] [$a] ["idcarpeta"] = $rs2->fields ['CODI_CARP'];
						$combi [$i] ["personales"] [$a] ["totalcarp"] = $rs2->fields ['CUENTA'];
						$a++;
						$rs2->MoveNext();
					}
				}
				else
				{
				}
				
				$sql2 = "select c.carp_codi CODI_CARP, carp_desc CARP_DESC,count(carp_desc) CUENTA from carpeta c, radicado r where c.carp_codi<>11 and  
		r.carp_codi=c.carp_codi and  r.radi_depe_actu=$dependencia
						and r.radi_usua_actu=$codigous and id_rol=$rol and r.carp_per=0 and r.radi_usua_doc_actu='$documento' $where2 group by c.carp_codi,c.carp_desc";
				//print $sql2;
				$rs3 = $this->link->query($sql2);
				$c = 0;
				if (!$rs3->EOF) {
					while (!$rs3->EOF)
					{
						$combi [$i] ["carpetas"] [$c] ["namecarp"] = $rs3->fields ['CARP_DESC'];
						$combi [$i] ["carpetas"] [$c] ["idcarpeta"] = $rs3->fields ['CODI_CARP'];
						$combi [$i] ["carpetas"] [$c] ["totalcarp"] = $rs3->fields ['CUENTA'];
						$c++;
						$rs3->MoveNext();
					}
					
				}
				else
				{
				}
				if($rol==1)
				{
					$sql3="select count(1) as CONTADOR from radicado
					where carp_per=0 and carp_codi=11
					and  radi_depe_actu=$dependencia
					and radi_usua_actu=$codigous $where3";
					//print $sql3;
					$rs4 = $this->link->query($sql3);
					
					if (!$rs4->EOF) {
						while (!$rs4->EOF)
						{
							$combi [$i] ["carpetas"] [$c] ["namecarp"] = "Vo.Bo.";
							$combi [$i] ["carpetas"] [$c] ["idcarpeta"] = 11;
							$combi [$i] ["carpetas"] [$c] ["totalcarp"] = $rs4->fields ['CONTADOR'];
							$c++;
							$rs4->MoveNext();
						}
					
					}
					else
					{
					}
					
				}

                $combi [$i] ["ucodi"] = $rs->fields ['UCODI'];
                $combi [$i] ["usuario"] = $rs->fields ['USUARIO'];
				$combi [$i] ["dependencia"] = $rs->fields ['DEPENDENCIA'];
                $i++;
                $rs->MoveNext();
            }
        } else {
            $combi['error'] = 'No se encontraron datos';
        }
		//print_r ($combi);
        return $combi;
    }
	
	/************************************************************
	**							   **
	**          DETALLES DE LA ESTADISTICA NUMERO 10           **
	**							   **
	************************************************************/
	
	function detallesEst10($idusuaban, $dependencia_ban, $tipocarpetaban, $carpetanumero, $tipoDocumento) {
		$where = ' ';
		if ($tipoDocumento != 9999)
		{
            $where =' and b.tdoc_codi='.$tipoDocumento;
		}
		$sql="select distinct b.RADI_NUME_RADI as RADICADOS ,TO_CHAR(b.RADI_FECH_RADI,'YYYY-MM-DD HH24:MI AM') as FECHA_RADICADO ,
		b.RA_ASUN as ASUNTO,d.SGD_DIR_NOMREMDES as REMITENTE, c.SGD_TPR_DESCRIP as TIPO_DOCUMENTO ,
		date_part('days', radi_fech_radi-CURRENT_TIMESTAMP)+floor(c.sgd_tpr_termino * 7/5)+(select count(*) 
		from sgd_noh_nohabiles where NOH_FECHA between radi_fech_radi and CURRENT_TIMESTAMP) as DIAS_RESTANTES ,
		b.RADI_NUME_DERI as HID_RADI_NUME_DERI ,b.RADI_TIPO_DERI as HID_RADI_TIPO_DERI 
		from radicado b left outer join SGD_TPR_TPDCUMENTO c on b.tdoc_codi=c.sgd_tpr_codigo 
		left outer join SGD_DIR_DRECCIONES d on b.radi_nume_radi=d.radi_nume_radi 
		where b.radi_nume_radi is not null and sgd_dir_cpcodi is null and (d.sgd_dir_tipo<2 or d.sgd_dir_tipo is null) 
		and b.radi_depe_actu=".$dependencia_ban." and b.radi_usua_actu='".$idusuaban."' and b.carp_codi=".$carpetanumero." and b.carp_per=".$tipocarpetaban." $where order by 3 DESC";

           
		//echo $sql;
        $rs = $this->link->query($sql);
        $i = 0;
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                $combi [$i] ["radicados"] = $rs->fields ['RADICADOS'];
                $combi [$i] ["fecha_radicado"] = $rs->fields ['FECHA_RADICADO'];
                $combi [$i] ["asunto"] = $rs->fields ['ASUNTO'];
				$combi [$i] ["remitente"] = $rs->fields ['REMITENTE'];
                $combi [$i] ["tipo_documento"] = $rs->fields ['TIPO_DOCUMENTO'];
                $combi [$i] ["dias_restantes"] = $rs->fields ['DIAS_RESTANTES'];
                $combi [$i] ["radi_nume_deri"] = $rs->fields ['RADI_NUME_DERI'];
                $combi [$i] ["hid_radi_tipo_deri"] = $rs->fields ['HID_RADI_TIPO_DERI'];
                $i++;
                $rs->MoveNext();
            }
			if($tipocarpetaban==0)
				{
					$sqlminic="select carp_desc BANDEJA from carpeta where carp_codi=$carpetanumero";
					$rsminic = $this->link->query($sqlminic);
					$combi [0] ["bandeja"] = $rsminic->fields ['BANDEJA'];
				}
				else
				{
					$sqlminip="select  DISTINCT ON  ( nomb_carp ) CODI_CARP,nomb_carp BANDEJA from carpeta_per where codi_carp=$carpetanumero";
					$rsminip = $this->link->query($sqlminip);
					$combi [0] ["bandeja"] = $rsminip->fields ['BANDEJA'];
				}
			
        } else {
            $combi['error'] = 'No se encontraron datos';
        }
        return $combi;
    }
    
     function est11($depe,$fechIni,$fechFin, $usua, $estado, $tipoRadicado) {
        $where = ' ';
        if ( $tipoRadicado!= null)
            $where .=" and substring(cast(r.radi_nume_radi as char(18)) from 14 for 1)='$tipoRadicado'";

        if ($usua != 0)
            $where .=' and b.usua_codi=' . $usua;

        if ($estado == 0)
            $where .=" and b.USUA_esta='1' ";
        if ($idusua != 0)
            $where .=' and b.usua_DOC=' . $idusua;
         if ($depe != '99999')
            $where .=' and h.DEPE_CODI=' . $depe;
          
          $sql="SELECT b.USUA_NOMB USUARIO
					, count(*) RADICADOS
					, SUM(r.RADI_NUME_folios) 	HOJAS_DIGITALIZADAS
					, MIN(b.USUA_DOC) HID_COD_USUARIO
					FROM RADICADO r, USUARIO b, HIST_EVENTOS h
				WHERE
					h.USUA_DOC=b.usua_DOC
					AND h.RADI_NUME_RADI=r.RADI_NUME_RADI
					AND h.SGD_TTR_CODIGO in (22,42)
					AND h.hist_fech BETWEEN  '$fechIni' and '$fechFin'
                                            $where
				GROUP BY b.USUA_NOMB order BY b.USUA_NOMB asc";

        $rs = $this->link->query($sql);
        $i = 0;
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                $combi [$i] ["ucodi"] = $rs->fields ['HID_COD_USUARIO'];
                $combi [$i] ["usuario"] = $rs->fields ['USUARIO'];
                $combi [$i] ["radicados"] = $rs->fields ['RADICADOS'];
                $combi [$i] ["folios"] = $rs->fields ['HOJAS_DIGITALIZADAS'];
                $i++;
                $rs->MoveNext();
            }
        } else {
            $combi['error'] = 'No se encontraron datos';
        }
        return $combi;
    }
    
     function detallesEst11($fechIni,$fechFin, $idusua, $tipoRadicado, $tipoDocumento,$tpenvio,$depe,$estado) {
        $where = ' ';
        $where = ' ';
        if ( $tipoRadicado!= null)
            $where .=" and substring(cast(r.radi_nume_radi as char(18)) from 14 for 1)='$tipoRadicado'";

        if ($usua != 0)
            $where .=' and b.usua_codi=' . $usua;

        if ($estado == 0)
            $where .=" and b.USUA_esta='1' ";
        if ($idusua != 0)
            $where .=' and b.usua_DOC=' . $idusua;
         if ($depe != '99999')
            $where .=' and h.DEPE_CODI=' . $depe;
 echo         $sql="SELECT b.USUA_NOMB USUARIO
					, count(*) RADICADOS
					, SUM(r.RADI_NUME_folios) 	HOJAS_DIGITALIZADAS
					, MIN(b.USUA_DOC) HID_COD_USUARIO
					FROM RADICADO r, USUARIO b, HIST_EVENTOS h
				WHERE
					h.USUA_DOC=b.usua_DOC
					AND h.RADI_NUME_RADI=r.RADI_NUME_RADI
					AND h.SGD_TTR_CODIGO in (22,42)
					AND h.hist_fech BETWEEN  '$fechIni' and '$fechFin'
                                            $where
				GROUP BY b.USUA_NOMB order BY b.USUA_NOMB asc";

           

        $rs = $this->link->query($sql);
        $i = 0;
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                $combi [$i] ["ucodi"] = $rs->fields ['UCODI'];
                $combi [$i] ["usuario"] = $rs->fields ['USUARIO'];
                $combi [$i] ["radicados"] = $rs->fields ['RADICADOS'];
                $combi [$i] ["fech"] = $rs->fields ['FECH'];
                $combi [$i] ["asunto"] = $rs->fields ['ASUN'];
                $combi [$i] ["uante"] = $rs->fields ['UANTE'];
                $combi [$i] ["tpdesc"] = $rs->fields ['TPDESC'];
                $combi [$i] ["mrec"] = $rs->fields ['MEDIO_RECEPCION'];
                $i++;
                $rs->MoveNext();
            }
        } else {
            $combi['error'] = 'No se encontraron datos';
        }
        return $combi;
    }
}
?>
