<?php date_default_timezone_set('America/Bogota');
session_start();
foreach ($_GET as $key => $valor)
    ${$key} = $valor;
foreach ($_POST as $key => $valor)
    ${$key} = $valor;
$krd = $_SESSION ["krd"];
$dependencia = $_SESSION ["dependencia"];
$usua_doc = $_SESSION ["usua_doc"];
$codusuario = $_SESSION ["codusuario"];
$ruta_raiz = "../../../..";
include_once $ruta_raiz . '/core/Modulos/estadistica/clases/estadistica-class.php';
if (!$fecha_busq)
    $fecha_busq = Date('Y-m-d');
if (!$fecha_busq2)
    $fecha_busq2 = Date('Y-m-d');
//print_r($_POST);
if ($subserie == 0 && ($accion == 'tdocxAsig' || $accion == 'tdocAsig')) {
    die('Debe selecionar subserie');
}
$accion = $action;
if ($accion == 'Combo') {
    include_once $ruta_raiz . '/core/Modulos/trd/clases/subSeries.php';
    $serieSub = new subSeries($ruta_raiz);
    $serieSub->setCodserie($_POST ['codSerie']);

    if ($depe == 0)
        $Listado = $serieSub->consultar();
    else
        $Listado = $serieSub->consultarDepe($depe);
    $numn = count($Listado);
    for ($i = 0; $i < $numn; $i++) {
        $codserie = $Listado [$i] ["CODIGO"];
        $dserie = $Listado [$i] ["DESCRIP"];
        $option .= "<option value='$codserie' > $codserie - $dserie</option>";
    }
    echo "<select  id='sbrd' name='sbrd' onChange='asignartipo()'  class='select'>
			<option value='0'>-- Seleccione Subserie --</option> $option
			</select>";
} else if ($accion == 'add') {
    //echo 'add';
    $mdoc = new matrizDoc($ruta_raiz);
    $Listado = $mdoc->add($_POST ['depe'], $_POST ['serieid'], $_POST ['subserie'], $_POST ['tpdoc'], $_POST ['med']);
    echo 'OK';
} else if ($accion == 'estado') {
    //	print_r($_POST);
    $codigomrd = $_POST ['codigo'];
    if ($_POST ['estado'] == 1) {
        $estado = 0;
    } else {
        $estado = 1;
        $checked = 'checked="yes"';
    }

    $mdoc = new matrizDoc($ruta_raiz);
    $Listado = $mdoc->modesta($codigomrd, $estado);
    echo "<input type='checkbox'  $checked  onclick='updateEsta(\"mrd1$codigomrd\",$codigomrd,\"estado\",$estado);'></input>";
} else if ($accion == 'estexp') {
    //	print_r($_POST);
    $codigomrd = $_POST ['codigo'];
    if ($_POST ['estado'] == 1) {
        $estado = 0;
    } else {
        $estado = 1;
        $checked = 'checked="yes"';
    }
    $mdoc = new matrizDoc($ruta_raiz);
    $Listado = $mdoc->modestexp($codigomrd, $estado);
    echo "<input type='checkbox'  $checked  onclick='updateEsta(\"mrd2$codigomrd\",$codigomrd,\"estexp\",$estado);'></input>";
} elseif ($accion == 'SerieComb') {
    include_once $ruta_raiz . '/core/Modulos/trd/clases/series.php';
    //print_r($_POST);
    $serie = new series($ruta_raiz);
    if ($depe == 0)
        $Listado = $serie->consultar();
    else
        $Listado = $serie->consultarDepe($depe);
    $numn = count($Listado);
    //include_once $ruta_raiz.'/core/language/'.$_SESSION ["lang"].'/data.inc';
    $optionSSd = "";
    for ($i = 0; $i < $numn; $i++) {
        $codserie = $Listado[$i]["CODIGO"];
        $dserie = $Listado[$i]["DESCRIP"];
        $optionSSd.="<option value='$codserie'>$codserie - $dserie</option>";
    }
    ?><select  id="srd" name='srd' onChange='consultarSub()'  class='select'>
        <option value='0'>-- Seleccione Serie --</option>
        <?php echo $optionSSd; ?>
    </select><?php } elseif ($accion == 'tdocxAsig') {

    if ($_POST ['subserie'] != 0) {
        $npg = 20;
        if (!$_POST['campo'])
            $pagina = 0;
        else
            $pagina = $_POST['campo'];

        $mdoc = new matrizDoc($ruta_raiz);
        if ($_POST['Busqueda'] == 1) {
            $Listado = $mdoc->buscar($_POST['critebusq'], $_POST['namebusq'], $_POST ['depe'], $_POST ['serieid'], $_POST ['subserie'], 0);
            $numn = count($Listado) - 1;
            if ($_POST['critebusq'] == 1) {
                $select = 'selected';
            }
            if ($_POST['critebusq'] == 2) {
                $select2 = 'selected';
            }
        } else {
            $Listado = $mdoc->consultarXAsignar($_POST ['depe'], $_POST ['serieid'], $_POST ['subserie'], $pagina);
            $numn = count($Listado);
        }
    }
    ?>
    <TABLE width='100%' cellspacing="0">
        <tr>
            <td colspan=3 class='titulos2'>Tipos Doc por asignados Buscar por <select
                    id='critebusq'>
                    <option value=1 <?php echo $select; ?>>codigo</option>
                    <option value=2 <?php echo $select2; ?>>descripción</option>
                </select> <input type="text" id='namebusq'
                                 value='<?php echo $_POST['namebusq']; ?>' /><input type="button"
                                 id='bt' value='Buscar' onclick='buscar()' />
            </td>
        </tr>
        <tr class=tpar>

            <td class=titulos3 align='center'>Codigo</td>
            <td class=titulos3 align='center'>Descripción</td>
            <td class=titulos3 align='center'></td>
        </tr>
        <?php         if ($_POST ['subserie'] != 0) {
            for ($i = 0; $i < $numn; $i++) {
                $codigo = $Listado [$i] ["CODIGO"];
                $dtpd = $Listado [$i] ["DESCRIP"];
                ?>
                <tr class=paginacion>
                    <td><?php echo  $codigo ?></td>
                    <td align=left><?php echo  $dtpd ?>
                    </td>
                    <td>
                        <div id='divtp<?php                         echo $codigo;
                        ?>'>
                            <a
                                onclick='asignar("divtp<?php                                 echo $codigo;
                                ?>", "<?php echo  $codigo ?>")'><img width="20px" height="15px"
                                   src='<?php 
                                   echo $ruta_raiz;
                                   ?>/imagenes/flecha.png' /> </a>
                        </div>
                    </td>
                </tr>
                <?php             }
            for ($i = $i; $i < $npg; $i++) {
                ?>
                <tr class=paginacion>
                    <td height="17px"></td>
                    <td align=left></td>
                    <td></td>
                </tr>
                <?php             }

            $paginador = $mdoc->paginadorConsultarXAsignar($_POST ['depe'], $_POST ['serieid'], $_POST ['subserie']);
            $numPag = ($paginador / $npg) + 1;
            ?>
            <tr>
                <td colspan=3 align="center" class='titulos2'>pagina<?php                     if ($_POST['Busqueda'] != 1) {
                        for ($ij = 1; $ij < $numPag; $ij++) {
                            //echo $pagina.'!='.(($ij-1)*15).' - ';
                            $vp = (($ij - 1) * $npg);
                            if ($pagina == $vp) {
                                echo $ij . " ";
                            } else {
                                echo "<a href='#2' onclick='pagina(" . $_POST ['depe'] . "," . $_POST ['serieid'] . "," . $_POST ['subserie'] . "," . $vp . ");'>" . $ij . "</a> ";
                            }
                        }
                    }
                }
                ?>
            </td>


        <tr>

    </table>
    <?php } elseif ($accion == 'consultar') {

//	print_r($_POST);
    $error = '';
    //consultar($_POST['subserie'],$_POST['depe'],$_POST['serieid'],$_POST['ano'] )
    $vaslidar = 0;
    if ($depe == 0 ) {
        $error.='<br> Debe Seleccionar una dependencia ';
        $vaslidar = 1;
    }
    if ($serieid == 0 ) {
        $error.='<br> Debe Seleccionar una Serie ';
        $vaslidar = 1;
    }
    if ($subserie == 0 ) {
        $error.='<br> Debe Seleccionar una Subserie ';
        $vaslidar = 1;
    }
    if ($vaslidar == 0 && !isset($creacion)) {
        $mdoc = new estadisticas($ruta_raiz);
	$result=$mdoc->consultar($subserie, $depe, $serieid, $ano);
	if(isset($excel) && substr($result,0,5)!='Error'){
	    header("Content-Type: application/vnd.ms-excel");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("content-disposition: attachment;filename=estExpedienteClasica.xls");
	}elseif(substr($result,0,5)!='Error'){
	    echo "<a href='#' onclick='excel();'><img src='../../../../imagenes/calc.png' style='width: 20px;height: 20px' align='right'></a>";
	}
        echo $result;
    } elseif (isset($creacion)){
	$mdoc = new estadisticas($ruta_raiz);
	$result=$mdoc->consultarCreacion($depe,$fecha_ini,$fecha_fin);
        if(isset($excel) && substr($result,0,5)!='Error'){
            header("Content-Type: application/vnd.ms-excel");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("content-disposition: attachment;filename=estExpedienteCreacion.xls");
	}elseif(substr($result,0,5)!='Error'){
	    echo "<a href='#' onclick='excel();'><img src='../../../../imagenes/calc.png' style='width: 20px;height: 20px' align='right'></a>";
	}
	echo $result;
    }else {
        echo $error;
    }
} else {
    
}?>
