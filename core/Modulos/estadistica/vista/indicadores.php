<?php session_start(); date_default_timezone_set('America/Bogota'); date_default_timezone_set('America/Bogota');
/**
  * Modificacion Variables Globales Infometika 2009
  */
$krd = $_SESSION["krd"];
$dependencia = $_SESSION["dependencia"];
$usua_doc = $_SESSION["usua_doc"];
$codusuario = $_SESSION["codusuario"];
$ruta_raiz = "../../../..";
include_once "$ruta_raiz/core/vista/validadte.php";
//include($ruta_raiz.'/validadte.php');
//if(!$_SESSION['dependencia']) include "$ruta_raiz/rec_session.php";
include_once "$ruta_raiz/core/Modulos/estadistica/clases/indicadores-class.php";
$ind= new indicadores($ruta_raiz);
$ind->setTpRadicado(1);
$mesante = date("d/m/Y", mktime(0, 0, 0, date("m") - 1, date("d"), date("Y")));
$url="$ruta_raiz/core/Modulos/estadistica/vista/operIndicadores.php";
?>
<html><head>
<link href="<?php echo $ruta_raiz?>/estilos/default/orfeo.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="<?php echo $ruta_raiz?>/estilos/jquery-ui.css">
<script src="<?php echo $ruta_raiz?>/js/jquery-1.10.2.js"></script>
<script src="<?php echo $ruta_raiz?>/js/jquery-ui.js"></script>
<script>
	$(function(){
            $.datepicker.setDefaults({dateFormat:"dd/mm/yy",
                monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
                monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
                dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
                dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
                dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
                changeYear: true,
                showOn: "button",
                buttonImage: "<?php echo $ruta_raiz?>/imagenes/calendar.gif",
                buttonImageOnly: true,
                buttonText: "Seleccione fecha",
                constrainInput: true,
                maxDate: "<?php echo date("d/m/Y")?>",
                beforeShow: function(){
                    $(".ui-datepicker").css('font-size', 12)
                }
            })
            $("#fecha_ini").datepicker({
                onSelect: function(selected){
                    $("#fecha_fin").datepicker('option','minDate',selected);
                }
            });
            $("#fecha_ini").datepicker("setDate","<?php echo $mesante?>");
            $("#fecha_fin").datepicker({
                minDate: "<?php echo $mesante?>",
                onSelect: function(selected){
                    $("#fecha_ini").datepicker('option','maxDate',selected);
                }
            });
            $("#fecha_fin").datepicker("setDate","<?php echo date("d/m/Y")?>");
        });

	function generarInforme(){
	    var trad=$("#trad").val();
	    //var indica=$("#selindica").val();
	    var fecha_ini=$("#fecha_ini").val();
	    var fecha_fin=$("#fecha_fin").val();
	    $("#response").html('<center><img  alt="Procesando" src="<?php echo $ruta_raiz?>/imagenes/loading.gif"></center>');
	    //var formData="action=genInfo&trad="+trad+"&indica="+indica+"&fecha_ini="+fecha_ini+"&fecha_fin="+fecha_fin;
	    var formData="action=genInfo&trad="+trad+"&fecha_ini="+fecha_ini+"&fecha_fin="+fecha_fin;
            $.ajax({
                url : "<?php echo $url?>",
                type: "POST",
                data: formData
            })
            .done(function(data){
                $("#response").html(data);
            })
            .fail(function(){
                alert("Fallo el ajax");
            });
	}

	function excel(){
            var trad=$("#trad").val();
            //var indica=$("#selindica").val();
            var fecha_ini=$("#fecha_ini").val();
            var fecha_fin=$("#fecha_fin").val();
            //var formData="action=genInfo&trad="+trad+"&indica="+indica+"&fecha_ini="+fecha_ini+"&fecha_fin="+fecha_fin;
            var formData="action=excel&trad="+trad+"&fecha_ini="+fecha_ini+"&fecha_fin="+fecha_fin+"&excel=1";
	    var url="<?php echo $url?>?"+formData;
	    window.open(url, 'Estadisiticas', '');
        }
</script>
    </head><body>
<div style='text-align: center ;width: 100%;margin: 0 auto;'>
<table class="borde_tab" width="100%" cellspacing="2"><tbody><tr><td align="center" class="titulos4">
  Indicadores del Sistema
</td></tr></tbody></table><br>
<center>
<table  style='width: 90%' class="borde_tab" >
        <tr class='titulo1' >
                        <td colspan="4">
                             Datos para indicadores &nbsp;
                        </td>
        </tr>
        <tr>
                        <td class='titulos2' width='35%'>
			Tipo de Radicaci&oacute;n
                        </td>
                        <td class='listado2' width='35%'>
			    <select id='trad' class='select' onchange="cambiarTrad(this.value);">
				<option value=1>Salida</option>
				<option value=2>Entrada</option>
				<option value=3>Interna</option>
			    </select>
			</td>
                        <td class='titulos2' width='15%'>Desde fecha (dd/mm/aaaa)
			</td>
                <td class='listado2' width='15%'><input id='fecha_ini' type='text' readonly/></td>
        </tr>
	<tr>
                <td class='titulos2' colspan=2>
                    
                </td>
                <!--td class='listado2' width='35%'>
                    <div id='indica'><</div>
                </td-->
                <td class='titulos2' width='15%'>Hasta fecha (dd/mm/aaaa)
                </td>
                <td class='listado2' width='15%'><input id='fecha_fin' type='text' readonly/></td>
        </tr>
	<tr>
	<td colspan="4" class='info' align="center">
            <input type="button" class="botones_mediano" value="Generar Informe" onclick="generarInforme();">
	    <input type="button" class="botones_mediano" value="Limpiar" onclick="window.location.reload();">
        </td>
	</tr>
</table>
<table style='width:90%;'>
<tr>
<td valign='top' style="width:100%;height:600px;";>
<div style=" padding : 1px; width : 100%; height : 600px; overflow : auto;" id='response'></div>
</td>
</tr>
</table>
