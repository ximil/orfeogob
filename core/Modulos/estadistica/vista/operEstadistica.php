<?php 
date_default_timezone_set('America/Bogota');
session_start();
$idusua = '';
foreach ($_POST as $key => $valor)
    ${ $key} = $valor;
foreach ($_GET as $key => $valor)
    ${$key} = $valor;
$krd = $_SESSION ["krd"];
$dependencia = $_SESSION ["dependencia"];
$usua_doc = $_SESSION ["usua_doc"];
$codusuario = $_SESSION ["codusuario"];
$usua_nomb = $_SESSION ["usua_nomb"];
$permEsta = $_SESSION['sgd_perm_estadistica'];
$ruta_raiz = "../../../..";

//print_r($_POST); 
if ($action == 'usuarios') {

    include_once $ruta_raiz . '/core/clases/usuarioOrfeo.php';
    $usuarios = new usuarioOrfeo($ruta_raiz);
    $usuarios->setDepe_codi($dependencia_busq);
    if ($permEsta >= 1) {
        $listado = $usuarios->consultarGrupo($usActivos);
        //print_r($listado);
        $numndep = count($listado);

        $optionUsuario = " <option value='0'> -- AGRUPAR POR TODOS LOS USUARIOS --</option>";
        for ($i = 0; $i < $numndep; $i++) {
            $nomDepe = $listado[$i]["USUA_NOMB"];
            $codDepe = $listado[$i]["USUA_CODI"];
            $optionUsuario.="<option value='$codDepe'> $nomDepe</option>";
        }
    } else {
        $nomDepe = $usua_nomb;
        $codDepe = $codusuario;
        $optionUsuario.="<option value='$codDepe'> $nomDepe</option>";
    }
    echo '<select name="codus" id="codus"  class="select"  >' . $optionUsuario . '</select>';
}

if ($action == 'consultar') {
    //echo 'consultar';
    if ($excel) {
        header("Content-Type: application/vnd.ms-excel");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("content-disposition: attachment;filename=estadisitica$tipoEstadistica.xls");
    }
    include_once $ruta_raiz . '/core/Modulos/estadistica/clases/estadistica-class.php';
    $estadisticas = new estadisticas($ruta_raiz);
    switch ($tipoEstadistica) {
        case 1:
	    if(!isset($Inac)){
		$Inac=0;
	    }
            $result = $estadisticas->estadistica1($dependencia_busq, $fecha_ini . " " . $hora_ini . ":00", $fecha_fin . " " . $hora_fin . ":59", $codus, $usActivos, $tipoRadicado, $tipoDocumento,$Inac);
            break;
        case 2:
	    if(!isset($Inac)){
                $Inac=0;
            }
            $result = $estadisticas->estadistica2($dependencia_busq, $fecha_ini . " " . $hora_ini . ":00", $fecha_fin . " " . $hora_fin . ":59", $codus, $usActivos, $tipoRadicado, $tipoDocumento,$Inac);
            break;
        case 3:
	    if(!isset($Inac)){
                $Inac=0;
            }
            $result = $estadisticas->estadistica3($dependencia_busq, $fecha_ini . " " . $hora_ini . ":00", $fecha_fin . " " . $hora_fin . ":59",$Inac);
            break;
        case 5:
            $result = $estadisticas->estadistica5($fecha_ini . " " . $hora_ini . ":00", $fecha_fin . " " . $hora_fin . ":59", $dependencia_busq, $usActivos);
            break;
        case 6:
            $result = $estadisticas->estadistica6($dependencia_busq, $codus, $usActivos, $tipoRadicado, $tipoDocumento);
            break;
        case 10:
	    if(!isset($Inac)){
                $Inac=0;
            }
            $result = $estadisticas->estadistica10($dependencia_busq, $codus, $usActivos, $tipoDocumento, $fecha_ini . " " . $hora_ini . ":00", $fecha_fin . " " . $hora_fin . ":59",$Inac);
            break;
        case 11:
            $result = $estadisticas->estadistica11($dependencia_busq, $fecha_ini . " " . $hora_ini . ":00", $fecha_fin . " " . $hora_fin . ":59", $codus, $usActivos, $tipoRadicado, $tipoDocumento);
            break;
        case 15:
	    if(!isset($Inac)){
                $Inac=0;
            }
            $result = $estadisticas->estadistica15($dependencia_busq, $fecha_ini . " " . $hora_ini . ":00", $fecha_fin . " " . $hora_fin . ":59", $tipoDocumento, $tipoRadicado,$Inac);
            break;
        case 17:
            $result = $estadisticas->estadistica17($dependencia_busq, $fecha_ini . " " . $hora_ini . ":00", $fecha_fin . " " . $hora_fin . ":59", $codus, $usActivos, $tipoRadicado, $tipoDocumento);
            break;
        case 18:
            $result = $estadisticas->estadistica18($dependencia_busq, $fecha_ini . " " . $hora_ini . ":00", $fecha_fin . " " . $hora_fin . ":59", $Serie, $tipoRadicado);
            break;
        case 19:
            $result = $estadisticas->estadistica19($dependencia_busq, $fecha_ini . " " . $hora_ini . ":00", $fecha_fin . " " . $hora_fin . ":59", $codus, $tipoRadicado, $tipoDocumento);
            break;
        case 20:            
            $result = $estadisticas->estadistica20($dependencia_busq, $fecha_ini . " " . $hora_ini . ":00", $fecha_fin . " " . $hora_fin . ":59", $tipoRadicado);
            break;
	case 21:
	    include_once $ruta_raiz.'/core/clases/alertas.php';
	    $alertas=new alertas($ruta_raiz);
	    $result=$alertas->reporteVencidos($fecha_ini . " " . $hora_ini,$fecha_fin. " " . $hora_fin ,$dependencia_busq);
	    break;
        default:
            break;
    }
    if ($result) {
        if (!$excel) {
            if($tipoEstadistica!=20)
            echo "<a href='#' onclick='total();'><img src='../../../../imagenes/total.png' style='width: 20px;height: 20px' align='right' alt='Todo'></a>";
            echo "<a href='#' onclick='excel();'><img src='../../../../imagenes/calc.png' style='width: 20px;height: 20px' align='right'></a>";
        }
        echo $result;
    }
}

if ($action == 'detalles') {
    //echo 'consultar';
    include_once $ruta_raiz . '/core/Modulos/estadistica/clases/estadistica-class.php';
    $estadisticas = new estadisticas($ruta_raiz);
    if ($excel) {
        header("Content-Type: application/vnd.ms-excel");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("content-disposition: attachment;filename=detalles$tipoEstadistica.xls");
    }
    $vdata = '';
    switch ($tipoEstadistica) {
        case 1:
	    if(!isset($Inac)){
                $Inac=0;
            }
            $result = $estadisticas->detallesEst1($fecha_ini . " " . $hora_ini . ":00", $fecha_fin . " " . $hora_fin . ":59", $idusua, $tipoRadicado, $tipoDocumento, $radicador,$dependencia_busq,$Inac);
            break;
        case 2:
	    if(!isset($Inac)){
                $Inac=0;
            }
            $result = $estadisticas->detallesEst2($fecha_ini . " " . $hora_ini . ":00", $fecha_fin . " " . $hora_fin . ":59", $idusua, $tipoRadicado, $tipoDocumento, $tpmedio, $dependencia_busq, $usActivos,$Inac);
            break;
        case 3:
	    if(!isset($Inac)){
                $Inac=0;
            }
            $result = $estadisticas->detallesEst3($dependencia_busq, $fecha_ini . " " . $hora_ini . ":00", $fecha_fin . " " . $hora_fin . ":59", $nombremed, $idmedio,$Inac);
            break;
        case 5:
            $result = $estadisticas->detallesEst5($fecha_ini . " " . $hora_ini . ":00", $fecha_fin . " " . $hora_fin . ":59", $idusua);
            break;
        case 6:
            $result = $estadisticas->detallesEst6($idusua, $tipoRadicado, $tipoDocumento);
            break;
        case 10:
            $result = $estadisticas->detallesEst10($idusuaban, $dependencia_ban, $tipocarpetaban, $userban, $carpetanumero, $tipoDocumento, $fecha_ini . " " . $hora_ini . ":00", $fecha_fin . " " . $hora_fin . ":59", $nomcarp);
            break;
        case 11:
            $result = $estadisticas->detallesEst11($fecha_ini . " " . $hora_ini . ":00", $fecha_fin . " " . $hora_fin . ":59", $idusua, $tipoRadicado, $depeA);
            $vdata = $depeA;
            break;
        case 15:
            $result = $estadisticas->detallesEst15($dependencia_in, $tipoDocumento, $fecha_ini . " " . $hora_ini . ":00", $fecha_fin . " " . $hora_fin . ":59", $tipoRadicado);
            break;
        case 17:
            $result = $estadisticas->detallesEst17($dependencia_busq, $fecha_ini . " " . $hora_ini . ":00", $fecha_fin . " " . $hora_fin . ":59", $codus, $tipoRadicado, $tipoDocumento);
            break;
        case 18:
            $subserien = $radicador;
            $subseriec = $idusua;
            //       echo "$dependencia_busq,$fecha_ini  $hora_ini :00 $fecha_fin  $hora_fin :59 - $Serie - $subserien - $subseriec, $tipoRadicado";
            $result = $estadisticas->detallesEst18($dependencia_busq, $fecha_ini . " " . $hora_ini . ":00", $fecha_fin . " " . $hora_fin . ":59", $Serie, $subserien, $subseriec, $tipoRadicado);
            break;
        case 19:
            $result = $estadisticas->detallesEst19($depeusua, $fecha_ini . " " . $hora_ini . ":00", $fecha_fin . " " . $hora_fin . ":59", $codus, $tipoRadicado, $tipoDocumento);
            break;
         case 20:
          //   print_r($_POST);
            $result = $estadisticas->detallesEst20($dependencia_busq, $fecha_ini . " " . $hora_ini . ":00", $fecha_fin . " " . $hora_fin . ":59", $idusua);
            break;
	 case 21:
	    include_once $ruta_raiz.'/core/clases/alertas.php';
            $alertas=new alertas($ruta_raiz);
	    $alertas->setDepeNume($idusua);
	    $result=$alertas->reporteDetalles($fecha_ini." ".$hora_ini ,$fecha_fin." ".$hora_fin,$radicador);
	    break;
        default:
            break;
    }

    if ($result) {
        if (!$excel) {
            if ($tipoEstadistica == 10) {
                echo "<a href='#' onclick='excelbandejas($idusuaban,$carpetanumero,$dependencia_ban,\"$userban\",\"$nomcarp\",$tipocarpetaban);'><img src='../../../../imagenes/calc.png' style='width: 20px;height: 20px'></a>";
            } elseif ($tipoEstadistica == 3) {
                echo "<a href='#' onclick='exceldest3(\"$nombremed\", $idmedio);'><img src='../../../../imagenes/calc.png' style='width: 20px;height: 20px'></a>";
            } elseif ($tipoEstadistica == 15) {
                echo "<a href='#' onclick='exceldest15($dependencia_in);'><img src='../../../../imagenes/calc.png' style='width: 20px;height: 20px'></a>";
            } elseif ($tipoEstadistica == 17) {
                echo "<a href='#' onclick='exceldest17($codus);'><img src='../../../../imagenes/calc.png' style='width: 20px;height: 20px'></a>";
            } elseif ($tipoEstadistica == 18) {
                //echo "<a href='#' onclick='exceldest18($idusua,\"$radicador\");'><img src='../../../../imagenes/calc.png' style='width: 20px;height: 20px'></a>";
                echo "<a href='#' onclick='exceld($idusua,\"$radicador\");'><img src='../../../../imagenes/calc.png' style='width: 20px;height: 20px'></a>";
            } elseif ($tipoEstadistica == 19) {
                echo "<a href='#' onclick='exceldest19($codus,$depeusua);'><img src='../../../../imagenes/calc.png' style='width: 20px;height: 20px'></a>";
            } else {
                echo "<a href='#' onclick='exceld($idusua,\"$vdata\");'><img src='../../../../imagenes/calc.png' style='width: 20px;height: 20px'></a>";
            }
        }
        echo $result;
    }
}
if ($action == 'total') {
//    echo 'total';
    //print_r($_POST);
    if ($excel) {
        header("Content-Type: application/vnd.ms-excel");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("content-disposition: attachment;filename=estadisitica$tipoEstadistica.xls");
    }
    include_once $ruta_raiz . '/core/Modulos/estadistica/clases/estadistica-class.php';
    $estadisticas = new estadisticas($ruta_raiz);
    switch ($tipoEstadistica) {
        case 1:
            $result = $estadisticas->Total1($dependencia_busq, $fecha_ini . " " . $hora_ini . ":00", $fecha_fin . " " . $hora_fin . ":59", $codus, $usActivos, $tipoRadicado, $tipoDocumento);
            break;

        default:
            break;
    }
    if ($result) {
        if (!$excel) {
            //echo "<a href='#' onclick='total1();'><img src='../../../../imagenes/total.png' style='width: 20px;height: 20px' align='right' alt='Todo'></a>";
            echo "<a href='#' onclick='total1();'><img src='../../../../imagenes/calc.png' style='width: 20px;height: 20px' align='right'></a>";
        }
        echo $result;
    }
}
?>
