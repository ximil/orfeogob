<?php date_default_timezone_set('America/Bogota');
session_start();
foreach ($_GET as $key => $valor)
    ${$key} = $valor;
foreach ($_POST as $key => $valor)
    ${$key} = $valor;
$rol = $_SESSION ["id_rol"];
$krd = $_SESSION["krd"];
$dependencia = $_SESSION["dependencia"];
$usua_doc = $_SESSION["usua_doc"];
$codusuario = $_SESSION["codusuario"];
$permEsta = $_SESSION['sgd_perm_estadistica'];
$ruta_raiz = '../../../..';
include_once $ruta_raiz . '/core/Modulos/trd/clases/tpDocumento.php';
include_once $ruta_raiz . '/core/clases/dependencia.php';
include_once $ruta_raiz . '/core/Modulos/trd/clases/series.php';
$script = "$ruta_raiz/core/Modulos/estadistica/vista/operEstadistica.php";
//inicializa  dependecias
$depe = new dependencia($ruta_raiz);
//echo 1;
$depe->setDepe_codi($dependencia);
$optionDepe = "";
if ($permEsta == 2) {
    $dependecias = $depe->consultarTodo();
    $numndep = count($dependecias);
//include_once $ruta_raiz.'/core/language/'.$_SESSION ["lang"].'/data.inc';
    $optionDepe = "<option value=99999  >-- Todas las Dependencias --</option>";
    for ($i = 0; $i < $numndep; $i++) {
        $nomDepe = $dependecias[$i]["depe_nomb"];
        $codDepe = $dependecias[$i]["depe_codi"];
        $optionDepe.="<option value='$codDepe'>$codDepe - $nomDepe</option>";
    }
} elseif ($rol == 1) {
    $dependecias = $depe->consultar();
    $nomDepe = $dependecias["depe_nomb"];
    $codDepe = $dependecias["depe_codi"];
    $optionDepe.="<option value='$codDepe'>$codDepe - $nomDepe</option>";
    $dependecias = $depe->consuDepHijas();
    $numndep = count($dependecias);
//include_once $ruta_raiz.'/core/language/'.$_SESSION ["lang"].'/data.inc';
    ///$optionDepe = "<option value=99999  >-- Todas las Dependencias --</option>";
    for ($i = 0; $i < $numndep; $i++) {
        $nomDepe = $dependecias[$i]["depe_nomb"];
        $codDepe = $dependecias[$i]["depe_codi"];
        $optionDepe.="<option value='$codDepe'>$codDepe - $nomDepe</option>";
    }
} else {
    $dependecias = $depe->consultar();
    $optionDepe = "";
    $nomDepe = $dependecias["depe_nomb"];
    $codDepe = $dependecias["depe_codi"];
    $optionDepe.="<option value='$codDepe'>$codDepe - $nomDepe</option>";
}
//inicializa  tpdoc
$tp = new tpDocumento($ruta_raiz);
$Listado = $tp->consultar();
//$numn=count($Listado);
$numn = count($Listado);

$optionTp = "";
for ($i = 0; $i < $numn; $i++) {
    $cod = $Listado[$i]["CODIGO"];
    $dserie = $Listado[$i]["DESCRIP"];
    $optionTp.="<option value='$cod'> $dserie</option>";
}
$serie = new series($ruta_raiz);
$ListadoS = $serie->consultar();
$numn = count($ListadoS);
//include_once $ruta_raiz.'/core/language/'.$_SESSION ["lang"].'/data.inc';
$optionSr = '';
for ($i = 0; $i < $numn; $i++) {
    $codserie = $ListadoS[$i]["CODIGO"];
    $dserie = $ListadoS[$i]["DESCRIP"];
    $optionSr.="<option value='$codserie'>$codserie - $dserie</option>";
}
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <title>.:: Estadisitcas Orfeo::.</title>
        <link rel="stylesheet" href="<?php echo $ruta_raiz; ?>/estilos/default/orfeo.css">
        <link rel="stylesheet" type="text/css" 	href="<?php echo $ruta_raiz ?>/js/calendario/calendar.css">
        <style>
            .titulosEst {
                font-family: Verdana, Arial, Helvetica, sans-serif;
                font-size: 14px;
                font-style: normal;
                font-weight: bolder;
                color: #000;
                background-color: window;
                text-indent: 5pt;
                vertical-align: bottom;
                height: 20px;
                text-shadow: 0.1em 0.1em 0.2em rgb(165, 161, 255);
            }
            .cosa
            {
                margin-top: 0;
                margin: 0;
                padding: 0;
                color: #ffffff;
                font-size: 14px;
                text-align: center;
                background-color: #006699;
                text-transform: inherit;
                text-shadow: 0.1em 0.1em 0.2em black;
            }
            table#est10
            {
                border: 1px solid black;
                width:330px;
            }
            table#est10 tr td
            {
                border: 1px solid #f0f0f0;
            }
            .est10tit
            {
                vertical-align:top;
            }
            table#est10 tr td table tr td
            {
                border: 0px;
            }
            .tdest10
            {
                font-style: normal;
                text-align:center;
                font-size:10px;
                font-weight:bold;
                width:165px;
                padding-top:0px;
            }
            .tdest10u
            {
                vertical-align:bottom;
            }
        </style>
        <script language="JavaScript" type="text/javascript"	src="<?php echo $ruta_raiz ?>/js/calendario/calendar_eu.js"></script>
        <script>
            function adicionarOp(forma, combo, desc, val, posicion) {
                o = new Array;
                o[0] = new Option(desc, val);
                eval(forma.elements[combo].options[posicion] = o[0]);
                //alert ("Adiciona " +val+"-"+desc );

            }
        </script>
        <script language="JavaScript" src="<?php echo  $ruta_raiz ?>/js/common.js"></script>
        <script language="javascript">
            //    <!--
            /*                  var dateAvailable = new ctlSpiffyCalendarBox("dateAvailable", "formulario", "fecha_ini", "btnDate1", "2013/01/18", scBTNMODE_CUSTOMBLUE);
             var dateAvailable2 = new ctlSpiffyCalendarBox("dateAvailable2", "formulario", "fecha_fin", "btnDate2", "2013/02/18", scBTNMODE_CUSTOMBLUE);
             */
            //--></script>
        <script type="text/javascript">
            function usuarios() {
                var dependencia_busq = document.getElementById('dependencia_busq').value;
                var usActivos = document.getElementById('usActivos').value;
                var poststr = "action=usuarios&usActivos=" + usActivos + "&dependencia_busq=" + dependencia_busq;
                url = "<?php echo $script; ?>";
                partes(url, 'divUsu', poststr, '');
                //	document.getElementById('xasi').innerHTML = 'Lista de tipos documentales por asignar';
                //	document.getElementById('asi').innerHTML = 'Lista de tipos documentales asignados';
            }
            function consultar() {
                var tipoEstadistica = document.getElementById('tipoEstadistica').value;
                if (tipoEstadistica == 1) {
                    var r = confirm('Esta estadistica no refleja las copias.');
                    if (r == true) {
                    }
                    else
                        return false;
                }
                document.getElementById('resp').innerHTML = '<center><img  alt="Procesando" src="<?php echo $ruta_raiz; ?>/imagenes/loading.gif"></center>';
                document.getElementById('resp2').innerHTML = '';
                var dependencia_busq = document.getElementById('dependencia_busq').value;
                var usActivos = document.getElementById('usActivos').value;
                var codus = document.getElementById('codus').value;
                var tipoRadicado = document.getElementById('tipoRadicado').value;
                var tipoDocumento = document.getElementById('tipoDocumento').value;
                var fecha_ini = document.getElementById('fecha_ini').value;
                var fecha_fin = document.getElementById('fecha_fin').value;
                var hora_ini = document.getElementById('hora_ini').value;
                var hora_fin = document.getElementById('hora_fin').value;
                var Serie = document.getElementById('Serie').value;
                var poststr = "action=consultar&hora_ini=" + hora_ini + "&hora_fin=" + hora_fin + "&usActivos=" + usActivos + "&dependencia_busq=" + dependencia_busq + "&tipoEstadistica=" + tipoEstadistica + "&codus=" + codus + "&tipoRadicado=" + tipoRadicado + "&tipoDocumento=" + tipoDocumento + "&fecha_ini=" + fecha_ini + "&fecha_fin=" + fecha_fin + "&Serie=" + Serie;
                url = "<?php echo $script; ?>";
                partes(url, 'resp', poststr, '');
                //	document.getElementById('xasi').innerHTML = 'Lista de tipos documentales por asignar';
                //	document.getElementById('asi').innerHTML = 'Lista de tipos documentales asignados';
            }
            function total() {
                document.getElementById('resp2').innerHTML = '<center><img  alt="Procesando" src="<?php echo $ruta_raiz; ?>/imagenes/loading.gif"></center>';
                //document.getElementById('resp2').innerHTML = '';
                var dependencia_busq = document.getElementById('dependencia_busq').value;
                var usActivos = document.getElementById('usActivos').value;
                var tipoEstadistica = document.getElementById('tipoEstadistica').value;
                var codus = document.getElementById('codus').value;
                var tipoRadicado = document.getElementById('tipoRadicado').value;
                var tipoDocumento = document.getElementById('tipoDocumento').value;
                var fecha_ini = document.getElementById('fecha_ini').value;
                var fecha_fin = document.getElementById('fecha_fin').value;
                var hora_ini = document.getElementById('hora_ini').value;
                var hora_fin = document.getElementById('hora_fin').value;
                var Serie = document.getElementById('Serie').value;
                var poststr = "action=total&hora_ini=" + hora_ini + "&hora_fin=" + hora_fin + "&usActivos=" + usActivos + "&dependencia_busq=" + dependencia_busq + "&tipoEstadistica=" + tipoEstadistica + "&codus=" + codus + "&tipoRadicado=" + tipoRadicado + "&tipoDocumento=" + tipoDocumento + "&fecha_ini=" + fecha_ini + "&fecha_fin=" + fecha_fin + "&Serie=" + Serie;
                url = "<?php echo $script; ?>";
                partes(url, 'resp2', poststr, '');
                //	document.getElementById('xasi').innerHTML = 'Lista de tipos documentales por asignar';
                //	document.getElementById('asi').innerHTML = 'Lista de tipos documentales asignados';
            }
            function total1() {
                //document.getElementById('resp2').innerHTML = '<center><img  alt="Procesando" src="<?php echo $ruta_raiz; ?>/imagenes/loading.gif"></center>';
                //document.getElementById('resp2').innerHTML = '';
                var dependencia_busq = document.getElementById('dependencia_busq').value;
                var usActivos = document.getElementById('usActivos').value;
                var tipoEstadistica = document.getElementById('tipoEstadistica').value;
                var codus = document.getElementById('codus').value;
                var tipoRadicado = document.getElementById('tipoRadicado').value;
                var tipoDocumento = document.getElementById('tipoDocumento').value;
                var fecha_ini = document.getElementById('fecha_ini').value;
                var fecha_fin = document.getElementById('fecha_fin').value;
                var hora_ini = document.getElementById('hora_ini').value;
                var hora_fin = document.getElementById('hora_fin').value;
                var Serie = document.getElementById('Serie').value;
                var poststr = "action=total&excel=1&hora_ini=" + hora_ini + "&hora_fin=" + hora_fin + "&usActivos=" + usActivos + "&dependencia_busq=" + dependencia_busq + "&tipoEstadistica=" + tipoEstadistica + "&codus=" + codus + "&tipoRadicado=" + tipoRadicado + "&tipoDocumento=" + tipoDocumento + "&fecha_ini=" + fecha_ini + "&fecha_fin=" + fecha_fin + "&Serie=" + Serie;
                // url = "<?php echo $script; ?>";
                // partes(url, 'resp2', poststr, '');
                url = "<?php echo $script; ?>?" + poststr;
                var params = '';
                window.open(url, 'Estadisiticas', params);

                //      document.getElementById('xasi').innerHTML = 'Lista de tipos documentales por asignar';
                //      document.getElementById('asi').innerHTML = 'Lista de tipos documentales asignados';
            }

            function excel() {
                //  document.getElementById('resp').innerHTML = '<center><img  alt="Procesando" src="<?php echo $ruta_raiz; ?>/imagenes/loading.gif"></center>';
                //    document.getElementById('resp2').innerHTML = '';
                var dependencia_busq = document.getElementById('dependencia_busq').value;
                var usActivos = document.getElementById('usActivos').value;
                var tipoEstadistica = document.getElementById('tipoEstadistica').value;
                var codus = document.getElementById('codus').value;
                var tipoRadicado = document.getElementById('tipoRadicado').value;
                var tipoDocumento = document.getElementById('tipoDocumento').value;
                var fecha_ini = document.getElementById('fecha_ini').value;
                var fecha_fin = document.getElementById('fecha_fin').value;
                var hora_ini = document.getElementById('hora_ini').value;
                var hora_fin = document.getElementById('hora_fin').value;
                var Serie = document.getElementById('Serie').value;
                var poststr = "action=consultar&excel=1&&hora_ini=" + hora_ini + "&hora_fin=" + hora_fin + "&usActivos=" + usActivos + "&dependencia_busq=" + dependencia_busq + "&tipoEstadistica=" + tipoEstadistica + "&codus=" + codus + "&tipoRadicado=" + tipoRadicado + "&tipoDocumento=" + tipoDocumento + "&fecha_ini=" + fecha_ini + "&fecha_fin=" + fecha_fin + "&Serie=" + Serie;
                url = "<?php echo $script; ?>?" + poststr;
                var params = '';
                window.open(url, 'Estadisiticas', params);
                //partes(url, 'resp', poststr, '');
                //	document.getElementById('xasi').innerHTML = 'Lista de tipos documentales por asignar';
                //	document.getElementById('asi').innerHTML = 'Lista de tipos documentales asignados';
            }
            function exceld(idusuario, radicador) {
                var tp = document.getElementById('tipoEstadistica').value;
                if (tp == '2') {
                    var tpmedio = idusuario;
                    idusuario = document.getElementById('codus').value;
                }
                if (tp == '11') {
                    var depeA = radicador;
                }
                if (tp == '18') {
                    var dato1 = radicador;
                    alert(dato1);
                }
                //   document.getElementById('resp2').innerHTML = '<center><img  alt="Procesando" src="<?php echo $ruta_raiz; ?>/imagenes/loading.gif"></center>';
                var dependencia_busq = document.getElementById('dependencia_busq').value;
                var usActivos = document.getElementById('usActivos').value;
                var tipoEstadistica = document.getElementById('tipoEstadistica').value;
                var codus = document.getElementById('codus').value;
                var tipoRadicado = document.getElementById('tipoRadicado').value;
                var tipoDocumento = document.getElementById('tipoDocumento').value;
                var fecha_ini = document.getElementById('fecha_ini').value;
                var fecha_fin = document.getElementById('fecha_fin').value;
                var hora_ini = document.getElementById('hora_ini').value;
                var hora_fin = document.getElementById('hora_fin').value;
                var Serie = document.getElementById('Serie').value;
                var poststr = "action=detalles&excel=1&tpmedio=" + tpmedio + "&idusua=" + idusuario + "&hora_ini=" + hora_ini + "&hora_fin=" + hora_fin + "&usActivos=" + usActivos + "&dependencia_busq=" + dependencia_busq + "&tipoEstadistica=" + tipoEstadistica + "&codus=" + codus + "&tipoRadicado=" + tipoRadicado + "&tipoDocumento=" + tipoDocumento + "&fecha_ini=" + fecha_ini + "&fecha_fin=" + fecha_fin + "&depeA=" + depeA + "&radicador=" + dato1 + "&Serie=" + Serie;
                url = "<?php echo $script; ?>?" + poststr;
                //alert(url);
                var params = '';
                window.open(url, 'Estadisiticas', params);

            }
            function exceldest3(nombremed, idmedio) {
                //alert("Nombre del medio: " + nombremed + " Id del Medio: " + idmedio);
                var tp = document.getElementById('tipoEstadistica').value;
                var dependencia_busq = document.getElementById('dependencia_busq').value;
                var tipoEstadistica = document.getElementById('tipoEstadistica').value;
                var fecha_ini = document.getElementById('fecha_ini').value;
                var fecha_fin = document.getElementById('fecha_fin').value;
                var hora_ini = document.getElementById('hora_ini').value;
                var hora_fin = document.getElementById('hora_fin').value;
                var poststr = "action=detalles&excel=1&nombremed=" + nombremed + "&idmedio=" + idmedio + "&hora_ini=" + hora_ini + "&hora_fin=" + hora_fin + "&dependencia_busq=" + dependencia_busq + "&tipoEstadistica=" + tipoEstadistica + "&fecha_ini=" + fecha_ini + "&fecha_fin=" + fecha_fin;
                url = "<?php echo $script; ?>?" + poststr;
                var params = '';
                window.open(url, 'Estadisiticas', params);

            }
            function exceldest15(dependencia) {
                //alert("Codigo de la Dependencia: " + dependencia);
                var tipoEstadistica = document.getElementById('tipoEstadistica').value;
                var fecha_ini = document.getElementById('fecha_ini').value;
                var fecha_fin = document.getElementById('fecha_fin').value;
                var hora_ini = document.getElementById('hora_ini').value;
                var hora_fin = document.getElementById('hora_fin').value;
                var tipoRadicado = document.getElementById('tipoRadicado').value;
                var tipoDocumento = document.getElementById('tipoDocumento').value;
                var poststr = "action=detalles&excel=1&dependencia_in=" + dependencia + "&tipoEstadistica=" + tipoEstadistica + "&tipoDocumento=" + tipoDocumento + "&fecha_ini=" + fecha_ini + "&fecha_fin=" + fecha_fin + "&hora_ini=" + hora_ini + "&hora_fin=" + hora_fin + "&tipoRadicado=" + tipoRadicado;
                url = "<?php echo $script; ?>?" + poststr;
                var params = '';
                window.open(url, 'Estadisiticas', params);
            }

            function exceldest17(codserie, titulo) {
                //alert("Codigo del usuario: " + usuacodi);
                var tipoEstadistica = document.getElementById('tipoEstadistica').value;
                var fecha_ini = document.getElementById('fecha_ini').value;
                var fecha_fin = document.getElementById('fecha_fin').value;
                var hora_ini = document.getElementById('hora_ini').value;
                var hora_fin = document.getElementById('hora_fin').value;
                var tipoDocumento = document.getElementById('tipoDocumento').value;
                var tipoRadicado = document.getElementById('tipoRadicado').value;
                var dependencia_busq = document.getElementById('dependencia_busq').value;
                var Serie = document.getElementById('Serie').value;
                var poststr = "action=detalles&excel=1&dependencia_busq=" + dependencia_busq + "&tipoEstadistica=" + tipoEstadistica + "&fecha_ini=" + fecha_ini + "&fecha_fin=" + fecha_fin + "&hora_ini=" + hora_ini + "&hora_fin=" + hora_fin + "&tipoDocumento=" + tipoDocumento + "&tipoRadicado=" + tipoRadicado + "&codus=" + usuacodi + "%Serie=" + Serie;
                url = "<?php echo $script; ?>?" + poststr;
                var params = '';
                window.open(url, 'Estadisiticas', params);
            }

            function exceldest18(idusuario, radicador) {
                //alert("Codigo del Usuario: " + usuacodi);
                var dependencia_busq = document.getElementById('dependencia_busq').value;
                var tipoEstadistica = document.getElementById('tipoEstadistica').value;
                var tipoRadicado = document.getElementById('tipoRadicado').value;
                var fecha_ini = document.getElementById('fecha_ini').value;
                var fecha_fin = document.getElementById('fecha_fin').value;
                var hora_ini = document.getElementById('hora_ini').value;
                var hora_fin = document.getElementById('hora_fin').value;
                var Serie = document.getElementById('Serie').value;
                var poststr = "action=detalles&dependencia_busq=" + dependencia_busq + "&Serie=" + Serie + "&excel=1&radicador=" + radicador + "&idusua=" + idusuario + "&tipoEstadistica=" + tipoEstadistica + "&fecha_ini=" + fecha_ini + "&fecha_fin=" + fecha_fin + "&hora_ini=" + hora_ini + "&hora_fin=" + hora_fin + "&tipoRadicado=" + tipoRadicado;
                var params = '';
                url = "<?php echo $script; ?>?" + poststr;
                ///partes(url, 'Estadisiticas', poststr, '');
                window.open(url, 'Estadisiticas', params);
            }
            function exceldest19(usuacodi, depeusua) {
                //alert("Codigo del usuario: " + usuacodi);
                var tipoEstadistica = document.getElementById('tipoEstadistica').value;
                var fecha_ini = document.getElementById('fecha_ini').value;
                var fecha_fin = document.getElementById('fecha_fin').value;
                var hora_ini = document.getElementById('hora_ini').value;
                var hora_fin = document.getElementById('hora_fin').value;
                var tipoDocumento = document.getElementById('tipoDocumento').value;
                var tipoRadicado = document.getElementById('tipoRadicado').value;
                var poststr = "action=detalles&excel=1&depeusua=" + depeusua + "&tipoEstadistica=" + tipoEstadistica + "&fecha_ini=" + fecha_ini + "&fecha_fin=" + fecha_fin + "&hora_ini=" + hora_ini + "&hora_fin=" + hora_fin + "&tipoDocumento=" + tipoDocumento + "&tipoRadicado=" + tipoRadicado + "&codus=" + usuacodi;
                url = "<?php echo $script; ?>?" + poststr;
                var params = '';
                window.open(url, 'Estadisiticas', params);
            }

            function excelbandejas(idusuario, idcarpeta, dependencia, userban, nomcarp, tipocarpetaban) {
                //alert("Id del usuario: " + idusuario + " id carpeta: " + idcarpeta + " Dependencia:" + dependencia + " Tipo de Carpeta: " + tipocarpetaban + " Usuario: " + userban);
                //document.getElementById('resp2').innerHTML = '<center><img  alt="Procesando" src="<?php echo $ruta_raiz; ?>/imagenes/loading.gif"></center>';
                var tipoEstadistica = document.getElementById('tipoEstadistica').value;
                var tipoDocumento = document.getElementById('tipoDocumento').value;
                var fecha_ini = document.getElementById('fecha_ini').value;
                var fecha_fin = document.getElementById('fecha_fin').value;
                var hora_ini = document.getElementById('hora_ini').value;
                var hora_fin = document.getElementById('hora_fin').value;
                var poststr = "action=detalles&excel=1&nomcarp=" + nomcarp + "&idusuaban=" + idusuario + "&dependencia_ban=" + dependencia + "&carpetanumero=" + idcarpeta + "&tipoEstadistica=" + tipoEstadistica + "&tipocarpetaban=" + tipocarpetaban + "&userban=" + userban + "&tipoDocumento=" + tipoDocumento + "&fecha_ini=" + fecha_ini + "&fecha_fin=" + fecha_fin + "&hora_ini=" + hora_ini + "&hora_fin=" + hora_fin;
                url = "<?php echo $script; ?>?" + poststr;
                var params = '';
                window.open(url, 'Estadisiticas', params);
                //partes(url, 'resp2', poststr, '');
            }

            function detalles(idusuario, radicador) {
                var tp = document.getElementById('tipoEstadistica').value;
                if (tp == '2') {
                    var tpmedio = idusuario;
                    idusuario = document.getElementById('codus').value;
                }
                if (tp == '11') {
                    var depeA = radicador;
                }
                document.getElementById('resp2').innerHTML = '<center><img  alt="Procesando" src="<?php echo $ruta_raiz; ?>/imagenes/loading.gif"></center>';
                var dependencia_busq = document.getElementById('dependencia_busq').value;
                var usActivos = document.getElementById('usActivos').value;
                var tipoEstadistica = document.getElementById('tipoEstadistica').value;
                var codus = document.getElementById('codus').value;
                var tipoRadicado = document.getElementById('tipoRadicado').value;
                var tipoDocumento = document.getElementById('tipoDocumento').value;
                var fecha_ini = document.getElementById('fecha_ini').value;
                var fecha_fin = document.getElementById('fecha_fin').value;
                var hora_ini = document.getElementById('hora_ini').value;
                var hora_fin = document.getElementById('hora_fin').value;
                var Serie = document.getElementById('Serie').value;
                var poststr = "action=detalles&radicador=" + radicador + "&tpmedio=" + tpmedio + "&idusua=" + idusuario + "&hora_ini=" + hora_ini + "&hora_fin=" + hora_fin + "&usActivos=" + usActivos + "&dependencia_busq=" + dependencia_busq + "&tipoEstadistica=" + tipoEstadistica + "&codus=" + codus + "&tipoRadicado=" + tipoRadicado + "&tipoDocumento=" + tipoDocumento + "&fecha_ini=" + fecha_ini + "&fecha_fin=" + fecha_fin + "&Serie=" + Serie + "&depeA=" + depeA;
                url = "<?php echo $script; ?>";
                partes(url, 'resp2', poststr, '');

            }

            function detallesbandejas(idusuario, idcarpeta, dependencia, userban, nomcarp, tipocarpetaban) {
                //alert("Id del usuario: " + idusuario + " id carpeta: " + idcarpeta + " Dependencia:" + dependencia + " Tipo de Carpeta: " + tipocarpetaban + " Usuario: " + userban);
                document.getElementById('resp2').innerHTML = '<center><img  alt="Procesando" src="<?php echo $ruta_raiz; ?>/imagenes/loading.gif"></center>';
                var tipoEstadistica = document.getElementById('tipoEstadistica').value;
                var tipoDocumento = document.getElementById('tipoDocumento').value;
                var fecha_ini = document.getElementById('fecha_ini').value;
                var fecha_fin = document.getElementById('fecha_fin').value;
                var hora_ini = document.getElementById('hora_ini').value;
                var hora_fin = document.getElementById('hora_fin').value;
                var poststr = "action=detalles&nomcarp=" + nomcarp + "&idusuaban=" + idusuario + "&dependencia_ban=" + dependencia + "&carpetanumero=" + idcarpeta + "&tipoEstadistica=" + tipoEstadistica + "&tipocarpetaban=" + tipocarpetaban + "&userban=" + userban + "&tipoDocumento=" + tipoDocumento + "&fecha_ini=" + fecha_ini + "&fecha_fin=" + fecha_fin + "&hora_ini=" + hora_ini + "&hora_fin=" + hora_fin;
                url = "<?php echo $script; ?>";
                partes(url, 'resp2', poststr, '');
            }

            function detallesEst3(nombremed, idmedio) {
                //alert("Nombre del Medio: " + nombremed + " id del medio: " + idmedio);
                document.getElementById('resp2').innerHTML = '<center><img  alt="Procesando" src="<?php echo $ruta_raiz; ?>/imagenes/loading.gif"></center>';
                var tipoEstadistica = document.getElementById('tipoEstadistica').value;
                var fecha_ini = document.getElementById('fecha_ini').value;
                var fecha_fin = document.getElementById('fecha_fin').value;
                var hora_ini = document.getElementById('hora_ini').value;
                var hora_fin = document.getElementById('hora_fin').value;
                var dependencia_busq = document.getElementById('dependencia_busq').value;
                var poststr = "action=detalles&nombremed=" + nombremed + "&dependencia_busq=" + dependencia_busq + "&tipoEstadistica=" + tipoEstadistica + "&fecha_ini=" + fecha_ini + "&fecha_fin=" + fecha_fin + "&hora_ini=" + hora_ini + "&hora_fin=" + hora_fin + "&idmedio=" + idmedio;
                url = "<?php echo $script; ?>";
                partes(url, 'resp2', poststr, '');

            }

            function detalleseq(dependencia) {
                //alert("Codigo de la Dependencia: " + dependencia);
                document.getElementById('resp2').innerHTML = '<center><img  alt="Procesando" src="<?php echo $ruta_raiz; ?>/imagenes/loading.gif"></center>';
                var tipoEstadistica = document.getElementById('tipoEstadistica').value;
                var fecha_ini = document.getElementById('fecha_ini').value;
                var fecha_fin = document.getElementById('fecha_fin').value;
                var hora_ini = document.getElementById('hora_ini').value;
                var hora_fin = document.getElementById('hora_fin').value;
                var tipoRadicado = document.getElementById('tipoRadicado').value;
                var tipoDocumento = document.getElementById('tipoDocumento').value;
                var poststr = "action=detalles&dependencia_in=" + dependencia + "&tipoEstadistica=" + tipoEstadistica + "&fecha_ini=" + fecha_ini + "&fecha_fin=" + fecha_fin + "&hora_ini=" + hora_ini + "&hora_fin=" + hora_fin + "&tipoDocumento=" + tipoDocumento + "&tipoRadicado=" + tipoRadicado;
                url = "<?php echo $script; ?>";
                partes(url, 'resp2', poststr, '');
            }

            function detallesedst(usuacodi) {
                //alert("Codigo del Usuario: " + usuacodi);
                document.getElementById('resp2').innerHTML = '<center><img  alt="Procesando" src="<?php echo $ruta_raiz; ?>/imagenes/loading.gif"></center>';
                var tipoEstadistica = document.getElementById('tipoEstadistica').value;
                var fecha_ini = document.getElementById('fecha_ini').value;
                var fecha_fin = document.getElementById('fecha_fin').value;
                var hora_ini = document.getElementById('hora_ini').value;
                var hora_fin = document.getElementById('hora_fin').value;
                var tipoDocumento = document.getElementById('tipoDocumento').value;
                var tipoRadicado = document.getElementById('tipoRadicado').value;
                var dependencia_busq = document.getElementById('dependencia_busq').value;
                var poststr = "action=detalles&dependencia_busq=" + dependencia_busq + "&tipoEstadistica=" + tipoEstadistica + "&fecha_ini=" + fecha_ini + "&fecha_fin=" + fecha_fin + "&hora_ini=" + hora_ini + "&hora_fin=" + hora_fin + "&tipoDocumento=" + tipoDocumento + "&tipoRadicado=" + tipoRadicado + "&codus=" + usuacodi;
                url = "<?php echo $script; ?>";
                partes(url, 'resp2', poststr, '');
            }

            function detallese19(usuacodi, depeusua) {
                //alert("Codigo del Usuario: " + usuacodi);
                document.getElementById('resp2').innerHTML = '<center><img  alt="Procesando" src="<?php echo $ruta_raiz; ?>/imagenes/loading.gif"></center>';
                var tipoEstadistica = document.getElementById('tipoEstadistica').value;
                var fecha_ini = document.getElementById('fecha_ini').value;
                var fecha_fin = document.getElementById('fecha_fin').value;
                var hora_ini = document.getElementById('hora_ini').value;
                var hora_fin = document.getElementById('hora_fin').value;
                var tipoRadicado = document.getElementById('tipoRadicado').value;
                var tipoDocumento = document.getElementById('tipoDocumento').value;
                document.getElementById('tipoRadicado').value = 0;
                var poststr = "action=detalles&depeusua=" + depeusua + "&tipoEstadistica=" + tipoEstadistica + "&fecha_ini=" + fecha_ini + "&fecha_fin=" + fecha_fin + "&hora_ini=" + hora_ini + "&hora_fin=" + hora_fin + "&codus=" + usuacodi + "&tipoDocumento=" + tipoDocumento + "&tipoRadicado=" + tipoRadicado;
                url = "<?php echo $script; ?>";
                partes(url, 'resp2', poststr, '');
            }

            function est() {
                var tp = document.getElementById('tipoEstadistica').value;

                document.getElementById('dependencia_busq').disabled = false;
                document.getElementById('fecha_ini').disabled = false;
                document.getElementById('fecha_fin').disabled = false;
                document.getElementById('hora_ini').disabled = false;
                document.getElementById('hora_fin').disabled = false;
                document.getElementById('tipoRadicado').disabled = false;
                document.getElementById('tipoDocumento').disabled = false;
                document.getElementById('usActivos').disabled = false;
                document.getElementById('codus').disabled = false;
                vistaFormUnitid('divUsu', 1);
                vistaFormUnitid('tpdocdiv', 1);
                vistaFormUnitid('tpdocdivt', 1);
                vistaFormUnitid('tpdocdivdd', 2);
                vistaFormUnitid('tpdocdivtd', 2);
                vistaFormUnitid('timedate1', 1);
                vistaFormUnitid('timedate2', 1);
                leyenda(tp);
                switch (tp)
                {
                    case '2':
                        document.getElementById('tipoDocumento').disabled = true;
                        break;
                    case '3':
                        document.getElementById('tipoDocumento').disabled = true;
                        document.getElementById('tipoRadicado').disabled = true;
                        document.getElementById('usActivos').disabled = true;
                        document.getElementById('codus').disabled = true;
                        break;
                    case '5':

                        document.getElementById('tipoRadicado').disabled = true;
                        document.getElementById('tipoDocumento').disabled = true;
                        break;
                    case '6':
                        document.getElementById('fecha_ini').disabled = true;
                        document.getElementById('fecha_fin').disabled = true;
                        document.getElementById('hora_ini').disabled = true;
                        document.getElementById('hora_fin').disabled = true;
                        break;

                    case '10':
                        document.getElementById('tipoRadicado').disabled = true;
                        document.getElementById('hora_ini').disabled = true;
                        document.getElementById('hora_fin').disabled = true;
                        vistaFormUnitid('timedate1', 2);
                        vistaFormUnitid('timedate2', 2);
                        break;
                    case '15':
                        //  document.getElementById('tipoRadicado').disabled = true;
                        document.getElementById('tipoRadicado').value = 2;
                        document.getElementById('usActivos').disabled = true;
                        document.getElementById('codus').disabled = true;
                        break;
                    case '18':
                        //document.getElementById('tipoRadicado').disabled = true;
                        document.getElementById('usActivos').disabled = true;
                        document.getElementById('codus').disabled = true;
                        document.getElementById('tipoDocumento').disabled = true;
                        //document.getElementById('dependencia_busq').disabled = true;
                        vistaFormUnitid('divUsu', 2);
                        vistaFormUnitid('tpdocdiv', 2);
                        vistaFormUnitid('tpdocdivt', 2);
                        vistaFormUnitid('tpdocdivdd', 1);
                        vistaFormUnitid('tpdocdivtd', 1);
                        break;
                    case '19':
                        document.getElementById('usActivos').disabled = true;
                        break;
                    default:

                }

                function leyenda(tp) {
                    var data;
                    switch (tp)
                    {
                        case '1':
                            data = "Este reporte muestra la cantidad de radicados generados por usuario. Se puede discriminar por tipo de radicación.";
                            break;
                        case '2':
                            data = "Este reporte genera la cantidad de radicados de acuerdo al medio de recepción al momento de la radicación.";
                            break;
                        case '3':
                            data = "Este reporte genera la cantidad de radicados enviados a su destino final por el área.";
                            break;
                        case '6':
                            data = "Esta estadistica trae la cantidad de radicados generados por usuario, se puede discriminar por tipo de Radicacion.";
                            break;
                        case '10':
                            data = "Este reporte muestra la cantidad de radicados que se encuentran actualmente en las diferentes bandejas de los usuarios.";
                            break;
                        case '11':
                            data = "Este reporte genera la cantidad de radicados digitalizados por usuario y el total de hojas digitalizadas."; //Se puede seleccionar el tipo de radicación.";
                            break;
                        case '15':
                            data = "Este reporte muestra la cantidad de radicados recibidos por una dependencia desde la dependencia de radicación.";
                            break;
                        case '17':
                            data = "Este reporte muestra la cantidad de radicados tramitados por un usuario.";
                            break;
                        case '18':
                            data = "Este reporte muestra la cantidad de radicados tipificados por serie y subserie .";
                            break;
                        case '19':
                            data = "Este reporte muestra la cantidad de radicados reasignados, informados, devueltos , archivados y visto bueno por los usuarios de la dependencia.";
                            break;
                        case '20':
                            data = "Este reporte muestra la cantidad de radicados por dependencia.";
                            break;
			case '21':
			    data="Este reporte muestra la cantidad de radicados los cuales ya estan por vencer o vencidos su tiempo de tramite";
			    break;
                        default:

                    }

                    document.getElementById('leyenda').innerHTML = data;

                }
            }
        </script>
        <link rel="stylesheet" href="<?php echo $ruta_raiz; ?>/estilos/Style.css" type="text/css">
    </head>

    <body>
        <header id="page_header" style="width: 98%">
            <h1>Estadisticas Por Radicados
            <!--<img valign='bottom'  src="../img/top_orfeo21.png" />-->
            </h1>
            <nav >
                <ul>
                    <li><a href="<?php echo $ruta_raiz; ?>/core/Modulos/estadistica/vista/estadisticas.php" style="color:green;">Radicados</a></li>
                    <li><a href="<?php echo $ruta_raiz; ?>/core/Modulos/estadistica/vista/EstadisticaExp.php" style="color:blue">Por Expedientes</a></li>
                    <li><a href="<?php echo $ruta_raiz; ?>/old/cuerpoest.php" style="color:blue">Tiempo Tramite</a></li>
                    <?php if ($_SESSION['esta_inac'] == 1) { ?>
                        <li><a href="<?php echo $ruta_raiz; ?>/core/Modulos/estadistica/vista/estadisticas-inac.php" style="color:blue;">Inactivas</a>
                        <?php } ?>
                </ul>
            </nav>
        </header>
        <div id='contenido' >
            <section id="posts" >
                <article class="post" >


                    <form name="formulario" style="margin: 0" method="GET" action='./vistaFormConsulta.php?PHPSESSID=m24qnhd4e6rtlet5tsgg8eegi1&fechah=20130218090243'>
                        <table width="98%"  border="0" cellpadding="0" cellspacing="2" class="tabS">

                            <tr>
                                <td class="titulos2" style="width:25%">Tipo de Consulta / Estadistica</td>
                                <td class="listado2" align="left" style="width:25%">
                                    <select name='tipoEstadistica' id='tipoEstadistica' onclick='est()' class="select"  style="width:300px">
                                        <option value='0'  selected  > << Seleccione >> </option>
                                        <option value='1'  >RADICACION - CONSULTA DE RADICADOS POR USUARIO</option>
                                        <option value='2'  >RADICACION - ESTADISTICAS POR MEDIO DE RECEPCION-ENVIO</option>
                                        <option value='3'  >RADICACION - ESTADISTICAS DE MEDIO ENVIO FINAL DE DOCUMENTOS</option>
                                        <!-- <option value='4'  >RADICACION - ESTADISTICAS DE DIGITALIZACION DE DOCUMENTOS</option> -->
                                        <!--                                       <option value='5'  >RADICADOS DE ENTRADA RECIBIDOS DEL AREA DE CORRESPONDENCIA</option>
                                                                              cambio solicitado para ANDJE el nombre anterior es:
                                                                                <option value='6'  >RADICADOS ACTUALES EN LA DEPENDENCIA</option>-->
                                        <option value='10' >RADICADOS POR BANDEJA</option>
                                        <option value='11' >ESTADISTICAS DE DIGITALIZACION</option>
                                        <option value='15' >RADICADOS DE ENTRADA RECIBIDOS DEL AREA DE CORRESPONDENCIA (DEPENDENCIA)</option>
                                        <option value='17' >ESTADISTICA POR RADICADOS Y SUS RESPUESTAS</option>
                                        <option value='18' >ESTADISTICAS POR SERIES DOCUMENTALES</option>
                                        <option value='19' >RADICADOS POR TRANSACCIONES</option>
                                        <?php                                         if ($_SESSION['perm_esta_extra']) {
                                            ?>

                                            <option value='20' >RADICADOS - POR DEPENDENCIA</option>
					    <option value='21'>RADICADOS POR VENCIMIENTOS - POR DEPENDENCIA</option>
                                            <?php                                         }
                                        ?>
                                    </select>
                                </td>

                                <td   class="titulos2" style="width:25%">Desde fecha (dd/mm/aaaa) </td>
                                <td  class="listado2" style="width:25%"><div id='timedate1' style='display: none'>
                                        &nbsp;<?php $mesante = date("d/m/Y", mktime(0, 0, 0, date("m") - 1, date("d"), date("Y"))); ?>
                                        <input readonly="true" type="text" class="tex_area" name="fecha_ini"	id="fecha_ini" size="10" value="<?php echo $mesante; ?>" /> 
                                        <script	language="javascript">
                                            var A_CALTPL = {'imgpath': '<?php echo  $ruta_raiz ?>/js/calendario/img/',
                                                'months': ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                                                'weekdays': ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'], 'yearscroll': true, 'weekstart': 1, 'centyear': 70}
                                            new tcal({'formname': 'formulario', 'controlname': 'fecha_ini'}, A_CALTPL);</script>

                                        <select  class="select" id="hora_ini" name="hora_ini" >

                                            <?php                                             $optionp = '';
                                            for ($index = 0; $index < 24; $index++) {
                                                if ($index == 6)
                                                    $sel = 'selected';
                                                else
                                                    $sel = '';
                                                $optionp.="<option $sel>$index:00</option>";
                                            }
                                            echo $optionp;
                                            ?>
                                            <option >23:59</option>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td  class="titulos2">Dependencia</td>

                                <td class="listado2" >
                                    <select name='dependencia_busq' id='dependencia_busq'  class="select"  onChange="usuarios()" style="width:300px">
                                        <?php echo $optionDepe; ?>	</select>
                                </td>
                                <td   class="titulos2"  >
                                    Hasta  fecha (dd/mm/aaaa)
                                </td>

                                <td   class="listado2"><div id='timedate2' style='display: none'>
                                        &nbsp;
                                        <input readonly="true" type="text" class="tex_area" name="fecha_fin"	id="fecha_fin" size="10" value="<?php echo date('d/m/Y'); ?>" /> 
                                        <script	language="javascript">
                                            var A_CALTPL = {'imgpath': '<?php echo  $ruta_raiz ?>/js/calendario/img/',
                                                'months': ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                                                'weekdays': ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'], 'yearscroll': true, 'weekstart': 1, 'centyear': 70}
                                            new tcal({'formname': 'formulario', 'controlname': 'fecha_fin'}, A_CALTPL);</script>

                                        <select  class="select" id="hora_fin" name="hora_fin" >
                                            <?php                                             $optionp = '';
                                            for ($index = 0; $index < 24; $index++) {
                                                if ($index == 19)
                                                    $sel = 'selected';
                                                else
                                                    $sel = '';
                                                $optionp.="<option $sel>$index:00</option>";
                                            }
                                            echo $optionp;
                                            ?>
                                            <option >23:59</option>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr id="cUsuario">
                                <td  class="titulos2" >Usuario
                                    &nbsp;&nbsp;
                                    <select name="usActivos" id="usActivos"  onchange="usuarios()">
                                        <option value='0'>Solo Activos</option>
                                        <option value='1'>Incluir Inactivos</option>
                                    </select>
                                    <!--<input name="usActivos" id="usActivos" type="checkbox" class="select" onChange="usuarios();" >
                                    Incluir Usuarios Inactivos-->  </td>
                                <td class="listado2">
                                    <div id='divUsu'>
                                        <select name="codus" id="codus" class="select"  >
                                            <option value=0> -- AGRUPAR POR TODOS LOS USUARIOS --</option>
                                        </select></div>
                                    &nbsp;</td>
                                <td  height="40" class="titulos2" colspan="1">Tipo de Radicado  </td>
                                <td  height="40" class="listado2" colspan="1">
                                    <select name="tipoRadicado" id="tipoRadicado" class=select>
                                        <option value=""> -- Todos -- </option>
                                        <option value='1'>Salida</option>
                                        <option value='2'>Entrada</option>
                                        <option value='3'>Memorando</option>
                                        <option value='4'>Reclamaciones</option>
                                        <option value='5'>Reportes</option>
                                        <option value='7'>Salida de Ministerio</option>
                                        <option value='9'>Informes</option>
                                    </select>
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td  height="40" class="titulos2"><div id="tpdocdivt">Tipo de Documento </div><div id="tpdocdivtd" style='display: none'>Serie </div></td>

                                <td class="listado2" >
                                    <div id="tpdocdiv"><select name='tipoDocumento' id='tipoDocumento'  class="select" style="width:290px">
                                            <option value='9999'  >-- No Agrupar Por Tipo de Documento</option>
                                            <option value='0'  >-- Tipos Documentales No Definidos</option>
                                            <?php echo $optionTp; ?>		</select></div>
                                    <div id="tpdocdivdd" style='display: none'>
                                        <select name='Serie' id='Serie'  class="select" style="width:290px">
                                            <?php echo $optionSr; ?></select></div>
                                </td>



                                <td colspan="2" class="listado2">
                            <center>
                                <input  type="reset" class="botones_funcion" value="Limpiar"> 
                                <input type="button" onclick="consultar();" class="botones_funcion" value="Generar" name="generarOrfeo">
                            </center>
                            </td>
                            </tr>
                        </table>
                    </form>

                    <table style="width: 98%">
                        <tr>
                            <td class="tabS"  colspan="2" ><marquee id="leyenda" behavior="scroll" direction="left" class="titulosEst"></marquee></td>
                        </tr>

                        <tr>
                            <td valign='top' style='width : 350px; height : 350px;' ><div style=" margin-top: 0;padding : 1px; width : 350px; height : 350px; overflow : auto;"  id="resp" ></div></td>
                            <td ><div id="resp2"  style=" padding : 1px; width : 100%; height : 350px; overflow : auto;  "></div></td>
                        </tr>
                    </table>
                    <footer>
                    </footer>
                </article>
            </section>



        </div><?php if ($permEsta < 2) { ?>
            <script>usuarios()</script>
        <?php } ?>
    </body>
</html>
