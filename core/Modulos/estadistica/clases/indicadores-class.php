<?php 
/**
 * Clase de indicadores encargada de proveer información procesada del sistema
 * @author Diego Enrique Rodriguez
 * @name Indicadores
 * @version     1.0
 */
if (!$ruta_raiz)
    $ruta_raiz = '../../../..';
include_once "$ruta_raiz/core/Modulos/estadistica/modelo/modeloIndicadores.php";

/**
 *  Objecto indicadores
 */
class indicadores {
	private $tpRadicado;
	private $indicador;
	private $depeCodi;
	private $fechaIni;
	private $fechaFin;
    private $modelo;
    private $ruta_raiz;

    function __construct($ruta_raiz) {
        $this->modelo = new modeloIndicadores($ruta_raiz);
        $this->ruta_raiz = $ruta_raiz;
    }
	
	public function getTpRadicado(){
		return $this->tpRadicado;
	}
	
	public function setTpRadicado($tpRadicado){
		$this->tpRadicado=$tpRadicado;
	}
	
	public function getDepeCodi(){
		return $this->depeCodi;
	}
	
	public function setDepeCodi($depeCodi){
		$this->depeCodi=$depeCodi;
	}
	
	public function getIndicador(){
		return $this->indicador;
	}
	
	public function setIndicador($indicador){
		$this->indicador=$indicador;
	}
	
	public function getFechaIni(){
		return $this->fechaIni;
	}
	
	public function setFechaIni($fechaIni){
		$this->fechaIni=$fechaIni;
	}
	
	public function getFechaFin(){
		return $this->fechaFin;
	}
	
	public function setFechaFin($fechaFin){
		$this->fechaFin=$fechaFin;
	}
	
	function listarIndicadores(){
		$retorno="<select class='select' id='selindica'>\n";
		$tprad=$this->tpRadicado;
		switch($tprad){
			case 1:
				$retorno.=	"<option value=1>Gestión de Documentos</option>
							<option value=2>Documentos Sin Imagen</option>
							<option value=3>Documentos Anulados</option>
							<option value=4>Origen Tr&aacute;mite</option>
							<option value=5>Correspondencia Externa</option>
							<option value=6>Oficio de tr&aacute;mites</option>\n";
				break;
			case 2:
				$retorno.=	"<option value=1>Gestión de Documentos</option>
							<option value=2>Documentos Sin Imagen</option>
							<option value=7>Origen Documento</option>\n";
				break;
			case 3:
				$retorno.=	"<option value=1>Gestión de Documentos</option>
							<option value=2>Documentos Sin Imagen</option>
							<option value=3>Documentos Anulados</option>
							<option value=4>Origen Tr&aacute;mite</option>\n";
				break;
		}
		$retorno.="</select>\n";
		return $retorno;
	}
	
	function infoIndicador($indica){
	//	echo "El indicador es ".$this->indicador;
		switch($indica){
			case 1:
				$titulo="Gesti&oacute;n de Documentos";
				list($resultado,$total)=$this->modelo->infoGestionDocumentos($this->tpRadicado,$this->depeCodi,$this->fechaIni,$this->fechaFin);
				break;
			case 2:
                                $titulo="Documentos Sin Imagen";
				list($resultado,$total)=$this->modelo->infoSinImagen($this->tpRadicado,$this->depeCodi,$this->fechaIni,$this->fechaFin);
				break;
			case 3:
                                $titulo="Documentos Anulados";
				list($resultado,$total)=$this->modelo->infoAnulados($this->tpRadicado,$this->depeCodi,$this->fechaIni,$this->fechaFin);
				break;
			case 4:
                                $titulo="Origen Tr&aacute;mite";
				list($resultado,$total)=$this->modelo->infoOrigenTx($this->tpRadicado,$this->depeCodi,$this->fechaIni,$this->fechaFin);
				break;
			case 5:
                                $titulo="Correspondencia Externa";
				list($resultado,$total)=$this->modelo->infoCorresExt($this->tpRadicado,$this->depeCodi,$this->fechaIni,$this->fechaFin);
				break;
			case 6:
                                $titulo="Oficio de tr&aacute;mites";
				list($resultado,$total)=$this->modelo->infoOficioTr($this->tpRadicado,$this->depeCodi,$this->fechaIni,$this->fechaFin);
				break;
			case 7:
                                $titulo="Origen Tr&aacute;mite";
				list($resultado,$total)=$this->modelo->infoOrigenTr($this->tpRadicado,$this->fechaIni,$this->fechaFin);
				break;
		}
		if($total>0){
		   $retorno="<table width=\"100%\">
			<tr class='titulos4'><th colspan=3>$titulo</th></tr>
			<tr class='tpar'>
                	<td class='titulos3'>Indicador</td><td class='titulos3'>Radicados</td><td class='titulos3'>Porcentaje</td></tr>";
		   for($i=0;$i<count($resultado);$i++){
			if ($i % 2) {
                	    $style = " class='listado1' ";
            		} else {
                	    $style = " class='listado2' ";
            		}
			$retorno.="<tr $style><td>{$resultado[$i]['indicador']}</td>
				<td style='text-align:center;'>{$resultado[$i]['radicados']}</td>
				<td style='text-align:center;'>".round($resultado[$i]['radicados']*100/$total,2)."%</td></tr>";
		   }
		   $retorno.="</table>\n";
		}
		else{
		    $retorno="No se encontraron datos";
		}
		return $retorno;
	}

	function todoIndicador($usuario){
	    $trad=$this->tpRadicado;
	    $retorno="";
	    switch($trad){
		case 1:
		    $retorno.="<table class='borde_tab' width='96%'>
			<tr class='titulos4'><td colspan=6>Indicadores de documentos de salida</td></tr>
			<tr><td class='titulos2' width='16%'>Total Radicados:</td><td class='titulos5' width='16%'>".$this->modelo->totalRadicados($trad,$this->fechaIni,$this->fechaFin)."</td>
			<td class='titulos2' width='16%'>Intervalo de tiempo:</td><td class='titulos5' width='16%'>De $this->fechaIni a $this->fechaFin</td>
			<td class='titulos2' width='16%'>Usuario solicitante:</td><td class='titulos5' width='16%'>$usuario</td></tr>";
		    $tind=array(1,2,3,4,5,6);
		    for($i=0;$i<count($tind);$i++){
			//$this->setTpRadicado($tind[$i]);
			if($i%3==0 && $i==0){
			    $retorno.="<tr>\n";
			}
			elseif($i%3==0){
			    $retorno.="</tr><tr>\n";
			}
			$retorno.="<td colspan=2 valign='top'>\n";
			$retorno.=$this->infoIndicador($tind[$i]);
			$retorno.="</td>";
		    }
		    $retorno.="</tr></table>";
		    break;
		case 2:
		    $retorno.="<table class='borde_tab' width='96%'>
                        <tr class='titulos4'><td colspan=6>Indicadores de documentos de entrada</td></tr>
                        <tr><td class='titulos2' width='16%'>Total Radicados:</td><td class='titulos5' width='16%'>".$this->modelo->totalRadicados($trad,$this->fechaIni,$this->fechaFin)."</td>
                        <td class='titulos2' width='16%'>Intervalo de tiempo:</td><td class='titulos5' width='16%'>De $this->fechaIni a $this->fechaFin</td>
                        <td class='titulos2' width='16%'>Usuario solicitante:</td><td class='titulos5' width='16%'>$usuario</td></tr>";
                    $tind=array(1,2,7);
                    for($i=0;$i<count($tind);$i++){
                        //$this->setTpRadicado($tind[$i]);
			if($i%3==0 && $i==0){
                            $retorno.="<tr>\n";
                        }
                        elseif($i%3==0){
                            $retorno.="</tr><tr>\n";
                        }
			$retorno.="<td colspan=2 valign='top'>\n";
                        $retorno.=$this->infoIndicador($tind[$i]);
			$retorno.="</td>";
                    }
		    $retorno.="</tr></table>";
                    break;
		case 3:
		    $retorno.="<table class='borde_tab' width='96%'>
                        <tr class='titulos4'><td colspan=6>Indicadores de documentos de interna</td></tr>
                        <tr><td class='titulos2' width='16%'>Total Radicados:</td><td class='titulos5' width='16%'>".$this->modelo->totalRadicados($trad,$this->fechaIni,$this->fechaFin)."</td>
                        <td class='titulos2' width='16%'>Intervalo de tiempo:</td><td class='titulos5' width='16%'>De $this->fechaIni a $this->fechaFin</td>
                        <td class='titulos2' width='16%'>Usuario solicitante:</td><td class='titulos5' width='16%'>$usuario</td></tr>";
                    $tind=array(1,2,3,4);
                    for($i=0;$i<count($tind);$i++){
                        //$this->setTpRadicado($tind[$i]);
			if($i%3==0 && $i==0){
                            $retorno.="<tr>\n";
                        }
                        elseif($i%3==0){
                            $retorno.="</tr><tr>\n";
                        }
                        $retorno.="<td colspan=2 valign='top'>\n";
                        $retorno.=$this->infoIndicador($tind[$i]);
			$retorno.="</td>";
                    }
		    $retorno.="</tr></table>";
                    break;
	    }
	    return $retorno;
	}
}
?>
