<?php 
/** 
 * Estadisticas es la clase encargada de gestionar las operaciones estadisticas
 * @author Hardy Deimont Niño  Velasquez
 * @name Estadisticas
 * @version	1.0
 */ 
if (! $ruta_raiz)
	$ruta_raiz = '../../../..';
    include_once "$ruta_raiz/core/Modulos/estadistica/modelo/modeloEstadistica.php";
	//==============================================================================================	
	// CLASS tpDocumento
	//==============================================================================================	
	
	/**
	 *  Objecto estadisticas 
	 */ 

class estadisticas  {
	private $modelo;

	 
	
	function __construct($ruta_raiz) {
		$this->modelo = new modeloEstadisticas( $ruta_raiz );
	}
	

	/**
	 * Consulta los datos de las series 
	 * @return   void
	 */
	function consultar($subserie,$depe,$serieid,$ano) {
			$rs = $this->modelo->consultar ($subserie,$depe,$serieid,$ano);
			if($rs['error']){
				return $rs['error'];
				
			}
			$f=count($rs);
			$data.="<table>
	<tr class=tpar>
		<td class=titulos3 colspan=4>Expedientes total $f</td>
	</tr>					
					<trclass='tpar'>
		<td class='titulos3' >#</td><td class='titulos3'>N&uacute;mero</td><td class='titulos3'>A&ntilde;o</td><td class='titulos3'>Titulo</td></tr>";
			for ($i = 0; $i < count($rs); $i++) {
				if($i%2){
					$style=" class='listado1' ";
				}else{
					$style=" class='listado2' ";
				}
				$c=$i+1;
				$data.="<tr $style><td>$c</td><td>".$rs [$i] ["numero"]."</td><td>". $rs [$i] ["year"]."</td><td>". $rs [$i] ["titulo"]."</td></tr>";
			}
			$data.="</table>";
								
			return $data;	
	}
	
        	/**
	 * Consulta los datos de las series 
	 * @return   void
	 */
	function estadistica5($fecha_ini,$fecha_fin,$depe,$estado) {
           // echo "$fechIni,$fechFin,$depe,$estado";
			$rs = $this->modelo->est5 ($fecha_ini,$fecha_fin,$depe,$estado);
			if($rs['error']){
				return $rs['error'];
				
			}
                        //print_r($rs);
			$f=count($rs);		
                         $data.="<table>
					<trclass='tpar'>
		<td class='titulos3'>#</td><td class='titulos3'>Usuario</td><td class='titulos3'>Radicados</td></tr>";
			for ($i = 0; $i < count($rs); $i++) {
				if($i%2){
					$style=" class='listado1' ";
				}else{
					$style=" class='listado2' ";
				}
				$c=$i+1;
				$data.="<tr $style><td>$c</td><td>".$rs [$i] ["usuario"]."</td><td><a href='#' onclick='detalles(". $rs [$i] ["ucodi"].");'>". $rs [$i] ["radicados"]."</a></td></tr>";
			} 
			$data.="</table>";

                        
			return $data;	
	}
	

        function detallesEst5($fecha_ini,$fecha_fin,$idusu) {
           // echo "$fechIni,$fechFin,$depe,$estado";
			$rs = $this->modelo->detallesEst5 ($fecha_ini,$fecha_fin,$idusu);
			if($rs['error']){
				return $rs['error'];
				
			}
                        //print_r($rs);
     
			$f=count($rs);		
                         $data.="<table>
                             <trclass='tpar'>
		<td class='titulos3' colspan='10'>".$rs [0] ["usuario"]."</td></tr>
					<trclass='tpar'>
		<td class='titulos3'>#</td><td class='titulos3'>Radicados</td><td class='titulos3'>Asunto</td><td class='titulos3'>Fecha Radicacion</td></tr>";
			for ($i = 0; $i < count($rs); $i++) {
				if($i%2){
					$style=" class='listado1' ";
				}else{
					$style=" class='listado2' ";
				}
				$c=$i+1;
				$data.="<tr $style><td>$c</td><td>".$rs [$i] ["radicados"]."</td><td>". $rs [$i] ["asunto"]."</td><td>". $rs [$i] ["fech"]."</td></tr>";
			} 
			$data.="</table>";

                        
			return $data;	
	}
        
              	/**
	 * Consulta los datos de las series 
	 * @return   void
	 */
	function estadistica6($depe,$usuario,$estado,$tipoRadicado,$tipoDocumento) {
           // echo "$fechIni,$fechFin,$depe,$estado";
			$rs = $this->modelo->est6 ($depe,$usuario,$estado,$tipoRadicado,$tipoDocumento);
			if($rs['error']){
				return $rs['error'];
				
			}
                        
                        //print_r($rs);
			$f=count($rs);		
                         $data.="<table>
					<trclass='tpar'>
		<td class='titulos3'>#</td><td class='titulos3'>Usuario</td><td class='titulos3'>Radicados</td></tr>";
			for ($i = 0; $i < count($rs); $i++) {
				if($i%2){
					$style=" class='listado1' ";
				}else{
					$style=" class='listado2' ";
				}
				$c=$i+1;
				$data.="<tr $style><td>$c</td><td>".$rs [$i] ["usuario"]."</td><td><a href='#' onclick='detalles(". $rs [$i] ["ucodi"].");'>". $rs [$i] ["radicados"]."</a></td></tr>";
			} 
			$data.="</table>";

                        
			return $data;	
	}
	
         function detallesEst6($idusu,$tipoRadicado,$tipoDocumento) {
           // echo "$fechIni,$fechFin,$depe,$estado";
			$rs = $this->modelo->detallesEst6 ($idusu,$tipoRadicado,$tipoDocumento);
			if($rs['error']){
				return $rs['error'];
				
			}
                        //print_r($rs);
                          
			$f=count($rs);		
                         $data.="<table>
                             <trclass='tpar'>
		<td class='titulos3' colspan='10'>".$rs [0] ["usuario"]."</td></tr>
					<trclass='tpar'>
		<td class='titulos3'>#</td><td class='titulos3'>Radicados</td><td class='titulos3'>Asunto</td><td class='titulos3'>Fecha Radicacion</td>
               <td class='titulos3'>Tipo Documento</td><td class='titulos3'>Usuario anterior</td> </tr>";
			for ($i = 0; $i < count($rs); $i++) {
				if($i%2){
					$style=" class='listado1' ";
				}else{
					$style=" class='listado2' ";
				}
				$c=$i+1;
				$data.="<tr $style><td>$c</td><td>".$rs [$i] ["radicados"]."</td><td>". $rs [$i] ["asunto"]."</td><td>". $rs [$i] ["fech"]."</td>
                                    <td>". $rs [$i] ["tpdesc"]."</td><td>". $rs [$i] ["uante"]."</td></tr>";
			} 
			$data.="</table>";

                        
			return $data;	
	}
        
	function __destruct() {
			
	}
	  	/**
	 * Consulta los datos de las series 
	 * @return   void
	 */
	function estadistica1($depe,$fecha_ini,$fecha_fin,$usuario,$estado,$tipoRadicado,$tipoDocumento) {
           // echo "$fechIni,$fechFin,$depe,$estado";
			$rs = $this->modelo->est1 ($depe,$fecha_ini,$fecha_fin,$usuario,$estado,$tipoRadicado,$tipoDocumento);
			if($rs['error']){
				return $rs['error'];
				
			}
                        //print_r($rs);
			$f=count($rs);		
                         $data.="<table>
					<trclass='tpar'>
		<td class='titulos3'>#</td><td class='titulos3'>Usuario</td><td class='titulos3'>Radicados</td></tr>";
			for ($i = 0; $i < count($rs); $i++) {
				if($i%2){
					$style=" class='listado1' ";
				}else{
					$style=" class='listado2' ";
				}
				$c=$i+1;
				$data.="<tr $style><td>$c</td><td>".$rs [$i] ["usuario"]."</td><td><a href='#' onclick='detalles(". $rs [$i] ["ucodi"].");'>". $rs [$i] ["radicados"]."</a></td></tr>";
			} 
			$data.="</table>";

                        
			return $data;	
	}
         function detallesEst1($fechIni, $fechFin, $idusua, $tipoRadicado, $tipoDocumento) {
           // echo "$fechIni,$fechFin,$depe,$estado";
			$rs = $this->modelo->detallesEst1($fechIni, $fechFin, $idusua, $tipoRadicado, $tipoDocumento);
			if($rs['error']){
				return $rs['error'];
				
			}
                        //print_r($rs);
                          
			$f=count($rs);		
                         $data.="<table>
                             <trclass='tpar'>
		<td class='titulos3' colspan='10'>".$rs [0] ["usuario"]."</td></tr>
					<trclass='tpar'>
		<td class='titulos3'>#</td><td class='titulos3'>Radicados</td><td class='titulos3' style='width: 100px'>Asunto</td><td class='titulos3'>Radicador</td><td class='titulos3'>Fecha Radicacion</td>
               <td class='titulos3'>Tipo Documento</td></tr>";
			for ($i = 0; $i < count($rs); $i++) {
				if($i%2){
					$style=" class='listado1' ";
				}else{
					$style=" class='listado2' ";
				}
				$c=$i+1;
				$data.="<tr $style><td>$c</td><td>".$rs [$i] ["radicados"]."</td><td><label alt='". $rs [$i] ["asunto"]."'>". substr( $rs [$i] ["asunto"], 0, 100)."</label></td><td>". $rs [$i] ["usuario"]."</td><td>". $rs [$i] ["fech"]."</td>
                                    <td>". $rs [$i] ["tpdesc"]."</td></tr>";
			} 
			$data.="</table>";
			return $data;	
                        
	}

        function estadistica2($depe,$fecha_ini,$fecha_fin,$usuario,$estado,$tipoRadicado,$tipoDocumento) {
           // echo "$fechIni,$fechFin,$depe,$estado";
            
			$rs = $this->modelo->est2 ($depe,$fecha_ini,$fecha_fin,$usuario,$estado,$tipoRadicado,$tipoDocumento);
			if($rs['error']){
				return $rs['error'];
				
			}
                        //print_r($rs);
			$f=count($rs);		
                         $data.="<table>
					<trclass='tpar'>
		<td class='titulos3'>#</td><td class='titulos3'>Medio De Racepción</td><td class='titulos3'>Radicados</td></tr>";
			for ($i = 0; $i < count($rs); $i++) {
				if($i%2){
					$style=" class='listado1' ";
				}else{
					$style=" class='listado2' ";
				}
				$c=$i+1;
				$data.="<tr $style><td>$c</td><td>".$rs [$i] ["usuario"]."</td><td><a href='#' onclick='detalles(". $rs [$i] ["ucodi"].");'>". $rs [$i] ["radicados"]."</a></td></tr>";
			} 
			$data.="</table>";

                        
			return $data;	
	}
        
            function detallesEst2($fechIni, $fechFin, $idusua, $tipoRadicado, $tipoDocumento,$tpEnv,$depe,$estado) {
           // echo "$fechIni,$fechFin,$depe,$estado";
			$rs = $this->modelo->detallesEst2($fechIni, $fechFin, $idusua, $tipoRadicado, $tipoDocumento,$tpEnv,$depe,$estado);
			if($rs['error']){
				return $rs['error'];
				
			}
                        //print_r($rs);
                          
			$f=count($rs);		
                         $data.="<table>
                             <trclass='tpar'>
		<td class='titulos3' colspan='10'>".$rs [0] ["usuario"]."</td></tr>
					<trclass='tpar'>
		<td class='titulos3'>#</td><td class='titulos3'>Radicados</td><td class='titulos3'>Asunto</td><td class='titulos3'>Radicador</td><td class='titulos3'>Fecha Radicacion</td>
               <td class='titulos3'>Tipo Documento</td><td class='titulos3'>Medio Recepcion</td></tr>";
			for ($i = 0; $i < count($rs); $i++) {
				if($i%2){
					$style=" class='listado1' ";
				}else{
					$style=" class='listado2' ";
				}
				$c=$i+1;
				$data.="<tr $style><td>$c</td><td>".$rs [$i] ["radicados"]."</td><td>". $rs [$i] ["asunto"]."</td><td>". $rs [$i] ["usuario"]."</td><td>". $rs [$i] ["fech"]."</td>
                                    <td>". $rs [$i] ["tpdesc"]."</td><td>". $rs [$i] ["mrec"]."</td></tr>";
			} 
			$data.="</table>";
			return $data;	
                        
	}
        
        /*****************************************************
	**                                                  **
	**  Consulta para la estadistica numero 3           ** 
	**  ESTADISTICAS DE MEDIO ENVIO FINAL DE DOCUMENTOS **
	**                                                  **
	*****************************************************/
	
	function estadistica3($depe,$fecha_ini,$fecha_fin) {
            
			$rs = $this->modelo->est3($depe,$fecha_ini,$fecha_fin);
			if($rs['error']){
				return $rs['error'];
				
			}
                        //print_r($rs);
			$f=count($rs);		
                         $data.="<table>
						 <tr><td colspan='3'class='cosa'>MEDIO DE ENVIO FINAL DE DOCUMENTOS</td></tr>
					<trclass='tpar'>
		<td class='titulos3'>#</td><td class='titulos3'>Medio De Racepción</td><td class='titulos3'>Radicados</td></tr>";
			for ($i = 0; $i < count($rs); $i++) {
				if($i%2){
					$style=" class='listado1' ";
				}else{
					$style=" class='listado2' ";
				}
				$c=$i+1;
				$data.="<tr $style><td>$c</td><td>".$rs [$i] ["nombremed"]."</td><td><a href='#' onclick='detallesEst3(\"". $rs [$i] ["nombremed"]."\",".$rs [$i] ["idmedio"].");'>". $rs [$i] ["radicados"]."</a></td></tr>";
			} 
			$data.="</table>";

                        
			return $data;	
	}
	
	/*****************************************************
	**                                                  **
	**  Detalles para la estadistica numero 3           ** 
	**  ESTADISTICAS DE MEDIO ENVIO FINAL DE DOCUMENTOS **
	**                                                  **
	*****************************************************/
	
	function detallesEst3($depe,$fecha_ini,$fecha_fin,$nombremed,$idmedio) {
            
			$rs = $this->modelo->detallesEst3($depe,$fecha_ini,$fecha_fin,$idmedio);
			if($rs['error']){
				return $rs['error'];
				
			}
                        //print_r($rs);
			$f=count($rs);
			$fechainf = date ( "Y:m:d H:m:s" );
                         $data.="<table><tr class='tpar'><td class='titulos3' colspan='9'>ESTADISTICAS DE MEDIO DE ENVIO FINAL DE DOCUMENTOS</td></tr>
                            <trclass='tpar'>
								<td class='titulos3' colspan='10'>
									<table>
										<tr class='tpar'>
											<td class='titulos3' colspan='2'>Medio de Envio: ".$nombremed."</td>
											<td class='titulos3'>Fecha Generacion del Informe: ".$fechainf."</td>
										</tr>
									</table>
								</td>
							</tr>
					<trclass='tpar'>
		<td class='titulos3'>#</td><td class='titulos3'>Numero de Radicado</td><td class='titulos3'>Fecha de Radicación</td><td class='titulos3'>Destinatario</td><td class='titulos3'>Dirección</td>
               <td class='titulos3'>Numero de <br>Planilla</td></tr>";
			for ($i = 0; $i < count($rs); $i++) {
				if($i%2){
					$style=" class='listado1' ";
				}else{
					$style=" class='listado2' ";
				}
				$c=$i+1;
				$data.="<tr $style><td>$c</td><td>".$rs [$i] ["radicado"]."</td><td>". $rs [$i] ["rfecha"]."</td><td>".$rs [$i] ["rnombre"]."</td>
				<td>". $rs [$i] ["rdireccion"]."<br>".$rs [$i] ["rdepartamento"]." ".$rs [$i] ["rmunicipio"]."</td>
                                    <td>". $rs [$i] ["planilla"]."</td></tr>";
			} 
			$data.="</table>";

                        
			return $data;	
	}
	
	/*********************************************
	**                                          **
	**  Consulta para la estadistica numero 10  ** 
	**  RADICADOS POR BANDEJA                   **
	**                                          **
	*********************************************/
	function estadistica10($depe,$usuario,$estado,$tipoDocumento) {
			$rs = $this->modelo->est10($depe,$usuario,$estado,$tipoDocumento);
			if($rs['error']){
				return $rs['error'];
				
			}
                        //print_r($rs);
			$f=count($rs);		
                         $data.="<table id='est10'>
						 <tr><td colspan='3'class='cosa'>RADICADOS POR BANDEJAS</td></tr>
					<trclass='tpar'>
		<td class='titulos3'>#</td><td class='titulos3' colspan='2'>DETALLES</td></tr>";
			for ($i = 0; $i < count($rs); $i++) {
				if($i%2){
					$style=" class='listado1' ";
				}else{
					$style=" class='listado2' ";
				}
				
				$c=$i+1;
				//----- carpetas normales -----
				$carpetascont = count($rs[$i]["carpetas"]);
				$resultadocarpetas = "<table class='est10tit'>";
				for($z=0;$z<$carpetascont;$z++)
				{
					$array = $rs[$i]["carpetas"][$z];
					$w=0;
					foreach ($array as $uno => $valor)
					{
						if($w==0)
							{
								$resultadocarpetas .= "<tr><td>".$valor."</td>";
								$w++;
							}
							elseif($w==1)
							{
								$norcar = 0;
								$resultadocarpetas .= "<td><a href='#' onclick='detallesbandejas(".$rs [$i] ["ucodi"].",".$valor.",".$rs [$i] ["dependencia"].",\"".$rs [$i] ["usuario"]."\",".$norcar.");'>";
								$w++;
							}
							elseif($w==2)
							{
								$resultadocarpetas .= $valor."</a></td></tr>";
								$w=0;
							}
					}
				}
				$resultadocarpetas .= "</table>";
				//---------------------------------
				$carpetaspercont = count($rs[$i]["personales"]);
				$resultadoper = "<table>";
				for($x=0;$x<$carpetaspercont;$x++)
				{
					$array2 = $rs[$i]["personales"][$x];
					$y=0;
					foreach ($array2 as $dos => $valor2)
					{
							if($y==0)
							{
								$resultadoper .= "<tr><td>".$valor2."</td>";
								$y++;
							}
							elseif($y==1)
							{
								$percar = 1;
								$resultadoper .= "<td><a href='#' onclick='detallesbandejas(".$rs [$i] ["ucodi"].",".$valor2.",".$rs [$i] ["dependencia"].",\"".$rs [$i] ["usuario"]."\",".$percar.");'>";
								$y++;
							}
							elseif($y==2)
							{
								$resultadoper .= $valor2."</a></td></tr>";
								$y=0;
							}
					}
				}
				
				$resultadoper .= "</table>";
				//---------------------------------
				
				$data.="<tr $style><td rowspan='3'>$c</td><td colspan='2'>Usuario: ".$rs [$i] ["usuario"]." (".$rs [$i] ["dependencia"].")</td></tr>
				<tr $style><td class='tdest10'>CARPETAS<BR>PREDETERMINADAS</td>
				<td class='tdest10'>CARPETAS<BR>PERSONALES</td></tr>
				<tr $style><td class='est10tit'>".$resultadocarpetas."</td>
				<td class='est10tit'>".$resultadoper."</td></tr>";
			} 
			$data.="</table>";           
			return $data;
	}
	/*********************************************
	**                                          **
	**  Detalles para la estadistica numero 10  ** 
	**  RADICADOS POR BANDEJA                   **
	**                                          **
	*********************************************/
	function detallesEst10($idusuaban, $dependencia_ban, $tipocarpetaban, $userban, $carpetanumero, $tipoDocumento) {
		//echo "$idusuaban,$dependencia_ban,$tipocarpetaban,$userban,$carpetanumero<br>";
		$rs = $this->modelo->detallesEst10 ($idusuaban, $dependencia_ban, $tipocarpetaban, $carpetanumero, $tipoDocumento);
		if($rs['error']){
				return $rs['error'];
				
			}
                        //print_r($rs);
                          
			$f=count($rs);	
			$fechainf = date ( "Y:m:d H:m:s" );
                         $data.="<table><tr class='tpar'><td class='titulos3' colspan='10'>ESTADISTICA RADICADOS POR BANDEJA</td></tr>
                            <trclass='tpar'>
								<td class='titulos3' colspan='10'>
									<table>
										<tr class='tpar'>
											<td class='titulos3'>Dependencia: ".$dependencia_ban."</td>
											<td class='titulos3'>Radicados del Usuario: ".$userban."</td>
											<td class='titulos3'>Bandeja: ".$rs [0] ["bandeja"]."</td>
											<td class='titulos3'>Fecha Generacion del Informe: ".$fechainf."</td>
										</tr>
									</table>
								</td>
							</tr>
					<trclass='tpar'>
		<td class='titulos3'>#</td><td class='titulos3'>Radicados</td><td class='titulos3'>Asunto</td><td class='titulos3'>Remitente</td><td class='titulos3'>Fecha Radicacion</td>
               <td class='titulos3'>Tipo Documento</td><td class='titulos3'>Dias Restantes</td></tr>";
			for ($i = 0; $i < count($rs); $i++) {
				if($i%2){
					$style=" class='listado1' ";
				}else{
					$style=" class='listado2' ";
				}
				$c=$i+1;
				$data.="<tr $style><td>$c</td><td>".$rs [$i] ["radicados"]."</td><td>". $rs [$i] ["asunto"]."</td><td>".$rs [$i] ["remitente"]."</td><td>". $rs [$i] ["fecha_radicado"]."</td>
                                    <td>". $rs [$i] ["tipo_documento"]."</td><td>". $rs [$i] ["dias_restantes"]."</td></tr>";
			} 
			$data.="</table>";
			return $data;
		
	}
        
               	/**
	 * Consulta Estadistica 11 de digitalizacion
	 * @return   void
	 */
	function estadistica11($depe, $fechIni, $fechFin, $usua, $estado, $tipoRadicado) {
           // echo "$fechIni,$fechFin,$depe,$estado";
			$rs = $this->modelo->est11($depe, $fechIni, $fechFin, $usua, $estado, $tipoRadicado) ;
			if($rs['error']){
				return $rs['error'];
				
			}
                        //print_r($rs);
			$f=count($rs);		
                         $data.="<table>
					<trclass='tpar'>
		<td class='titulos3'>#</td><td class='titulos3'>Usuario</td><td class='titulos3'>Radicados</td><td class='titulos3'>Folios</td></tr>";
			for ($i = 0; $i < count($rs); $i++) {
				if($i%2){
					$style=" class='listado1'  ";
				}else{
					$style=" class='listado2' ";
				}
				$c=$i+1;
				//$data.="<tr $style><td>$c</td><td>".$rs [$i] ["usuario"]."</td><td ><div style=' text-align: center'><a href='#' onclick='detalles(". $rs [$i] ["ucodi"].");'>". $rs [$i] ["radicados"]."</a></div></td><td><span style=' text-align: center'>". $rs [$i] ["folios"]."</span></td></tr>";
                                $data.="<tr $style><td>$c</td><td>".$rs [$i] ["usuario"]."</td><td ><div style=' text-align: center'>". $rs [$i] ["radicados"]."</div></td><td><span style=' text-align: center'>". $rs [$i] ["folios"]."</span></td></tr>";
			} 
			$data.="</table>";

                        
			return $data;	
	}
	
         function detallesEst11($idusu,$tipoRadicado,$tipoDocumento) {
           // echo "$fechIni,$fechFin,$depe,$estado";
			$rs = $this->modelo->detallesEst11 ($idusu,$tipoRadicado,$tipoDocumento);
			if($rs['error']){
				return $rs['error'];
				
			}
                        //print_r($rs);
                          
			$f=count($rs);		
                         $data.="<table>
                             <trclass='tpar'>
		<td class='titulos3' colspan='10'>".$rs [0] ["usuario"]."</td></tr>
					<trclass='tpar'>
		<td class='titulos3'>#</td><td class='titulos3'>Radicados</td><td class='titulos3'>Asunto</td><td class='titulos3'>Fecha Radicacion</td>
               <td class='titulos3'>Tipo Documento</td><td class='titulos3'>Usuario anterior</td> </tr>";
			for ($i = 0; $i < count($rs); $i++) {
				if($i%2){
					$style=" class='listado1' ";
				}else{
					$style=" class='listado2' ";
				}
				$c=$i+1;
				$data.="<tr $style><td>$c</td><td>".$rs [$i] ["radicados"]."</td><td>". $rs [$i] ["asunto"]."</td><td>". $rs [$i] ["fech"]."</td>
                                    <td>". $rs [$i] ["tpdesc"]."</td><td>". $rs [$i] ["uante"]."</td></tr>";
			} 
			$data.="</table>";

                        
			return $data;	
	}
        
}

?>