<?php 
/**
 * Estadisticas es la clase encargada de gestionar las operaciones estadisticas
 * @author Hardy Deimont Niño  Velasquez
 * @name Estadisticas
 * @version	1.0
 */
if (!$ruta_raiz)
    $ruta_raiz = '../../../..';
include_once "$ruta_raiz/core/Modulos/estadistica/modelo/modeloEstadistica.php";

//==============================================================================================	
// CLASS tpDocumento
//==============================================================================================	

/**
 *  Objecto estadisticas 
 */
class estadisticas {

    private $modelo;
    private $ruta_raiz;

    function __construct($ruta_raiz) {
        $this->modelo = new modeloEstadisticas($ruta_raiz);
        $this->ruta_raiz = $ruta_raiz;
    }

    /**
     * Consulta los datos de las series 
     * @return   void
     */
    function consultar($subserie, $depe, $serieid, $ano) {
        $rs = $this->modelo->consultar($subserie, $depe, $serieid, $ano);
        if ($rs['error']) {
            return $rs['error'];
        }
        $f = count($rs);
        $data.="<table>
	<tr class=tpar>
		<td class=titulos3 colspan=5>Expedientes total $f</td>
	</tr>					
					<trclass='tpar'>
		<td class='titulos3' >#</td><td class='titulos3'>N&uacute;mero</td><td class='titulos3'>A&ntilde;o</td><td class='titulos3'>Titulo</td>
		<td class='titulos3'>Asunto</td></tr>";
        for ($i = 0; $i < count($rs); $i++) {
            if ($i % 2) {
                $style = " class='listado1' ";
            } else {
                $style = " class='listado2' ";
            }
            $c = $i + 1;
            $data.="<tr $style><td>$c</td><td>" . $rs [$i] ["numero"] . "</td><td>" . $rs [$i] ["year"] . "</td><td>" . $rs [$i] ["titulo"] . "</td>
		<td>{$rs [$i] ["asunto"]}</td></tr>";
        }
        $data.="</table>";

        return $data;
    }

    /**
     * Consulta los datos de las series 
     * @return   void
     */
    function estadistica5($fecha_ini, $fecha_fin, $depe, $estado) {
        // echo "$fechIni,$fechFin,$depe,$estado";
        $rs = $this->modelo->est5($fecha_ini, $fecha_fin, $depe, $estado);
        if ($rs['error']) {
            return $rs['error'];
        }
        //print_r($rs);
        $f = count($rs);
        $data.="<table>
					<trclass='tpar'>
		<td class='titulos3'>#</td><td class='titulos3'>Usuario</td><td class='titulos3'>Radicados</td></tr>";
        for ($i = 0; $i < count($rs); $i++) {
            if ($i % 2) {
                $style = " class='listado1' ";
            } else {
                $style = " class='listado2' ";
            }
            $c = $i + 1;
            $data.="<tr $style><td>$c</td><td>" . $rs [$i] ["usuario"] . "</td><td><a href='#' onclick='detalles(" . $rs [$i] ["ucodi"] . ",\"\");'>" . $rs [$i] ["radicados"] . "</a></td></tr>";
        }
        $data.="</table>";


        return $data;
    }

    function detallesEst5($fecha_ini, $fecha_fin, $idusu) {
        // echo "$fechIni,$fechFin,$depe,$estado";
        $rs = $this->modelo->detallesEst5($fecha_ini, $fecha_fin, $idusu);
        if ($rs['error']) {
            return $rs['error'];
        }
        //print_r($rs);

        $f = count($rs);
        $data.="<table>
                             <trclass='tpar'>
		<td class='titulos3' colspan='10'>" . $rs [0] ["usuario"] . "</td></tr>
					<trclass='tpar'>
		<td class='titulos3'>#</td><td class='titulos3'>Radicados</td><td class='titulos3'>Asunto</td><td class='titulos3'>Fecha Radicacion</td></tr>";
        for ($i = 0; $i < count($rs); $i++) {
            if ($i % 2) {
                $style = " class='listado1' ";
            } else {
                $style = " class='listado2' ";
            }
            $c = $i + 1;
            $data.="<tr $style><td>$c</td><td><a href='#' onclick='verDocumento(\"../../../../\",{$rs [$i] ["radicados"]})'>" . $rs [$i] ["radicados"] . "</a></td><td>" . $rs [$i] ["asunto"] . "</td><td>" . $rs [$i] ["fech"] . "</td></tr>";
        }
        $data.="</table>";


        return $data;
    }

    /**
     * Consulta los datos de las series 
     * @return   void
     */
    function estadistica6($depe, $usuario, $estado, $tipoRadicado, $tipoDocumento) {
        //echo "clase $depe,$usuario,$estado,$tipoRadicado, tipodoc $tipoDocumento <br>";
        $rs = $this->modelo->est6($depe, $usuario, $estado, $tipoRadicado, $tipoDocumento);
        if ($rs['error']) {
            return $rs['error'];
        }

        //print_r($rs);
        $f = count($rs);
        $data.="<table>
					<trclass='tpar'>
		<td class='titulos3'>#</td><td class='titulos3'>Usuario</td><td class='titulos3'>Radicados</td></tr>";
        for ($i = 0; $i < count($rs); $i++) {
            if ($i % 2) {
                $style = " class='listado1' ";
            } else {
                $style = " class='listado2' ";
            }
            $c = $i + 1;
            $data.="<tr $style><td>$c</td><td>" . $rs [$i] ["usuario"] . "</td><td><a href='#' onclick='detalles(" . $rs [$i] ["ucodi"] . ",\"\");'>" . $rs [$i] ["radicados"] . "</a></td></tr>";
        }
        $data.="</table>";


        return $data;
    }

    function detallesEst6($idusu, $tipoRadicado, $tipoDocumento) {
        // echo "$fechIni,$fechFin,$depe,$estado";
        $rs = $this->modelo->detallesEst6($idusu, $tipoRadicado, $tipoDocumento);
        if ($rs['error']) {
            return $rs['error'];
        }
        //print_r($rs);

        $f = count($rs);
        $data.="<table>
                             <trclass='tpar'>
		<td class='titulos3' colspan='10'>" . $rs [0] ["usuario"] . "</td></tr>
					<trclass='tpar'>
		<td class='titulos3'>#</td><td class='titulos3'>Radicados</td><td class='titulos3'>Asunto</td><td class='titulos3'>Fecha Radicacion</td>
               <td class='titulos3'>Tipo Documento</td><td class='titulos3'>Usuario anterior</td> </tr>";
        for ($i = 0; $i < count($rs); $i++) {
            if ($i % 2) {
                $style = " class='listado1' ";
            } else {
                $style = " class='listado2' ";
            }
            $c = $i + 1;
            $data.="<tr $style><td>$c</td><td><a href='#' onclick='verDocumento(\"../../../../\",{$rs [$i] ["radicados"]})'>" . $rs [$i] ["radicados"] . "</a></td><td>" . $rs [$i] ["asunto"] . "</td><td>" . $rs [$i] ["fech"] . "</td>
                                    <td>" . $rs [$i] ["tpdesc"] . "</td><td>" . $rs [$i] ["uante"] . "</td></tr>";
        }
        $data.="</table>";


        return $data;
    }

    function __destruct() {
        
    }

    /**
     * Consulta los datos de las series 
     * @return   void
     */
    function estadistica1($depe, $fecha_ini, $fecha_fin, $usuario, $estado, $tipoRadicado, $tipoDocumento,$Inac) {
        // echo "$fechIni,$fechFin,$depe,$estado";
        $rs = $this->modelo->est1($depe, $fecha_ini, $fecha_fin, $usuario, $estado, $tipoRadicado, $tipoDocumento,$Inac);
        if ($rs['error']) {
            return $rs['error'];
        }
        //print_r($rs);
        $f = count($rs);
        $data.="<table>
            <tr><td class=\"cosa\" colspan=\"4\">
      RADICACION POR USUARIO
    </td></tr>
					<trclass='tpar'>
		<td class='titulos3'>#</td><td class='titulos3'>Usuario</td>
                <td class='titulos3'>Radicados</td><td class='titulos3'>Estado U</td></tr>";
        for ($i = 0; $i < count($rs); $i++) {
            if ($i % 2) {
                $style = " class='listado1' ";
            } else {
                $style = " class='listado2' ";
            }
            $c = $i + 1;
            $estausua = $rs [$i] ["estado"];
            if ($estausua == 1) {
                $estausua = "ACTIVO";
            } else {
                $estausua = "INACTIVO";
            }
            $data.="<tr $style><td>$c</td><td style=\"white-space: pre-line\">" . $rs [$i] ["usuario"] . "</td><td><a href='#' onclick='detalles(" . $rs [$i] ["ucodi"] . ",\"{$rs [$i] ["usuario"]}\");'>" . $rs [$i] ["radicados"] . "</a></td>
				<td>" . $estausua . "</td></tr>";
        }
        $data.="</table>";


        return $data;
    }

    function detallesEst1($fechIni, $fechFin, $idusua, $tipoRadicado, $tipoDocumento, $radicador,$depe,$Inac) {
        // echo "$fechIni,$fechFin,$depe,$estado";
        $rs = $this->modelo->detallesEst1($fechIni, $fechFin, $idusua, $tipoRadicado, $tipoDocumento,$depe,$Inac);
        if ($rs['error']) {
            return $rs['error'];
        }
        //print_r($rs);

        $f = count($rs);
        $data.="<table style='width:95%'>
            <tr><td class=\"cosa\" colspan=\"10\">
      RADICACION - CONSULTA DE RADICADOS POR USUARIO
    </td></tr>
                             <trclass='tpar'>
		<td class='titulos3' colspan='10'>$radicador</td></tr>
					<trclass='tpar'>
		<td class='titulos3'>#</td><td class='titulos3'>Radicados</td>
                <td class='titulos3' style='width: 200px'>Asunto</td>
                <td class='titulos3'>Usuario Actual</td>
                <td class='titulos3'>Dependencia Actual</td>
                <td class='titulos3'>Fecha Radicacion</td>
               <td class='titulos3'>Tipo Documento</td>
               <td class='titulos3'>Destinatario</td></tr>";
        for ($i = 0; $i < count($rs); $i++) {
            if ($i % 2) {
                $style = " class='listado1' ";
            } else {
                $style = " class='listado2' ";
            }
            $c = $i + 1;
            $data.="<tr $style><td>$c</td><td><a href='#' onclick='verDocumento(\"../../../../\",{$rs [$i] ["radicados"]})'>" . $rs [$i] ["radicados"] . "</a></td>
                <td style=\"white-space: pre-line\"><label alt='" . $rs [$i] ["asunto"] . "'>" . substr($rs [$i] ["asunto"], 0, 100) . "</label></td><td style=\"white-space: pre-line\">" . $rs [$i] ["usuario"] . "</td><td style=\"white-space: pre-line\">" . $rs [$i] ["dactu"] . "</td><td>" . $rs [$i] ["fech"] . "</td>
                <td style=\"white-space: pre-line\">" . $rs [$i] ["tpdesc"] . "</td><td style=\"white-space: pre-line\">" . $rs [$i] ["dest"] . "</td></tr>";
        }
        $data.="</table>";
        return $data;
    }

    function estadistica2($depe, $fecha_ini, $fecha_fin, $usuario, $estado, $tipoRadicado, $tipoDocumento,$Inac) {
        // echo "$fechIni,$fechFin,$depe,$estado";

        $rs = $this->modelo->est2($depe, $fecha_ini, $fecha_fin, $usuario, $estado, $tipoRadicado, $tipoDocumento,$Inac);
        if ($rs['error']) {
            return $rs['error'];
        }
        //print_r($rs);
        $f = count($rs);
        $data.="<table>
					<trclass='tpar'>
		<td class='titulos3'>#</td><td class='titulos3'>Medio De Racepción</td><td class='titulos3'>Radicados</td></tr>";
        for ($i = 0; $i < count($rs); $i++) {
            if ($i % 2) {
                $style = " class='listado1' ";
            } else {
                $style = " class='listado2' ";
            }
            $c = $i + 1;
            $data.="<tr $style><td>$c</td><td>" . $rs [$i] ["usuario"] . "</td><td><a href='#' onclick='detalles(" . $rs [$i] ["ucodi"] . ",\"\");'>" . $rs [$i] ["radicados"] . "</a></td></tr>";
        }
        $data.="</table>";


        return $data;
    }

    function detallesEst2($fechIni, $fechFin, $idusua, $tipoRadicado, $tipoDocumento, $tpEnv, $depe, $estado) {
        // echo "$fechIni,$fechFin,$depe,$estado";
        $rs = $this->modelo->detallesEst2($fechIni, $fechFin, $idusua, $tipoRadicado, $tipoDocumento, $tpEnv, $depe, $estado);
        if ($rs['error']) {
            return $rs['error'];
        }
        //print_r($rs);

        $f = count($rs);
        $data.="<table>
                             <trclass='tpar'>
		<td class='titulos3' colspan='10'>" . $rs [0] ["usuario"] . "</td></tr>
					<trclass='tpar'>
		<td class='titulos3'>#</td><td class='titulos3'>Radicados</td><td class='titulos3'>Asunto</td><td class='titulos3'>Radicador</td><td class='titulos3'>Fecha Radicacion</td>
               <td class='titulos3'>Tipo Documento</td><td class='titulos3'>Medio Recepcion</td></tr>";
        for ($i = 0; $i < count($rs); $i++) {
            if ($i % 2) {
                $style = " class='listado1' ";
            } else {
                $style = " class='listado2' ";
            }
            $c = $i + 1;
            $data.="<tr $style><td>$c</td><td><a href='#' onclick='verDocumento(\"../../../../\",{$rs [$i] ["radicados"]})'>" . $rs [$i] ["radicados"] . "</a></td><td>" . $rs [$i] ["asunto"] . "</td><td>" . $rs [$i] ["usuario"] . "</td><td>" . $rs [$i] ["fech"] . "</td>
                                    <td>" . $rs [$i] ["tpdesc"] . "</td><td>" . $rs [$i] ["mrec"] . "</td></tr>";
        }
        $data.="</table>";
        return $data;
    }

    /*     * ***************************************************
     * *													**
     * *	Consulta para la estadistica numero 3			** 
     * *	ESTADISTICAS DE MEDIO ENVIO FINAL DE DOCUMENTOS	**
     * *													**
     * *************************************************** */

    function estadistica3($depe, $fecha_ini, $fecha_fin,$Inac) {

        $rs = $this->modelo->est3($depe, $fecha_ini, $fecha_fin,$Inac);
        if ($rs['error']) {
            return $rs['error'];
        }
        //print_r($rs);
        $f = count($rs);
        $data.="<table>
						 <tr><td colspan='3'class='cosa'>MEDIO DE ENVIO FINAL DE DOCUMENTOS</td></tr>
					<trclass='tpar'>
		<td class='titulos3'>#</td><td class='titulos3'>Medio De Racepción</td><td class='titulos3'>Radicados</td></tr>";
        for ($i = 0; $i < count($rs); $i++) {
            if ($i % 2) {
                $style = " class='listado1' ";
            } else {
                $style = " class='listado2' ";
            }
            $c = $i + 1;
            $data.="<tr $style><td>$c</td><td>" . $rs [$i] ["nombremed"] . "</td><td><a href='#' onclick='detallesEst3(\"" . $rs [$i] ["nombremed"] . "\"," . $rs [$i] ["idmedio"] . ");'>" . $rs [$i] ["radicados"] . "</a></td></tr>";
        }
        $data.="</table>";


        return $data;
    }

    /*     * ***************************************************
     * *													**
     * *	Detalles para la estadistica numero 3			** 
     * *	ESTADISTICAS DE MEDIO ENVIO FINAL DE DOCUMENTOS	**
     * *													**
     * *************************************************** */

    function detallesEst3($depe, $fecha_ini, $fecha_fin, $nombremed, $idmedio) {

        $rs = $this->modelo->detallesEst3($depe, $fecha_ini, $fecha_fin, $idmedio);
        if ($rs['error']) {
            return $rs['error'];
        }
        //print_r($rs);
        $f = count($rs);
        $fechainf = date("Y:m:d H:m:s");
        $data.="<table><tr class='tpar'><td class='titulos3' colspan='9'>ESTADISTICAS DE MEDIO DE ENVIO FINAL DE DOCUMENTOS</td></tr>
                            <trclass='tpar'>
								<td class='titulos3' colspan='10'>
									<table>
										<tr class='tpar'>
											<td class='titulos3' colspan='2'>Medio de Envio: " . $nombremed . "</td>
											<td class='titulos3'>Fecha Generacion del Informe: " . $fechainf . "</td>
										</tr>
									</table>
								</td>
							</tr>
					<trclass='tpar'>
		<td class='titulos3'>#</td><td class='titulos3'>Numero de Radicado</td><td class='titulos3'>Fecha de Radicación</td><td class='titulos3'>Destinatario</td><td class='titulos3'>Dirección</td>
               <td class='titulos3'>Numero de <br>Planilla</td><td class='titulos3'>Observacion</td></tr>";
        for ($i = 0; $i < count($rs); $i++) {
            if ($i % 2) {
                $style = " class='listado1' ";
            } else {
                $style = " class='listado2' ";
            }
            $c = $i + 1;
            $data.="<tr $style><td>$c</td><td><a href='#' onclick='verDocumento(\"../../../../\",{$rs [$i] ["radicado"]})'>" . $rs [$i] ["radicado"] . "</a></td><td>" . $rs [$i] ["rfecha"] . "</td><td>" . $rs [$i] ["rnombre"] . "</td>
				<td>" . $rs [$i] ["rdireccion"] . "<br>" . $rs [$i] ["rdepartamento"] . " " . $rs [$i] ["rmunicipio"] . "</td>
                                    <td>" . $rs [$i] ["planilla"] . "</td><td>" . $rs [$i] ["sgd_renv_observa"] . "</td></tr>";
        }
        $data.="</table>";


        return $data;
    }

    /*     * *******************************************
     * *											**
     * *	Consulta para la estadistica numero 10	** 
     * *	RADICADOS POR BANDEJA					**
     * *											**
     * ******************************************* */

    function estadistica10($depe, $usuario, $estado, $tipoDocumento, $fecha_ini, $fecha_fin,$Inac) {
        $rs = $this->modelo->est10($depe, $usuario, $estado, $tipoDocumento, $fecha_ini, $fecha_fin,$Inac);
        if ($rs['error']) {
            return $rs['error'];
        }
        $carpetas = $rs['carp'];
        $personales = $rs['per'];
        $datauser = $rs['usuario'];

        $f = count($datauser);
        $data.="<table id='est10' >
						 <tr><td colspan='3'class='cosa'>RADICADOS POR BANDEJAS</td></tr>";
        $detalles = "<trclass='tpar'><td class='titulos3'>#</td><td class='titulos3' colspan='2'>DETALLES</td></tr>";
        $dependenciaN = '';
        $c = 1;
        $tablex = '<table style="width: 80%">';
        $tablex2 = '</table>';
        $rscarpddper = '';
        for ($a = 0; $a < $f; $a++) {
            if ($a % 2) {
                $style = " class='listado1' style='height: 10px;' ";
            } else {
                $style = " class='listado2e' style='height: 10px;'";
            }
            if ($dependenciaN != $datauser[$a]["depenomb"]) {
                $dependenciaN = $datauser[$a]["depenomb"];
                $depeCodi = $datauser[$a]["depecodi"];
                $data.="<tr class='tpar'><td class='titulos2' colspan='3'>$dependenciaN</td></tr> $detalles";
            }
            //if($usuarioN!=$datauser[$a]["usuacodi"])       {
            $usuarioN = $datauser[$a]["usuacodi"];
            $data.="<tr class='tpar'><td rowspan='3' $style><span class='menu_princ'>$c</span></td><td $style colspan='2'><span class='menu_princ'>{$datauser[$a]["usuanomb"]}</span></td></tr>
                                    <tr $style><td class='tdest10' style='font-weight: bolder;'>Predeterminadas</td><td class='tdest10' style='font-weight: bolder;'>Personales</td></tr>";
            $c++;
            // }
            $resultadocarpetas = "";
            $carpActuuser = $carpetas[$usuarioN];

            for ($icarp = 0; $icarp <= count($carpActuuser); $icarp++) {
                $resultadocarpetas.= "<tr><td style='width: 5px'></td><td style='width: 30px'> "
                        . $carpActuuser[$icarp] ["namecarp"] . "</td><td style=' text-align:right'> 
                         <a href='#' onclick='detallesbandejas(" . $usuarioN . "," . $carpActuuser[$icarp] ["carpcodi"] . ",$depeCodi,\"{$datauser[$a]["usuanomb"]}\",\"{$carpActuuser[$icarp] ["namecarp"]}\",0);'>"
                        . $carpActuuser[$icarp] ["totalcarp"] . "</a></td></tr>";
                //<a href='#' onclick='detallesbandejas(".$usuarioN.",".$carpActuuser[$icarp] ["carpcodi"].",".$rs [$i] ["dependencia"].",\"".$rs [$i] ["usuario"]."\",".$norcar.");'>".$carpActuuser[$icarp] ["totalcarp"]."</a>";
            }
            $rscarpdd = $tablex . $resultadocarpetas . $tablex2;
            $perActuuser = $personales[$usuarioN];
            //echo count($perActuuser);
            $resultadoper = '';
            for ($icarper = 0; $icarper < count($perActuuser); $icarper++) {
                $resultadoper.= "<tr><td style='width: 5px'></td><td style='width: 30px'>" . $perActuuser[$icarper] ["namecarp"] . "</td>
                    <td style=' text-align:right'>
                             <a href='#' onclick='detallesbandejas(" . $usuarioN . "," . $perActuuser[$icarper] ["carpper"] . ",$depeCodi,\"{$datauser[$a]["usuanomb"]}\",\"{$perActuuser[$icarper] ["namecarp"]}\",1);'>"
                        . $perActuuser[$icarper] ["totalcarp"] . "</a></td></tr>";
            }
            $rscarpddper = $tablex . $resultadoper . $tablex2;
            $data.="<tr $style><td class='est10tit'>" . $rscarpdd . "</td>
				<td class='est10tit'>" . $rscarpddper . "</td></tr>";
        }
        $data.="</table>";
        return $data;
    }

    /*     * *******************************************
     * *											**
     * *	Detalles para la estadistica numero 10	** 
     * *	RADICADOS POR BANDEJA					**
     * *											**
     * ******************************************* */

    function detallesEst10($idusuaban, $dependencia_ban, $tipocarpetaban, $userban, $carpetanumero, $tipoDocumento, $fecha_ini, $fecha_fin, $nomcarp) {
        //echo "$idusuaban,$dependencia_ban,$tipocarpetaban,$userban,$carpetanumero<br>";
        $rs = $this->modelo->detallesEst10($idusuaban, $dependencia_ban, $tipocarpetaban, $carpetanumero, $tipoDocumento, $fecha_ini, $fecha_fin);
        if ($rs['error']) {
            return $rs['error'];
        }
        //print_r($rs);

        $f = count($rs);
        $fechainf = date("Y:m:d H:m:s");
        //$fechainf = date("Y:m:d );
        $data.="<table style='width='90%'><tr class='tpar'><td class='cosa' colspan='10'>ESTADISTICA RADICADOS POR BANDEJA</td></tr>
                            <trclass='tpar'>
								<td class='titulos3' colspan='10'>
									<table>
										<tr class='tpar'>
											<td class='titulos3'>Dependencia: " . $dependencia_ban . "</td>
											<td class='titulos3'>Usuario: " . $userban . "</td>
											<td class='titulos3'>Bandeja: " . $nomcarp . "</td>
											<td class='titulos3'>Fecha de Generación: " . $fechainf . "</td>
										</tr>
									</table>
								</td>
							</tr>
					<trclass='tpar'>
		<td class='titulos3'>#</td><td class='titulos3'>Radicados</td><td class='titulos3'>Asunto</td><td class='titulos3'>Remitente</td><td class='titulos3'>Fecha Radicacion</td>
               <td class='titulos3'>Tipo Documento</td><td class='titulos3' style='white-space: pre-line'>Dias calendario Restantes</td><td class='titulos3'>Enviado por</td></tr>";
        for ($i = 0; $i < count($rs); $i++) {
            if ($i % 2) {
                $style = " class='listado1' ";
            } else {
                $style = " class='listado2' ";
            }
            $c = $i + 1;
            $data.="<tr $style><td><span class='leidos'>$c</span></td><td><span class='leidos'><a href='#' onclick='verDocumento(\"../../../../\",{$rs [$i] ["radicados"]})'>" . $rs [$i] ["radicados"] . "</a></span></td><td style='white-space: pre-line'><span class='leidos'>" . $rs [$i] ["asunto"] . "</span></td><td style='white-space: pre-line'><span class='leidos'>" . $rs [$i] ["remitente"] . "</span></td><td><span class='leidos'>" . $rs [$i] ["fecha_radicado"] . "</span></td>
                    <td><span class='leidos'>" . $rs [$i] ["tipo_documento"] . "</td><td ><span class='leidos'>" . $rs [$i] ["dias_restantes"] . "</td><td><span class='leidos'>" . $rs [$i] ["usuante"] . "</span></td></tr>";
        }
        $data.="</table>";
        return $data;
    }

    /**
     * Consulta Estadistica 11 de digitalizacion
     * @return   void
     */
    function estadistica11($depe, $fechIni, $fechFin, $usua, $estado, $tipoRadicado, $tipoDocumento) {

        $rs = $this->modelo->est11($depe, $fechIni, $fechFin, $usua, $estado, $tipoRadicado, $tipoDocumento);

        if ($rs['error']) {
            return $rs['error'];
        }
        //print_r($rs);
        $f = count($rs);
        $data.="<table>
		<tr class='tpar'>
		<td class='titulos3' rowspan='2'>#</td><td rowspan='2' class='titulos3'>Usuario</td>
                <td class='titulos3' rowspan='2'>Rad.</td>
                <td class='titulos3'  colspan='3'>folios</td>
                </tr><tr class='tpar'>
                <td class='titulos3'>Rad.</td>
                <td class='titulos3'>Anex.</td>
                <td class='titulos3'>Total.</td></tr>";
        $total2=0;
        $totalR=0;
        $totalA=0;
        $totalR=0;
        for ($i = 0; $i < count($rs); $i++) {
            if ($i % 2) {
                $style = " class='listado1'  ";
            } else {
                $style = " class='listado2' ";
            }
            $c = $i + 1;
            $total=$rs [$i] ["anexos"]+ $rs [$i] ["folios"];
            $total2=$total+$total2;
             $totalR=$totalR+$rs [$i] ["folios"];
             $totalA=$totalA+$rs [$i] ["anexos"];
             $totalRR=$totalRR+$rs [$i] ["radicados"];
            $data.="<tr $style><td>$c</td><td style=' white-space: pre-line'>" . $rs [$i] ["usuario"] . "</td>"
                    . "<td text-align: right' ><div ><a href='#' onclick='detalles(" . $rs [$i] ["ucodi"] . "," . $rs [$i] ["depe"] . ");'>" . $rs [$i] ["radicados"] . "</a></div></td>"
                    . "<td text-align: right'>" . $rs [$i] ["folios"] . "</td>"
                    . "<td text-align: right'>" . $rs [$i] ["anexos"] . "</td>"
                    . "<td text-align: right'>" . $total . "</td></tr>";
            //$data.="<tr $style><td>$c</td><td>" . $rs [$i] ["usuario"] . "</td><td ><div style=' text-align: center'>" . $rs [$i] ["radicados"] . "</div></td><td><span style=' text-align: center'>" . $rs [$i] ["folios"] . "</span></td></tr>";
        }
        $data.="</table>";
$cabezote='<table style="width: 100%"><tr><td class="titulos4" >ESTADISTICAS DE DIGITALIZACION </td></TR>
                     <tr><td><table>  <tr class="titulo1" >
                           <td  style=" white-space: pre-line">Rango de fechas: </td>
                           <td style=" white-space: pre-line" >Fecha Generacion</td>
                           <td>Radicados</td>
                            </tr>
                            <tr class="listado2">
                        	<td style=" white-space: pre-line">Desde <br>'.$fechIni.' <br>Hasta <br>'.$fechFin.' </td>
				<td style=" white-space: pre-line"> '.date('d/m/Y h:i:s').'</td>
                                     <TD style="text-align:right"> '.$totalRR.'</td>
				</tr>
                                <tr class="titulo1">
                                <td>Rad. imag. Dig.</td>
                                <td>Anex. Imag. Dig.</td>
                                 <td>Total</td>
                              </tr>    
                              <tr class="listado2">
                             
                                  <TD style="text-align:right">  '.$totalR.'  </td><td style="text-align:right">  '.$totalA.'</td>
                                    <TD style="text-align:right"> '.$total.'</td>
				</tr>
<table></td></tr>
			</table>';

        return $cabezote.$data;
    }

    function detallesEst11($fechIni, $fechFin, $idusu, $tipoRadicado, $depe) {

        $rs = $this->modelo->detallesEst11($fechIni, $fechFin, $idusu, $tipoRadicado, $depe);

        if ($rs['error']) {
            return $rs['error'];
        }
//print_r($rs);
        $f = count($rs);
        $data.="<table>
            <trclass='tpar'>
		<td class='titulos3' rowspan='2'>#</td>
                <td class='titulos3' rowspan='2'>Radicados</td>
                <td class='titulos3' rowspan='2'>Fecha Radicaci&oacute;n</td>
                <td class='titulos3' rowspan='2'>Asunto</td>
                <td class='titulos3' rowspan='2'>Fecha Digitalizaci&oacute;n</td>
                <td class='titulos3' colspan='3'>Digitalizaci&oacute;n</td></tr>
                <tr>
               <td class='titulos3'>Radicado</td>
               <td class='titulos3'>Anexos.</td>
               <td class='titulos3'>Total</td> </tr>";
        $total=0;
        $totalA=0;
        $totalR=0;
        $redi=0;
        for ($i = 0; $i < count($rs); $i++) {
            if ($i % 2) {
                $style = " class='listado1' ";
            } else {
                $style = " class='listado2' ";
            }
            if($rs [$i] ["radicados"]==$rs [$i-1] ["radicados"]){
                $style = " style='font-family: Arial, Helvetica, sans-serif;
                                 font-size: 10px;font-style: normal;font-weight: bolder;text-transform: none;color: #000000;text-decoration: none;
                                background-color: #F7FD67;text-align: left;vertical-align: middle;height: 30px;' ";
                $redi++;
            }
            $c = $i + 1;
            $anxnum=0;
            if($rs [$i] ["ahdigitalizadas"]){
                $anxnum=$rs [$i] ["ahdigitalizadas"];
            }
            
            $rema=$rs [$i] ["hdigitalizadas"]+$anxnum;
            $total=$total+$rema;
            $totalA=$anxnum+$totalA;
            $totalR=$rs [$i] ["hdigitalizadas"]+$totalR;
            $data.="<tr $style><td>$c</td><td><a href='#' onclick='verDocumento(\"../../../../\",{$rs [$i] ["radicados"]})'>" . $rs [$i] ["radicados"] . "</a></td>"
                    . "<td>" . $rs [$i] ["fechRad"] . "</td><td style=' white-space: pre-line'>" . $rs [$i] ["asunto"] . "</td>"
                    . "<td>" . $rs [$i] ["fech"] . "</td>
                                    <td>" . $rs [$i] ["hdigitalizadas"] . "</td><td>" . $anxnum . "</td><td>" . $rema . "</td></tr>";
        }
        $data.="</table>";
        $ccc=$c-$redi;
$cabezote='<table style="width: 100%"><tr><td class="titulos4" colspan="10">ESTADISTICAS DE DIGITALIZACION ('.$rs [0] ["usuario"].')</td></TR>
                     <tr><td><table>  <tr class="titulo1">
                           <td >Rango de fechas: </td>
                           <td >Fecha Generacion</td>
                           <td >Cantidad de Radicados</td>
                           <td >redigitalizados</td>
                           <td >Cantidad Radicados</td>
                           <td>Digitalizacion </td>
                           <td>Total</td>
                            </tr>
                            <tr class="listado2">
                        	<td >Desde '.$fechIni.' <br>Hasta '.$fechFin.' </td>
				<td > '.date('d/m/Y h:i:s').'</td>
                                    <td > '.$c.'</td>
                                    <td > '.$redi.'</td>
                                    <td > '.$ccc.'</td>    
                                    <TD> Rad img: '.$totalR.'  <br> Anexos: '.$totalA.'</td>
                                    <TD> '.$total.'</td>
				</tr><table></td></tr>
			</table>';

        return $cabezote.$data;
    }

    /**
     * Consulta Estadistica 15 RADICADOS DE ENTRADA RECIBIDOS DEL AREA DE CORRESPONDENCIA (DEPENDENCIA)
     * 
     */
    function estadistica15($depe, $fechIni, $fechFin, $tipoDocumento, $tipoRadicado,$Inac) {
        //print "$depe, $fechIni, $fechFin, $tipoRadicado <br>";

        $rs = $this->modelo->est15($depe, $fechIni, $fechFin, $tipoDocumento, $tipoRadicado,$Inac);

        if ($rs['error']) {
            return $rs['error'];
        }
        //print_r ($rs);
        $f = count($rs);
        $data.="<table>
					<trclass='tpar'>
		<td class='titulos3'>#</td><td class='titulos3'>Dependencia</td><td class='titulos3'>Radicados</td></tr>";
        for ($i = 0; $i < count($rs); $i++) {
            if ($i % 2) {
                $style = " class='listado1'  ";
            } else {
                $style = " class='listado2' ";
            }
            $c = $i + 1;
            $data.="<tr $style><td>$c</td><td>" . $rs [$i] ["dependencia"] . "</td><td ><div style=' text-align: center'><a href='#' onclick='detalleseq(" . $rs [$i] ['codepe'] . ");'>" . $rs [$i] ["radicados"] . "</a></div></td></tr>";
        }
        $data.="</table>";


        return $data;
    }

    /**
     * Detalles Estadistica 15 RADICADOS DE ENTRADA RECIBIDOS DEL AREA DE CORRESPONDENCIA (DEPENDENCIA)
     * 
     */
    function detallesEst15($depe, $tipoDocumento, $fechIni, $fechFin, $tipoRadicado) {
        //print "$depe,$tipoDocumento, $fechIni, $fechFin <br>";

        $rs = $this->modelo->detallesEst15($depe, $tipoDocumento, $fechIni, $fechFin, $tipoRadicado);

        if ($rs['error']) {
            return $rs['error'];
        }
        //print_r($rs);

        $f = count($rs);
        $fechainf = date("Y:m:d H:m:s");
        $data.="<table><tr class='tpar'><td class='titulos4' colspan='10'>RADICADOS DE ENTRADA RECIBIDOS DEL AREA DE CORRESPONDENCIA (DEPENDENCIA)</td></tr>
                            <trclass='tpar'>
								<td class='titulos3' colspan='6'>
									<table>
										<tr class='tpar'>
											<td class='titulos3'>Dependencia: </td>
											<td class='titulos3'> " . $rs [0] ["dependencia"] . " ($depe)</td>
											<td class='titulos3'>Rango de fechas: <br>Desde $fechIni <br>Hasta $fechFin </td>
											<td class='titulos3'>Fecha Generacion del Informe: " . $fechainf . "</td>
										</tr>
									</table>
								</td>
							</tr>
					<trclass='tpar'>
		<td class='titulos3'>#</td><td class='titulos3'>Radicados</td><td class='titulos3'>Fecha</td><td class='titulos3'>Asunto</td><td class='titulos3'>Descripcion Anexos</td><td class='titulos3'>Remintente</td></tr>";
        for ($i = 0; $i < count($rs); $i++) {
            if ($i % 2) {
                $style = " class='listado1' ";
            } else {
                $style = " class='listado2' ";
            }
            $c = $i + 1;
            $data.="<tr $style><td>$c</td><td><a href='#' onclick='verDocumento(\"../../../../\",{$rs [$i] ["radicados"]})'>" . $rs [$i] ["radicados"] . "</a></td>"
                    . "<td>" . $rs [$i] ["fecha"] . "</td><td>" . $rs [$i] ["asunto"] . "</td>"
                    . "<td>" . $rs [$i] ["anexos"] . "</td><td>" . $rs [$i] ["rem"] . "</td></tr>";
        }
        $data.="</table>";
        return $data;
    }

    /**
     * Consulta Estadistica 17 ESTADISTICA POR RADICADOS Y SUS RESPUESTAS
     * 
     */
    function estadistica17($depe, $fechIni, $fechFin, $usua, $estado, $tipoRadicado, $tipoDocumento) {
        //print "$depe, $fechIni, $fechFin, $usua, $estado, $tipoRadicado, $tipoDocumento <br>";

        $rs = $this->modelo->est17($depe, $fechIni, $fechFin, $usua, $estado, $tipoRadicado, $tipoDocumento);
        if ($rs['error']) {
            return $rs['error'];
        }

        //print_r ($rs);
        $f = count($rs);
        $data.="<table>
					<trclass='tpar'>
		<td class='titulos3'>#</td><td class='titulos3'>Usuario</td><td class='titulos3'>Radicados</td><td class='titulos3'>Tramitados</td></tr>";
        for ($i = 0; $i < count($rs); $i++) {
            if ($i % 2) {
                $style = " class='listado1'  ";
            } else {
                $style = " class='listado2' ";
            }
            $c = $i + 1;
            $data.="<tr $style><td>$c</td><td>" . $rs [$i] ["usuario"] . "</td><td ><div style=' text-align: center'><a href='#' onclick='detallesedst(" . $rs [$i] ['usuacodi'] . ");'>" . $rs [$i] ["radicados"] . "</a></div></td><td>" . $rs [$i] ["tramitados"] . "</td></tr>";
        }
        $data.="</table>";

        return $data;
    }

    /**
     * Detalles Estadistica 17 ESTADISTICA POR RADICADOS Y SUS RESPUESTAS
     * 
     */
    function detallesEst17($depe, $fechIni, $fechFin, $usua, $tipoRadicado, $tipoDocumento) {
        //print "$depe, $fechIni, $fechFin, $usua, $tipoRadicado, $tipoDocumento <br>";

        $rs = $this->modelo->detallesEst17($depe, $fechIni, $fechFin, $usua, $tipoRadicado, $tipoDocumento);

        if ($rs['error']) {
            return $rs['error'];
        }
        //print_r($rs);

        $f = count($rs);
        $fechainf = date("Y:m:d H:m:s");
        $data.="<table style='width:100%'><tr class='tpar'><td class='titulos3' colspan='14'>ESTADISTICA POR RADICADOS Y SUS RESPUESTAS</td></tr>
                            <trclass='tpar'>
								<td class='titulos3' colspan='14'>
									<table>
										<tr class='tpar'>
											<td class='titulos3'>Dependencia: </td>
											<td class='titulos3'> $depe</td>
											<td class='titulos3'>Fechas:</td>
											<td class='titulos3'>Desde $fechIni Hasta $fechFin </td>
											<td class='titulos3'>Generación del Informe: " . $fechainf . "</td>
										</tr>
									</table>
								</td>
							</tr>
							<trclass='tpar'>
								<td class='titulos3'>#</td>
								<td class='titulos3'>Radicado</td>
								<td class='titulos3'>Usuario Radicador</td>
                                                                <td class='titulos3'>Usuario Actual</td>
								<td class='titulos3'>Asunto</td>
								<td class='titulos3' style='white-space: pre-line'>Fecha Radicacion</td>
								<td class='titulos3' style='white-space: pre-line'>Radicado de Respuesta</td>
                                                                <td class='titulos3' style='white-space: pre-line'>Asociado o Anexo</td>
								<td class='titulos3' style='white-space: pre-line'>Fecha de Respuesta</td>
                                                                
								<td class='titulos3' style='white-space: pre-line'>Fecha del Envio</td>
								<td class='titulos3' style='white-space: pre-line'>Termino en dias</td>
								<td class='titulos3' style='white-space: pre-line'>Tipo documental</td>
								<td class='titulos3' style='white-space: pre-line'>Remitente / Destinatario</td>
								<td class='titulos3' style='white-space: pre-line'>Ciudad</td>
								<td class='titulos3' style='white-space: pre-line'>Quien responde</td>
							</tr>";

        for ($i = 0; $i < count($rs); $i++) {
            if ($i % 2) {
                $style = " class='listado1' ";
            } else {
                $style = " class='listado2' ";
            }
            $c = $i + 1;
            $data.="<tr $style>
							<td>$c</td>
							<td><a href='#' onclick='verDocumento(\"../../../../\",{$rs [$i] ["radicado"]})'>" . $rs [$i] ["radicado"] . "</a></td>
							<td style='white-space: pre-line'>" . $rs [$i] ["usuario"] . "</td>
                                                        <td style='white-space: pre-line'>" . $rs [$i] ["usua_actu"] . "</td>
							<td style='white-space: pre-line'>" . $rs [$i] ["asunto"] . "</td>
							<td style='white-space: pre-line'>" . $rs [$i] ["fecha_radicacion"] . "</td>
							<td style='white-space: pre-line'>" . $rs [$i] ["radi_nume_salida"] . "</td>
                                                            <td style='white-space: pre-line'>" . $rs [$i] ["radi_nume_deri"] . "</td>
							<td style='white-space: pre-line'>" . $rs [$i] ["anex_radi_fech"] . "</td>
							<td style='white-space: pre-line'>" . $rs [$i] ["anex_fech_envio"] . "</td>
							<td style='white-space: pre-line'>" . $rs [$i] ["sgd_tpr_termino"] . "</td>
							<td style='white-space: pre-line'>" . $rs [$i] ["sgd_tpr_descrip"] . "</td>
							<td style='white-space: pre-line'>" . $rs [$i] ["dato_1"] . "</td>
							<td style='white-space: pre-line'>" . $rs [$i] ["municipio_1"] . "</td>
							<td>" . $rs [$i] ["anex_creador"] . "</td>
						</tr>";
        }
        $data.="</table>";
        return $data;
    }

    /**
     * Consulta Estadistica 18 (PQRS) PETICIONES, QUEJAS, RECLAMOS, SOLICITUDES
     * 
     */
    function estadistica18($depe, $fechIni, $fechFin, $serie, $tipoRadicado) {
        //print "$depe, $fechIni, $fechFin, $usua, $estado, $tipoRadicado, $tipoDocumento <br>";
        $rs = $this->modelo->est18($depe, $fechIni, $fechFin, $serie, $tipoRadicado);
        if ($rs['error']) {
            return $rs['error'];
        }
        $f = count($rs);
        $data.="<table id='est18'>
            <tr >
		<td class='cosa' colspan='3'>{$rs[0] ["codiSerie"]}  {$rs [0] ["nomSerie"]}</td></tr>
					<tr class='tpar'>
		<td class='titulos3'>#</td><td class='titulos3'>SubSerie</td><td class='titulos3'>Tipificados</td></tr>";
        for ($i = 0; $i < count($rs); $i++) {
            if ($i % 2) {
                $style = " class='listado1'  ";
            } else {
                $style = " class='listado2' ";
            }
            $c = $i + 1;
            $TITULO = $rs[$i] ["codserie"] . " " . $rs [0] ["nomSerie"] . " - " . $rs[$i] ["codSubserie"] . " " . $rs [$i] ["nomSubserie"];
            $data.="<tr $style><td>$c</td><td style='white-space: pre-line'> {$rs[$i] ["codSubserie"]} {$rs [$i] ["nomSubserie"]}</td><td ><div style=' text-align: center'><a href='#' onclick='detalles({$rs[$i] ["codSubserie"]},\"$TITULO\");'>{$rs [$i] ["valor"]}</a></div></td></tr>";
        }
        $data.="</table>";

        return $data;
    }

    /**
     * Detalles Estadistica 18 (PQRS) PETICIONES, QUEJAS, RECLAMOS, SOLICITUDES
     * 
     */
    function detallesEst18($depe, $fechIni, $fechFin, $Serie, $subserien, $subseriec, $tipoRadicado) {
        //print "$fechIni, $fechFin, $usua <br>";

        $rs = $this->modelo->detallesEst18($depe, $fechIni, $fechFin, $Serie, $subseriec, $tipoRadicado);
        /* $combie['resp']=$combia;
          $combie['data']=$combi;
          $combia[$rs2->fields ['DERI']][$i]
          $combi[$i]['radicado'] = $rs->fields ['RADI'];
          $combi[$i]['asunto'] = $rs->fields ['ASUNTO'];
          $combi[$i]['dependencia'] = $rs->fields ['DEPEN']; */
        $dataRs = $rs['data'];
        $respuestas = $rs['resp'];
        //print_r($rs);
        if ($dataRs['error']) {
            return $rs['error'];
        }
        $fechainf = date("Y:m:d H:m:s");
        $data.="<table style='width: 100%'><tr class='tpar'><td class='cosa' colspan='7'>$subseriec - $subserien</td></tr>
                            <trclass='tpar'>
								<td class='titulos3' colspan='7'>
									<table>
										<tr class='tpar'>
											<td class='titulos3'>Rango de fechas:</td>
											<td class='titulos3'>Desde $fechIni Hasta $fechFin </td>
											<td class='titulos3'>Fecha Generacion del Informe: " . $fechainf . "</td>
										</tr>
									</table>
								</td>
							</tr>
							<trclass='tpar'>
								<td class='titulos3'>#</td>
                                                                <td class='titulos3'>Fecha Rad</td>
								<td class='titulos3'>Radicado</td>
								<td class='titulos3' >Asunto</td>
                                                                <td class='titulos3' >Dependencia Act.</td>
                                                                <td class='titulos3' >Usuario Act.</td>
								<td class='titulos3'>Respuesta</td>
							</tr>";
        $respRadi = '';
        for ($i = 0; $i < count($dataRs); $i++) {
            if ($i % 2) {
                $style = " class='listado1' ";
            } else {
                $style = " class='listado2' ";
            }
            $c = $i + 1;
            $data.="<tr $style>	<td>$c</td>    <td style='white-space: pre-line'>{$dataRs [$i] ["radiFech"]}</td>
                <td style='white-space: pre-line'><a href='#' onclick='verDocumento(\"../../../../\",{$dataRs [$i] ["radicado"]})'>{$dataRs [$i] ["radicado"]}</a></td>
                
                        <td style='white-space: pre-line'>{$dataRs [$i] ["asunto"]}</td>
                    <td style='white-space: pre-line'> {$dataRs [$i] ["dependencia"]}</td>
                    <td style='white-space: pre-line'> {$dataRs [$i] ["usuario"]}</td>";
            //  print_r($radff);
            $radff = $respuestas[$dataRs[$i]["radicado"]];
            $espace = '';
            for ($index = 0; $index < count($radff); $index++) {
                $respRadi.=$espace . $radff[$index]['numeradi'] . " - " . $radff[$index]['fecha'];
                $espace = '<br>';
            }

            $data.="<td>$respRadi</td></tr>";
            $respRadi = '';
        }
        $data.="</table>";
        /* $combia[$rs2->fields ['DERI']][$i]['numeradi'] = $rs2->fields ['RADICADO'];
          $combia[$rs2->fields ['DERI']][$i]['fecha'] = $rs2->fields ['FECH']; */
        return $data;
    }

    /**
     * Consulta Estadistica 19 RADICADOS REASIGNADOS
     * 
     */
    function estadistica19($depe, $fechIni, $fechFin, $usua, $tipoRadicado, $tipoDocumento) {
        //print "$depe, $fechIni, $fechFin, $usua, $tipoRadicado, $tipoDocumento <br>";

        $rs = $this->modelo->est19($depe, $fechIni, $fechFin, $usua, $tipoRadicado, $tipoDocumento);
        if ($rs['error']) {
            return $rs['error'];
        }

        //print_r ($rs);
        $f = count($rs);
        $data.="<table>
					<trclass='tpar'>
		<td class='titulos3'>#</td><td class='titulos3'>Usuario</td><td class='titulos3'>Radicados</td></tr>";
        for ($i = 0; $i < count($rs); $i++) {
            if ($i % 2) {
                $style = " class='listado1'  ";
            } else {
                $style = " class='listado2' ";
            }
            $c = $i + 1;
            $data.="<tr $style><td>$c</td><td>" . $rs [$i] ["usuario"] . "</td><td ><div style=' text-align: center'><a href='#' onclick='detallese19(" . $rs [$i] ['usuacodi'] . "," . $rs [$i] ['usuadepe'] . ");'>" . $rs [$i] ["radicados"] . "</a></div></td></tr>";
        }
        $data.="</table>";

        return $data;
    }

    /**
     * Detalles Estadistica 19 RADICADOS REASIGNADOS
     * 
     */
    function detallesEst19($depe, $fechIni, $fechFin, $usua, $tipoRadicado, $tipoDocumento) {
        //print "$depe, $fechIni, $fechFin, $usua, $tipoRadicado, $tipoDocumento <br>";

        $rs = $this->modelo->detallesEst19($depe, $fechIni, $fechFin, $usua, $tipoRadicado, $tipoDocumento);

        if ($rs['error']) {
            return $rs['error'];
        }
        //print_r($rs);

        $f = count($rs);
        $fechainf = date("Y:m:d H:m:s");
        $data.="<table width='100%'><tr class='tpar'><td class='titulos3' colspan='11'>ESTADISTICA POR TRANSACCIONES </td></tr>
                            <trclass='tpar'>
								<td class='titulos3' colspan='11'>
									<table>
										<tr class='tpar'>
											<td class='titulos3'>Dependencia: </td>
											<td class='titulos3'> $depe</td>
											<td class='titulos3'>Rango de fechas:</td>
											<td class='titulos3'>Desde $fechIni Hasta $fechFin </td>
											<td class='titulos3'>Fecha Generacion del Informe: " . $fechainf . "</td>
										</tr>
									</table>
								</td>
							</tr>
							<trclass='tpar'>
								<td class='titulos3'>#</td>
								<td class='titulos3'>Radicado</td>
								<td class='titulos3'>Fecha Radicacion</td>
								<td class='titulos3' style='white-space: pre-line'>Asunto</td>
                                                                <td class='titulos3'>Usuario Destino</td>
								<td class='titulos3'>Dependencia Destino</td>
                                                                <!--<td class='titulos3'>Usuario Rol Destino</td>-->
                                                                <td class='titulos3'>Observacion</td>
                                                                <td class='titulos3'>Transaccion</td>
								<td class='titulos3'>Fecha Transaccion</td>
								<td class='titulos3'>Dependencia Origen </td>
								<td class='titulos3'>Usuario Origen</td>
                                                                <!--<td class='titulos3'>Usuario Rol Origen</td>-->
								
							</tr>";
        for ($i = 0; $i < count($rs); $i++) {
            if ($i % 2) {
                $style = " class='listado1' ";
            } else {
                $style = " class='listado2' ";
            }
            $c = $i + 1;
            $data.="<tr $style style='white-space: pre-line'>	<td>$c</td>
							<td><a href='#' onclick='verDocumento(\"../../../../\",{$rs [$i] ["radicado"]})'>" . $rs [$i] ["radicado"] . "</a></td>
							<td>" . $rs [$i] ["fecharad"] . "</td>
							<td style='white-space: pre-line'>" . $rs [$i] ["asunto"] . "</td>
							<td>" . $rs [$i] ["usuario_d"] . "</td>
							<td>" . $rs [$i] ["dependencia_d"] . "</td>
                                                        <!--<td>" . $rs [$i] ["rol_d"] . "</td>    -->
							<td style='white-space: pre-line'>" . $rs [$i] ["detalle"] . "</td>
							<td>" . $rs [$i] ["trans"] . "</td>
                                                        <td>" . $rs [$i] ["fechareasig"] . "</td>
							<td>" . $rs [$i] ["dependencia_t"] . "</td>
                                                        <td>" . $rs [$i] ["usuario_t"] . "</td>
							<!--<td>" . $rs [$i] ["rol_t"] . "</td>-->
                                                        
							
						</tr>";
        }
        $data.="</table>";
        return $data;
    }

    /**
     * Consulta los datos de las series 
     * @return   void
     */
    function Total1($depe, $fecha_ini, $fecha_fin, $usuario, $estado, $tipoRadicado, $tipoDocumento) {
//         echo "$depe, $fecha_ini, $fecha_fin, $usuario, $estado, $tipoRadicado, $tipoDocumento";
        $rs = $this->modelo->Total1($depe, $fecha_ini, $fecha_fin, $usuario, $estado, $tipoRadicado, $tipoDocumento);
        if ($rs['error']) {
            return $rs['error'];
        }
        //print_r($rs);
        $f = count($rs);
        $data.="<table style=\"width :100%\ ;padding:1 ; border: 1\">
            <tr><td class=\"cosa\" colspan=\"6\">
      RADICACION POR USUARIO COSOLIDADO
    </td></tr>
					<trclass='tpar'>
		<th class='titulos3'>#</th><th class='titulos3'>Radicado</th>
                <th class='titulos3'>Fecha Radicacion</th><th class='titulos3'>Asunto</th><th class='titulos3'>Remitente</th><th class='titulos3'>Dependencia Actual</th></tr>";
        for ($i = 0; $i < count($rs); $i++) {
            if ($i % 2) {
                $style = " class='listado1' ";
            } else {
                $style = " class='listado2' ";
            }
            $c = $i + 1;
            $estausua = $rs [$i] ["estado"];
            if ($estausua == 1) {
                $estausua = "ACTIVO";
            } else {
                $estausua = "INACTIVO";
            }
            $data.="<tr $style ><td style='border:1px solid white;' class='leidos'>$c</td>
                               <td class='leidos' style='border:1px solid white;'><a href='#' onclick='verDocumento(\"../../../../\",{$rs [$i] ["radicado"]})'>" . $rs [$i] ["radicado"] . "</a></td>
                               <td class='leidos' style='border:1px solid white;'>" . $rs [$i] ["fech"] . "</td>    
                               <td class='leidos' style=\"border:1px solid white;white-space: pre-line\">" . $rs [$i] ["asunto"] . "</td>
                               <td class='leidos' style=\"border:1px solid white;white-space: pre-line\">" . $rs [$i] ["remitente"] . "</td>
                               <td class='leidos' style=\"border:1px solid white;white-space: pre-line\">" . $rs [$i] ["depeA"] . "</td>
                                </tr>";
        }
        $data.="</table>";

        return $data;
    }

    /**
     * Consulta los datos de las series 
     * @return   void
     */
    function estadistica20($depe, $fecha_ini, $fecha_fin, $tipoRadicado, $estado = 0) {

        $rs = $this->modelo->est20($depe, $fecha_ini, $fecha_fin, $tipoRadicado);

        $dataDepe = $this->modelo->consultarDepe();
        if ($rs['error']) {
            return $rs['error'];
        }
        $data2.="<table>";
        $data2.="<tr class='titulos4'>
                    <th colspan=2>Rango de fechas $fecha_ini <br>a $fecha_fin</th></tr>";
        $depeX = 0;
        for ($i = 0; $i < count($rs); $i++) {
            if ($depeX != $rs [$i] ["DEPE"]) {
                $data.="<tr class='titulos4'>
                    <td colspan=2>{$dataDepe[$rs [$i] ["DEPE"]]['depe_nomb']} - {$dataDepe[$rs [$i] ["DEPE"]]['depe_codi']}</td></tr>
		<td class='titulos3'>Tipo Radicado</td><td class='titulos3'>cantidad</td></tr>";
                $depeX = $rs [$i] ["DEPE"];
            }
            if ($i % 2) {
                $style = " class='listado1' ";
            } else {
                $style = " class='listado2' ";
            }
            $c = $i + 1;
            $data.="<tr $style><td>" . strtoupper($rs [$i] ["TIPORADQ"]) . "</td><td>" . $rs [$i] ["NUM"] . "</td></tr>";
            $resultado[$rs [$i] ["TIPORADQ"]] = $rs [$i] ["NUM"] + $resultado[$rs [$i] ["TIPORADQ"]];
            $tipoC[$rs [$i] ["TIPORADQ"]] = $rs [$i] ["TPR"];
        }
        $data3.="<tr class='titulos2'>
                    <td colspan=2>Todas las Dependencias</td></tr>
		<tr class='titulos2'><td >Tipo Radicado</td><td class='titulos3'>cantidad</td></tr>";
        foreach ($resultado as $key => $value) {
            $data3.="<tr $style><td>" . strtoupper($key) . "</td>"
                    . "<td><a href='#' onclick='detalles(\"{$tipoC[$key]}\",\"$value\")'>"
                    . "" . $value . "</a></td></tr>";
        }
        $data2 = $data2 . $data3 . $data . "</table>";


        return $data2;
    }

    function detallesEst20($depe, $fechIni, $fechFin, $usua, $tipoRadicado) {
        //print "$depe, $fechIni, $fechFin, $usua, $tipoRadicado, $tipoDocumento <br>";

        $rs = $this->modelo->detallesEst20($depe, $fechIni, $fechFin, $usua, $tipoRadicado);
        $dataDepe = $this->modelo->consultarDepe();
        $dataUsua = $this->modelo->consultarUsu();
        $muni = $this->modelo->consultarMuni();
        $depto= $this->modelo->consultarDepto();
        $tpd= $this->modelo->consultarTP();
        if ($rs['error']) {
            return $rs['error'];
        }
        ini_set('memory_limit','2200M');
       //print_r($tpd);
       /* echo "<hr>";
        print_r($muni);
        echo "<hr>";
        //print_r($dataUsua);
        echo "<hr>";*/
        $pdf = 0;
        $sin = 0;
        $docx = 0;
        $f = count($rs);
        $fechainf = date("Y:m:d H:m:s");
        for ($i = 0; $i < count($rs); $i++) {
            if ($i % 2) {
                $style = " class='listado1' ";
            } else {
                $style = " class='listado2' ";
            }
            $c = $i + 1;
            $path = explode('.', $rs [$i] ["RADI_PATH"]);
            $ext = $path[1];
            switch ($ext) {
                case 'pdf':
                case 'tif':
                    $pdf++;
                    break;
                case 'docx':
                case 'odt':
                    $docx++;
                    break;
                default:
                    $sin++;
                    break;
            }
            $mn=$rs[$i]['ID_PAIS'].'-'.$rs[$i]['DPTO_CODI'].'-'.$rs[$i]['MUNI_CODI'];
            $dp=  $rs[$i]['ID_PAIS'].'-'.$rs[$i]['DPTO_CODI'];
     
            $data.="<tr $style style='white-space: pre-line'>"
                    . "	<td>$c</td>
            		<td><a href='#' onclick='verDocumento(\"../../../../\",{$rs [$i] ["RADI_NUME_RADI"]})'>" . $rs [$i] ["RADI_NUME_RADI"] . "</a></td>
			<td style='white-space: pre-line'>" . $rs [$i] ["RADI_FECH_RADI"] . "</td>
			<td style='white-space: pre-line'>" . $rs [$i] ["RA_ASUN"] . "</td>
                        <td style='white-space: pre-line'>" . $dataUsua[$rs [$i] ["RADI_USUA_RADI"]]['usua_nomb'] . "</td>
			<td style='white-space: pre-line'>" . $dataDepe[$rs [$i] ["RADI_DEPE_RADI"]]['depe_nomb'] . "</td>
                            <td style='white-space: pre-line'>" . $dataUsua[$rs [$i] ["RADI_USUA_ACTU"]]['usua_nomb'] . "</td>
			<td style='white-space: pre-line'>" . $dataDepe[$rs [$i] ["RADI_DEPE_ACTU"]]['depe_nomb'] . "</td>
                        <td style='white-space: pre-line'>" . $ext . "</td>
			 <td style='white-space: pre-line'>" . $rs [$i] ["CONSE"] . "</td>
                         <td style='white-space: pre-line'>" . $rs [$i] ["SGD_DIR_NOMREMDES"] . "</td>
                         <td style='white-space: pre-line'>" . $muni [$mn] ["MUNI_NOMB"] . "</td>
                             <td style='white-space: pre-line'>" . $depto [$dp] ["DPTO_NOMB"] . "</td>
                         <td style='white-space: pre-line'>" . $tpd [$rs[$i] ["TDOC_CODI"]]['sgd_tpr_descrip'] . "</td>
		   </tr>";
                       
        }
        if ($depe == 99999) {
            $depeX = 'todas';
        } else
            $depeX = $depe;
        
        $drre = $rs [count($rs) - 1] ["CONSE"] - $rs [0] ["CONSE"] + 1;
        $difrencia = count($rs);
        $data1.="<table width='100%'>
                  <tr class='tpar'><th class='titulos4' colspan='7'>RADICADOS POR DEPENDENCIAS - " . $fechainf . " </th></tr>
                  <trclass='tpar'>
		    <td class='titulos3' colspan='7'>
			<table>
			    <tr class='tpar'>
				<td class='listado2'>Dependencia $depeX</td>
                                    <td class='titulos1'>Inicial: " . $rs [0] ["RADI_NUME_RADI"] . " </td>
                                 <td class='listado2' style='text-align:left' rowspan='2' colspan='2'>
<table><tr><td>Radicados Listados</td><td>$difrencia</td></tr>                                
<tr><td>Secuncias usadas</td><td>$drre</td></tr>
    </table></tD>                                         

                                     <td class='titulos1' style='text-align:left' rowspan='2' colspan='2'>
<table><tr><td>Imagen pdf o tif</td><td>$pdf</td></tr>                                
<tr><td>Otros formatos</td><td>$docx</td></tr>
    <tr><td>Sin imagen</td><td>$sin</td></tr></table></tD> 

                            </tr>
                            <tr>                                
                                <td class='listado2'>Rango $fechIni - $fechFin </td>
                                <td class='titulos1'>Final: " . $rs [count($rs) - 1] ["RADI_NUME_RADI"] . "</td>
                               </tr>
			</table>
		    </td>
		   </tr>
                   </table>
                   <table width='100%'>
		   <tr class='tpar'>
			<td class='titulos3'>#</td>
			<td class='titulos3'>Radicado</td>
			<td class='titulos3'>Fecha Radicacion</td>
			<td class='titulos3' style='white-space: pre-line'>Asunto</td>
                        <td class='titulos3'>Usuario rad.</td>
                        <td class='titulos3'>Dependencia rad.</td>
                        <td class='titulos3'>Usuario Actual</td>
                        <td class='titulos3'>Dependencia actual.</td>
                        <td class='titulos3'>Tipo Imagen.</td>
			<td class='titulos3'>Consecutivo</td>	
                        <td class='titulos3'>Destinatario</td>	
                        <td class='titulos3'>Ciudad</td>	
                        <td class='titulos3'>Depertamento</td>	
                        <td class='titulos3'>Tipo Documental</td>	
		   </tr>";

        $data.="</table>";
        return $data1 . $data;
    }

function consultarCreacion($depe,$fecha_ini,$fecha_fin){
	$retorno1=$this->modelo->consultarDepExp($depe,$fecha_ini,$fecha_fin);
	if(is_array($retorno1)){
	    $data="\t\t<table>\n";
            $data.="\t\t\t<tr class='tpar'><th class='titulos3'>Dependencia</th><th class='titulos3'>Expedientes Creados</th></tr>\n";
	    $acum=0;
	    for($i=0;$i<count($retorno1);$i++){
		if($i%2==0){
		    $style="class='listado1'";
		}else{
		    $style="class='listado2'";
		}
	    	$data.="\t\t\t<tr $style><td>{$retorno1[$i]['depe_nomb']}</td><td>{$retorno1[$i]['expedientes']}</td></tr>";
		$acum=$acum+$retorno1[$i]['expedientes'];
	    }
	    $data.="\t\t\t<tr class='titulos3'><td>Total</td><td>$acum Expedientes</td></tr>";
	    $data.="\t\t</table>\n";
	    $data.="\t\t<br>\n";
            $retorno2=$this->modelo->consultarCreacion($depe,$fecha_ini,$fecha_fin);
	    //print_r($retorno2);
	    $data.="\t\t<table>";
	    $data.="\t\t\t<tr class='tpar'><th class='titulos3'>Expediente</th><th class='titulos3'>Dependencia</th><th class='titulos3'>Fecha de creacion</th><th class='titulos3'>Usuario Creador</th><th class='titulos3'>Usuario Responsable</th><th class='titulos3'>No. de radicados</th><th class='titulos3'>Rad. Inicial</th><th class='titulos3'>Titulo del Exp.</th><th class='titulos3'>Asunto u Objeto</th></tr>\n";
	    for($i=0;$i<count($retorno2);$i++){
		if($i%2==0){
                    $style="class='listado1'";
                }else{
                    $style="class='listado2'";
                }
		$data.="\t\t\t<tr $style><td>{$retorno2[$i]['expediente']}</td><td>{$retorno2[$i]['depe_nomb']}</td><td>{$retorno2[$i]['fecha_crea']}</td><td>{$retorno2[$i]['usua_crea']}</td><td>{$retorno2[$i]['usua_responsable']}</td><td>{$retorno2[$i]['radicados']}</td><td>{$retorno2[$i]['numradi']}</td><td>{$retorno2[$i]['titulo']}</td><td>{$retorno2[$i]['asunto']}</td></tr>";
	    }
	    $data.="\t\t</table>";
	    return $data;
	}else{
	    return $retorno1;
	}
    }
}

?>
