<?php /**
 * modeloMatrizDoc es la clase encargada de gestionar las operaciones y los datos basicos referentes a los tipos de documento
 * @author Hardy Deimont Niño  Velasquez
 * @name modeloMatrizDoc
 * @version	1.0
 */
if (! $ruta_raiz)
	$ruta_raiz = '../../../..';
	//include_once "$ruta_raiz/include/db/ConnectionHandler.php";
include_once "$ruta_raiz/core/clases/ConnectionHandler.php";
class modeloMatrizDoc {

	public $link;

	function __construct($ruta_raiz) {

		$db = new ConnectionHandler ( "$ruta_raiz" );
		$db->conn->SetFetchMode ( ADODB_FETCH_NUM );
		$db->conn->SetFetchMode ( ADODB_FETCH_ASSOC );
		$this->link = $db;
                //$this->link->conn->debug=true;
	}

	/**
	 * @return consulta los tipos de  docuementos asignados.
	 */
	function consultarAsignados($coddepe,$codserie,$tsub) {
	 	 $isqlC = "select m.sgd_mrd_codigo as CODIGOMRD,t.sgd_tpr_codigo as CODIGO,
                           t.sgd_tpr_descrip as DETALLE, m.sgd_mrd_esta as estado,
                           m.sgd_mrd_esta_exp as estexp ,  m.sgd_mrd_esta_radi as estradi
	                 from sgd_mrd_matrird m, sgd_tpr_tpdcumento t
			 where m.depe_codi = '$coddepe'
 			       and m.sgd_srd_codigo = '$codserie'
			       and m.sgd_sbrd_codigo = '$tsub'
				   and m.sgd_tpr_codigo = t.sgd_tpr_codigo";
		$rs = $this->link->query ( $isqlC );
		$i = 0;
		while ( ! $rs->EOF ) {
			$combi [$i] ["CODIGO"] = $rs->fields ['CODIGO'];
			$combi [$i] ["CODIGOMRD"] = $rs->fields ['CODIGOMRD'];
			$combi [$i] ["DESCRIP"] = $rs->fields ['DETALLE'];
			$combi [$i] ["ESTADO"] = $rs->fields ['ESTADO'];
                        $combi [$i] ["ESTRAD"] = $rs->fields ['ESTRADI'];
                        if($rs->fields ['ESTEXP']==1){
                          $combi["ESTEXP"] =1 ;
                          $combi["code"] =$rs->fields ['CODIGOMRD'] ;
                        }
                        else{
                           $combi["ESTEXP"] =0 ;
                           $combi["code"] =$rs->fields ['CODIGOMRD'] ;
                        }
			$i ++;
			$rs->MoveNext ();
		}
		return $combi;

	}

	/**
	 * @return consulta los  tipos de documentos sin asignar.
	 */
	function consultarSinAsignar($coddepe,$codserie,$tsub,$offset=0) {
		$fin=$offset/20;
		 if($offset==0){
                    $ini=1;
                    $fin=20;
                }else{
                    $fin=($fin+1)*20;
		    $ini=$fin-19;
		}
		 $isqlC = "select a.sgd_tpr_codigo as CODIGO
			, a.sgd_tpr_descrip as DETALLE
	         from sgd_tpr_tpdcumento a
			 where a.sgd_tpr_codigo not in (select t.sgd_tpr_codigo
	         from sgd_mrd_matrird m, sgd_tpr_tpdcumento t
			 where m.depe_codi = '$coddepe'
 			       and m.sgd_srd_codigo = '$codserie'
			       and m.sgd_sbrd_codigo = '$tsub'
				   and m.sgd_tpr_codigo = t.sgd_tpr_codigo)
			      and a.sgd_tpr_codigo != '0'";
	 //$isqlF = $isqlF .  'order by '.$order .' ' .$orderTipo;

		$rs = $this->link->query ( $isqlC );
		$i = 0;
		while ( ! $rs->EOF ) {
			$combi [$i] ["CODIGO"] = $rs->fields ['CODIGO'];
			$combi [$i] ["DESCRIP"] = $rs->fields ['DETALLE'];

			$i ++;
			$rs->MoveNext ();
		}
		return $combi;

	}
	/**
	 * @return consulta los  tipos de documentos sin asignar.
	 */
	function contadorConsultarSinAsignar($coddepe,$codserie,$tsub,$offset=0) {
		 $isqlC = "	select count(1) contador
	         from sgd_tpr_tpdcumento a
			 where a.sgd_tpr_codigo not in (select t.sgd_tpr_codigo
	         from sgd_mrd_matrird m, sgd_tpr_tpdcumento t
			 where m.depe_codi = '$coddepe'
 			       and m.sgd_srd_codigo = '$codserie'
			       and m.sgd_sbrd_codigo = '$tsub'
				   and m.sgd_tpr_codigo = t.sgd_tpr_codigo)
			      and a.sgd_tpr_codigo != '0' ";
		$rs = $this->link->query ( $isqlC );
		$i = 0;
		while ( ! $rs->EOF ) {
			$combi = $rs->fields ['CONTADOR'];
			$i ++;
			$rs->MoveNext ();
		}
		return $combi;

	}

	/**
	 * @return consulta los datos de los Roles.
	 */

	function Buscar($tipo, $busque,$coddepe,$codserie,$tsub,$offset=0) {
	if ($tipo == 1){
			$where = " and a.sgd_tpr_codigo = '$busque'";
		}
		if ($tipo == 2){
			$where = " and upper(a.sgd_tpr_descrip) like upper('%$busque%')";
		}
		$fin=$offset*13;
		if($offset==0){
		    $ini=1;
		    $fin=13;
		}else
		    $ini=$fin-12;
		$query = "select * from (select a.sgd_tpr_codigo as \"CODIGO\"
                        , a.sgd_tpr_descrip as \"DETALLE\", row_number() over (order by a.sgd_tpr_codigo) as rn
                 from sgd_tpr_tpdcumento a
                         where a.sgd_tpr_codigo not in (select t.sgd_tpr_codigo
                 from sgd_mrd_matrird m, sgd_tpr_tpdcumento t
                         where m.depe_codi = '$coddepe'
                               and m.sgd_srd_codigo = '$codserie'
                               and m.sgd_sbrd_codigo = '$tsub'
                                   and m.sgd_tpr_codigo = t.sgd_tpr_codigo)
                                   $where
                              and a.sgd_tpr_codigo != '0') as subquery  where rn between $ini and $fin";



		$rs = $this->link->query ( $query );

		$i = 0;
		if (! $rs->EOF) {
			while ( ! $rs->EOF ) {
				$combi [$i] ["CODIGO"] = $rs->fields ['CODIGO'];
				$combi [$i] ["DESCRIP"] = $rs->fields ['DETALLE'];
				$i ++;
				$combi ['ERROR'] = 'OK';
				$rs->MoveNext ();
			}

			return $combi;
		} else {
			return $combi ['ERROR'] = 'No se encontro  dato Buscado.';
		}

	}
	/**
	 * @return crea un Rol.
	 */
    function add($coddepe,$codserie,$tsub,$tp,$med) {
    	$isqlCount = "select max(sgd_mrd_codigo) as NUMREGT from sgd_mrd_matrird";
					$this->link->conn->SetFetchMode(ADODB_FETCH_ASSOC);
					$rsC = $this->link->query($isqlCount);
					//$this->link->debug=true;
					$numreg = $rsC->fields["NUMREGT"];
				    $numreg = $numreg+1;
					$record = array(); # Inicializa el arreglo que contiene los datos a insertar

					$record["SGD_MRD_CODIGO"] = $numreg;
					$record["DEPE_CODI"]      = $coddepe;
					$record["SGD_SRD_CODIGO"] = $codserie;
					$record["SGD_SBRD_CODIGO"]= $tsub;
					$record["SGD_TPR_CODIGO"] = $tp;
					$record["SOPORTE"] = $med;
					$record["SGD_MRD_ESTA"] = '1';
					$record["SGD_MRD_FECHINI"] = $this->link->conn->OffsetDate(0);
					$insertSQL = $this->link->insert("SGD_MRD_MATRIRD", $record, "true");




	}

	/**
	 * @return resultado de la operacion de actualizacion un estado  tipificacion esxpedientes.
	 */
function modestexp($codigomrd,$estado) {
		 $query1="select depe_codi depe,sgd_srd_codigo srd,sgd_sbrd_codigo sbrd from sgd_mrd_matrird   where sgd_mrd_codigo=$codigomrd";
                $rs2 = $this->link->conn->Execute ( $query1 );
                $DP=$rs2->fields ['DEPE'];
                $SRD=$rs2->fields ['SRD'];
                $SBRD=$rs2->fields ['SBRD'];
		 $query="update sgd_mrd_matrird set sgd_mrd_esta_exp=$estado where depe_codi=$DP and sgd_srd_codigo=$SRD and sgd_sbrd_codigo=$SBRD ";
		$rs = $this->link->conn->Execute ( $query );
		if (! $rs) {
			return 'ERROR';
		}
		return "";

	}

        	/**
	 * @return resultado de la operacion de actualizacion un estado  tipificacion esxpedientes.
	 */
function RADestexp($codigomrd,$estado) {

		$query="update sgd_mrd_matrird set sgd_mrd_esta_radi=$estado where sgd_mrd_codigo=$codigomrd";
		$rs = $this->link->conn->Execute ( $query );
		if (! $rs) {
			return 'ERROR';
		}
		return "";

	}
	/**
	 * @return resultado de la operacion de actualizacion un estado  tipificacion esxpedientes.
	 */
	function modestado($codigomrd,$estado) {

		$query="update sgd_mrd_matrird set sgd_mrd_esta=$estado where sgd_mrd_codigo=$codigomrd";
		$rs = $this->link->conn->Execute ( $query );
		if (! $rs) {
			return 'ERROR';
		}
		return "";

	}

	/**
	 * @return resultado del informe de trd.
	 */
	function consultarInformetrd ($coddepe,$codserie,$tsub,$tp,$option){

	 $query = "SELECT
        		m.depe_codi,
				d.depe_nomb,
				m.sgd_srd_codigo,
				s.sgd_srd_descrip,
				m.sgd_sbrd_codigo,
				su.sgd_sbrd_descrip,
				m.sgd_tpr_codigo,
				t.sgd_tpr_descrip,
				(CASE WHEN m.sgd_mrd_esta = '1' THEN 'A' ELSE 'I' END) AS ESTADO,
				su.sgd_sbrd_tiemag,
				su.sgd_sbrd_tiemac,
				(CASE WHEN su.SGD_SBRD_DISPFIN = '1' THEN 'C. TOTAL' ELSE CASE WHEN su.SGD_SBRD_DISPFIN = '2' THEN 'ELIMINACION' ELSE CASE WHEN su.SGD_SBRD_DISPFIN = '3' THEN 'M.TECNICO' ELSE  'MUESTREO' END END END) AS DISPOSICION,
				su.sgd_sbrd_soporte,
				su.SGD_SBRD_PROCEDI,
				m.SGD_MRD_ESTA_EXP as TSERIE,
				m.SGD_MRD_ESTA as TTIPO
				FROM SGD_MRD_MATRIRD m,SGD_SRD_SERIESRD s,SGD_SBRD_SUBSERIERD su, SGD_TPR_TPDCUMENTO t, DEPENDENCIA d
				WHERE m.depe_codi = d.depe_codi
				AND m.sgd_srd_codigo = s.sgd_srd_codigo
				AND m.sgd_sbrd_codigo = su.sgd_sbrd_codigo
				AND m.sgd_tpr_codigo = t.sgd_tpr_codigo
				AND s.sgd_srd_codigo = su.sgd_srd_codigo
				AND m.SGD_MRD_ESTA='1' ";
	 		$where_depe='';
	 	if($coddepe!=0)
	 		$where_depe = " and m.depe_codi = ".$coddepe;
	 		$where_SRD='';
	 	if($codserie!=0)
	 		$where_SRD = " and m.sgd_srd_codigo  = ".$codserie;
	 	$where_SbRD='';
	 	if($tsub!=0)
	 		$where_SbRD = " and m.sgd_sbrd_codigo  = ".$tsub;
	 	$where_SbTP='';
	 	if($tp!=0)
	 		$where_SbTP = " and m.sgd_tpr_codigo  = ".$tp;
	 		$where_Option='';
		 $order_isql = " order by m.depe_codi, m.sgd_srd_codigo,m.sgd_sbrd_codigo,m.sgd_tpr_codigo ";
	     $query_t = $query .$where_depe. $where_SRD. $where_SbRD . $where_SbTP.$where_Option . $order_isql ;
		$rs = $this->link->query ( $query_t );
		$i = 0;
		while ( ! $rs->EOF ) {
			$combi [$i] ["depe_codi"] = $rs->fields ['DEPE_CODI'];
			$combi [$i] ["depe_nomb"] = $rs->fields ['DEPE_NOMB'];
			$combi [$i] ["sgd_srd_codigo"] = $rs->fields ['SGD_SRD_CODIGO'];
			$combi [$i] ["sgd_sbrd_codigo"] = $rs->fields ['SGD_SBRD_CODIGO'];
			$combi [$i] ["sgd_srd_descrip"] = $rs->fields ['SGD_SRD_DESCRIP'];
			$combi [$i] ["sgd_sbrd_descrip"] = $rs->fields ['SGD_SBRD_DESCRIP'];
			$combi [$i] ["sgd_tpr_codigo"] = $rs->fields ['SGD_TPR_CODIGO'];
			$combi [$i] ["sgd_tpr_descrip"] = $rs->fields ['SGD_TPR_DESCRIP'];
			$combi [$i] ["tserie"] = $rs->fields ['TSERIE'];
			$combi [$i] ["ttipo"] = $rs->fields ['TTIPO'];
			$combi [$i] ["estado"] = $rs->fields ['ESTADO'];
			$combi [$i] ["sgd_sbrd_tiemag"] = $rs->fields ['SGD_SBRD_TIEMAG'];
			$combi [$i] ["sgd_sbrd_tiemac"] = $rs->fields ['SGD_SBRD_TIEMAC'];
			$combi [$i] ["disposicion"] = $rs->fields ['DISPOSICION'];
			$combi [$i] ["sgd_sbrd_soporte"] = $rs->fields ['SGD_SBRD_SOPORTE'];
			$combi [$i] ["sgd_sbrd_procedi"] = $rs->fields ['SGD_SBRD_PROCEDI'];
			$i ++;
			$rs->MoveNext ();
		}
		return $combi;


	}
}
?>
