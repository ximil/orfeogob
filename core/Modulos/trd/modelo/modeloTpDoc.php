<?php /** 
 * modeloTpDoc es la clase encargada de gestionar las operaciones y los datos basicos referentes a los tipos de documento
 * @author Hardy Deimont Niño  Velasquez
 * @name modeloTpDoc
 * @version	1.0
 */
if (! $ruta_raiz)
	$ruta_raiz = '../../../..';
	//include_once "$ruta_raiz/include/db/ConnectionHandler.php";
include_once "$ruta_raiz/core/clases/ConnectionHandler.php";
class modeloTpDoc {
	
	public $link;
	
	function __construct($ruta_raiz) {
		
		$db = new ConnectionHandler ( "$ruta_raiz" );
		$db->conn->SetFetchMode ( ADODB_FETCH_NUM );
		$db->conn->SetFetchMode ( ADODB_FETCH_ASSOC );
                //$db->conn->debug=true;
		$this->link = $db;
                
	}
	
	/**
	 * @return consulta los datos de tpdocumentos.
	 */
	function consultar() {
		 $isqlC = 'select 
			  SGD_TPR_CODIGO          AS "CODIGO",
			   SGD_TPR_DESCRIP        AS "TIPOD",
			  SGD_TPR_TERMINO		  as "TERMINO",
			  SGD_TPR_TP2   		  as "ENTRADA",
			  SGD_TPR_TP1   		  as "SALIDA",
			  SGD_TPR_TP3   		  as "MEMORANDO",
			  SGD_TPR_TP4			  as "TP4",
                          SGD_TPR_TP5			  as "TP5",
                          SGD_TPR_TP6			  as "TP6",
                          SGD_TPR_TP7			  as "TP7",
			  SGD_TPR_TP8			  as "TP8", 
                          SGD_TPR_TP9			  as "TP9",
			  SGD_TPR_ESTADO 	      as "ESTADO" 
			from 
				SGD_TPR_TPDCUMENTO				
			order by  SGD_TPR_DESCRIP';
		$rs = $this->link->query ( $isqlC );
		$i = 0;
if(! $rs->EOF){
		while ( ! $rs->EOF ) {
			$combi [$i] ["CODIGO"] = $rs->fields ['CODIGO'];
			$combi [$i] ["DESCRIP"] = $rs->fields ['TIPOD'];
			$combi [$i] ["TERMINO"] = $rs->fields ['TERMINO'];
			$combi [$i] ["TP2"] = $rs->fields ['ENTRADA'];
			$combi [$i] ["TP1"] = $rs->fields ['SALIDA'];
			$combi [$i] ["TP3"] = $rs->fields ['MEMORANDO'];
			$combi [$i] ["TP4"] = $rs->fields ['TP4'];
                        $combi [$i] ["TP5"] = $rs->fields ['TP5'];
			$combi [$i] ["TP6"] = $rs->fields ['TP6'];
                        $combi [$i] ["TP7"] = $rs->fields ['TP7'];
                        $combi [$i] ["TP8"] = $rs->fields ['TP8'];
                        $combi [$i] ["TP9"] = $rs->fields ['TP9'];
			$combi [$i] ["ESTADO"] = $rs->fields ['ESTADO'];
			$i ++;
			$rs->MoveNext ();
		}
}
		return $combi;
	
	}
	
	/**
	 * @return consulta los datos de tpdocumentos.
	 */
	function consultarLimit($offset=0) {
		 $isqlC = 'select 
			  SGD_TPR_CODIGO          AS "CODIGO",
			   SGD_TPR_DESCRIP        AS "TIPOD",
			  SGD_TPR_TERMINO		  as "TERMINO",
			  SGD_TPR_TP2   		  as "ENTRADA",
			  SGD_TPR_TP1   		  as "SALIDA",
			  SGD_TPR_TP3   		  as "MEMORANDO",
			  SGD_TPR_TP4			  as "TP4",
                          SGD_TPR_TP5			  as "TP5",
                          SGD_TPR_TP6			  as "TP6",
                          SGD_TPR_TP7			  as "TP7",
			  SGD_TPR_TP8			  as "TP8", 
                          SGD_TPR_TP9			  as "TP9", 
			  SGD_TPR_ESTADO 	      as "ESTADO" 
			from 
				SGD_TPR_TPDCUMENTO				
			order by  SGD_TPR_DESCRIP limit 13 OFFSET '.$offset;
		$rs = $this->link->query ( $isqlC );
		$i = 0;
		while ( ! $rs->EOF ) {
			$combi [$i] ["CODIGO"] = $rs->fields ['CODIGO'];
			$combi [$i] ["DESCRIP"] = $rs->fields ['TIPOD'];
			$combi [$i] ["TERMINO"] = $rs->fields ['TERMINO'];
			$combi [$i] ["TP2"] = $rs->fields ['ENTRADA'];
			$combi [$i] ["TP1"] = $rs->fields ['SALIDA'];
			$combi [$i] ["TP3"] = $rs->fields ['MEMORANDO'];
			$combi [$i] ["TP4"] = $rs->fields ['TP4'];
                        $combi [$i] ["TP5"] = $rs->fields ['TP5'];
			$combi [$i] ["TP6"] = $rs->fields ['TP6'];
                        $combi [$i] ["TP7"] = $rs->fields ['TP7'];
                        $combi [$i] ["TP8"] = $rs->fields ['TP8'];
                        $combi [$i] ["TP9"] = $rs->fields ['TP9'];
			$combi [$i] ["ESTADO"] = $rs->fields ['ESTADO'];
			$i ++;
			$rs->MoveNext ();
		}
		return $combi;
	
	}
	/**
	 * @return consulta los datos de tpdocumentos.
	 */
	function consultarTama() {
		 $isqlC = "select 	 count(1) contador	from SGD_TPR_TPDCUMENTO";				
		$rs = $this->link->query ( $isqlC );
			$combi = $rs->fields ['contador'];
		return $combi;
	
	}
	/**
	 * @return consulta los datos de los Roles.
	 */
	
	function Buscar($id, $detaserie, $like = 0) {
		//echo $id,$detaserie;
		if ($like == 2){
			$where = "where SGD_TPR_CODIGO != '$id' and upper(SGD_TPR_DESCRIP)= upper('$detaserie')";
		}
		if ($like == 1){
			$where = "where upper(SGD_TPR_DESCRIP)= upper('$detaserie')";
		}
		if ($like == 0){
			$where = "where upper(SGD_TPR_DESCRIP) like upper('%$detaserie%')";
		}
	    $query = 'select 
			  SGD_TPR_CODIGO          AS "CODIGO",
			   SGD_TPR_DESCRIP        AS "TIPOD",
			  SGD_TPR_TERMINO		  as "TERMINO",
			  SGD_TPR_TP2   		  as "ENTRADA",
			  SGD_TPR_TP1   		  as "SALIDA",
			  SGD_TPR_TP3   		  as "MEMORANDO",
			  SGD_TPR_TP4			  as "TP4",
                          SGD_TPR_TP5			  as "TP5",
                          SGD_TPR_TP6			  as "TP6",
                          SGD_TPR_TP7			  as "TP7",
			  SGD_TPR_TP8			  as "TP8", 
                          SGD_TPR_TP9			  as "TP9", 
			  SGD_TPR_ESTADO 	      as "ESTADO" 
			from 
				SGD_TPR_TPDCUMENTO	'.$where.' order by  SGD_TPR_DESCRIP';
		$rs = $this->link->query ( $query );
		
		$i = 0;
		if (! $rs->EOF) {
			while ( ! $rs->EOF ) {
			$combi [$i] ["CODIGO"] = $rs->fields ['CODIGO'];
			$combi [$i] ["DESCRIP"] = $rs->fields ['TIPOD'];
			$combi [$i] ["TERMINO"] = $rs->fields ['TERMINO'];
			$combi [$i] ["TP2"] = $rs->fields ['ENTRADA'];
			$combi [$i] ["TP1"] = $rs->fields ['SALIDA'];
			$combi [$i] ["TP3"] = $rs->fields ['MEMORANDO'];
			$combi [$i] ["TP4"] = $rs->fields ['TP4'];
                        $combi [$i] ["TP5"] = $rs->fields ['TP5'];
			$combi [$i] ["TP6"] = $rs->fields ['TP6'];
                        $combi [$i] ["TP7"] = $rs->fields ['TP7'];
                        $combi [$i] ["TP8"] = $rs->fields ['TP8'];
                        $combi [$i] ["TP9"] = $rs->fields ['TP9'];
			$combi [$i] ["ESTADO"] = $rs->fields ['ESTADO'];
				$i ++;
				$combi ['ERROR'] = 'OK';
				$rs->MoveNext ();
			}
			
			return $combi;
		} else {
			return $combi ['ERROR'] = 'No se encontro  dato Buscado.';
		}
	
	}
	/**
	 * @return crea un Rol.
	 */
    function crear($detatipod, $terminot, $Arraytp) {
		$isql = "select max(sgd_tpr_codigo) as NUME from sgd_tpr_tpdcumento"; 
		$rs = $this->link->conn->Execute($isql); # Executa la busqueda y obtiene el Codigo del documento.
	  	$codtdocI = $rs->fields["NUME"];
		$codtdocI =$codtdocI + 1;
    	
    	for ($index = 0; $index < count($Arraytp); $index++) {
		$columnValues.=",".$Arraytp[$index]['valor'];
		$columnNames.=",".$Arraytp[$index]['campo'];
	    }
    		$query="insert into SGD_TPR_TPDCUMENTO(SGD_TPR_CODIGO, SGD_TPR_DESCRIP,SGD_TPR_TERMINO" . $columnNames . " ) ";
		 $query .= "VALUES ('$codtdocI','$detatipod','$terminot'" . $columnValues . " ) ";

		$rs = $this->link->conn->Execute ( $query );
		if (! $rs) {
			return 'ERROR: No se realizo la Transacción';
		}
		return 'Tipo Documental Creado';
	}
	
	/**
	 * @return resultado de la operacion de actualizacion un Rol.
	 * @param $codtdocI
	 * @param $detatipod
	 * @param $terminot
	 */
function actualizar($codtdocI, $detatipod, $terminot, $Arraytp) {
		
	$query = "update SGD_TPR_TPDCUMENTO set SGD_TPR_DESCRIP ='$detatipod'
			  ,SGD_TPR_TERMINO = '$terminot'";
	for ($index = 0; $index < count($Arraytp); $index++) {
		$query.=",".$Arraytp[$index]['campo']."=".$Arraytp[$index]['valor'];
	}
	
	 $query .= " where  sgd_tpr_codigo = $codtdocI ";
		
		$rs = $this->link->conn->Execute ( $query );
		if (! $rs) {
			return 'ERROR';
		}
		return "SE MODIFIC&Oacute; Tipo Documental $detatipod codigo $codtdocI";
	
	}
}
?>