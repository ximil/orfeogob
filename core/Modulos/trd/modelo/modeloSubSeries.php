<?php /** 
 * modeloSubSeries es la clase encargada de gestionar las operaciones y los datos basicos referentes a un sub series
 * @author Hardy Deimont Niño  Velasquez
 * @name modeloSubSeries
 * @version	1.0
 */
if (! $ruta_raiz)
	$ruta_raiz = '../../../..';
	//include_once "$ruta_raiz/include/db/ConnectionHandler.php";
include_once "$ruta_raiz/core/clases/ConnectionHandler.php";
class modeloSubSeries {
	
	public $link;
	
	function __construct($ruta_raiz) {
		
		$db = new ConnectionHandler ( "$ruta_raiz" );
		$db->conn->SetFetchMode ( ADODB_FETCH_NUM );
		$db->conn->SetFetchMode ( ADODB_FETCH_ASSOC );
		$this->link = $db;
		//$this->link->conn->debug=true;
	}
	
	/**
	 * @return consulta los datos de serie.
	 *   sgd_sbrd_tiemag,sgd_sbrd_tiemac, sgd_sbrd_dispfin, sgd_sbrd_soporte, sgd_sbrd_procedi
	 */
	function consultar($codserie) {
	$query ="select SGD_SBRD_CODIGO, SGD_SBRD_DESCRIP,to_char(SGD_SBRD_FECHFIN,'DD/MM/YYYY') as SGD_SBRD_FECHFIN,to_char(SGD_SBRD_FECHINI,'DD/MM/YYYY') as SGD_SBRD_FECHINI
	,sgd_sbrd_tiemag,sgd_sbrd_tiemac, sgd_sbrd_dispfin, sgd_sbrd_soporte, sgd_sbrd_procedi from SGD_SBRD_SUBSERIERD where	SGD_SRD_CODIGO = '$codserie' order by SGD_SBRD_DESCRIP ";
		$rs = $this->link->query ( $query );
		$i = 0;
		while ( ! $rs->EOF ) {
			$combi [$i] ["CODIGO"] = $rs->fields ['SGD_SBRD_CODIGO'];
			$combi [$i] ["DESCRIP"] = $rs->fields ['SGD_SBRD_DESCRIP'];
			$combi [$i] ["FECHINI"] = $rs->fields ['SGD_SBRD_FECHINI'];
			$combi [$i] ["FECHFIN"] = $rs->fields ['SGD_SBRD_FECHFIN'];
			$combi [$i] ["TIEMAG"] = $rs->fields ['SGD_SBRD_TIEMAG'];
			$combi [$i] ["TIEMAC"] = $rs->fields ['SGD_SBRD_TIEMAC'];
			$combi [$i] ["DISPOFIN"] = $rs->fields ['SGD_SBRD_DISPFIN'];
			$combi [$i] ["SOPORTE"] = $rs->fields ['SGD_SBRD_SOPORTE'];
			$combi [$i] ["PROCEDI"] = $rs->fields ['SGD_SBRD_PROCEDI'];
			$i ++;
			$rs->MoveNext ();
		}
		return $combi;
	
	}
        
        /**
	 * @return consulta los datos de serie.
	 *   sgd_sbrd_tiemag,sgd_sbrd_tiemac, sgd_sbrd_dispfin, sgd_sbrd_soporte, sgd_sbrd_procedi
	 */
	function consultarDepe($codserie,$depe) {
	/*$query ="select SGD_SBRD_CODIGO, SGD_SBRD_DESCRIP,to_char(SGD_SBRD_FECHFIN,'DD/MM/YYYY') SGD_SBRD_FECHFIN,to_char(SGD_SBRD_FECHINI,'DD/MM/YYYY') SGD_SBRD_FECHINI
	,sgd_sbrd_tiemag,sgd_sbrd_tiemac, sgd_sbrd_dispfin, sgd_sbrd_soporte, sgd_sbrd_procedi from SGD_SBRD_SUBSERIERD where	SGD_SRD_CODIGO = '$codserie' order by SGD_SBRD_DESCRIP ";*/
        $query =" select distinct s.SGD_SBRD_CODIGO, s.SGD_SBRD_DESCRIP,to_char(s.SGD_SBRD_FECHFIN,'DD/MM/YYYY') SGD_SBRD_FECHFIN,to_char(s.SGD_SBRD_FECHINI,'DD/MM/YYYY') SGD_SBRD_FECHINI
	,s.sgd_sbrd_tiemag,s.sgd_sbrd_tiemac, s.sgd_sbrd_dispfin, s.sgd_sbrd_soporte, s.sgd_sbrd_procedi
	         from sgd_mrd_matrird m, SGD_SBRD_SUBSERIERD s
			 where m.depe_codi = '$depe' and m.sgd_mrd_esta='1'
			 	   and s.sgd_srd_codigo = m.sgd_srd_codigo
                                   and m.sgd_sbrd_codigo=s.sgd_sbrd_codigo
                                   and s.SGD_SRD_CODIGO = '$codserie'
			 order by s.SGD_SBRD_CODIGO";
                $rs = $this->link->query ( $query );
		$i = 0;
		while ( ! $rs->EOF ) {
			$combi [$i] ["CODIGO"] = $rs->fields ['SGD_SBRD_CODIGO'];
			$combi [$i] ["DESCRIP"] = $rs->fields ['SGD_SBRD_DESCRIP'];
			$combi [$i] ["FECHINI"] = $rs->fields ['SGD_SBRD_FECHINI'];
			$combi [$i] ["FECHFIN"] = $rs->fields ['SGD_SBRD_FECHFIN'];
			$combi [$i] ["TIEMAG"] = $rs->fields ['SGD_SBRD_TIEMAG'];
			$combi [$i] ["TIEMAC"] = $rs->fields ['SGD_SBRD_TIEMAC'];
			$combi [$i] ["DISPOFIN"] = $rs->fields ['SGD_SBRD_DISPFIN'];
			$combi [$i] ["SOPORTE"] = $rs->fields ['SGD_SBRD_SOPORTE'];
			$combi [$i] ["PROCEDI"] = $rs->fields ['SGD_SBRD_PROCEDI'];
			$i ++;
			$rs->MoveNext ();
		}
		return $combi;
	
	}
        
        
        
        
       
	/**
	 * @return consulta los datos de los Roles.
	 */
	function Buscar($serie, $idsubserie, $detaserie, $like = 0) {
		//echo $id,$detaserie;

			if ($like == 2)
				$where = "and sgd_sbrd_codigo = '$idsubserie' ";
			elseif ($like == 1)
				$where = "and  sgd_sbrd_descrip = '$detaserie'";
				
			else
			    $where = "and upper(sgd_sbrd_descrip) like upper('%$detaserie%')";

	 	$query = "select SGD_SBRD_CODIGO, SGD_SBRD_DESCRIP,to_char(SGD_SBRD_FECHFIN,'DD/MM/YYYY') as SGD_SBRD_FECHFIN,to_char(SGD_SBRD_FECHINI,'DD/MM/YYYY') as SGD_SBRD_FECHINI
	,sgd_sbrd_tiemag,sgd_sbrd_tiemac, sgd_sbrd_dispfin, sgd_sbrd_soporte, sgd_sbrd_procedi from sgd_sbrd_subserierd 
					  where sgd_srd_codigo = '$serie' $where ";
	
		//$query = "select SGD_SRD_CODIGO,SGD_SRD_DESCRIP,to_char(SGD_SRD_FECHFIN,'DD/MM/YYYY') SGD_SRD_FECHFIN ,to_char(SGD_SRD_FECHINI,'DD/MM/YYYY') SGD_SRD_FECHINI  from sgd_srd_seriesrd  $where ";
		$rs = $this->link->query ( $query );
		
		$i = 0;
		if (! $rs->EOF) {
			while ( ! $rs->EOF ) {
			$combi [$i] ["CODIGO"] = $rs->fields ['SGD_SBRD_CODIGO'];
			$combi [$i] ["DESCRIP"] = $rs->fields ['SGD_SBRD_DESCRIP'];
			$combi [$i] ["FECHINI"] = $rs->fields ['SGD_SBRD_FECHINI'];
			$combi [$i] ["FECHFIN"] = $rs->fields ['SGD_SBRD_FECHFIN'];
			$combi [$i] ["TIEMAG"] = $rs->fields ['SGD_SBRD_TIEMAG'];
			$combi [$i] ["TIEMAC"] = $rs->fields ['SGD_SBRD_TIEMAC'];
			$combi [$i] ["DISPOFIN"] = $rs->fields ['SGD_SBRD_DISPFIN'];
			$combi [$i] ["SOPORTE"] = $rs->fields ['SGD_SBRD_SOPORTE'];
			$combi [$i] ["PROCEDI"] = $rs->fields ['SGD_SBRD_PROCEDI'];
				$i ++;
				$combi ['ERROR'] = 'OK';
				$rs->MoveNext ();
			}
			
			return $combi;
		} else {
			return $combi ['ERROR'] = 'No se encontro  dato Buscado.';
		}
	
	}
	/**
	 * @return crea una subserie.
	 */
	function crear($codserie,$tsub,$detasub,$sqlFechaD,$sqlFechaH,$tiem_ag,$tiem_ac,$med,$soporte,$asu) {
		$query="insert into SGD_SBRD_SUBSERIERD(SGD_SRD_CODIGO   , SGD_SBRD_CODIGO,SGD_SBRD_DESCRIP,SGD_SBRD_FECHINI,SGD_SBRD_FECHFIN,SGD_SBRD_TIEMAG ,SGD_SBRD_TIEMAC,SGD_SBRD_DISPFIN,SGD_SBRD_SOPORTE,SGD_SBRD_PROCEDI)
						VALUES ($codserie,$tsub,'$detasub',to_date('".$sqlFechaD."','dd/mm/yyyy'),to_date('".$sqlFechaH."','dd/mm/yyyy'),$tiem_ag,$tiem_ac,'$med','$soporte','$asu')";
		$rs = $this->link->conn->Execute ( $query );
		if (! $rs) {
			return 'ERROR: No se realizo la Transacción de creacion de Subseries';
		}
		return 'Creación de Subserie realizada con exito';
	}
	
	/**
	 * @return resultado de la operacion de actualizacion un Rol.
	 * @param $nombre
	 * @param $tipo
	 * @param $id
	 */
	function actualizar($codserie,$tsub, $detasub, $sqlFechaD, $sqlFechaH,$tiem_ag,$tiem_ac,$med,$soporte,$asu) {
		$isqlUp = "update sgd_sbrd_subserierd 
					   			set SGD_SBRD_DESCRIP= '$detasub' 
						  			,SGD_SBRD_FECHINI=to_date('$sqlFechaD','dd/mm/yyyy')
						  			,SGD_SBRD_FECHFIN =to_date('$sqlFechaH','dd/mm/yyyy') 
						  			,SGD_SBRD_TIEMAG = $tiem_ag
 						  			,SGD_SBRD_TIEMAC = $tiem_ac
 						  			,SGD_SBRD_DISPFIN ='$med'
						  			,SGD_SBRD_SOPORTE ='$soporte'
						  			,SGD_SBRD_PROCEDI ='$asu'
                        		where sgd_srd_codigo = $codserie
									and sgd_sbrd_codigo = $tsub
								";
		$rs = $this->link->conn->Execute ( $isqlUp );
		if (! $rs) {
			return 'ERROR';
		}
		return "Modificaci&oacute;n de la Subserie $detaserie realizada con exito";
	
	}
}
?>
