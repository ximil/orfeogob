<?php 
/** 
 * matrizDoc es la clase encargada de gestionar las operaciones tipos documentales
 * @author Hardy Deimont Niño  Velasquez
 * @name matrizDoc
 * @version	1.0
 */ 
if (! $ruta_raiz)
	$ruta_raiz = '../../../..';
    include_once "$ruta_raiz/core/Modulos/trd/modelo/modeloMatrizDoc.php";
	//==============================================================================================	
	// CLASS matrizDoc
	//==============================================================================================	
	
	/**
	 *  Objecto matrizDoc 
	 */ 

class matrizDoc  {
	private $modelo;
	 
	
	function __construct($ruta_raiz) {
		$this->modelo = new modeloMatrizDoc( $ruta_raiz );
	}
	

	/**
	 * Consulta los datos de los tipos docuemtales asignados
	 * @return   void
	 */
	function consultarAsinados($coddepe,$codserie,$tsub) {
			$rs = $this->modelo->consultarAsignados ($coddepe,$codserie,$tsub);
			return $rs;	
	}
	
	/**
	 * Consulta los datos de los  tipos docuemtales por asignados
	 * @return   void
	 */
	function consultarXAsignar($coddepe,$codserie,$tsub,$offset) {
			$rs = $this->modelo->consultarSinAsignar ($coddepe,$codserie,$tsub,$offset);
			return $rs;	
	}
	/**
	 * Consulta el tamaño de de la tabla 
	 * @return   void
	 */

	function paginadorConsultarXAsignar($coddepe,$codserie,$tsub) {
			$rs = $this->modelo->contadorConsultarSinAsignar ($coddepe,$codserie,$tsub);
			return $rs;	
	}	
	function buscar($tipo,$descrip,$coddepe,$codserie,$tsub,$offset) {

		   $combi = $this->modelo->Buscar( $tipo,$descrip,$coddepe,$codserie,$tsub,$offset);
			if($combi  ["ERROR"]=='OK'){
				return $combi;
			}
		return 'No hay resultados';
		
	}

	/**
	 * adiciona  tipo  documental 
	 */
	function add($coddepe,$codserie,$tsub,$tp,$med) {
		
		 	return $this->modelo->add( $coddepe,$codserie,$tsub,$tp,$med);
	//	return 'Ya existe el codigo';
	}
	/**
	 * actualiza  estado  tipificacion doc
	 */
	function modesta($codigomrd,$estado) {
		 	return $this->modelo->modestado($codigomrd,$estado);

	}
		/**
	 * actualiza  estado  tipificacion expo
	 */
	function modestexp($codigomrd,$estado) {
		 	return $this->modelo->modestexp($codigomrd,$estado);
	} 	
        /**
	 * actualiza  estado  tipificacion expo
	 */
	function modestrad($codigomrd,$estado) {
		 	return $this->modelo->RADestexp($codigomrd,$estado);
	} 	


	/**
	 * Consulta realiza un informe de trd
	 * 
	 * @return   void
	 */
	function informes($coddepe,$codserie,$tsub,$tp,$option) {
		 //echo $coddepe,$codserie,$tsub,$tp,$option;
		if($option==1){
				$rs = $this->modelo->consultarInformetrd ($coddepe,$codserie,$tsub,$tp,$option);
		}else{
			$rs = $this->modelo->consultarInformetrd ($coddepe,$codserie,$tsub,$tp);
		}
			return $rs;	
	}

	function __destruct() {
			
	}
	
}

?>