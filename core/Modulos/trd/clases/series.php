<?php 
/** 
 * series es la clase encargada de gestionar las operaciones series documentales
 * @author Hardy Deimont Niño  Velasquez
 * @name series
 * @version	1.0
 */ 
if (! $ruta_raiz)
	$ruta_raiz = '../../../..';
    include_once "$ruta_raiz/core/Modulos/trd/modelo/modeloSeries.php";
	//==============================================================================================	
	// CLASS series
	//==============================================================================================	
	
	/**
	 *  Objecto series 
	 */ 

class series  {
	public $ruta_raiz;
	private $modelo;
	private $descripcion; //login de  usuario
	private $fechaInicio; //fecha de inicio
	private $fechaFin; // fecha  fin
	private $codigo; //codigo serie
	
	
	
	/**
	 * @return the $descripcion
	 */
	public function getDescripcion() {
		return $this->descripcion;
	}

	/**
	 * @return the $fechaInicio
	 */
	public function getFechaInicio() {
		return $this->fechaInicio;
	}

	/**
	 * @return the $fechaFin
	 */
	public function getFechaFin() {
		return $this->fechaFin;
	}

	/**
	 * @return the $codigo
	 */
	public function getCodigo() {
		return $this->codigo;
	}

	/**
	 * @param $descripcion the $descripcion to set
	 */
	public function setDescripcion($descripcion) {
		$this->descripcion = $descripcion;
	}

	/**
	 * @param $fechaInicio the $fechaInicio to set
	 */
	public function setFechaInicio($fechaInicio) {
		$this->fechaInicio = $fechaInicio;
	}

	/**
	 * @param $fechaFin the $fechaFin to set
	 */
	public function setFechaFin($fechaFin) {
		$this->fechaFin = $fechaFin;
	}

	/**
	 * @param $codigo the $codigo to set
	 */
	public function setCodigo($codigo) {
		$this->codigo = $codigo;
	}

	function __construct($ruta_raiz) {

		$this->ruta_raiz = $ruta_raiz;
	    $this->modelo = new modeloSeries( $ruta_raiz );
	}
	

	/**
	 * Consulta los datos de las series 
	 * @return   void
	 */
	function consultar() {
			$rs = $this->modelo->consultar (  );

			return $rs;
		
	}

        	/**
	 * Consulta los dependecias de la serie tp segun la matriz
	 * @return   void
	 */
	function consultarDepe($depe) {
			$rs = $this->modelo->consultarDepe ($depe  );

			return $rs;
		
	}
        
	function buscar() {
		$combi = $this->modelo->Buscar( 0,$this->descripcion,1);
			if($combi  ["ERROR"]=='OK'){
				return $combi;
			}
		return 'No se encotro la Descripción';
		
	}

	/**
	 * crea usuario
	 */
	function crear() {
		$combi = $this->modelo->Buscar( $this->codigo,'',0);
		if($combi  ["ERROR"]!='OK'){
			$combi2 = $this->modelo->Buscar( 0,$this->descripcion,0);
			if($combi2  ["ERROR"]!='OK'){
		 	return $this->modelo->crear($this->codigo,$this->descripcion, $this->fechaInicio, $this->fechaFin);
			}
			else
			return 'Ya existe la Descripción';
		}
		else 
		return 'Ya existe el codigo';
	}
	/**
	 * actualiza  usuario
	 */
	function actualizar() {

			$combi2 = $this->modelo->Buscar( 0,$this->descripcion);
			if($combi2  ["ERROR"]!='OK'||$combi2  [0]["DESCRIP"]==$this->descripcion){
		 	return $this->modelo->actualizar($this->codigo,$this->descripcion, $this->fechaInicio, $this->fechaFin);
			}
			else
			return 'Ya existe la Descripción';
		
		
	}
 	
	/** 
	 * Limpia los atributos de la instancia referentes a la informacion del usuario
	 * @return   void
	 */
	function __destruct() {
			
	}
	
}

?>