<?php 
/** 
 * subSeries es la clase encargada de gestionar las operaciones sub series documentales
 * @author Hardy Deimont Niño  Velasquez
 * @name subSeries
 * @version	1.0
 */
if (! $ruta_raiz)
	$ruta_raiz = '../../../..';
include_once "$ruta_raiz/core/Modulos/trd/modelo/modeloSubSeries.php";
//==============================================================================================	
// CLASS subSeries
//==============================================================================================	


/**
 * Objecto subSeries 
 */

class subSeries {
	public $ruta_raiz;
	private $modelo;
	private $descripcion; //descripcion subserie
	private $fechaInicio; //fecha de inicio
	private $fechaFin; // fecha  fin
	private $codigo; //codigo Subserie
	private $codserie; //codigo serie
	private $tiemag; //codigo serie
	private $dispfin; //codigo serie
	private $tiemac; //codigo serie
	private $soporte; //codigo serie
	private $procedi; //codigo serie
	


	
	/**
	 * @return the $descripcion
	 */
	public function getDescripcion() {
		return $this->descripcion;
	}

	/**
	 * @return the $fechaInicio
	 */
	public function getFechaInicio() {
		return $this->fechaInicio;
	}

	/**
	 * @return the $fechaFin
	 */
	public function getFechaFin() {
		return $this->fechaFin;
	}

	/**
	 * @return the $codigo
	 */
	public function getCodigo() {
		return $this->codigo;
	}

	/**
	 * @return the $codserie
	 */
	public function getCodserie() {
		return $this->codserie;
	}

	/**
	 * @return the $tiemag
	 */
	public function getTiemag() {
		return $this->tiemag;
	}

	/**
	 * @return the $dispfin
	 */
	public function getDispfin() {
		return $this->dispfin;
	}

	/**
	 * @return the $tiemac
	 */
	public function getTiemac() {
		return $this->tiemac;
	}

	/**
	 * @return the $soporte
	 */
	public function getSoporte() {
		return $this->soporte;
	}

	/**
	 * @return the $procedi
	 */
	public function getProcedi() {
		return $this->procedi;
	}

	/**
	 * @param $descripcion the $descripcion to set
	 */
	public function setDescripcion($descripcion) {
		$this->descripcion = $descripcion;
	}

	/**
	 * @param $fechaInicio the $fechaInicio to set
	 */
	public function setFechaInicio($fechaInicio) {
		$this->fechaInicio = $fechaInicio;
	}

	/**
	 * @param $fechaFin the $fechaFin to set
	 */
	public function setFechaFin($fechaFin) {
		$this->fechaFin = $fechaFin;
	}

	/**
	 * @param $codigo the $codigo to set
	 */
	public function setCodigo($codigo) {
		$this->codigo = $codigo;
	}

	/**
	 * @param $codserie the $codserie to set
	 */
	public function setCodserie($codserie) {
		$this->codserie = $codserie;
	}

	/**
	 * @param $tiemag the $tiemag to set
	 */
	public function setTiemag($tiemag) {
		$this->tiemag = $tiemag;
	}

	/**
	 * @param $dispfin the $dispfin to set
	 */
	public function setDispfin($dispfin) {
		$this->dispfin = $dispfin;
	}

	/**
	 * @param $tiemac the $tiemac to set
	 */
	public function setTiemac($tiemac) {
		$this->tiemac = $tiemac;
	}

	/**
	 * @param $soporte the $soporte to set
	 */
	public function setSoporte($soporte) {
		$this->soporte = $soporte;
	}

	/**
	 * @param $procedi the $procedi to set
	 */
	public function setProcedi($procedi) {
		$this->procedi = $procedi;
	}

	function __construct($ruta_raiz) {
		
		$this->ruta_raiz = $ruta_raiz;
		$this->modelo = new modeloSubSeries ( $ruta_raiz );
	}
	
	/**
	 * Consulta los datos de las series 
	 * @return   void
	 */
	function consultar() {
		$rs = $this->modelo->consultar ($this->codserie);
		return $rs;
	
	}
        
        	/**
	 * Consulta los datos de las series 
	 * @return   void
	 */
	function consultarDepe($depe) {
		$rs = $this->modelo->consultarDepe ($this->codserie,$depe);
		return $rs;
	
	}
	
	function buscar() {
		//echo $this->codserie;
		$combi = $this->modelo->Buscar ( $this->codserie,'', $this->descripcion ,0 );
		if ($combi ["ERROR"] == 'OK') {
			return $combi;
		}
		return 'No se encotro la Descripción';
	
	}
	
	/**
	 * crear subserie
	 */
	function crear() {
		$combi = $this->modelo->Buscar ( $this->codserie,$this->codigo, $this->descripcion ,2 );
		if ($combi ["ERROR"] != 'OK') {
			$combi2 = $this->modelo->Buscar ($this->codserie,$this->codigo, $this->descripcion ,1 );
			if ($combi2 ["ERROR"] != 'OK') {
				return $this->modelo->crear ($this->codserie ,$this->codigo, $this->descripcion, $this->fechaInicio, $this->fechaFin,$this->tiemag,$this->tiemac,$this->dispfin,$this->soporte,$this->procedi);
			} else
				return 'Ya existe la Descripción';
		} else
			return 'Ya existe el codigo';
	}
	/**
	 * actualiza subserie
	 */
	function actualizar() {
		
		$combi2 = $this->modelo->Buscar ($this->codserie,$this->codigo, $this->descripcion ,2);
		//print_r($combi2);
		if ($combi2 ["ERROR"] == 'OK') {
			return $this->modelo->actualizar ($this->codserie ,$this->codigo, $this->descripcion, $this->fechaInicio, $this->fechaFin,$this->tiemag,$this->tiemac,$this->dispfin,$this->soporte,$this->procedi);		
		} else
			return 'Ya existe la Descripción';
	
	}
	
	/** 
	 * Limpia los atributos de la instancia referentes a la informacion del usuario
	 * @return   void
	 */
	function __destruct() {
	
	}

}

?>