<?php 
date_default_timezone_set('America/Bogota');session_start(); 
foreach ($_GET as $key => $valor)   ${$key} = $valor;
foreach ($_POST as $key => $valor)   ${$key} = $valor;
$krd = $_SESSION["krd"];
$dependencia = $_SESSION["dependencia"];
$usua_doc = $_SESSION["usua_doc"];
$codusuario = $_SESSION["codusuario"];
$ruta_raiz = ".."; 

$ruta_raiz="../../../..";
include($ruta_raiz.'/core/vista/validadte.php');
include $ruta_raiz.'/core/config/config-inc.php';
//include_once $ruta_raiz.'/core/language/'.$_SESSION ["lang"].'/data.inc';
include($ruta_raiz.'/core/Modulos/radicacion/clases/tipoRadicado.php');
$scriptname=$ruta_raiz.'/core/Modulos/trd/vista/operTp.php';
$tprad= new tipoRadicado($ruta_raiz);
$tparray=$tprad->consultar();
      // print_r($tparray);
      $cad = "perm_tp";
      $option = "<option value='0'>inactivo</option><option value='1'>Activo</option>";
      for ($index = 0; $index < count($tparray); $index++) {
      	$nombrecampo=$cad.$tparray[$index]['CODIGO'];
      	$val.="<tr><td class='listado2'>".$tparray[$index]['DESCRIP']."</td><td><select name='$nombrecampo' id='$nombrecampo' class='select'>$option</select></td></tr>";
      	$valJS.=",".$nombrecampo;
      	$documenJs.="document.getElementById('$nombrecampo').value = $nombrecampo;\n";
      	$documenJS2.="var $nombrecampo = document.getElementById('$nombrecampo').value;";
      	$documenJSend.="+'&$nombrecampo='+$nombrecampo";
      }
      
?>
<html>
<head>
<link rel="stylesheet" type="text/css" 	href="<?php echo $ruta_raiz?>/js/calendario/calendar.css">
<link rel="stylesheet" href="<?php  echo $ruta_raiz ?>/<?php echo $ESTILOS_PATH ?>/orfeo.css" type="text/css">
<script language="JavaScript" src="<?php echo $ruta_raiz?>/js/common.js"></script>
<script language="JavaScript" type="text/javascript"	src="<?php echo $ruta_raiz?>/js/calendario/calendar_eu.js"></script>
<script type="text/javascript">
function pasardatos(codigo,deta,term<?php echo $valJS; ?> ){
	
	document.adm_tp.cod.value = codigo;
	document.adm_tp.detaserie2.value = deta;
	document.adm_tp.termTram.value = term;
	<?php echo $documenJs; ?>

	//alert(perm_tp1+perm_tp3+perm_tp2);
	vistaFormUnitid('busIns',2);
}

function modiSerie(div,action){

		var termTram = document.getElementById('termTram').value;
		var deta = document.getElementById('detaserie2').value;
		<?php echo $documenJS2; ?>
		if(deta.length==0 || termTram.length ==0 ){
				alert('Debe llenar los campos');
				return false;
			}
		var cod='';
		if(action=='mod'){
			 cod = document.getElementById('cod').value;
		}
		var poststr = "action="+action+"&cod="+cod+"&deta="+deta+"&termTram="+termTram<?php echo $documenJSend; ?>; 
		partes('<?php  echo $scriptname; ?>',div,poststr,'');
		partes('<?php  echo $scriptname; ?>','listadoSERIE','accion=listado','');
}
function buscar(){
	var deta = document.getElementById('detaserie2').value;
	if( deta.length==0 ){
			alert('Debe llenar campo de descripcion');
			return false;
		}
	var poststr = "action=buscar&deta="+deta; 
	partes('<?php  echo $scriptname; ?>','listadoSERIE',poststr,'');
}
</script>
</head>
<body bgcolor="#FFFFFF">
<table class=borde_tab width='100%' cellspacing="1"><tr><td  align='center' class="titulos4">
  TIPOS DOCUMENTALES
</td></tr></table>
<form method="post" action="#" name="adm_tp"> 
<center>
<table width="100%" border="0"  cellspacing="0" cellpadding="0">
  <tr><td height="5"></td></tr><tr><td width="5"></td>
    <td width="210" valign="top"><TABLE width="100%" height="424" cellspacing="2" class="borde_tab">
      <tr>
        <TD height="26" class='titulos2'> Descripci&oacute;n</td>
       </tr>
      <tr> <TD height="62" align="left" valign="top" class='listado2'><textarea name="detaserie2" rows="4"  class="tex_area" id="detaserie2"></textarea>
      <input type="hidden" name='cod' id='cod'></input>
      </td>
      </tr>
	      <tr>
        <TD height="26" class='titulos2'>T&eacute;rmino tr&aacute;mite (d&iacute;as)</td>
       </tr>
      <tr> <TD height="62" align="left" valign="top" class='listado2'><input name="termTram" type="text" class="tex_area" id="termTram" value=""></td>
      </tr>
        <tr>
       <TD height="26" class='titulos2'> Seleccione el tipo de documento</td>
       </tr>
      <tr> <TD height="62" align="left" valign="top" class='listado2'>
      <table cellpadding="0" cellspacing="0"><?php echo $val;?></table>
      </td>
      </tr>
      <tr>
        <td height="26"  align="center" class='titulos2'><div id='busIns'>
            <input type="button" name=buscar_serie2 Value='Buscar' onclick='buscar();' class=botones >
            <input type="button" name=insertar_serie2 Value='Insertar' class=botones onClick="modiSerie('actu','crear')" >
            </div>
      </td>
		 </TR>
      <tr>
		        <td height="26"   align="center"  class='titulos2'>
            <input type="button" name=actua_serie2 Value='Modificar' class=botones onClick="modiSerie('actu','mod')" >
            <input type="reset"  name=aceptar2 class=botones id=aceptar onClick="vistaFormUnitid('busIns',1);" value='Cancelar'>
		</td>
      </tr>
      <tr><td valign="top" ><div id='actu'  > </div></td></tr>
    </table></td><td width="5"></td>
    <td valign="top" ><div id='listadoSERIE'  style=" padding : 4px; width : 99%; height : 430; overflow : auto" class="borde_tab"> </div></td>
	<td width="5"></td>
  </tr>
</table>
<script language="javascript">
partes('<?php  echo $scriptname; ?>','listadoSERIE','accion=listado','');
</script>
</form>
</body>
</html>