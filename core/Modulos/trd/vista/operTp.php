<?php date_default_timezone_set('America/Bogota');session_start();
foreach ($_GET as $key => $valor)    ${$key} = $valor;
foreach ($_POST as $key => $valor)     ${$key} = $valor;
$krd = $_SESSION["krd"];
$dependencia = $_SESSION["dependencia"];
$usua_doc = $_SESSION["usua_doc"];
$codusuario = $_SESSION["codusuario"];
$ruta_raiz = "../../../..";
include_once $ruta_raiz . '/core/Modulos/trd/clases/tpDocumento.php';
include($ruta_raiz . '/core/Modulos/radicacion/clases/tipoRadicado.php');
//$scriptname = $ruta_raiz . '/core/Modulos/trd/vista/operTp.php';
$tprad = new tipoRadicado($ruta_raiz);
$tparray = $tprad->consultar();

$tpdoc = new tpDocumento($ruta_raiz);
//Modificacion del log de usuario
if(isset($_SERVER['HTTP_X_FORWARD_FOR'])){
            $proxy=$_SERVER['HTTP_X_FORWARD_FOR'];
        }else
            $proxy=$_SERVER['REMOTE_ADDR'];
        $REMOTE_ADDR=$_SERVER['REMOTE_ADDR'];
        include_once "$ruta_raiz/core/clases/log.php";
        $log = new log($ruta_raiz);
        $log->setAddrC($REMOTE_ADDR);
        $log->setProxyAd($proxy);

$accion = $_POST['action'];
if ($accion == 'crear') {
    //echo 'crear ';
    $tpdoc->setCodtdocI($_POST['cod']);
    $tpdoc->setDetatipod($_POST['deta']);
    $tpdoc->setTerminot($_POST['termTram']);
    // $numn = count($Listado);
    $cad = "perm_tp";
    $cad2 = "sgd_tpr_tp";
    $nombrecampo = array();
    for ($index = 0; $index < count($tparray); $index++) {
        $nombrecampo[$index]['campo'] = $cad2 . $tparray[$index]['CODIGO'];
        $nombrecampo[$index]['valor'] = $_POST[$cad . $tparray[$index]['CODIGO']];
    }
    $tpdoc->setArraytp($nombrecampo);
    echo $tpdoc->crear();
    $log->setUsuaCodi($codusuario);
    $log->setDepeCodi($dependencia);
    $log->setRolId($_SESSION['id_rol']);
    $log->setDenomDoc(' ');
    $log->setAction('trd_document_type_created');
    $log->setOpera("Creado TipoDocumental ".$_POST['cod'].'-'.$_POST['deta']);
    $log->setNumDocu(' ');
    $log->registroEvento();
} elseif ($accion == 'mod') {
    //echo 'modificar ';
    $tpdoc->setCodtdocI($_POST['cod']);
    $tpdoc->setDetatipod($_POST['deta']);
    $tpdoc->setTerminot($_POST['termTram']);
    //$numn = count($Listado);
    $cad = "perm_tp";
    $cad2 = "sgd_tpr_tp";
    for ($index = 0; $index < count($tparray); $index++) {
        $nombrecampo[$index]['campo'] = $cad2 . $tparray[$index]['CODIGO'];
        $nombrecampo[$index]['valor'] = $_POST[$cad . $tparray[$index]['CODIGO']];
    }
    $tpdoc->setArraytp($nombrecampo);
    echo $tpdoc->actualizar();
    $log->setUsuaCodi($codusuario);
    $log->setDepeCodi($dependencia);
    $log->setRolId($_SESSION['id_rol']);
    $log->setDenomDoc(' ');
    $log->setAction('trd_document_type_modification');
    $log->setOpera("Modificado TipoDocumental ".$_POST['cod'].'-'.$_POST['deta']);
    $log->setNumDocu(' ');
    $log->registroEvento();
} else {


    if ($accion == 'buscar') {

        $tpdoc->setDetatipod($_POST['deta']);
        $Listado = $tpdoc->buscar();
//print_r($Listado);
        $numn = count($Listado) - 1;
    } else {

        $Listado = $tpdoc->consultar();

        $numn = count($Listado);
        $cad = "perm_tp";
        for ($index = 0; $index < count($tparray); $index++) {
            $titulos.="<td class=titulos3 align='center''>" . $tparray[$index]['DESCRIP'] . "</td>";
        }
    }
    ?>


    <TABLE style='width :100% ; padding: 2;'  >
        <tr class=tpar>
            <td class=titulos3 align='center'>EDITAR</td>
            <td class=titulos3 align='center'>Codigo</td>

            <td class=titulos3 align='center'>Descripción</td>
            <td class=titulos3 align='center'>Termino</td>
            <?php echo $titulos; ?>
            <td class=titulos3 align='center'>ESTADO</td>

        </tr>
        <?php        // print_r($Listado);
        for ($i = 0; $i < $numn; $i++) {
            $codigo = $Listado[$i]["CODIGO"];
            $dtpd = trim($Listado[$i]["DESCRIP"]);
            $term = $Listado[$i]["TERMINO"];
            $estado = $Listado[$i]["ESTADO"];
            $val = '';
            $valJs = '';
            for ($index = 0; $index < count($tparray); $index++) {
                $dd=$Listado[$i]['TP'.$tparray[$index]['CODIGO']];
                 if($dd==NULL)
                     $dd=0;
                $val.="<td >$dd</td>";
                $valJs.=",'$dd'";
            }
            ?>
            <tr class='paginacion'>
                <td> <a onclick="pasardatos('<?php echo  $codigo ?>', '<?php echo  $dtpd ?>', '<?php echo  $term ?>'<?php echo $valJs; ?>);">
                        <img width="20px" src='<?php  echo $ruta_raiz; ?>/imagenes/fleditar.png'/></a></td>
                <td> <?php echo  $codigo ?></td>
                <td align='left'> <?php echo  $dtpd ?> </td>
                <td> <?php echo  $term ?> </td>
                <?php echo $val; ?>
                <td> <?php echo  $estado ?> </td>
            </tr>
        <?php } ?>
    </table>
<?php }
?>