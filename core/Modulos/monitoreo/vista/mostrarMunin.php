<?php session_start(); date_default_timezone_set('America/Bogota'); date_default_timezone_set('America/Bogota');
foreach ($_GET as $key => $valor)  $$key = $valor;
foreach ($_POST as $key => $valor)  $$key = $valor;
$krd = $_SESSION["krd"];
$dependencia = $_SESSION["dependencia"];
$usua_doc = $_SESSION["usua_doc"];
$codusuario = $_SESSION["codusuario"];
$id_rol = $_SESSION["id_rol"];
$tpDepeRad=$_SESSION["tpDepeRad"];
$ruta_raiz="../../../..";
$url_munin="//".$_SERVER['HTTP_HOST']."/munin/";
$ruta_frontend="iea.local/sieamon.iea.local/";
include_once "$ruta_raiz/core/vista/validadte.php";
?>
<html>
   <head>
	<title>.::Modulo de monitoreo de maquinas::.</title>
	<link rel="stylesheet" href="<?php echo $ruta_raiz?>/estilos/default/orfeo.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css">
	<style type="text/css">
        #lightbox {
        position: fixed;
        top: 0;
        height: 400px;
        left: 24%;
        width: 90%;
        margin-left: -250px;
        background: #fff;
        overflow: auto;
        z-index: 1001;
        display: none;
        }
        #lightbox-shadow{
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background: #000;
        filter: alpha(opacity=75);
        -moz-opacity: 0.75;
        -khtml-opacity: 0.75;
        opacity: 0.75;
        z-index: 1000;
        display: none;
        }
        </style>
	<script language="JavaScript" src="<?php echo $ruta_raiz?>/js/common.js"></script>
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
	<script>
	function rutaMostrada(ruta){
		$.get(ruta,function(data){
		    $("#response").html($(data).find('#content').contents());
		    $('img').each(function (){
			var x=$(this).attr('src').replace('../..','<?php echo $url_munin?>');
			$(this).attr('src',x);
		    });
		    $('a').each(function(){
			var y=$(this).attr('href');
			$(this).attr('href','#');
			$(this).attr("onclick","overWindow('"+ruta+y+"')");
		    });
                });
	}
	function overWindow(script){
            if($("#lightbox").size()==0){
            var theLightbox = $('<div id="lightbox"/>');
            var theShadow = $('<div id="lightbox-shadow" onclick="cerrar()"/>');
                $('body').append(theShadow);
                $('body').append(theLightbox);
            }
            $("#lightbox").empty();
            $.get(script, function(data){
               	$("#lightbox").append($(data).find('#content').contents());
	   	$('img').each(function (){
                    var x=$(this).attr('src').replace('../..','<?php echo $url_munin?>');
                    $(this).attr('src',x);
                });
		$("#lightbox").find('a').removeAttr('href');
            });
            //$('#lightbox').css('top', $(window).scrollTop() + 100 + 'px');
	    /*$('#lightbox').find('a').each(function(){
		$(this).replaceWith('p');
	    });*/
            // display the lightbox
            $('#lightbox').show();
            $('#lightbox-shadow').show();
        }
	function cerrar(){
            $('#lightbox').hide();
            $('#lightbox-shadow').hide();
            $('#lightbox').empty();
        }
	</script>
   </head>
   <body>
	<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center" class="BORDE_TAB">
            <tr align="center" >
        	<td class="titulos4">
        	<center>MONITOREO DE ESTADO DEL SISTEMA</center></td></tr>
	    <tr>
	    <?php 	    if ($_SESSION["usua_admin_sistema"] == 1) {
	    ?>
            	<td class="info" colspan=4 id='devolver'><center><input class='botones_mediano' type='button' value='Estado Front-End' onclick="rutaMostrada('<?php echo $url_munin.$ruta_frontend?>')"></center></td>
           </tr>
	   <?php 	   }
	   else{
	   ?>
		<td class="info" colspan=4 id='devolver'><center><b>No tiene permiso para revisar el monitoreo de la maquina</b></center></td>
	   <?php 	   }
	   ?>
        </table>
	<center>
	<div id="response"></div>
	</center>
   </body>
</html>
