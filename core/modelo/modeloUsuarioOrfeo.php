<?php
/**
 * @author Hardy Deimont Niño  Velasquez
 *
 *
 */
//error_reporting( ~E_NOTICE);
if (!$ruta_raiz)
    $ruta_raiz = '../..';

//include_once "$ruta_raiz/include/db/ConnectionHandler.php";
include_once "$ruta_raiz/core/clases/ConnectionHandler.php";
include_once "$ruta_raiz/core/config/config-inc.php";

class modeloUsuarioOrfeo {

    public $link;

    function __construct($ruta_raiz) {
        $db = new ConnectionHandler("$ruta_raiz");
        $db->conn->SetFetchMode(ADODB_FETCH_NUM);
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        /*if ($_SESSION['test'] == 1)
            $db->conn->debug = true;*/
        $this->link = $db;
    }

    /**
     * @return consulta los datos de usuario.
     */
    function consultar($login) {
/*        if ($ext == 1) {
            $busqueda = "upper(usua_login_externo) =upper('$login')";
        } else {
            $busqueda = "upper(USUA_LOGIN) =upper('$login')";
        }*/
         $busqueda = "(upper(USUA_LOGIN) =upper('$login') or upper(usua_login_externo) =upper('$login'))";
        $query = "SELECT * from usuario, sgd_urd_usuaroldep where $busqueda and usuario.usua_codi=sgd_urd_usuaroldep.usua_codi";
        $rs = $this->link->query($query);
        $resultado = '';
        if (!$rs->EOF) {
            foreach ($rs->fields as $key => $valor) {
                $resultado [strtoupper($key)] = $valor;
            }
            $resultado ['Error'] = '0';
        } else {
            $resultado ['Error'] = 'No se contienen datos';
        }
        /*
          echo "<hr>";
          print_r($resultado);
          echo "<hr>";
         */
        return $resultado;
    }

    /**
     * @return consulta los datos de usuario.
     */
    function consultar_depe_rol($depe_codi, $rol_codi) {
        $query = "SELECT * from usuario u, sgd_urd_usuaroldep d  where
                 d.depe_codi='$depe_codi' and d.rol_codi=$rol_codi and u.usua_codi=d.usua_codi";
        $rs = $this->link->query($query);
        if (!$rs->EOF) {
            $resultado ['LOGIN'] = $rs->fields ['USUA_LOGIN'];
            $resultado ['DEPE_CODI'] = $rs->fields ['DEPE_CODI'];
            $resultado ['USUA_PASW'] = $rs->fields ['USUA_PASW'];
            $resultado ['USUA_NOMB'] = $rs->fields ['USUA_NOMB'];
            $resultado ['USUA_FECH_CREA'] = $rs->fields ['USUA_FECH_CREA'];
            $resultado ['USUA_NUEVO'] = $rs->fields ['USUA_NUEVO'];
            $resultado ['USUA_DOC'] = $rs->fields ['USUA_DOC'];
            $resultado ['USUA_NACIM'] = $rs->fields ['USUA_NACIM'];
            $resultado ['USUA_EMAIL'] = $rs->fields ['USUA_EMAIL'];
            $resultado ['USUA_ESTA'] = $rs->fields ['USUA_ESTA'];
            $resultado ['USUA_SESION'] = $rs->fields ['USUA_SESION'];
            $resultado ['CODI_NIVEL'] = $rs->fields ['CODI_NIVEL'];
            $resultado ['USUA_DEBUG'] = $rs->fields ['USUA_DEBUG'];
            $resultado ['USUA_AT'] = $rs->fields ['USUA_AT'];
            $resultado ['USUA_CODI'] = $rs->fields ['USUA_CODI'];
            $resultado ['ROL_CODI'] = $rs->fields ['ROL_CODI'];
            $resultado ['USU_TELEFONO1'] = $rs->fields ['USU_TELEFONO1'];
            $resultado ['ID_PAIS'] = $rs->fields ['ID_PAIS'];
            $resultado ['ID_CONT'] = $rs->fields ['ID_CONT'];
            $resultado ['LDAP'] = $rs->fields ['USUA_AUTH_LDAP'];
            $resultado ['USUA_VISTAGRAPHIP'] = $rs->fields ['USUA_VISTAGRAPHIP'];
            $resultado ['USUA_PAGES'] = $rs->fields ['USUA_PAGES'];
            $resultado ['USUA_LOGIN_EXTERNO'] = $rs->fields ['USUA_LOGIN_EXTERNO'];
            $resultado ['Error'] = '0';
        } else {
            $resultado ['Error'] = 'No se contienen datos';
        }

        return $resultado;
    }

    /**
     * @return consulta los datos de usuario.
     */
    function consultarGrupo($depecodi, $usActivos) {
        if ($usActivos == 1)
            $where = " and u.USUA_ESTA='1' ";
        else
            $where = '';
        //echo 	$query = " SELECT * from usuario where DEPE_CODI ='$depecodi' $where";
        //$query = " SELECT * from usuario where DEPE_CODI ='$depecodi'";
        $query = "SELECT * from usuario  u, sgd_urd_usuaroldep d  where
                 d.depe_codi='$depecodi' and u.usua_codi=d.usua_codi  $where";
        $rs = $this->link->query($query);
        if (!$rs->EOF) {
            $i = 0;
            while (!$rs->EOF) {
                $resultado [$i] ['DEPE_CODI'] = $rs->fields ['DEPE_CODI'];
                //	$resultado[$i]['USUA_PASW'] = $rs->fields ['USUA_PASW'];
                $resultado [$i] ['USUA_NOMB'] = $rs->fields ['USUA_NOMB'];
                $resultado [$i] ['USUA_FECH_CREA'] = $rs->fields ['USUA_FECH_CREA'];
                $resultado [$i] ['USUA_NUEVO'] = $rs->fields ['USUA_NUEVO'];
                $resultado [$i] ['USUA_DOC'] = $rs->fields ['USUA_DOC'];
                $resultado [$i] ['USUA_NACIM'] = $rs->fields ['USUA_NACIM'];
                $resultado [$i] ['USUA_EMAIL'] = $rs->fields ['USUA_EMAIL'];
                $resultado [$i] ['USUA_ESTA'] = $rs->fields ['USUA_ESTA'];
                $resultado [$i] ['USUA_SESION'] = $rs->fields ['USUA_SESION'];
                $resultado [$i] ['CODI_NIVEL'] = $rs->fields ['CODI_NIVEL'];
                $resultado [$i] ['USUA_DEBUG'] = $rs->fields ['USUA_DEBUG'];
                $resultado [$i] ['USU_AT'] = $rs->fields ['USU_AT'];
                $resultado [$i] ['USUA_CODI'] = $rs->fields ['USUA_CODI'];
                $resultado [$i] ['US_TELEFONO1'] = $rs->fields ['US_TELEFONO1'];
                $resultado [$i] ['ID_PAIS'] = $rs->fields ['ID_PAIS'];
                $resultado [$i] ['ID_CONT'] = $rsa->fields ['ID_CONT'];
                $resultado [$i] ['LDAP'] = $rsa->fields ['USUA_AUTH_LDAP'];
                $resultado [$i] ['USUA_VISTAGRAPHIP'] = $rs->fields ['USUA_VISTAGRAPHIP'];
                $resultado [$i] ['USUA_PAGES'] = $rs->fields ['USUA_PAGES'];
                $i++;
                $rs->MoveNext();
            }
        } else {
            $retono ['Error'] = 'No contiene datos';
        }

        return $resultado;
    }

    /**
     * @return consulta los datos de usuario.
     */
    function consultarTodos() {
        $sqlChar = "to_char(USUA_NACIM,'DD/MM/YYYY') as USUA_NACIM";
        $query = " SELECT upper(USUA_LOGIN_EXTERNO),upper(USUA_LOGIN),USUA_NOMB,USUA_FECH_CREA,USUA_NUEVO,USUA_DOC,$sqlChar,
            USUA_EMAIL,USUA_ESTA,USUA_SESION,CODI_NIVEL,USUA_AT,USUA_CODI,USUA_EXT,
            USUA_AUTH_LDAP from usuario order by USUA_LOGIN";
        $rs = $this->link->query($query);
        if (!$rs->EOF) {
            $i = 0;
            while (!$rs->EOF) {
                $resultado [$i] ['USUA_LOGIN'] = $rs->fields ['USUA_LOGIN'];
                $resultado [$i] ['USUA_NOMB'] = $rs->fields ['USUA_NOMB'];
                $resultado [$i] ['USUA_FECH_CREA'] = $rs->fields ['USUA_FECH_CREA'];
                $resultado [$i] ['USUA_NUEVO'] = $rs->fields ['USUA_NUEVO'];
                $resultado [$i] ['USUA_DOC'] = $rs->fields ['USUA_DOC'];
                $resultado [$i] ['USUA_NACIM'] = $rs->fields ['USUA_NACIM'];
                $resultado [$i] ['USUA_EMAIL'] = $rs->fields ['USUA_EMAIL'];
                $resultado [$i] ['USUA_ESTA'] = $rs->fields ['USUA_ESTA'];
                $resultado [$i] ['USUA_SESION'] = $rs->fields ['USUA_SESION'];
                $resultado [$i] ['CODI_NIVEL'] = $rs->fields ['CODI_NIVEL'];
                $resultado [$i] ['USUA_DEBUG'] = $rs->fields ['USUA_DEBUG'];
                $resultado [$i] ['USU_AT'] = $rs->fields ['USUA_AT'];
                $resultado [$i] ['USUA_CODI'] = $rs->fields ['USUA_CODI'];
                $resultado [$i] ['US_TELEFONO1'] = $rs->fields ['USUA_EXT'];
                $resultado [$i] ['LDAP'] = $rs->fields ['USUA_AUTH_LDAP'];
                $resultado [$i] ['USUA_LOGIN_EXTERNO'] = $rs->fields ['USUA_LOGIN_EXTERNO'];

                // $resultado [$i] ['ASOCIMG'] = $rs->fields ['PERM_RADI'];
                /* $resultado[$i]['USUA_VISTAGRAPHIP']=$rs->fields['USUA_VISTAGRAPHIP'];
                  $resultado[$i]['USUA_PAGES']=$rs->fields['USUA_PAGES']; */
                $i++;
                $rs->MoveNext();
            }
        } else {
            $retono ['Error'] = 'No contiene datos';
        }

        return $resultado;
    }

    function sessionUpdate($login, $sid) {
        $fech = $this->link->conn->OffsetDate(0, $this->link->conn->sysTimeStamp);
        $query = "update usuario set usua_sesion='" . $sid . "',usua_fech_sesion=$fech where  USUA_LOGIN ='$login'  ";

        $rs = $this->link->query($query);

        //die();
    }

    function cerrarSession() {
        //	$fech=$this->link->conn->OffsetDate(0,$db->conn->sysTimeStamp);
        $fecha = "'FIN  " . date("Y:m:d H:mi:s") . "'";
        $query = "update usuario
		set usua_sesion=" . $fecha . "
		where USUA_SESION = '" . session_id() . "'";
        $rs = $this->link->query($query);
    }

    function cambioclave($login, $pass, $codusuario, $fecha, $ipserver, $iplocal, $opsys) {
        $query = "update usuario set usua_pasw='" . substr(md5($pass), 1, 26) . "', usua_nuevo=1 where  upper(USUA_LOGIN) = upper('$login')  ";
        $rs = $this->link->query($query);

        $query2 = "select count(id) as cambios from user_security where usua_codi=$codusuario";
        $rs2 = $this->link->query($query2);

        if (!$rs2->EOF) {
            $conteo = $rs2->fields ['CAMBIOS'];
            if ($conteo >= 3) {
                $query5 = "select id from user_security where fecha_cambio = (select min(fecha_cambio) as ultima from user_security where usua_codi=$codusuario) limit 1";
                $rs5 = $this->link->query($query5);
                $value = $rs5->fields ['ID'];

                $query6 = "update user_security set fecha_cambio=to_timestamp('$fecha','yyyy/mm/dd hh:mi:ss'), usua_passw='" . substr(md5($pass), 1, 26) . "', ip_server='$ipserver', ip_client='$iplocal', sistema_operativo='$opsys' where id=$value";
                $rs6 = $this->link->query($query6);
            } else {
                $query4 = "insert into user_security (usua_codi,fecha_cambio,usua_passw,ip_server,ip_client,sistema_operativo) VALUES ($codusuario,to_timestamp('$fecha','yyyy/mm/dd hh:mi:ss'),'" . substr(md5($pass), 1, 26) . "','$ipserver','$iplocal','$opsys')";
                $rs4 = $this->link->query($query4);
            }
        } else {
            $query3 = "insert into user_security (usua_codi,fecha_cambio,usua_passw,ip_server,ip_client,sistema_operativo) VALUES ($codusuario,to_timestamp('$fecha','yyyy/mm/dd hh:mi:ss'),'" . substr(md5($pass), 1, 26) . "','$ipserver','$iplocal','$opsys')";
            $rs3 = $this->link->query($query3);
        }
    }

    /**
     * @return consulta los datos de usuario.
     */
    function crearUsuario($vector) {

        $sqlcodi = 'select max(usua_codi) as codi from usuario';
        $rs = $this->link->query($sqlcodi);
        $vector ['USUA_CODI'] = $rs->fields ['CODI'] + 1;

        $vector ['USUA_PASW'] = substr(md5(USUANUEVO), 1, 26);
        $data = '';
        $campo = '';
        if ($vector ['USUA_NACIM']) {
            $data.=' usua_nacim,';
            $campo.="to_date('" . $vector ['USUA_NACIM'] . "','dd/mm/yyyy'),";
        }
        if ($vector ['USUA_EMAIL']) {
            $data.=' usua_email,';
            $campo.="'" . $vector ['USUA_EMAIL'] . "',";
        }
        if ($vector ['USUA_EXT']) {
            $data.=' usua_ext,';
            $campo.="'" . $vector ['USUA_EXT'] . "',";
        }
        if ($vector ['USUA_AT']) {
            $data.=' usua_at,';
            $campo.="'" . $vector ['USUA_AT'] . "',";
        }
        //print_r($vector);
        $sql = "insert into usuario (usua_codi,usua_login,usua_fech_crea,usua_pasw,usua_esta,
		usua_nomb,usua_nuevo,usua_doc,$data
		usua_auth_ldap,usua_pages,usua_vistagraphip,usua_debug,usua_login_externo)
		values (" . $vector ['USUA_CODI'] . ",upper('" . $vector ['LOGIN'] . "'),to_date('" . date('Y/m/d') . "','yyyy/mm/dd'),'" . $vector ['USUA_PASW'] . "'," . $vector ['USUA_ESTA'] . ",'" .
                $vector ['USUA_NOMB'] . "'," . $vector ['USUA_NUEVO'] . ",'" . trim($vector ['USUA_DOC']) . "',$campo " . $vector ['USUA_AUTH_LDAP'] . "," .
                $vector ['USUA_PAGES'] . "," . $vector ['USUA_VISTAGRAPHIP'] . "," . $vector ['USUA_DEBUG'] . ",'" . $vector ['USUA_LOGIN_EXTERNO'] . "' )";
        $rs = $this->link->query($sql);

        return $rs->EOF;
    }

    function modUsuario($vector) {
        $data = '';
        $campo = '';
        $vector ['USUA_PASW'] = substr(md5(USUANUEVO), 1, 26);
        if ($vector ['USUA_NACIM']) {
            $data.=", usua_nacim=to_date('" . $vector ['USUA_NACIM'] . "','dd/mm/yyyy')";
        }
        if ($vector ['USUA_EMAIL']) {
            $data.=", usua_email='" . $vector ['USUA_EMAIL'] . "'";
        }
        if ($vector ['USUA_EXT']) {
            $data.=", usua_ext='" . $vector ['USUA_EXT'] . "'";
        }
        if ($vector ['USUA_AT']) {
            $data.=", usua_at='" . $vector ['USUA_AT'] . "'";
        }
        if ($vector ['USUA_NUEVO'] == 0) {
            $data.=", usua_nuevo=" . $vector ['USUA_NUEVO'] . " ";
            $data.=", usua_pasw='" . $vector ['USUA_PASW'] . "'";
        }

        //print_r($vector);
        $sql = "update usuario set  usua_esta=" . $vector ['USUA_ESTA'] . ",
		usua_nomb='" . $vector ['USUA_NOMB'] . "',usua_doc='" . $vector ['USUA_DOC'] . "',
		usua_auth_ldap=" . $vector ['USUA_AUTH_LDAP'] . ",usua_debug=" . $vector ['USUA_DEBUG'] . ",usua_login_externo='" . $vector ['USUA_LOGIN_EXTERNO'] . "' $data where usua_codi=" . $vector['USUA_CODI'] . " ";
        $rs = $this->link->query($sql);

        return $rs->EOF;
    }

    /**
     * @return consulta los datos de usuario.
     */
    function buscar($login, $nombre, $doc) {
        $where = ' where ';
        if ($login) {
            $where.=" upper(usua_login) like upper('%$login%') ";
        }
        if ($nombre) {
            if ($login)
                $where.=' and ';
            $where.=" upper(usua_nomb) like upper('%$nombre%') ";
        }
        if ($doc) {
            if ($nombre || $login)
                $where.=' and ';
            $where.="USUA_DOC like '%$doc%'";
        }

        $sqlChar = "to_char(USUA_NACIM,'DD/MM/YYYY') as USUA_NACIM";
        $query = " SELECT upper(USUA_LOGIN_EXTERNO) as externo, upper(USUA_LOGIN) as logain,USUA_NOMB,USUA_FECH_CREA,USUA_NUEVO,USUA_DOC,"
                . "$sqlChar,USUA_EMAIL,USUA_ESTA,USUA_SESION,CODI_NIVEL,USUA_AT,
                    USUA_CODI,USUA_EXT,USUA_AUTH_LDAP,USUA_DEBUG
		                from usuario $where order by USUA_LOGIN";

        $rs = $this->link->query($query);
        if (!$rs->EOF) {
            $i = 0;
            while (!$rs->EOF) {
                $resultado [$i] ['USUA_LOGIN'] = $rs->fields ['LOGAIN'];
                $resultado [$i] ['USUA_NOMB'] = $rs->fields ['USUA_NOMB'];
                $resultado [$i] ['USUA_FECH_CREA'] = $rs->fields ['USUA_FECH_CREA'];
                $resultado [$i] ['USUA_NUEVO'] = $rs->fields ['USUA_NUEVO'];
                $resultado [$i] ['USUA_DOC'] = $rs->fields ['USUA_DOC'];
                $resultado [$i] ['USUA_NACIM'] = $rs->fields ['USUA_NACIM'];
                $resultado [$i] ['USUA_EMAIL'] = $rs->fields ['USUA_EMAIL'];
                $resultado [$i] ['USUA_ESTA'] = $rs->fields ['USUA_ESTA'];
                $resultado [$i] ['USUA_SESION'] = $rs->fields ['USUA_SESION'];
                $resultado [$i] ['CODI_NIVEL'] = $rs->fields ['CODI_NIVEL'];
                $resultado [$i] ['USUA_DEBUG'] = $rs->fields ['USUA_DEBUG'];
                $resultado [$i] ['USU_AT'] = $rs->fields ['USUA_AT'];
                $resultado [$i] ['USUA_CODI'] = $rs->fields ['USUA_CODI'];
                $resultado [$i] ['US_TELEFONO1'] = $rs->fields ['USUA_EXT'];
                $resultado [$i] ['LDAP'] = $rs->fields ['USUA_AUTH_LDAP'];
                $resultado [$i] ['USUA_LOGIN_EXTERNO'] = $rs->fields ['EXTERNO'];
                /* $resultado[$i]['USUA_VISTAGRAPHIP']=$rs->fields['USUA_VISTAGRAPHIP'];
                  $resultado[$i]['USUA_PAGES']=$rs->fields['USUA_PAGES']; */
                $i++;
                $rs->MoveNext();
            }
        } else {
            $retono ['Error'] = 'No contiene datos';
        }
//print_r($resultado);
        return $resultado;
    }

    /**
     * @return consulta los datos de usuario.
     */
    function buscarInfo($login, $nombre, $doc) {
        //$where=' where ';
	$where="";
        if ($login) {
            $where.=" upper(usuario.usua_login) like upper('%$login%') ";
        }
        if ($nombre) {
            if ($login)
                $where.=' and ';
            $where.=" upper(usuario.usua_nomb) like upper('%$nombre%') ";
        }
        if ($doc) {
            if ($nombre || $login)
                $where.=' and ';
            $where.="usuario.USUA_DOC like '%$doc%'";
        }



        //$sqlChar = "to_char(USUA_NACIM,'DD/MM/YYYY') as USUA_NACIM";
        $query = "SELECT
  sgd_urd_usuaroldep.usua_codi as ucodi,
  sgd_urd_usuaroldep.depe_codi as depcodi,
  sgd_urd_usuaroldep.rol_codi as idrol,
  dependencia.depe_nomb as nombdepe,
  sgd_rol_roles.sgd_rol_nombre as rolnomb,
  sgd_rol_roles.sgd_rol_estado as rolest,
  usuario.usua_login as USUA_LOGIN,
  usuario.usua_fech_crea as USUA_FECH_CREA ,
  usuario.usua_debug as USUA_DEBUG,
  usuario.usua_auth_ldap as USUA_AUTH_LDAP,
  usuario.usua_nomb as USUA_NOMB,
  usuario.usua_esta as USUA_ESTA,
  usuario.usua_codi as USUA_CODI ,
  usuario.usua_doc as USUA_DOC,
  usuario.USUA_LOGIN_EXTERNO,
  usuario.usua_nuevo,
  usuario.usua_at,
  usuario.usua_email,
  usuario.usua_nacim,
  usuario.usu_telefono1
FROM
  usuario,
  sgd_urd_usuaroldep,
  sgd_rol_roles,
  dependencia
WHERE
  usuario.usua_codi = sgd_urd_usuaroldep.usua_codi AND
  sgd_rol_roles.sgd_rol_id = sgd_urd_usuaroldep.rol_codi AND
  dependencia.depe_codi = sgd_urd_usuaroldep.depe_codi and $where ";
        $rs = $this->link->query($query);
        if (!$rs->EOF) {
            $i = 0;
            while (!$rs->EOF) {
                $resultado [$i] ['USUA_LOGIN'] = $rs->fields ['USUA_LOGIN'];
                $resultado [$i] ['USUA_NOMB'] = $rs->fields ['USUA_NOMB'];
                $resultado [$i] ['USUA_FECH_CREA'] = $rs->fields ['USUA_FECH_CREA'];
                $resultado [$i] ['USUA_NUEVO'] = $rs->fields ['USUA_NUEVO'];
                $resultado [$i] ['USUA_DOC'] = $rs->fields ['USUA_DOC'];
                $resultado [$i] ['ROLEST'] = $rs->fields ['ROLEST'];
                $resultado [$i] ['USUA_EMAIL'] = $rs->fields ['USUA_EMAIL'];
                $resultado [$i] ['USUA_ESTA'] = $rs->fields ['USUA_ESTA'];
                $resultado [$i] ['ROL_NOMBRE'] = $rs->fields ['ROLNOMB'];
                $resultado [$i] ['NOMBDEPE'] = $rs->fields ['NOMBDEPE'];
                $resultado [$i] ['USUA_DEBUG'] = $rs->fields ['USUA_DEBUG'];
                $resultado [$i] ['IDROL'] = $rs->fields ['IDROL'];
                $resultado [$i] ['ROLNOMB'] = $rs->fields ['ROLNOMB'];
                $resultado [$i] ['USUA_CODI'] = $rs->fields ['USUA_CODI'];
                $resultado [$i] ['DEPCODI'] = $rs->fields ['DEPCODI'];
                $resultado [$i] ['LDAP'] = $rs->fields ['USUA_AUTH_LDAP'];
                $resultado [$i] ['UCODI'] = $rs->fields ['UCODI'];
		$resultado [$i] ['USUA_NACIM'] = $rs->fields ['USUA_NACIM'];
                $resultado [$i] ['USUA_LOGIN_EXTERNO'] = $rs->fields['USUA_LOGIN_EXTERNO'];
		$resultado [$i] ['USUA_AT'] = $rs->fields['USUA_AT'];
		$resultado [$i] ['US_TELEFONO1'] = $rs->fields['USU_TELEFONO1'];
                /*  $resultado[$i]['USUA_PAGES']=$rs->fields['USUA_PAGES']; */
                $i++;
                $rs->MoveNext();
            }
        } else {
            $retono ['Error'] = 'No contiene datos';
        }

        return $resultado;
    }

    /**
     * @return consulta los datos de usuario.
     */
    function validaUsua($login, $doc) {
        $resultado = '';
        $where = ' where ';
        if ($login) {
            $where.=" upper(usua_login) = upper('$login') ";
        }
        if ($doc) {
            if ($login)
                $where.=' or ';
            $where.="USUA_DOC ='$doc'";
        }
        $query = " SELECT USUA_LOGIN,uSUA_DOC from usuario $where order by USUA_LOGIN";
        $rs = $this->link->query($query);
        if (!$rs->EOF) {
            if ($rs->fields ['USUA_DOC'] == $doc)
                $resultado = 'Numero de Documento';
            else
                $resultado = 'Login';
        }

        return $resultado;
    }

    /**
     * @return consulta las tres ultimas claves que ha registrado un en el sistema.
     */
    function claverepetida($codusuario) {
        /* Modify sentence for each database driver supported */
        switch($this->link->driver){
            case 'postgres':
                $query = "select id, usua_passw from user_security where usua_codi = $codusuario order by id desc limit 3";
                break;
            case 'oci8':
                $query = "select id, usua_passw from user_security where usua_codi = $codusuario and rownum<=3 order by id desc";
                break;
        }

        $rs = $this->link->query($query);
        if (!$rs->EOF) {
            $i = 0;
            while (!$rs->EOF) {
                $result [$i] = $rs->fields ['USUA_PASSW'];
                $i++;
                $rs->MoveNext();
            }
        } else {
            $result ['Error'] = 'No contiene datos';
        }

        return $result;
    }

    /**
     *
     * @param type $correo
     * @return type
     */
    function consultadatos($correo) {
        $query = "SELECT
  urd.usua_codi ucodi,   urd.depe_codi depcodi,
  urd.rol_codi idrol,   d.depe_nomb nombdepe,
  r.sgd_rol_nombre rolnomb,   r.sgd_rol_estado rolest,
  u.usua_login USUA_LOGIN,   u.usua_fech_crea USUA_FECH_CREA ,
  u.usua_debug USUA_DEBUG,   u.usua_auth_ldap USUA_AUTH_LDAP,
  u.usua_nomb USUA_NOMB,   u.usua_esta USUA_ESTA,
  u.usua_codi USUA_CODI ,   u.usua_doc USUA_DOC,u.USUA_EMAIL,u.USUA_NUEVO
FROM
  usuario u,
  sgd_urd_usuaroldep urd,
  sgd_rol_roles r,
  dependencia d
WHERE
  u.usua_codi = urd.usua_codi AND
  r.sgd_rol_id = urd.rol_codi AND
  d.depe_codi = urd.depe_codi and u.usua_email='$correo' ";
        $resultado = '';
        $rs = $this->link->query($query);
        if (!$rs->EOF) {
            $i = 0;
            while (!$rs->EOF) {
                $resultado [$i] ['USUA_LOGIN'] = $rs->fields ['USUA_LOGIN'];
                $resultado [$i] ['USUA_NOMB'] = $rs->fields ['USUA_NOMB'];
                $resultado [$i] ['USUA_FECH_CREA'] = $rs->fields ['USUA_FECH_CREA'];
                $resultado [$i] ['USUA_NUEVO'] = $rs->fields ['USUA_NUEVO'];
                $resultado [$i] ['USUA_DOC'] = $rs->fields ['USUA_DOC'];
                $resultado [$i] ['ROLEST'] = $rs->fields ['ROLEST'];
                $resultado [$i] ['USUA_EMAIL'] = $rs->fields ['USUA_EMAIL'];
                $resultado [$i] ['USUA_ESTA'] = $rs->fields ['USUA_ESTA'];

                $resultado [$i] ['NOMBDEPE'] = $rs->fields ['NOMBDEPE'];
                $resultado [$i] ['USUA_DEBUG'] = $rs->fields ['USUA_DEBUG'];
                $resultado [$i] ['IDROL'] = $rs->fields ['IDROL'];
                $resultado [$i] ['ROLNOMB'] = $rs->fields ['ROLNOMB'];
                $resultado [$i] ['USUA_CODI'] = $rs->fields ['USUA_CODI'];
                $resultado [$i] ['DEPCODI'] = $rs->fields ['DEPCODI'];
                $resultado [$i] ['LDAP'] = $rs->fields ['USUA_AUTH_LDAP'];
                $resultado [$i] ['UCODI'] = $rs->fields ['UCODI'];
                /* $resultado[$i]['USUA_VISTAGRAPHIP']=$rs->fields['USUA_VISTAGRAPHIP'];
                  $resultado[$i]['USUA_PAGES']=$rs->fields['USUA_PAGES']; */
                $i++;
                $rs->MoveNext();
            }
        } else {
            $retono ['Error'] = 'No contiene datos';
        }

        return $resultado;
    }

    /**
     * @return consulta los datos de usuario.
     */
    function consultadatosJefe($depe) {
        $query = "SELECT
  urd.usua_codi ucodi,   urd.depe_codi depcodi,
  urd.rol_codi idrol,   d.depe_nomb nombdepe,
  r.sgd_rol_nombre rolnomb,   r.sgd_rol_estado rolest,
  u.usua_login USUA_LOGIN,   u.usua_fech_crea USUA_FECH_CREA ,
  u.usua_debug USUA_DEBUG,   u.usua_auth_ldap USUA_AUTH_LDAP,
  u.usua_nomb USUA_NOMB,   u.usua_esta USUA_ESTA,
  u.usua_codi USUA_CODI ,   u.usua_doc USUA_DOC
FROM
  usuario u,
  sgd_urd_usuaroldep urd,
  sgd_rol_roles r,
  dependencia d
WHERE
  u.usua_codi = urd.usua_codi AND
  r.sgd_rol_id = urd.rol_codi AND r.sgd_rol_id=1 and
  d.depe_codi = urd.depe_codi and d.depe_codi='$depe' ";
        $resultado = '';
        $rs = $this->link->query($query);
        if (!$rs->EOF) {
            $i = 0;
            while (!$rs->EOF) {
                $resultado [$i] ['USUA_LOGIN'] = $rs->fields ['USUA_LOGIN'];
                $resultado [$i] ['USUA_NOMB'] = $rs->fields ['USUA_NOMB'];
                $resultado [$i] ['USUA_DOC'] = $rs->fields ['USUA_DOC'];
                $resultado [$i] ['ROLEST'] = $rs->fields ['ROLEST'];
                $resultado [$i] ['USUA_EMAIL'] = $rs->fields ['USUA_EMAIL'];
                $resultado [$i] ['USUA_ESTA'] = $rs->fields ['USUA_ESTA'];
                $resultado [$i] ['NOMBDEPE'] = $rs->fields ['NOMBDEPE'];
                $resultado [$i] ['USUA_DEBUG'] = $rs->fields ['USUA_DEBUG'];
                $resultado [$i] ['IDROL'] = $rs->fields ['IDROL'];
                $resultado [$i] ['ROLNOMB'] = $rs->fields ['ROLNOMB'];
                $resultado [$i] ['USUA_CODI'] = $rs->fields ['USUA_CODI'];
                $resultado [$i] ['DEPCODI'] = $rs->fields ['DEPCODI'];
                $resultado [$i] ['LDAP'] = $rs->fields ['USUA_AUTH_LDAP'];
                $resultado [$i] ['UCODI'] = $rs->fields ['UCODI'];
                /* $resultado[$i]['USUA_VISTAGRAPHIP']=$rs->fields['USUA_VISTAGRAPHIP'];
                  $resultado[$i]['USUA_PAGES']=$rs->fields['USUA_PAGES']; */
                $i++;
                $rs->MoveNext();
            }
        } else {
            $retono ['Error'] = 'No contiene datos';
        }

        return $resultado;
    }

}

?>
