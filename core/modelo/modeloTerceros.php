<?php 
/**
 * modeloTercero es la clase encargada de gestionar las operaciones y los datos basicos referentes a un dependencia
 * @author Hardy Deimont Niño  Velasquez
 * @name modeloDependencia
 * @version	1.0
 */
if (!$ruta_raiz)
    $ruta_raiz = '../../';
//include_once "$ruta_raiz/include/db/ConnectionHandler.php";
include_once "$ruta_raiz/core/clases/ConnectionHandler.php";

class modeloTerceros {

    public $link;

    function __construct($ruta_raiz) {
        $db = new ConnectionHandler("$ruta_raiz");
        $db->conn->SetFetchMode(ADODB_FETCH_NUM);
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        //$db->conn->debug=true;
        $this->link = $db;
    }

    /**
     * @return consulta los datos de sgd_ciu_ciudadano.
     *
     */
    function consultarCiu($codi = null, $nomb = null) {
        if ($codi)
            $where = " and sc.sgd_ciu_cedula='$codi'";
        if ($nomb) {
            $nomb2 = explode(' ', $nomb);
            foreach ($nomb2 as $key => $value) {
                $nomb3.="%$value";
            }
            $where .= " and upper(sc.sgd_ciu_nombre) like upper('$nomb3%')";
        }
        $q = "select sgd_ciu_codigo,sgd_ciu_nombre, sgd_ciu_apell1,sgd_ciu_apell2,sgd_ciu_telefono,sgd_ciu_email
		,sgd_ciu_estado,sgd_ciu_direccion,muni_nomb, dpto_nomb,sc.sgd_ciu_cedula
		from sgd_ciu_ciudadano sc,municipio m, departamento d
		where
		sc.dpto_codi=d.dpto_codi
		and m.dpto_codi=d.dpto_codi
		and sc.muni_codi=m.muni_codi
		$where order by sgd_ciu_nombre, sgd_ciu_apell1";
        $rs = $this->link->query($q);
        $retorno = array();
        $i = 0;
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                $retorno [$i]['depe_nomb'] = $rs->fields ['DPTO_NOMB'];
                $retorno [$i]['muni_nomb'] = $rs->fields ['MUNI_NOMB'];
                $retorno [$i]['codigo'] = $rs->fields ['SGD_CIU_CODIGO'];
                $retorno [$i]['nombre'] = $rs->fields ['SGD_CIU_NOMBRE'];
                $retorno [$i]['apellido'] = $rs->fields ['SGD_CIU_APELL1'];
                $retorno [$i]['apellido2'] = $rs->fields ['SGD_CIU_APELL2'];
                $retorno [$i]['telefono'] = $rs->fields ['SGD_CIU_TELEFONO'];
                $retorno [$i]['email'] = $rs->fields ['SGD_CIU_EMAIL'];
                $retorno [$i]['dir'] = $rs->fields ['SGD_CIU_DIRECCION'];
                $retorno [$i]['estado'] = $rs->fields ['SGD_CIU_ESTADO'];
                $retorno [$i]['cedula'] = $rs->fields ['SGD_CIU_CEDULA'];
                $i++;
                $rs->MoveNext();
            }
        } else {
            $retono ['Error'] = 'No se contienen datos';
        }

        return ($retorno);
    }

    /**
     * @return resultado de la operacion de actualizacion un sgd_ciu_ciudadano.
     * @param $nombre
     * @param $tipo
     * @param $id
     */
    function actualizarCiu($codigo, $estado = 1) {
        /* 	$recordWhere ['SGD_CIU_CODIGO'] = $codigo;
          echo $record ['SGD_CIU_ESTADO'] = $estado;
          $rs = $this->link->conn->update ( 'sgd_ciu_ciudadano', $record, $recordWhere );
         */
        $q = "update  sgd_ciu_ciudadano
		set  SGD_CIU_ESTADO = $estado
		where SGD_CIU_CODIGO = $codigo";
        $rs = $this->link->query($q);
        if (!$rs) {
            return 'ERROR';
        }
        return 'OK';
    }

    /**
     * @return consulta los datos de sgd_oem_oempresa
     *
     */
    function consultarOem($codi = null, $nomb = null) {
        if ($codi)
            $where = " and oem.sgd_oem_nit='$codi'";
        if ($nomb) {
            $nomb2 = explode(' ', $nomb);
            foreach ($nomb2 as $key => $value) {
                $nomb3.="%$value";
            }
            $where .= " and upper(oem.sgd_oem_oempresa) like upper('$nomb3%')";
        }
        $q = "SELECT
				oem.sgd_oem_codigo,
				oem.tdid_codi,
				oem.sgd_oem_oempresa,
				oem.sgd_oem_rep_legal,
				oem.sgd_oem_nit,
				oem.sgd_oem_sigla,
				oem.muni_codi,
				oem.dpto_codi,
				oem.sgd_oem_direccion,
				oem.sgd_oem_telefono,
				oem.id_cont,
				oem.id_pais,
				oem.sgd_oem_estado,
				m.muni_nomb,
				d.dpto_nomb
				FROM
				sgd_oem_oempresas oem , municipio m, departamento d
				where
				oem.dpto_codi=d.dpto_codi
				and m.dpto_codi=d.dpto_codi
				and oem.muni_codi=m.muni_codi
				$where order by oem.sgd_oem_oempresa";
        $rs = $this->link->query($q);
        $retorno = array();
        $i = 0;
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                $retorno [$i]['depe_nomb'] = $rs->fields ['DPTO_NOMB'];
                $retorno [$i]['muni_nomb'] = $rs->fields ['MUNI_NOMB'];
                $retorno [$i]['codigo'] = $rs->fields ['SGD_OEM_CODIGO'];
                $retorno [$i]['nombre'] = $rs->fields ['SGD_OEM_OEMPRESA'];
                $retorno [$i]['repre'] = $rs->fields ['SGD_OEM_REP_LEGAL'];
                $retorno [$i]['sigla'] = $rs->fields ['SGD_OEM_SIGLA'];
                $retorno [$i]['telefono'] = $rs->fields ['SGD_OEM_TELEFONO'];
                $retorno [$i]['dir'] = $rs->fields ['SGD_OEM_DIRECCION'];
                $retorno [$i]['estado'] = $rs->fields ['SGD_OEM_ESTADO'];
                $retorno [$i]['nit'] = $rs->fields ['SGD_OEM_NIT'];
                $i++;
                $rs->MoveNext();
            }
        } else {
            $retono ['Error'] = 'No se contienen datos';
        }

        return ($retorno);
    }

    /**
     * @return resultado de la operacion de actualizacion un sgd_oem_oempresa.
     * @param $nombre
     * @param $tipo
     * @param $id
     */
    function actualizarOem($codigo, $estado = 1) {
        $q = "update  sgd_oem_oempresas
		set  SGD_OEM_ESTADO = $estado
		where SGD_OEM_CODIGO = $codigo";
        $rs = $this->link->query($q);
        if (!$rs) {
            return 'ERROR';
        }
        return 'OK';
    }

}
?>
