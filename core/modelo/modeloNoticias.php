<?php /**
 * modeloSeries es la clase encargada de gestionar las operaciones y los datos basicos referentes a un series
 * @author Hardy Deimont Niño  Velasquez
 * @name modeloSeries
 * @version	1.0
 */
if (! $ruta_raiz)
	$ruta_raiz = '../../';
	//include_once "$ruta_raiz/include/db/ConnectionHandler.php";
include_once "$ruta_raiz/core/clases/ConnectionHandler.php";
class modeloNoticias {

	public $link;

	function __construct($ruta_raiz) {

		$db = new ConnectionHandler ( "$ruta_raiz" );
		$db->conn->SetFetchMode ( ADODB_FETCH_NUM );
		$db->conn->SetFetchMode ( ADODB_FETCH_ASSOC );
		$this->link = $db;
	}

	/**
	 * @return consulta los datos de noticia.
	 */
	function consultar() {
		$query = " select id as id,not_desc as descripcion,not_estado as estado,to_char(NOT_fech,'DD/MM/YYYY') as fecha
				   from not_noticias order by id asc";
		$rs = $this->link->query ( $query );
		$i = 0;
		while ( ! $rs->EOF ) {
			$combi [$i] ["CODIGO"] = $rs->fields ['ID'];
			$combi [$i] ["DESCRIP"] = $rs->fields ['DESCRIPCION'];
			$combi [$i] ["ESTADO"] = $rs->fields ['ESTADO'];
			$combi [$i] ["FECHA"] = $rs->fields ['FECHA'];
			$i ++;
			$rs->MoveNext ();
		}
		return $combi;

	}

	/**
	 * @return consulta los datos de noticia.
	 */
	function consultarE($id) {
	 	$query = " select id as id,not_desc as descripcion,not_estado as estado,to_char(NOT_fech,'DD/MM/YYYY') as fecha
				   from not_noticias where id= $id ";
		$rs = $this->link->query ( $query );
		$i = 0;

			$combi ["CODIGO"] = $rs->fields ['ID'];
			$combi ["DESCRIP"] = $rs->fields ['DESCRIPCION'];
			$combi ["ESTADO"] = $rs->fields ['ESTADO'];
			$combi ["FECHA"] = $rs->fields ['FECHA'];
			$i ++;

		return $combi;

	}
	/**
	 * @return consultaActual los datos de serie.
	 */
	function consultarActual() {
		/* Inicializa el arreglo que retorna la primera noticia */
		$combi = array();

		$query = " select id as id,not_desc as descripcion,not_estado as estado,to_char(NOT_fech,'DD/MM/YYYY') as fecha
				   from not_noticias where not_estado=1 order by id asc";

		$rs = $this->link->query ( $query );

		if(!$rs->EOF){
			$combi ["CODIGO"] = $rs->fields ['ID'];
			$combi ["DESCRIP"] = $rs->fields ['DESCRIPCION'];
			$combi ["ESTADO"] = $rs->fields ['ESTADO'];
			$combi ["FECHA"] = $rs->fields ['FECHA'];
		}

		return $combi;

	}
	/**
	 * @return consulta los datos de los Roles.
	 */
	function Buscar($id, $detaserie, $like = 0) {
		//echo $id,$detaserie;
	/*	if ($id != 0)
			$where = "where sgd_srd_codigo = '$id'";
		else if (strlen ( $detaserie ) > 0) {
			if ($like == 0)
				$where = "where upper(sgd_srd_descrip) = upper('$detaserie')";
			else
				$where = "where upper(sgd_srd_descrip) like upper('%$detaserie%')";
		}
		$query = "select SGD_SRD_CODIGO,SGD_SRD_DESCRIP,to_char(SGD_SRD_FECHFIN,'DD/MM/YYYY') SGD_SRD_FECHFIN ,to_char(SGD_SRD_FECHINI,'DD/MM/YYYY') SGD_SRD_FECHINI  from sgd_srd_seriesrd  $where ";
		$rs = $this->link->query ( $query );

		$i = 0;
		if (! $rs->EOF) {
			while ( ! $rs->EOF ) {
				$combi [$i] ["CODIGO"] = $rs->fields ['SGD_SRD_CODIGO'];
				$combi [$i] ["DESCRIP"] = $rs->fields ['SGD_SRD_DESCRIP'];
				$combi [$i] ["FECHINI"] = $rs->fields ['SGD_SRD_FECHINI'];
				$combi [$i] ["FECHFIN"] = $rs->fields ['SGD_SRD_FECHFIN'];
				$i ++;
				$combi ['ERROR'] = 'OK';
				$rs->MoveNext ();
			}

			return $combi;
		} else {
			return $combi ['ERROR'] = 'No se encontro  dato Buscado.';
		}*/

	}
	/**
	 * @return crea un Rol.
	 */
	function crear($descp) {
		//$fecha = $this->link->conn->DBDate ( date('d/m/Y')); $fecha,
        $query = "insert into not_noticias (not_desc,not_estado,not_fech)
					VALUES  ('$descp', 0,".$this->link->conn->DBDate(date('Y-m-d H:m:s')).")";

		$rs = $this->link->conn->Execute ( $query );
		if (! $rs) {
			return 'ERROR: No se realizo la creción de la noticia';
		}
		return 'Creación realizada con exito';
	}

	/**
	 * @return resultado de la operacion de actualizacion un Rol.
	 * @param $nombre
	 * @param $tipo
	 * @param $id
	 */
	function actualizar($codserieI,$descp) {

		$isqlUp = "update not_noticias
						set not_desc= '$descp'
						where id = $codserieI";

		$rs = $this->link->conn->Execute ( $isqlUp );
		if (! $rs) {
			return 'ERROR';
		}
		return "Modificaci&oacute;n de la Noticia realizada con exito";

	}

	/**
	 * @return resultado de la operacion de actualizacion un Rol.
	 * @param $nombre
	 * @param $tipo
	 * @param $id
	 */
	function actualizarEs($codserieI) {
		$isqlUp2 = "update not_noticias set	not_estado=0";
		$rs = $this->link->conn->Execute ( $isqlUp2 );
		$isqlUp = "update not_noticias set	not_estado=1 where id = $codserieI";

		$rs = $this->link->conn->Execute ( $isqlUp );
		if (! $rs) {
			return 'ERROR';
		}
		return "Noticia a mostrar $codserieI realizada con exito";

	}
}
?>