<?php 
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

if(!$ruta_raiz){
    $ruta_raiz="../..";
}
include_once "$ruta_raiz/core/clases/ConnectionHandler.php";

/**
 * Description of modeloAlertas
 *
 * @author derodriguez
 */
class modeloAlertas {
    //put your code here
    private $link;
    
    public function __construct($ruta_raiz) {
        $db = new ConnectionHandler ( "$ruta_raiz" );
        $db->conn->SetFetchMode ( ADODB_FETCH_NUM );
        $db->conn->SetFetchMode ( ADODB_FETCH_ASSOC );
        $this->link = $db;
        //$this->link->conn->debug=true;
    }
    
    function numVencidos($fecha_ini=null,$fecha_fin=null,$depe_codi=null){
	if($fecha_ini!=null && $fecha_fin!=null){
	    $fecha=" and r.radi_fech_radi between to_timestamp('$fecha_ini','dd/mm/yyyy hh24:mi:ss') and to_timestamp('$fecha_fin','dd/mm/yyyy hh24:mi:ss')";
	}
	else{
	    $fecha="";
	}
	if($depe_codi!=null){
	    $depe_busq=" and d.depe_codi=$depe_codi";
	}
	else{
	    $depe_busq="";
	}
        $ssql="select count(*) as Vencidos,d.depe_codi,d.depe_nomb from radicado r , sgd_tpr_tpdcumento t, dependencia d, usuario u 
            where tdoc_codi=sgd_tpr_codigo and d.depe_codi=r.radi_depe_actu and u.usua_codi=r.radi_usua_actu 
	    and d.depe_codi not in (998,999)
            and round((r.radi_fech_radi+(t.sgd_tpr_termino*7/5))-sysdate)+(select count(*) from  sgd_noh_nohabiles
            where NOH_FECHA between radi_fech_radi and current_timestamp)<=3 and u.usua_codi<>2 $fecha $depe_busq group by d.depe_codi, depe_nomb";
        $i=0;
        $resul['error']='';
        $rs=  $this->link->conn->Execute($ssql);
        if(!$rs->EOF){
            while(!$rs->EOF){
                $resul[$i]['vencidos']=$rs->fields['VENCIDOS'];
                $resul[$i]['numdep']=$rs->fields['DEPE_CODI'];
                $resul[$i]['nomdep']=$rs->fields['DEPE_NOMB'];
                $i++;
                $rs->MoveNext();
            }
        }else{
            $resul['error']='Error no hay documentos vencidos';
        }
        return $resul;
    }

    /*function numxVencer(){
        $ssql="select count(*) as porvencer,d.depe_codi,d.depe_nomb from radicado r , sgd_tpr_tpdcumento t, dependencia d, usuario u 
            where substr(cast(r.radi_nume_radi as char(18)),14,1)='2' and tdoc_codi=sgd_tpr_codigo and d.depe_codi=r.radi_depe_actu and u.usua_codi=r.radi_usua_actu 
	    and d.depe_codi=998
            and round((r.radi_fech_radi-(t.sgd_tpr_termino*7/5))-sysdate)+(select count(*) from  sgd_noh_nohabiles where 
            NOH_FECHA between radi_fech_radi and current_timestamp)in (1,2) and u.usua_codi<>2 group by d.depe_codi, depe_nomb";
        $rs=  $this->link->conn->Execute($ssql);
        $i=0;
        $resul['error']='';
        if(!$rs->EOF){
            while(!$rs->EOF){
                $resul[$i]['porvencer']=$rs->fields['PORVENCER'];
                $resul[$i]['numdep']=$rs->fields['DEPE_CODI'];
                $resul[$i]['nomdep']=$rs->fields['DEPE_NOMB'];
                $i++;
                $rs->MoveNext();
            }
        }else{
            $resul['error']='Error no hay documentos por vencerse';
        }
        return $resul;
    }*/
    
    function vencidoxDepe($depsel,$fecha_ini=null,$fecha_fin=null){
	if($fecha_ini!=null && $fecha_fin!=null){
            $fecha=" and r.radi_fech_radi between to_timestamp('$fecha_ini','dd/mm/yyyy hh24:mi:ss') and to_timestamp('$fecha_fin','dd/mm/yyyy hh24:mi:ss')";
        }
        else{
            $fecha="";
        }
        $ssql="select r.radi_nume_radi, u.usua_login,ra_asun,t.sgd_tpr_descrip,round((r.radi_fech_radi+(t.sgd_tpr_termino*7/5))-sysdate)+(select count(*) 
            from  sgd_noh_nohabiles where NOH_FECHA between radi_fech_radi and current_timestamp) as dias_vencidos, usua_nomb, u.usua_codi, tdoc_codi,
	    to_char(r.radi_fech_radi,'dd/mm/yyyy hh24:mi:ss') fecha from radicado r , 
            sgd_tpr_tpdcumento t, dependencia d,usuario u where tdoc_codi=sgd_tpr_codigo and d.depe_codi=r.radi_depe_actu
            and u.usua_codi=r.radi_usua_actu and round((r.radi_fech_radi+(t.sgd_tpr_termino*7/5))-sysdate)+(select count(*) from
            sgd_noh_nohabiles where NOH_FECHA between radi_fech_radi and current_timestamp)<=3 and d.depe_codi=$depsel $fecha order by u.usua_codi";
        $rs=  $this->link->conn->Execute($ssql);
        $i=0;
        while (!$rs->EOF){
            $result[$i]['numrad']=$rs->fields['RADI_NUME_RADI'];
            $result[$i]['diasven']=$rs->fields['DIAS_VENCIDOS'];
            $result[$i]['codus']=$rs->fields['USUA_CODI'];
            $result[$i]['nomus']=$rs->fields['USUA_NOMB'];
	    $result[$i]['login']=$rs->fields['USUA_LOGIN'];
	    $result[$i]['tipo_documental']=$rs->fields['SGD_TPR_DESCRIP'];
	    $result[$i]['fecha']=$rs->fields['FECHA'];
	    $result[$i]['trd']=$rs->fields['TDOC_CODI'];
	    $result[$i]['asunto']=$rs->fields['RA_ASUN'];
            $i++;
            $rs->MoveNext();
        }
        return $result;
    }
    
    /*function xvencerDepe($depsel){
        $ssql="select r.radi_nume_radi ,d.depe_nomb,r.ra_asun,round((r.radi_fech_radi-(t.sgd_tpr_termino*7/5))-sysdate)+(select count(*) from
            sgd_noh_nohabiles where NOH_FECHA between radi_fech_radi and current_timestamp) as dias_avencer, u.usua_codi, usua_nomb, tdoc_codi from radicado r , sgd_tpr_tpdcumento t,
            dependencia d,usuario u where substr(cast(r.radi_nume_radi as char(18)),14,1)='2' and tdoc_codi=sgd_tpr_codigo and d.depe_codi=r.radi_depe_actu and 
            u.usua_codi=r.radi_usua_actu and round((r.radi_fech_radi-(t.sgd_tpr_termino*7/5))-sysdate)+(select count(*) from
            sgd_noh_nohabiles where NOH_FECHA between radi_fech_radi and current_timestamp) in (1,2,3) and d.depe_codi=$depsel order by usua_codi";
        $rs=  $this->link->conn->Execute($ssql);
        $i=0;
        while (!$rs->EOF){
            $result[$i]['numrad']=$rs->fields['RADI_NUME_RADI'];
            $result[$i]['diasaven']=$rs->fields['DIAS_AVENCER'];
            $result[$i]['codus']=$rs->fields['USUA_CODI'];
            $result[$i]['nomus']=$rs->fields['USUA_NOMB'];
	    $result[$i]['trd']=$rs->fields['TDOC_CODI'];
	    $result[$i]['asunto']=$rs->fields['RA_ASUN'];
            $i++;
            $rs->MoveNext();
        }
        return $result;
    }*/
    
    function jefexDep($depsel){
        $ssql="select u.usua_codi  from dependencia d, usuario u, sgd_rol_roles r, sgd_urd_usuaroldep urd where u.usua_codi=urd.usua_codi
            and d.depe_codi=urd.depe_codi and r.sgd_rol_id=urd.rol_codi and d.depe_codi=$depsel and r.sgd_rol_id=1";
        $rs=  $this->link->conn->Execute($ssql);
        $codus=$rs->fields['USUA_CODI'];
        return $codus;
    }
    
    function dataUsua(){
        $ssql="select usua_codi,usua_nomb,case when usua_email='' or usua_email is NULL then 'No tiene correo' else usua_email end as email 
            from usuario";
        $rs=  $this->link->conn->Execute($ssql);
        while(!$rs->EOF){
            $i=$rs->fields['USUA_CODI'];
            $data[$i]['usua_nomb']=$rs->fields['USUA_NOMB'];
            $data[$i]['email']=$rs->fields['EMAIL'];
            $rs->MoveNext();
        }
        return $data;
    }
}

?>
