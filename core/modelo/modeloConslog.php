<?php 
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
if(!$ruta_raiz){
    $ruta_raiz="../..";
}
include_once "$ruta_raiz/core/clases/ConnectionHandler.php";

class modeloConslog{
    var $link;
    
    function __construct($ruta_raiz) {
        $db = new ConnectionHandler ( "$ruta_raiz" );
        $db->conn->SetFetchMode ( ADODB_FETCH_NUM );
        $db->conn->SetFetchMode ( ADODB_FETCH_ASSOC );
        $this->link = $db;
        //$this->link->conn->debug=true;
    }
    
    function consUsua(){
        $sql="select usua_codi, usua_login from usuario  order by usua_codi";
        $rs=$this->link->conn->Execute($sql);
        while(!$rs->EOF){
            $i=$rs->fields['USUA_CODI'];
            $resul[$i]=$rs->fields['USUA_LOGIN'];
            $rs->MoveNext();
        }
        return $resul;
    }
    
    function consDepe(){
        $sql="select depe_codi,depe_nomb from dependencia";
        $rs=  $this->link->conn->Execute($sql);
        while(!$rs->EOF){
            $i=$rs->fields['DEPE_CODI'];
            $resul[$i]=$rs->fields['DEPE_NOMB'];
            $rs->MoveNext();
        }
        return $resul;
    }
    
    function consRol(){
        $sql="select sgd_rol_id as rol_id, sgd_rol_nombre as rol_nomb from sgd_rol_roles";
        $rs=  $this->link->conn->Execute($sql);
        while(!$rs->EOF){
            $i=$rs->fields['ROL_ID'];
            $resul[$i]=$rs->fields['ROL_NOMB'];
            $rs->MoveNext();
        }
        return $resul;
    }
    
    function usuaConsDep($depsel){
        if($depsel!=1000){
            $sql="select u.usua_codi, usua_login||' - '||u.usua_nomb as usua_nomb from usuario u, sgd_urd_usuaroldep urd where u.usua_codi=urd.usua_codi and 
            urd.depe_codi=$depsel order by usua_login";
            $rs=  $this->link->conn->Execute($sql);
            $i=0;
            $resul['error']="";
            if(!$rs->EOF){
                while(!$rs->EOF){
                    $resul[$i]['codus']=$rs->fields['USUA_CODI'];
                    $resul[$i]['nomus']=$rs->fields['USUA_NOMB'];
                    $rs->MoveNext();
                    $i++;
                }
            }else{
                $resul['error']="No nay datos para esta operaci&oacute;n";
            }
        }else{
            $sql="select usua_codi, usua_nomb from usuario where usua_codi not in (select usua_codi 
                from sgd_urd_usuaroldep)";
            $rs =$this->link->conn->Execute($sql);
            $i=0;
            $resul['error']="";
            if(!$rs->EOF){
                while(!$rs->EOF){
                    $resul[$i]['codus']=$rs->fields['USUA_CODI'];
                    $resul[$i]['nomus']=$rs->fields['USUA_NOMB'];
                    $rs->MoveNext();
                    $i++;
                }
            }else{
                $resul['error']="No nay datos para esta operaci&oacute;n";
            }
        }
        
        return $resul;
    }
    /*function usuasinDepen
     * Funci&oactue;n que evalua usuarios deselazados, que no tienen dependencia especifica
     * 
     * @return array $resul Arreglo con resultado de todos usuarios desenlazados
     * 
     */
    function usuasinDepen(){
        $sql="select usua_codi from usuario where usua_codi not in (select usua_codi 
            from sgd_urd_usuaroldep)";
        $rs=  $this->link->conn->Execute($sql);
        $i=0;
        if(!$rs->EOF){
            while(!$rs->EOF){
                $resul[$i]=$rs->fields['USUA_CODI'];
                $rs->MoveNext();
            }
        }else{
            $resul="Esta consulta no tiene datos";
        }
    }
    
    

}

?>
