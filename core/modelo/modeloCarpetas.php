<?php
/**
 * modeloCarpetas es la clase encargada de gestionar las operaciones y los datos basicos referentes a un carpetas
 * @author Hardy Deimont Niño  Velasquez
 * @name modeloCarpetas
 * @version	1.0
 */
if (! $ruta_raiz)
	$ruta_raiz = '../../';
	//include_once "$ruta_raiz/include/db/ConnectionHandler.php";
include_once "$ruta_raiz/core/clases/ConnectionHandler.php";

class modeloCarpetas {

	public $link;

	function __construct($ruta_raiz) {
		$db = new ConnectionHandler ( "$ruta_raiz" );
		$db->conn->SetFetchMode ( ADODB_FETCH_NUM );
		$db->conn->SetFetchMode ( ADODB_FETCH_ASSOC );
		if($_SESSION['usua_debug']==2)
		$db->conn->debug=true;
		$this->link = $db;
	}


	/**
	 * @return resultado de la operacion obtienw los  datos requerido para el  usuario  en carpetas.
	 *
	 */
	function carpetas() {
		// Esta consulta selecciona las carpetas Basicas de DocuImage que son extraidas de la tabla Carp_Codi
  	       $query="select CARP_CODI,CARP_DESC from carpeta order by carp_codi ";
		$rs = $this->link->conn->Execute ( $query );
		$varQuery = $query;
		//include "$ruta_raiz/include/tx/ComentarioTx.php";
		$i = 1;
		$carpetas = array ();
		while ( ! $rs->EOF ) {
			$carpetas[$i]['CARP_CODI'] = $rs->fields ["CARP_CODI"];
			$carpetas[$i]['CARP_DESC'] = $rs->fields ["CARP_DESC"];
			$i ++;
			$rs->MoveNext ();
		}

		return $carpetas;

	}
	/**
	 * @return resultado de la operacion obtienw los  datos requerido para el  usuario  en carpetas personales.
	 *
	 */
	function carpetasper($rolcodi,$dependencia,$usuacodi) {
		// Esta consulta selecciona las carpetas Basicas de DocuImage que son extraidas de la tabla Carp_Codi
  	   /* $query="select CODI_CARP,DESC_CARP,NOMB_CARP from carpeta_per
  	    where usua_codi=$usuacodi and id_rol = $rolcodi and depe_codi=$dependencia order by codi_carp  ";*/
     //carp_per=1 and carp_codi
            $query="select  distinct CODI_CARP,DESC_CARP,NOMB_CARP,(SELECT count(1) as CUENTA  FROM   radicado r
            WHERE  r.carp_per=1 and carp_codi=   CODI_CARP AND   r.radi_usua_actu =$usuacodi AND
            r.radi_depe_actu =$dependencia) as cuenta from carpeta_per where usua_codi=$usuacodi and depe_codi=$dependencia order by nomb_carp ";
		$rs = $this->link->conn->Execute ( $query );
		//$varQuery = $query;
		//include "$ruta_raiz/include/tx/ComentarioTx.php";
		$i = 1;
		$carpetas = array ();
		while ( ! $rs->EOF ) {
			$carpetas[$i]['CARP_CODI'] = $rs->fields ["CODI_CARP"];
			$carpetas[$i]['NOMB_CARP'] = $rs->fields ["NOMB_CARP"];
			$carpetas[$i]['DESC_CARP'] = $rs->fields ["DESC_CARP"];
                        $carpetas[$i]['CUENTA'] = $rs->fields ["CUENTA"];
			$i ++;
			$rs->MoveNext ();
		}

		return $carpetas;

	}
	/**
	 * @return resultado de la operacion obtienw los  datos requerido para el  usuario  en carpetas personales.
	 *
	 */
	function carpetasperMasiva($rolcodi,$dependencia,$usuacodi) {
		// Esta consulta selecciona las carpetas Basicas de DocuImage que son extraidas de la tabla Carp_Codi
  	   /* $query="select CODI_CARP,DESC_CARP,NOMB_CARP from carpeta_per
  	    where usua_codi=$usuacodi and id_rol = $rolcodi and depe_codi=$dependencia order by codi_carp  ";*/
     //carp_per=1 and carp_codi
            $query="select codi_carp from carpeta_per  where id_rol=$rolcodi and depe_codi=$dependencia and usua_codi=$usuacodi and codi_carp=5";
		$rs = $this->link->conn->Execute ( $query );
		//$varQuery = $query;
		//include "$ruta_raiz/include/tx/ComentarioTx.php";

		$carpetas = array ();
		if ( ! $rs->EOF ) {
			$carpetas['CARP_CODI'] = $rs->fields ["CODI_CARP"];
		}

		return $carpetas;

	}
	/**
	 *
	 */
	function carpetasCuenta($dependencia,$rolcodi,$usua_doc,$usuacod) {
		 $query="select carp_desc as CARP_DESC,count(carp_desc) as CUENTA from carpeta c, radicado r where c.carp_codi<>11 and
		r.carp_codi=c.carp_codi and  r.radi_depe_actu=$dependencia
						and r.radi_usua_actu=$usuacod and r.carp_per=0 group by c.carp_desc";

		$rs = $this->link->conn->Execute ( $query );
		$i=0;
		while ( ! $rs->EOF ) {
			$modulo[$i]["CARP_DESC"] = $rs->fields ["CARP_DESC"];
			$modulo[$i]["CUENTA"] = $rs->fields ["CUENTA"];
			$rs->MoveNext ();
			$i++;
		}
 			return $modulo;
	}
	function Agendado($usua_doc) {
	$sqlFechaHoy=$this->link->conn->DBTimeStamp(time());
	//$db->conn->debug = true;
	$sqlAgendado=" and (agen.SGD_AGEN_FECHPLAZO >= ".$sqlFechaHoy.")";
	$data="Agendados no vencidos";
    $query="select count(*) as CONTADOR from SGD_AGEN_AGENDADOS agen
	where  usua_doc='$usua_doc' and agen.SGD_AGEN_ACTIVO=1 $sqlAgendado";
		$rs = $this->link->conn->Execute ( $query );
			$modulo["CARP_DESC"] = 'Agendado';
			$modulo["CUENTA"] = $rs->fields ["CONTADOR"];
 			return $modulo;
	}

function AgenVencido($usua_doc) {

		$sqlFechaHoy=$this->link->conn->DBTimeStamp(time());
	//$db->conn->debug = true;
	$sqlAgendado=" and (agen.SGD_AGEN_FECHPLAZO <= ".$sqlFechaHoy.")";
	$data="Agendados no vencidos";
    $query="select count(*) as CONTADOR from SGD_AGEN_AGENDADOS agen
	where  usua_doc='$usua_doc' and agen.SGD_AGEN_ACTIVO=1 $sqlAgendado";
		$rs = $this->link->conn->Execute ( $query );
			$modulo["CARP_DESC"] = 'Agen. Vencido';
			$modulo["CUENTA"] = $rs->fields ["CONTADOR"];
 			return $modulo;
	}

function informados($dependencia,$id_rol,$ucodi) {
	 	$query="select count(*) as CONTADOR from informados where depe_codi=$dependencia and usua_codi=$ucodi";
		$rs = $this->link->conn->Execute ( $query );
			$modulo["CARP_DESC"] = 'Informados';
			$modulo["CUENTA"] = $rs->fields ["CONTADOR"]+0;
 			return $modulo;
		}

function VistoBueno($dependencia,$rolcodi,$login,$ucodi) {

    $query="select count(1) as CONTADOR from radicado
        where carp_per=0 and carp_codi=11
        and  radi_depe_actu=$dependencia
        and radi_usua_actu=$ucodi
        ";

    $rs = $this->link->conn->Execute ( $query );
    $modulo["CODI"] = 11;
    $modulo["CUENTA"] = $rs->fields ["CONTADOR"]+0;
    return $modulo;
}

function crearCarpMasiva($rolcodi,$dependencia,$usuacodi) {
	//$query="select codi_carp from carpeta_per  where id_rol=$dependencia and depe_codi=$dependencia and usua_codi=$usuacodi and codi_carp=5";
     $query="insert into  carpeta_per  (codi_carp,id_rol,depe_codi, usua_codi,nomb_carp,desc_carp) values(5,$rolcodi,$dependencia,$usuacodi,'Masivas','Carpetas Masivas')";
		$rs = $this->link->conn->Execute ( $query );
	;
}
}

?>
