<?php /** 
 * modelosRoles es la clase encargada de gestionar las operaciones y los datos basicos referentes a un rol
 * @author Hardy Deimont Niño  Velasquez
 * @name modeloRoles
 * @version	1.0
 */ 
if(!$ruta_raiz) $ruta_raiz='../../';
//include_once "$ruta_raiz/include/db/ConnectionHandler.php";
include_once "$ruta_raiz/core/clases/ConnectionHandler.php";
class modeloRoles {

	public $link;
	
	function __construct($ruta_raiz)  
	{  
		$db = new ConnectionHandler("$ruta_raiz");
		$db->conn->SetFetchMode(ADODB_FETCH_NUM);
		$db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
	    $this->link=$db;   
	}   
	
	/**
	 * @return consulta los datos de rol.
	 * @param $idrol
	 */
	function consultar($idrol) 
	{
		 $query=" SELECT * from sgd_rol_roles where sgd_rol_id ='$idrol'";
		 return $this->link->query($query);		 
	}
	/**
	 * @return consulta los datos de los Roles.
	 */
	function consultarTodo()
	{
		 $query="SELECT * from sgd_rol_roles ";
		 $rs=$this->link->query($query);
				 $i=0;
		while ( !$rs->EOF ) {
				$combi [$i] ["ID_ROL"] =$rs->fields ['SGD_ROL_ID'];
	 			$combi [$i] ["NOMBRE"] =$rs->fields ['SGD_ROL_NOMBRE'];
	 			$combi [$i] ["ESTADO"] = $rs->fields ['SGD_ROL_ESTADO'];
				$i++;
				$rs->MoveNext();
		}
		return $combi;
	}
	/**
	 * @return crea un Rol.
	 */
	function crear($nombre,$estado) {
		$query1 = "select max(sgd_rol_id) as id  from sgd_rol_roles";
		$rs1=$this->link->conn->Execute($query1);
		$idrol=$rs1->fields["ID"] +1;
		$query = "INSERT INTO sgd_rol_roles (sgd_rol_id,sgd_rol_nombre, sgd_rol_estado) VALUES ($idrol,'$nombre','$estado')";
	 	$rs=$this->link->conn->Execute($query);	
			if (!$rs){
		   return 'ERROR';
	    }	
		return 'OK';	 
	}
	
	/**
	 * @return resultado de la operacion de actualizacion un Rol.
	 * @param $nombre
	 * @param $tipo
	 * @param $id
	 */
	function actualizar($nombre,$tipo,$id)
	{
	
	    $query = "UPDATE sgd_rol_roles SET sgd_rol_nombre= '$nombre', sgd_rol_estado = '$tipo' WHERE sgd_rol_id = ".$id;
	 	$rs=$this->link->conn->Execute($query);
       	if (!$rs){
		   return 'ERROR';
	    }	
		return 'OK';
	}
}
?>