<?php
/**
 * modeloCarpetas es la clase encargada de gestionar las operaciones y los datos basicos referentes a un carpetas
 * @author Hardy Deimont Niño  Velasquez
 * @name modeloCarpetas
 * @version	1.0
 */
if (! $ruta_raiz)
	$ruta_raiz = '../../';
	//include_once "$ruta_raiz/include/db/ConnectionHandler.php";
include_once "$ruta_raiz/core/clases/ConnectionHandler.php";

class modeloMenu {

	public $link;

	function __construct($ruta_raiz) {
		$db = new ConnectionHandler ( "$ruta_raiz" );
		$db->conn->SetFetchMode ( ADODB_FETCH_NUM );
		$db->conn->SetFetchMode ( ADODB_FETCH_ASSOC );
		if($_SESSION['usua_debug']==2)
		$db->conn->debug=true;
		$this->link = $db;
	}


	/**
	 * @return resultado de la operacion obtienw los  datos requerido para el  usuario  en carpetas.
	 *
	 */
	function consultar() {
		// Esta consulta selecciona las carpetas Basicas de DocuImage que son extraidas de la tabla Carp_Codi
  	    $query="select sgd_men_nombre as nomb,sgd_men_perm as permiso,sgd_men_permvalor as valor, sgd_men_categoria as cat, sgd_men_url as url from sgd_men_menu where sgd_men_codigo<>11 order by sgd_men_nombre asc";

		$rs = $this->link->conn->Execute ( $query );
		$varQuery = $query;
		//include "$ruta_raiz/include/tx/ComentarioTx.php";
		$i = 1;
		$carpetas = array ();
		while ( ! $rs->EOF ) {
			$carpetas[$i]['NOMBRE'] = trim($rs->fields ["NOMB"]);
			$carpetas[$i]['PERMISO'] = trim($rs->fields ["PERMISO"]);
			$carpetas[$i]['VALOR'] = trim($rs->fields ["VALOR"]);
			$carpetas[$i]['CAT'] = trim($rs->fields ["CAT"]);
			$carpetas[$i]['URL'] = trim($rs->fields ["URL"]);
			$i ++;
			$rs->MoveNext ();
		}

		return $carpetas;

	}
}
?>
