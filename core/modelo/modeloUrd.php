<?php /** 
 * @author Hardy Deimont Niño  Velasquez
 * 
 * 
 */
//error_reporting( ~E_NOTICE); 
if (! $ruta_raiz)
	$ruta_raiz = '../..';
	//include_once "$ruta_raiz/include/db/ConnectionHandler.php";
include_once "$ruta_raiz/core/clases/ConnectionHandler.php";
class modeloUrd {
	
	public $link;
	
	function __construct($ruta_raiz) {
		$db = new ConnectionHandler ( "$ruta_raiz" );
		$db->conn->SetFetchMode ( ADODB_FETCH_NUM );
		$db->conn->SetFetchMode ( ADODB_FETCH_ASSOC );
		$this->link = $db;
	}
	
	/**
	 * @return consulta los datos de usuario.
	 */
	function depe_usu_rol($depe_codi) {
		$query = "select r.sgd_rol_nombre, r.sgd_rol_id,u.usua_login,u.usua_nomb,u.usua_codi from sgd_rol_roles r ,sgd_urd_usuaroldep urd,  usuario u
where urd.usua_codi=u.usua_codi and r.sgd_rol_id=urd.rol_codi and urd.depe_codi=$depe_codi
order by  u.usua_login asc";
		if (! $rs->EOF) {
			$resultado ['Error'] = '0';
			$i=0;
			$rs = $this->link->query ( $query );
			while ( ! $rs->EOF ) {
				$resultado [$i]['ROL_NOMB'] = $rs->fields ['SGD_ROL_NOMBRE'];
				$resultado [$i]['ID_ROL'] = $rs->fields ['SGD_ROL_ID'];
				$resultado [$i]['LOGIN'] = $rs->fields ['USUA_LOGIN'];
				$resultado [$i]['USUA_NOMB'] = $rs->fields ['USUA_NOMB'];
				$resultado [$i]['USUA_CODI'] = $rs->fields ['USUA_CODI'];
				$i ++;
				$rs->MoveNext ();
			}
		
		} else {
			$resultado ['Error'] = 'No se contienen datos';
		
		}
		
		return $resultado;
	}

	/**
	 * @return consulta los datos de usuario.
	 */
	function CosultaUsuaLibres() {
		$query = "select usua_codi,usua_login,usua_nomb from usuario where usua_codi not in (select usua_codi from sgd_urd_usuaroldep urd)";
		
			$i=0;
			$rs = $this->link->query ( $query );
			if (! $rs->EOF) {
			$resultado ['Error'] = '0';
			while ( ! $rs->EOF ) {
				$resultado [$i]['LOGIN'] = $rs->fields ['USUA_LOGIN'];
				$resultado [$i]['USUA_NOMB'] = $rs->fields ['USUA_NOMB'];
				$resultado [$i]['USUA_CODI'] = $rs->fields ['USUA_CODI'];
				$i ++;
				$rs->MoveNext ();
			}
		
		} else {
			$resultado ['Error'] = 'No se contienen datos';
		
		}
		
		return $resultado;
	}
	
	
	
/**
	 * @return consulta los datos rol vs dependencia.
	 */
	function CosultaRolxDep($dep) {
		 $query = "SELECT DISTINCT  sgd_rol_id,   sgd_rol_nombre FROM   dependencia,  sgd_rol_roles,  sgd_drm_dep_mod_rol
WHERE   depe_codi = sgd_drm_depecodi AND  sgd_drm_rolcodi = sgd_rol_id AND  depe_codi = $dep order by sgd_rol_nombre";	
		 $sqls="select * from sgd_urd_usuaroldep where  rol_codi =1 AND depe_codi = $dep ";	
			$i=0;
			$rs2 = $this->link->query ( $sqls );
			$rolno=0;
			if (!$rs2->EOF) {
				$rolno=1;
			}
			$rs = $this->link->query ( $query );
			if (! $rs->EOF) {
			$resultado ['Error'] = '0';
			while ( ! $rs->EOF ) {
				if($rolno!=$rs->fields ['SGD_ROL_ID']){
				  $resultado [$i]['IDROL'] = $rs->fields ['SGD_ROL_ID'];
				 $resultado [$i]['ROL_NOMB'] = $rs->fields ['SGD_ROL_NOMBRE'];
				}
				$i ++;
				$rs->MoveNext ();
			}
		
		} else {
			$resultado ['Error'] = 'No se contienen datos';
		
		}
		
		return $resultado;
	}
/**
	 * @return consulta los datos de usuario.
	 */
	function delUrd($dependecia,$rol,$codusuario) {
		$query = "delete from sgd_urd_usuaroldep where depe_codi=$dependecia and rol_codi=$rol and usua_codi=$codusuario";
			$rs = $this->link->query ( $query );
		//if (! $rs->EOF) {
		    $resultado  ='Operaci&oacute;n realizada correctamente';
		//} else {
		//	$resultado = 'Operaci&oacute;n no  ejecutada';
		
	//	}
		
		return $resultado;
	}
/**
	 * @return validar los datos de usuario.
	 */
	function validDelUrd($dependecia,$rol,$codusuario,$usuadoc) {
		$query = "select count(*) contador from radicado where radi_usua_actu=$codusuario ";
			$rs = $this->link->query ( $query );
			$resultado='';
		if ($rs->fields ['CONTADOR']>0) {
		    $resultado=$rs->fields ['CONTADOR']." Radicados ";
		}
		
		return $resultado;
	}	
	
	
/**
	 * @return consulta los datos de usuario.
	 */
	function addUrd($dependecia,$rol,$codusuario) {
		$query = "insert into sgd_urd_usuaroldep (depe_codi,rol_codi,usua_codi) values ( $dependecia,$rol ,$codusuario)";
			$rs = $this->link->query ( $query );
		if (! $rs->EOF) {
		    $resultado ['Error'] ='';
		} else {
			$resultado ['Error'] = 'No se contienen datos';
		
		}
		
		return $resultado;
	}	
}

?>