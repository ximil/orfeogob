<?php 
/**
 * modeloCarpetas es la clase encargada de gestionar las operaciones y los datos basicos referentes a un carpetas
 * @author Hardy Deimont Niño  Velasquez
 * @name modeloCarpetas
 * @version	1.0
 */
if (!$ruta_raiz)
    $ruta_raiz = '../../';
//include_once "$ruta_raiz/include/db/ConnectionHandler.php";
include_once "$ruta_raiz/core/clases/ConnectionHandler.php";

class modeloPlantillas {

    public $link;

    function __construct($ruta_raiz) {
        $db = new ConnectionHandler("$ruta_raiz");
        $db->conn->SetFetchMode(ADODB_FETCH_NUM);
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        if ($_SESSION['usua_debug'] == 2)
            $db->conn->debug = true;
        $this->link = $db;
    }

    /**
     * @return resultado de la operacion obtienw los  datos requerido para el  usuario  en carpetas.
     *  
     */
    function listar() {
        // Esta consulta selecciona las carpetas Basicas de DocuImage que son extraidas de la tabla Carp_Codi
        $query = "select * from  plantilla_pl ";
        $rs = $this->link->conn->Execute($query);
        $varQuery = $query;
        //include "$ruta_raiz/include/tx/ComentarioTx.php";
        $i = 1;
        $carpetas = array();
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                foreach ($rs->fields as $key => $value) {
                    $resp[$i][$key] = $value;
                }
                $i ++;
                $rs->MoveNext();
            }
        }

        return $resp;
    }
   /**
     * @return resultado de la operacion obtienw los  datos requerido para el  usuario  en carpetas.
     *  
     */
function listardta() {
        // Esta consulta selecciona las carpetas Basicas de DocuImage que son extraidas de la tabla Carp_Codi
        $query = "select * from  plantilla_pl where pl_uso=1 ";
        $rs = $this->link->conn->Execute($query);
        $varQuery = $query;
        //include "$ruta_raiz/include/tx/ComentarioTx.php";
        $i = 1;
        $carpetas = array();
        if (!$rs->EOF) {
            while (!$rs->EOF) {
                foreach ($rs->fields as $key => $value) {
                    $resp[$i][$key] = $value;
                }
                $i ++;
                $rs->MoveNext();
            }
        }

        return $resp;
    }
    function maxcodi() {
        // Esta consulta selecciona las carpetas Basicas de DocuImage que son extraidas de la tabla Carp_Codi
        $query = "select max(pl_codi) codi from  plantilla_pl ";
        $rs = $this->link->conn->Execute($query);
        $varQuery = $query;
        //include "$ruta_raiz/include/tx/ComentarioTx.php";
        $i = 1;
     //   print_r($rs);
        $carpetas = array();
        if (!$rs->EOF) {
            foreach ($rs->fields as $key => $value) {
                    $resp = $value;
                }
            //$resp=$rs->fields['CODI'];
            
        }else{ $resp=0; }

        return $resp;
    }

    function crear($nomb, $usua_codi, $depe) {
        $codigo=$this->maxcodi()+1;
        $archivo='plantilla'.$codigo.'.docx';
        //$query="select codi_carp from carpeta_per  where id_rol=$dependencia and depe_codi=$dependencia and usua_codi=$usuacodi and codi_carp=5";
        $query = "insert into  plantilla_pl  (pl_codi,pl_nomb, pl_archivo,pl_fech,usua_codi,depe_codi,pl_uso)"
                . "values($codigo,'$nomb','$archivo',current_date,$usua_codi,$depe,1)";
        $rs = $this->link->conn->Execute($query);
        return $archivo;
    }
    function eliminar($id) {
        
        $data=$this->consultarUni($id);
        //$query="select codi_carp from carpeta_per  where id_rol=$dependencia and depe_codi=$dependencia and usua_codi=$usuacodi and codi_carp=5";
        $query = "delete from plantilla_pl  where pl_codi=$id";
        $rs = $this->link->conn->Execute($query);
        return $data['PL_ARCHIVO'];
    }

    function modificar($id, $estado) {
        //$query="select codi_carp from carpeta_per  where id_rol=$dependencia and depe_codi=$dependencia and usua_codi=$usuacodi and codi_carp=5";
        $query = "update plantilla_pl set pl_uso=$estado where pl_codi=$id  ";
        $rs = $this->link->conn->Execute($query);
        ;
    }
  function consultarUni($id) {
        // Esta consulta selecciona las carpetas Basicas de DocuImage que son extraidas de la tabla Carp_Codi
        $query = "select * from  plantilla_pl where pl_codi=$id ";
        $rs = $this->link->conn->Execute($query);
        $varQuery = $query;
        //include "$ruta_raiz/include/tx/ComentarioTx.php";
        $i = 1;
        $carpetas = array();
        if (!$rs->EOF) {
            
                foreach ($rs->fields as $key => $value) {
                    $resp[$key] = $value;
                }
                $i ++;
                $rs->MoveNext();
            
        }

        return $resp;
    }
}

?>