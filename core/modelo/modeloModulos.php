<?php 
/** 
 * modeloModulos es la clase encargada de gestionar las operaciones y los datos basicos referentes a un modulo
 * @author Hardy Deimont Niño  Velasquez
 * @name modeloModulos
 * @version	1.0
 */
if (! $ruta_raiz)
	$ruta_raiz = '../../';
//include_once "$ruta_raiz/include/db/ConnectionHandler.php";
include_once "$ruta_raiz/core/clases/ConnectionHandler.php";

class modeloRoles {
	
	public $link;
	
	function __construct($ruta_raiz) {
		$db = new ConnectionHandler ( "$ruta_raiz" );
		$db->conn->SetFetchMode ( ADODB_FETCH_NUM );
		$db->conn->SetFetchMode ( ADODB_FETCH_ASSOC );
		$this->link = $db;
	}
	
	/**
	 * @return consulta los datos de modulo.
	 * @param $idmod
	 */
	function consultar($idmod) {
		
		$query = "SELECT * FROM sgd_mod_modules where SGD_MOD_ID=$idmod";
		$rs = $this->link->query ( $query );
		if (! $rs->EOF) {
			$combi ["SGD_MOD_ID"] = $rs->fields ['SGD_MOD_ID'];
			$combi ["SGD_MOD_MODULO"] = $rs->fields ['SGD_MOD_MODULO'];
			$combi ["SGD_MOD_VALMAX"] = $rs->fields ['SGD_MOD_VALMAX'];
			$combi ["SGD_MOD_DETALLES"] = $rs->fields ['SGD_MOD_DETALLES'];
			$combi ["SGD_MOD_PATH"] = $rs->fields ['SGD_MOD_PATH'];
			$combi ["SGD_MOD_TITULO"] = $rs->fields ['SGD_MOD_TITULO'];
			$combi ["SGD_MOD_MENU"] = $rs->fields ['SGD_MOD_MENU'];
			$combi ["SGD_MOD_ESTADO"] = $rs->fields ['SGD_MOD_ESTADO'];
		} else
			$combi ['ERROR'] = 'No se encontraron datos';
		return $combi;
	
	}
	/**
	 * @return consulta los datos de los modulo.
	 */
	function consultarTodo($e = 0) {
		if ($e == 1)
			$estadoSql = " where sgd_mod_estado=1 ";
		$query = "SELECT * FROM sgd_mod_modules $estadoSql order by SGD_MOD_ID asc";
		$rs = $this->link->query ( $query );
		$i = 0;
		if (! $rs->EOF) {
			while ( ! $rs->EOF ) {
				$combi [$i] ["SGD_MOD_ID"] = $rs->fields ['SGD_MOD_ID'];
				$combi [$i] ["SGD_MOD_MODULO"] = $rs->fields ['SGD_MOD_MODULO'];
				$combi [$i] ["SGD_MOD_VALMAX"] = $rs->fields ['SGD_MOD_VALMAX'];
				$combi [$i] ["SGD_MOD_DETALLES"] = $rs->fields ['SGD_MOD_DETALLES'];
				$combi [$i] ["SGD_MOD_PATH"] = $rs->fields ['SGD_MOD_PATH'];
				$combi [$i] ["SGD_MOD_TITULO"] = $rs->fields ['SGD_MOD_TITULO'];
				$combi [$i] ["SGD_MOD_MENU"] = $rs->fields ['SGD_MOD_MENU'];
				$combi [$i] ["SGD_MOD_ESTADO"] = $rs->fields ['SGD_MOD_ESTADO'];
				$i ++;
				$rs->MoveNext ();
			}
		} else
			$combi ['ERROR'] = 'No se encontraron datos';
		return $combi;
	}
	/**
	 * @return crea un Modulo.
	 */
	function crear($nombre, $code, $valmax, $path, $deta, $titulo, $estado) {
		$query1 = "select max(sgd_mod_id) as id  from sgd_mod_modules";
		$rs1 = $this->link->conn->Execute ( $query1 );
		$idmod = $rs1->fields ["ID"] + 1;
		$query = "INSERT INTO sgd_mod_modules (sgd_mod_id,sgd_mod_modulo,sgd_mod_valmax,sgd_mod_detalles,sgd_mod_path,sgd_mod_titulo,sgd_mod_menu,sgd_mod_estado) 
		VALUES ($idmod,'$nombre',$valmax,'$deta','$path','$titulo','$code','$estado')";
		$rs = $this->link->conn->Execute ( $query );
		if (! $rs) {
			return 'ERROR';
		}
		return 'OK';
	}

	/**
	 * @return resultado de la operacion de actualizacion un Modulo.
	 * @param $id
	 * @param $nombre
	 * @param $code
	 * @param $valmax
	 * @param $path
	 * @param $deta
	 * @param $titulo
	 * @param $estado
	 */
	function actualizar($id, $nombre, $code, $valmax, $path, $deta, $titulo, $estado) {
		$query = "UPDATE sgd_mod_modules SET sgd_mod_modulo= '$nombre', 
	 			sgd_mod_valmax=$valmax,	sgd_mod_detalles='$deta',	sgd_mod_path='$path',	
	 			sgd_mod_titulo='$titulo',	sgd_mod_menu='$code',
	 			sgd_mod_estado = '$estado' WHERE sgd_mod_id = " . $id;
		
		$rs=$this->link->conn->Execute ( $query );
		if (! $rs) {
			return 'ERROR';
		}
		return 'OK';
	}
}

?>