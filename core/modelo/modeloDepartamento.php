<?php 
/** 
 * modeloDepartamento es la clase encargada de gestionar las operaciones y los datos basicos referentes a un departamento
 * @author Hardy Deimont Niño  Velasquez
 * @name modeloDepartamento
 * @version	1.0
 */
if (! $ruta_raiz)
	$ruta_raiz = '../../';
//include_once "$ruta_raiz/include/db/ConnectionHandler.php";
include_once "$ruta_raiz/core/clases/ConnectionHandler.php";

class modeloDepartamento {
	
	public $link;
	
	function __construct($ruta_raiz) {
		$db = new ConnectionHandler ( $ruta_raiz );
		$db->conn->SetFetchMode ( ADODB_FETCH_NUM );
		$db->conn->SetFetchMode ( ADODB_FETCH_ASSOC );
		$this->link = $db;
	}
	
	/**
	 * @return consulta los datos de un departamento.
	 * @param $codigo
	 */
	function consultar($codigo) {
		$q = "select * from departamento  where id_pais =$codigo";
		
                $rs = $this->link->query ( $q );
                
		$retorno = array ();
		
		if (! $rs->EOF) {
			$retorno ['id_cont'] = $rs->fields ['ID_CONT'];
                        $retorno ['id_pais'] = $rs->fields ['ID_PAIS'];
			$retorno ['dpto_nomb'] = $rs->fields ['DPTO_NOMB'];
                        $retorno ['dpto_codi'] = $rs->fields ['DPTO_CODI'];
			
		} else {
			$retono ['Error'] = 'No se contienen datos';
		
		}
		
		return ($retorno);
	}
	/**
	 * @return consulta los datos de las Departamentos.
	 */
	function consultarTodo() {
		$q = "select * from departamento ";
		$rs = $this->link->query  ( $q );
		$retorno = array ();
                //print_r($rs);
		if (! $rs->EOF) {
			
			$i = 0;
			while ( ! $rs->EOF ) {
                                $retorno [$i]['id_cont'] = $rs->fields ['ID_CONT'];
                                $retorno [$i]['id_pais'] = $rs->fields ['ID_PAIS'];
                                $retorno [$i]['dpto_nomb'] = $rs->fields ['DPTO_NOMB'];
                                $retorno [$i]['dpto_codi'] = $rs->fields ['DPTO_CODI'];
				$i ++;
				$rs->MoveNext ();
			}
		} else {
			$retono ['Error'] = 'No se contienen datos';
		
		}
		return $retorno;
	}
	/**
	 * @return crea Continente.
	 */
	function crear($nomb_cont) {
		/*$q = "select max(ID_CONT) maximo from sgd_def_continentes ";
		$rs = $this->link->query ( $q );
		$cont_codi= $rs->fields ['MAXIMO'] +1;
			$record['ID_CONT'] = $cont_codi;
		    $record['NOMBRE_CONT'] = $nomb_cont;
			$rs = $this->link->conn->insert('sgd_def_continentes',$record);
		
		if (! $rs) {
			return 'ERROR';
		}
		return 'OK';*/
	}
	
	/**
	 * @return resultado de la operacion de actualizacion un Continente.
	 * @param $cont_codi
	 * @param $nomb_cont
	 */
	function actualizar($cont_codi,$nomb_cont) {
  		/*if($cont_codi!='' && $nomb_cont!=''){	
		$recordWhere['ID_CONT'] = $cont_codi;
		$record['NOMBRE_CONT'] = $nomb_cont;
			
		$rs = $this->link->update('sgd_def_continentes',$record,$recordWhere);
		if (! $rs) {
			return 'ERROR';
		}
		return 'OK';
  		}
  			return 'ERROR';*/
	}
}


/** 
 * modeloMunicipio es la clase encargada de gestionar las operaciones y los datos basicos referentes a un municipio
 * @author Hardy Deimont Niño  Velasquez
 * @name modeloMunicipio
 * @version	1.0
 */

class modeloMunicipio {
	
	public $link;
	
	function __construct($ruta_raiz) {
		$db = new ConnectionHandler ( "$ruta_raiz" );
		$db->conn->SetFetchMode ( ADODB_FETCH_NUM );
		$db->conn->SetFetchMode ( ADODB_FETCH_ASSOC );
		$this->link = $db;
	}
	
	/**
	 * @return consulta los datos de un municuipio.
	 * @param $codigo
	 */
	function consultar($codigo) {
		$q = "select * from municipio where muni_codi =$codigo";
		$rs = $this->link->query ( $q );
		$retorno = array ();
		
		if (! $rs->EOF) {
			  $retorno ['id_cont'] = $rs->fields ['ID_CONT'];
                          $retorno ['id_pais'] = $rs->fields ['ID_PAIS'];
                          $retorno ['dpto_codi'] = $rs->fields ['DPTO_CODI'];
                          $retorno ['muni_codi'] = $rs->fields ['MUNI_CODI'];
                          $retorno ['muni_nomb'] = trim($rs->fields ['MUNI_NOMB']);
		} else {
			$retono ['Error'] = 'No se contienen datos';
		
		}
		
		return ($retorno);
	}
	/**
	 * @return consulta los datos de las municipio de un continete.
	 */
	function consultarTodo($codigo) {
		$q = "select * from municipio where dpto_codi =$codigo";
		$rs = $this->link->query ( $q );
		$retorno = array ();
		if (! $rs->EOF) {
			
			$i = 0;
			while ( ! $rs->EOF ) {
			        $retorno [$i]['id_cont'] = $rs->fields ['ID_CONT'];
                                $retorno [$i]['id_pais'] = $rs->fields ['ID_PAIS'];
                                $retorno [$i]['dpto_codi'] = $rs->fields ['DPTO_CODI'];
                                $retorno [$i]['muni_codi'] = $rs->fields ['MUNI_CODI'];
                                $retorno [$i]['muni_nomb'] = trim($rs->fields ['MUNI_NOMB']);
				$i ++;
				$rs->MoveNext ();
			}
		} else {
			$retono ['Error'] = 'No se contienen datos';
		
		}
		return $retorno;
	}
        	/**
	 * @return consulta los datos de las municipio de un continete.
	 */
	function consultarTodoData() {
		$q = "select * from municipio where activa=1";
		$rs = $this->link->query ( $q );
		$retorno = array ();
		if (! $rs->EOF) {
			
			$i = 0;
			while ( ! $rs->EOF ) {
                                $retorno [$i]['id_cont'] = $rs->fields ['ID_CONT'];
                                $retorno [$i]['id_pais'] = $rs->fields ['ID_PAIS'];
                                $retorno [$i]['dpto_codi'] = $rs->fields ['DPTO_CODI'];
                                $retorno [$i]['muni_codi'] = $rs->fields ['MUNI_CODI'];
                                $retorno [$i]['muni_nomb'] = trim($rs->fields ['MUNI_NOMB']);
				$i ++;
				$rs->MoveNext ();
			}
		} else {
			$retono ['Error'] = 'No se contienen datos';
		
		}
		return $retorno;
	}

	/**
	 * @return crea pais.
	 */
	function crear($nomb_cont,$cont_codi,$ID_PAIS) {
		/*	$record ['ID_PAIS'] = $ID_PAIS;
			$record['ID_CONT'] = $cont_codi;
		    $record['NOMBRE_CONT'] = $nomb_cont;
			$rs = $this->link->conn->insert('sgd_def_paises',$record);
		
		if (! $rs) {
			return 'ERROR';
		}
		return 'OK';*/
	}
	
	/**
	 * @return resultado de la operacion de actualizacion un paises.
	 * @param $cont_codi
	 * @param $nomb_cont
	 * @param $id_pais
	 */
	function actualizar($cont_codi,$nomb_cont,$id_pais) {
  		/*if($cont_codi!='' && $nomb_cont!=''){	
		$recordWhere['ID_CONT'] = $cont_codi;
		$recordWhere['ID_PAIS'] = $id_pais;
		$record ['ID_PAIS'] = $id_pais;
		$record['NOMBRE_CONT'] = $nomb_cont;
			
		$rs = $this->link->conn->update('sgd_def_paises',$record,$recordWhere);
		if (! $rs) {
			return 'ERROR';
		}
		return 'OK';
  		}
  			return 'ERROR';*/
	}
}
?>