<?php 
/**
 * continente es la clase encargada de gestionar las operaciones planilla
 * @author Hardy Deimont Niño  Velasquez
 * @name continente
 * @version	1.0
 */
if (!$ruta_raiz)
    $ruta_raiz = '../..';
include_once "$ruta_raiz/core/modelo/modeloPlantillas.php";
//==============================================================================================	
// CLASS continente
//==============================================================================================	

/**
 *  Objecto continente. 
 */
class plantillas {

    private $modelo;

    function __construct($ruta_raiz) {
        $this->modelo = new modeloPlantillas($ruta_raiz);
        //TODO - Insert your code here
    }


    function consultarTodo() {

        return $this->modelo->listar();
    }

    function consultarActivo() {
        return $this->modelo->listardta();
    }

    function crear($nomb,  $usua_codi, $depe) {
      return $this->modelo->crear($nomb,  $usua_codi, $depe);
    }
    function eliminar($id) {
      return $this->modelo->eliminar($id);
    }
    function modificar() {
        ;
    }

    function validarDir($rutadir){
	try{
	    if(!file_exists($rutadir)){
		if(!mkdir($rutadir,0775,true)){
		    throw new Exception("No hay permisos para crear permisos de carpeta de plantillas $rutadir");
		}
	    }
	}
	catch(Exception $e){
	    return false;
	}
    }

    /**
     * 
     */
    function __destruct() {
        $this->idcont = NULL;
        $this->name = NULL;
    }

}
