<?php 

/** 
 * Departamento es la clase encargada de gestionar las operaciones continente
 * @author Hardy Deimont Niño  Velasquez
 * @name departamento
 * @version	1.0
 */ 
if (! $ruta_raiz)
	$ruta_raiz = '../..';
include_once "$ruta_raiz/core/modelo/modeloDepartamento.php";
	//==============================================================================================	
	// CLASS Departamento
	//==============================================================================================	
	
	/**
	 *  Objecto departamento. 
	 */ 

class departamento {
private $modelo;
	
	function __construct($ruta_raiz) {
		$this->modelo = new modeloDepartamento( $ruta_raiz );
	//TODO - Insert your code here
	}
	
	/**
	 * Funci
	 */
	/*function consultar() {
		;
	}*/
	
    function consultarTodo() {
		return $this->modelo->consultarTodo();
	}
    /*function crear() {
		;
	}
    function modificar() {
		;
	}*/
	
	/**
	 * 
	 */
	function __destruct() {
		$this->idcont=NULL;
		$this->name=NULL;		
		

	}

	
}

/** 
 * Municipio es la clase encargada de gestionar las operaciones municipio
 * @author Hardy Deimont Niño  Velasquez
 * @name pais
 * @version	1.0
 */ 

	//==============================================================================================	
	// CLASS municipio
	//==============================================================================================	
	
	/**
	 *  Objecto municipio. 
	 */ 


class municipio {
 
	
	private $modelo;

	function __construct($ruta_raiz) {
		$this->modelo = new modeloMunicipio ( $ruta_raiz );
	//TODO - Insert your code here
	}
	
	/**
	 * Funci
	 */
	function consultar() {
		/* $rs=$this->modelo->consultar($this->idpais);
		 	$this->idcont=$rs['id_cont'];
		 	$this->name=$rs['nombre'];*/
	}
	
    function consultarTodo($iddpto) {
		return $this->modelo->consultarTodo($iddpto);
	}
        
    function consultarTodoData() {
		return $this->modelo->consultarTodoData();
	}
        
        
    /*function crear() {
		;
	}
    function modificar() {
		;
	}*/
	
	/**
	 * 
	 */
	function __destruct() {
	}
	
	
}
?>