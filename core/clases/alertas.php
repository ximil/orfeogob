<?php 
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of alertas
 *
 * @author derodriguez
 */
if(!$ruta_raiz){
    $ruta_raiz="../..";
}
include_once "$ruta_raiz/core/modelo/modeloAlertas.php";

class alertas {
    //put your code here
    private $DepeNume;
    private $IdRol;
    private $Correo;
    private $UsuaCodi;
    private $modelo;
    
    public function __construct($ruta_raiz) {
        $this->modelo=new modeloAlertas($ruta_raiz);
    }

    
    public function getDepeNume() {
        return $this->DepeNume;
    }

    public function setDepeNume($DepeNume) {
        $this->DepeNume = $DepeNume;
    }

    public function getIdRol() {
        return $this->IdRol;
    }

    public function setIdRol($IdRol) {
        $this->IdRol = $IdRol;
    }

    public function getCorreo() {
        return $this->Correo;
    }

    public function setCorreo($Correo) {
        $this->Correo = $Correo;
    }

    public function getUsuaCodi() {
        return $this->UsuaCodi;
    }

    public function setUsuaCodi($UsuaCodi) {
        $this->UsuaCodi = $UsuaCodi;
    }

    function numVencidos(){
        return $this->modelo->numVencidos();
    }
    
    /*function numxVencer(){
        return $this->modelo->numxVencer();
    }*/
    
    function vencidoxDepe(){
        return $this->modelo->vencidoxDepe($this->DepeNume);
    }

    function reporteVencidos($fecha_ini,$fecha_fin,$depe){
	if($depe==99999){
 	    $resultado=$this->modelo->numVencidos($fecha_ini,$fecha_fin);
	}
	else{
	    $resultado=$this->modelo->numVencidos($fecha_ini,$fecha_fin,$depe);
	}
	if($resultado['error']!=''){
	    return $resultado['error'];
	}
	$retorno="<table>
            <tr><td class=\"cosa\" colspan=\"3\">
      RADICADOS POR VENCIMIENTOS
    </td></tr>
                <tr class='tpar'>
                <td class='titulos3'>#</td><td class='titulos3'>Dependencia</td>
                <td class='titulos3'>Radicados</td></tr>";
	for($i=0;$i<count($resultado)-1;$i++){
	    $c=$i+1;
	    if ($i % 2) {
                $style = " class='listado1' ";
            } else {
                $style = " class='listado2' ";
            }
	    $retorno.="<tr $style><td>$c</td><td style=\"white-space: pre-line\">{$resultado [$i] ['nomdep']}</td><td><a href='#' onclick='detalles({$resultado[$i]['numdep']} ,\"{$resultado [$i]['nomdep']}\");'>{$resultado[$i]['vencidos']}</a></td>";
	}
	$retorno.="</table>";
	return $retorno;
    }

    function reporteDetalles($fecha_ini,$fecha_fin,$depe){
	$resultado=$this->modelo->vencidoxDepe($this->DepeNume,$fecha_ini,$fecha_fin);
	$f=count($resultado);
	$retorno="<table style='width:95%'>
            <tr><td class=\"cosa\" colspan=\"8\">
      RADICADOS POR VENCIMIENTOS - RADICADOS POR DEPENDENCIA
    </td></tr>
                <tr class='tpar'>
                <td class='titulos3' colspan='8'>$depe</td></tr>
                <tr class='tpar'>
                <td class='titulos3'>#</td><td class='titulos3'>Radicados</td>
                <td class='titulos3' style='width: 200px'>Asunto</td>
                <td class='titulos3'>Usuario Actual</td>
                <td class='titulos3'>Fecha Radicacion</td>
		<td class='titulos3'>D&iacute;as<br>Restantes</td>
                <td class='titulos3'>Tipo Documento</td>
		<td class='titulos3'>Estado</td>";
	for($i=0;$i<$f;$i++){
	    if($resultado[$i]['diasven']<=0){
                $estado="Vencido";
            }else{
                $estado="Por Vencer";
            }
            if($resultado[$i]['trd']=='0'){
               $estado="Sin tipificar";
            }
	    $c=$i+1;
	    if ($i % 2) {
                $style = " class='listado1' ";
            } else {
                $style = " class='listado2' ";
            }
	    $retorno.="<tr $style><td>$c</td><td><a href='#' onclick='verDocumento(\"../../../../\",{$resultado[$i]['numrad']})'>{$resultado[$i]['numrad']}</a></td>
                <td style=\"white-space: pre-line\"><label alt='{$resultado[$i]['asunto']}'>" . substr($resultado[$i]['asunto'], 0, 100) . "</label></td><td style=\"white-space: pre-line\">{$resultado[$i]['nomus']}</td><td>{$resultado[$i]['fecha']}</td><td style=\"white-space: pre-line\">{$resultado[$i]['diasven']}</td>
                <td style=\"white-space: pre-line\">{$resultado[$i]['tipo_documental']}</td><td style=\"white-space: pre-line\">$estado</td></tr>";
	}
	$retorno.="</table>";
	return $retorno;
    }
    
    /*function xvencerDepe(){
        return $this->modelo->xvencerDepe($this->DepeNume);
    }*/
    function jefexDep(){
        return $this->modelo->jefexDep($this->DepeNume);
    }
    
    function dataUsua(){
        return $this->modelo->dataUsua();
    } 
}

?>
