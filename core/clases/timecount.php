<?php 
/**
 * Description of timecount
 * Clase de cuenta el tiempo de transaccion.
 * @author hnino
 */
class timecount {

    /**
     * Función que calcula el tiempo transcurrido 
     */
    private $time_start;
    private $time_end;
    private $intervalo=array();
    private $nomIntervalo=array();
    private $contador=0;
    public function getTime_start() {
        return $this->time_start;
    }

    public function getTime_end() {
        return $this->time_end;
    }

        function __construct() {
        $this->time_start = $this->microtime_float();
    }

    /**
     * termina el proceso.
     */
    function endtime() {
        $this->time_end = $this->microtime_float();
    }
    
    /**
     * Funcion  toma intervalos funcionales. 
     * @param type $nom
     */
    function intertime($nom) {
        $this->nomIntervalo[$this->contador]=$nom;
        $this->intervalo[$this->contador] = $this->microtime_float();
        $this->contador=$this->contador+1;
    }
    
    /**
     * Imprime los datos
     */
     
    function printToIntervalo() {
        $timeT=$this->time_end-$this->time_start;
       // echo $this->contador;
        if($this->contador!=0){
            for ($index = 0; $index < $this->contador; $index++) {
                $timeTI=$this->intervalo[$index]-$this->time_start;
               echo "<b>Intervalo {$this->nomIntervalo[$index]} : $timeTI</b></br>";        
            }
        }
        echo "<b>Tiempo  Total : $timeT</b>";
    }
    
    /**
     * funcion que trae el tiempo de la transacciones.
     * @return float
     */
    function microtime_float() {
        list($usec, $sec) = explode(" ", microtime());
        return ((float) $usec + (float) $sec);
    }
    
    function __destruct() {
        ;
    }

}

?>
