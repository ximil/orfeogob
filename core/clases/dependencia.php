<?php 
/** 
 * dependencia es la clase encargada de gestionar las operaciones y los datos basicos referentes a una dependencia
 * @author Hardy Deimont Niño  Velasquez
 * @name dependencia
 * @version	1.0
 */

if (! $ruta_raiz)
       $ruta_raiz = '../..';
include_once "$ruta_raiz/core/modelo/modeloDependencia.php";

class dependencia {
	public $depe_codi;
	public $depe_nomb;
	public $dpto_codi;
	public $depe_codi_padre;
	public $muni_codi;
	public $depe_codi_territorial;
	public $dep_sigla;
	public $dep_central;
	public $dep_direccion;
	public $depe_num_interna;
	public $depe_num_resolucion;
	public $depe_rad_tp1;
	public $depe_rad_tp2;
	public $depe_rad_tp3;
	public $depe_rad_tp4;
	public $depe_rad_tp5;
	public $depe_rad_tp6;
	public $depe_rad_tp7;
	public $depe_rad_tp8;
	public $depe_rad_tp9;
	public $id_cont;
	public $id_pais;
	public $depe_estado;
	public $modelo;
	private $depehistAnt;
        private $ruta_raiz;

	
	/**
	 * @return the $depe_codi
	 */
	public function getDepe_codi() {
		return $this->depe_codi;
	}

	/**
	 * @return the $depe_nomb
	 */
	public function getDepe_nomb() {
		return $this->depe_nomb;
	}

	/**
	 * @return the $dpto_codi
	 */
	public function getDpto_codi() {
		return $this->dpto_codi;
	}

	/**
	 * @return the $depe_codi_padre
	 */
	public function getDepe_codi_padre() {
		return $this->depe_codi_padre;
	}

	/**
	 * @return the $muni_codi
	 */
	public function getMuni_codi() {
		return $this->muni_codi;
	}

	/**
	 * @return the $depe_codi_territorial
	 */
	public function getDepe_codi_territorial() {
		return $this->depe_codi_territorial;
	}

	/**
	 * @return the $dep_sigla
	 */
	public function getDep_sigla() {
		return $this->dep_sigla;
	}

	/**
	 * @return the $dep_central
	 */
	public function getDep_central() {
		return $this->dep_central;
	}

	/**
	 * @return the $dep_direccion
	 */
	public function getDep_direccion() {
		return $this->dep_direccion;
	}

	/**
	 * @return the $depe_num_interna
	 */
	public function getDepe_num_interna() {
		return $this->depe_num_interna;
	}

	/**
	 * @return the $depe_num_resolucion
	 */
	public function getDepe_num_resolucion() {
		return $this->depe_num_resolucion;
	}

	/**
	 * @return the $depe_rad_tp1
	 */
	public function getDepe_rad_tp1() {
		return $this->depe_rad_tp1;
	}

	/**
	 * @return the $depe_rad_tp2
	 */
	public function getDepe_rad_tp2() {
		return $this->depe_rad_tp2;
	}

	/**
	 * @return the $depe_rad_tp3
	 */
	public function getDepe_rad_tp3() {
		return $this->depe_rad_tp3;
	}

	/**
	 * @return the $depe_rad_tp4
	 */
	public function getDepe_rad_tp4() {
		return $this->depe_rad_tp4;
	}

	/**
	 * @return the $depe_rad_tp5
	 */
	public function getDepe_rad_tp5() {
		return $this->depe_rad_tp5;
	}

	/**
	 * @return the $depe_rad_tp6
	 */
	public function getDepe_rad_tp6() {
		return $this->depe_rad_tp6;
	}

	/**
	 * @return the $depe_rad_tp7
	 */
	public function getDepe_rad_tp7() {
		return $this->depe_rad_tp7;
	}

	/**
	 * @return the $depe_rad_tp8
	 */
	public function getDepe_rad_tp8() {
		return $this->depe_rad_tp8;
	}

	/**
	 * @return the $depe_rad_tp9
	 */
	public function getDepe_rad_tp9() {
		return $this->depe_rad_tp9;
	}

	/**
	 * @return the $id_cont
	 */
	public function getId_cont() {
		return $this->id_cont;
	}

	/**
	 * @return the $id_pais
	 */
	public function getId_pais() {
		return $this->id_pais;
	}

	/**
	 * @return the $depe_estado
	 */
	public function getDepe_estado() {
		return $this->depe_estado;
	}

	/**
	 * @param $depe_codi the $depe_codi to set
	 */
	public function setDepe_codi($depe_codi) {
		$this->depe_codi = $depe_codi;
	}

	/**
	 * @param $depe_nomb the $depe_nomb to set
	 */
	public function setDepe_nomb($depe_nomb) {
		$this->depe_nomb = $depe_nomb;
	}

	/**
	 * @param $dpto_codi the $dpto_codi to set
	 */
	public function setDpto_codi($dpto_codi) {
		$this->dpto_codi = $dpto_codi;
	}

	/**
	 * @param $depe_codi_padre the $depe_codi_padre to set
	 */
	public function setDepe_codi_padre($depe_codi_padre) {
		$this->depe_codi_padre = $depe_codi_padre;
	}

	/**
	 * @param $muni_codi the $muni_codi to set
	 */
	public function setMuni_codi($muni_codi) {
		$this->muni_codi = $muni_codi;
	}

	/**
	 * @param $depe_codi_territorial the $depe_codi_territorial to set
	 */
	public function setDepe_codi_territorial($depe_codi_territorial) {
		$this->depe_codi_territorial = $depe_codi_territorial;
	}

	/**
	 * @param $dep_sigla the $dep_sigla to set
	 */
	public function setDep_sigla($dep_sigla) {
		$this->dep_sigla = $dep_sigla;
	}

	/**
	 * @param $dep_central the $dep_central to set
	 */
	public function setDep_central($dep_central) {
		$this->dep_central = $dep_central;
	}

	/**
	 * @param $dep_direccion the $dep_direccion to set
	 */
	public function setDep_direccion($dep_direccion) {
		$this->dep_direccion = $dep_direccion;
	}

	/**
	 * @param $depe_num_interna the $depe_num_interna to set
	 */
	public function setDepe_num_interna($depe_num_interna) {
		$this->depe_num_interna = $depe_num_interna;
	}

	/**
	 * @param $depe_num_resolucion the $depe_num_resolucion to set
	 */
	public function setDepe_num_resolucion($depe_num_resolucion) {
		$this->depe_num_resolucion = $depe_num_resolucion;
	}

	/**
	 * @param $depe_rad_tp1 the $depe_rad_tp1 to set
	 */
	public function setDepe_rad_tp1($depe_rad_tp1) {
		$this->depe_rad_tp1 = $depe_rad_tp1;
	}

	/**
	 * @param $depe_rad_tp2 the $depe_rad_tp2 to set
	 */
	public function setDepe_rad_tp2($depe_rad_tp2) {
		$this->depe_rad_tp2 = $depe_rad_tp2;
	}

	/**
	 * @param $depe_rad_tp3 the $depe_rad_tp3 to set
	 */
	public function setDepe_rad_tp3($depe_rad_tp3) {
		$this->depe_rad_tp3 = $depe_rad_tp3;
	}

	/**
	 * @param $depe_rad_tp4 the $depe_rad_tp4 to set
	 */
	public function setDepe_rad_tp4($depe_rad_tp4) {
		$this->depe_rad_tp4 = $depe_rad_tp4;
	}

	/**
	 * @param $depe_rad_tp5 the $depe_rad_tp5 to set
	 */
	public function setDepe_rad_tp5($depe_rad_tp5) {
		$this->depe_rad_tp5 = $depe_rad_tp5;
	}

	/**
	 * @param $depe_rad_tp6 the $depe_rad_tp6 to set
	 */
	public function setDepe_rad_tp6($depe_rad_tp6) {
		$this->depe_rad_tp6 = $depe_rad_tp6;
	}

	/**
	 * @param $depe_rad_tp7 the $depe_rad_tp7 to set
	 */
	public function setDepe_rad_tp7($depe_rad_tp7) {
		$this->depe_rad_tp7 = $depe_rad_tp7;
	}

	/**
	 * @param $depe_rad_tp8 the $depe_rad_tp8 to set
	 */
	public function setDepe_rad_tp8($depe_rad_tp8) {
		$this->depe_rad_tp8 = $depe_rad_tp8;
	}

	/**
	 * @param $depe_rad_tp9 the $depe_rad_tp9 to set
	 */
	public function setDepe_rad_tp9($depe_rad_tp9) {
		$this->depe_rad_tp9 = $depe_rad_tp9;
	}

	/**
	 * @param $id_cont the $id_cont to set
	 */
	public function setId_cont($id_cont) {
		$this->id_cont = $id_cont;
	}

	/**
	 * @param $id_pais the $id_pais to set
	 */
	public function setId_pais($id_pais) {
		$this->id_pais = $id_pais;
	}

	/**
	 * @param $depe_estado the $depe_estado to set
	 */
	public function setDepe_estado($depe_estado) {
		$this->depe_estado = $depe_estado;
	}

	function __construct($ruta_raiz) {
		$this->ruta_raiz = $ruta_raiz;
		$this->modelo = new modeloDependencia ( $this->ruta_raiz );
		;
	}
	
	/**
	 * consulta un dependencia
	 */
	function consultar() {
		return $this->modelo->consultar ( $this->depe_codi );
	
	}
	/**
	 * consulta varios dependecias
	 */
	
	function consultarTodo() {
		return $this->modelo->consultarTodo ();
	
	}
        
        function consuDepHijas() {
		$dato=$this->modelo->consultarDepeHijas( $this->depe_codi );
                $i3=0;
                for ($i = 0; $i < count($dato); $i++) {
                    $datoR[$i3]=$dato[$i];
                    $i3++;
                /*    $dato1=$this->modelo->consultarDepeHijas( $dato[$i]['depe_codi'] );
                    if(!$dato1['Error']){
                          for ($i2 = 0; $i2 < count($dato1); $i2++){
                              //$dato[count($dato)+$i2]=$dato1[$i2];
                              $datoR[$i3]=$dato1[$i2];
                             // $i3++;
                          }
                    }*/
                    $gdata=new dependencia($this->ruta_raiz);
                    $gdata->setDepe_codi($dato[$i]['depe_codi']);
                    $datoX=$gdata->consuDepHijas();
                     if(!$datoX['Error']){
                          for ($i2 = 0; $i2 < count($datoX); $i2++){
                              //$dato[count($dato)+$i2]=$dato1[$i2];
                              $datoR[$i3]=$datoX[$i2];
                              $i3++;
                          }
                    }
                    
                }
                            //print_r($datoR)    ;
	       return $datoR;
	}
	
	/**
	 * Crear una depdendencia
	 */
	function crear($usua_doc) {
		return $this->modelo->crear ( $this->depe_codi, $this->depe_nomb, $this->dpto_codi, $this->depe_codi_padre, $this->muni_codi, $this->depe_codi_territorial, $this->dep_sigla, $this->dep_central, $this->dep_direccion, $this->depe_num_interna, $this->depe_num_resolucion, $this->id_cont, $this->id_pais, $this->depe_estado, $this->depe_rad_tp1, $this->depe_rad_tp2, $this->depe_rad_tp3, $this->depe_rad_tp4, $this->depe_rad_tp5, $this->depe_rad_tp6, $this->depe_rad_tp7, $this->depe_rad_tp8, $this->depe_rad_tp9 );
	
	}
	/**
	 * Modifica una dependecia
	 */
	function modificar($usua_doc) {
		$this->HistoricoConsultarActual();
		$res=$this->modelo->actualizar ( $this->depe_codi, $this->depe_nomb, $this->dpto_codi, $this->depe_codi_padre, $this->muni_codi, $this->depe_codi_territorial, $this->dep_sigla, $this->dep_central, $this->dep_direccion, $this->depe_num_interna, $this->depe_num_resolucion, $this->id_cont, $this->id_pais, $this->depe_estado, $this->depe_rad_tp1, $this->depe_rad_tp2, $this->depe_rad_tp3, $this->depe_rad_tp4, $this->depe_rad_tp5, $this->depe_rad_tp6, $this->depe_rad_tp7, $this->depe_rad_tp8, $this->depe_rad_tp9 );
		$this->HistoricoModificacion($usua_doc);
		return $res; 
	}
	/**
	 * tipos de radicacion
	 */
	/*function getTpDepeRad() {
		$res[1]=$this->depe_rad_tp1;
		$res[2]=$this->depe_rad_tp2;
		$res[3]=$this->depe_rad_tp3;
		$res[4]=$this->depe_rad_tp4;
		$res[5]=$this->depe_rad_tp5;
		$res[6]=$this->depe_rad_tp6;
		$res[7]=$this->depe_rad_tp7;
		$res[8]=$this->depe_rad_tp8;
		$res[9]=$this->depe_rad_tp9;
		return $res; 
	}*/
	
	
	/**
	 * Consulta el  historico  anterior
	 */
	function HistoricoConsulta() {
		//return $this->modelo->consultarHistorico ($this->depe_codi);
	}
	
	/**
	 * Consulta el  historico  anterior
	 */
	function HistoricoConsultarActual() {
		$hist=new dependencia();
		$this->depehistAnt=$hist->consultar ( $this->depe_codi );
	}
	/**
	 * crear el  historico de creacion
	 */
	function HistoricoCreacion($usua_doc) {
		//$hist=new dependencia();
		//$this->depehistAnt=$hist->consultar ( $this->depe_codi );
	}
	/**
	 * crear el  historico de modificacion
	 */
	function HistoricoModificacion($usua_doc) {
		/*$hist=new dependencia();
		$this->depehistAnt=$hist->consultar ( $this->depe_codi );*/
	}
	/**
	 * funcion que devuelve dependencias Inactivas
	 */
	function consultarDepInac(){
	    return $this->modelo->consultarDepInac();
	}
	
	
	function __destruct() {
		$this->depe_codi = NULL;
		$this->depe_nomb = NULL;
		$this->dpto_codi = NULL;
		$this->depe_codi_padre = NULL;
		$this->muni_codi = NULL;
		$this->depe_codi_territorial = NULL;
		$this->dep_sigla = NULL;
		$this->dep_central = NULL;
		$this->dep_direccion = NULL;
		$this->depe_num_interna = NULL;
		$this->depe_num_resolucion = NULL;
		$this->depe_rad_tp1 = NULL;
		$this->depe_rad_tp2 = NULL;
		$this->depe_rad_tp3 = NULL;
		$this->depe_rad_tp4 = NULL;
		$this->depe_rad_tp5 = NULL;
		$this->depe_rad_tp6 = NULL;
		$this->depe_rad_tp7 = NULL;
		$this->depe_rad_tp8 = NULL;
		$this->depe_rad_tp9 = NULL;
		$this->id_cont = NULL;
		$this->id_pais = NULL;
		$this->depe_estado = NULL;
		$this->modelo = NULL;
	}
}

?>
