<?php 
/**
 * modeloUsuarioOrfeo es la clase encargada de gestionar las operaciones usuario
 * @author Hardy Deimont Niño  Velasquez
 * @name modeloUsuarioOrfeo
 * @version	1.0
 */
if (!$ruta_raiz)
    $ruta_raiz = '../..';
include_once "$ruta_raiz/core/modelo/modeloUsuarioOrfeo.php";
//==============================================================================================	
// CLASS usuarioOrfeo
//==============================================================================================	

/**
 * Objecto UsuarioOrfeo. 
 */
class usuarioOrfeo {

    public $ruta_raiz;
    private $modelo;
    private $login; //login de  usuario
    private $nombre; //nombre y  apellidos del  usuario
    private $clave; //password actual
    private $usua_codi; //codigo del  rol
    private $depe_codi; //codigo de la dependencia 
    private $fech_crea; //fecha de  creacion
    private $estado; //si esta activo  o  inactivo
    private $usua_doc; //si esta activo  o  inactivo
    private $rol_codi; //cod rol
    private $codi_nivel; //nivel de seguridad
    private $usua_session; //session de php
    private $email; //session de php
    private $nacimiento; //fecha de nacimiento
    private $at; //nombre de maquina
    private $usua_debug; //debug
    private $usua_nuevo; //sie usuario  nuevo
    private $telefono; //telefono usuario  
    private $id_pais; //pais usuario  
    private $id_cont; //continente usuario
    private $ldap; //continente usuario
    private $orfeoScan; //continente usuario    
    private $error; //error
    private $graphic;
    private $pages;
    private $ldapE;
    private $usua_login_externo;

    public function setLdapE($ldapE) {
        $this->ldapE = $ldapE;
    }

    public function setUsua_login_externo($usua_login_externo) {
        $this->usua_login_externo = $usua_login_externo;
    }

    public function getLdapE() {
        return $this->ldapE;
    }

    public function getUsua_login_externo() {
        return $this->usua_login_externo;
    }

    /**
     * @return the vista
     */
    public function getOrfeoScan() {
        return $this->orfeoScan;
    }

    /**
     * @param $grafi the $telefono to set
     */
    public function setOrfeoScan($orfeoScan) {
        $this->orfeoScan = $orfeoScan;
    }

    /**
     * @return the vista
     */
    public function getRol() {
        return $this->rol_codi;
    }

    /**
     * @param $grafi the $telefono to set
     */
    public function setRols($rolcodi) {
        $this->rol_codi = $rolcodi;
    }

    /**
     * @return the vista
     */
    public function getPages() {
        return $this->pages;
    }

    /**
     * @param $grafi the $telefono to set
     */
    public function setPages($pages) {
        $this->pages = $pages;
    }

    /**
     * @return the vista
     */
    public function getGraphic() {
        return $this->graphic;
    }

    /**
     * @param $grafi 
     */
    public function setGraphic($grap) {
        $this->graphic = $grap;
    }

    /**
     * @return the $error
     */
    public function getError() {
        return $this->error;
    }

    /**
     * @return the $telefono
     */
    public function getTelefono() {
        return $this->telefono;
    }

    /**
     * @return the $id_pais
     */
    public function getId_pais() {
        return $this->id_pais;
    }

    /**
     * @return the $id_cont
     */
    public function getId_cont() {
        return $this->id_cont;
    }

    /**
     * @return the $id_cont
     */
    public function getRol_cont() {
        return $this->rol_codi;
    }

    /**
     * @param $telefono the $telefono to set
     */
    public function setTelefono($telefono) {
        $this->telefono = $telefono;
    }

    /**
     * @param $id_pais the $id_pais to set
     */
    public function setId_pais($id_pais) {
        $this->id_pais = $id_pais;
    }

    /**
     * @param $id_cont the $id_cont to set
     */
    public function setId_cont($id_cont) {
        $this->id_cont = $id_cont;
    }

    /**
     * @return the $usua_nuevo
     */
    public function getUsua_nuevo() {
        return $this->usua_nuevo;
    }

    /**
     * @param $usua_nuevo the $usua_nuevo to set
     */
    public function setUsua_nuevo($usua_nuevo) {
        $this->usua_nuevo = $usua_nuevo;
    }

    /**
     * @return the $login
     */
    public function getLogin() {
        return $this->login;
    }

    /**
     * @return the $nombre
     */
    public function getNombre() {
        return $this->nombre;
    }

    /**
     * @return the $clave
     */
    public function getClave() {
        return $this->clave;
    }

    /**
     * @return the $usua_codi
     */
    public function getUsua_codi() {
        return $this->usua_codi;
    }

    /**
     * @return the $depe_codi
     */
    public function getDepe_codi() {
        return $this->depe_codi;
    }

    /**
     * @return the $fech_crea
     */
    public function getFech_crea() {
        return $this->fech_crea;
    }

    /**
     * @return the $estado
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * @return the $usua_doc
     */
    public function getUsua_doc() {
        return $this->usua_doc;
    }

    /**
     * @return the $codi_nivel
     */
    public function getCodi_nivel() {
        return $this->codi_nivel;
    }

    /**
     * @return the $usua_session
     */
    public function getUsua_session() {
        return $this->usua_session;
    }

    /**
     * @return the $email
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * @return the $nacimiento
     */
    public function getNacimiento() {
        return $this->nacimiento;
    }

    /**
     * @return the $at
     */
    public function getAt() {
        return $this->at;
    }

    /**
     * @return the $usua_debug
     */
    public function getUsua_debug() {
        return $this->usua_debug;
    }

    /**
     * @param $login the $login to set
     */
    public function setLogin($login) {
        $this->login = $login;
    }

    /**
     * @param $nombre the $nombre to set
     */
    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    /**
     * @param $clave the $clave to set
     */
    public function setClave($clave) {
        $this->clave = $clave;
    }

    /**
     * @param $usua_codi the $usua_codi to set
     */
    public function setUsua_codi($usua_codi) {
        $this->usua_codi = $usua_codi;
    }

    /**
     * @param $depe_codi the $depe_codi to set
     */
    public function setDepe_codi($depe_codi) {
        $this->depe_codi = $depe_codi;
    }

    /**
     * @param $fech_crea the $fech_crea to set
     */
    public function setFech_crea($fech_crea) {
        $this->fech_crea = $fech_crea;
    }

    /**
     * @param $estado the $estado to set
     */
    public function setEstado($estado) {
        $this->estado = $estado;
    }

    /**
     * @param $usua_doc the $usua_doc to set
     */
    public function setUsua_doc($usua_doc) {
        $this->usua_doc = $usua_doc;
    }

    /**
     * @param $codi_nivel the $codi_nivel to set
     */
    public function setCodi_nivel($codi_nivel) {
        $this->codi_nivel = $codi_nivel;
    }

    /**
     * @param $usua_session the $usua_session to set
     */
    public function setUsua_session($usua_session) {
        $this->usua_session = $usua_session;
    }

    /**
     * @param $email the $email to set
     */
    public function setEmail($email) {
        $this->email = $email;
    }

    /**
     * @param $nacimiento the $nacimiento to set
     */
    public function setNacimiento($nacimiento) {
        $this->nacimiento = $nacimiento;
    }

    /**
     * @param $at the $at to set
     */
    public function setAt($at) {
        $this->at = $at;
    }

    /**
     * @param $usua_debug the $usua_debug to set
     */
    public function setUsua_debug($usua_debug) {
        $this->usua_debug = $usua_debug;
    }

    function __construct($ruta_raiz) {
        $this->ruta_raiz = $ruta_raiz;
        /* $this->usuario=$clave;
          $this->clave=$usu; */

        $this->modelo = new modeloUsuarioOrfeo($this->ruta_raiz);
    }

    function sessionUpdate($sid) {
        $this->modelo->sessionUpdate($this->login, $sid);
    }

    /**
     * Consulta los datos de usuario 
     * @return   void
     */
    function consultar_usuario() {

        $rs = $this->modelo->consultar($this->login);
        //print_r($rs);
        $this->depe_codi = $rs ['DEPE_CODI'];
        $this->clave = $rs ['USUA_PASW'];
        $this->nombre = $rs ['USUA_NOMB'];
        $this->fech_crea = $rs ['USUA_FECH_CREA'];
        $this->usua_nuevo = $rs ['USUA_NUEVO'];
        $this->usua_doc = $rs ['USUA_DOC'];
        $this->nacimiento = $rs ['USUA_NACIM'];
        $this->email = $rs ['USUA_EMAIL'];
        $this->estado = $rs ['USUA_ESTA'];
        $this->usua_session = $rs ['USUA_SESION'];
        $this->codi_nivel = $rs ['CODI_NIVEL'];
        $this->usua_debug = $rs ['USUA_DEBUG'];
        $this->at = $rs ['USUA_AT'];
        $this->usua_codi = $rs ['USUA_CODI'];
        $this->rol_codi = $rs ['ROL_CODI'];
        $this->telefono = $rs ['USU_TELEFONO1'];
        $this->id_pais = $rs ['ID_PAIS'];
        $this->id_cont = $rs ['ID_CONT'];
        $this->ldap = $rs ['USUA_AUTH_LDAP'];
        $this->graphic = $rs ['USUA_VISTAGRAPHIP'];
        $this->usua_login_externo = $rs ['USUA_LOGIN_EXTERNO'];
        $this->login = $rs ['USUA_LOGIN'];
        $this->error = $rs ['Error'];
    }

    /**
     * Consulta los datos de usuario 
     * @return   void
     */
    function consultar_usuario_depe_rol() {
        /* if($this->login)
          { */
        $rs = $this->modelo->consultar_depe_rol($this->depe_codi, $this->rol_codi);
        $this->login = $rs ['LOGIN'];
        $this->depe_codi = $rs ['DEPE_CODI'];
        $this->clave = $rs ['USUA_PASW'];
        $this->nombre = $rs ['USUA_NOMB'];
        $this->fech_crea = $rs ['USUA_FECH_CREA'];
        $this->usua_nuevo = $rs ['USUA_NUEVO'];
        $this->usua_doc = $rs ['USUA_DOC'];
        $this->nacimiento = $rs ['USUA_NACIM'];
        $this->email = $rs ['USUA_EMAIL'];
        $this->estado = $rs ['USUA_ESTA'];
        $this->usua_session = $rs ['USUA_SESION'];
        $this->codi_nivel = $rs ['CODI_NIVEL'];
        $this->usua_debug = $rs ['USUA_DEBUG'];
        $this->at = $rs ['USUA_AT'];
        $this->usua_codi = $rs ['USUA_CODI'];
        $this->rol_codi = $rs ['ROL_CODI'];
        $this->telefono = $rs ['USU_TELEFONO1'];
        $this->id_pais = $rs ['ID_PAIS'];
        $this->id_cont = $rs ['ID_CONT'];
        $this->ldap = $rs ['LDAP'];
        $this->graphic = $rs ['USUA_VISTAGRAPHIP'];

        $this->error = $rs ['Error'];

        /* }
          else
          $this->error='fallos'; */
    }

    /**
     * Consulta los datos de usuario 
     * @return   void
     */
    function consultar() {
        /* if($this->login)
          { */
        $rs = $this->modelo->consultar($this->login);
        $this->depe_codi = $rs ['DEPE_CODI'];
        $this->clave = $rs ['USUA_PASW'];
        $this->nombre = $rs ['USUA_NOMB'];
        $this->fech_crea = $rs ['USUA_FECH_CREA'];
        $this->usua_nuevo = $rs ['USUA_NUEVO'];
        $this->usua_doc = $rs ['USUA_DOC'];
        $this->nacimiento = $rs ['USUA_NACIM'];
        $this->email = $rs ['USUA_EMAIL'];
        $this->estado = $rs ['USUA_ESTA'];
        $this->usua_session = $rs ['USUA_SESION'];
        $this->codi_nivel = $rs ['CODI_NIVEL'];
        $this->usua_debug = $rs ['USUA_DEBUG'];
        $this->at = $rs ['USUA_AT'];
        $this->usua_codi = $rs ['USUA_CODI'];
        $this->rol_codi = $rs ['ROL_CODI'];
        $this->telefono = $rs ['USU_TELEFONO1'];
        $this->id_pais = $rs ['ID_PAIS'];
        $this->id_cont = $rs ['ID_CONT'];
        $this->ldap = $rs ['LDAP'];
        $this->graphic = $rs ['USUA_VISTAGRAPHIP'];

        $this->error = $rs ['Error'];

        /* }
          else
          $this->error='fallos'; */
    }

    /**
     * @return the $ldap
     */
    public function getLdap() {
        return $this->ldap;
    }

    /**
     * @param $ldap the $ldap to set
     */
    public function setLdap($ldap) {
        $this->ldap = $ldap;
    }

    /**
     * Consulta todos los usuarios
     */
    function consultarGrupo($usActivos = 0) {
        return $this->modelo->consultarGrupo($this->depe_codi, $usActivos);
    }

    /**
     * Consulta todos los usuarios
     */
    function consultadatos($correo) {
        return $this->modelo->consultadatos($correo);
    }

    /**
     * Consulta todos los usuarios
     */
    function consultadatosJefe($depe) {
        return $this->modelo->consultadatosJefe($depe);
    }

    /**
     * Consulta todos los usuarios
     */
    function consultarTodos() {
        return $this->modelo->consultarTodos();
    }

    /**
     * crea usuario
     */
    function crear() {
        //$this->clave = '0367fa0591680092de0d179106';
        $valida = $this->modelo->validaUsua($this->login, $this->usua_doc);
        if ($valida) {

            return "Login de $valida ya  existe.";
        }
	if(trim($this->usua_doc)!=$this->usua_doc){
	    return "N&uacute;mero del documento contiene espacios al principio o al final del campo";
	}

        $this->graphic = 1;
        $this->pages = 20;
        $vector ['USUA_NOMB'] = $this->nombre;
        $vector ['USUA_NUEVO'] = $this->usua_nuevo;
        $vector ['USUA_DOC'] = $this->usua_doc;
        $vector ['USUA_NACIM'] = $this->nacimiento;
        $vector ['USUA_EMAIL'] = $this->email;
        $vector ['USUA_ESTA'] = $this->estado;
        $vector ['USUA_DEBUG'] = $this->usua_debug;
        $vector ['USUA_AT'] = $this->at;
        $vector ['PERM_RADI'] = $this->orfeoScan;
        $vector ['USUA_EXT'] = $this->telefono;
        $vector ['USUA_AUTH_LDAP'] = $this->ldap;
        $vector ['USUA_VISTAGRAPHIP'] = $this->graphic;
        $vector ['USUA_PAGES'] = $this->pages;
        $vector ['LOGIN'] = $this->login;
        $vector ['USUA_LOGIN_EXTERNO'] = $this->usua_login_externo;

        if (!$vector ['LOGIN'])
            return 'Debe tener Un login';
        elseif (!$vector ['USUA_DOC'])
            return 'Debe tener Un Numero de documento';
        elseif (!$vector ['USUA_NOMB'])
            return 'Debe tener Un nombre';
        else {
            $res = $this->modelo->crearUsuario($vector);
            if ($res == 1) {
                return 'Usuario ' . $vector ['LOGIN'] . ' creado con Exito';
            } else {
                return 'Error al crear al Usuario ' . $vector ['LOGIN'];
            }
        }
    }

    /**
     * actualiza  usuario
     */
    function actualizar() {
        $vector ['USUA_CODI'] = $this->usua_codi;
        $vector ['USUA_NOMB'] = $this->nombre;
        $vector ['USUA_NUEVO'] = $this->usua_nuevo;
        $vector ['USUA_DOC'] = $this->usua_doc;
        $vector ['USUA_NACIM'] = $this->nacimiento;
        $vector ['USUA_EMAIL'] = $this->email;
        $vector ['USUA_ESTA'] = $this->estado;
        $vector ['USUA_DEBUG'] = $this->usua_debug;
        $vector ['USUA_AT'] = $this->at;
        $vector ['PERM_RADI'] = $this->orfeoScan;
        $vector ['USUA_EXT'] = $this->telefono;
        $vector ['USUA_AUTH_LDAP'] = $this->ldap;
        $vector ['USUA_LOGIN_EXTERNO'] = $this->usua_login_externo;
        if (!$vector ['USUA_DOC'])
            return 'Debe tener Un Numero de documento';
        elseif (!$vector ['USUA_NOMB'])
            return 'Debe tener Un nombre';
        else {
            $res = $this->modelo->modUsuario($vector);
            $vector ['LOGIN'] = $this->login;
            if ($res == 1) {
                return 'Usuario ' . $vector ['LOGIN'] . ' modificado con Exito';
            } else {
                return 'Error al modificar al Usuario' . $vector ['LOGIN'];
            }
        }
    }

    /**
     * buscar  usuario
     */
    function buscar() {


        return $this->modelo->buscar($this->login, $this->nombre, $this->usua_doc);
    }

    /**
     * buscar  usuario
     */
    function buscarInfo() {


        return $this->modelo->buscarInfo($this->login, $this->nombre, $this->usua_doc);
    }

    /**
     * cerrar usuario
     */
    function cerrar_session() {
        $this->modelo->cerrarSession();
    }

    /**
     * cambio de clave orfeo
     */
    function cambioClave($login, $pass, $codusuario, $fecha, $ipserver, $iplocal, $opsys) {
        $this->modelo->cambioclave($login, $pass, $codusuario, $fecha, $ipserver, $iplocal, $opsys);
    }

    /**
     * validar claves reutilizadas en el logeo de orfeo
     */
    function claverepetida($codusuario) {
        return $this->modelo->claverepetida($codusuario);
    }

    /**
     * Limpia los atributos de la instancia referentes a la informacion del usuario
     * @return   void
     */
    function __destruct() {
        $this->depe_codi = NULL;
        $this->clave = NULL;
        $this->nombre = NULL;
        $this->fech_crea = NULL;
        $this->usua_nuevo = NULL;
        $this->usua_doc = NULL;
        $this->nacimiento = NULL;
        $this->email = NULL;
        $this->estado = NULL;
        $this->usua_session = NULL;
        $this->codi_nivel = NULL;
        $this->usua_debug = NULL;
        $this->at = NULL;
        $this->usua_codi = NULL;
        $this->telefono = NULL;
        $this->id_pais = NULL;
        $this->id_cont = NULL;
    }

}

?>
