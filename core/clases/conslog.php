<?php 
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of conslog
 *
 * @author derodriguez
 */
if(!$ruta_raiz){
    $ruta_raiz="../..";
}
include_once "$ruta_raiz/core/modelo/modeloConslog.php";

class conslog {
    var $DepeNume;
    var $modelo;
    
    function __construct($ruta_raiz) {
        $this->modelo = new modeloConslog($ruta_raiz);
    }
    
    public function getDepeNume() {
        return $this->DepeNume;
    }

    public function setDepeNume($DepeNume) {
        $this->DepeNume = $DepeNume;
    }
    
    function consUsua(){
        return $this->modelo->consUsua();
    }
    
    function consDepe(){
        return $this->modelo->consDepe();
    }

    function consRol(){
        return $this->modelo->consRol();
    }
    
    function usuaConsDep(){
        return $this->modelo->usuaConsDep($this->DepeNume);
    }
    function usuasinDepen(){
        return $this->modelo->usuasinDepen();
    }
}

?>
