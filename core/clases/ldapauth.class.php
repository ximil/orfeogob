<?php 
/**
 * LDAPAuth Orfeo es la clase encargada de gestionar las operaciones contra un servidor ldap
 * @author Hardy Deimont Niño  Velasquez
 * @name modeloUsuarioOrfeo
 * @version	1.0
 */
/* if (! $ruta_raiz)
  $ruta_raiz = '../..'; */
//==============================================================================================	
// CLASS LDAPAuth 
//==============================================================================================	

/**
 *  Objecto LDAPAuth. 
 */

/**
 *  Class used to manage Auth LDAP config.
 * */
class LDAPAuth {

    public $Conection; //Objeto  conexion.
    private $cadenaBusq; //cadena de busqueda.
    private $campoBusq; //campo de Busqueda.
    public $logind;
    public $passd;
    //public $error;
    //Genera  la conecion contra el servidor ldap

    function __construct($ruta_raiz,$loginc='',$passc='') {
        include($ruta_raiz . "/core/config/config-ldap.php");
        /*echo $this->logind=$loginc;
        $this->passd=$passc;*/
        $this->Conection = $this->connectToServer($ldapServer, $ldapPost, $ldapRootDnG, $ldapRootPassG, false, $ldapDerefOptions,$authldapU,$urlCorreo,$loginc,$passc);
        $this->cadenaBusq = $ldapCadenaBusq;
        $this->campoBusq = $ldapCampoBusq;
    }

    /**
     * Connecta a un servidor LDAP 
     *
     * @param $host : LDAP servidor a conectar
     * @param $port : puerto de conexion
     * @param $login : login de conexion o  rootdn
     * @param $password : password de usuario de conexion
     * @param $use_tls : us tls para la connexion ?
     * @param $deref_options define la opcion deref
     *
     * @return link to the LDAP server : false if connection failed
     * */
    static function connectToServer($host, $port, $login2 , $password , $use_tls = false, $deref_options,$authldapU,$urlCorreo,$loginc,$passc) {
	if($authldapU==1){
	 	$login2=$login2.$urlCorreo;
	 	//$login2="cn=".$loginc.$urlCorreo;
	 	//$login2=$login2;
		//$password=$passc;
	}

        $ds = @ldap_connect($host, intval($port));
        if ($ds) {
            @ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3);
            @ldap_set_option($ds, LDAP_OPT_REFERRALS, 0);
            @ldap_set_option($ds, LDAP_OPT_DEREF, $deref_options);
            if ($use_tls) {
                if (!@ldap_start_tls($ds)) {
                    return false;
                }
            }
            // Auth bind
            if ($login2 != '') {
                //     echo 'bind'." $login2, $password";
                $b = @ldap_bind($ds, $login2, $password);
            } else { // Anonimo bind
                $b = @ldap_bind($ds);
            }
            if ($b) {
                return $ds;
            }
        }
        return false;
    }

    /**
     * Muestra los errores servidor LDAP 
     *
     * @return  el error
     * */
    function error() {
        return ldap_error($this->Conection);
    }

    /**
     * Autentica un usuario  contra el  servidor LDAP 
     *
     * @param $login : login de usuario
     * @param $password : password de usuario
     *
     * @return devuelve ok si serealizo con exito la autenticacion o sino  devuelve el  error
     * */
    public function autenticar($login, $password) {

        if (($res_id = ldap_search($this->Conection, $this->cadenaBusq, $this->campoBusq . "=$login")) == false) {
            $mensajeError = "No encontrado el usuario en el arbol LDAP";
            return $mensajeError;
        }
        //recolecta datos
        $g = ldap_count_entries($this->Conection, $res_id);
//print_r($g);        
if ( $g != 1) {
            if($g==0)
               $mensajeError = "USUARIO O CONTRASE&Ntilde;A INCORRECTOS";
            else
               $mensajeError = "El usuario $login se encontr&oacute; mas de 1 vez";
            return $mensajeError;
        }

        if (( $entry_id = ldap_first_entry($this->Conection, $res_id)) == false) {
            $mensajeError = "No se obtuvieron resultados";
            return $mensajeError;
        }

        if (( $user_dn = ldap_get_dn($this->Conection, $entry_id)) == false) {
            $mensajeError = "No se puede obtener el dn del usuario";
            return $mensajeError;
        }
        error_reporting(0);
        /* Autentica el usuario */
        if (($link_id = ldap_bind($this->Conection, $user_dn, $password)) == false) {
            error_reporting(0);
            $mensajeError = "USUARIO O CONTRASE&Ntilde;A INCORRECTOS";
            return $mensajeError;
        }

        return 'ok';
       
    }
    
	function search_all(){

$result = ldap_search();
if(ldap_count_entries($this->Conection, $result) === 1) {
    echo "si hay datos";
}
/*           $startFilter        = "(".$this->campoBusq ."=*)";
           $attrs = array($this->campoBusq, "modifyTimestamp");
           $filter = "(&(objectCategory=user)(memberOf=IM-ALL_USERS))";; 
        $attr = array("displayname", "mail", "mobile", "homephone", "telephonenumber", "streetaddress", "postalcode", "physicaldeliveryofficename", "l");   
	$startResults        = ldap_search($this->Conection, $this->cadenaBusq, $filter, $attr);
           ///echo "Number of entires returned is " . ldap_count_entries($this->Conection, $startResults  ) . "<br />";
/*            $info = ldap_get_entries($this->Conection,$startResults);
//return        $info["count"];    //$countResult        = ldap_count_entries($this->Conection,$startResults); 
 $user_infos = array();

                        for ($ligne = 0; $ligne < $info["count"]; $ligne++)
                        {
                                //If ldap add
                         /*       if (!$sync)
                                {*/
  /*                                 echo $ldap_users[$info[$ligne][$this->campoBusq ][0]] = $info[$ligne][$this->campoBusq ][0];
                           //             $user_infos[$info[$ligne][$config_ldap->fields['ldap_login']][0]]["timestamp"]=ldapStamp2UnixStamp($info[$ligne]['modifytimestamp'][0],$config_ldap->fields['timezone'],true);

/*                                else
                                {
                                //If ldap synchronisation
                           //             $ldap_users[$info[$ligne][$config_ldap->fields['ldap_login']][0]] = ldapStamp2UnixStamp($info[$ligne]['modifytimestamp'][0],$config_ldap->fields['timezone'],true);
                             //           $user_infos[$info[$ligne][$config_ldap->fields['ldap_login']][0]]["timestamp"]=ldapStamp2UnixStamp($info[$ligne]['modifytimestamp'][0],$config_ldap->fields['timezone'],true);
                                }i*/
                        //}
//return $ldap_users;
	}

    function __destruct() {
       @ldap_close($this->Conection);
    }

}

?>
