<?php
/**
 * series es la clase encargada de gestionar las operaciones series documentales
 * @author Hardy Deimont Niño  Velasquez
 * @name series
 * @version	1.0
 */
if (! $ruta_raiz)
	$ruta_raiz = '../../';
    include_once "$ruta_raiz/core/modelo/modeloNoticias.php";
	//==============================================================================================
	// CLASS series
	//==============================================================================================

	/**
	 *  Objecto series
	 */

class Noticias  {
	public $ruta_raiz;
	private $modelo;
	private $descripcion; //login de  usuario
	private $estado; //fecha de inicio
	private $fecha; // fecha  fin
	private $codigo; //codigo serie



	/**
	 * @return the $descripcion
	 */
	public function getDescripcion() {
		return $this->descripcion;
	}

	/**
	 * @return the $fecha
	 */
	public function getFecha() {
		return $this->fecha;
	}

	/**
	 * @return the $estado
	 */
	public function getEstado() {
		return $this->estado;
	}

	/**
	 * @return the $codigo
	 */
	public function getCodigo() {
		return $this->codigo;
	}

	/**
	 * @param $descripcion the $descripcion to set
	 */
	public function setDescripcion($descripcion) {
		$this->descripcion = $descripcion;
	}

	/**
	 * @param $fechaInicio the $fechaInicio to set
	 */
	public function setFecha($fecha) {
		$this->fecha= $fecha;
	}

	/**
	 * @param $fechaFin the $estado to set
	 */
	public function setEstado($estado) {
		$this->estado = $estado;
	}

	/**
	 * @param $codigo the $codigo to set
	 */
	public function setCodigo($codigo) {
		$this->codigo = $codigo;
	}

	function __construct($ruta_raiz) {

		$this->ruta_raiz = $ruta_raiz;
	    $this->modelo = new modeloNoticias( $ruta_raiz );
	}


	/**
	 * Consulta los datos de las noticias
	 * @return   void
	 */
	function consultar() {
			$rs = $this->modelo->consultar (  );
			return $rs;

	}

	/**
	 * Consulta los datos de las noticias
	 * @return   void
	 */
	function consultarE() {
		$rs = $this->modelo->consultarE ($this->codigo  );
		return $rs;

	}
	/**
	 * Consulta los datos de las noticias
	 * @return   void
	 */
	function noticiaActual() {
		$rs = $this->modelo->consultarActual();
		if(count($rs) > 0){
			$this->codigo = $rs["CODIGO"];
			$this->descripcion = $rs["DESCRIP"];
			$this->estado = $rs["ESTADO"];
			$this->fecha = $rs["FECHA"];
		}
		return $rs;

	}

	/*function buscar() {
		$combi = $this->modelo->Buscar( 0,$this->descripcion,1);
			if($combi  ["ERROR"]=='OK'){
				return $combi;
			}
		return 'No se encotro la Descripción';

	}*/

	/**
	 * crea Noticia
	 */
	function crear() {
	/*	$combi = $this->modelo->Buscar( $this->codigo,'',0);
		if($combi  ["ERROR"]!='OK'){
			$combi2 = $this->modelo->Buscar( 0,$this->descripcion,0);
			if($combi2  ["ERROR"]!='OK'){*/
		 	return $this->modelo->crear($this->descripcion);
			/*}
			else
			return 'Ya existe la Descripción';
		}
		else
		return 'Ya existe el codigo';*/
	}
	/**
	 * actualiza  Noticia
	 */
	function actualizar() {
		     return $this->modelo->actualizar($this->codigo,$this->descripcion);
	}

	/**
	 * actualiza  Noticia
	 */
	function actualizarEstado() {
		return $this->modelo->actualizarEs($this->codigo);
	}
	/**
	 * Limpia los atributos de la instancia referentes a la informacion del usuario
	 * @return   void
	 */
	function __destruct() {

	}

}

?>