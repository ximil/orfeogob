<?php /** 
 * modulos es la clase encargada de traer el menu
 * @author Hardy Deimont Niño  Velasquez
 * @name modulos
 * @version	1.0
 */
if (! $ruta_raiz)
$ruta_raiz = '../..';
include_once "$ruta_raiz/core/modelo/modeloMenu.php";
class menu {
	 public $ruta_raiz;
	 
	
	function __construct($ruta_raiz) {
		$this->ruta_raiz = $ruta_raiz;
		$this->modelo = new modeloMenu ( $this->ruta_raiz );
	
	}
	
	/**
	 * consulta un dependencia
	 */
	function consultar() {
		return $this->modelo->consultar (  );
	
	}
	
	function __destruct() {
	
	}
}

?>