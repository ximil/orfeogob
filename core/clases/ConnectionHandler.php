<?php 
/** 
 * @author deimont
 * 
 * 
 */
class ConnectionHandler {
	
	var $Error;
	var $id_query;
	
	var $driver;
	var $rutaRaiz;
	var $conn;
	var $entidad;
	var $entidad_largo;
	var $entidad_tel;
	var $entidad_dir;
	var $querySql;
	
	function __construct($ruta_raiz) {
		if (! defined ( 'ADODB_ASSOC_CASE' ))	define ( 'ADODB_ASSOC_CASE', 1 );
		include ("$ruta_raiz/core/adodb5/adodb.inc.php");
		include_once($ruta_raiz.'/core/adodb5/adodb-pager.inc.php');
	    /*include_once ("$ruta_raiz/adodb5/tohtml.inc.php");*/
		include ("$ruta_raiz/core/config/dbconfig.php");
		$ADODB_COUNTRECS = false;
		$this->driver = $driver;
		$this->conn  = NewADOConnection("$driver");
		$this->conn->charSet="AL32UTF8";
		$this->rutaRaiz = $ruta_raiz;
		if ($this->conn->Connect($servidor,$usuario,$contrasena,$servicio) == false)
			die("Error de conexi&oacute;n a la B.D. ");
		//Llamado al diccionario de datos para alterar el modelo para insertar metadatos
		$this->dict=NewDataDictionary($this->conn);
/*		$this->entidad = $entidad;
		$this->entidad_largo = $entidad_largo;
		$this->entidad_tel = $entidad_tel;
		$this->entidad_dir = $entidad_dir;*/
	}
	
function query($sql)
{
  //$this->conn->debug=true;
$cursor = $this->conn->Execute($sql);
  return $cursor;
}

/* Funcion miembro que realiza una consulta a la base de datos y devuelve un record set */

function getResult($sql) {
	if ($sql == "") {
		$this->Error = "No ha especificado una consulta SQL";
		print($this->Error);
		return 0;
	}
	return ($this->query($sql));
}

/*
   Funcion miembro que recibe como parametros: nombre de la tabla, un array con los nombres de los campos,
   y un array con los valores respectivamente.
*/
function insert($table,$record) {
  	$temp = array();
    $fieldsnames = array();
  	foreach($record as $fieldName=>$field )
  	{
      $fieldsnames[] = $fieldName;
    	$temp[] = $field;
    }
  	$sql = "insert into " . $table . "(" . join(",",$fieldsnames) . ") values (" . join(",",$temp) . ")";
  	if ($this->conn->debug==true)
  	{
  	 echo "<hr>(".$this->driver.") $sql<hr>";
  	}
		$this->querySql = $sql;
		
		//$this->conn->BeginTrans();
		$res = $this->conn->query( $sql );
		if( !$res )
		{
			//$this->conn->RollbackTrans();
		}
		else
		{
			//$this->conn->CommitTrans();
		}
	return( $res );
  	//return ($this->conn->query($sql));


	}

	
/*
   Funcion miembro que recibe como parametros: nombre de la tabla,
   un array con los nombres de los campos
   ,un array con los valores, un array con los nombres de los campo id y
   un array con los valores de los campos id respectivamente.
*/



	function update($table, $record, $recordWhere) {

	$tmpSet = array();
	$tmpWhere = array();
	foreach($record as $fieldName=>$field )
	{
	  $tmpSet[] = $fieldName . "=" . $field;
	}

	foreach($recordWhere as $fieldName=>$field )
	{
	  $tmpWhere[] = " " . $fieldName . " = " . $field . " ";
	}
	$sql = "update " . $table ." set " . join(",",$tmpSet) . "    where " . join(" and ",$tmpWhere);

	if ($this->conn->debug==true)
  	{
  	 echo "<hr>(".$this->driver.") $sql<hr>";
  	}
//$this->conn->debug=true;
  	$res = $this->conn->Execute( $sql );
	
  	if( !$res )
	{
		//$this->conn->RollbackTrans();
	}
	else
	{	
		//$this->conn->CommitTrans();
	}
	return( $res );
	//return ($this->conn->Execute($sql));

}


/*
   Funcion miembro que recibe como parametros: nombre de la tabla, un array con los
   nombres de los campos id, y un array con los valores de los id.
*/


	function delete($table, $record) {

	$temp = array();

	foreach($record as $fieldName=>$field )
	{
	$tmpWhere[] = "  " . $fieldName . "=" . $field;
	}
	$sql = "delete from " . $table . " where " . join(" and ",$tmpWhere);

	//print("*** $sql ****");
 	if ($this->conn->debug==true)
  	{
  	 echo "<hr>(".$this->driver.") $sql<hr>";
  	}
	return ($this->query($sql));

	}

	function nextId($secName){
		if ($this->conn->hasGenID)
			return $this->conn->GenID($secName);
		else{
			$retorno=-1;

			if ($this->driver=="oracle"){
				$q= "select $secName.nextval as SEC from dual";
				$this->conn->SetFetchMode(ADODB_FETCH_ASSOC);
				$rs=$this->query($q);
				//$rs!=false &&
				if  ( !$rs->EOF){
					$retorno = $rs->fields['SEC'];
					//print ("Retorna en la funcion de secuencia($retorno)");
				}
			}
			return $retorno;
		}
	}
	
	
	/**
	 * 
	 */
	function __destruct() {
	
	}
}

?>
