<?php 
/** 
 * roles es la clase encargada de gestionar las operaciones y los datos basicos referentes a un rol
 * @author Hardy Deimont Niño  Velasquez
 * @name roles
 * @version	1.0
 */ 
if (! $ruta_raiz)
$ruta_raiz = '../..';
include_once "$ruta_raiz/core/modelo/modeloRoles.php";
class roles {
	
	private $modelo;
	private $ruta_raiz;
	private $nombre_rol;
	private $id_rol;
	private $estado;
	
	/**
	 * @return the $nombre_rol
	 */
	public function getNombre_rol() {
		return $this->nombre_rol;
	}

	/**
	 * @return the $id_rol
	 */
	public function getId_rol() {
		return $this->id_rol;
	}

	/**
	 * @return the $estado
	 */
	public function getEstado() {
		return $this->estado;
	}

	/**
	 * @param $nombre_rol the $nombre_rol to set
	 */
	public function setNombre_rol($nombre_rol) {
		$this->nombre_rol = $nombre_rol;
	}

	/**
	 * @param $id_rol the $id_rol to set
	 */
	public function setId_rol($id_rol) {
		$this->id_rol = $id_rol;
	}

	/**
	 * @param $estado the $estado to set
	 */
	public function setEstado($estado) {
		$this->estado = $estado;
	}

	function __construct($ruta_raiz) {
	 $this->ruta_raiz=$ruta_raiz;
	 $this->modelo = new modeloRoles( $this->ruta_raiz );
	 
	}
	
	/**
	 * consulta un rol
	 */
	function consultar() {
		$rs=$this->modelo->consultar($this->id_rol);
		$this->nombre_rol=trim($rs->fields ['SGD_ROL_NOMBRE']);
	 	$this->id_rol=$rs->fields ['SGD_ROL_ID'];
	 	$this->estado=$rs->fields ['SGD_ROL_ESTADO'];
	}
	
	/**
	 * Consulta todos los roles
	 */
	function consultarTodo($e) {
		$rs=$this->modelo->consultarTodo($e);
		return $rs;
	}
	
     /**
	 * crea rol
	 */
	function crearRol() {
			$this->modelo->crear($this->nombre_rol,$this->estado);
	}
	/**
	 * actualiza  rol
	 */
	function actulaizarrRol() {
			$rs=$this->modelo->actualizar($this->nombre_rol,$this->estado,$this->id_rol);
			return $rs;
	}
	
	/** 
	 * Limpia los atributos de la instancia referentes a la informacion del rol
	 * @return   void
	 */
	function __destruct() {
	 $this->ruta_raiz=Null;
	 $this->nombre_rol=Null;
	 $this->id_rol=Null;
	 $this->estado=Null;
	}
}

?>