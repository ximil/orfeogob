<?php /** 
 * urd Orfeo es la clase encargada de gestionar usuarios vs rol vs dependencias
 * @author Hardy Deimont Niño  Velasquez
 * @name urd
 * @version	1.0
 */ 
if (! $ruta_raiz)
	$ruta_raiz = '../..';
include_once "$ruta_raiz/core/modelo/modeloUrd.php";
	//==============================================================================================	
	// usuarios rol dependecia
	//==============================================================================================	
	
	/**
	 *  Objecto urd. 
	 */ 
class urd {
	private $modelo;
	function __construct($ruta_raiz){
	 $this->modelo= new modeloUrd($ruta_raiz);
	}
	
	function listaUrd($dependecia) {
	   return $this->modelo->depe_usu_rol($dependecia);
	}
	
    function eliminarRelacioUrd($dependecia,$rol,$codusuario,$usuadoc) {
    $data=$this->modelo->validDelUrd($dependecia,$rol,$codusuario,$usuadoc);
    if($data){
    	$res="Operacion no realizada Motivo:<br><span style='font-size: 16;color: blue;'> Tiene  en bandeja ".$data."</span>";
    }else{
    	$res=$this->modelo->delUrd($dependecia,$rol,$codusuario);
    }
	   return$res; 
	}

    function CrearRelacionUrd($dependecia,$rol,$codusuario) {
	   return $this->modelo->addUrd($dependecia,$rol,$codusuario);
	}
	/**
	 * 
	 */
	function CosultaUsuaLibres() {
	   return $this->modelo->CosultaUsuaLibres();
	
	}
/**
	 * 
	 */
	function CosultaRolxDep($dep) {
	   return $this->modelo->CosultaRolxDep($dep);
	
	}
	
	function __destruct() {
		
	//TODO - Insert your code here
	}
}

?>