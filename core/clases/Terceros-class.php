<?php /**
 * terceros es la clase encargada de gestionar las operaciones y los datos basicos referentes a un tercero
 * @author Hardy Deimont Niño  Velasquez
 * @name dependencia
 * @version	1.0
 */
if (! $ruta_raiz)
	$ruta_raiz = '../..';
include_once "$ruta_raiz/core/modelo/modeloTerceros.php";
    
class terceros {
	public  $cedula;
        public  $nomb;
	function __construct($ruta_raiz) {
		$this->ruta_raiz = $ruta_raiz;
		$this->modelo = new modeloTerceros ( $this->ruta_raiz );
		;
	}
	
	
	function ConsultarCiu() {
		$resp=$this->modelo->consultarCiu( $this->cedula,$this->nomb );
		//print_r($resp);
		$tabla='<table class="titulo1"><tr>
				<td>Codigo</td>
				<td>Nombres y  apellidos</td>
				<td>Documento</td>
				<td>Direccion</td>
				<td>CIudad</td>
				<td>Departamento</td>
				<td>email</td>
				<td>estado</td>
				</tr>';
		$style="class='listado2'";
		for ($i = 0; $i < count($resp); $i++) {
			$tabla.="<tr $style>";
			$tabla.="<td>".$resp[$i]['codigo']."</td>";
			$tabla.="<td>".$resp[$i]['nombre']." ".$resp[$i]['apellido']." ".$resp[$i]['apellido2']."</td>";
			$tabla.="<td>".$resp[$i]['cedula']."</td>";
			$tabla.="<td>".$resp[$i]['dir']."</td>";
			$tabla.="<td>".$resp[$i]['muni_nomb']."</td>";
			$tabla.="<td>".$resp[$i]['depe_nomb']."</td>";
			$tabla.="<td>".$resp[$i]['email']."</td>";
			$ests='Activo';
			$est="";
			$esta=0;
			if($resp[$i]['estado']==1){
				$est='checked';
				$ests='Inactivo';
				$esta=1;
				
			}
			$div='"div'.$resp[$i]['codigo'].'"';
			$tabla.="<td><div name=$div id=$div> <input type='checkbox' $est onclick='cambiar(".$resp[$i]['codigo'].",$esta,$div,1)' name='cc".$resp[$i]['codigo']."'> $ests</div></td>";

			$tabla.="</tr>";
		}
		return $tabla;
	}
	function actualizarCiu($codigo,$estado) {
		$resp=$this->modelo->actualizarCiu( $codigo,$estado );
	}	
	function actualizarOem($codigo,$estado) {
		$resp=$this->modelo->actualizarOem( $codigo,$estado );
	}
	function ConsultarOem() {
		$resp=$this->modelo->consultarOem( $this->cedula ,$this->nomb);
		$tabla='<table class="titulo1"><tr>
				<td>Codigo</td>
				<td>Nombre Emp </td>
				<td>Sigla </td>
				<td>Representante Legal </td>
				<td>Documento</td>
				<td>Direccion</td>
				<td>CIudad</td>
				<td>Departamento</td>
				<td>estado</td>
				</tr>';
		$style="class='listado2'";
		for ($i = 0; $i < count($resp); $i++) {
			$tabla.="<tr $style>";
			$tabla.="<td>".$resp[$i]['codigo']."</td>";
			$tabla.="<td>".$resp[$i]['nombre']."</td>";
			$tabla.="<td>".$resp[$i]['sigla']."</td>";
			$tabla.="<td>".$resp[$i]['repre']."</td>";
			$tabla.="<td>".$resp[$i]['nit']."</td>";
			$tabla.="<td>".$resp[$i]['dir']."</td>";
			$tabla.="<td>".$resp[$i]['muni_nomb']."</td>";
			$tabla.="<td>".$resp[$i]['depe_nomb']."</td>";
			$ests='Activo';
			$est="";
			$esta=0;
			if($resp[$i]['estado']==1){
				$est='checked';
				$ests='Inactivo';
				$esta=1;
	
			}
			$div='"div'.$resp[$i]['codigo'].'"';
			$tabla.="<td><div name=$div id=$div> <input type='checkbox' $est onclick='cambiar(".$resp[$i]['codigo'].",$esta,$div,0)' name='cc".$resp[$i]['codigo']."'> $ests</div></td>";
	
			$tabla.="</tr>";
		}
		return $tabla;
	}
}
