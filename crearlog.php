<?php // Estructura de la carpeta deseada
$estructura = '/mnt/bodega/log';

// Para crear una estructura anidada se debe especificar
// el parámetro $recursive en mkdir().

if(!mkdir($estructura, 0777, true)) {
    die('Fallo al crear las carpetas...');
}

// ...
?>
