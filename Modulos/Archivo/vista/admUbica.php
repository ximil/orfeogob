<?php session_start();
date_default_timezone_set('America/Bogota');
date_default_timezone_set('America/Bogota');
foreach ($_GET as $key => $valor)
    $$key = $valor;
foreach ($_POST as $key => $valor)
    $$key = $valor;
$krd = $_SESSION["krd"];
$dependencia = $_SESSION["dependencia"];
$usua_doc = $_SESSION["usua_doc"];
$codusuario = $_SESSION["codusuario"];
$id_rol = $_SESSION["id_rol"];
$ruta_raiz = "../../..";
include($ruta_raiz.'/core/vista/validadte.php');
//include_once $ruta_raiz.'/core/language/'.$_SESSION ["lang"].'/data.inc';
include_once $ruta_raiz . '/Modulos/Archivo/clases/archivo-class.php';
$adminstrador = 1;
$scripturl = $ruta_raiz . '/Modulos/Archivo/vista/operAdmUbi.php';
//inicializa  dependecias
$archivo = new archivo($ruta_raiz);
//echo 1;
$edificio = $archivo->ConsultarEdi();
$arregloEdificio = 'vp = new Array(); \n';
$numndep = count($edificio);
//$optionEdi="<option value=''>Todas Las dependencias</option>";
$vector2 = "var vector2= new Array();\n";
/*
 * 			$combi [$i] ["CODIGO"] = $rs->fields ['ARCH_EDI_CODIGO'];
  			$combi [$i] ["NOMBRE"] = $rs->fields ['ARCH_EDI_NOMBRE'];
  			$combi [$i] ["SIGLA"] = $rs->fields ['ARCH_EDI_SIGLA'];
  			$combi [$i] ["CONTINENTE"] = $rs->fields ['CODI_CONT'];
  			$combi [$i] ["PAIS"] = $rs->fields ['CODI_PAIS'];
  			$combi [$i] ["DEPTO"] = $rs->fields ['CODI_DPTO'];
  			$combi [$i] ["MUNI"] = $rs->fields ['CODI_MUNI']; */
for ($i = 0; $i < $numndep; $i++) {
    $nomDepe = $edificio[$i]["NOMBRE"];
    $sigla = $edificio[$i]["SIGLA"];
    $codDepe = $edificio[$i]["CODIGO"];
    $codcont = $edificio[$i]["CONTINENTE"];
    $codpais = $edificio[$i]["PAIS"];
    $coddepto = $edificio[$i]["DEPTO"];
    $codmuni = $edificio[$i]["MUNI"];
    $vector2.="vector2[$codDepe]=new Array();\n";
    $vector2.="vector2[$codDepe]['NOMBRE']='$nomDepe';\n";
    $vector2.="vector2[$codDepe]['SIGLA']='$sigla';\n";
    $vector2.="vector2[$codDepe]['CONTINENTE']='$codcont';\n";
    $vector2.="vector2[$codDepe]['PAIS']='$codpais';\n";
    $vector2.="vector2[$codDepe]['DEPTO']='$coddepto';\n";
    $vector2.="vector2[$codDepe]['MUNI']='$codmuni';\n";
    $arregloEdificio.='vp[0] = new Array();';
    $optionEdi.="<option value='$codDepe'>$nomDepe - $sigla </option>";
}
$fechaf = date('d/m/Y');
$fechaI = fnc_date_calcm(date('d/m/Y'), '1');

function fnc_date_calcm($this_date, $num_month) {
    //echo $this_date;
    $y = explode('/', $this_date);
    $this_date = $y[0] . "-" . $y[1] . "-" . $y[2];
    $my_time = strtotime($this_date); //converts date string to UNIX timestamp
    $timestamp = $my_time - ($num_month * 2678400 ); //calculates # of days passed ($num_days) * # seconds in a day (86400)
    $return_date = date("Y-m-d", $timestamp);  //puts the UNIX timestamp back into string format
    $x = explode('-', $return_date);
    $return_date = $x[2] . "/" . $x[1] . "/" . $x[0];
    return $return_date; //exit function and return string
}
?>

<html>
    <head>
        <link rel="stylesheet" type="text/css" 	href="<?php echo  $ruta_raiz ?>/js/calendario/calendar.css">
        <link rel="stylesheet" href="<?php echo  $ruta_raiz ?>/estilos/default/orfeo.css">
        <script language="JavaScript" src="<?php echo  $ruta_raiz ?>/js/common.js"></script>
        <SCRIPT Language="JavaScript" src="<?php echo $ruta_raiz; ?>/core/vista/combos_universales.js.php"></SCRIPT>
        <SCRIPT Language="JavaScript" src="<?php echo $ruta_raiz; ?>/js/crea_combos_2.js"></SCRIPT>
        <script language="JavaScript" type="text/javascript"	src="<?php echo  $ruta_raiz ?>/js/calendario/calendar_eu.js"></script>
        <script type="text/javascript">
            // 
            function mostrarItem(item, padre, divItemX) {
                vistaFormUnitid('crearedi', 2);
                document.getElementById('itemName').value = '';
                document.getElementById('itemSigla').value = '';
                document.getElementById('itemCant').value = 1;
                document.getElementById('itemtitle').innerHTML = "Crear " + item;
                document.getElementById('itemdiv').value = divItemX;
                if (padre == "sEdi")
                    data = 0;
                else {
                    data = document.getElementById(padre).value;
                }
                document.getElementById('itemPadre').value = data;
                edi = document.getElementById("sEdi").value;
                if (edi == 0) {
                    alert('Seleccione un Edificio');
                    return false;
                }
                if (data == 0 && padre != "sEdi") {
                    alert('Seleccione un item anterior al de ' + item);
                    return false;
                }
                vistaFormUnitid('crearItem', 1);

            }
            function grabarEdi(div) {
                var nombre = document.getElementById('edificio').value;
                var sigla = document.getElementById('ediSigla').value;
                var cont = document.getElementById('idcont1').value;
                var pais = document.getElementById('idpais1').value;
                var dep = document.getElementById('codep_us1').value;
                var muni = document.getElementById('muni_us1').value;
                var padre = 0;
                //var listar = document.getElementById('lista').value;
                if (nombre.length == 0) {
                    alert('Debe llenar el campo nombre');
                    return false;
                }
                if (sigla.length == 0) {
                    alert('Debe llenar el campo sigla');
                    return false;
                }
                if (cont == 0) {
                    alert('Debe Seleccionar el campo continente');
                    return false;
                }
                if (pais == 0) {
                    alert('Debe Seleccionar el campo pa\xeds');
                    return false;
                }
                if (dep == 0) {
                    alert('Debe Seleccionar el campo departamento');
                    return false;
                }
                if (muni == 0) {
                    alert('Debe Seleccionar el campo municipio');
                    return false;
                }
                var poststr = "action=grabarEdi&nombre=" + nombre + "&sigla=" + sigla + "&cont=" + cont + "&pais=" + pais + "&dep=" + dep + "&muni=" + muni + "&padre=" + padre;
                //alert(poststr); 
                partes('<?php echo $scripturl; ?>', div, poststr, '');
                listarEdi("edificioDiv");
            }

            function listarEdi(div) {
                document.getElementById('piso').innerHTML = '<select size="10" class="select"  id="piso1" name="piso1"> <option value=0 selected> -- Selecione -- </option></select>';
                document.getElementById('area').innerHTML = '<select size="10" class="select"  id="area1" name="area1" > <option value=0 selected> -- Selecione -- </option></select>';
                document.getElementById('modulo').innerHTML = '<select   id="modulo1" name="modulo1" size="10" class="select" > <option selected value=0> -- Selecione -- </option></select>';
                document.getElementById('estantes').innerHTML = '<select size="10"  id="estantes1" name="estantes1" class="select" > <option value=0 selected> -- Selecione -- </option></select>';
                document.getElementById('entrepano').innerHTML = '<select size="10" id="entrepano1" name="entrepano1" no class="select" > <option value=0 selected> -- Selecione -- </option></select>';
                var poststr = "action=listEdi";
                partes('<?php echo $scripturl; ?>', div, poststr, '');
            }
            function Item(idiv, tipo) {
                var padre;
                //	document.getElementById('crearItem').innerHTML = '';
                vistaFormUnitid('crearItem', 2);
                document.getElementById('modItem').innerHTML = '';
                if (tipo == "p") {
                    padre = 0;
                    document.getElementById('piso').innerHTML = '<select size="10" class="select"  id="piso1" name="piso1"> <option value=0 selected> -- Selecione -- </option></select>';
                    document.getElementById('area').innerHTML = '<select size="10" class="select"  id="area1" name="area1" > <option value=0 selected> -- Selecione -- </option></select>';
                    document.getElementById('modulo').innerHTML = '<select   id="modulo1" name="modulo1" size="10" class="select" > <option selected value=0> -- Selecione -- </option></select>';
                    document.getElementById('estantes').innerHTML = '<select size="10"  id="estantes1" name="estantes1" class="select" > <option value=0 selected> -- Selecione -- </option></select>';
                    document.getElementById('entrepano').innerHTML = '<select size="10" id="entrepano1" name="entrepano1" no class="select" > <option value=0 selected> -- Selecione -- </option></select>';
                }
                else {
                    padre = document.getElementById(tipo).value;
                }
                if ( idiv == "area"){
                 document.getElementById('modulo').innerHTML = '<select   id="modulo1" name="modulo1" size="10" class="select" > <option selected value=0> -- Selecione -- </option></select>';
                 document.getElementById('estantes').innerHTML = '<select size="10"  id="estantes1" name="estantes1" class="select" > <option selected value=0> -- Selecione -- </option></select>';
                 document.getElementById('entrepano').innerHTML = '<select size="10" id="entrepano1" name="entrepano1" no class="select" > <option selected value=0> -- Selecione -- </option></select>';
                 }
                if (idiv == "modulo") {
                    document.getElementById('estantes').innerHTML = '<select size="10"  id="estantes1" name="estantes1" class="select" > <option selected value=0> -- Selecione -- </option></select>';
                    document.getElementById('entrepano').innerHTML = '<select size="10" id="entrepano1" name="entrepano1" no class="select" > <option selected value=0> -- Selecione -- </option></select>';
                }
                if (idiv == "estantes") {
                    document.getElementById('entrepano').innerHTML = '<select size="10" id="entrepano1" name="entrepano1" no class="select" > <option selected value=0> -- Selecione -- </option></select>';
                }
                if (padre == 0 && tipo != "p") {
                    padre = -1;
                }
                edi = document.getElementById('sEdi').value;
                var poststr = "action=listarItem&padre=" + padre + "&edi=" + edi + "&nomsel=" + idiv + "1";
                //alert(poststr); 
                partes('<?php echo $scripturl; ?>', idiv, poststr, '');

            }

            function grabarItem() {

                itemdiv = document.getElementById('itemdiv').value;
                if (itemdiv == "piso") {
                    padre = 0;
                }
                else {
                    padre = document.getElementById('itemPadre').value;
                }
                nombre = document.getElementById('itemName').value;
                sigla = document.getElementById('itemSigla').value;
                cantidad = document.getElementById('itemCant').value;
                if (nombre.length == 0) {
                    alert('Debe colocar El nombre');
                    return false;
                }
                if (sigla.length == 0) {
                    alert('Debe colocar Una sigla');
                    return false;
                }
                if (cantidad.length == 0) {
                    alert('Debe colocar el valor cantidad');
                    return false;
                }
                edi = document.getElementById('sEdi').value;

                var poststr = "action=crearItem&padre=" + padre + "&edificio=" + edi + "&nombre=" + nombre + "&sigla=" + sigla + "&cantidad=" + cantidad;
                //alert(poststr); 
                partes('<?php echo $scripturl; ?>', 'resultItem', poststr, '');
            }

            function formMod(itemNom, itemx) {
                codI = document.getElementById(itemx).value;
                if (codI == 0) {
                    alert('Por favor Seleccione un itemNom ');
                    return false;
                }
                edi = document.getElementById('sEdi').value;
                vistaFormUnitid("crearItem", 2);
                var poststr = "action=itemMod&codItem=" + codI + "&edificio=" + edi + '&Titulo=' + itemNom;
                partes('<?php echo $scripturl; ?>', 'modItem', poststr, '');
            }

            function itemMod() {
                codI = document.getElementById('itemCod2').value;
                sigla = document.getElementById('itemSigla2').value;
                name = document.getElementById('itemName2').value;
                if (name.length == 0) {
                    alert('Debe colocar El nombre');
                    return false;
                }
                if (sigla.length == 0) {
                    alert('Debe colocar Una sigla');
                    return false;
                }
                var poststr = "action=ModItem&codItem=" + codI + "&sigla=" + sigla + "&name=" + name;
                partes('<?php echo $scripturl; ?>', 'resultItem2', poststr, '');
            }
            function formEli(itemNom, itemx) {
                codI = document.getElementById(itemx).value;
                if (codI == 0) {
                    alert('Por favor Seleccione un itemNom ');
                    return false;
                }
                edi = document.getElementById('sEdi').value;
                vistaFormUnitid("crearItem", 2);
                var poststr = "action=itemEli&codItem=" + codI + "&edificio=" + edi + '&Titulo=' + itemNom;
                partes('<?php  echo $scripturl; ?>', 'modItem', poststr, '');
            }
            function itemEli() {
                codI = document.getElementById('itemCod2').value;
                sigla = document.getElementById('itemSigla2').value;
                name = document.getElementById('itemName2').value;
                edi = document.getElementById('itemEdi2').value;
                if (!confirm("Se encuentra  seguro de eliminar " + name + ", presione ACEPTAR, si no presione CANCELAR"))
                    return " ";

                var poststr = "action=eliItem&codItem=" + codI + "&sigla=" + sigla + "&name=" + name + "&edi=" + edi;
                partes('<?php echo $scripturl; ?>', 'resultItem2', poststr, '');
            }

            function vistaformEdi(accion) {
                vistaFormUnitid("crearedi", 1);
                vistaFormUnitid("crearItem", 2);
                document.getElementById("modItem").innerHTML = "";
<?php echo $vector2; ?>
                //alert(vector2[1]['NOMBRE']);
                titulo = 'Crear ';
                vistaFormUnitid("beliedi", 2);
                vistaFormUnitid("bmodedi", 2);
                vistaFormUnitid("bcrearedi", 2);

                if (accion == 'edi') {
                    vistaFormUnitid("bmodedi", 1);
                    titulo = 'Editar ';
                }
                if (accion == 'eli') {
                    vistaFormUnitid("beliedi", 1);
                    titulo = 'Eliminar ';

                }
                if (accion != 'crea') {
                    code = document.getElementById('sEdi').value;
                    if (code == 0) {
                        alert('seleccione Un edificio');
                        return false;
                    }
                    document.getElementById('ediCod').value = code;
                    document.getElementById('ediSigla').value = vector2[code]['SIGLA'];
                    document.getElementById('edificio').value = vector2[code]['NOMBRE'];
                    document.getElementById('idcont1').value = vector2[code]['CONTINENTE'];
                    document.getElementById('idpais1').value = vector2[code]['PAIS'];
                    document.getElementById('codep_us1').value = vector2[code]['DEPTO'];
                    document.getElementById('muni_us1').value = vector2[code]['MUNI'];

                } else {
                    vistaFormUnitid("bcrearedi", 1);
                    document.getElementById('ediCod').value = '';
                    document.getElementById('ediSigla').value = '';
                    document.getElementById('edificio').value = '';
                    document.getElementById('idcont1').value = '';
                    document.getElementById('idpais1').value = '';
                    document.getElementById('codep_us1').value = '';
                    document.getElementById('muni_us1').value = '';
                }
                document.getElementById('tilEdi').innerHTML = titulo + " Edificio ";

            }
            function eliEdi(div) {
                cod = document.getElementById('ediCod').value;
                name = document.getElementById('edificio').value;
                sigla = document.getElementById('ediSigla').value;
                if (!confirm("Se encuentra  seguro de eliminar " + name + ", presione ACEPTAR, si no presione CANCELAR"))
                    return " ";

                var poststr = "action=eliEdi&cod=" + cod + "&sigla=" + sigla + "&name=" + name;
                partes('<?php echo $scripturl; ?>', div, poststr, '');
                listarEdi("edificioDiv");
            }
            function modiEdi(div) {
                cod = document.getElementById('ediCod').value;
                name = document.getElementById('edificio').value;
                sigla = document.getElementById('ediSigla').value;
                cont = document.getElementById('idcont1').value;
                pais = document.getElementById('idpais1').value;
                depto = document.getElementById('codep_us1').value;
                muni = document.getElementById('muni_us1').value;
                if (!confirm("Se encuentra  seguro de Modificar " + name + ", presione ACEPTAR, si no presione CANCELAR"))
                    return " ";

                var poststr = "action=ModEdi&cod=" + cod + "&sigla=" + sigla + "&name=" + name + "&cont=" + cont + "&pais=" + pais + "&depto=" + depto + "&muni=" + muni;
                partes('<?php echo $scripturl; ?>', div, poststr, '');
                listarEdi("edificioDiv");
                listarEdi("edificioDiv");
                listarEdi("edificioDiv");
            }

        </script>

    </head>
    <body bgcolor="#FFFFFF">
<?php include 'admArchivotab.php'; ?>
        <table class=borde_tab width='100%' cellspacing="2"><tr><td  align='center' class="titulos4">
                    Modulo de Archivo - Administrador de Edificios. 
                </td></tr></table>

        <script language="javascript">
            //vistaFormUnitid("crearedi",2);itemEli
            //cambia(this.form, 'codep', 'idpais');
            //cambia(document.forms["formexp"], 'codep_us1', 'idpais1');
            //partes('<?php  echo $scripturl ?>','listados','accion=listado','');
        </script>
        <table width="100%">
            <tr>
                <td>
                    <div id="data" >
                        <table border=0 width="" cellpadding="0" cellspacing="1"   class="borde_tab">
                            <tr class='titulo1'>
                                <TD colspan='7' >Datos de Ubicaci&oacute;n</td></tr>
                            <tr>
                                <th class="titulos5">Edificio</th>
                                <th class="titulos5">Piso</th>
                                <th class="titulos5">&Aacute;rea</th>
                                <th class="titulos5">M&oacute;dulo</th>
                                <th class="titulos5">Estantes</th>
                                <th class="titulos5">Entrepa&ntilde;os</th>
                                <td valign="top" rowspan='4'>
                                    <img src="../../../imagenes/add1.png" alt="Agregar" width="24" height="24" longdesc="Agregrar Modulo">Agregar<br>
                                    <img src="../../../imagenes/borrar.png" alt="Eliminar " width="24" height="24" longdesc="Eliminar Edificio"> Eliminar<br>
                                    <img src="../../../imagenes/edit.png" alt="Editar " width="24" height="24" longdesc="Editar Edificio"> Modificar<br>
                                    <img src="../../../imagenes/actualizar.png" alt="actualizar "  width="24" height="24" longdesc="Actualizar lista"> Actualizar lista


                                </td>
                            </tr>
                            <tr>	
                                <td  valign="top" >
                                    <div id='edificioDiv'>
                                        <select name="sEdi" id="sEdi" onchange="Item('piso', 'p')" size="10" class='select'>
                                            <option value="0" selected>-- Seleccione --</option>
<?php echo $optionEdi; ?>
                                        </select> 
                                    </div>
                                </td>	
                                <td  valign="top">
                                    <div id="piso"><select  id="piso1" name="piso1" size="10" onchange="Item('area', 'piso1')" class='select'>
                                            <option value="0" selected>-- Seleccione --</option>
                                        </select></div>
                                </td>
                                <td valign="top">
                                <div id="area"><select id="area1" name="area1" class='select' size="10" no>
                                                                                <option value="0" selected>-- Seleccione --</option>
                                </select></div>
                                </td>
                                <td valign="top">
                                    <div id="modulo"><select id="modulo1" name="modulo1" class='select' size="10" no>
                                            <option value="0" selected>-- Seleccione --</option>
                                        </select></div>
                                </td>
                                <td valign="top">
                                    <div id="estantes"><select id="estante1" name="estante1" class='select' size="10"
                                                               multiple="multiple" no>
                                            <option value="0" selected>-- Seleccione --</option>
                                        </select></div>
                                </td>
                                <td valign="top" >
                                    <div id="entrepano"><select  id="entrepano1" name="entrepano1" class='select' size="10"
                                                                 multiple="multiple" no>
                                            <option value="0" selected>-- Seleccione --</option>
                                        </select></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="titulos5" align="center">
                                    <a href='#'><img src="../../../imagenes/add1.png" alt="Agregar" onclick='vistaformEdi("crea")' width="24" height="24" longdesc="Agregrar edificio"></a>
                                    <a href='#'><img src="../../../imagenes/borrar.png" alt="Eliminar " width="24" height="24"  onclick='vistaformEdi("eli")' longdesc="Eliminar Edificio"></a>
                                    <!--a href='#'><img src="../../../imagenes/edit.png" alt="Editar " onclick='vistaformEdi("edi")' width="24" height="24" longdesc="Editar Edificio"></a-->
                                    <a href='#'><img src="../../../imagenes/actualizar.png" alt="actualizar " onclick='listarEdi("edificioDiv");' width="24" height="24" longdesc="Actualizar lista"></a>
                                </td>
                                <td class="titulos5" align="center">
                                    <a href='#'><img src="../../../imagenes/add1.png" alt="Agregar" onclick='mostrarItem("Piso", "sEdi", "piso");
                                            document.getElementById("modItem").innerHTML = "";' width="24" height="24" longdesc="Agregrar piso"></a>
                                    <a href='#' onclick='formEli("Piso", "piso1");'><img src="../../../imagenes/borrar.png" alt="Eliminar " width="24" height="24" longdesc="Eliminar Edificio"></a>
                                    <a href='#' onclick='formMod("Piso", "piso1");'><img src="../../../imagenes/edit.png" alt="Editar " width="24" height="24" longdesc="Editar Edificio"></a>
                                    <a href='#'><img src="../../../imagenes/actualizar.png" alt="actualizar " onclick="Item('piso', 'p')" width="24" height="24" longdesc="Actualizar lista"></a>
                                </td>
                                <td class="titulos5" align="center">
                                <a href='#'><img src="../../../imagenes/add1.png" alt="Agregar" onclick='mostrarItem("\xc1rea","piso1","area");document.getElementById("modItem").innerHTML="";' width="24" height="24" longdesc="Agregrar Piso"></a>
                                <a href='#' onclick='formEli("\xc1rea","area1");'><img src="../../../imagenes/borrar.png" alt="Eliminar " width="24" height="24" longdesc="Eliminar Edificio"></a>
                                <a href='#' onclick='formMod("\xc1rea","area1");'><img src="../../../imagenes/edit.png" alt="Editar " width="24" height="24" longdesc="Editar piso"></a>
                                <a href='#'><img src="../../../imagenes/actualizar.png" alt="actualizar" onclick="Item('area','piso1')" width="24" height="24" longdesc="Actualizar lista"></a>
                                </td>
                                <td class="titulos5" align="center">
                                    <a href='#'><img src="../../../imagenes/add1.png" alt="Agregar" onclick='mostrarItem("M\xf3dulo", "area1", "modulo");
                                            document.getElementById("modItem").innerHTML = "";' width="24" height="24" longdesc="Agregrar Modulo"></a>
                                    <a href='#' onclick='formEli("M\xf3dulo", "modulo1")'><img src="../../../imagenes/borrar.png" alt="Eliminar " width="24" height="24" longdesc="Eliminar Edificio"></a>
                                    <a href='#'  onclick='formMod("M\xf3dulo", "modulo1")'><img src="../../../imagenes/edit.png" alt="Editar " width="24" height="24" longdesc="Editar Edificio"></a>
                                    <a href='#'><img src="../../../imagenes/actualizar.png" alt="actualizar " onclick="Item('modulo', 'area1')" width="24" height="24" longdesc="Actualizar lista"></a>
                                </td>
                                <td class="titulos5" align="center">
                                    <a href='#'><img src="../../../imagenes/add1.png" alt="Agregar" onclick='mostrarItem("Estante", "modulo1", "estante");
                                            document.getElementById("modItem").innerHTML = "";' width="24" height="24" longdesc="Agregrar Estante"></a>
                                    <a href='#' onclick='formEli("Estante", "estantes1")'><img src="../../../imagenes/borrar.png" alt="Eliminar " width="24" height="24" longdesc="Eliminar Edificio"></a>
                                    <a href='#' onclick='formMod("Estante", "estantes1")'><img src="../../../imagenes/edit.png" alt="Editar " width="24" height="24" longdesc="Editar Edificio"></a>
                                    <a href='#'><img src="../../../imagenes/actualizar.png" alt="actualizar " onclick="Item('estantes', 'modulo1')" width="24" height="24" longdesc="Actualizar lista"></a>
                                </td>
                                <td class="titulos5" align="center">
                                    <a href='#'><img src="../../../imagenes/add1.png" alt="Agregar" onclick='mostrarItem("Entrepa\xf1o", "estantes1", "entrepano");
                                            document.getElementById("modItem").innerHTML = "";' width="24" height="24" longdesc="Agregrar edificio"></a>
                                    <a href='#' onclick='formEli("Entrepa\xf1o", "entrepano1")'><img src="../../../imagenes/borrar.png" alt="Eliminar " width="24" height="24" longdesc="Eliminar Edificio"></a>
                                    <a href='#' onclick='formMod("Entrepa\xf1o", "entrepano1")'><img src="../../../imagenes/edit.png" alt="Editar " width="24" height="24" longdesc="Editar Edificio"></a>
                                    <a href='#' onclick="Item('entrepano', 'estantes1')"><img src="../../../imagenes/actualizar.png" alt="actualizar "  width="24" height="24" longdesc="Actualizar lista"></a>
                                </td>
                            </tr>
                            <tr>
                                <th class="titulos5">Edificio</th>
                                <th class="titulos5">Piso</th>
                                <th class="titulos5">&Aacute;rea</th>
                                <th class="titulos5">M&oacute;dulo</th>
                                <th class="titulos5">Estantes</th>
                                <th class="titulos5">Entrepa&ntilde;os</th>
                            </tr>
                        </table>   
                    </div><br>
                    <div id='crearedi' style="display:none">
                        <table  class="borde_tab" width="100%"><tr><td class="titulos5" style="color: red;font-size: 12">
                                    <div id='resultEdi'></div></td></tr></table><br>
                        <form method="_POST" action="#" name="formexp"  style="margin:0">
                            <table width="100%" class="borde_tab">
                                <tr><td class="titulos4" colspan="6"> <div id="tilEdi"></div></td></tr>
                                <tr class="titulos5">
                                    <td  >Nombre</td>
                                    <td >Sigla</td>
                                    <td >Continete</td>
                                    <td>Pa&iacute;s</td>
                                    <td >Departamento</td>
                                    <td >Municipio</td>
                                </tr>
                                <tr >
                                    <td >
                                        <input name="edificio" type='text' id="edificio" maxlength="40"  >
                                        <input name="ediCod" type="hidden" id="ediCod" maxlength="40"  >
                                    </td>
                                    <td >
                                        <input name="ediSigla" type='text' maxlength="4" size="4" id="ediSigla" >
                                    </td>
                                    <td >
                                        <select name="idcont1" id="idcont1" class="select"  onchange="cambia(document.forms['formexp'], 'idpais1', 'idcont1')">
                                            <option value="0">&lt;&lt; seleccione &gt;&gt;</option>
                                            <option value="1"> America  </option>                            </select>
                                    </td>
                                    <td width="19%">
                                        <select name='idpais1' class="select"  id='idpais1' onchange="cambia(this.form, 'codep_us1', 'idpais1');">
                                            <option value="0">&lt;&lt; seleccione &gt;&gt;</option>
                                        </select>
                                    </td>
                                    <td width="19%">
                                        <select  name='codep_us1' class="select"  id='codep_us1' onchange="cambia(this.form, 'muni_us1', 'codep_us1');">
                                            <option value="0">&lt;&lt; seleccione &gt;&gt;</option></select></td>
                                    <td width="19%">
                                        <select name="muni_us1" class="select"  id='muni_us1'>
                                            <option value="0">&lt;&lt; seleccione &gt;&gt;</option>
                                        </select></td>

                                </tr>
                                <tr class='listado2'>
                                    <td  colspan="6" align="CENTER">
                                        <table><tr><td>
                                                    <div id='bcrearedi' style="display:none">
                                                        <input name="button"  type="button" class="botones" value="Grabar" onclick='grabarEdi("resultEdi");'>
                                                    </div><div id='bmodedi' style="display:none">
                                                        <input name="button"  type="button" class="botones" value="Modificar" onclick='modiEdi("resultEdi");'>
                                                    </div><div id='beliedi' style="display:none">
                                                        <input name="button"  type="button" class="botones" value="Eliminar" onclick='eliEdi("resultEdi");'>
                                                    </div>  </td><td>
                                                    <input name="button" class="botones" type="reset"" value="Cancelar" onclick='vistaFormUnitid("crearedi", 2);'>
                                                </td></tr></table>
                                    </td>

                                </tr>
                            </table>


                        </form>
                    </div>
                    <div id='crearItem' style="display:none">
                        <table  class="borde_tab" width="100%"><tr><td class="titulos5" style="color: red;font-size: 12">
                                    <div id='resultItem'></div></td></tr></table><br>
                        <table  class="borde_tab">
                            <tr><td class="titulos4" colspan="6"><div id='itemtitle'>Piso</div></td></tr>
                            <tr class="titulos5">
                                <td >Nombre</td>
                                <td >Sigla</td>
                                <td >Cantidad</td>
                            </tr>
                            <tr >
                                <td >
                                    <input name="itemName" type='text' id="itemName" maxlength="40">
                                    <input name="itemPadre" type="hidden" id="itemPadre" >
                                    <input name="itemCod" type="hidden" id="itemCod" >
                                    <input name="itemdiv" type="hidden" id="itemdiv" >
                                </td>
                                <td >
                                    <input name="itemSigla" type='text' id="itemSigla" size='4' maxlength="4" >
                                </td>
                                <td >
                                    <input name="itemCant" type='text' id="itemCant" size='4' maxlength="3" value="1">
                                </td>
                            </tr>
                            <tr class='listado2'>
                                <td  colspan="3" align="center">
                                    <input name="button"  type="button" class="botones" value="Grabar" onclick='grabarItem();'>                
                                  <!--   <input name="button"  type="button" class="botones" value="Modificar" onclick='modItem("edificioDiv");'>-->
                                    <input name="button" class="botones" type="reset"" value="Cancelar" onclick='vistaFormUnitid("crearItem", 2);'></td>

                            </tr>
                        </table>



                    </div>
                    <div id='modItem' ></div>

                </td></tr>
        </table>

    </form>
</body>
</html>
