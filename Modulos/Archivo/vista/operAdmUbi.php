<?php session_start();
date_default_timezone_set('America/Bogota');
date_default_timezone_set('America/Bogota');
foreach ($_GET as $key => $valor)
    $$key = $valor;
foreach ($_POST as $key => $valor)
    $$key = $valor;
$krd = $_SESSION ["krd"];
$dependencia = $_SESSION ["dependencia"];
$usua_doc = $_SESSION ["usua_doc"];
$codusuario = $_SESSION ["codusuario"];
$ruta_raiz = "../../..";
if (isset($_SERVER['HTTP_X_FORWARD_FOR'])) {
    $proxy = $_SERVER['HTTP_X_FORWARD_FOR'];
} else
    $proxy = $_SERVER['REMOTE_ADDR'];
$REMOTE_ADDR = $_SERVER['REMOTE_ADDR'];
include_once "$ruta_raiz/core/clases/log.php";
$log = new log($ruta_raiz);
$log->setAddrC($REMOTE_ADDR);
$log->setProxyAd($proxy);
$log->setUsuaCodi($codusuario);
$log->setDepeCodi($dependencia);
$log->setRolId($_SESSION['id_rol']);

if (!$fecha_busq)
    $fecha_busq = Date('Y-m-d');
if (!$fecha_busq2)
    $fecha_busq2 = Date('Y-m-d');

//print_r($_POST);
include_once $ruta_raiz . '/Modulos/Archivo/clases/archivo-class.php';

$accion = $_POST ['action'];
$archivo = new archivo($ruta_raiz);

if ($accion == 'grabarEdi') {

    $nombre = $_POST ['nombre'];
    $sigla = $_POST ['sigla'];
    $cont = $_POST ['cont'];
    $pais = $_POST ['pais'];
    $depto = $_POST ['dep'];
    $muni = $_POST ['muni'];
    $mensaje = "Crear - $nombre ";
    $mensaje .= $status = $archivo->crearEdi($nombre, $sigla, $cont, $pais, $depto, $muni);
    //$accion='listEdi';
    if (substr($status, 5) != 'Trans') {

        $log->setOpera("Crear archivo edificio $nombre - $sigla");
        $log->registroEvento();
    }

    echo "Transaccion realizada para visualizar dar click en actualizar el item Edificio ";
}
if ($accion == 'listEdi') {
    $edificio = $archivo->ConsultarEdi();
    $numndep = count($edificio);
    //$optionEdi="<option value=''>Todas Las dependencias</option>";
    for ($i = 0; $i < $numndep; $i ++) {
        $nomDepe = $edificio [$i] ["NOMBRE"];
        $sigla = $edificio [$i] ["SIGLA"];
        $codDepe = $edificio [$i] ["CODIGO"];
        if ($codDepe != '999')
            $optionEdi .= "<option value='$codDepe'>$nomDepe - $sigla </option>";
    }
    ?><select name="sEdi" id="sEdi" onclick="piso('sEdi')" size="10"
            class='select'>
        <option selected value=0>-- Seleccione --</option>
        <?php         echo $optionEdi;
        ?>
    </select>
    <br><?php     echo $mensaje;
}
if ($accion == 'listarItem') {
    //print_r($_POST);
    $tama = $_POST ['tama'];
    $padre = $_POST ['padre'];
    $edificio = $_POST ['edi'];
    $nomsel = $_POST ['nomsel'];
    $selected = $_POST ['selected'];
    switch ($nomsel) {
         case 'piso1' :
          $item2 = 'area';
          break;
        case 'area1' :
            $item2 = 'modulo';
            break;
        case 'modulo1' :
            $item2 = 'estantes';
            break;
        case 'estantes1' :
            $item2 = 'entrepano';
            break;
    }
    if ($tama == 0) {
        $size = "size='10'";
    } else {
        $size = '';
    }
    $itemList = $archivo->ConsultarItem($padre, $edificio);
    $numndep = count($itemList);
    if ($selected == 0) {
        $selected2 = ' selected ';
    }
    for ($i = 0; $i < $numndep; $i ++) {
        $nomDepe = $itemList [$i] ["NOMBRE"];
        $sigla = $itemList [$i] ["SIGLA"];
        $codDepe = $itemList [$i] ["CODIGO"];
        //	  			if($codDepe!='999')
        if ($selected == $codDepe)
            $selected1 = ' selected ';
        else
            $selected1 = ' ';
        $optionlist .= "<option $selected1 value='$codDepe'>$nomDepe - $sigla </option>";
    }
    echo "<select name='$nomsel' id='$nomsel' onchange=\"Item('$item2','$nomsel')\" $size class='select'>
                	<option $selected2 value=0>-- Seleccione --</option>
                	$optionlist
    				</select> <br>";
}

if ($accion == 'listarItems') {
    //print_r($_POST);// [codEdi] => 1 [codpiso] => 1 [codarea] => 0 [codmod] => 0
    $codarea = $_POST ['codarea'];
    $codpiso = $_POST ['codpiso'];
    $edificio = $_POST ['codEdi'];
    $nomsel = $_POST ['nomsel'];
    $codmod = $_POST ['codmod'];
    //$codarea = $codpiso;
    $itemListP = $archivo->ConsultarItem(0, $edificio);
    $itemListA = $archivo->ConsultarItem($codpiso, $edificio);
    $itemListM = $archivo->ConsultarItem($codarea, $edificio);
    $numndepP = count($itemListP);
    $numndepA = count($itemListA);
    $numndepM = count($itemListM);
    if ($selected == 0) {
        $selected2 = ' selected ';
    }
    for ($i = 0; $i < $numndepP; $i ++) {
        $nomDepe = $itemListP [$i] ["NOMBRE"];
        $sigla = $itemListP [$i] ["SIGLA"];
        $codDepe = $itemListP [$i] ["CODIGO"];
        if ($codpiso == $codDepe)
            $selected1 = ' selected ';
        else
            $selected1 = ' ';
        $optionlistP .= "<option $selected1 value='$codDepe'>$nomDepe - $sigla </option>";
    }
    for ($i = 0; $i < $numndepA; $i ++) {
        $nomDepe = $itemListA [$i] ["NOMBRE"];
        $sigla = $itemListA [$i] ["SIGLA"];
        $codDepe = $itemListA [$i] ["CODIGO"];
        if ($codarea == $codDepe)
            $selected1 = ' selected ';
        else
            $selected1 = ' ';
        $optionlistA .= "<option $selected1 value='$codDepe'>$nomDepe - $sigla </option>";
    }
    for ($i = 0; $i < $numndepM; $i ++) {
        $nomDepe = $itemListM [$i] ["NOMBRE"];
        $sigla = $itemListM [$i] ["SIGLA"];
        $codDepe = $itemListM [$i] ["CODIGO"];
        if ($codmod == $codDepe)
            $selected1 = ' selected ';
        else
            $selected1 = ' ';
        $optionlistM .= "<option $selected1 value='$codDepe'>$nomDepe - $sigla </option>";
    }
    ?><table>
        <tr>
            <th class="titulos2" width="70">Piso</th>
            <td width="19%">
                <div id='piso'><select name="piso1" class="select" id='piso1'
                                       onchange="Item('area', 'piso1')">
                        <option value="0">-- Seleccione --</option>
                        <?php                         echo $optionlistP;
                        ?>
                    </select></div>
            </td>
        </tr>
        <tr>
                <th class="titulos2" width="70">&Aacute;rea</th>
                <td width="19%">
                <div id='area'><select name="area1" class="select" id='area1'
                        onchange="Item('modulo','area')">
                        <option value="0">-- Seleccione --</option>
        <?php         echo $optionlistA;
        ?>
          </select></div>
                </td>
        </tr>
        <tr>
            <th class="titulos2" width="70">M&oacute;dulo</th>
            <td width="19%">
                <div id='modulo'><select name="modulo1" class="select" id='modulo1'>
                        <option value="0">-- Seleccione --</option>
                        <?php                         echo $optionlistM;
                        ?>
                    </select></div>
            </td>
        </tr>
    </table>
    <?php }

if ($accion == 'crearItem') {
    //print_r($_POST);
    $nombre = $_POST ['nombre'];
    $sigla = $_POST ['sigla'];
    $padre = $_POST ['padre'];
    $edificio = $_POST ['edificio'];
    $cantidad = $_POST ['cantidad'];

    $mensaje = "Crear - $nombre ";
    $mensaje .= $status = $archivo->crearItem($nombre, $sigla, $padre, $edificio, $cantidad);
    if (substr($status, 5) != "Trans") {
        $log->setAction('create_item_in_building');
        $log->setOpera("Creado $nombre en edificio $edificio");
        $log->registroEvento();
    }
    echo "Transaccion realizada para visualizar dar click en actualizar del item ";
}

if ($accion == 'itemMod') {
    $Titulo = $_POST ['Titulo'];
    $codigo = $_POST ['codItem'];
    $itemList = $archivo->ConsulItem($codigo);
    //print_r($itemList);
    ?>
    <table class="borde_tab" width="100%">
        <tr>
            <td class="titulos5" style="color: red; font-size: 12">
                <div id='resultItem2'></div>
            </td>
        </tr>
    </table>
    <br>
    <table class="borde_tab">
        <tr>
            <td class="titulos4" colspan="6">Modificar <?php                 echo $Titulo;
                ?></td>
        </tr>
        <tr class="titulos5">
            <td>Nombre</td>
            <td>Sigla</td>
        </tr>
        <tr>
            <td><input name="itemName2" type='text' id="itemName2" maxlength="40"
                       value='<?php                        echo $itemList ["NOMBRE"];
                       ?>'> <input name="itemPadre2" type="hidden" id="itemPadre2"
                       value='<?php                        echo $itemList ["PADRE"];
                       ?>'> <input name="itemCod2" type="hidden" id="itemCod2"
                       value='<?php                        echo $itemList ["CODIGO"];
                       ?>'></td>
            <td><input name="itemSigla2" type='text' id="itemSigla2" size='4'
                       maxlength="4" value='<?php                        echo $itemList ["SIGLA"];
                       ?>'></td>
            <td></td>
        </tr>
        <tr class='listado2'>
            <td colspan="3" align="center"><input name="button" type="button"
                                                  class="botones" value="Modificar" onclick='itemMod();'> <input
                                                  name="button" class="botones" type="reset" " value="Cancelar"
                                                  onclick='document.getElementById("modItem").innerHTML = "";'></td>

        </tr>
    </table>
    <?php }
if ($accion == 'ModItem') {
    $codigo = $_POST ['codItem'];
    $sigla = $_POST ['sigla'];
    $nombre = $_POST ['name'];
    $mensaje = "Modificar - $nombre - $sigla : ";
    $mensaje .= $archivo->modItem($codigo, $nombre, $sigla);
    echo "$mensaje ";
}
if ($accion == 'ModEdi') {
    //print_r($_POST);
    $codigo = $_POST ['cod'];
    $sigla = $_POST ['sigla'];
    $nombre = $_POST ['name'];
    $cont = $_POST ['cont'];
    $pais = $_POST ['pais'];
    $dpto = explode('-', $_POST ['depto']);
    $mni = explode('-', $_POST ['muni']);
    $depto = $dpto[1];
    $muni = $mni[2];
    $mensaje = "Modificar - $nombre - $sigla : ";
    $mensaje .= $status = $archivo->modEdi($codigo, $nombre, $sigla, $cont, $pais, $depto, $muni);
    if (substr($status, 5) != 'Trans') {
        $log->setAction('record_building_modification');
        $log->setOpera("Modificado Edificio $nombre");
        $log->registroEvento();
    }
    echo "$mensaje ";
}



if ($accion == 'eliItem') {
    //print_r($_POST);
    $codigo = $_POST ['codItem'];
    $sigla = $_POST ['sigla'];
    $nombre = $_POST ['name'];
    $edificio = $_POST ['edi'];
    $mensaje = "Eliminar - $nombre - $sigla : ";
    $mensaje .= $status = $archivo->eliItem($codigo, $edificio);
    if (substr($status, 5) != 'Trans') {
        $log->setAction('record_delete_item_from_building');
        $log->setOpera("Borrado de item $nombre de edifcio $edificio");
        $log->registroEvento();
    }
    echo "$mensaje ";
}
if ($accion == 'eliEdi') {
    //print_r($_POST);
    $codigo = $_POST ['cod'];
    $sigla = $_POST ['sigla'];
    $nombre = $_POST ['name'];
    $mensaje = "Eliminar - $nombre - $sigla : ";
    $mensaje .= $archivo->eliEdi($codigo);
    if (substr($status, 5) != 'Trans') {
        $log->setAction('record_delete_building');
        $log->setOpera("Borrado edifcio $nombre");
        $log->registroEvento();
    }
    echo "$mensaje ";
}

if ($accion == 'itemEli') {
    $Titulo = $_POST ['Titulo'];
    $codigo = $_POST ['codItem'];
    $itemList = $archivo->ConsulItem($codigo);
    //print_r($_POST);
    //print_r($itemList);
    ?>
    <table class="borde_tab" width="100%">
        <tr>
            <td class="titulos5" style="color: red; font-size: 12">
                <div id='resultItem2'></div>
            </td>
        </tr>
    </table>
    <br>
    <table class="borde_tab">
        <tr>
            <td class="titulos4" colspan="6">Eliminar <?php                 echo $Titulo;
                ?></td>
        </tr>
        <tr class="titulos5">
            <td>Nombre</td>
            <td>Sigla</td>
        </tr>
        <tr>
            <td><input name="itemName2" type='text' readonly="readonly"
                       id="itemName2" maxlength="40"
                       value='<?php                        echo $itemList ["NOMBRE"];
                       ?>'> <input name="itemPadre2" type="hidden" id="itemPadre2"
                       value='<?php                        echo $itemList ["PADRE"];
                       ?>'> <input name="itemCod2" type="hidden" id="itemCod2"
                       value='<?php                        echo $itemList ["CODIGO"];
                       ?>'> <input name="itemCod2" type="hidden" id="itemEdi2"
                       value='<?php                        echo $itemList ["CODEDI"];
                       ?>'></td>
            <td><input name="itemSigla2" type='text' readonly="readonly"
                       id="itemSigla2" size='4' maxlength="4"
                       value='<?php                        echo $itemList ["SIGLA"];
                       ?>'></td>
            <td></td>
        </tr>
        <tr class='listado2'>
            <td colspan="3" align="center"><input name="button" id="btEliIt"
                                                  type="button" class="botones" value="Eliminar" onclick='itemEli();'>
                <input name="button" class="botones" type="reset" " value="Cancelar"
                       onclick='document.getElementById("modItem").innerHTML = "";'></td>

        </tr>
    </table>
    <?php }

if ($accion == 'relMod') {
    //print_r ( $_POST );
    $codigo = $_POST ['codRel'];
    $edificio = $_POST ['edi'];
    $depe = $_POST ['depe'];
    $piso = $_POST ['piso'];
    $area = $_POST ['area'];
    $modulo = $_POST ['modulo'];
    $item = $_POST ['item'];
    $mensaje = "Modificar Relacion : ";
    $mensaje .= $archivo->modRelacionEdiDepe($codigo, $edificio, $depe, $item, $modulo, $piso, $area);
    echo "$mensaje ";
}

if ($accion == 'relEli') {
    //print_r ( $_POST );
    $codigo = $_POST ['codRel'];
    $mensaje = "Eliminar Relaci&oacute;n : ";
    $mensaje .= $archivo->eliRela($codigo);
    echo "$mensaje ";
}
if ($accion == 'relGra') {
    //print_r ( $_POST ); //Array ( [action] => relGra [edi] => 1 [depe] => 900 [piso] => 1 [area] => 6 [modulo] => 8 [item] => 1 )
    $edi = $_POST ['edi'];
    $depe = $_POST ['depe'];
    $piso = $_POST ['piso'];
    $area = $_POST ['area'];
    $modulo = $_POST ['modulo'];
    $item = $_POST ['item'];
    $mensaje = "Relacionar  ";
    $mensaje .= $archivo->crearRelacionEdiDepe($edi, $depe, $item, $modulo, $piso, $area);
    echo "$mensaje ";
}
//crear tipo de caja
if ($accion == 'creartpcj') {
    //print_r ( $_POST ); //[tipo] => 0 [nomb] => caja [descrip] => prueba [tama] => 3
    $nombre = $_POST ['nomb'];
    $descrp = $_POST ['descrip'];
    $tama = $_POST ['tama'];
    $tipo = $_POST ['tipo'];
    $mensaje = "Adicionar Tipo de caja  ";
    $mensaje .= $archivo->creartp($nombre, $descrp, $tama, $tipo);
    echo "$mensaje ";
}

//modificar tipo de almacenaje


if ($accion == 'moditpal') {
    //print_r ( $_POST );
    $codigo = $_POST ['idcod'];
    $nombre = $_POST ['nomb'];
    $descrp = $_POST ['descrip'];
    $tama = $_POST ['tama'];
    $tipo = $_POST ['tipo'];
    $mensaje = "Modificar Tipo de Almacenaje : ";
    $mensaje .= $archivo->modtp($codigo, $nombre, $descrp, $tama, $tipo);
    echo "$mensaje ";
}

//modificar tiyulo expediente


if ($accion == 'modTitulo') {
    //print_r ( $_POST );
    $titulo = trim($_POST ['titulo']);
    $numExpediente = $_POST ['exp'];
    $mensaje = "<textarea  name='tituloExp' id='tituloExp' class='select' size='15'>$titulo</textarea>";
    $mensaje .= $archivo->TituloExpediente($numExpediente, $titulo);
    echo "$mensaje";
}
if ($accion == 'modTitulo2') {
    //print_r ( $_POST );
    $titulo = trim($_POST ['titulo']);
    $numExpediente = $_POST ['exp'];
    $mensaje = "<input name=\"nomb_expediente\" class='select' type=\"text\" size=\"30\" maxlength=\"50\" id='nom_expediente' value=\"$titulo\"  readonly>";
    $mensaje .= $archivo->TituloExpediente($numExpediente, $titulo);
    $log->setDenomDoc($doc = 'Expediente');
    $log->setAction('record_modification');
    $log->setOpera("Cambio al titulo del expediente");
    $log->setNumDocu($numExpediente);
    $log->registroEvento();
    //echo "$mensaje";
    echo $titulo;
}
if ($accion == 'incluirExp') {
    //print_r ( $_POST );
    $valor = $_POST['valor1'];
    $numRAdicado = $_POST['radicado'];
    $numExpediente = $_POST['exp'];
    $numcaja = $_POST['numcj1'];
    $numuc = $_POST['numuc'];
    $mensaje .= $archivo->IncluirExpediente($numExpediente, $numRAdicado, $numcaja, $numuc);
    $log->setDenomDoc('Radicado');
    $log->setAction('document_phisically_located');
    $log->setOpera("Documento (re-)Ubicado en Fisico en Caja $numcaja Unidad de Conservacion $numuc y Expediente $numExpediente");
    $log->setNumDocu($numRAdicado);
    $log->registroEvento();
    echo "<table  width='100%'><tr class='leidos'><td width='30%'>Incluido</td><td>";
    echo $seleccion = " <input type='button' value='Modificar' onclick='incluir($valor,$numRAdicado)' class='botones'></td></tr></table>";
}
if ($accion == 'addUbicacion') {
//	print_r ( $_POST ); //Array ( [action] => addUbicacion [exp] => 2011900989805592E [udc] => 6 [tpcaja] => 1 [Edi] => 1 [piso] => 1 [area] => 6 [modulo] => 8 [estantes] => 13 [entrepano] => 19 [posicion] => 1 [numcj] => 454 )
    $exp_numero = $_POST ['exp'];
    $posicion = $_POST ['posicion'];
    $tpcaja = $_POST ['tpcaja'];
    $tpcarpeta = $_POST ['udc'];
    $edificio = $_POST ['Edi'];
    $piso = $_POST ['piso'];
    $area = $_POST ['area'];
    $modulo = $_POST ['modulo'];
    $estante = $_POST ['estantes'];
    $entrepano = $_POST ['entrepano'];
    $codcaja = $_POST ['numcj'];
    $mensaje2 = $archivo->addUbiExpediente($exp_numero, $posicion, $tpcaja, $tpcarpeta, $edificio, $piso, $area, $modulo, $estante, $entrepano, $codcaja);
    $log->setDenomDoc('Expediente');
    $log->setOpera($mensaje2);
    $log->setNumDocu($exp_numero);
    $log->registroEvento();
    //ubicardepu ( exp_numero, $object );
    echo "agregado con exito";
}
if ($accion == 'listUbicacion') {
    //print_r ( $_POST );
    $exp_numero = $_POST ['exp'];
    ubicardepu($exp_numero, $archivo);
}

function ubicardepu($numExp, $object) {
    //echo $numExp;
    $list = $object->datosUbiExpediente($numExp);

    //print_r($list);
    $numcount = count($list);
    //if()
    for ($i = 0; $i < $numcount; $i ++) {
        $nomcaja = $list [$i] ["NUMCAJA"];
        if ($i == 0)
            $select = 'selected';
        else
            $select = '';
        $optioncaja .= " <option value='$nomcaja' $select >$nomcaja</option>";
    }
    ?>
    <TABLE width="100%" border='0' cellspacing="0" class="borde_tab"
           style="border-collapse: collapse">
        <!-- <TR>
                <TD class='titulo1' colspan="2">Ubicacion Fisica</td>
        </TR>
        <TR>-->
        <TD class='titulos2' width="50">N&uacute;mero caja</td>
        <td class='titulos2'>
            <div align="right"><select style="width: 125px" id='cjnum'	name='cjnum' onchange="cambioU()" class="select">
                    <option value="0">-- Seleccione --</option>
                    <?php                     echo $optioncaja;
                    ?>
                </select></div>
        </td>
    </TR>
    </TABLE>
    <div id='detalleContenedor'>
        <?php         for ($i = 0; $i < $numcount; $i ++) {
            $nomcaja = trim($list [$i] ["NUMCAJA"]);
            $nomexp = $list [$i] ["NUMEXP"];
            $estubi = $list [$i] ["ESTUBI"];
            $edi = $list [$i] ["EDIFICIO"];
            $piso = $list [$i] ["PISO"];
            $area = $list [$i] ["AREA"];
            $mod = $list [$i] ["MODULO"];
            $esta = $list [$i] ["ESTANTE"];
            $entr = $list [$i] ["ENTREPANO"];
            $ref = $list [$i] ["REFERENCIA"];
            $tpcaja = $list [$i] ["TIPOCAJA"];
            $tpudc = $list [$i] ["TPUDC"];
            $posicion = $list [$i] ["ESTUBI"];
            if ($i == 0)
                $style = ' style= "display : block"';
            else
                $style = ' style= "display : none"';
            ?><div id='d<?php echo $nomcaja; ?>' <?php echo $style; ?>>
                <TABLE width="100%" border='0' cellspacing="0" class="borde_tab"
                       style="border-collapse: collapse">
                    <TR>
                        <TD class='titulo1' colspan="2">Ubicaci&oacute;n Fisica Caja N&uacute;mero <?php echo $nomcaja; ?></td>
                    </TR>
                    <TR>
                        <TD class='titulos2' width="50">Edificio</td>
                        <td class='titulos2'><?php                             echo $edi;
                            ?></td>
                    </TR>
                    <TR>
                        <TD class='titulos2'>Piso</td>
                        <td class='titulos2'>
                            <?php                             echo $piso;
                            ?></td>
                    </TR>
                    <TR>
                        <TD class='titulos2'>&Aacute;rea</td>
                        <td class='titulos2'>
                    <?php                     echo $area;
                    ?>
                        </td>
                    </TR>
                    <TR>
                        <TD class='titulos2'>M&oacute;dulo</td>
                        <td class='titulos2'><?php                             echo $mod;
                            ?></td>
                    </TR>
                    <TR>
                        <TD class='titulos2'>Estante</td>
                        <td class='titulos2'><?php                             echo $esta;
                            ?></td>
                    </TR>
                    <TR>
                        <TD class='titulos2'>Entrepa&ntilde;o</td>
                        <td class='titulos2'><?php                             echo $entr;
                            ?></td>
                    </TR>
                    <TR>
                        <TD class='titulos2'>Posici&oacute;n</td>
                        <td class='titulos2'><?php                             echo $posicion;
                            ?></td>
                    </TR>
                    <tr class='titulos2'>
                        <td>Nro de Ref.</td>
                        <td> <?php                             echo $ref;
                            ?></td>
                    </tr>
                    <TR>
                        <TD class='titulos2'>U. de conservaci&oacute;n</td>
                        <td class="titulos2"><?php                             echo $tpudc;
                            ?></td>
                    </TR>
                    <TR>
                        <TD class='titulos2'>Tipo de Caja</td>
                        <td class='titulos2'><?php                             echo $tpcaja;
                            ?></td>
                    </TR>
                </TABLE>  </div> <?php         }
        ?>

    </div>
    <table  width="100%" border='0' cellspacing="0" class="borde_tab"
            style="border-collapse: collapse">
        <TR>
            <TD class='titulos2' colspan="2">
                <div align="center"><input type="button" class="botones"
                                           value="Modificar"> <input type="button"
                                           class="botones" value="Agregar" onClick="agregarUbcacion()"></div>
            </td>
        </TR>
    </table>



    <?php }

if ($accion == 'listarItem2') {
    //print_r($_POST);
    $tama = $_POST ['tama'];
    $padre = $_POST ['padre'];
    $edificio = $_POST ['edi'];
    $nomsel = $_POST ['nomsel'];
    $selected = $_POST ['selected'];
    //$extra = 21;
    switch ($nomsel) {
        case 'piso21' :
            $item = 'area2';
            break;
        case 'area21' :
            $item = 'modulo2';
            break;
        case 'modulo21' :
            $item = 'estantes2';
            break;
        case 'estantes21' :
            $item = 'entrepano2';
            break;
    }
    if ($tama == 0) {
        $size = "size='10'";
    } else {
        $size = '';
    }
    $itemList = $archivo->ConsultarItem($padre, $edificio);
    $numndep = count($itemList);
    if ($selected == 0) {
        $selected2 = ' selected ';
    }
    for ($i = 0; $i < $numndep; $i ++) {
        $nomDepe = $itemList [$i] ["NOMBRE"];
        $sigla = $itemList [$i] ["SIGLA"];
        $codDepe = $itemList [$i] ["CODIGO"];
        //	  			if($codDepe!='999')
        if ($selected == $codDepe)
            $selected1 = ' selected ';
        else
            $selected1 = ' ';
        $optionlist .= "<option $selected1 value='$codDepe'>$nomDepe - $sigla </option>";
    }

    if ($item == '') {
        echo "<select name='$nomsel' id='$nomsel' onchange=\"caja('cajaY','$nomsel','cajan')\" $size class='select'>
                	<option $selected2 value=0>-- Seleccione --</option>
                	$optionlist
    				</select> <br>";
    } else {
        echo "<select name='$nomsel' id='$nomsel' onchange=\"Item2('$item','$nomsel')\" $size class='select'>
                	<option $selected2 value=0>-- Seleccione --</option>
                	$optionlist
    				</select> <br>";
    }
}

if ($accion == 'listarItem3') {
    //print_r($_POST);
    $tama = $_POST ['tama'];
    $padre = $_POST ['padre'];
    $edificio = $_POST ['edi'];
    $nomsel = $_POST ['nomsel'];
    $selected = $_POST ['selected'];
    switch ($nomsel) {
        case 'piso1' :
            $item2 = 'area';
            break;
        case 'area1' :
            $item2 = 'modulo';
            break;
        case 'modulo1' :
            $item2 = 'estantes';
            break;
        case 'estantes1' :
            $item2 = 'entrepano';
            break;
    }
    if ($tama == 0) {
        $size = "size='10'";
    } else {
        $size = '';
    }
    $itemList = $archivo->ConsultarItem($padre, $edificio);
    $numndep = count($itemList);
    if ($selected == 0) {
        $selected2 = ' selected ';
    }
    for ($i = 0; $i < $numndep; $i ++) {
        $nomDepe = $itemList [$i] ["NOMBRE"];
        $sigla = $itemList [$i] ["SIGLA"];
        $codDepe = $itemList [$i] ["CODIGO"];
        //	  			if($codDepe!='999')
        if ($selected == $codDepe)
            $selected1 = ' selected ';
        else
            $selected1 = ' ';
        $optionlist .= "<option $selected1 value='$codDepe'>$nomDepe - $sigla </option>";
    }
    if ($item2 == '') {
        echo "<select name='$nomsel' id='$nomsel' onchange=\"caja('cajaX','$nomsel','cajam')\" $size class='select'>
                	<option $selected2 value=0>-- Seleccione --</option>
                	$optionlist
    				</select> <br>";
    } else {
        echo "<select name='$nomsel' id='$nomsel' onchange=\"Item('$item2','$nomsel')\" $size class='select'>
                	<option $selected2 value=0>-- Seleccione --</option>
                	$optionlist
    				</select> <br>";
    }
}
if ($accion == 'cajaO') {
    //print_r($_POST);
    $numEnt = $_POST['entre'];
    $nomCaja = $_POST['nomcamp'];
    $itemList = $archivo->consultaCsjas($numEnt);
    $numndep = count($itemList);
    /* 	$combi [$i] ["CODIGO"] = $rs->fields ['CODCAJA'];
      $combi [$i] ["TAMANO"] = $rs->fields ['TAMANO'];
      $combi [$i] ["ESTUBI"] = $rs->fields ['CODRPCAJA'];
      $combi [$i] ["CODRPCAJA"] = $rs->fields ['UBICACION'];
      $combi [$i] ["NOMBTIPOCJ"] = $rs->fields ['NOMBTIPOCJ']; */
//	print_r($itemList);
    for ($i = 1; $i <= $numndep; $i ++) {
        $nomDepe = $itemList [$i] ["CODIGO"];
        $codDepe = $itemList [$i] ["CODIGO"];
        $posi = $itemList [$i] ["UBICACION"];
        //	  			if($codDepe!='999')
        //if ($posi == $i)
        $optionlist .= "<option $selected1 value='$codDepe'>$posi-$nomDepe</option>";

        //else
        //$optionlist .= "<option $selected1 value='$i'>$posi- </option>";
    }

    echo "<select name='$nomCaja' id='$nomCaja' size=4  class='select'>
                	<option  value=0>-- Seleccione --</option>
                	$optionlist
    				</select> ";
}
if ('changeUbica' == $accion) {
//print_r($_POST);
// [caja] => 454 [Edi] => 1 [piso] => 1 [area] => 6 [modulo] => 8 [estantes] => 13 [entrepano] => 19
//[Edi2] => 1 [piso2] => 1 [area2] => 6 [modulo2] => 8 [estantes2] => 13 [entrepano2] => 20 [posicion] => 1 [numref] =>
    $edi = $_POST['Edi'];
    $piso = $_POST['piso'];
    $area = $_POST['area'];
    $modulo = $_POST['modulo'];
    $estante = $_POST['estantes'];
    $entrepano = $_POST['entrepano'];
    $numcaja = $_POST['caja'];
    $nref = $_POST['numref'];
    $edi2 = $_POST['Edi2'];
    $piso2 = $_POST['piso2'];
    $area2 = $_POST['area2'];
    $modulo2 = $_POST['modulo2'];
    $estante2 = $_POST['estantes2'];
    $entrepano2 = $_POST['entrepano2'];
    $nref2 = $_POST['numref2'];
    $pos = $_POST['posicion'];
    $Result = $archivo->cambioColecion($edi, $piso, $area, $modulo, $estante, $entrepano, $numcaja, $nref, $edi2, $piso2, $area2, $modulo2, $estante2, $entrepano2, $nref2, $pos);
	if (substr($Result,-3)=='ito'){
	    $numExpediente=$archivo->getExpediente();
	    $numCaja=$archivo->getNumCaja();
	    $log->setDenomDoc('Expediente');
	        $log->setAction('record_box_modification');
            $log->setOpera("Expediente caja $numCaja fisica ha tenido un cambio de coleccion");
            $log->setNumDocu($numExpediente);
            $log->registroEvento();
	}
    echo $Result . " visualizar el  cambio <a href='#2' onclick=\"caja('cajaX','entrepano1','cajam');
	 caja('cajaY','entrepano21','cajan');\">Actualizar</a>";
}
