<?php 
/** 
 * archivo es la clase encargada de gestionar las operaciones de archivo
 * @author Hardy Deimont Niño  Velasquez
 * @name modeloUsuarioOrfeo
 * @version	1.0
 */
if (! $ruta_raiz)
	$ruta_raiz = '../../..';
include_once "$ruta_raiz/Modulos/Archivo/modelo/modeloArchivo.php";
//==============================================================================================	
// CLASS usuarioOrfeo
//==============================================================================================	


/**
 * Objecto UsuarioOrfeo. 
 */

class archivo {
	public $ruta_raiz;
	private $modelo;
	private $expediente; //login de  usuario
	private $radicado; //nombre y  apellidos del  usuario
	private $fechaI; //password actual
	private $fechaF; //codigo del  rol
	private $lista; //codigo de la dependencia 
	private $depe; //fecha de  creacion
        private $faro; //fecha de  creacion
        private $matrix; //fecha de  creacion
	private $NumCaja;//Numero de caja asignada a Expediente
	
	public function setNumCaja($NumCaja){
	    $this->NumCaja=$NumCaja;
	}

	public function getNumCaja(){
	    return $this->NumCaja;
	}
	
        public function getFaro() {
            return $this->faro;
        }

        public function getMatrix() {
            return $this->matrix;
        }

        public function setFaro($faro) {
            $this->faro = $faro;
        }

        public function setMatrix($matrix) {
            $this->matrix = $matrix;
        }

	
	
	/**
	 * @return the $expediente
	 */
	public function getExpediente() {
		return $this->expediente;
	}

	/**
	 * @return the $radicado
	 */
	public function getRadicado() {
		return $this->radicado;
	}

	/**
	 * @return the $fechaI
	 */
	public function getFechaI() {
		return $this->fechaI;
	}

	/**
	 * @return the $fechaF
	 */
	public function getFechaF() {
		return $this->fechaF;
	}

	/**
	 * @return the $lista
	 */
	public function getLista() {
		return $this->lista;
	}

	/**
	 * @return the $depe
	 */
	public function getDepe() {
		return $this->depe;
	}

	/**
	 * @param field_type $expediente
	 */
	public function setExpediente($expediente) {
		$this->expediente = $expediente;
	}

	/**
	 * @param field_type $radicado
	 */
	public function setRadicado($radicado) {
		$this->radicado = $radicado;
	}

	/**
	 * @param field_type $fechaI
	 */
	public function setFechaI($fechaI) {
		$this->fechaI = $fechaI;
	}

	/**
	 * @param field_type $fechaF
	 */
	public function setFechaF($fechaF) {
		$this->fechaF = $fechaF;
	}

	/**
	 * @param field_type $lista
	 */
	public function setLista($lista) {
		$this->lista = $lista;
	}

	/**
	 * @param field_type $depe
	 */
	public function setDepe($depe) {
		$this->depe = $depe;
	}

	function __construct($ruta_raiz) {
		$this->ruta_raiz = $ruta_raiz;
		/*$this->usuario=$clave;
		$this->clave=$usu;*/
		$this->modelo = new modeloArchivo( $this->ruta_raiz );
	}
	
     /**
	 * buscar  exp
	 */
	function buscar() {		
		return $this->modelo->buscar ( $this->expediente,$this->radicado ,$this->fechaI,$this->fechaF ,$this->lista,$this->depe);
	}
	 /**
	 * buscar  avanzada exp
	 */
	function buscarAvan($edificio,$piso,$area,$modulo,$estante,$entrepano) {		
		//return $this->modelo->buscar ( $this->expediente,$this->radicado ,$this->fechaI,$this->fechaF ,$this->lista,$this->depe);
		return $this->modelo->buscarAvan($this->expediente,$this->depe, $edificio,$piso,$area,$modulo,$estante,$entrepano,$this->fechaI,$this->fechaF );
	}
	
	 
	/** 
	 * Limpia los atributos de la instancia referentes a la informacion del usuario
	 * @return   void
	 */
	function __destruct() {

	}

	function crearEdi($nombre,$sigla,$cont,$pais,$depto,$muni) {
		$dpto=explode('-', $depto);
		$mni=explode('-', $muni);
		$this->modelo->crearEdificio($nombre,$sigla,$cont,$pais,$dpto[1],$mni[2]);
	}
	function ConsultarEdi($codigo = Null,$nombre = Null, $sigla = Null) {
		return $this->modelo->ConsultarEdi($codigo,$nombre, $sigla);
	}
    function ConsultarItem($padre,$edificio) {
		return $this->modelo->ConsultarItem($padre,$edificio);
	}
    function ConsulItem($codigo) {
		return $this->modelo->ConsulItem($codigo);
	}	
	function  consultaCsjas($numEnt){
		return $this->modelo->consultaCsjas($numEnt);
	}	
	function crearItem($nombre,$sigla,$padre,$edificio,$cantidad) {
		if($cantidad==1){
		   $this->modelo->crearItem($nombre,$sigla,$padre,$edificio);
		}
		else{
		for($i=1;$i<=$cantidad;$i++){
		    $this->modelo->crearItem($nombre." ".$i,$sigla.$i,$padre,$edificio);
		}
			
		}
	}
	
function modItem($codigo,$nombre,$sigla) {
		$data=$this->ConsulItem($codigo);
		if($nombre==$data['NOMBRE'] && $sigla==$data['SIGLA']){
			return 'Transacci&oacute;n No realizada, Debe Cambiar alg&uacute;n dato';
		}
		$this->modelo->modItem($codigo,$nombre,$sigla);
		$data2=$this->ConsulItem($codigo);
        if($data['NOMBRE']==$data2['NOMBRE'] && $data['CODIGO']==$data2['SIGLA']){
			return 'Transacci&oacute;n No realizada';
		}
		else{
			return 'Modificaci&oacute;n Realizada con &eacute;xito';
			
		}	
		
	}
function eliItem($codigo,$edificio) {
	    $data=$this->modelo->ConsultarItem($codigo,$edificio);
	    //echo count($data);
		if(count($data)==0){
			$this->modelo->eliItem($codigo);
			return 'Eliminaci&oacute;n Realizada con &eacute;xito';
		}
		else{
			return 'Transacci&oacute;n No realizada, Debido el item tiene sub-items ';			
		}
		
	}
	
function	eliEdi ( $codigo){
	    $data=$this->modelo->ConsultarItem(0,$codigo);
	    //echo count($data);
		if(count($data)==0){
			$this->modelo->eliEdi($codigo);
			return 'Eliminaci&oacute;n Realizada con &eacute;xito';
		}
		else{
			return 'Transacci&oacute;n No realizada, Debido el item tiene sub-items ';			
		}
		
	}
function modEdi($codigo, $nombre, $sigla, $cont, $pais, $depto, $muni) {
		$data=$this->ConsultarEdi($codigo);
		//echo "$nombre==".$data[0]['NOMBRE']." && $sigla==".$data['SIGLA']." &&  $cont==".$data['CONTINENTE']." && $pais==".$data['PAIS']."&& $depto==".$data['DEPTO']." && $muni==".$data['MUNI'];
		if($nombre==$data[0]['NOMBRE'] && $sigla==$data[0]['SIGLA'] &&  $cont==$data[0]['CONTINENTE'] && $pais==$data[0]['PAIS']
		 && $depto==$data[0]['DEPTO'] && $muni==$data[0]['MUNI'] ){
			return 'Transacci&oacute;n No realizada, Debe Cambiar alg&uacute;n dato';
		}
		$this->modelo->modEdi($codigo, $nombre, $sigla, $cont, $pais, $depto, $muni);
		$data2=$this->ConsultarEdi($codigo);
        if($data[0]['NOMBRE']==$data2[0]['NOMBRE'] && $data[0]['CODIGO']==$data2[0]['SIGLA'] &&  $data2[0]['CONTINENTE']==$data[0]['CONTINENTE'] && $data2[0]['PAIS']==$data[0]['PAIS']
		 && $data2[0]['DEPTO']==$data[0]['DEPTO'] && $data2[0]['MUNI']==$data[0]['MUNI']){
			return 'Transacci&oacute;n No realizada';
		}
		else{
			return 'Modificaci&oacute;n Realizada con &eacute;xito';
			
		}	
		
	}
	function ConsulRealEdivsDepe() {
		return $this->modelo->ConsulRealEdivsDepe();
	}
	
function ConsulTpAlmacen() {
		return $this->modelo->ConsulTpAlmacen();
	}
	function crearRelacionEdiDepe($edificio, $depe, $item,$modulo,$piso,$area) {
		return $this->modelo->crearRelacionEdiDepe($edificio, $depe, $item,$modulo,$piso,$area);
	}
	
	function modRelacionEdiDepe($codigo, $edificio, $depe, $item,$modulo,$piso,$area) {
		
		$data=$this->modelo->ConsulRelacion($codigo);
		/*print_r($data);
		echo "$codigo, $edificio, $depe, $item,$modulo,$piso,$area";*/
		if($edificio==$data['ECOD'] &&  $depe==$data['DCOD'] &&  $item==$data['ICOD'] && 
		$modulo==$data['CODMOD'] && $piso==$data['CODPISO'] && $area==$data['CODAREA'] 
		){
			return 'Transacci&oacute;n No realizada, Debe Cambiar alg&uacute;n dato';
		}
		$this->modelo->modRelacionEdiDepe($codigo, $edificio, $depe, $item,$modulo,$piso,$area);
		$data2=$this->ConsulItem($codigo);
        if($data['ECOD']==$data2['ECOD'] &&  $data2['DCOD']==$data['DCOD'] &&  $data2['ICOD']==$data['ICOD'] && 
		$data2['CODMOD']==$data['CODMOD'] && $data2['CODPISO']==$data['CODPISO'] && $data2['CODAREA']==$data['CODAREA'] ){
			return 'Transacci&oacute;n No realizada';
		}
		else{
			return 'Modificaci&oacute;n Realizada con &eacute;xito';
			
		}	
		
	}
       function eliRela($codigo) {
       	echo $codigo;
	    $data=$this->modelo->ConsulRelacion($codigo);
	    //echo count($data);
		if($data['CODIGO']==$codigo){
			$this->modelo->eliRelacionEdiDepe($codigo);
			return 'Realizada con &eacute;xito';
		}
		else{
			return 'Transacci&oacute;n No realizada ';			
		}
       }
       
	function creartp($nombre, $descrp, $tama, $tipo) {		
		   $this->modelo->crearTpAlmacen($nombre, $descrp, $tama, $tipo);
			
	}
	function modtp($codigo, $nombre,$descrp,$tama, $tipo) {		
		   $this->modelo->modTpAlmacen($codigo, $nombre,$descrp,$tama, $tipo);
			
	}
	function datosExpediente($numExpediente){
		return    $this->modelo->datosExpediente($numExpediente);
		
	}
	function TituloExpediente($numExpediente,$titulo){
		return    $this->modelo->TituloExpediente($numExpediente,$titulo);
		
	}
	
	function IncluirExpediente($numExpediente,$numrad,$numcaja,$numuc){
		return    $this->modelo->IncluirExpediente($numExpediente,$numrad,$numcaja,$numuc);
		
	}
	
	function DatosExpBasicos($codigo){
		return    $this->modelo->DatosExpBasicos($codigo);
	}
	function addUbiExpediente($exp_numero,$posicion,$tpcaja,$tpcarpeta,$edificio,$piso,$area,$modulo,$estante,$entrepano,$codcaja){
		$x=$this->modelo->addUbiExpediente($exp_numero,$posicion,$tpcaja,$tpcarpeta,$edificio,$piso,$area,$modulo,$estante,$entrepano,$codcaja);
		return  $this->modelo->datosUbiExpedienteXcaja($exp_numero,$codcaja);
	}
	
    function datosUbiExpediente($numExpediente){
        return    $this->modelo->datosUbiExpediente($numExpediente);
    } 
    

    function cambioColecion($edi, $piso,$area,$modulo,$estante,$entrepano,$numcaja,$nref,$edi2, $piso2,$area2,$modulo2,$estante2,$entrepano2,$nref2,$pos){
    	$i=1;
    	if($numcaja && $entrepano2){
    		$data= $this->modelo->consuldataCaja($edi, $piso,$area,$modulo,$estante,$entrepano,$numcaja);
    		$ueCod=$data [$i] ["CODIGO"];
		$expedientes=array();
		for($j=0;$j<count($data);$j++){
		    if(!in_array($data [$j]["EXPEDIENTE"],$expedientes))
			$expedientes[]=$data [$j]["EXPEDIENTE"];
		}
		$this->setExpediente($expedientes);
		$this->setNumCaja($data [$i]["CODCAJA"]);
			//print_r($data);
    		if($data [$i]["CODCAJA"]==$numcaja){
			$this->modelo->cambioCaja($ueCod, $numcaja, $edi2, $piso2,$area2,$modulo2,$estante2,$entrepano2,$nref2,$pos);
			return 'Modificaci&oacute;n Realizada con &eacute;xito';
    		}
    		else{
    			return 'Modificaci&oacute;n No Realizada';    			
    		}
    	}else{
    				return 'Modificaci&oacute;n No Realizada debe selecionar un item';
    	}
    	
           
    }
}
?>
