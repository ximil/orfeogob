<?php 
/** 
 * @author deimont
 * 
 * 
 */
if (! $ruta_raiz)
	$ruta_raiz = '../../../';
include_once "$ruta_raiz/core/clases/ConnectionHandler.php";

class modeloArchivo {
	//TODO - Insert your code here
	

	function __construct($ruta_raiz) {
		
		$db = new ConnectionHandler ( "$ruta_raiz" );
		$db->conn->SetFetchMode ( ADODB_FETCH_NUM );
		$db->conn->SetFetchMode ( ADODB_FETCH_ASSOC );
		$this->link = $db;
	}
	
	function buscar($expediente, $radicado, $fechaI, $fechaF, $lista, $depe) {
		$estaSql="";
		if ($expediente)
			$expSql = "and upper(d.sgd_exp_numero) like '%$expediente%'";
		if ($radicado)
			$radSql = "and cast(d.RADI_NUME_RADI as char(18)) like '%$radicado%'";
		if ($depe)
			$depeSql = "and d.depe_codi = $depe";
		if ($lista) {
			$estaSql .= "and d.sgd_exp_estado=$lista ";
		}
		if ($lista == 0) {
			$estaSql .= "and d.sgd_exp_estado!=1 and d.sgd_exp_estado!=2 ";
		
		}
		
		 $sql = "select d.sgd_exp_numero, d.sgd_exp_estado, d.RADI_NUME_RADI , a.RADI_NUME_HOJA, a.RADI_FECH_RADI,
               a.RA_ASUN , a.RADI_PATH , a.RADI_USUA_ACTU , TO_CHAR(a.RADI_FECH_RADI, 'DD-MM-YYYY HH24:MI AM') AS FECHA ,
               b.sgd_tpr_descrip, b.sgd_tpr_codigo, b.sgd_tpr_termino, RADI_LEIDO, RADI_TIPO_DERI, RADI_NUME_DERI, 
               a.radi_depe_actu, e.depe_nomb, f.usua_nomb,  d.sgd_exp_fech_arch, d.sgd_exp_fech , d.SGD_EXP_ASUNTO 
               ,radi_depe_actu,radi_usua_arch
               from radicado a, sgd_tpr_tpdcumento b, SGD_EXP_EXPEDIENTE d, DEPENDENCIA e, USUARIO f, SGD_SEXP_SECEXPEDIENTES g
               where f.usua_codi=a.radi_usua_actu  and e.depe_codi=a.radi_depe_actu 
               and a.tdoc_codi=b.sgd_tpr_codigo AND a.radi_nume_radi=d.radi_nume_radi  and d.sgd_exp_numero=g.sgd_exp_numero
               $expSql $radSql $depeSql $estaSql
               and d.sgd_exp_fech BETWEEN  to_timestamp('$fechaI 0:00:00','dd/mm/yyyy hh24:mi:ss')  and  to_timestamp('$fechaF 23:59:59' ,'dd/mm/yyyy hh24:mi:ss')
               order by d.sgd_exp_numero";
		
		$rs = $this->link->query ( $sql );
		$i = 0;
		while ( ! $rs->EOF ) {
			$combi [$i] ["SGD_EXP_NUMERO"] = $rs->fields ['SGD_EXP_NUMERO'];
			$combi [$i] ["SGD_EXP_ESTADO"] = $rs->fields ['SGD_EXP_ESTADO'];
			$combi [$i] ["RADI_PATH"] = $rs->fields ['RADI_PATH'];
			$combi [$i] ["RA_ASUN"] = $rs->fields ['RA_ASUN'];
			$combi [$i] ["RADI_FECH_RADI"] = $rs->fields ['RADI_FECH_RADI'];
			$combi [$i] ["RADI_NUME_HOJA"] = $rs->fields ['RADI_NUME_HOJA'];
			$combi [$i] ["RADI_NUME_RADI"] = $rs->fields ['RADI_NUME_RADI'];
			$combi [$i] ["RADI_USUA_ACTU"] = $rs->fields ['RADI_USUA_ACTU'];
			$combi [$i] ["FECHA"] = $rs->fields ['FECHA'];
			$combi [$i] ["SGD_TPR_DESCRIP"] = $rs->fields ['SGD_TPR_DESCRIP'];
			$combi [$i] ["SGD_TPR_CODIGO"] = $rs->fields ['SGD_TPR_CODIGO'];
			$combi [$i] ["SGD_TPR_TERMINO"] = $rs->fields ['SGD_TPR_TERMINO'];
			$combi [$i] ["RADI_LEIDO"] = $rs->fields ['RADI_LEIDO'];
			$combi [$i] ["RADI_TIPO_DERI"] = $rs->fields ['RADI_TIPO_DERI'];
			$combi [$i] ["RADI_NUME_DERI"] = $rs->fields ['RADI_NUME_DERI'];
			$combi [$i] ["RADI_DEPE_ACTU"] = $rs->fields ['RADI_DEPE_ACTU'];
			$combi [$i] ["DEPE_NOMB"] = $rs->fields ['DEPE_NOMB'];
			$combi [$i] ["USUA_NOMB"] = $rs->fields ['USUA_NOMB'];
			$combi [$i] ["SGD_EXP_FECH_ARCH"] = $rs->fields ['SGD_EXP_FECH_ARCH'];
			$combi [$i] ["SGD_EXP_FECH"] = $rs->fields ['SGD_EXP_FECH'];
			$combi [$i] ["SGD_EXP_ASUNTO"] = $rs->fields ['SGD_EXP_ASUNTO'];
			$combi [$i] ["RADI_USUA_ARCH"] = $rs->fields ['RADI_USUA_ARCH'];
			$i ++;
			$rs->MoveNext ();
		}
		return $combi;
	
	}
	
	function crearEdificio($nombre, $sigla, $cont, $pais, $depto, $muni) {
		$sec = $this->link->conn->nextId ( 'SEC_ARCH_EDIFICIO' );
		$sql = "INSERT INTO arch_edi_edificio
             ( arch_edi_codigo, arch_edi_nombre, arch_edi_sigla,codi_cont,codi_pais,codi_dpto,codi_muni)
	         VALUES( '$sec', upper('$nombre'),upper('$sigla'),$cont,$pais,$depto,$muni)";
		
		$listo = $this->link->conn->Execute ( $sql );
	}
	
	function ConsultarEdi($codigo = Null, $nombre = Null, $sigla = Null) {
		$where = '';
		if ($codigo) {
			$where = " where  arch_edi_codigo=$codigo";
		} elseif ($codigo) {
			$where = " where  arch_edi_nombre='$nombre'";
		} elseif ($codigo) {
			$where = " where  arch_edi_sigla='$sigla'";
		}
		$sql = "Select  arch_edi_codigo, arch_edi_nombre, arch_edi_sigla,codi_cont,codi_pais,codi_dpto,codi_muni 
              from arch_edi_edificio $where";
		$rs = $this->link->conn->Execute ( $sql );
		$i = 0;
		while ( ! $rs->EOF ) {
			$combi [$i] ["CODIGO"] = $rs->fields ['ARCH_EDI_CODIGO'];
			$combi [$i] ["NOMBRE"] = $rs->fields ['ARCH_EDI_NOMBRE'];
			$combi [$i] ["SIGLA"] = $rs->fields ['ARCH_EDI_SIGLA'];
			$combi [$i] ["CONTINENTE"] = $rs->fields ['CODI_CONT'];
			$combi [$i] ["PAIS"] = $rs->fields ['CODI_PAIS'];
			$combi [$i] ["DEPTO"] = $rs->fields ['CODI_DPTO'];
			$combi [$i] ["MUNI"] = $rs->fields ['CODI_MUNI'];
			$i ++;
			$rs->MoveNext ();
		}
		return $combi;
	}
	
	function ConsultarItem($padre, $edificio) {
		$sql = "select  arch_eit_codigo as codigo,arch_eit_cod_padre as padre , arch_eit_nombre as nombre, arch_eit_sigla as sigla, arch_eit_codiedi as codedi
from arch_eit_items  where arch_eit_cod_padre=$padre and arch_eit_codiedi=$edificio order by arch_eit_codigo asc";
		$rs = $this->link->query ( $sql );
		$i = 0;
		while ( ! $rs->EOF ) {
			$combi [$i] ["CODIGO"] = $rs->fields ['CODIGO'];
			$combi [$i] ["NOMBRE"] = $rs->fields ['NOMBRE'];
			$combi [$i] ["SIGLA"] = $rs->fields ['SIGLA'];
			$combi [$i] ["PADRE"] = $rs->fields ['PADRE'];
			$combi [$i] ["CODEDI"] = $rs->fields ['CODEDI'];
			$i ++;
			$rs->MoveNext ();
		}
		return $combi;
	}
	
	function ConsulItem($codigo) {
		$sql = "select  arch_eit_codigo codigo,arch_eit_cod_padre padre , arch_eit_nombre nombre, arch_eit_sigla sigla, arch_eit_codiedi codedi
                 from arch_eit_items  where arch_eit_codigo=$codigo order by arch_eit_nombre";
		$rs = $this->link->query ( $sql );
		$i = 0;
		if (! $rs->EOF) {
			$combi ["CODIGO"] = $rs->fields ['CODIGO'];
			$combi ["NOMBRE"] = $rs->fields ['NOMBRE'];
			$combi ["SIGLA"] = $rs->fields ['SIGLA'];
			$combi ["PADRE"] = $rs->fields ['PADRE'];
			$combi ["CODEDI"] = $rs->fields ['CODEDI'];
			$i ++;
			$rs->MoveNext ();
		}
		return $combi;
	}
	function eliItem($codigo) {
		$sql = "delete from arch_eit_items  where arch_eit_codigo=$codigo ";
		$rs = $this->link->query ( $sql );
	
	}
	function eliEdi($codigo) {
		$sql = "delete from arch_edi_edificio  where arch_edi_codigo=$codigo ";
		$rs = $this->link->query ( $sql );
	
	}
	
	function crearItem($nombre, $sigla, $padre, $edificio) {
		$sq2l = "select  max(arch_eit_codigo) codigo from arch_eit_items";
		$rs = $this->link->query ( $sq2l );
		$codigo = $rs->fields ['CODIGO'] + 1;
		$sql = "INSERT INTO arch_eit_items
             (arch_eit_codigo,arch_eit_cod_padre,arch_eit_nombre,arch_eit_sigla,arch_eit_codiedi)
	         VALUES( $codigo,$padre, upper('$nombre'),upper('$sigla'),$edificio)";
		$listo = $this->link->conn->Execute ( $sql );
	}
	
	function modItem($codigo, $nombre, $sigla) {
		$sq2l = "update  arch_eit_items set arch_eit_nombre='$nombre' , arch_eit_sigla='$sigla'
                    where arch_eit_codigo=$codigo";
		$rs = $this->link->query ( $sq2l );
	}
	
	function modEdi($codigo, $nombre, $sigla, $cont, $pais, $depto, $muni) {
		$sq2l = "update  arch_edi_edificio set arch_edi_nombre='$nombre' , arch_edi_sigla='$sigla',
		codi_cont=$cont,codi_pais=$pais,codi_dpto=$depto,codi_muni=$muni
                    where arch_edi_codigo=$codigo";
		$rs = $this->link->query ( $sq2l );
	}
	
	function ConsulRealEdivsDepe() {
		$sql = "select d.depe_nomb as dnomb,ed.arch_edi_nombre enomb,it.arch_eit_nombre inomb,
  	               evd.arch_evd_id codigo,evd.arch_evd_depe dcod,evd.arch_evd_edificio ecod,evd.arch_evd_item icod,arch_evd_modulo codmod,arch_evd_piso codpiso, arch_evd_area codarea
 from arch_evd_edivsdep evd,arch_eit_items it, arch_edi_edificio ed,dependencia d
where  evd.arch_evd_edificio=ed.arch_edi_codigo and evd.arch_evd_depe=d.depe_codi and evd.arch_evd_item=it.arch_eit_codigo";
		$rs = $this->link->query ( $sql );
		$i = 0;
		while ( ! $rs->EOF ) {
			$combi [$i] ["CODIGO"] = $rs->fields ['CODIGO'];
			$combi [$i] ["DNOMB"] = $rs->fields ['DNOMB'];
			$combi [$i] ["ENOMB"] = $rs->fields ['ENOMB'];
			$combi [$i] ["INOMB"] = $rs->fields ['INOMB'];
			$combi [$i] ["DCOD"] = $rs->fields ['DCOD'];
			$combi [$i] ["ECOD"] = $rs->fields ['ECOD'];
			$combi [$i] ["ICOD"] = $rs->fields ['ICOD'];
			$combi [$i] ["CODPISO"] = $rs->fields ['CODPISO'];
			$combi [$i] ["CODAREA"] = $rs->fields ['CODAREA'];
			$combi [$i] ["CODMOD"] = $rs->fields ['CODMOD'];
			$i ++;
			$rs->MoveNext ();
		}
		return $combi;
	}
	
	function ConsulRelacion($codigo) {
		$sql = "select  evd.arch_evd_id codigo,evd.arch_evd_depe dcod,evd.arch_evd_edificio ecod,evd.arch_evd_item icod,arch_evd_modulo codmod,arch_evd_piso codpiso, arch_evd_area codarea
 from arch_evd_edivsdep evd
where  evd.arch_evd_id=$codigo";
		$rs = $this->link->query ( $sql );
		if (! $rs->EOF) {
			$combi ["CODIGO"] = $rs->fields ['CODIGO'];
			$combi ["DCOD"] = $rs->fields ['DCOD'];
			$combi ["ECOD"] = $rs->fields ['ECOD'];
			$combi ["ICOD"] = $rs->fields ['ICOD'];
			$combi ["CODPISO"] = $rs->fields ['CODPISO'];
			$combi ["CODAREA"] = $rs->fields ['CODAREA'];
			$combi ["CODMOD"] = $rs->fields ['CODMOD'];
		
		}
		return $combi;
	}
	/** opercion de Eliminar de  relacion de edificios depe*/
	function eliRelacionEdiDepe($codigo) {
		$sql = "delete from arch_evd_edivsdep  where arch_evd_id=$codigo ";
		$rs = $this->link->query ( $sql );
	
	}
	/** opercion de crear de  relacion de edificios depe*/
	function crearRelacionEdiDepe($edificio, $depe, $item, $modulo, $piso, $area) {
		$sq2l = "select  max(arch_evd_id) codigo from arch_evd_edivsdep";
		$rs = $this->link->query ( $sq2l );
		$codigo = $rs->fields ['CODIGO'] + 1;
		$sql = "INSERT INTO arch_evd_edivsdep
             (arch_evd_id,arch_evd_edificio,arch_evd_depe,arch_evd_item,arch_evd_modulo,arch_evd_piso,arch_evd_area)
	         VALUES( $codigo,$edificio,$depe,$item,$modulo,$piso,$area)";
		$listo = $this->link->conn->Execute ( $sql );
	}
	/** opercion de modificacion de  relacion de edificios depe*/
	function modRelacionEdiDepe($codigo, $edificio, $depe, $item, $modulo, $piso, $area) {
		$sq2l = "update  arch_evd_edivsdep set arch_evd_edificio=$edificio , arch_evd_depe='$depe',arch_evd_item=$item,arch_evd_modulo=$modulo,arch_evd_piso=$piso,arch_evd_area=$area
                    where arch_evd_id=$codigo";
		$rs = $this->link->query ( $sq2l );
	}
	
	/***tipo almacenaje***/
	function ConsulTpAlmacen() {
		$sql = "SELECT 
  arch_tpa_id codigo,arch_tpa_name nomb,arch_tpa_desc descrip,arch_tpa_tamano tama, arch_tpa_tipo tipo
FROM 
  arch_tpa_tpalmacenaje";
		//return "";
		$rs = $this->link->query ( $sql );
		$i = 0;
		while ( ! $rs->EOF ) {
			$combi [$i] ["CODIGO"] = $rs->fields ['CODIGO'];
			$combi [$i] ["NOMB"] = $rs->fields ['NOMB'];
			$combi [$i] ["DESC"] = $rs->fields ['DESCRIP'];
			$combi [$i] ["TAMA"] = $rs->fields ['TAMA'];
			$combi [$i] ["TIPO"] = $rs->fields ['TIPO'];
			$i ++;
			$rs->MoveNext ();
		}
		return $combi;
	}
	
	/** opercion de Eliminar de  tipo almacenaje*/
	function eliTpAlmacen($codigo) {
		$sql = "delete from arch_tpa_tpalmacenaje  where arch_tpa_id=$codigo ";
		$rs = $this->link->query ( $sql );
	
	}
	/** opercion de crear tipo almacenaje*/
	function crearTpAlmacen($nombre, $descrp, $tama, $tipo) {
		$sq2l = "select  max(arch_tpa_id) codigo from arch_tpa_tpalmacenaje";
		$rs = $this->link->query ( $sq2l );
		$codigo = $rs->fields ['CODIGO'] + 1;
		$sql = "INSERT INTO arch_tpa_tpalmacenaje
             (arch_tpa_id,arch_tpa_name,arch_tpa_desc,arch_tpa_tamano, arch_tpa_tipo)
	         VALUES($codigo, '$nombre','$descrp',$tama, $tipo)";
		$listo = $this->link->conn->Execute ( $sql );
	}
	/** opercion de modificaciontipo almacenaje*/
	function modTpAlmacen($codigo, $nombre, $descrp, $tama, $tipo) {
		$sq2l = "update  arch_tpa_tpalmacenaje set arch_tpa_name='$nombre' , arch_tpa_desc='$descrp',arch_tpa_tamano=$tama,arch_tpa_tipo=$tipo
                    where arch_tpa_id=$codigo";
		$rs = $this->link->query ( $sq2l );
	}
	/**Informacion de radicados en expedientes */
	function datosExpediente($numExpediente) {
		$sql = "select r.radi_nume_radi nradi,e.sgd_exp_subexpediente sbexp,e.sgd_exp_fech fechaexp,e.sgd_exp_estado estado, radi_nume_folios nhoja,
		    count(a.anex_codigo) as anexos, sum(a.anex_folios_dig) folanex, e.sgd_exp_carpeta as carp,e.sgd_exp_caja as numcaja from sgd_exp_expediente e, 
		    radicado r left outer join anexos a on a.anex_radi_nume=r.radi_nume_radi and a.anex_borrado='N' where e.SGD_EXP_NUMERO ='$numExpediente'
		    and e.radi_nume_radi=r.radi_nume_radi and (r.sgd_eanu_codigo>2 or r.sgd_eanu_codigo is null) 
		    group by r.radi_nume_radi, e.sgd_exp_subexpediente, e.sgd_exp_estado, radi_nume_folios,e.sgd_exp_carpeta,e.sgd_exp_caja, e.sgd_exp_fech";
		//and e.sgd_exp_estado !=2
		$rs = $this->link->query ( $sql );
		$i = 0;
		$combi ['TFOL'] = 0;
		$combi ['TANEXO'] = 0;
		while ( ! $rs->EOF ) {
			$combi [$i] ["NRADI"] = $rs->fields ['NRADI'];
			$combi [$i] ["SBEXP"] = $rs->fields ['SBEXP'];
			$combi [$i] ["FECHAEXP"] = $rs->fields ['FECHAEXP'];
			$combi [$i] ["ESTADO"] = $rs->fields ['ESTADO'];
			$combi [$i] ["NHOJA"] = $rs->fields ['NHOJA']+0;
			$combi [$i] ["FOLANEX"]= $rs->fields['FOLANEX']+0;
			$combi [$i] ["NUMCAJA"] = $rs->fields ['NUMCAJA'];
			$combi [$i] ["CARP"] = $rs->fields ['CARP'];
			$combi [$i] ["ANEXOS"] = $rs->fields ['ANEXOS'];
			$combi ['TFOL'] = $combi [$i]["NHOJA"] + $combi ['TFOL']+ $combi [$i] ["FOLANEX"];
			$combi ['TANEXO'] = $combi ['TANEXO'] + $combi [$i]["ANEXOS"];
			$i++;
			$rs->MoveNext ();
		}
		return $combi;
	
	}
	/**Cambnio de titulo expediente ****/
	function TituloExpediente($numExpediente, $titulo) {
		$sql = "update SGD_EXP_EXPEDIENTE SET SGD_EXP_titulo='$titulo' WHERE SGD_EXP_NUMERO ='$numExpediente' ";
		$rs = $this->link->query ( $sql );
	}
	
	function DatosExpBasicos($codigo) {
		
		 $sql = "select min(e.sgd_exp_fech) inclucion,max(e.sgd_exp_fech_mod) maxmod,
                    min(e.sgd_exp_fech_mod) minmod ,max(e.SGD_EXP_titulo) titulo ,max(e.SGD_EXP_ARCHIVO) estado
                    
		from sgd_exp_expediente e,SGD_SEXP_SECEXPEDIENTES g  where e.sgd_exp_numero = '$codigo' and e.sgd_exp_numero=g.sgd_exp_numero ";
		$rs = $this->link->query ( $sql );
		if (! $rs->EOF) {
			$combi ["TITULO"] = $rs->fields ['TITULO'];
			$combi ["ESTADO"] = $rs->fields ['ESTADO'];
			$combi ["MINMOD"] = $rs->fields ['MINMOD'];
			$combi ["MAXMOD"] = $rs->fields ['MAXMOD'];
			$combi ["INCLUCION"] = $rs->fields ['INCLUCION'];
		
		}
		return $combi;
	}
	
	//***Ubicaion expediente ***/
	function addUbiExpediente($exp_numero, $posicion, $tpcaja, $tpcarpeta, $edificio, $piso, $area, $modulo, $estante, $entrepano, $codcaja) {
		$sq2l = "select  max(arch_uexp_codi) codigo from arch_uexp_ubicacionexp";
		$rs = $this->link->query ( $sq2l );
		$codigo = $rs->fields ['CODIGO'] + 1;
		$sql = "insert into arch_uexp_ubicacionexp (arch_uexp_codi,sgd_exp_numero,arch_uexp_ubica,arch_uexp_tpcaja,arch_uexp_tpcarpeta,arch_uexp_edificio,arch_uexp_psio,arch_uexp_area,arch_uexp_modulo,arch_uexp_estante,arch_uexp_entrepano,arch_uexp_codcaja) 
values ($codigo,'$exp_numero',$posicion,$tpcaja,$tpcarpeta,$edificio,$piso,$area,$modulo,$estante,$entrepano,$codcaja)";
		$rs = $this->link->query ( $sql );
	}
	
	/**Informacion de radicados en expedientes */
	function datosUbiExpediente($numExpediente) {
		$sql = "SELECT   i.arch_uexp_codi codigo,  g.arch_edi_nombre edificio,  h.arch_tpa_name tpudc,  f.arch_tpa_name tipocaja, 
  e.arch_eit_nombre piso,   d.arch_eit_nombre modulo,  c.arch_eit_nombre area,  b.arch_eit_nombre entrepano,  a.arch_eit_nombre estante,  
  i.sgd_exp_numero numexp,  i.arch_uexp_ubica estubi,  i.arch_uexp_codcaja numcaja,  i.arch_uexp_nref referencia
FROM 
  arch_uexp_ubicacionexp i,   arch_tpa_tpalmacenaje h,  arch_edi_edificio g,  arch_eit_items e,  arch_tpa_tpalmacenaje f, 
  arch_eit_items a,  arch_eit_items b,  arch_eit_items c,  arch_eit_items d
 WHERE 
  f.arch_tpa_id = i.arch_uexp_tpcaja AND
  g.arch_edi_codigo = i.arch_uexp_edificio AND
  e.arch_eit_codigo = i.arch_uexp_psio AND
  h.arch_tpa_id = i.arch_uexp_tpcarpeta AND
  d.arch_eit_codigo = i.arch_uexp_modulo AND
  c.arch_eit_codigo = i.arch_uexp_area AND
  b.arch_eit_codigo = i.arch_uexp_entrepano AND
  a.arch_eit_codigo = i.arch_uexp_estante AND
  i.sgd_exp_numero = '$numExpediente'";
		//and e.sgd_exp_estado !=2 c.arch_eit_codigo = i.arch_uexp_area AND arch_eit_items c,  c.arch_eit_nombre area,
		$rs = $this->link->query ( $sql );
		$i = 0;
		
		while ( ! $rs->EOF ) {
			$combi [$i] ["CODIGO"] = $rs->fields ['CODIGO'];
			$combi [$i] ["NUMEXP"] = $rs->fields ['NUMEXP'];
			$combi [$i] ["ESTUBI"] = $rs->fields ['ESTUBI'];
			$combi [$i] ["TIPOCAJA"] = $rs->fields ['TIPOCAJA'];
			$combi [$i] ["MODULO"] = $rs->fields ['MODULO'];
			$combi [$i] ["TPUDC"] = $rs->fields ['TPUDC'];
			$combi [$i] ["NUMCAJA"] = $rs->fields ['NUMCAJA'];
			$combi [$i] ["EDIFICIO"] = $rs->fields ['EDIFICIO'];
			$combi [$i] ["PISO"] = $rs->fields ['PISO'];
			$combi [$i] ["AREA"] = $rs->fields ['AREA'];
			$combi [$i] ["ENTREPANO"] = $rs->fields ['ENTREPANO'];
			$combi [$i] ["ESTANTE"] = $rs->fields ['ESTANTE'];
			$combi [$i] ["REFERENCIA"] = $rs->fields ['REFERENCIA'];
			$i ++;
			$rs->MoveNext ();
		}
		return $combi;
	
	}
	/***/
	function consultaCsjas($numEnt) {
		
		 $sql = "SELECT 
  arch_uexp_ubicacionexp.arch_uexp_codcaja codcaja, 
  arch_tpa_tpalmacenaje.arch_tpa_tamano tamano, 
  arch_uexp_ubicacionexp.arch_uexp_tpcaja codtpcaja, 
  arch_uexp_ubicacionexp.arch_uexp_ubica ubicacion, 
  arch_tpa_tpalmacenaje.arch_tpa_name nombtipocj
FROM 
  arch_tpa_tpalmacenaje, 
  arch_uexp_ubicacionexp
WHERE 
  arch_tpa_tpalmacenaje.arch_tpa_id = arch_uexp_ubicacionexp.arch_uexp_tpcaja
  and arch_uexp_entrepano=$numEnt order by ubicacion asc
  ";
		//and e.sgd_exp_estado !=2
		$rs = $this->link->query ( $sql );
		$i = 1;
		
		while ( ! $rs->EOF ) {
			$combi [$i] ["CODIGO"] = $rs->fields ['CODCAJA'];
			$combi [$i] ["TAMANO"] = $rs->fields ['TAMANO'];
			$combi [$i] ["CODRPCAJA"] = $rs->fields ['CODRPCAJA'];
			$combi [$i] ["UBICACION"] = $rs->fields ['UBICACION'];
			$combi [$i] ["NOMBTIPOCJ"] = $rs->fields ['NOMBTIPOCJ'];
			
			$i ++;
			$rs->MoveNext ();
		}
		return $combi;
	
	}
	
	function IncluirExpediente($numExpediente, $numrad, $numcaja, $numuc) {
		$krd = $_SESSION ['krd'];
		$fecha = $this->link->conn->OffsetDate ( 0, $this->db->conn->sysTimeStamp );
		$sql = "update SGD_EXP_EXPEDIENTE SET SGD_EXP_carpeta=$numuc ,SGD_EXP_caja=$numcaja,SGD_EXP_estado=1,
		sgd_exp_fech_arch=$fecha,radi_usua_arch='$krd'
		WHERE SGD_EXP_NUMERO ='$numExpediente' and radi_nume_radi=$numrad";
		$rs = $this->link->query ( $sql );
	}
	
	function buscarAvan($expediente,$depe, $edificio, $piso, $area, $modulo, $estante, $entrepano, $fechaI, $fechaF) {
		//echo "$expediente,  $depe,$edificio,$piso,$area,$modulo,$estante,$entrepano, $fechaI, $fechaF";
		$expSql="";
		if ($expediente)
			$expSql.= "and upper(d.sgd_exp_numero) like '%$expediente%'";
		if ($depe)
			$depeSql = "and d.depe_codi = $depe";
		if ($edificio) {
			$ediSql = "and ue.arch_uexp_edificio=$edificio ";
		} else {
			$datedd = " and d.sgd_exp_fech BETWEEN  " . $this->link->conn->DBTimeStamp ( $fechaI ) . " and " . $this->link->conn->DBTimeStamp ( $fechaF );
		
		}
		if ($piso != 0) {
			$pisoSql = "and ue.arch_uexp_psio=$piso ";
		}
		if ($modulo) {
			$modSql = "and ue.arch_uexp_area=$area ";
		}
		if ($modulo) {
			$modSql = "and ue.arch_uexp_modulo=$modulo ";
		}
		if ($estante) {
			$estSql = "and ue.arch_uexp_estante=$estante ";
		}
		if ($entrepano) {
			$entSql = "and ue.arch_uexp_entrepano=$entrepano ";
		}
		$sql = "select distinct ue.arch_uexp_codcaja,d.sgd_exp_numero, d.sgd_exp_estado, d.RADI_NUME_RADI , a.RADI_NUME_HOJA, a.RADI_FECH_RADI,
               a.RA_ASUN , a.RADI_PATH , a.RADI_USUA_ACTU , TO_CHAR(a.RADI_FECH_RADI, 'DD-MM-YYYY HH24:MI AM') AS FECHA ,
               b.sgd_tpr_descrip, b.sgd_tpr_codigo, b.sgd_tpr_termino, RADI_LEIDO, RADI_TIPO_DERI, RADI_NUME_DERI, 
               a.radi_depe_actu, e.depe_nomb, f.usua_nomb,  d.sgd_exp_fech_arch, d.sgd_exp_fech , d.SGD_EXP_ASUNTO 
               ,radi_depe_actu,radi_usua_arch
               from arch_uexp_ubicacionexp  ue,radicado a, sgd_tpr_tpdcumento b, SGD_EXP_EXPEDIENTE d, DEPENDENCIA e, USUARIO f, SGD_SEXP_SECEXPEDIENTES s
               where f.usua_codi=a.radi_usua_actu  and e.depe_codi=a.radi_depe_actu and d.sgd_exp_numero=ue.sgd_exp_numero
	       and s.sgd_exp_numero=d.sgd_exp_numero
               and a.tdoc_codi=b.sgd_tpr_codigo AND a.radi_nume_radi=d.radi_nume_radi and d.sgd_exp_estado=1  and CAST(d.sgd_exp_caja  AS integer)=ue.arch_uexp_codcaja
               $expSql  $depeSql $ediSql $pisoSql $modSql $estSql $entSql
                $datedd
               order by ue.arch_uexp_codcaja asc";
		
		$rs = $this->link->query ( $sql );
		$i = 0;
		while ( ! $rs->EOF ) {
			$combi [$i] ["ARCH_UEXP_CODCAJA"] = $rs->fields ['ARCH_UEXP_CODCAJA'];
			$combi [$i] ["SGD_EXP_NUMERO"] = $rs->fields ['SGD_EXP_NUMERO'];
			$combi [$i] ["MATRIX"]	       = $rs->fields ['MATRIX'];
			$combi [$i] ["FARO"]	       = $rs->fields ['FARO'];
			$combi [$i] ["SGD_EXP_ESTADO"] = $rs->fields ['SGD_EXP_ESTADO'];
			$combi [$i] ["RADI_PATH"] = $rs->fields ['RADI_PATH'];
			$combi [$i] ["RA_ASUN"] = $rs->fields ['RA_ASUN'];
			$combi [$i] ["RADI_FECH_RADI"] = $rs->fields ['RADI_FECH_RADI'];
			$combi [$i] ["RADI_NUME_HOJA"] = $rs->fields ['RADI_NUME_HOJA'];
			$combi [$i] ["RADI_NUME_RADI"] = $rs->fields ['RADI_NUME_RADI'];
			$combi [$i] ["RADI_USUA_ACTU"] = $rs->fields ['RADI_USUA_ACTU'];
			$combi [$i] ["FECHA"] = $rs->fields ['FECHA'];
			$combi [$i] ["SGD_TPR_DESCRIP"] = $rs->fields ['SGD_TPR_DESCRIP'];
			$combi [$i] ["SGD_TPR_CODIGO"] = $rs->fields ['SGD_TPR_CODIGO'];
			$combi [$i] ["SGD_TPR_TERMINO"] = $rs->fields ['SGD_TPR_TERMINO'];
			$combi [$i] ["RADI_LEIDO"] = $rs->fields ['RADI_LEIDO'];
			$combi [$i] ["RADI_TIPO_DERI"] = $rs->fields ['RADI_TIPO_DERI'];
			$combi [$i] ["RADI_NUME_DERI"] = $rs->fields ['RADI_NUME_DERI'];
			$combi [$i] ["RADI_DEPE_ACTU"] = $rs->fields ['RADI_DEPE_ACTU'];
			$combi [$i] ["DEPE_NOMB"] = $rs->fields ['DEPE_NOMB'];
			$combi [$i] ["USUA_NOMB"] = $rs->fields ['USUA_NOMB'];
			$combi [$i] ["SGD_EXP_FECH_ARCH"] = $rs->fields ['SGD_EXP_FECH_ARCH'];
			$combi [$i] ["SGD_EXP_FECH"] = $rs->fields ['SGD_EXP_FECH'];
			$combi [$i] ["SGD_EXP_ASUNTO"] = $rs->fields ['SGD_EXP_ASUNTO'];
			$combi [$i] ["RADI_USUA_ARCH"] = $rs->fields ['RADI_USUA_ARCH'];
			
			$i ++;
			$rs->MoveNext ();
		}
		return $combi;
	
	}
	function cambioCaja($ueCod, $numcaja, $edi, $piso, $area, $modulo, $estante, $entrepano, $nref, $pos) {
		$krd = $_SESSION ['krd'];
		$fecha = $this->link->conn->OffsetDate ( 0, $this->db->conn->sysTimeStamp );
		 $sql = "update arch_uexp_ubicacionexp set   arch_uexp_ubica=$pos,  arch_uexp_edificio=$edi, arch_uexp_psio=$piso, arch_uexp_area=$area, arch_uexp_modulo=$modulo, arch_uexp_estante=$estante, 
 arch_uexp_entrepano=$entrepano,  arch_uexp_nref='$nref' where  arch_uexp_codi=$ueCod  and  arch_uexp_codcaja=$numcaja  ";
		$rs = $this->link->query ( $sql );
	}
	function cambioEstante($ueCod, $numcaja, $edi, $piso, $area, $modulo, $estante, $entrepano, $nref, $pos) {
		$krd = $_SESSION ['krd'];
		$fecha = $this->link->conn->OffsetDate ( 0, $this->db->conn->sysTimeStamp );
		$sql = "update arch_uexp_ubicacionexp set ,  arch_uexp_ubica=$pos,  arch_uexp_edificio=$edi, arch_uexp_psio=$piso, arch_uexp_area=$area, arch_uexp_modulo=$modulo, arch_uexp_estante=$estante, 
 arch_uexp_entrepano=$entrepano,  arch_uexp_nref='$nref' where  arch_uexp_codi=$ueCod  and  arch_uexp_codcaja=$numcaja  ";
		$rs = $this->link->query ( $sql );
	}
	function cambioEntrepano($ueCod, $numcaja, $edi, $piso, $area, $modulo, $estante, $entrepano, $nref, $pos) {
		$krd = $_SESSION ['krd'];
		$fecha = $this->link->conn->OffsetDate ( 0, $this->db->conn->sysTimeStamp );
		$sql = "update arch_uexp_ubicacionexp set ,  arch_uexp_ubica=$pos,  arch_uexp_edificio=$edi, arch_uexp_psio=$piso, arch_uexp_area=$area, arch_uexp_modulo=$modulo, arch_uexp_estante=$estante, 
 arch_uexp_entrepano=$entrepano,  arch_uexp_nref='$nref' where  arch_uexp_codi=$ueCod  and  arch_uexp_codcaja=$numcaja  ";
		$rs = $this->link->query ( $sql );
	}
	function consuldataCaja($edi, $piso, $area, $modulo, $estante, $entrepano, $numcaja) {
		 $sql = "SELECT arch_uexp_codi CODIGO, arch_uexp_codcaja CODCAJA, sgd_exp_numero EXPEDIENTE FROM arch_uexp_ubicacionexp
WHERE arch_uexp_edificio =$edi  AND arch_uexp_psio =$piso  AND arch_uexp_area =$area  AND arch_uexp_modulo =$modulo  AND 
arch_uexp_entrepano = $entrepano AND arch_uexp_estante = $estante and arch_uexp_codcaja=$numcaja";
		//and e.sgd_exp_estado !=2
		$rs = $this->link->query ( $sql );
		$i = 1;
		
		while ( ! $rs->EOF ) {
			$combi [$i] ["CODIGO"] = $rs->fields ['CODIGO'];
			$combi [$i] ["CODCAJA"] = $rs->fields ['CODCAJA'];
			$combi [$i] ["EXPEDIENTE"] = $rs->fields ['EXPEDIENTE'];
			$i ++;
			$rs->MoveNext ();
		}
		return $combi;
	
	}

	function datosUbiExpedienteXcaja($numExpediente,$numcj){
	    $sql = "SELECT   i.arch_uexp_codi codigo,  g.arch_edi_nombre edificio,  h.arch_tpa_name tpudc,  f.arch_tpa_name tipocaja,
  e.arch_eit_nombre piso,   d.arch_eit_nombre modulo,  c.arch_eit_nombre area,  b.arch_eit_nombre entrepano,  a.arch_eit_nombre estante,
  i.sgd_exp_numero numexp,  i.arch_uexp_ubica estubi,  i.arch_uexp_codcaja numcaja,  i.arch_uexp_nref referencia
FROM
  arch_uexp_ubicacionexp i,   arch_tpa_tpalmacenaje h,  arch_edi_edificio g,  arch_eit_items e,  arch_tpa_tpalmacenaje f,
  arch_eit_items a,  arch_eit_items b,  arch_eit_items c,  arch_eit_items d
 WHERE
  f.arch_tpa_id = i.arch_uexp_tpcaja AND
  g.arch_edi_codigo = i.arch_uexp_edificio AND
  e.arch_eit_codigo = i.arch_uexp_psio AND
  h.arch_tpa_id = i.arch_uexp_tpcarpeta AND
  d.arch_eit_codigo = i.arch_uexp_modulo AND
  c.arch_eit_codigo = i.arch_uexp_area AND
  b.arch_eit_codigo = i.arch_uexp_entrepano AND
  a.arch_eit_codigo = i.arch_uexp_estante AND
  i.sgd_exp_numero = '$numExpediente' and i.arch_uexp_codcaja=$numcj";
	    $rs=$this->link->conn->Execute($sql);
	    if(!$rs->EOF){
                $posicion = $rs->fields ['ESTUBI'];
                $tpcaja = $rs->fields ['TIPOCAJA'];
                $modulo = $rs->fields ['MODULO'];
                $tpuc = $rs->fields ['TPUDC'];
                $numcaja = $rs->fields ['NUMCAJA'];
                $edi = $rs->fields ['EDIFICIO'];
                $piso = $rs->fields ['PISO'];
                $area = $rs->fields ['AREA'];
                $entrepano = $rs->fields ['ENTREPANO'];
                $estante = $rs->fields ['ESTANTE'];
                //$combi [$i] ["REFERENCIA"] = $rs->fields ['REFERENCIA'];
		$retorno="Expediente ubicado en Edificio $edi piso $piso area $area modulo $modulo estante $estante posicion $posicion tipo de unidad de conservacion $tpuc en caja No. $numcaja de tipo $tpcaja";
	    }
	    else{
		$retorno="Error posicion de expediente no fue dada correctamente";
	    }
	    return $retorno;
        }
}
?>
