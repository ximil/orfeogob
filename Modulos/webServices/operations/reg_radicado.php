<?php 
/**
 * Script que contiene el resgistro de las operaciones que se pueden
 * asociar dentro de la categoria de Anexos
 * 
 * @author Hardy deimont Niño
 * 
 */


$server->register('crearAnexo',  //nombre del servicio                 
    array('radiNume' => 'xsd:string',//numero de radicado	
     'file' => 'xsd:base64Binary',//archivo en base 64
     'filename' => 'xsd:string',//nombre original del archivo
     'correo' => 'xsd:string',//correo electronico
     'descripcion'=>'xsd:string',//descripcion del anexo
     ),//fin parametros del servicio        	
    array('return' => 'xsd:string'),//retorno del servicio
    $ns,
    $ns."/crearAnexo",
    'rpc',
    'encoded',
    "Operacion que permite crear un anexo. <br/> 
    Entradas: <br/>
    - radiNume: Numero de radicado al cual se le va a crear el anexo.<br/>
    - file: Documento asociado al anexo que se va a crear. <br/>
    - filename: nombre del archivo. <br/>
    - correo: Correo electronico del usuario que va a crear el anexo. <br/>
    - descripcion: Descripcion asociada al anexo. <br/>
    Salida: La cadena 'Anexo Creado' en caso de exito o 'ERROR ..' en caso contrario."       
);

$server->register('subirImagenDocumento',
	array(
		'numRadicado'=>'xsd:string',
		'ext'=>'xsd:string',
		'file'=>'xsd:base64Binary',
		'correo'=>'xsd:string'
	),
	array(
		'return'=>'xsd:string'
	),
	$ns,
	$ns."/subirImagenDocumento",
	'rpc',
	'encoded',
	'Operacion subir imagen del documento. <br/>
	Entradas: <br/>
	- numRadicado: Numero de radicado al cual se le desea cambiar la imagen. <br/>
	- ext: Extension del documento a subir. <br/>
	- file: Archivo codificaco en base64 que corresponde al nuevo documento que va a quedar asociado al radicado. <br/>
	- correo: Correo indica el usuario que radica (Debe estar asociado  a un usuario de Orfeo.). <br/>
	Salida: String "Ok" en caso de exito, "ERROR: ..." en caso de fallo.'
);
$server->register('consultarRadicado',
	array(
		'numRadicado'=>'xsd:string',
                'tipo'=>'xsd:string',
	),
	array(
		'return'=>'tns:Matriz'
	),
	$ns,
	$ns."/consultarRadicado",
	'rpc',
	'encoded',
	'Operacion consultar radicado. 
         <ul>
           <li><font color="white">Entradas:</font>
                <ol>
                    <li>numRadicado: Numero de radicado al cual se le desea cambiar la imagen.</li>
                    <li>tipo: 1. datos de radicado, 2. datos radicado y  anexos,3 datos radicado anexos y historico,4 datos radicado y historico.</li>
                </ol>
             </li>
            <li><font color="white">Salida:</font>
                <ol>
                    <li>Array con los datos en caso de exito, "ERROR: ..." en caso de fallo.</li>
                </ol>
             </li>
        </ul> '
);

$server->register('consultarRadicadoCo',
	array(
		'numRadicado'=>'xsd:string',
                'tipo'=>'xsd:string',
	),
	array(
		'return'=>'xsd:string'
	),
	$ns,
	$ns."/consultarRadicado",
	'rpc',
	'encoded',
	'Operacion consultar radicado. 
         <ul>
           <li><font color="white">Entradas:</font>
                <ol>
                    <li>numRadicado: Numero de radicado al cual se le desea cambiar la imagen.</li>
                    <li>tipo: 1. datos de radicado, 2. datos radicado y  anexos .</li>
                </ol>
             </li>
            <li><font color="white">Salida:</font>
                <ol>
                    <li>String con los datos en caso de exito, "ERROR: ..." en caso de fallo.</li>
                </ol>
             </li>
        </ul> '
);
 
$server->register('radicarDocumento',
	array(		                
		'destinatario'=>'tns:Destinatario',
		'asu'=>'xsd:string',
		'ane'=>'xsd:string',
                'correo' => 'xsd:string',	
		'tpRadicado'=>'xsd:string',
		'cuentai'=>'xsd:string',
		'depe_actu'=>'xsd:string',
		'tip_rem'=>'xsd:string',
		'tdoc'=>'xsd:string',
		'tip_doc'=>'xsd:string'		
	),
	array(
		'return' => 'xsd:string'
	),
	$ns,
	$ns."/radicarDocumento",
	'rpc',
	'encoded',
	'Operacion que permite generar radicado nuevo. <br/>
	Entradas: <br/>

	- destinatario: Array que contiene informacion del destinatario. <br/>
	- asu: Asunto del radicado. <br/>
	- ane: Descripcion de anexos. <br/>
        - correo: Correo del usuario que realiza la radicacion. <br/> 
	- coddepe: Codigo de la dependencia. <br/>
	- tpRadicado: Tipo de radicado. <br/>
	- cuentai: Cuenta interna del radicado. <br/>
	- depe_actu: Codigo dependencia actual. <br/>
	- tip_rem: Tipo de remitente. <br/>
	- tdoc: <br/>
	- tip_doc: Tipo de documento. <br/>
	Salida: String con el numero de radicado genrado en caso de exito. "ERROR:...." en caso de fallo.'
);
?>
