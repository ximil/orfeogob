<?php 
include_once $ruta_raiz . "/core/Modulos/radicacion/clases/radicado.php";
include_once $ruta_raiz . "/core/clases/dependencia.php";
include_once $ruta_raiz . "/core/Modulos/radicacion/clases/tipoRadicado.php";
include_once $ruta_raiz . "/core/clases/usuarioOrfeo.php";
include_once "$ruta_raiz/core/Modulos/radicacion/clases/tx.php";
include_once $ruta_raiz . '/core/clases/file-class.php';

/**
 * funcion que crea un Anexo, y ademas decodifica el anexo enviasdo en base 64
 *
 * @author  cgprada - modificado 10-11-2010
 * @author  hardy deimont Niño - modificado 10-10-2013
 * 
 * Ajustes para que devuelva el numero de radicado e inlcusion de algunas validaciones
 *
 * @param string $radiNume numero del radicado al cual se adiciona el anexo
 * @param base64 $file archivo codificado en base64
 * @param string $filename nombre original del anexo, con extension
 * @param string $correo correo electronico del usuario que adiciona el anexo 
 * @param string $descripcion descripcion del anexo
 * @return string mensaje de error en caso de fallo o el numero del anexo en caso de exito
 */
function crearAnexo($radiNume, $file, $filename, $correo, $descripcion) {

    global $ruta_raiz;
    /* $fp = @fopen("/tmp/gino.txt", "w");
      //   $bytes = base64_decode($archivo);
      $bytes=$file;
      fwrite($fp, $bytes);
      fclose($fp); */

    if (($radiNume == null && strlen($radiNume) == 0) or ( $file == null && strlen($file) == 0) or ( $correo === null &&
            strlen($correo) == 0) or ( $descripcion == null && strlen($descripcion) == 0)) {

        return "ERROR: No se recibieron todos los datos necesarios para realizar la operacion.";
    }

    $rad = new radicado($ruta_raiz);
//			//validacion existencia de radicado
    $rad->setRadiNumeRadi($radiNume);
    $rsRad = $rad->consultar();
    $radiDepeActu = $rsRad['RADI_DEPE_ACTU'];
    $radiUsuaActu = $rsRad['RADI_USUA_ACTU'];

    if ($radiUsuaActu == null) {
        return "ERROR: El radicado $radiNume no existe.";
    }

    //validacion existencia de usuario
    if (!verificarCorreo($correo)) {
        return "ERROR: " . "el mail no corresponde a un email valido";
    }
    $usuario = new usuarioOrfeo($ruta_raiz);
    $datos = $usuario->consultadatos($correo);
    $usuCod = $datos[0]['USUA_CODI'];
    $usuDep = $datos[0]['DEPCODI'];
    $usuLogin = $datos[0]['USUA_LOGIN'];
    //print_r($datos);
    if ($usuLogin == null or sizeof($usuLogin) == 0) {
        return "ERROR:  El email no  corresponde a ningun usuario";
    }
    //validacion de usuario actual
//    if ($radiDepeActu == $usuDep && $usuCod == $radiUsuaActu) {

    $numeanxo = $rad->crearAnexo($radiNume, $filename, $file, $usuLogin, $descripcion, $usuDep);
    /*  } else {
      return "ERROR: El usuario identificado con el mail $correo no es el usuario actual del radicado $radiNume.";
      } */
    return $numeanxo;
}

/**
 * funcion que verifica que un correo electronico cumpla con 
 * un patron estandar
 *
 * @param strig $correo correo a verificar
 * @return boolean
 */
function verificarCorreo($correo) {
    $expresion = preg_match("(^\w+([\.-] ?\w+)*@\w+([\.-]?\w+)*(\.\w+)+)", $correo);
    return $expresion;
}

/**
 * subirImagenDocumento es una funcion que permite almacenar cualquier tipo de archivo en el lado del servidor
 * @param $bytes 
 * @param $filename es el nombre del archivo con que queremos almacenar en el servidor
 * @author Hardy Niño
 * @return Retorna un String indicando si la operacion fue satisfactoria o no
 */
function subirImagenDocumento($radinum, $ext, $file, $correo) {

    try {
        if (strlen($radinum) != "14") {
            return "ERROR: El numero de radicado es encuentra incompleto. ";
        }
        global $ruta_raiz;
        $actualiza = Null;
        $rutaArch = Null;
        $year = substr($radinum, 0, 4);
        $depe = substr($radinum, 4, 3);

        $rad = new radicado($ruta_raiz);
        //validacion existencia de radicado
        $rad->setRadiNumeRadi($radinum);
        $rsRad = $rad->consultar();
        // print_r($rsRad);
        $coddepe = $rsRad['RADI_DEPE_ACTU'];
        $radicado = $rsRad['RADI_NUME_RADI'];
        $radi_usua_actu = $rsRad['RADI_USUA_ACTU'];
        $radi_nume_deri = $rsRad['RADI_NUME_DERI'];
        $rutaArch = $rsRad['RADI_PATH'];
        if ($radi_nume_deri == null)
            $radi_nume_deri = $radinum;
        //consulta si el radicado existe
        $radinums [0] = $radinum;
        if (!$radicado) {
            return "ERROR: El radicado no existe";
        }
        if ($rutaArch) {
            return "ERROR: El radicado ya tiene imagen. ";
        }
        //validacion existencia de usuario
        if (!verificarCorreo($correo)) {
            return "ERROR: " . "el mail no corresponde a un email valido";
        }
        $usuario = new usuarioOrfeo($ruta_raiz);
        $datos = $usuario->consultadatos($correo);
        $usuCod = $datos[0]['USUA_CODI'];
        $usuDep = $datos[0]['DEPCODI'];
        $usuLogin = $datos[0]['USUA_LOGIN'];
        $id_rol = $datos [0]['IDROL'];
        $codusuario = $datos [0]['USUA_CODI'];
        $usuadoc = $datos [0]['USUA_DOC'];
        //print_r($datos);
        if ($usuLogin == null or sizeof($usuLogin) == 0) {
            return "ERROR:  El email no  corresponde a ningun usuario";
        }
        $rutaArch = "/$year/$depe/docs/$radinum.$ext";
        $resultado = $rad->SubirImageDoc($radinum, $rutaArch, $file, $usuLogin, $usuDep);
        if ($resultado == 'ok') {
            $tx = new tx($ruta_raiz);
            $codTx = 22; //Codigo de la transaccion
            //$hist = new Historico($db);
            $radicadosSel[0] = $radinum;
            $tx->insertaHistorico($radicadosSel, $usuDep, $usuDep, $usuadoc, $usuadoc, $codusuario, $codusuario, $id_rol, $id_rol, "Asociar Imagen a Radicado", $codTx);
        }
        return $resultado;
    } catch (Exception $e) {

        return "ERROR: " . $e->getMessage();
    }
}

/**
 * Consultar radicado es una funcion que permite almacenar cualquier tipo de archivo en el lado del servidor
 * @param $numRadicado 
 * @param $tipo
 * @author Hardy Niño
 * @return Retorna un String indicando si la operacion fue satisfactoria o no
 */
function consultarRadicado($numRadicado, $tipo) {
    try {
        /* @var $radicado type */
        $radinum = $numRadicado;
        //return $radinum, $tipo";
        if (strlen($radinum) != "14") {
            return "ERROR: El numero de radicado es encuentra incompleto. ";
        }
        global $ruta_raiz;

        $rad = new radicado($ruta_raiz);
        $rad->setRadiNumeRadi($radinum);
        $rsRad = $rad->consultar();
        // print_r($rsRad);
        //$coddepe = $rsRad['RADI_DEPE_ACTU'];
        $radicado = $rsRad['RADI_NUME_RADI'];
        //$radi_usua_actu = $rsRad['RADI_USUA_ACTU'];
        //$radi_nume_deri = $rsRad['RADI_NUME_DERI'];
        $rutaArch = $rsRad['RADI_PATH'];
        if (!$radicado) {
            return "ERROR: El radicado no existe";
        }
        $encrypt = new file();
        $year = substr($radinum, 0, 4);
        $depe = substr($radinum, 4, 3);
        $data[0]['nombre'] = $radicado;
        if ($rsRad['RADI_PATH'])
            $data[0]['ruta'] = RUTAWSIMA . "/core/vista/image.php?nombArchivo=" . $encrypt->encriptar(RUTA_BODEGA . $rutaArch);
        else
            $data[0]['ruta'] = 'SIN';
        $data[0]['tpDoc'] = $rsRad['tpDoc'];
        $data[0]['asunto'] = $rsRad['RA_ASUN'];
        if ($tipo == 2 || $tipo == 3) {
            $rsAnex = $rad->consultarDatosAnexos();
            $TPdocAnex = $rad->consultartpDoc();
            //  print_r($rsAnex);
            for ($i = 1; $i <= count($rsAnex); $i++) {
                if ($rsAnex[$i]['salida'] != $radicado) {
                    if ($rsAnex[$i]['rad']) {
                        $data[$i]['nombre'] = $rsAnex[$i]['rad'];
                        $rutaArch = $rsAnex[$i]['pathr'];
                        if ($rsAnex[$i]['pathr'])
                            $data[$i]['ruta'] = RUTAWSIMA . "/core/vista/image.php?nombArchivo=" . $encrypt->encriptar(RUTA_BODEGA . $rutaArch);
                        else
                            $data[$i]['ruta'] = 'SIN';
                        $data[$i]['tpDoc'] = $rsAnex[$i]['codiTP'];
                    } else {
                        $data[$i]['nombre'] = $rsAnex[$i]['anex_codigo'];
                        //$data[$i]['ruta']=$rsAnex[$i]['path'];       
                        if ($rsAnex[$i]['path'])
                            $data[$i]['ruta'] = RUTAWSIMA . "/core/vista/image.php?nombArchivo=" . $encrypt->encriptar(RUTA_BODEGA . "/$year/$depe/docs/" . $rsAnex[$i]['path']);
                        else
                            $data[$i]['ruta'] = 'SIN';
                        $data[$i]['tpDoc'] = $TPdocAnex[$rsAnex[$i]['codiTP']];
                        $data[$i]['asunto'] = $rsAnex[$i]['anex_desc'];
			$data[$i]['tipo_anexo'] = $rsAnex[$i]['anex_tipo'];
                    }
                }
            }
        }

        if (!$i)
            $i = 1;
        if ($tipo == 4 || $tipo == 3) {
            $rsHist = $rad->consultarHistorico();
            //    print_r($rsHist);
            while ($a <= count($rsHist)) {
                //for ($i = $aa; $i <= count($rsHist); $i++) {
                $data[$i]['fecha'] = $rsHist[$a]['fecha'];
                $data[$i]['depetx'] = $rsHist[$a]['depe'];
                $data[$i]['usuatx'] = $rsHist[$a]['usua'];
                $data[$i]['obs'] = $rsHist[$a]['obs'];
                $data[$i]['ususuadestuatx'] = $rsHist[$a]['usuadest'];
                $data[$i]['depedest'] = $rsHist[$a]['depedest'];
                $data[$i]['tipotx'] = $rsHist[$a]['tx'];
                $i++;
                $a++;
            }
        }

        return $data;
    } catch (Exception $e) {

        return "ERROR: " . $e->getMessage();
    }
}

/**
 * Consultar radicado es una funcion que permite almacenar cualquier tipo de archivo en el lado del servidor
 * @param $numRadicado 
 * @param $tipo
 * @author Hardy Niño
 * @return Retorna un String indicando si la operacion fue satisfactoria o no
 */
function consultarRadicadoCo($numRadicado, $tipo) {
    try {
        /* @var $radicado type */
        $radinum = $numRadicado;
        //return $radinum, $tipo";
        if (strlen($radinum) != "14") {
            return "ERROR: El numero de radicado es encuentra incompleto. ";
        }
        global $ruta_raiz;

        $rad = new radicado($ruta_raiz);
        $rad->setRadiNumeRadi($radinum);
        $rsRad = $rad->consultar();
        // print_r($rsRad);
        //$coddepe = $rsRad['RADI_DEPE_ACTU'];
        $radicado = $rsRad['RADI_NUME_RADI'];
        $dataResult = $radicado;
        //$radi_usua_actu = $rsRad['RADI_USUA_ACTU'];
        //$radi_nume_deri = $rsRad['RADI_NUME_DERI'];
        $rutaArch = $rsRad['RADI_PATH'];
        if (!$radicado) {
            return "ERROR: El radicado no existe";
        }
        $encrypt = new file();
        $year = substr($radinum, 0, 4);
        $depe = substr($radinum, 4, 3);
        $data[0]['nombre'] = $radicado;
        if ($rsRad['RADI_PATH']) {
            $rutaI = RUTAWSIMA . "/core/vista/image.php?nombArchivo=" . $encrypt->encriptar(RUTA_BODEGA . $rutaArch);
        } else {
            $rutaI = 'SIN';
        }
        $dataResult.="," . $rutaI;
        //$data[0]['tpDoc'] = $rsRad['tpDoc'];
        $dataResult.="," . $rsRad['tpDoc'];
        $dataResult.="," . $rsRad['RA_ASUN'];
        if ($tipo == 2) {
            $rsAnex = $rad->consultarDatosAnexos();
            $TPdocAnex = $rad->consultartpDoc();
            //print_r($rsAnex);
            for ($i = 1; $i <= count($rsAnex); $i++) {
                if ($rsAnex[$i]['salida'] != $radicado) {
                    if ($rsAnex[$i]['rad']) {
                        $dataResult.="," . $rsAnex[$i]['rad'];
                        //  $data[$i]['nombre'] = $rsAnex[$i]['rad'];
                        $rutaArch = $rsAnex[$i]['pathr'];
                        // $dataResult.="," . $rsAnex[$i]['pathr'];
                        if ($rsAnex[$i]['pathr'])
                            $rutaA = RUTAWSIMA . "/core/vista/image.php?nombArchivo=" . $encrypt->encriptar(RUTA_BODEGA . $rutaArch);
                        else
                            $rutaA = 'SIN';
                        //$data[$i]['ruta'] $rutaA;
                        $dataResult.="," . $rutaA;
                        //$data[$i]['tpDoc'] = $rsAnex[$i]['codiTP'];
                        $dataResult.="," . $rsAnex[$i]['codiTP'];
                        $dataResult.=',' . $rsAnex[$i]['anex_desc'];
                    } else {
                        //$data[$i]['nombre'] = $rsAnex[$i]['anex_codigo'];
                        $dataResult.="," . $rsAnex[$i]['anex_codigo'];
                        //$data[$i]['ruta']=$rsAnex[$i]['path'];       
                        if ($rsAnex[$i]['path'])
                            $rutaA = RUTAWSIMA . "/core/vista/image.php?nombArchivo=" . $encrypt->encriptar(RUTA_BODEGA . "/$year/$depe/docs/" . $rsAnex[$i]['path']);
                        else
                            $rutaA = 'SIN';
                        //$data[$i]['ruta'] $rutaA;
                        $dataResult.="," . $rutaA;
                        //$data[$i]['tpDoc'] = $TPdocAnex[$rsAnex[$i]['codiTP']];
                        $dataResult.=',' . $TPdocAnex[$rsAnex[$i]['codiTP']];
                        $dataResult.=',' . $rsAnex[$i]['anex_desc'];
                    }
                }
            }
        }
        return $dataResult;
    } catch (Exception $e) {

        return "ERROR: " . $e->getMessage();
    }
}

/**
 * 
 * @global type $ruta_raiz
 * @param type $correo
 * @param type $destinatarioOrg
 * @param type $asu
 * @param type $ane
 * @param type $correo
 * @param type $tpRadicado
 * @param type $cuentai
 * @param type $depe_actu
 * @param type $tip_rem
 * @param type $tdoc
 * @param type $tip_doc
 */
function radicarDocumento($destinatarioOrg, $asu, $ane, $correo, $tpRadicado, $cuentai, $depe_actu, $tip_rem, $tdoc, $tip_doc4) {
    $asu = mb_convert_encoding($asu, "HTML-ENTITIES", "UTF-8");
    $ane = mb_convert_encoding($ane, "HTML-ENTITIES", "UTF-8");
    // return "$destinatarioOrg, $asu, $ane,$correo, $tpRadicado, $cuentai, $depe_actu, $tip_rem, $tdoc, $tip_doc ";
    global $ruta_raiz;
    //if (is_numeric($tip_doc)==false)
        $tip_doc = 0;
    //return $tip_doc;
    try {
        $deptoclass = new dependencia($ruta_raiz);
        $deptoclass->setDepe_codi($depe_actu);
        $dtdepe = $deptoclass->consultar();
        if (!$dtdepe['depe_codi']) {
            throw new Exception("Dependencia no  valida");
        }elseif($dtdepe['depe_estado']=='0'){
	    throw new Exception("Dependencia Inactiva");
	}
        //echo  is_numeric($tip_rem)." $tip_rem";
        //return false;
        if(is_numeric($tip_rem)==false)
             throw new Exception("Tipo de remitente no valida ");
        if ($tip_rem != 0 and $tip_rem != 1 and $tip_rem != 2 and $tip_rem != 3) 
            throw new Exception("Tipo de remitente no valida ");
        
        /*switch ($tip_rem) {
            case 0:
            case 1:
            case 2:
            case 3:
                break;
            default:
                throw new Exception("Tipo de remitente no valida $tip_rem");
                break;
        }*/

        $carp_codi = $ent;
        //$carp_per=0;
        $med = 1;
        //validacion existencia de usuario
        if (!verificarCorreo($correo)) {
            throw new Exception("el mail no corresponde a un email valido");
        }
        $destinatario = array('documento' => $destinatarioOrg ['cc_documento'], 'cc_documento' => $destinatarioOrg ['cc_documento'], 'tipo_emp' => $destinatarioOrg ['tipo_emp']
            , 'nombre' => $destinatarioOrg ['nombre'], 'prim_apel' => $destinatarioOrg ['prim_apel'], 'seg_apel' => $destinatarioOrg ['seg_apel'],
            'telefono' => $destinatarioOrg ['telefono'], 'direccion' => $destinatarioOrg ['direccion'], 'mail' => $destinatarioOrg ['mail'],
            'otro' => $destinatarioOrg ['otro'], 'idcont' => $destinatarioOrg ['idcont'], 'idpais' => $destinatarioOrg ['idpais'], 'codep' => $destinatarioOrg ['codep'], 'muni' => $destinatarioOrg ['muni']);

        if ($destinatarioOrg ['nombre'] == null) {
            throw new Exception("El nombre del destinario no puede estar vacio");
        }

        if ($destinatarioOrg ['direccion'] == null) {
            throw new Exception("Falta la direci&oacute;n direccion");
        }
        $tpradclass = new tipoRadicado($ruta_raiz);
        $rsTpRad = $tpradclass->consultar();
        $tpvalida = 'bad';
        //print_r($rsTpRad); 
        for ($ai = 0; $ai < count($rsTpRad); $ai++) {

            if ($rsTpRad[$ai]['CODIGO'] == $tpRadicado)
                $tpvalida = 'ok';
        }

        if ($tpvalida == 'bad') {
            throw new Exception("Tipo de documento invalido");
        }

        $rad = new radicado($ruta_raiz);

        $usuario = new usuarioOrfeo($ruta_raiz);
        $datos = $usuario->consultadatos($correo);
        //validacion existencia de usuario
        if (!$datos[0] ['USUA_LOGIN']) {
            throw new Exception("ERROR: " . "el mail no corresponde a un usuario de sistema orfeo");
        }
        $radi_usua_radi = $datos[0]['USUA_CODI'];
        $coddepeRadi = $datos[0]['DEPCODI'];
        $usuLogin = $datos[0]['USUA_LOGIN'];
        $id_rol = $datos [0]['IDROL'];
        $codusuario = $datos [0]['USUA_CODI'];
        $usuadoc = $datos [0]['USUA_DOC'];
        $usuaDest = new usuarioOrfeo($ruta_raiz);
        //datos radicador
        $datos2 = $usuario->consultadatosJefe($depe_actu);
        $radi_usua_actu = $datos2[0]['USUA_CODI'];
        $coddepeactu = $datos2[0]['DEPCODI'];
        $usuLoginactu = $datos2[0]['USUA_LOGIN'];
        $id_rolactu = $datos2 [0]['IDROL'];
        $codusuarioActu = $datos2 [0]['USUA_CODI'];
        $usuadocActu = $datos2 [0]['USUA_DOC'];
        $med = 1;
        //$tmp_mun->municipio_codigo ( $destinatario ["codep"], $destinatario ["muni"] );
        $rad->setRadiTipoDeri($tpRadicado);
        $rad->setRadiCuentai($cuentai);
        $rad->setEespCodi(0);
        $rad->setRadiNumeHoja(0);
        $rad->setMrecCodi($med);
        $rad->setRadiFechOfic(date("Y-m-d"));

        $rad->setRadiNumeDeri(null);
        $rad->setRadiPais(170);
        $rad->setDescAnex($ane);
        $rad->setRaAsun($asu);
        //usuario actual
        $rad->setRadiDepeActu($coddepeactu);
        $rad->setRadiUsuaActu($radi_usua_actu);
        $rad->setRadiUsuaActuDoc($usuadocActu);
        $rad->setRadiUsuaActurol($id_rolactu);
        //usuario Radicador
        $rad->setRadiDepeRadi($coddepeRadi);
        $rad->setUsuaCodi($radi_usua_radi);
        $rad->setUsuadoc($usuadoc);
        $rad->setrolid($id_rol);
        $rad->setTrteCodi($tip_rem);
	//if($tdoc=='' || $tdoc==NULL){
	$rad->setTdocCodi('0');
	/*}else
            $rad->setTdocCodi($tdoc);*/
        $rad->setTdidCodi($tip_doc);
        $rad->setCarpCodi($carp_codi);
        //$rad->setCarPer($carp_per);
        // $rad->setTrteCodi($tip_rem);
        $rad->setRadiPath('null');
        $rad->setSgd_apli_codi(0);
        $rad->setTipRad($tpRadicado);
        $codTx = 2;
        $flag = 1;
        //datos de resmitente o destinatario
        $rad->setTipo($destinatario['tipo_emp']);
        //$rad->setCodigo_orfeo();
        $rad->setCedula($destinatario['documento']);
        if(!$destinatario['documento']){
            $rad->setCedula(0);
        }
	elseif(!is_numeric($destinatario['documento'])){
	    throw new Exception ("El documento del Remitente/Destinatario no es numerico");
	}
        $rad->setNombre_remitente($destinatario['nombre']);
        $rad->setPrimer_apellidos($destinatario['prim_apel']);
        $rad->setSeg_apellidos($destinatario['seg_apel']);
        $rad->setContinente($destinatario['idcont']);
        $rad->setPais($destinatario['idpais']);
        $rad->setMuni($destinatario['muni']);
        $rad->setDepto($destinatario['codep']);
        $rad->setDireccion_remitente($destinatario['direccion']);
        $rad->setTelefono_remitente($destinatario['telefono']);
        $rad->setSigla($destinatario['otro']);
	if(trim($destinatario['mail'])!='' && $destinatario['mail']!=null){
		if(!verificarCorreo){
		    throw new Exception("ERROR:"." Email de destinatario/remitente no valido");
		}
        }
        $rad->setEmail($destinatario['mail']);
        $rad->setNit(0);
        if ($rad->validar($asu, 350, 1) == 'fail')
            throw new Exception("El asunto  sobre paso el  tama&ntilde;o maximo de caracteres ");
        if ($rad->validar($ane, 100, 1) == 'fail')
            throw new Exception("El descripcion de anexois  sobre paso el  tama&ntilde;o maximo de caracteres ");
	if ($rad->validar($cuentai, 30, 1) == 'fail')
            throw new Exception("Cuenta interna sobrepaso del tama&ntilde;o maximo de caracteres");
        //agregar ciudadano
        $numEmpre = 0;
        $numCiu = 0;
        $numEsp = 0;
        $numfun = 0;

        if ($destinatario['tipo_emp'] == 3) {
            $numEmpre = $rad->empresa();
            if ($numCiu == 0 && $numEmpre <= 0) {
                throw new Exception($numEmpre);
            }
        }
        if ($destinatario['tipo_emp'] == 2) {
            $numCiu = $rad->ciudadano();
            if ($numEmpre == 0 && $numCiu <= 0) {
                throw new Exception($numCiu);
            }
        }

        //if (($numEmpre == 0 && $numCiu <= 0) || ($numEmpre <= 0 && $numCiu == 0)) {
        if ($destinatario['tipo_emp'] != 2 && $destinatario['tipo_emp'] != 3) {
            throw new Exception(" Tipo de destinatario Erroneo");
        }
        $numRAdicado = $rad->radicar();
        if ($numRAdicado == 0 or $numRAdicado == null) {
            throw new Exception("ERROR: " . "Problemas con la radicacion");
        }
	$rad->setCiu($numCiu);
	$rad->setOem($numEmpre);
        $rad->dir_drecciones();
        /* if($numRAdicado==0 or $numRAdicado== null){
          throw new Exception( "ERROR: " . "Problemas con la radicacion");
          } */
        return $numRAdicado;
        $rad->dependencia = trim($coddepe);
    } catch (Exception $e) {

        return "ERROR: " . $e->getMessage();
    }
}

?>
