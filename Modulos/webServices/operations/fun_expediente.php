<?php 
include_once $ruta_raiz . "/core/Modulos/radicacion/clases/radicado.php";
include_once $ruta_raiz . "/core/Modulos/Expediente/clases/expediente.php";
include_once $ruta_raiz . "/core/clases/usuarioOrfeo.php";
include_once "$ruta_raiz/core/Modulos/radicacion/clases/tx.php";
include_once $ruta_raiz . '/core/clases/file-class.php';
/**
 * @author Hardy Deimont Niño
 * 
 * Esta funcion permite crear un expediente a partir de un radicado
 * 
 * @param $nurad, numero de radicado
 * @param $usuario, este parametro es el usuario que crea el expediente, es el usuario de correo
 * @param $anoExp, año del expediente yyyy
 * @param $fechaExp, fecha de creacion del expediente 'dd/mm/yy'
 * @param $codiSRD, codigo serie
 * @param $codiSBRD, codigo subserie
 * @param $codiProc, codigo procedimiento
 * @param $digCheck, digito de verificacion. Corresponde a la 'E'
 * @param $tmr
 * @return El numero de expediente para asignarlo en aplicativo de contribuciones AI 
 */
/* function crearExpediente($nurad, $usuario, $anoExp, $fechaExp, $codiSRD, $codiSBRD, $codiProc, $digCheck, $tmr) {

  global $ruta_raiz;
  //include_once $ruta_raiz . "/core/Modulos/Expediente/clases/expediente.php";
  include_once $ruta_raiz . "/old/include/tx/Expediente.php";

  $db = new ConnectionHandler("$ruta_raiz");
  $db->conn->SetFetchMode(ADODB_FETCH_NUM);
  $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);

  $expediente = new Expediente ( $ruta_raiz );

  //Aqui busco la informacion necesaria del usuario para la creacion de expedientes
  $sql = "select USUA_CODI,DEPE_CODI,USUA_DOC from usuario where upper(usua_email) = upper ('" . $usuario . "@superservicios.gov.co')";
  $rs = $db->conn->query ( $sql );
  while ( ! $rs->EOF ) {
  $codusuario = $rs->fields ['USUA_CODI'];
  $dependencia = $rs->fields ['DEPE_CODI'];
  $usua_doc = $rs->fields ['USUA_DOC'];
  $usuaDocExp = $usua_doc;
  $rs->MoveNext ();
  }

  //Insercion para el TMR
  $sql = "insert into sgd_rdf_retdocf (sgd_mrd_codigo,radi_nume_radi,depe_codi,usua_codi,usua_doc,sgd_rdf_fech)";
  $sql .= " values ($tmr,$nurad,$dependencia,$codusuario,'$usua_doc',SYSDATE)";

  $db->conn->query ( $sql );

  $trdExp = substr ( "00" . $codiSRD, - 2 ) . substr ( "00" . $codiSBRD, - 2 );
  $secExp = $expediente->secExpediente ( $dependencia, $codiSRD, $codiSBRD, $anoExp );
  $consecutivoExp = substr ( "00000" . $secExp, - 5 );
  $numeroExpediente = $anoExp . $dependencia . $trdExp . $consecutivoExp . $digCheck;

  $numeroExpedienteE = $expediente->crearExpediente ( $numeroExpediente, $nurad, $dependencia, $codusuario, $usua_doc, $usuaDocExp, $codiSRD, $codiSBRD, 'false', $fechaExp, $codiProc );

  $insercionExp = $expediente->insertar_expediente ( $numeroExpediente, $nurad, $dependencia, $codusuario, $usua_doc );

  return $numeroExpedienteE;

  } */

/**
 * 
 * @param $numRadicado
 * @param $numExpediente
 * @param $usuaLoginMail 
 * @param $observa Observacion de anexo
 * @return string $numRadicado si fue exitoso ó "ERROR: ..." si fallo la operacion
 */
function anexarExpediente($numRadicado, $numExpediente, $usuaLoginMail, $observa) {
//return  "$numRadicado, $numExpediente,$matrix,$faro, $usuaLoginMail, $observa";
    global $ruta_raiz;
    try {
        if ($numRadicado == null or $usuaLoginMail == null or $observa == null or $numExpediente == null) {
            throw new Exception("ERROR: No se recibieron todos los datos necesarios para realizar la operacion.");
        }


        $rad = new radicado($ruta_raiz);
        //validacion existencia de radicado
        $rad->setRadiNumeRadi($numRadicado);
        $rsRad = $rad->consultar();
        $radiDepeActu = $rsRad['RADI_DEPE_ACTU'];
        $radiUsuaActu = $rsRad['RADI_USUA_ACTU'];

        if ($radiUsuaActu == null) {
            throw new Exception("ERROR: El radicado $numRadicado no existe.");
        }
        $exp = new expediente($ruta_raiz);
        $exp->setNumExp($numExpediente);
        $exp->setNumRadicado($numRadicado);
        $expdata = $exp->consultarExpediente2();
//print_r($expdata);
        if ($expdata=='error') {
            throw new Exception("El numero de expediente $numExpediente no existe.");
        }
        // return $expdata['expediente'];
        $exp->setNumExp($expdata);
        $estado = $exp->estadoRadicadoExpediente();
        $usuario = new usuarioOrfeo($ruta_raiz);
        $datos = $usuario->consultadatos($usuaLoginMail);
        $usua_codi = $datos[0]['USUA_CODI'];
        $usua_depe = $datos[0]['DEPCODI'];
        $usuLogin = $datos[0]['USUA_LOGIN'];
        $id_rol = $datos [0]['IDROL'];
        $codusuario = $datos [0]['USUA_CODI'];
        $usua_doc = $datos [0]['USUA_DOC'];
        $resultado = null;
        if ($usuLogin == null or sizeof($usuLogin) == 0) {
            throw new Exception(" El email no  corresponde a ningun usuario");
        }
        /* print_r($estado);
          echo "<hr>".$estado."<hr>"; */
        try {
            switch ($estado) {
                case 0 :
                    throw new Exception("El documento con numero de radicado  {$numRadicado} ya fue anexado al expediente {$numExpediente}");
                case 1 :
                    throw new Exception("El documento con numero de radicado {$numRadicado} ya fue anexado al expediente {$numExpediente} y archivado fisicamente");
                case 2 :
                    throw new Exception("pendiente");
                    /* $consulta = "UPDATE SGD_EXP_EXPEDIENTE SET SGD_EXP_ESTADO=0,SGD_EXP_FECH={$fecha},USUA_CODI=" . $usua ['usua_codi'] . ",USUA_DOC='" . $usua ['usua_doc'] . "'
                      ,DEPE_CODI=" . $usua ['usua_depe'] . " WHERE RADI_NUME_RADI={$numRadicado}
                      AND SGD_EXP_NUMERO='{$numExpediente}'"; */
                    break;
                default :
                    $resultado = $exp->incluirExp($usua_codi, $usua_doc, $usua_depe);
                    break;
            }
        } catch (Exception $e) {

            throw $e;
        }


        if ($resultado['ERROR']) {
            throw new Exception("No se realizo la operacion");
        } else {
            $tx = new tx($ruta_raiz);
            $tipoTx = 53; //Codigo de la transaccion
            $radicadosSel[0] = $numRadicado;
            $radicados = $exp->insertarHistoricoExp($radicadosSel, $usua_depe, $usua_codi, $observa, $tipoTx, 0);
            $resultado = $radicados [0];
        }
    } catch (Exception $e) {

        $resultado = "ERROR: " . $e->getMessage();
    }

    return $resultado;
}

/**
 * 
 * @param $numeExpediente
 * @param $tipo
 * @return string $numRadicado si fue exitoso ó "ERROR: ..." si fallo la operacion
 */
function consultarExp($numeExpediente) {
//return  "$numeExpediente, $tipo";
    global $ruta_raiz;
    try {


        $encrypt = new file();
        if ($numeExpediente == null){
            throw new Exception("ERROR: No se recibi&oacute; dato de numero de expediente.");
	}
        $exp = new expediente($ruta_raiz);
        $exp->setNumExp($numeExpediente);
        $tpnom = 'Expediente';
        $respd = 'no existe';
        $expdata = $exp->consultarExpediente2();
//return  $expdata."$numeExpediente, $tipo hs";
        if ($expdata == 'error') {
            throw new Exception("El numero de $tpnom $numeExpediente  $respd.");
        }
        $expencritar = $expdata . '/' . date('Y-m-d');
//return $encrypt->encriptar($expencritar);
        // return $expdata['expediente'];
        $resultado = RUTAWSIMA . "/core/Modulos/Expediente/vista/expedienteWeb.php?eo=" . $encrypt->encriptar($expencritar);
    } catch (Exception $exc) {
        $resultado=  'ERROR: '.$exc->getMessage();
    }

    return $resultado;
}

?>
