<?php 
include_once "Message.php";
include_once "Properties.php";
include_once 'Property.php';
include_once 'Record.php';

/**
 * Clase que ofrece operaciones para realizar 
 * conversiones entre obtejos Message y Strings
 *
 */
class MessageUtil {
	
	

	function __construct(){
		
		
	}

	/**
	 * Dado un Objeto Message devuelve su string representativo.
	 *
	 * @param Message $mensaje
	 * @return String representativo del mensaje
	 */
	public function getStringFromMessage(Message $mensaje) {
				
		$mensajeString = NULL;
		
		try {
			
			if ($mensaje != NULL) {
				
				$doc = new DOMDocument ( "1.0", "iso-8859-1" );
				//$doc = new DOMDocument ();
				$doc->formatOutput = true;
				
				$tagMessage = $doc->createElement ( 'message' );
				$doc->appendChild ( $tagMessage );
				
				//1. Miramos las properties
				if ($mensaje->getProperties () != NULL && sizeof ( $mensaje->getProperties () ) > 0) {
					
					$tagProperties = $doc->createElement ( "properties" );
					$tagMessage->appendChild ( $tagProperties );
					
					$propiedades = $mensaje->getProperties ();
					
					for($i = 0; $i < sizeof ( $propiedades ); $i ++) {
						
						$propiedad = $propiedades [$i];
						
						if ($propiedad != NULL) {
							
							$name = $propiedad->getName ();
							$value = $propiedad->getValue ();
							$type = $propiedad->getType ();
							$dateFormat = $propiedad->getDateFormat ();
							
							if ($name != NULL && $type != NULL) {
								
								//creo el tag property
								$tagProperty = $doc->createElement ( "property" );
								$tagProperties->appendChild ( $tagProperty );
								
								//   Creamos un nuevo elemento llamado Nombre
								$tagName = $doc->createElement ( "name" );
								$tagValue = $doc->createElement ( "value" );
								$tagType = $doc->createElement ( "type" );
								
								//   Lo añadimos dentro del nodo $DatosPersonales
								$tagProperty->appendChild ( $tagName );
								$tagProperty->appendChild ( $tagValue );
								$tagProperty->appendChild ( $tagType );
								
								// Codificamos el texto a añadir dentro de Nombre
								$valorName = $doc->createTextNode (utf8_encode( $name));
								
								if ($value == null) {

									$valorValue = $doc->createTextNode (utf8_encode(""));
									
								}else {

									$valorValue = $doc->createTextNode (utf8_encode($value) );
								}
								
								
								$valorType = $doc->createTextNode (utf8_encode($type) );
								
								// 	Añadimos el texto dentro de Nombre
								$tagName->appendChild ( $valorName );
								$tagValue->appendChild ( $valorValue );
								$tagType->appendChild ( $valorType );
								
								if ($dateFormat != NULL) {
									
									//creo el tag dateFormat y lo agrego a la propiedad
									
									$tagDateFormat = $doc->createElement ( "dateFormat" );
									$tagProperty->appendChild ( $tagDateFormat );
									$valorDateFormat = $doc->createTextNode ( $dateFormat );
									$tagDateFormat->appendChild ( $valorDateFormat );
								
								} //end if - existe dateFormat
							

							} //end if - datos completos de la propiedad
						

						} //end if - propiedad no null
					

					} // end for - recorreo properties
				

				} //si hay properties
				

				//2. Miramos el dataSet
				if ($mensaje->getDataSet () != NULL && sizeof ( $mensaje->getDataSet () ) > 0) {
					
					$tagDataSet = $doc->createElement ( "dataSet" );
					$tagMessage->appendChild ( $tagDataSet );
					
					$registrosList = $mensaje->getDataSet ();
					
					for($j = 0; $j < sizeof ( $registrosList ); $j ++) {
						
						$unRegistro = $registrosList [$j];
						
						if ($unRegistro != NULL) {
							
							$tagRecord = $doc->createElement ( "record" );
							$tagDataSet->appendChild ( $tagRecord );
							
							$propiedadesRegistro = $unRegistro->getValue ();
							
							if ($propiedadesRegistro != NULL && sizeof ( $propiedadesRegistro ) > 0) {
								
								$tagValueRecord = $doc->createElement ( "value" );
								$tagRecord->appendChild ( $tagValueRecord );
								
								for($z = 0; $z < sizeof ( $propiedadesRegistro ); $z ++) {
									
									$unaPropiedad = $propiedadesRegistro [$z];
									
									if ($unaPropiedad != NULL) {
										
										$name = $unaPropiedad->getName ();
										$value = $unaPropiedad->getValue ();
										$type = $unaPropiedad->getType ();
										$dateFormat = $unaPropiedad->getDateFormat ();
										
										if ($name != NULL && $type != NULL) {
											
											//creo el tag property
											$tagProperty = $doc->createElement ( "property" );
											$tagValueRecord->appendChild ( $tagProperty );
											
											//   Creamos un nuevo elemento llamado Nombre
											$tagName = $doc->createElement ( "name" );
											$tagValue = $doc->createElement ( "value" );
											$tagType = $doc->createElement ( "type" );
											
											//   Lo añadimos dentro del nodo $DatosPersonales
											$tagProperty->appendChild ( $tagName );
											$tagProperty->appendChild ( $tagValue );
											$tagProperty->appendChild ( $tagType );
											
											// Codificamos el texto a añadir dentro de Nombre
											$valorName = $doc->createTextNode (utf8_encode($name));
											
											if ($value == null) {

												$valorValue = $doc->createTextNode (utf8_encode(""));
									
											}else {

												$valorValue = $doc->createTextNode (utf8_encode($value) );
											}
											
											$valorType = $doc->createTextNode (utf8_encode($type));
											
											// 	Añadimos el texto dentro de Nombre
											$tagName->appendChild ( $valorName );
											$tagValue->appendChild ( $valorValue );
											$tagType->appendChild ( $valorType );
											
											if ($dateFormat != NULL) {
												
												//creo el tag dateFormat y lo agrego a la propiedad
												$tagDateFormat = $doc->createElement ( "dateFormat" );
												$tagProperty->appendChild ( $tagDateFormat );
												
												$valorDateFormat = $doc->createTextNode ( $dateFormat );
												$tagDateFormat->appendChild ( $valorDateFormat );
											
											} //end if - existe dateFormat
										
										} //end if - datos completos de la propiedad		
									

									} // si la propiedad no es null
								
								} // end for - recorre propiedades del record
							
							} // end if - si el registro tiene proppiedades
						
						} //end if - registro no null
					
					} //end for - recorre dataset
				
				} // si hay dataSet

				//obtengo el String del documento construido
				$mensajeString = $doc->saveXML ();
				//$mensajeString = $doc->saveHTML();

			} //end if mensaje no es null
		
		} catch ( Exception $e ) {
			
			throw new Exception ( "Ocurrio un error al convertir el mensaje a cadena de texto." );
		
		}
				
		return $mensajeString;
		
	}
	
	
	/**
	 * Dado un String representativo de mensaje XML 
	 * debe retornar el objeto Message representativo
	 *
	 * @param String $mensajeString
	 * @return Message Objeto que representa el String
	 */
	public function getMessageFromString($mensajeString) {
		
		$mensaje = NULL;
		
		if ($mensajeString != NULL) {
						
			$doc = new DOMDocument("1.0", "iso-8859-1");
			$doc->loadXML($mensajeString);
			
			//si obtuvo un documento XML
			if ($doc != NULL) {
				
				$tagMessage = $doc->getElementsByTagName("message");
				
				if ($tagMessage!= NULL && $tagMessage->length > 0) {
					
					$mensaje = new Message();
					
					//1. obtenemos las properties					
					$tagProperties = $tagMessage->item(0)->getElementsByTagName("properties");
					
					if ($tagProperties != NULL && $tagProperties->length > 0) {
						
						$properties = $this->getArrayPropertyFromTag($tagProperties->item(0));
						
						if ($properties != NULL && sizeof($properties) > 0) {
							
							$mensaje->setProperties($properties);
							
						}// si se obtuvieron propiedades 
						
					}//si se encontro un tagProperties
					
					//2. Obtenemos los registros
					$tagDataSet = $tagMessage->item(0)->getElementsByTagName("dataSet");
					
					if ($tagDataSet != NULL && $tagDataSet->length > 0) {
						
						$registros = $this->getArrayRecordFromTag($tagDataSet->item(0));
									
						if ($registros != NULL && sizeof($registros) > 0) {
								
							$mensaje->setDataSet($registros);
						
						}// si se obtuvieron registros
						
						
					}// si encontro la propiedad dataset
					
				}// si encontro el tag message
				
			}// end if - doc no null
			
		}// end if - mensaje no null

		return $mensaje;
		
	}
	
	/**
	 * 
	 * @param $arrayProperty
	 * @param $nombrePropiedad
	 * @return Property
	 */
	public function getPropertyFromArray($arrayProperty, $nombrePropiedad) {
		
		$myPropiedad = null;
		
		if ($arrayProperty != null && sizeof($arrayProperty) > 0 && $nombrePropiedad != null) {
			
			for ($i = 0; $i < sizeof($arrayProperty); $i++) {
				
				$unaProperty = $arrayProperty[$i];

				if ($unaProperty != null) {
					
					if (strtoupper($unaProperty->getName()) == strtoupper($nombrePropiedad) ) {
						
						$myPropiedad = $unaProperty;
						break;
					}
					
					
				}//end if - property no null			
				
			}//end for - recorrer array de propiedades
			
		}//end if completitud de datos
		
		return $myPropiedad;
	}
	
	
	/**
	 * Devuelve el listado de propiedades de un tag
	 *
	 * @param DOMNode $nombreTag
	 * @return Array de Objetos de tipo Property
	 */
	private  function getArrayPropertyFromTag($nombreTag) {
		
		//array de Property a devolver
		$arrayProperty = NULL;
		
		if ($nombreTag != NULL) {
			
			
			$properties = $nombreTag->getElementsByTagName ( "property" );
			
			if ($properties != NULL && $properties->length > 0) {
				
				//inicializo el array
				$arrayProperty = array();
				
				foreach ( $properties as $property ) {
					
					//obtengo el nombre
					$names = $property->getElementsByTagName ( "name" );
					$name = $names->item(0)->nodeValue;
					
					//obtengo el valor
					$values = $property->getElementsByTagName ( "value" );
					$value = $values->item(0)->nodeValue;
					
					//obtengo el tipo
					$types = $property->getElementsByTagName ( "type" );
					$type = $types->item(0)->nodeValue;
					
					//obtengo el formato de la fecha
					$dateFormat = NULL;
					$dateFormats = $property->getElementsByTagName ( "dateFormat" );
					
					if ($dateFormats != NULL) {
						
						$dateFormat = $dateFormats->item(0)->nodeValue;
					
					} // si encontro formato de fecha
					

					//Completitud de datos para la propiedad
					if ($name != NULL && $value != NULL && $type != NULL) {
						
						$unaProperty =  new Property($name,$value,$type);
						
						if ($dateFormat != NULL) {
							$unaProperty->setDateFormat($dateFormat);
						}
						
						array_push($arrayProperty,$unaProperty);
						
					}
				
				} //end for - recorre propiedades

			}
		
		} // si el tag no  es null
		

		return $arrayProperty;
	
	}//end method
	
	
	/**
	 * Devuelve un listado de Record del tag dataSet
	 *
	 * @param DOMNode $nombreTag
	 * @return Array Record
	 */
	private function getArrayRecordFromTag($nombreTag) {
		
		$arrayRecord = NULL;
		
		if ($nombreTag != NULL) {
						
			$registros = $nombreTag->getElementsByTagName ("record");
			
			if ($registros != NULL && $registros->length > 0) {
				
				$arrayRecord = array();
				
				foreach ($registros as $registro){
					
					$unRecord = new Record();
					
					$value = $registro->getElementsByTagName ("value");
					
					if ($value != NULL && $value->length > 0) {
						
						$propiedades = $this->getArrayPropertyFromTag($value->item(0));						
						
						if ($propiedades != NULL && sizeof($propiedades) > 0) {
							
							$unRecord->setValue($propiedades);
							
						}//end if - si se obtuvieron propiedades asociadas al record
						
					}// si encontro el tag value
					
					array_push($arrayRecord, $unRecord);
					
				}//end for - recorrer los record
				
			}//si hay registros
			
		}// si el tag no es null
		
		return $arrayRecord;
		
	}//end method
	
}

?>
