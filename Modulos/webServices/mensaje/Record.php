<?php 
include_once 'Property.php';
/**
 * Clase que modela un registro del mensaje
 *
 */
class Record {
		
	/**
	 * Array de Property.
	 *
	 * @var Array
	 */
	protected $value;
	
	
	public function __construct(){
		
		$this->value = array();
		
	}
	
	/**
	 * Devuelve una instancia del listado de propiedades
	 * 
	 * 
	 * @return Array
	 */
	public function getValue() {
	
		return $this->value;
	}

	
	/**
	 * Agrega una propiedad
	 *
	 * @param Property $property
	 */
	public function addProperty(Property $property) {
		
		if ($property != NULL) {
			
			array_push($this->value, $property);
			
		}
		
	}
	
	/**
	 * @param Array $value
	 */
	public function setValue($value) {
		
		if ($value != NULL && sizeof($value) > 0) {

			$this->value = $value;
			
		}
		
	}
	
}

?>
