<?php 
/**
 * Clase que modela una propiedad o variable
 *
 */
class Property {
	
	/**
	 * Nombre de la propiedad.
	 * No puede ser null
	 *
	 * @var unknown_type  
	 */
	protected $name;
	/**
	 * Valor de la propiedad.
	 * No puede ser null
	 *
	 * @var unknown_type
	 */
	protected $value;
	/**
	 * Tipo de dato asociadoa al valor de la propiedad
	 * Posibles valores: String, Integer, Boolean, Long, Double, Date
	 * 
	 *
	 * @var unknown_type
	 */
	protected $type;
	/**
	 * Formato de la fecha en caso de que Type = Date.
	 * Ejemplo: dd/mm/YYYYY
	 *
	 * @var unknown_type
	 */
	protected $dateFormat; 
	
	
	public function __construct($name, $value, $type){
		
		
		$this->name = $name;
		$this->value = $value;
		$this->type = $type;
		$this->dateFormat = NULL;
		
	}
	
	/**
	 * @return unknown_type
	 */
	public function getDateFormat() {
		return $this->dateFormat;
	}
	
	/**
	 * @return unknown_type
	 */
	public function getName() {
		return $this->name;
	}
	
	/**
	 * @return unknown_type
	 */
	public function getType() {
		return $this->type;
	}
	
	/**
	 * @return unknown_type
	 */
	public function getValue() {
		
		 return $this->value;
		
	}
	
	/**
	 * @param unknown_type $dateFormat
	 */
	public function setDateFormat($dateFormat) {
		$this->dateFormat = $dateFormat;
	}
	
	/**
	 * @param unknown_type $name
	 */
	public function setName($name) {
		$this->name = $name;
	}
	
	/**
	 * @param unknown_type $type
	 */
	public function setType($type) {
		$this->type = $type;
	}
	
	/**
	 * @param unknown_type $value
	 */
	public function setValue($value) {
		$this->value = $value;
	}

	
	
	
}


?>
