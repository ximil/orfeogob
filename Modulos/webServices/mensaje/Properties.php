<?php 
include_once 'Property.php';
/**
 * Clase que modela una agrupacion de propiedades
 *
 */
class Properties {
	
	/**
	 * Array de objetos de tipo Property.
	 *
	 * @var Array
	 */
	protected $properties;
	
	/**
	 * 
	 * Retorna el array de propiedades
	 * 
	 * @return Array
	 */
	public function getProperties() {
		/*
		if ($this->properties == NULL) {
			
			$this->properties = array();
		}
		*/
		
		return $this->properties;
	}

	/**
	 * Agrega una propiedad
	 *
	 * @param Property $property
	 */
	public function addProperty(Property $property) {
		
		if ($property != NULL) {
			
			//array_push($this->getProperties(), $property);
			array_push($this->properties, $property);
			
		}	
		
	}
	
	/**
	 * @param Array $properties
	 */
	public function setProperties($properties) {
		
		if ($properties != NULL && sizeof($properties) > 0) {

			$this->properties = $properties;
			
		}
		
	}
	
}


?>
