<?php 
include_once 'Properties.php';
include_once 'Record.php';
/**
 * Clase que modela un ojeto de tipo Mensaje
 *
 */
class Message extends Properties {
	
	
	/**
	 * Array de Record
	 *
	 * @var Array
	 */
	protected $dataSet;
	
	/**
	 * Constructor
	 *
	 */
	function __construct(){
		
		$this->properties = array();
		$this->dataSet = array();
		
	}
	
	
	/**
	 * @return Array
	 */
	public function getDataSet() {
			
				
		return $this->dataSet;
	}

	/**
	 * Agraga un record al dataset
	 *
	 * @param Record $record
	 */
	public function addRecord(Record $record) {
		
		array_push($this->dataSet,$record);
		
		
	}
	
	/**
	 * @param Array $dataSet
	 */
	public function setDataSet($dataSet) {
		
		if ($dataSet != NULL && sizeof($dataSet) > 0) {

			$this->dataSet = $dataSet;
			
		}
		
	}

	
	
	
}




?>
