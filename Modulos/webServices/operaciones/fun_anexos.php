<?php 
/**
 *   
 * Script que contiene las funciones que registradas como operaciones de servicio 
 * asociadas a la categoria Anexos 
 * 
 */

/**
 * funcion que crea un Anexo, y ademas decodifica el anexo enviasdo en base 64
 *
 * @author  cgprada - modificado 10-11-2010
 * @author  hardy deimont Niño - modificado 10-10-2013
 * 
 * Ajustes para que devuelva el numero de radicado e inlcusion de algunas validaciones
 *
 * @param string $radiNume numero del radicado al cual se adiciona el anexo
 * @param base64 $file archivo codificado en base64
 * @param string $filename nombre original del anexo, con extension
 * @param string $correo correo electronico del usuario que adiciona el anexo
 * @param string $descripcion descripcion del anexo
 * @return string mensaje de error en caso de fallo o el numero del anexo en caso de exito
 */
function crearAnexo($radiNume,$file,$filename,$correo,$descripcion){
	
	global $ruta_raiz;
	
	if ($radiNume != null && strlen($radiNume) > 0 &&  $file != null && strlen($file) > 0 && $correo != null && 
	strlen($correo) > 0 && $descripcion != null && strlen($descripcion) > 0) {
		
		$db = new ConnectionHandler ( $ruta_raiz, 'WS' );
		
		//validacion existencia de radicado
		$consultaRad = "SELECT radi_nume_radi, radi_depe_actu, radi_usua_actu FROM RADICADO WHERE radi_nume_radi = '$radiNume'";
		$rsRad = $db->query($consultaRad);
		
		if ($rsRad != null && !$rsRad->EOF) {
			
			$radiDepeActu = $rsRad->fields['RADI_DEPE_ACTU'];
			$radiUsuaActu = $rsRad->fields['RADI_USUA_ACTU'];
			
			//validacion existencia de usuario
			$usuario = getUsuarioCorreo ( $correo );
			
			if ($usuario != null && sizeof($usuario) > 1 && $usuario['error'] == null) {
				
				$usuCod = $usuario['codusuario'];
				$usuDep = $usuario['dependencia'];
				$usuLogin = $usuario['login'];
				
				//validacion de usuario actual
				
				if ($radiDepeActu == $usuDep && $usuCod == $radiUsuaActu) {
					
					$ruta = RUTA_BODEGA . substr ( $radiNume, 0, 4 ) . "/" . substr ( $radiNume, 4, 3 ) . "/docs/";
					$numAnexos = numeroAnexos ( $radiNume, $db ) + 1;
					$maxAnexos = maxRadicados ( $radiNume, $db ) + 1;
					$extension = substr ( $filename, strrpos ( $filename, "." ) + 1 );
					$numAnexo = ($numAnexos > $maxAnexos) ? $numAnexos : $maxAnexos;
					$nombreAnexo = $radiNume . substr ( "00000" . $numAnexo, - 5 );
					$subirArchivo = subirArchivo ( $ruta, $file, $nombreAnexo . "." . $extension );

					if ($subirArchivo) {
						
						$tamanoAnexo = $subirArchivo / 1024; //tamano en kilobytes
						$fechaAnexado = $db->conn->OffsetDate ( 0, $db->conn->sysTimeStamp );
						$tipoAnexo = tipoAnexo ( $extension, $db );
						$tipoAnexo = ($tipoAnexo) ? $tipoAnexo : "NULL";
						
						/**
						 * @author cgprada 
						 * se ajustara la consulta para que quede lo mas parecida a la funcionalidad original
						 * 
						 */
						/*
						$consulta = "INSERT INTO ANEXOS (ANEX_CODIGO,ANEX_RADI_NUME,ANEX_TIPO,ANEX_TAMANO,ANEX_SOLO_LECT,ANEX_CREADOR,
		            				ANEX_DESC,ANEX_NUMERO,ANEX_NOMB_ARCHIVO,ANEX_ESTADO,SGD_REM_DESTINO,ANEX_FECH_ANEX, ANEX_BORRADO) 
		            				VALUES('$nombreAnexo',$radiNume,$tipoAnexo,$tamanoAnexo,'N','$usuario ['login']','$descripcion'
		            				,$numAnexo,'$nombreAnexo.$extension',0,1,$fechaAnexado, 'N')";
						*/
						$consulta = "INSERT INTO ANEXOS (ANEX_RADI_NUME, ANEX_CODIGO, ANEX_TIPO, ANEX_TAMANO, ANEX_SOLO_LECT, ANEX_CREADOR, ANEX_DESC, 
						ANEX_NUMERO, ANEX_NOMB_ARCHIVO, ANEX_BORRADO, ANEX_ESTADO, SGD_REM_DESTINO, SGD_DIR_TIPO, ANEX_DEPE_CREADOR, ANEX_FECH_ANEX) 
						VALUES ('$radiNume', '$nombreAnexo', $tipoAnexo, $tamanoAnexo, 'N', '$usuLogin', '$descripcion', $numAnexo, 
						'$nombreAnexo.$extension', 'N', 0, 1, 1, $usuDep, $fechaAnexado)";
						
						$rsInsert = $db->query ( $consulta );
						
						$consultaVerificacion = "SELECT ANEX_CODIGO FROM ANEXOS WHERE ANEX_CODIGO = '$nombreAnexo'";
						$rs = $db->query ( $consultaVerificacion );
						
						if ($rs != null && !$rs->EOF) {

							$codAnexo = $rs->fields ['ANEX_CODIGO'];
							
							return  $codAnexo;
						}
					
					} else {
						
						return "ERROR: Ocurrio un error al momento de subir el archivo asociado al anexo.";
					}
				
				} else {
					
					return "ERROR: El usuario identificado con el mail $correo no es el usuario actual del radicado $radiNume.";
				}
				
			}else {
			
				return "ERROR: " . $usuario['error'];
			}
			
		}else {
			
			return "ERROR: El radicado $radiNume no existe.";
		}
	
	} else {
		
		return "ERROR: No se recibieron todos los datos necesarios para realizar la operacion.";
	}
}

/**
 * ESta funcion tiene como objetivo retornar el documento asociado al numero de anexo respectivo pasado por parametros 
 * @param $numeroAnexo numero del anexo al que se le desea buscar el documento asociado
 * @author Jorge Suarez
 * @return Retorna un archivo base64 asociado al numero de anexo respectivo.
 */
function getDocumentoAnexo($numeroAnexo) {
	
	global $ruta_raiz;
	global $RUTA_BODEGA;
	
	$resultado = null;
	
	try {
		
		if ($numeroAnexo != null) {
			
			if (strlen ( $numeroAnexo ) == "19") {
				
				$db = new ConnectionHandler ( $ruta_raiz, 'WS' );
				
				$consultaAnex = "select ANEX_RADI_NUME, ANEX_NOMB_ARCHIVO from ANEXOS where ANEX_CODIGO='$numeroAnexo'";
				
				$rs = $db->query ( $consultaAnex );
				
				//validacion si la longitud de numero de anexo es correcta
				if (! $rs->EOF) {
					
					$nombreArchivoAnexo = $rs->fields ["ANEX_NOMB_ARCHIVO"];
					
					if ($nombreArchivoAnexo != null) {
						
						$year = substr($numeroAnexo, 0, 4);
						$dependencia = substr($numeroAnexo, 4, 3);
						$pathArchivo = $RUTA_BODEGA . $year . "/" . $dependencia . "/docs/" . $nombreArchivoAnexo;
						
						if (file_exists($pathArchivo)) {
							
							$strFile =  file_get_contents ( $pathArchivo );
							
							if ($strFile != null) {

								$resultado = base64_encode($strFile);
								
							}else {
								
								$resultado = "ERROR: No se pudo cargar el archivo $pathArchivo asociado al anexo $numeroAnexo.";
							}
							
						}else {
							
							$resultado = "ERROR: El archivo '$pathArchivo' asociado al anexo no existe.";
						}
						
					}else {
						
						$resultado = "ERROR: No se pudo obtener informacion del archivo asociado al numero de anexo.";
						
					}
					
				//validacion si numero de anexo existe 
				} else {
					
					$resultado =  "ERROR: El numero de Anexo '$numeroAnexo' no existe.";
				}
			
			} else {
				
				$resultado = "ERROR: El numero de Anexo '$numeroAnexo' no tiene el formato correcto.";
			}
		
		} else {
			
			$resultado = "ERROR: No se recibio informacion del numero de anexo.";
		}
		
	} catch ( Exception $e ) {
		
		$resultado = "ERROR: " . $e->getMessage();
	}
	
	return $resultado;
	
}

?>