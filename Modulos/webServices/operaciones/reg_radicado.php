<?php 

/**
 * Script que contiene el resgistro de las operaciones que se pueden
 * asociar dentro de la categoria de Radicado
 * 
 * @author cgprada
 * 
 */

$server->register('anexoRadicadoToRadicado',                                 //nombre del servicio
   	array('radiNume' => 'xsd:string',                                    //numero de radicado
   'file' => 'xsd:base64Binary',                                       //archivo en base 64
   'filename' => 'xsd:string',                                        //nombre original del archivo
   'correo' => 'xsd:string',                                           //correo electronico
   'descripcion'=>'xsd:string',                                        //descripcion del anexo
   'radiSalida'=>'xsd:string',
   'estadoAnexo'=>'xsd:string'
   	),                                                                //fin parametros del servicio
  	array('return' => 'xsd:string'),                                   //retorno del servicio
  	$ns,
	$ns."/anexoRadicadoToRadicado",
	'rpc',
	'encoded',
	'Operacion que permite anexar un radicado a otro creando previamente un anexo. <br/>
	Entradas: <br/>
	- radiNume: Numero de radicado padre. <br/>
	- file: Archivo codificado en base 64. <br/>
	- filename: Nombre del archivo a subir. <br/>
	- correo: Correo electronico del usuario que realiza la operacion. <br/>
	- descripcion: Descripcion asociada al anexo a crear. <br/>
	- radiSalida: Numero de radicado hijo. <br/>
	- estadoAnexo: Estado en el que se encuentra el anexo. Valor "3" si ya se encuentra radicado el docuemento hijo. <br/>
	Salida: String con el numero de anexo generado o una cadena de tipo "ERROR: ..." en caso de que la operacion no se realice correctamente.' //Elemento namespace para el metod
);


//Servicio para anular radicacion de Orfeo
/*$server->register('anularRadicado',
	array(
		'checkValue'=>'tns:Vector',
		'dependencia'=>'xsd:string',
		'usua_doc'=>'xsd:string',
		'observa'=>'xsd:string',
		'codusuario'=>'xsd:string'
	),
	array(
		'return'=>'xsd:string'),
	$ns,
	$ns."/anularRadicado",
	'rpc',
	'encoded',
	'Operacion que permite solicitar la anulacion de un conjunto de radicados en Orfeo. <br/>
	Entradas: <br/>
	- checkValue: Array de radicados a anular. <br/>
	- dependencia: Codigo de la dependencia del usuario que va a solicitar la anulacion. <br/>
	- usua_doc: Documento del usuario que va a solicitar la anulacion. <br/>
	- observa: Observacion asociada a la solicitud de anulacion. <br/>
	- codusuario: Codigo del usuario que va a solicitar la anulacion. <br/>	
	Salida: Cadena con informacion de los radicados en formato no estandar.'
);
*/
/**
 * Funcion que verifica si un radicado ha sido enviado o no 
 * 
 * @param   radicado 
 * @return  string representativo de mensaje Xml con respuesta si se envio o no el radicado
 *
 */
/*
$server->register('getInfoEnvioRadicado',
	array('numRadicado'=>'xsd:string'),
	array('return'=>'xsd:string'),
	$ns,
	$ns.'/getInfoEnvioRadicado',
	'rpc',
	'encoded',
	'Operacion que dado un numero de radicado devuelve informacion relacionada con su envio. <br/>
	Entradas: <br/>
	- numRadicado: Numero de radicado que se desea consultar. <br/>
	Salida: La respuesta es un String que representa un mensaje XML generico que contiene 
	las propiedades: <br/>
	- respuestaWS: "OK" en caso de exito, "ERROR: ..." en caso de fallo. <br/>
	- envio: "si" en caso de que haya sido enviado, "no" en caso contrario. <br/>
	- fechaEnvio: Fecha en la que se realizo en el envio. Formato "dd/mm/yyyy". <br/>
	- medioEnvio: Medio por el cual se realizo el envio.'
);
*/
//Servicio para validacion Radicado
/*
$server->register('getInfoUltimaTransaccion', 
	array(
		'numRadicado' => 'xsd:string' 
    ),
    array(
		'return' => 'xsd:string'
	),
   	$ns,
	$ns."/getInfoUltimaTransaccion",
	'rpc',
	'encoded',
	'Operacion que devuelve la informacion de la ultima transaccion que se realizo sobre el radicado. <br/> 
	Entradas: <br/>
	- numRadicado: Numero de radicado del cual se desea consultar la ultima transaccion. <br/>
	Salida: String representativo de mensaje XML generico que contiene las siguientes propiedades: <br/> 
	- respuestaWS: "OK" en caso de exito, "ERROR: ..." en caso de fallo. <br/>
	- fechaUltimaTransaccion: Fecha de la ultima transaccion. Formato "dd/mm/yyyy". <br/>
	- codigoDependenciaActual: Codigo de la dependencia actual del radicado. <br/>
	- nombreDependeciaActual: Nombre de la dependencia actual. <br/>
	- codigoDependenciaOrigen: Codigo de la dependencia origen de la transaccion. <br/>
	- nombreDependenciaOrigen: Nombre de la dependencia origen de la transaccion. <br/>
	- comentarioTransaccion: Comentario asociado a la transaccion. <br/>
	- loginUsuarioActual: Login de usuario que tiene actualmente el radicado. <br/>
	- loginUsuarioOrigen: Login de usuario origen de la transaccion. <br/>
	- tipoTransaccion: Nombre de la transaccion realizada.' 
);*/
/*
$server->register('informacionRadicado',
	array(
	'objDoc'=> 'xsd:string'
	),
	array('return'=>'xsd:string'),
	$ns,
	$ns."/informacionRadicado",
	'rpc',
	'encoded',
	'Operacion que devuelve informacion de la ESP asociada a un radicado. <br/>
	Entradas: <br/>
	- objDoc: Numero de radicado que se desea consultar. <br/>
	Salida: String con formato XML que contiene informacion de la ESP.'
);

*/
// Servicio que realiza una radicacion en Orfeo
/*
$server->register('modificarRadicado',
	array(
     	'radiNume' => 'xsd:string',
     	'correo' => 'xsd:string',	
		'destinatario'=>'tns:Destinatario',
		'tipo'=>'xsd:string'
	),
	array(
		'return' => 'xsd:string'
	),
	$ns,
	$ns."/modificarRadicado",
	'rpc',
	'encoded',
	'Operacion que permite modificar un Radicado. '
);

*/
// Servicio que realiza una radicacion en Orfeo
$server->register('radicarDocumento',
	array(
		'file' => 'xsd:base64Binary',										//archivo en base 64
     	'fileName' => 'xsd:string',
     	'correo' => 'xsd:string',	
		'destinatario'=>'tns:Destinatario',
		'predio'=>'tns:Destinatario',
		'esp'=>'tns:Destinatario',
		'asu'=>'xsd:string',
		'med'=>'xsd:string',
		'ane'=>'xsd:string',
		'coddepe'=>'xsd:string',
		'tpRadicado'=>'xsd:string',
		'cuentai'=>'xsd:string',
		'radi_usua_actu'=>'xsd:string',
		'tip_rem'=>'xsd:string',
		'tdoc'=>'xsd:string',
		'tip_doc'=>'xsd:string',
		'carp_codi'=>'xsd:string',
		'carp_per'=>'xsd:string'
	),
	array(
		'return' => 'xsd:string'
	),
	$ns,
	$ns."/radicarDocumento",
	'rpc',
	'encoded',
	'Operacion que permite generar radicado nuevo. <br/>
	Entradas: <br/>
	- file: Archivo asociado al radicado codificado en Base64. <br/>
	- fileName: Nombre del archivo que se radica. <br/>
	- correo: Correo del usuario que realiza la radicacion. <br/> 
	- destinatario: Array que contiene informacion del destinatario. <br/>
	- predio: Array que contiene informacion del predio. <br/>
	- esp: Array que contiene informacion de la esp. <br/>
	- asu: Asunto del radicado. <br/>
	- med: Medio de radicacion. <br/>
	- ane: Descripcion de anexos. <br/>
	- coddepe: Codigo de la dependencia. <br/>
	- tpRadicado: Tipo de radicado. <br/>
	- cuentai: Cuenta interna del radicado. <br/>
	- radi_usua_actu: Codigo usuario actual. <br/>
	- tip_rem: Tipo de remitente. <br/>
	- tdoc: <br/>
	- tip_doc: Tipo de documento. <br/>
	- carp_codi: Codigo de carpeta- <br/>
	- carp_per: Codigo de carpeta personal. <br/>
	Salida: String con el numero de radicado genrado en caso de exito. "ERROR:...." en caso de fallo.'
);
/*
$server->register('solicitarAnulacion',
	array(
		'checkValue'=>'tns:Vector',
		'dependencia'=>'xsd:string',
		'usua_doc'=>'xsd:string',
		'observa'=>'xsd:string',
		'codusuario'=>'xsd:string'
	),
	array(
		'return'=>'xsd:string'),
	$ns,
	$ns."/solicitarAnulacion",
	'rpc',
	'encoded',
	'Operacion que permite solicitar la anulacion de un conjunto de radicados en Orfeo. <br/>
	Entradas: <br/>
	- checkValue: Array de radicados a anular. <br/>
	- dependencia: Codigo de la dependencia del usuario que va a solicitar la anulacion. <br/>
	- usua_doc: Documento del usuario que va a solicitar la anulacion. <br/>
	- observa: Observacion asociada a la solicitud de anulacion. <br/>
	- codusuario: Codigo del usuario que va a solicitar la anulacion. <br/>	
	Salida: String "OK: .." en caso de exito, "ERROR:..." en caso de fallo.'
);

$server->register('updatePathRadicado',
	array(
	'radicado'=> 'xsd:string',
	'loginUsuario' => 'xsd:string',
	'path' => 'xsd:string'
	),
	array('return'=>'xsd:string'),
	$ns,
	$ns."/updatePathRadicado",
	'rpc',
	'encoded',
	'Operacion que permite actualizar el path de un radicado. <br/>
	Entradas: <br/>
	- radicado: Numero de radicado al cual se le va a actualizar el path. <br/>
	- loginUsuario: Login del usuario que va a realizar la operacion. <br/>
	- path: Nuevo path que se le va a asociar al radicado. <br/>
	Salida: String "OK: .." en caso de exito, "ERROR:..." en caso de fallo.'
);*/

//Servicio para validacion Radicado
$server->register('validaRadicado', 
    	array('expnum' => 'xsd:string','radinum' => 'xsd:string'),
    	array('return' => 'xsd:string'),
   	 	$ns,
   	 	$ns."/validaRadicado",
		'rpc',
		'encoded',
		'Operacion que realiza validacones con respecto al radicado y expediente. 1) Existencia de radicado 2) Existencia de Expediente
		3) Radicado contenido en Expediente. <br/>
		Entradas: <br/>
		- expnum: Numero de expediente (no obligatorio). <br/>
		- radinum: Numero de radicado (obligatorio). <br/>
		Salida: String "OK: .." en caso de exito, "ERROR:..." en caso de fallo.' 
);

?>