<?php 

/**
 * Script que contiene el resgistro de las operaciones que se pueden
 * asociar dentro de la categoria de Imagenes
 * 
 * @author cgprada
 * 
 */


$server->register('cambiarImagenRad',
	array(
		'numRadicado'=>'xsd:string',
		'ext'=>'xsd:string',
		'file'=>'xsd:base64Binary',
		'hist'=>'xsd:string'
	),
	array(
		'return'=>'xsd:string'
	),
	$ns,
	$ns."/cambiarImagenRad",
	'rpc',
	'encoded',
	'Operacion que permite cambiar el documento asociado a un radicado. <br/>
	Entradas: <br/>
	- numRadicado: Numero de radicado al cual se le desea cambiar la imagen. <br/>
	- ext: Extension del documento a subir. <br/>
	- file: Archivo codificaco en base64 que corresponde al nuevo documento que va a quedar asociado al radicado. <br/>
	- hist: Parametro indica si se registra la transaccion en el historico. Valor sugerido "S". <br/>
	Salida: String "Ok" en caso de exito, "ERROR: ..." en caso de fallo.'
);
/*
$server->register('UploadFile',  									 //nombre del servicio                 
    array(
    'bytes' => 'xsd:base64Binary', 
    'filename' => 'xsd:string'
    ),//entradas        
    array('return' => 'xsd:string'),   								 // salidas
    $ns,                         									 //Elemento namespace para el metodo
    $ns . '/UploadFile',   											 //Soapaction para el metodo	
    'rpc',                 											 //Estilo
  	'encoded',             
    'Operacion que permite subir un archivo y almacenarlo en la bodega de orfeo. <br/>
    Entradas: <br/>
    - bytes: Archivo codificado en base64. <br/>
    - filename: Nombre del documento a subir. Dependiendo del nombre se saca la ruta donde sebe quedar almacenado. 
    Los primeros cuatro caracteres del nombre indican el anno, los siguientes tres la dependencia. De forma que el path queda /anno/dependencia/docs/. <br/>
    Salida: String "exito" en caso de exito, "fallo", "error" en caso de fallo.'        
);
*/
?>