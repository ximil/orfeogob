<?php 
/**
 * Script que contiene el resgistro de las operaciones que se pueden
 * asociar dentro de la categoria de Anexos
 * 
 * @author Hardy deimont Niño
 * 
 */


$server->register('crearAnexo',  //nombre del servicio                 
    array('radiNume' => 'xsd:string',//numero de radicado	
     'file' => 'xsd:base64Binary',//archivo en base 64
     'filename' => 'xsd:string',//nombre original del archivo
     'correo' => 'xsd:string',//correo electronico
     'descripcion'=>'xsd:string',//descripcion del anexo
     ),//fin parametros del servicio        	
    array('return' => 'xsd:string'),//retorno del servicio
    $ns,
    $ns."/crearAnexo",
    'rpc',
    'encoded',
    "Operacion que permite crear un anexo. <br/> 
    Entradas: <br/>
    - radiNume: Numero de radicado al cual se le va a crear el anexo.<br/>
    - file: Documento asociado al anexo que se va a crear. <br/>
    - filename: nombre del archivo. <br/>
    - correo: Correo electronico del usuario que va a crear el anexo. <br/>
    - descripcion: Descripcion asociada al anexo. <br/>
    Salida: La cadena 'Anexo Creado' en caso de exito o 'ERROR ..' en caso contrario."       
);


/**
 * Funcion  que retorna el documento del anexo en Base64 de acuerdo a numero de expediente pasado por parametros
 * @author  Hardy deimont niño
 * @param   radicado 
 * @return  Ok Xml con respuesta si se envio o no el radicado
 *
 */
$server->register('getDocumentoAnexo',
	array('numeroAnexo'=>'xsd:string'),
	array('return'=>'xsd:string'),
	$ns,
	$ns.'/getDocumentoAnexo',
	'rpc',
	'encoded',
	'Operacion que permite obtener el documento asociado a un anexo codificado en base 64. <br/>
	Entradas: <br/>
	- numeroAnexo: Numero de anexo del cual se desea obtener su documento asociado.<br/>
	Salida: En caso de exito el archivo condificado en base 64, "ERROR: ..." en caso contrario.'
);


?>