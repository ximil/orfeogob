<?php 
/**
 * Script que contiene el resgistro de las operaciones que se pueden
 * asociar dentro de la categoria de Expediente
 * 
 * @author cgprada
 * 
 */


$server->register(
	'anexarExpediente',
	array(
		'numRadicado'=>'xsd:string',
		'numExpediente'=>'xsd:string',
		'usuaLogin'=>'xsd:string',
		'observa'=>'xsd:string'
	),
	array(
		'return'=>'xsd:string'
	),
	$ns,
	$ns."/anexarExpediente",
	'rpc',
	'encoded',
	'Operacion que permite anexar un radicado a un expediente. <br/>
	Entradas: <br/>
	- numRadicado: Numero de radicado a anexar. <br/>
	- numExpediente: Numero de expediente al cual se va a anexar el radicado. <br/>
	- usuaLogin: Login de usuario que realiza la operacion. <br/>
	- observa: Observacion asociada a la transaccion. <br/>
	Salida: String con el numero del radicado en caso de exito, "ERROR: .." en caso de falla.'	
);

$server->register ( 
	'crearExpediente', //nombre del servicio                 
	array ('nurad' => 'xsd:string', //numero de radicado	
	'usuario' => 'xsd:string', //usuario que genero la radicacion
	'anoExp' => 'xsd:string', //ano del expediente
	'fechaExp' => 'xsd:string', //fecha expediente
	'codiSRD' => 'xsd:string', //Serie del Expediendte
	'codiSBRD' => 'xsd:string', //Subserie del expediente
	'codiProc' => 'xsd:string', //Codigo del proceso
	'digCheck' => 'xsd:string', 
	'tmr' => 'xsd:string' ),  //digCheck	
	array ('return' => 'xsd:string' ), // salidas
	$ns,
	$ns . '/crearExpediente', 
	'rpc', 
	'encoded', 
	'Operacion que permite crear un nuevo expediente. <br/>
	Entradas: <br/> 
	- nurad: Numero de radicado a partir del cual se va a crear el expediente. <br/>
	- usuario: Login de correro electronico del usuario que va a realizar la operacion. <br/>
	- anoExp: Año del nuevo expediente en formato yyyy. <br/>
	- fechaExp: Fecha de creacion del expediente en formato dd/mm/yy. <br/>
	- codiSRD: Codigo de serie. <br/>
	- codiSBRD: Codigo de la subserie. <br/>
	- codiProc: Codigo del procedimiento. <br/>
	- digCheck: Digito de verificacion "E". <br/>
	- tmr: . <br/>
	Salida: String con el numero del expediente en caso de exito o algo diferente en caso de falla.' 
);

?>