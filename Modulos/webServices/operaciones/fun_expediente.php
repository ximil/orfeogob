<?php 
/**
 * 
 * @param $numRadicado
 * @param $numExpediente
 * @param $usuaLoginMail 
 * @param $observa Observacion de anexo
 * @return string $numRadicado si fue exitoso ó "ERROR: ..." si fallo la operacion
 */
function anexarExpediente($numRadicado, $numExpediente, $usuaLoginMail, $observa) {
	
	global $ruta_raiz;
	include_once $ruta_raiz . "include/tx/Historico.php";
	
	$db = new ConnectionHandler ( $ruta_raiz, 'WS' );
	
	$resultado = null;
	
	try {
		
		if ($numRadicado != null && $numExpediente != null && $usuaLoginMail != null && $observa != null) {
			
			//validacion existencia radicado
			$consultaRadicado = "SELECT radi_nume_radi FROM RADICADO WHERE radi_nume_radi = '$numRadicado'";
			$rs = $db->query ( $consultaRadicado );
			
			if ($rs != null && ! $rs->EOF) {
				//validacion existencia expediente
				$consultaExpediente = "SELECT sgd_exp_numero FROM SGD_SEXP_SECEXPEDIENTES WHERE SGD_EXP_NUMERO = '$numExpediente'";
				$rs1 = $db->query ( $consultaExpediente );
				
				if ($rs1 != null && ! $rs1->EOF) {
					
					//validacion usuario
					$usua = getInfoUsuario ( $usuaLoginMail );
					
					$estado = estadoRadicadoExpediente ( $numRadicado, $numExpediente );
					
					$tipoTx = 53;
					$Historico = new Historico ( $db );
					$fecha = $db->conn->OffsetDate ( 0, $db->conn->sysTimeStamp );
					
					try {
						switch ($estado) {
							case 0 :
								throw new Exception ( "El documento con numero de radicado  {$numRadicado} ya fue anexado al expediente {$numExpediente}" );
							case 1 :
								throw new Exception ( "El documento con numero de radicado {$numRadicado} ya fue anexado al expediente {$numExpediente} y archivado fisicamente" );
							case 2 :
								$consulta = "UPDATE SGD_EXP_EXPEDIENTE SET SGD_EXP_ESTADO=0,SGD_EXP_FECH={$fecha},USUA_CODI=" . $usua ['usua_codi'] . ",USUA_DOC='" . $usua ['usua_doc'] . "'
                                ,DEPE_CODI=" . $usua ['usua_depe'] . " WHERE RADI_NUME_RADI={$numRadicado} 
                                                AND SGD_EXP_NUMERO='{$numExpediente}'";
								break;
							default :
								$consulta = "INSERT INTO SGD_EXP_EXPEDIENTE (SGD_EXP_NUMERO,RADI_NUME_RADI,SGD_EXP_FECH,SGD_EXP_ESTADO,USUA_CODI,USUA_DOC,DEPE_CODI)
                                          VALUES ('{$numExpediente}',{$numRadicado},{$fecha},0," . $usua ['usua_codi'] . ",'" . $usua ['usua_doc'] . "'," . $usua ['usua_depe'] . ")";
								break;
						}
						
					} catch ( Exception $e ) {
						
						throw $e;
					}
					
					if ($db->query ( $consulta )) {
						$radicados = array ($numRadicado );
						$radicados = $Historico->insertarHistoricoExp ( $numExpediente, $radicados, $usua ['usua_depe'], $usua ['usua_codi'], $observa, $tipoTx, 0 );
						$resultado = $radicados [0];
					
					} else {
						throw new Exception ( "No se realizo la operacion" );
					}
				
				} else {
					
					throw new Exception ( "El numero de expediente $numExpediente no existe." );
				}
			
			} else {
				
				throw new Exception ( "El numero de radicado $numRadicado no existe." );
			}
		
		} else {
			
			throw new Exception ( "Faltan datos para realizar la operacion" );
		}
	
	} catch ( Exception $e ) {
		
		$resultado = "ERROR: " . $e->getMessage ();
	}
	
	return $resultado;

}

/**
 * @author German A. Mahecha
 * 
 * Esta funcion permite crear un expediente a partir de un radicado
 * 
 * @param $nurad, numero de radicado
 * @param $usuario, este parametro es el usuario que crea el expediente, es el usuario de correo
 * @param $anoExp, año del expediente yyyy
 * @param $fechaExp, fecha de creacion del expediente 'dd/mm/yy'
 * @param $codiSRD, codigo serie
 * @param $codiSBRD, codigo subserie
 * @param $codiProc, codigo procedimiento
 * @param $digCheck, digito de verificacion. Corresponde a la 'E'
 * @param $tmr
 * @return El numero de expediente para asignarlo en aplicativo de contribuciones AI 
 */
function crearExpediente($nurad, $usuario, $anoExp, $fechaExp, $codiSRD, $codiSBRD, $codiProc, $digCheck, $tmr) {
	
	global $ruta_raiz;
	include_once $ruta_raiz . "include/tx/Expediente.php";
	
	$db = new ConnectionHandler ( $ruta_raiz, 'WS' );
	$expediente = new Expediente ( $db );
	
	//Aqui busco la informacion necesaria del usuario para la creacion de expedientes
	$sql = "select USUA_CODI,DEPE_CODI,USUA_DOC from usuario where upper(usua_email) = upper ('" . $usuario . "@superservicios.gov.co')";
	$rs = $db->conn->query ( $sql );
	while ( ! $rs->EOF ) {
		$codusuario = $rs->fields ['USUA_CODI'];
		$dependencia = $rs->fields ['DEPE_CODI'];
		$usua_doc = $rs->fields ['USUA_DOC'];
		$usuaDocExp = $usua_doc;
		$rs->MoveNext ();
	}
	
	//Insercion para el TMR
	$sql = "insert into sgd_rdf_retdocf (sgd_mrd_codigo,radi_nume_radi,depe_codi,usua_codi,usua_doc,sgd_rdf_fech)";
	$sql .= " values ($tmr,$nurad,$dependencia,$codusuario,'$usua_doc',SYSDATE)";
	
	$db->conn->query ( $sql );
	
	$trdExp = substr ( "00" . $codiSRD, - 2 ) . substr ( "00" . $codiSBRD, - 2 );
	$secExp = $expediente->secExpediente ( $dependencia, $codiSRD, $codiSBRD, $anoExp );
	$consecutivoExp = substr ( "00000" . $secExp, - 5 );
	$numeroExpediente = $anoExp . $dependencia . $trdExp . $consecutivoExp . $digCheck;
	
	$numeroExpedienteE = $expediente->crearExpediente ( $numeroExpediente, $nurad, $dependencia, $codusuario, $usua_doc, $usuaDocExp, $codiSRD, $codiSBRD, 'false', $fechaExp, $codiProc );
	
	$insercionExp = $expediente->insertar_expediente ( $numeroExpediente, $nurad, $dependencia, $codusuario, $usua_doc );
	
	return $numeroExpedienteE;

}



?>