<?php 
/**
 * @author cgprada
 * @fecha 15-07-2010
 * 
 * Se incluyen los scripts que permiten construir el mensaje 
 * de respuesta
 * 
 */

include_once $ruta_raiz . "Modulos/webServices/mensaje/Message.php";
include_once $ruta_raiz . "Modulos/webServices/mensaje/MessageUtil.php";
include_once $ruta_raiz . "Modulos/webServices/mensaje/Properties.php";
include_once $ruta_raiz . "Modulos/webServices/mensaje/Property.php";
include_once $ruta_raiz . "Modulos/webServices/mensaje/Record.php";
include_once $ruta_raiz . "old/class_control/ControlAplIntegrada.php";
include_once $ruta_raiz . "old/anulacion/Anulacion.php";
include_once $ruta_raiz . "old/include/tx/Historico.php";
include_once $ruta_raiz . "old/include/tx/Tx.php";
//include_once $ruta_raiz . "include/db/ConnectionHandler.php";

/**
 * funcion que crea un Anexo, y ademas decodifica el anexo enviasdo en base 64
 *
 * @param string $radiNume numero del radicado al cual se adiciona el anexo
 * @param base64 $file archivo codificado en base64
 * @param string $filename nombre original del anexo, con extension
 * @param string $correo correo electronico del usuario que adiciona el anexo
 * @param string $descripcion descripcion del anexo
 * @return string mensaje de error en caso de fallo o el numero del anexo en caso de exito
 */
function anexoRadicadoToRadicado($radiNume, $file, $filename, $correo, $descripcion, $radiSalida, $estadoAnexo) {
	global $ruta_raiz;
	// Modificado SSPD 24-Noviembre-2008
	// Se agreg� include( $ruta_raiz_subdirectorios."include/db/ConnectionHandlerWs.php" )
	//	include_once( RUTA_RAIZ."include/db/ConnectionHandlerWs.php" );
	include_once (RUTA_RAIZ . "include/db/ConnectionHandler.php");
	$db = new ConnectionHandler ( $ruta_raiz, 'WS' );
	$usuario = getUsuarioCorreo ( $correo );
	$error = (isset ( $usuario ['error'] )) ? true : false;
	$ruta = RUTA_RAIZ . "bodega/" . substr ( $radiNume, 0, 4 ) . "/" . substr ( $radiNume, 4, 3 ) . "/docs/";
	$numAnexos = numeroAnexos ( $radiNume, $db ) + 1;
	$maxAnexos = maxRadicados ( $radiNume, $db ) + 1;
	//PARAMETROS
	if (is_null ( $estadoAnexo ) || strlen ( $estadoAnexo ) < 1) {
		$estadoAnexo = 0;
	}
	$origenAnexo = 0;
	$soloLectura = 'n';
	$fechaRadicado = 'null';
	$sgdDirTipo = 1;
	$numAnexo = ($numAnexos > $maxAnexos) ? $numAnexos : $maxAnexos;
	$nombreAnexo = $radiNume . substr ( "00000" . $numAnexo, - 5 );
	$nombreAnexoExtension = $nombreAnexo;
	if (is_null ( $radiSalida ) || strlen ( $radiSalida ) != 14) {
		$extension = substr ( $filename, strrpos ( $filename, "." ) + 1 );
		$subirArchivo = subirArchivo ( $ruta, $file, $nombreAnexo . "." . $extension );
		$tamanoAnexo = $subirArchivo / 1024; //tamano en kilobytes
		$error = ($error && ! $subirArchivo) ? true : false;
		$desc = $nombreAnexo;
		$radiSalida = 'null';
	} else {
		
		/* PROCEDIMIENTO CUANDO SE ENVIA EL PARAMETRO DE RADICADO DE SALIDA */
		$fechaRadicado = $db->conn->SQLDate ( "d-m-Y H:i A", "RADI_FECH_RADI" );
		$sql = "select $fechaRadicado as Fecha, r.radi_path from radicado r where r.radi_nume_radi=$radiSalida";
		$desc = $radiSalida;
		
		$rs = $db->query ( $sql );
		while ( ! $rs->EOF ) {
			$fechaRadicado = $rs->fields ['FECHA'];
			$radiSalidaPath = $rs->fields ['RADI_PATH'];
			break;
		}
		$fechaRadicado = $db->conn->DBTimestamp ( $fechaRadicado );
		//BUSCA EL PATH DE LA IMAGEN DEL RADICADO
		if (! is_null ( $radiSalidaPath ) || strlen ( $radiSalidaPath ) > 0) {
			$rutaSalida = RUTA_RAIZ . "bodega" . $radiSalidaPath;
			// Copiar  ruta radicado padre 11-02-2009
			$comando = "cp -f $rutaSalida $ruta";
			exec ( $comando );
			//  fin Copiar  ruta radicado padre 11-02-2009
			if (file_exists ( $rutaSalida )) {
				$tamanoAnexo = filesize ( $rutaSalida ) / 1024;
			} else {
				return "ERROR: La imagen no existe en la Bodega de Orfeo " . $rutaSalida;
			
			}
			$extension = substr ( $rutaSalida, strrpos ( $rutaSalida, "." ) + 1 );
			$nombreAnexoExtension = $radiSalida;
			if ($estadoAnexo == 0) {
				$estadoAnexo = 2; //COMO YA ESTA RADICADO
			}
		} else {
			return "ERROR: La imagen no existe en la Bodega de Orfeo";
		
		}
	}
	if ($extension == 'pdf') {
		$soloLectura = 's';
		$origenAnexo = 1;
	}
	$fechaAnexado = $db->conn->OffsetDate ( 0, $db->conn->sysTimeStamp );
	$tipoAnexo = tipoAnexo ( $extension, $db );
	if (! $error) {
		$tipoAnexo = ($tipoAnexo) ? $tipoAnexo : "NULL";
		$consulta = "INSERT INTO ANEXOS (ANEX_CODIGO,ANEX_RADI_NUME,ANEX_TIPO,ANEX_TAMANO,ANEX_SOLO_LECT,ANEX_CREADOR,
                  ANEX_DESC,ANEX_NUMERO,ANEX_NOMB_ARCHIVO,ANEX_ESTADO,SGD_REM_DESTINO,ANEX_FECH_ANEX, ANEX_BORRADO, RADI_NUME_SALIDA,ANEX_ORIGEN,ANEX_RADI_FECH,USUA_DOC, SGD_DIR_TIPO)
                  VALUES('$nombreAnexo',$radiNume,$tipoAnexo,$tamanoAnexo,'$soloLectura','" . $usuario ['login'] . "','$descripcion'
                  ,$numAnexo,'$nombreAnexoExtension.$extension','$estadoAnexo',1,$fechaAnexado, 'N',$radiSalida,$origenAnexo,$fechaRadicado," . $usuario ['documento'] . ",$sgdDirTipo)";
		if ($db->conn->query ( $consulta )) {
			// Copiar  cambio de radinume con radisalida 11-02-2009
			$recordSet ["RADI_NUME_DERI"] = $radiNume;
			$recordSet ["RADI_TIPO_DERI"] = 0;
			$recordWhere ["RADI_NUME_RADI"] = $radiSalida;
			$ok = $db->update ( "RADICADO", $recordSet, $recordWhere );
			include_once (RUTA_RAIZ . 'include/tx/Historico.php');
			$Historico = new Historico ( $db );
			$radicadosSel [0] = $radiNume;
			$Historico->insertarHistorico ( $radicadosSel, $usuario ['dependencia'], $usuario ['codusuario'], $usuario ['dependencia'], $usuario ['codusuario'], "Archivo anexado mediante servicio Web $desc", 64 );
			return $nombreAnexo;
		} else {
			return "ERROR: al insertar anexo";
		}
	} else {
		return "ERROR";
	}
}


/**
 * Esta funcion permite anular un  radicado  en Orfeo
 * @param $checkValue, es un arreglo con los numeros de radicado
 * @author Donaldo Jinete Forero
 * @return El numero del radicado o un mensaje de error en caso de fallo
 */
function anularRadicado($checkValue, $dependencia, $usua_doc, $observa, $codusuario) {
	/*  RADICADOS SELECCIONADOS
	 *  @$setFiltroSelect  Contiene los valores digitados por el usuario separados por coma.
	 *  @$filtroSelect Si SetfiltoSelect contiene algun valor la siguiente rutina 
	 *  realiza el arreglo de la condificacion para la consulta a la base de datos y lo almacena en whereFiltro.
	 *  @$whereFiltro  Si filtroSelect trae valor la rutina del where para este filtro es almacenado aqui.
	 */
	$radicadosXAnular = "";
	include_once (RUTA_RAIZ . "include/db/ConnectionHandler.php");
	global $ruta_raiz;
	$db = new ConnectionHandler ( $ruta_raiz, 'WS' );
	if ($checkValue) {
		$num = count ( $checkValue );
		$i = 0;
		while ( $i < $num ) {
			$estaRad = false;
			$record_id = $checkValue [$i];
			// Consulta para verificar el estado del radicado del radicado en sancionados
			$querySancionados = "SELECT ESTADO 
						FROM SANCIONADOS.SAN_RESOLUCIONES 
						WHERE nro_resol = '$record_id'";
			$rs = $db->conn->Execute ( $querySancionados );
			
			// Si esta el radicado
			if (! $rs->EOF) {
				$estado = $rs->fields ["ESTADO"];
				if ($estado != "V") {
					$vigente = false;
				}
				$estaRad = true;
			}
			
			// Si esta el radicado entonces verificar vigencia
			if ($estaRad) {
				// Si se encuentra vigente entonces no se puede anular
				if ($vigente) {
					$arregloVigentes [] = $record_id;
				} else {
					$setFiltroSelect .= $record_id;
					$radicadosSel [] = $record_id;
					$radicadosXAnular .= "'" . $record_id . "'";
				}
			} else {
				$setFiltroSelect .= $record_id;
				$radicadosSel [] = $record_id;
			}
			
			if ($i <= ($num - 2)) {
				if (! $vigente || ! $estaRad) {
					$setFiltroSelect .= ",";
				}
				if ($estaRad && ! empty ( $radicadosXAnular )) {
					$radicadosXAnular .= ",";
				}
			}
			next ( $checkValue );
			$i ++;
			// Inicializando los valores de comprobacion
			$estaRad = false;
			$vigente = true;
		}
		if ($radicadosSel) {
			$whereFiltro = " and b.radi_nume_radi in($setFiltroSelect)";
		}
	}
	$systemDate = $db->conn->OffsetDate ( 0, $db->conn->sysTimeStamp );
	include (RUTA_RAIZ . 'config.php');
	include_once (RUTA_RAIZ . 'anulacion/Anulacion.php');
	include_once (RUTA_RAIZ . 'include/tx/Historico.php');
	// Se vuelve crear el objeto por que saca un error con el anterior 
	$db = new ConnectionHandler ( $ruta_raiz, 'WS' );
	$Anulacion = new Anulacion ( $db );
	$observa = "Solicitud Anulacion.$observa";
	
	/* Sentencia para consultar en sancionados el estado en que se encuentra el radicado
	 * A = Anulado, V = Vigente, B = Estado temporal 
	 * Si el estado del radicado en sancionados es diferente de V puede realizar la sancion
	 */
	// Si por lo menos hay un radicado por anular
	

	$retval .= "<br> radicadosSel = " . $radicadosSel [0];
	
	if (! empty ( $radicadosSel [0] )) {
		$retval .= "<br>Anulacion
					<br>dependencia = $dependencia
					<br>usua_doc = $usua_doc
					<br>observa = $observa
					<br>codusuario = $codusuario
					<br>systemDate = $systemDate";
		$radicados = $Anulacion->solAnulacion ( $radicadosSel, $dependencia, $usua_doc, $observa, $codusuario, $systemDate );
		if (! empty ( $radicadosXAnular )) {
			$sqlSancionados = "update SGD_APLMEN_APLIMENS 
						set SGD_APLMEN_DESDEORFEO = 2 
						where SGD_APLMEN_REF in($radicadosXAnular)";
			$rs = $db->conn->Execute ( $sqlSancionados );
		}
		$fecha_hoy = date ( "Y-m-d" );
		$dateReplace = $db->conn->SQLDate ( "Y-m-d", "$fecha_hoy" );
		$Historico = new Historico ( $db );
		/** 
		 * Funcion Insertar Historico 
		 * insertarHistorico($radicados,  
		 * 			$depeOrigen, 
		 *			$usCodOrigen,
		 *			$depeDestino,
		 *			$usCodDestino,
		 *			$observacion,
		 *			$tipoTx)
		 */
		$depe_codi_territorial = $dependencia;
		
		$radicados = $Historico->insertarHistorico ( $radicadosSel, $dependencia, $codusuario, $depe_codi_territorial, 1, $observa, 25 );
	}
	return $retval;
}

/**
 * Esta funcion tiene como objetivo Consultar si un radicado ha sido enviado
 * y devolver la informacion de envio del mismo.
 * @param $radinum numero del radicado que se quiere consultar 
 * @author Carlos Guillermo Prada. Modificacion 14/09/2009 Debido a que no estaba funcionando
 * @return String representativo de un mensaje xml  con la informacion del resultado.
 */

function getInfoEnvioRadicado($radinum) {
	
	global $ruta_raiz;
	
	$respuesta = new MessageUtil();
	$mensajeRespuesta = new Message ();
	
	try {
		
		$db = new ConnectionHandler ($ruta_raiz );
		
		//1.validamos que venga un numero de radicado
		if ($radinum != NULL && strlen ( $radinum ) == "14") {
			
			//2. si existe en base de datos
			$consultaRad = "select radi_nume_radi from radicado where radi_nume_radi=" . $radinum;
			$rs = $db->query ( $consultaRad );
			
			if (! $rs->EOF) {
				
				//3. Consultar si ha sido enviado
				$consultaEnv = "SELECT SGD_FENV_CODIGO,TO_CHAR(SGD_RENV_FECH,'DD/MM/YYYY HH24:MI:ss') SGD_RENV_FECH,SGD_RENV_PLANILLA FROM SGD_RENV_REGENVIO WHERE RADI_NUME_SAL='$radinum'";
				//	 return $consultaEnv;
				$rs = $db->query ( $consultaEnv );
				
				if (! $rs->EOF) {
					
					//verificamos si no ha sido enviado por el tipo de envio
					$tipoEnvio = $rs->fields ['SGD_FENV_CODIGO'];
					$numeroPlanilla = $rs->fields ['SGD_RENV_PLANILLA'];
					$fechaEnvio = $rs->fields [1];
					
					if ($tipoEnvio != NULL) {
						
						$consultaMedioEnvio = "SELECT SGD_FENV_DESCRIP FROM SGD_FENV_FRMENVIO WHERE SGD_FENV_CODIGO='$tipoEnvio'";
						$rs1 = $db->query ( $consultaMedioEnvio );
						
						if (! $rs1->EOF) {
							
							$medioEnvio = $rs1->fields ['SGD_FENV_DESCRIP'];
							
							
							
							$propertyRespuestaWS = new Property ( "respuestaWS", "OK", "String" );
							$mensajeRespuesta->addProperty ( $propertyRespuestaWS );
							
							//despúes de obtener los datos hacemos la verificacion
							//condicion tipo de enio -> No enviado
							if ($tipoEnvio == 901) {
								
								$propertyEnvio = new Property ( "envio", "No", "String" );
								$mensajeRespuesta->addProperty ( $propertyEnvio );
							
							} else {
								
								if ($tipoEnvio == 101) {
									
									//debo mirar el numero de planilla
									if ($numeroPlanilla != NULL) {
										
										if ($numeroPlanilla != 00) {
											
											//fue enviado
											$propertyEnvio = new Property ( "envio", "Si", "String" );
											$mensajeRespuesta->addProperty ( $propertyEnvio );
										
										} else {
											//no fue enviado
											$propertyEnvio = new Property ( "envio", "No", "String" );
											$mensajeRespuesta->addProperty ( $propertyEnvio );
										}
									
									} else {
										
										//fue enviado
										$propertyEnvio = new Property ( "envio", "Si", "String" );
										$mensajeRespuesta->addProperty ( $propertyEnvio );
										
																			
									} //end else - si no hay numero de planilla

								} else {
									
									//ya ha sido enviado y debo obtener el medio de envio
									$propertyEnvio = new Property ( "envio", "Si", "String" );
									$mensajeRespuesta->addProperty ( $propertyEnvio );
								
								} //end if - si el tipo de envio no es 901 0 101 ya ha sido enviado

							} //end else - si no es el  tipo de envio 901

							//si se envio
							if ($propertyEnvio->getValue() == "Si") {
										
								$propertyFechaEnvio = new Property("fechaEnvio", $fechaEnvio, "Date");
								$propertyFechaEnvio->setDateFormat("dd/MM/yyyy HH:mm:ss");
																
								$propertyMedioEnvio = new Property("medioEnvio", $medioEnvio, "String");
								$mensajeRespuesta->addProperty($propertyFechaEnvio);
								$mensajeRespuesta->addProperty($propertyMedioEnvio);
								
							}
							

						} else {
							
							$mensaje = "ERROR: No se encontro informacion del medio de envio del radicado.";
							$propertyRespuestaWS = new Property ( "respuestaWS", $mensaje, "String" );
							$mensajeRespuesta->addProperty ( $propertyRespuestaWS );
						
						}
					
					} else {
						
						$mensaje = "ERROR: No se encontro informacion del medio de envio del radicado.";
						$propertyRespuestaWS = new Property ( "respuestaWS", $mensaje, "String" );
						$mensajeRespuesta->addProperty ( $propertyRespuestaWS );
					
					} //end if - Se obtuvo informacion del tipo de envio
				

				} else {
					
					$propertyRespuestaWS = new Property ( "respuestaWS", "OK", "String" );
					$propertyEnvio = new Property ( "envio", "No", "String" );
					
					$mensajeRespuesta->addProperty ( $propertyRespuestaWS );
					$mensajeRespuesta->addProperty ( $propertyEnvio );
				
				} //end else - si se obtuvieron registros de la consulta de envio
			

			} else {
				
				$mensaje = "ERROR: El radicado " . $radinum . " no existe.";
				$propertyRespuestaWS = new Property ( "respuestaWS", $mensaje, "String" );
				$mensajeRespuesta->addProperty ( $propertyRespuestaWS );
			
			} // end if - se obtuieron datos de la consulta del radicado
		

		} else {
			
			$mensaje = "ERROR: No se recibio informacion valida con respecto al radicado";
			$propertyRespuestaWS = new Property ( "respuestaWS", $mensaje, "String" );
			$mensajeRespuesta->addProperty ( $propertyRespuestaWS );
		}
	
	} catch ( Exception $e ) {
		
		$mensaje = "ERROR: " . $e->getMessage ();
		$propertyRespuestaWS = new Property ( "respuestaWS", $mensaje, "String" );
		$mensajeRespuesta->addProperty ( $propertyRespuestaWS );
	
	}
	
	return $respuesta->getStringFromMessage($mensajeRespuesta);
}

/**
 * Esta funcion retorna la ultima transaccion del historico correspondiente al numero de radicado ingresado
 * por  parametros 
 * @param $radinum numero del radicado que se le quiere consultar el historico
 * @author Jorge Suarez
 * @return String representativo de un mensaje xml  con la informacion del resultado.
 */
function getInfoUltimaTransaccion($numRadicado) {
	global $ruta_raiz;
	
	$message = new Message ( );
	$respuestaWS = "";
	$mensaje = "";
	
	try {
		
		//Validamos que se reciba un numero radicado
		if ($numRadicado != NULL && strlen ( $numRadicado ) == 14) {
			
			//Se crea coneccion con la base de datos
			$db = new ConnectionHandler ( $ruta_raiz, 'WS' );

			//2. si existe en base de datos
			$consultaRad = "select radi_nume_radi from radicado where radi_nume_radi=" . $numRadicado;
			$rs = $db->query ( $consultaRad );
			
			if (! $rs->EOF) {
				
				//3. se realiza un query para obtener los datos de interes del historico del radicado
				$sql = "select TO_CHAR(HIST_FECH,'DD/MM/YYYY HH24:MI:ss'),DEPE_CODI,USUA_CODI,DEPE_CODI_DEST,USUA_CODI_DEST,HIST_OBSE,SGD_TTR_CODIGO  from HIST_EVENTOS where RADI_NUME_RADI = $numRadicado order by HIST_FECH DESC";
				$rs2 = $db->conn->query ( $sql );
				$db->conn->SetFetchMode ( ADODB_FETCH_ASSOC );
				
				if (!$rs2->EOF) {
					
					//obtengo la info del historico
					$fechaUltimaTransaccion = $rs2->fields[0];
					$codDependenciaOrigen = $rs2->fields ['DEPE_CODI'];
					$codUsuarioOrigen = $rs2->fields ['USUA_CODI'];
					$codDependenciaDestino = $rs2->fields ['DEPE_CODI_DEST'];
					$codUsuarioDestino = $rs2->fields ['USUA_CODI_DEST'];
					$comentario = $rs2->fields ['HIST_OBSE'];
					$codTransaccion = $rs2->fields ['SGD_TTR_CODIGO'];
					
					//completitud de datos de historico
					if ($fechaUltimaTransaccion != NULL && strlen ( $fechaUltimaTransaccion ) > 0 && $codDependenciaOrigen != NULL && $codUsuarioOrigen != NULL && $codDependenciaDestino != NULL && $codUsuarioDestino != NULL && $codTransaccion != NULL) {
						
						//obtenemos las dependencias
						$queryDependenciaOrigen = "select depe_nomb from dependencia where depe_codi = " . $codDependenciaOrigen;
						$rs3 = $db->conn->query ( $queryDependenciaOrigen );
						
						if (! $rs3->EOF) {
							
							$dependenciaOrigen = $rs3->fields ['DEPE_NOMB'];
						
						}
						
						$queryDependenciaDestino = "select depe_nomb from dependencia where depe_codi = " . $codDependenciaDestino;
						$rs4 = $db->conn->query ( $queryDependenciaDestino );
						
						if (! $rs4->EOF) {
							
							$dependenciaDestino = $rs4->fields ['DEPE_NOMB'];
						
						}
						
						//obtenemos la descripcion de la transaccion
						$queryTransaccion = "select sgd_ttr_descrip from sgd_ttr_transaccion where sgd_ttr_codigo = $codTransaccion";
						$rs5 = $db->conn->query ( $queryTransaccion );
						
						if (! $rs5->EOF) {
							
							$descripcionTransaccion = $rs5->fields ['SGD_TTR_DESCRIP'];
						
						}else {
							
							$descripcionTransaccion = "Transaccion no identificada.";
						}
						
						//Obtenemos los login de usuario
						$queryLoginUsuOrigen = "select usua_login from usuario where usua_codi = $codUsuarioOrigen and depe_codi = $codDependenciaOrigen";
						$rs6 = $db->conn->query ( $queryLoginUsuOrigen );
						
						if (! $rs6->EOF) {
							
							$loginUsuarioOrigen = $rs6->fields ['USUA_LOGIN'];
						}
						
						$queryLoginUsuDest = "select usua_login from usuario where usua_codi = $codUsuarioDestino and depe_codi = $codDependenciaDestino";
						$rs7 = $db->conn->query ( $queryLoginUsuDest );
						
						if (! $rs7->EOF) {
							
							$loginUsuarioDestino = $rs7->fields ['USUA_LOGIN'];
						}
						
						//Validamos la completitud de datos de respuesta
						if ($dependenciaOrigen != NULL && $dependenciaDestino != NULL && $descripcionTransaccion != NULL && $loginUsuarioOrigen != NULL && $loginUsuarioDestino != NULL) {
							
							$mensaje = "OK";
							$propertyRespuestaWS = new Property ( "respuestaWS", $mensaje, "String" );
							$message->addProperty ( $propertyRespuestaWS );
							
							//agragamos las otras propiedades del radicado
							
							$property1 = new Property ( "fechaUltimaTransaccion", $fechaUltimaTransaccion, "Date" );
							$property1->setDateFormat ( "dd/MM/yyyy HH:mm:ss" );
							$property2 = new Property ( "codigoDependenciaActual", $codDependenciaDestino, "String" );
							$property3 = new Property ( "nombreDependeciaActual", $dependenciaDestino, "String" );
							$property4 = new Property ( "codigoDependenciaOrigen", $codDependenciaOrigen, "String" );
							$property5 = new Property ( "nombreDependenciaOrigen", $dependenciaOrigen, "String" );
							$property6 = new Property ( "comentarioTransaccion", $comentario, "String" );
							$property7 = new Property ( "loginUsuarioActual", $loginUsuarioDestino, "String" );
							$property8 = new Property ( "loginUsuarioOrigen", $loginUsuarioOrigen, "String" );
							$property9 = new Property ( "tipoTransaccion", $descripcionTransaccion, "String" );
							
							$message->addProperty ( $property1 );
							$message->addProperty ( $property2 );
							$message->addProperty ( $property3 );
							$message->addProperty ( $property4 );
							$message->addProperty ( $property5 );
							$message->addProperty ( $property6 );
							$message->addProperty ( $property7 );
							$message->addProperty ( $property8 );
							$message->addProperty ( $property9 );
						
						} else {
							
							$mensaje = "ERROR: No se obtuvieron todos los datos asociados a la ultima transaccion del radicado " . $numRadicado;
							$propertyRespuestaWS = new Property ( "respuestaWS", $mensaje, "String" );
							$message->addProperty ( $propertyRespuestaWS );
						
						} //end else - completitud de datos 
					

					} else {
						
						$mensaje = "ERROR: No se obtuvo informacion importante del historico asociado al radicado " . $numRadicado . ".";
						$propertyRespuestaWS = new Property ( "respuestaWS", $mensaje, "String" );
						$message->addProperty ( $propertyRespuestaWS );
					
					}
				
				} else {
					
					$mensaje = "ERROR: No se encontro informacion en el historico asociado radicado " . $numRadicado . ".";
					$propertyRespuestaWS = new Property ( "respuestaWS", $mensaje, "String" );
					$message->addProperty ( $propertyRespuestaWS );
				
				} //end else - si no obtuvo informacion del historico
			

			} else {
				
				$mensaje = "ERROR: El radicado " . $numRadicado . " no existe.";
				$propertyRespuestaWS = new Property ( "respuestaWS", $mensaje, "String" );
				$message->addProperty ( $propertyRespuestaWS );
			
			} //end else - radicado existe
		

		} else {
			
			$mensaje = "ERROR: No se recibio informacion valida con respecto al radicado.";
			$propertyRespuestaWS = new Property ( "respuestaWS", $mensaje, "String" );
			$message->addProperty ( $propertyRespuestaWS );
		
		} // end else -  Informacion valida de radicado
	

	} catch ( Exception $e ) {
		
		$mensaje = "ERROR:  Ocurrio un error al realizar la operacion.";
		$propertyRespuestaWS = new Property ( "respuestaWS", $mensaje, "String" );
		$message->addProperty ( $propertyRespuestaWS );
	
	}
	
	$mensajeConvertido = null;
	
	try {

		$mensajeConvertido = MessageUtil::getStringFromMessage ( $message );
		
	} catch (Exception $e) {
	
		$mensajeConvertido = "ERROR: " . $e->getMessage();
	}
	
	return $mensajeConvertido;
}

/**
 * @author cgprada - Modificado 29-09-2010 debido a que no funcionada
 * 
 * Se ajusto la consulta para que no utilizara la tabla HIST_EVENTOS
 * 
 * @param string $objDoc Numero de radicado a consultar
 * @return String 'ERROR: ...' en caso de error o informacion de la empresa
 */
function informacionRadicado($objDoc) {
	global $ruta_raiz;
	
	include_once ($ruta_raiz . 'include/db/ConnectionHandler.php');
	$respuesta = null;
	
	try {
		
		if ($objDoc != null && strlen ( $objDoc ) == 14) {
			
			$db = new ConnectionHandler ( $ruta_raiz );
			$db->conn->SetFetchMode ( ADODB_FETCH_ASSOC );
			
			$sql = "SELECT EESP_CODI FROM RADICADO WHERE RADI_NUME_RADI = '$objDoc'";
			$rs = $db->query ( $sql );
			
			$codigoEsp = null;
			
			if (! $rs->EOF) {
				
				$codigoEsp = $rs->fields ['EESP_CODI'];
				
				if ($codigoEsp != null) {
					
					$sql = "SELECT BE.NOMBRE_DE_LA_EMPRESA as nombre,BE.NIT_DE_LA_EMPRESA as nit,
							BE.SIGLA_DE_LA_EMPRESA as sigla ,BE.DIRECCION as direccion,
							BE.CODIGO_DEL_DEPARTAMENTO as departamento,DPT.DPTO_NOMB as n_departamento,
							BE.CODIGO_DEL_MUNICIPIO as municipio, MUN.MUNI_NOMB as n_municipio,
							BE.TELEFONO_1 as telefono_1, BE.TELEFONO_2 as telefono_2, BE.EMAIL as email, 
							BE.NOMBRE_REP_LEGAL as representante, BE.CARGO_REP_LEGAL as cargo,
							BE.IDENTIFICADOR_EMPRESA as identificador,
							BE.ARE_ESP_SECUE as secuencia,
							BE.ID_CONT as continente,
							BE.ID_PAIS as pais,
							BE.ACTIVA as activa,
							BE.FLAG_RUPS as rups,
							RAD.RADI_FECH_RADI as fechaRad
							FROM BODEGA_EMPRESAS BE,DEPARTAMENTO DPT,MUNICIPIO MUN, RADICADO RAD
							WHERE BE.IDENTIFICADOR_EMPRESA='" . $codigoEsp . "'
							AND BE.CODIGO_DEL_DEPARTAMENTO=DPT.DPTO_CODI
							AND BE.CODIGO_DEL_MUNICIPIO=MUN.MUNI_CODI
							AND BE.CODIGO_DEL_DEPARTAMENTO=MUN.DPTO_CODI
							AND RAD.RADI_NUME_RADI='" . $objDoc . "'";
					
					$rs1 = $db->query ( $sql );
					
					if (! $rs1->EOF) {
						
						$respuesta .= "&lt;EMPRESA&gt;";
						foreach ( $rs1->fields as $indice => $valor ) {
							if (! empty ( $valor )) {
								$respuesta .= "&lt;" . $indice . "&gt;" . "$valor" . "&lt;/" . $indice . "&gt;\n";
							}
						}
						$respuesta .= "&lt;/EMPRESA&gt;";
					
					} else {
						
						throw new Exception("No se encontro informacion de la ESP de codigo $codigoEsp");
					}
				
				} else {
					
					throw new Exception("El radicado $objDoc no tiene asociada una ESP.") ;
				}
			
			} else {
				
				throw new Exception("El radicado $objDoc no existe.");
			}
		
		} else {
			
			if ($objDoc == null || ! strlen ( $objDoc ) > 0) {
				
				throw new Exception ( "No se recibio informacion del radicado a consultar." );
			
			} elseif (strlen ( $objDoc ) < 14) {
				
				throw new Exception ( "Formato de radicado invalido." );
			}
		
		}
	
	} catch ( Exception $e ) {
		
		$respuesta = "ERROR: " . $e->getMessage ();
	
	}
	
	return $respuesta;
}

/**
 * @param $radiNume, este parametro es el Nuemro de radicado
 * @param $usuEmail, este parametro es el correo electronico del usuario
 * @param $correo, Correo del usuario
 *  @param $destinatarioOrg, arreglo destinatario
 *  @param $tipo, arreglo predio
      	0 - ESP
		1 - OTRA EMPRESA
		2 - PERSONA NATURAL
 * @author Hardy Deimont Niño Velasquez
 * @Modificacion Jorge Mario Suarez
 * @return El numero del radicado o un mensaje de error en caso de fallo
 */


function modificarRadicado($radiNume, $correo, $destinatarioOrg, $tipo) {
	
	//Conversiones de datos para compatibilidad con aplicaciones internas
	global $ruta_raiz;
	if ($tipo == 0)
		return "ERROR: No  se puede modificar la ESP";
	if ($radiNume == Null)
		return "ERROR: hace falta de numero de radicado";
	if ($correo == Null)
		return "ERROR: hace falta el correo";
	if ($destinatarioOrg == Null)
		return "ERROR: hace falta el destinatario";
	if ($tipo == Null || $tipo > 2)
		return "ERROR: hace falta el tipo o el valor es invalido";
	if (strlen ( $radiNume ) < 14)
		return "ERROR: hace falta el numero de radicado es inferior a 14 digitos";
	//include_once (RUTA_RAIZ . "include/db/ConnectionHandlerWs.php");
	include_once (RUTA_RAIZ . "include/db/ConnectionHandler.php");
	include_once (RUTA_RAIZ . "include/tx/Historico.php");
	
	$db = new ConnectionHandler ( $ruta_raiz,'WS' );
	$hist = new Historico ( $db );
	
	$documento_us = $destinatarioOrg [0];
	$cc_documento_us = $destinatarioOrg [1];
	$tipo_emp = $destinatarioOrg [2]; // define el sgdtrd
	$Nombres = $destinatarioOrg [3];
	$prim_apel = $destinatarioOrg [4];
	$seg_apel = $destinatarioOrg [5];
	$telefono_us = $destinatarioOrg [6];
	$direccion_us = $destinatarioOrg [7];
	$mail_us = $destinatarioOrg [8];
	$otro_us = $destinatarioOrg [9];
	$idcont = $destinatarioOrg [10];
	$idpais = $destinatarioOrg [11];
	$dpto_tmp = $destinatarioOrg [12];
	$muni_tmp = $destinatarioOrg [13];
	$grbNombresUs = trim ( $nombre ) . " " . trim ( $prim_apel ) . " " . trim ( $seg_apel );
	$muni = explode ( "-", $muni_tmp );
	$depto = explode ( "-", $dpto_tmp );
	/* 	0 - ESP
		1 - OTRA EMPRESA
		2 - PERSONA NATURAL*/
	
	if ($tipo_emp == 2) {
		////$sqlv = "select SGD_CIU_CODIGO from SGD_CIU_CIUDADANO where sgd_ciu_nombre='$Nombres' and sgd_ciu_apell1='$prim_apel' and	sgd_ciu_apell2= '$seg_apel'";
		$sqlv = "select SGD_CIU_CODIGO from SGD_CIU_CIUDADANO where SGD_CIU_CEDULA='$cc_documento_us'";
		
		$rsv = $db->conn->Execute ( $sqlv );
		if ($rsv->EOF){
			
			$sql2 = "SELECT (sec_ciu_ciudadano.nextval) VA FROM DUAL  ";
			$rs2 = $db->conn->Execute ( $sql2 );
			$VL = $rs2->fields [0];
			
		if ($rs2->EOF){
				return "ERROR: No pudo  asignar el codigo SGD_CIU_CODIGO";
			}
			$sql1 = "INSERT INTO SGD_CIU_CIUDADANO (SGD_CIU_CEDULA, TDID_CODI, SGD_CIU_CODIGO,SGD_CIU_NOMBRE, SGD_CIU_DIRECCION, SGD_CIU_APELL1, SGD_CIU_APELL2, SGD_CIU_TELEFONO, SGD_CIU_EMAIL, ID_CONT, ID_PAIS, DPTO_CODI, MUNI_CODI) 
		        values ('$cc_documento_us',$tipo_emp, $VL, '$Nombres', '$direccion_us',
		         '$prim_apel', '$seg_apel', '$telefono_us', '$mail_us',$idcont, $idpais, $depto[1], $muni[2])  ";
			$itemextra1 = "$VL";
			$rs1 = $db->conn->query ( $sql1 );
		}else{
			
			$sqlc = "update SGD_CIU_CIUDADANO set SGD_CIU_NOMBRE='$Nombres ', 
		    SGD_CIU_DIRECCION='$direccion_us', SGD_CIU_APELL1='$prim_apel ', SGD_CIU_APELL2='$seg_apel',
		    SGD_CIU_TELEFONO='$telefono_us', SGD_CIU_EMAIL='$mail_us', ID_CONT=$idcont, ID_PAIS=$idpais, DPTO_CODI=$depto[1], MUNI_CODI=$muni[2] where SGD_CIU_CEDULA='$cc_documento_us'";
			$rs1 = $db->conn->query ($sqlc);
			$itemextra1 = $rsv->fields [0];
			
		}
		$sgd_ciu_codigo=$documento_us;
		$itemextra = "SGD_CIU_CODIGO";
		//$rs1 = $db->conn->query ( $sql1 );
	    
		$sgdTrd = "1";
	}
	
	if ($tipo_emp == 1) {
		$sql2 = " SELECT (sec_oem_oempresas.nextval) VL FROM DUAL";
		$rs2 = $db->conn->query ( $sql2 );
		$VL = $rs2->fields [0];
		// $VL = $rs2->fields [0];
		if ($rs2->EOF) {
			return "ERROR: No pudo  asignar el codigo SGD_OEM_CODIGO 1";
		
		}
		$VL = $rs2->fields ["VL"];
		
		$conOem="select * from SGD_OEM_OEMPRESAS where SGD_OEM_NIT='$cc_documento_us'";
		$rsv = $db->conn->Execute ( $conOem );
		if ($rsv->EOF) {
		// return "ENTRA";
			$sql1 = "INSERT INTO SGD_OEM_OEMPRESAS
			        ( tdid_codi, SGD_OEM_CODIGO, SGD_OEM_NIT, SGD_OEM_OEMPRESA, SGD_OEM_DIRECCION,
			         SGD_OEM_REP_LEGAL, SGD_OEM_TELEFONO, ID_CONT, ID_PAIS, DPTO_CODI, MUNI_CODI) 
			        values (4, $VL, $cc_documento_us, '$Nombres', '$direccion_us', '$seg_apel', '$telefono_us',$idcont, $idpais, $depto[1], $muni[2])";
			$rs1 = $db->conn->query ( $sql1 );
			
			
		}else{
			
			$sql2 = "update SGD_OEM_OEMPRESAS set SGD_OEM_OEMPRESA='$Nombres ', 
		    SGD_OEM_DIRECCION='$direccion_us', SGD_OEM_REP_LEGAL='$seg_apel', SGD_OEM_TELEFONO='$telefono_us',
		    ID_CONT=$idcont, ID_PAIS=$idpais, DPTO_CODI=$depto[1], MUNI_CODI=$muni[2] where SGD_OEM_NIT='$cc_documento_us'";
			$rs1 = $db->conn->query ( $sql2 );
		}
		$itemextra = "SGD_OEM_CODIGO";
		$itemextra1 = "$VL ";
		$sgdTrd = "2";
	}
  
	$itemextra2 = $itemextra . "=" . $itemextra1;

	$isql = "update SGD_DIR_DRECCIONES
				set 
				MUNI_CODI=$muni[2], 
				DPTO_CODI=$depto[1], 
				id_pais=$idpais, 
				id_cont=$idcont
                ,$itemextra2
				,SGD_SEC_CODIGO=0
				,SGD_DIR_DIRECCION='$direccion_us'
				,SGD_DIR_TELEFONO='$telefono_us'
				,SGD_DIR_MAIL='$mail_us'
				,SGD_DIR_NOMBRE='$otro_us'
				,SGD_DIR_NOMREMDES='$grbNombresUs'
				,SGD_DIR_DOC='$cc_documento_us'
				,SGD_TRD_CODIGO='$sgdTrd'
			 	where radi_nume_radi=$radiNume and SGD_DIR_TIPO=$tipo ";
	$rs1 = $db->conn->query ( $isql );
	
	$csql = "select MUNI_CODI from SGD_DIR_DRECCIONES 	where radi_nume_radi=$radiNume and SGD_DIR_TIPO=$tipo 
				and $itemextra2 and SGD_SEC_CODIGO=0 and SGD_DIR_NOMREMDES='$grbNombresUs'	and  SGD_DIR_DOC='$cc_documento_us'
				and  SGD_TRD_CODIGO='$sgdTrd' ";
	$rsC = $db->conn->query ( $csql );
	
	if (!$rsC->EOF) {
		return "ok ";
	} else {
		$nextval = "sELECT sec_DIR_DiRECCIONES.nextval  FROM DUAL";
		$rss = $db->conn->query ( $nextval );
		$VLs = $rss->fields [0];
		 $sqlx = "insert into SGD_DIR_DRECCIONES (SGD_DIR_CODIGO,MUNI_CODI, 
				DPTO_CODI,id_pais,id_cont	,SGD_SEC_CODIGO
				,SGD_DIR_DIRECCION,SGD_DIR_TELEFONO,SGD_DIR_MAIL,SGD_DIR_NOMBRE
				,SGD_DIR_NOMREMDES,SGD_DIR_DOC,SGD_TRD_CODIGO,radi_nume_radi,SGD_DIR_TIPO, $itemextra) 
				values ($VLs,$muni[2],$depto[1],$idpais,$idcont,0,'$direccion_us','$telefono_us','$mail_us','$otro_us','$grbNombresUs','$cc_documento_us','$sgdTrd',$radiNume,$tipo,$itemextra1)";
		$rsp = $db->conn->query ( $sqlx );
		if (! $rsp->EOF) {
			return "ok";
		} else {
			//return "Fallo";
		}
	}
	return "OK";

}

/**
 * Esta funcion permite radicar un documento en Orfeo
 * @param $usuEmail, este parametro es el correo electronico del usuario
 * @param $file, Archivo asociado al radicado codificado en Base64 
 * @param $filename, Nombre del archivo que se radica
 * @param $correo, Correo del usuario
 * @param $destinos, arreglo de destinatarios destinatarios,predio,esp
 * @param $asu, Asunto del radicado
 * @param $med, Medio de radicacion
 * @param $ane, descripcion de anexos
 * @param $coddepe, codigo de la dependencia
 * @param $tpRadicado, tipo de radicado
 * @param $cuentai, cuenta interna del radicado
 * @param $radi_usua_actu, 
 * @param $tip_rem
 * @param $tdoc
 * @param $tip_doc
 * @param $carp_codi
 * @param $carp_per 
 * @author Donaldo Jinete Forero
 * @return El numero del radicado o un mensaje de error en caso de fallo
 */

function radicarDocumento($file, $filename, $correo, $destinatarioOrg, $predioOrg, $espOrg, $asu, $med, $ane, $coddepe, $tpRadicado, $cuentai, $radi_usua_actu, $tip_rem, $tdoc, $tip_doc, $carp_codi, $carp_per) {
	//Conversiones de datos para compatibilidad con aplicaciones internas
	

	$destinatario = array ('documento' => $destinatarioOrg [0], 'cc_documento' => $destinatarioOrg [1], 'tipo_emp' => $destinatarioOrg [2], 'nombre' => $destinatarioOrg [3], 'prim_apel' => $destinatarioOrg [4], 'seg_apel' => $destinatarioOrg [5], 'telefono' => $destinatarioOrg [6], 'direccion' => $destinatarioOrg [7], 'mail' => $destinatarioOrg [8], 'otro' => $destinatarioOrg [9], 'idcont' => $destinatarioOrg [10], 'idpais' => $destinatarioOrg [11], 'codep' => $destinatarioOrg [12], 'muni' => $destinatarioOrg [13] );
	
	$predio = array ('documento' => $predioOrg [0], 'cc_documento' => $predioOrg [1], 'tipo_emp' => $predioOrg [2], 'nombre' => $predioOrg [3], 'prim_apel' => $predioOrg [4], 'seg_apel' => $predioOrg [5], 'telefono' => $predioOrg [6], 'direccion' => $predioOrg [7], 'mail' => $predioOrg [8], 'otro' => $predioOrg [9], 'idcont' => $predioOrg [10], 'idpais' => $predioOrg [11], 'codep' => $predioOrg [12], 'muni' => $predioOrg [13] );
	$esp = array ('documento' => $espOrg [0], 'cc_documento' => $espOrg [1], 'tipo_emp' => $espOrg [2], 'nombre' => $espOrg [3], 'prim_apel' => $espOrg [4], 'seg_apel' => $espOrg [5], 'telefono' => $espOrg [6], 'direccion' => $espOrg [7], 'mail' => $espOrg [8], 'otro' => $espOrg [9], 'idcont' => $espOrg [10], 'idpais' => $espOrg [11], 'codep' => $espOrg [12], 'muni' => $espOrg [13] );
	
	try {
		$radi_usua_actu = getInfoUsuario ( $radi_usua_actu );
		$radi_usua_actu = trim ( $radi_usua_actu ['usua_codi'] );
		
		$coddepe = getInfoUsuario ( $coddepe );
		$coddepe = trim ( $coddepe ['usua_depe'] );
	} catch ( Exception $e ) {
		return $e->getMessage ();
	}
	
	// Fin
	

	global $ruta_raiz;
	
	//include_once( RUTA_RAIZ."include/db/ConnectionHandlerWs.php" );
	include_once (RUTA_RAIZ . "include/db/ConnectionHandler.php");
	include_once (RUTA_RAIZ . "include/tx/Tx.php");
	include_once (RUTA_RAIZ . "include/tx/Radicacion.php");
	include_once (RUTA_RAIZ . "class_control/Municipio.php");
	include_once (RUTA_RAIZ . "include/tx/Historico.php");
	
	$db = new ConnectionHandler ( $ruta_raiz );
	$hist = new Historico ( $db );
	$tmp_mun = new Municipio ( $db );
	$rad = new Radicacion ( $db );
	
	$tmp_mun->municipio_codigo ( $destinatario ["codep"], $destinatario ["muni"] );
	$rad->radiTipoDeri = $tpRadicado;
	$rad->radiCuentai = "'" . trim ( $cuentai ) . "'";
	$rad->eespCodi = $esp ["documento"];
	$rad->mrecCodi = $med;
	$rad->radiFechOfic = date ( "Y-m-d" );
	if (! $radicadopadre)
		$radicadopadre = null;
	$rad->radiNumeDeri = trim ( $radicadopadre );
	$rad->radiPais = $tmp_mun->get_pais_codi ();
	$rad->descAnex = $ane;
	$rad->raAsun = $asu;
	$rad->radiDepeActu = $coddepe;
	$rad->radiDepeRadi = $coddepe;
	$rad->radiUsuaActu = $radi_usua_actu;
	$rad->trteCodi = $tip_rem;
	$rad->tdocCodi = $tdoc;
	$rad->tdidCodi = $tip_doc;
	$rad->carpCodi = $carp_codi;
	$rad->carPer = $carp_per;
	$rad->trteCodi = $tip_rem;
	$rad->radiPath = 'null';
	if (strlen ( trim ( $aplintegra ) ) == 0)
		$aplintegra = "0";
	$rad->sgd_apli_codi = $aplintegra;
	$codTx = 2;
	$flag = 1;
	// Modificado SSPD 02-Diciembre-2008
	//$rad->usuaCodi=14 ;
	$rad->usuaCodi = $radi_usua_actu;
	$rad->dependencia = trim ( $coddepe );
	// Modificado SSPD 09-Diciembre-2008
	// Se consulta la secuencia que se debe utilizar para generar el consecutivo del tipo de radicado $tpRadicado,
	// asignada a la dependencia a la que pertenece el usuario.
	//$noRad = $rad->newRadicado($tpRadicado,$coddepe) ;
	$sqlSecuencia = 'SELECT DEPE_RAD_TP' . $tpRadicado . ' AS DEPE_RAD_TP';
	$sqlSecuencia .= ' FROM DEPENDENCIA D, USUARIO U';
	$sqlSecuencia .= ' WHERE U.DEPE_CODI = D.DEPE_CODI';
	$sqlSecuencia .= ' AND U.USUA_CODI = ' . $radi_usua_actu;
	$sqlSecuencia .= ' AND D.DEPE_CODI = ' . $coddepe;
	$sqlSecuencia .= ' AND U.USUA_ESTA = 1';
	// Modificado SSPD 10-Diciembre-2008
	// Por solicitud del Grupo de Aplicaciones Internas no se valida que el usuario
	// tenga permiso para generar un tipo de radicado espec�fico (en especial
	// resoluciones) hasta desligar Sancionados de Orfeo.
	// Plazo: Primer trimestre 2009.
	//$sqlSecuencia .= ' AND U.USUA_PRAD_TP'.$tpRadicado.' <> 0';
	

	$rsSecuencia = $db->query ( $sqlSecuencia );
	
	$noRad = $rad->newRadicado ( $tpRadicado, $rsSecuencia->fields ['DEPE_RAD_TP'] );
	$nurad = trim ( $noRad );
	$sql_ret = $rad->updateRadicado ( $nurad, "/" . date ( "Y" ) . "/" . $coddepe . "/docs/" . $noRad . ".pdf" );
	
	if ($noRad == "-1") {
		// Modificado SSPD 02-Diciembre-2008
		//return "Error no genero un Numero de Secuencia o Inserto el radicado";
		return "ERROR: no genero un N&uacute;mero de Secuencia o Inserto el radicado";
	}
	$radicadosSel [0] = $noRad;
	$hist->insertarHistorico ( $radicadosSel, $coddepe, $radi_usua_actu, $coddepe, $radi_usua_actu, " ", $codTx );
	$sgd_dir_us2 = 2;
	
	$conexion = $db;
	
	/*
		Preparacion de variables para llamar el codigo del
		archivo grb_direcciones.php
	*/
	
	$tipo_emp_us1 = trim ( $destinatario ['tipo_emp'] );
	$tipo_emp_us2 = trim ( $predio ['tipo_emp'] );
	
	$muni_us1 = trim ( $destinatario ['muni'] );
	$muni_us2 = trim ( $predio ['muni'] );
	$muni_us3 = trim ( $esp ['muni'] );
	
	$codep_us1 = trim ( $destinatario ['codep'] );
	$codep_us2 = trim ( $predio ['codep'] );
	$codep_us3 = trim ( $esp ['codep'] );
	
	$grbNombresUs1 = trim ( $destinatario ['nombre'] ) . " " . trim ( $destinatario ['prim_apel'] ) . " " . trim ( $destinatario ['seg_apel'] );
	$grbNombresUs2 = trim ( $predio ['nombre'] ) . " " . trim ( $predio ['prim_apel'] ) . " " . trim ( $predio ['seg_apel'] );
	
	$cc_documento_us1 = trim ( $destinatario ['cc_documento'] );
	$cc_documento_us2 = trim ( $predio ['cc_documento'] );
	
	$documento_us1 = trim ( $destinatario ['documento'] );
	$documento_us2 = trim ( $predio ['documento'] );
	
	$direccion_us1 = trim ( $destinatario ['direccion'] );
	$direccion_us2 = trim ( $predio ['direccion'] );
	
	$telefono_us1 = trim ( $destinatario ['telefono'] );
	$telefono_us2 = trim ( $predio ['telefono'] );
	
	$mail_us1 = trim ( $destinatario ['mail'] );
	$mail_us2 = trim ( $predio ['mail'] );
	
	$otro_us1 = trim ( $destinatario ['otro'] );
	$otro_us2 = trim ( $predio ['otro'] );
	
	//************** INSERTAR DIRECCIONES *******************************
	

	if (! $muni_us1)
		$muni_us1 = NULL;
	if (! $muni_us2)
		$muni_us2 = NULL;
	if (! $muni_us3)
		$muni_us3 = NULL;
		
	// Creamos las valores del codigo del dpto y mcpio desglozando el valor del <SELECT> correspondiente.
	if (! is_null ( $muni_us1 )) {
		$tmp_mun = new Municipio ( $conexion );
		$tmp_mun->municipio_codigo ( $codep_us1, $muni_us1 );
		$tmp_idcont = $tmp_mun->get_cont_codi ();
		$tmp_idpais = $tmp_mun->get_pais_codi ();
		$muni_tmp1 = explode ( "-", $muni_us1 );
		switch (count ( $muni_tmp1 )) {
			case 4 :
				{
					$idcont1 = $muni_tmp1 [0];
					$idpais1 = $muni_tmp1 [1];
					$dpto_tmp1 = $muni_tmp1 [2];
					$muni_tmp1 = $muni_tmp1 [3];
				
				}
				break;
			case 3 :
				{
					$idcont1 = $tmp_idcont;
					$idpais1 = $muni_tmp1 [0];
					$dpto_tmp1 = $muni_tmp1 [1];
					$muni_tmp1 = $muni_tmp1 [2];
				}
				break;
			case 2 :
				{
					$idcont1 = $tmp_idcont;
					$idpais1 = $tmp_idpais;
					$dpto_tmp1 = $muni_tmp1 [0];
					$muni_tmp1 = $muni_tmp1 [1];
				}
				break;
		}
		unset ( $tmp_mun );
		unset ( $tmp_idcont );
		unset ( $tmp_idpais );
	}
	
	if (! is_null ( $muni_us2 )) {
		$tmp_mun = new Municipio ( $conexion );
		$tmp_mun->municipio_codigo ( $codep_us2, $muni_us2 );
		$tmp_idcont = $tmp_mun->get_cont_codi ();
		$tmp_idpais = $tmp_mun->get_pais_codi ();
		$muni_tmp2 = explode ( "-", $muni_us2 );
		switch (count ( $muni_tmp2 )) {
			case 4 :
				{
					$idcont2 = $muni_tmp2 [0];
					$idpais2 = $muni_tmp2 [1];
					$dpto_tmp2 = $muni_tmp2 [2];
					$muni_tmp2 = $muni_tmp2 [3];
				}
				break;
			case 3 :
				{
					$idcont2 = $tmp_idcont;
					$idpais2 = $muni_tmp2 [0];
					$dpto_tmp2 = $muni_tmp2 [1];
					$muni_tmp2 = $muni_tmp2 [2];
				}
				break;
			case 2 :
				{
					$idcont2 = $tmp_idcont;
					$idpais2 = $tmp_idpais;
					$dpto_tmp2 = $muni_tmp2 [0];
					$muni_tmp2 = $muni_tmp2 [1];
				}
				break;
		}
		unset ( $tmp_mun );
		unset ( $tmp_idcont );
		unset ( $tmp_idpais );
	}
	if (! is_null ( $muni_us3 )) {
		$tmp_mun = new Municipio ( $conexion );
		$tmp_mun->municipio_codigo ( $codep_us3, $muni_us3 );
		$tmp_idcont = $tmp_mun->get_cont_codi ();
		$tmp_idpais = $tmp_mun->get_pais_codi ();
		$muni_tmp3 = explode ( "-", $muni_us3 );
		switch (count ( $muni_tmp3 )) {
			case 4 :
				{
					$idcont3 = $muni_tmp3 [0];
					$idpais3 = $muni_tmp3 [1];
					$dpto_tmp3 = $muni_tmp3 [2];
					$muni_tmp3 = $muni_tmp3 [3];
				}
				break;
			case 3 :
				{
					$idcont1 = $tmp_idcont;
					$idpais3 = $muni_tmp3 [0];
					$dpto_tmp3 = $muni_tmp3 [1];
					$muni_tmp3 = $muni_tmp3 [2];
				}
				break;
			case 2 :
				{
					$idcont3 = $tmp_idcont;
					$idpais3 = $tmp_idpais;
					$dpto_tmp3 = $muni_tmp3 [0];
					$muni_tmp3 = $muni_tmp3 [1];
				}
				break;
		}
		unset ( $tmp_mun );
		unset ( $tmp_idcont );
		unset ( $tmp_idpais );
	}
	
	$newId = false;
	if (! $modificar) {
		$nextval = $conexion->nextId ( "sec_dir_direcciones" );
	}
	if ($nextval == - 1) {
		// Modificado SSPD 02-Diciembre-2008
		//return "No se encontro la secuencia sec_dir_direcciones ";
		return "ERROR: No se encontro la secuencia sec_dir_direcciones ";
	}
	global $ADODB_COUNTRECS;
	if ($documento_us1 != '' and ! $cc != '') {
		$sgd_ciu_codigo = 0;
		$sgd_oem_codigo = 0;
		$sgd_esp_codigo = 0;
		$sgd_fun_codigo = 0;
		if ($tipo_emp_us1 == 0) {
			$sgd_ciu_codigo = $documento_us1;
			$sgdTrd = "1";
		}
		if ($tipo_emp_us1 == 1) {
			$sgd_esp_codigo = $documento_us1;
			$sgdTrd = "3";
		}
		if ($tipo_emp_us1 == 2) {
			$sgd_oem_codigo = $documento_us1;
			$sgdTrd = "2";
		}
		if ($tipo_emp_us1 == 6) {
			$sgd_fun_codigo = $documento_us1;
			$sgdTrd = "4";
		}
		
		$ADODB_COUNTRECS = true;
		
		$record = array ();
		$record ['SGD_TRD_CODIGO'] = $sgdTrd;
		$record ['SGD_DIR_NOMREMDES'] = $grbNombresUs1;
		$record ['SGD_DIR_DOC'] = $cc_documento_us1;
		$record ['MUNI_CODI'] = $muni_tmp1;
		$record ['DPTO_CODI'] = $dpto_tmp1;
		$record ['ID_PAIS'] = $idpais1;
		$record ['ID_CONT'] = $idcont1;
		$record ['SGD_DOC_FUN'] = $sgd_fun_codigo;
		$record ['SGD_OEM_CODIGO'] = $sgd_oem_codigo;
		$record ['SGD_CIU_CODIGO'] = $sgd_ciu_codigo;
		$record ['SGD_OEM_CODIGO'] = $sgd_oem_codigo;
		$record ['SGD_ESP_CODI'] = $sgd_esp_codigo;
		$record ['RADI_NUME_RADI'] = $nurad;
		$record ['SGD_SEC_CODIGO'] = 0;
		$record ['SGD_DIR_DIRECCION'] = $direccion_us1;
		$record ['SGD_DIR_TELEFONO'] = trim ( $telefono_us1 );
		$record ['SGD_DIR_MAIL'] = $mail_us1;
		$record ['SGD_DIR_TIPO'] = 1;
		$record ['SGD_DIR_CODIGO'] = $nextval;
		$record ['SGD_DIR_NOMBRE'] = $otro_us1;
		
		$insertSQL = $conexion->conn->Replace ( "SGD_DIR_DRECCIONES", $record, array ('RADI_NUME_RADI', 'SGD_DIR_TIPO' ), $autoquote = true );
		switch ($insertSQL) {
			case 1 :
				{ //Insercion Exitosa
					$dir_codigo_new = $nextval;
					$newId = true;
				}
				break;
			case 2 :
				{ //Update Exitoso
					$newId = false;
				}
				break;
			case 0 :
				{ //Error Transaccion.
					// Modificado SSPD 02-Diciembre-2008
					//return  "No se ha podido actualizar la informacion de SGD_DIR_DRECCIONES UNO -- $isql --";
					return "ERROR: No se ha podido actualizar la informacion de SGD_DIR_DRECCIONES UNO -- $isql --" . $conexion->conn->ErrorMsg ();
				}
				break;
		}
		unset ( $record );
		$ADODB_COUNTRECS = false;
	}
	// ***********************  us2
	if ($documento_us2 != '') {
		$sgd_ciu_codigo = 0;
		$sgd_oem_codigo = 0;
		$sgd_esp_codigo = 0;
		$sgd_fun_codigo = 0;
		
		if ($tipo_emp_us2 == 0) {
			$sgd_ciu_codigo = $documento_us2;
			$sgdTrd = "1";
		}
		if ($tipo_emp_us2 == 1) {
			$sgd_esp_codigo = $documento_us2;
			$sgdTrd = "3";
		}
		if ($tipo_emp_us2 == 2) {
			$sgd_oem_codigo = $documento_us2;
			$sgdTrd = "2";
		}
		if ($tipo_emp_us2 == 6) {
			$sgd_fun_codigo = $documento_us2;
			$sgdTrd = "4";
		}
		$isql = "select * from sgd_dir_drecciones where radi_nume_radi=$nurad and sgd_dir_tipo=2";
		$rsg = $conexion->query ( $isql );
		
		if ($rsg->EOF) {
			//if($newId==true)
			//{
			$nextval = $conexion->nextId ( "sec_dir_direcciones" );
			//}
			if ($nextval == - 1) {
				//$db->conn->RollbackTrans();
				echo "<span class='etextomenu'>No se encontr&oacute; la secuencia sec_dir_direcciones ";
			}
			
			$isql = "insert into SGD_DIR_DRECCIONES(SGD_TRD_CODIGO, SGD_DIR_NOMREMDES, SGD_DIR_DOC, DPTO_CODI, MUNI_CODI,
      			id_pais, id_cont, SGD_DOC_FUN, SGD_OEM_CODIGO, SGD_CIU_CODIGO, SGD_ESP_CODI, RADI_NUME_RADI, SGD_SEC_CODIGO,
      			SGD_DIR_DIRECCION, SGD_DIR_TELEFONO, SGD_DIR_MAIL, SGD_DIR_TIPO, SGD_DIR_CODIGO, SGD_DIR_NOMBRE)
	  			values('$sgdTrd', '$grbNombresUs2', '$cc_documento_us2', $dpto_tmp2, $muni_tmp2, $idpais2, $idcont2,
	  			$sgd_fun_codigo, $sgd_oem_codigo, $sgd_ciu_codigo, $sgd_esp_codigo, $nurad, 0,'" . trim ( $direccion_us2 ) . "', '" . trim ( $telefono_us2 ) . "', '$mail_us2', 2, $nextval, '$otro_us2')";
			$dir_codigo_new = $nextval;
			$newId = true;
		} else {
			$newId = false;
			$isql = "update SGD_DIR_DRECCIONES
				set MUNI_CODI=$muni_tmp2, DPTO_CODI=$dpto_tmp2, id_pais=$idpais2, id_cont=$idcont2
				,SGD_OEM_CODIGO=$sgd_oem_codigo
				,SGD_CIU_CODIGO=$sgd_ciu_codigo
				,SGD_ESP_CODI=$sgd_esp_codigo
				,SGD_DOC_FUN=$sgd_fun_codigo
				,SGD_SEC_CODIGO=0
				,SGD_DIR_DIRECCION='$direccion_us2'
				,SGD_DIR_TELEFONO='$telefono_us2'
				,SGD_DIR_MAIL='$mail_us2'
				,SGD_DIR_NOMBRE='$otro_us2'
				,SGD_DIR_NOMREMDES='$grbNombresUs2'
				,SGD_DIR_DOC='$cc_documento_us2'
				,SGD_TRD_CODIGO='$sgdTrd'
			 	where radi_nume_radi=$nurad and SGD_DIR_TIPO=2 ";
		}
		
		$rsg = $conexion->query ( $isql );
		
		if (! $rsg) {
			// Modificado SSPD 02-Diciembre-2008
			//return "No se ha podido actualizar la informacion de SGD_DIR_DRECCIONES DOS -- $isql --";
			return "ERROR: No se ha podido actualizar la informacion de SGD_DIR_DRECCIONES DOS -- $isql --";
		}
	
	}
	
	if ($documento_us1 != '' and $cc != '') {
		$sgd_ciu_codigo = 0;
		$sgd_oem_codigo = 0;
		$sgd_esp_codigo = 0;
		$sgd_fun_codigo = 0;
		
		echo "--$sgd_emp_us1--";
		if ($tipo_emp_us1 == 0) {
			$sgd_ciu_codigo = $documento_us1;
			$sgdTrd = "1";
		}
		if ($tipo_emp_us1 == 1) {
			$sgd_esp_codigo = $documento_us1;
			$sgdTrd = "3";
		}
		if ($tipo_emp_us1 == 2) {
			$sgd_oem_codigo = $documento_us1;
			$sgdTrd = "2";
		}
		if ($tipo_emp_us1 == 6) {
			$sgd_fun_codigo = $documento_us1;
			$sgdTrd = "4";
		}
		if ($newId == true) {
			$nextval = $conexion->nextId ( "sec_dir_direcciones" );
		}
		if ($nextval == - 1) {
			//$db->conn->RollbackTrans();
			// Modificado SSPD 02-Diciembre-2008
			//return "No se encontrasena la secuencia sec_dir_direcciones ";
			return "ERROR: No se encontrasena la secuencia sec_dir_direcciones ";
		}
		$num_anexos = $num_anexos + 1;
		$str_num_anexos = substr ( "00$num_anexos", - 2 );
		$sgd_dir_tipo = "7$str_num_anexos";
		$isql = "insert into SGD_DIR_DRECCIONES (SGD_TRD_CODIGO, SGD_DIR_NOMREMDES, SGD_DIR_DOC, MUNI_CODI, DPTO_CODI,
			id_pais, id_cont, SGD_DOC_FUN, SGD_OEM_CODIGO, SGD_CIU_CODIGO, SGD_ESP_CODI, RADI_NUME_RADI, SGD_SEC_CODIGO,
			SGD_DIR_DIRECCION, SGD_DIR_TELEFONO, SGD_DIR_MAIL, SGD_DIR_TIPO, SGD_DIR_CODIGO, SGD_ANEX_CODIGO, SGD_DIR_NOMBRE) ";
		$isql .= "values ('$sgdTrd', '$grbNombresUs1', '$cc_documento_us1', $muni_tmp1, $dpto_tmp1, $idpais1, $idcont1,
						$sgd_fun_codigo, $sgd_oem_codigo, $sgd_ciu_codigo, $sgd_esp_codigo, $nurad, 0, '$direccion_us1',
						'" . trim ( $telefono_us1 ) . "', '$mail_us1', $sgd_dir_tipo, $nextval, '$codigo', '$otro_us7' )";
		$dir_codigo_new = $nextval;
		$nextval ++;
		$rsg = $conexion->query ( $isql );
		if (! $rsg) {
			//$conexion->conn->RollbackTrans();
			// Modificado SSPD 02-Diciembre-2008
			//return "No se ha podido actualizar la informacion de SGD_DIR_DRECCIONES TRES -- $isql --";
			return "ERROR: No se ha podido actualizar la informacion de SGD_DIR_DRECCIONES TRES -- $isql --";
		}
	}
	
	//*********************** FIN INSERTAR DIRECCIONES **********************
	

	$retval .= $noRad;
	
	if ($filename != '') {
		$ext = explode ( '.', $filename );
		cambiarImagenRad ( $retval, $ext [1], $file );
	}
	
	return $retval;
}


/**
 * Esta funcion permite anular un  radicado  en Orfeo
 * @param $checkValue, es un arreglo con los numeros de radicado
 * @author Donaldo Jinete Forero
 * 
 * 
 * @author cgprada
 * @fecha 15-07-2010
 * 
 * Se modifica para:
 * - incluir validaciones
 * - arreglar insercción en historico
 * - agregar el proceso de archivar los radicados
 * - Responder un string representativo de mensaje xml que contine un registro por cada radicado recibido 
 * donde se presenta el resultado de realizar la operacion en cada radicado.
 * 
 * @param $dependencia int codigo de la dependencia del ususario
 * @param $usua_doc string documento del usuario que va a realizar la solicitid de anulacion
 * @param $observa string Observacion de porque se realiza la solicitud de anulacion
 * @param $codusuario int condigo del usuario que realiza la anulacion
 * @return $mensaje string representativo de mensaje XML que contiene el resumen de la operacion por cada radicado
 * 
 * 
 * 
 */
function solicitarAnulacion($checkValue, $dependencia, $usua_doc, $observa, $codusuario) {
	
	global $ruta_raiz;
	
	/*  Validaciones
	 *  1. Completitud de datos
	 *  2. Existencia de usuario
	 *  3. Permisos de Anulacion de Usuario
	 *  4. Existencia del radicado
	 *  6. Tipo del radicado
	 *  7. La dependencia de radicacion del radicado a anular (radi_depe_radi) sea la dependencia del usuario
	 *  5. Validacion anulacion con respecto a aplicativos externos
	 *   
	 */
	
	/**
	 * 
	 * @var string representativo de mensaje XML con la respuesta
	 */
	$respuesta = null;
	/**
	 * 
	 * @var Message 
	 */
	$mensajerespuesta = new Message ( );
	$valorRespuestaWS = null;
	
	/**
	 * 
	 * @var int obtenida de $MODULO_ANULACION en $ruta_raiz/config.php
	 */
	$moduloAnulacion = 11;
	
	try {
		
		//1. completitud de datos
		if ($checkValue != NULL && sizeof ( $checkValue ) > 0 && strlen ( $dependencia ) > 0 && strlen ( $usua_doc ) > 0 && strlen ( $observa ) > 0 && strlen ( $codusuario ) > 0) {
			
			$db = new ConnectionHandler ( $ruta_raiz, "WS" );
			$objCtrlAplInt = new ControlAplIntegrada ( $db );
			
			$archivador = new Tx ( $db );
			$anulador = new Anulacion ( $db );
			$Historico = new Historico ( $db );
			
			$radicadosXAnular = "";
			
			//Validacion de datos de usuario
			$sql = "SELECT DEPE_CODI, USUA_DOC, SGD_PANU_CODI, USUA_LOGIN FROM USUARIO WHERE DEPE_CODI=$dependencia AND USUA_CODI=$codusuario and USUA_DOC='$usua_doc'";
			$rs1 = $db->query ( $sql );
			
			if ($rs1 != null && ! $rs1->EOF) {
				
				//Validacion de permisos de solicitud de anulacion
				$permisoAnulacion = $rs1->fields ['SGD_PANU_CODI'];
				$loginUsuarioAnulador = $rs1->fields ['USUA_LOGIN'];
				
				if ($permisoAnulacion == 1 || $permisoAnulacion == 3 || $permisoAnulacion == 4) {
					
					//recorro lista de radicados
					for($index = 0; $index < sizeof ( $checkValue ); $index ++) {
						
						$radicado = $checkValue [$index];
						
						if ($radicado != null) {
							
							$registroRad = new Record ( );
							
							$propRad = new Property ( "radicado", $radicado, "String" );
							$registroRad->addProperty ( $propRad );
							
							$valorResRad = "";
							
							$consultaRad = "select r.radi_depe_radi, r.sgd_eanu_codigo, r.radi_usua_actu, r.radi_depe_actu, u.usua_login from radicado r, usuario u where r.radi_nume_radi = '$radicado' and r.radi_usua_actu = u.usua_codi and r.radi_depe_actu = u.depe_codi";
							$rs2 = $db->query ( $consultaRad );
							
							//Validaciones con respecto al radicado
							//1. Validamos que el radicado exista
							if ($rs2 != null && ! $rs2->EOF) {
								
								$codigoDescargado = null;
								$dependenciaRadicado = $rs2->fields ['RADI_DEPE_RADI'];
								$codigoDescargado = $rs2->fields ['SGD_EANU_CODIGO'];
								$loginUsuActuRad = $rs2->fields ['USUA_LOGIN'];
								$codDepActuRad = $rs2->fields ['RADI_DEPE_ACTU'];
								$codUsuActuRad = $rs2->fields ['RADI_USUA_ACTU'];
								
								//2. Validacion tipo de radicado -> no debe ser -2
								$tipoRadicado = substr ( $radicado, - 1 );
								
								if ($tipoRadicado != 2) {
									
									//3. Que la dependencia de radicacion del radicado sea la misma del usuario usuario actual
									if ($dependenciaRadicado != null && $dependenciaRadicado == $dependencia) {
										
										if ($codigoDescargado == null) {
											
											//4. validacion apliaccion externa
											$puedeContinuar = $objCtrlAplInt->contiInstancia ( $radicado, $moduloAnulacion, 1 );
											
											if ($puedeContinuar == 1) {
												
												// Inicializando los valores de comprobacion
												$estaRad = false;
												$vigente = true;
												
												//5. Se verificara el estado en sancionados
												$querySancionados = "SELECT ESTADO FROM SANCIONADOS.SAN_RESOLUCIONES WHERE nro_resol = '$radicado'";
												$rs = $db->conn->Execute ( $querySancionados );
												
												// Si esta el radicado
												if ($rs != null && ! $rs->EOF) {
													
													$estado = $rs->fields ["ESTADO"];
													
													if ($estado != "V") {
														
														$vigente = false;
													}
													
													$estaRad = true;
												}
												
												$continuarSolAnulacion = false;
												
												// Si esta el radicado entonces verificar vigencia
												if ($estaRad) {
													
													// Si se encuentra vigente entonces no se puede anular
													if ($vigente) {
														
														$valorResRad = "ERROR: El radicado " . $radicado . " no se puede anular debido a que se encuentra vigente en Sancionados.";
													
													} else {
														
														$continuarSolAnulacion = true;
													}
												
												} else {
													
													$continuarSolAnulacion = true;
												
												} //end else - radicado no esta en sancionados
												

												//si puedo continuar con la solicitud de anulacion
												if ($continuarSolAnulacion) {
													
													$observa = "Solicitud Anulacion. $observa";
													
													try {
														
														//Proceso de solicitud de anulacion del radicado
														

														//1. solicitar la anulacion del radicado
														$systemDate = $db->conn->OffsetDate ( 0, $db->conn->sysTimeStamp );
														$anulador->solAnulacionWS ( array ($radicado ), $dependencia, $usua_doc, $observa, $codusuario, $systemDate );
														
														//2. Actualizar la tabla SGD_APLMEN_APLIMENS			
														$sqlSancionados = "update SGD_APLMEN_APLIMENS set SGD_APLMEN_DESDEORFEO = 2	where SGD_APLMEN_REF = '$radicado'";
														$rs = $db->conn->Execute ( $sqlSancionados );
														
														//3. Insertar en el Historico
														$fecha_hoy = date ( "Y-m-d" );
														$dateReplace = $db->conn->SQLDate ( "Y-m-d", "$fecha_hoy" );
														$depe_codi_territorial = $dependencia;
														/** 
														 * Funcion Insertar Historico 
														 * insertarHistorico($radicados,  
														 * 			$depeOrigen, 
														 *			$usCodOrigen,
														 *			$depeDestino,
														 *			$usCodDestino,
														 *			$observacion,
														 *			$tipoTx)
														 */
														$radicados = $Historico->insertarHistorico ( array ($radicado ), $dependencia, $codusuario, $depe_codi_territorial, 1, $observa, 25 );
														
														if ($loginUsuActuRad != null && $codUsuActuRad != null && $codDepActuRad != null) {
															
															$observacionArchivar = "Archivo de Documentos En Solicitud de Anulacion";
															$archivador->archivar ( array ($radicado ), $loginUsuActuRad, $codDepActuRad, $codUsuActuRad, $observacionArchivar );
															
															$valorResRad = "OK: El proceso de solicitud de anulación se realizó de manera exitosa.";
														
														} else {
															
															$valorResRad = "ERROR: Falló la operación de archivado del radicado, debido a que no se pudo obtener la información del usuario actual del radicado.";
														}
													
													} catch ( Exception $e1 ) {
														
														$valorResRad = "ERROR: Se presento un problema al realizar el proceso de anulación del radicado " . $radicado . ": " . $e1->getMessage ();
													}
												
												} //end if 
											

											} else {
												
												$valorResRad = "ERROR: " . $puedeContinuar . ".";
											}
										
										} else {
											
											$valorResRad = "ERROR: No se puede realizar la operación debido a que el radicado ya ha se encuentra en proceso de anulación.";
										
										}
									
									} else {
										
										$valorResRad = "ERROR: La dependencia de radicación del radicado (" . $dependenciaRadicado . ") no es la misma del usuario que realiza la operación (" . $dependencia . ").";
									}
								
								} else {
									
									$valorResRad = "ERROR: El radicado " . $radicado . " no se puede anular debido a que es de entrada.";
								}
							
							} else {
								
								$valorResRad = "ERROR: El radicado " . $radicado . " no existe.";
							}
							
							$propResRad = new Property ( "resultado", $valorResRad, "String" );
							$registroRad->addProperty ( $propResRad );
							
							$mensajerespuesta->addRecord ( $registroRad );
						
						} //end if - radicado no null
					

					} //end for - recorrer array de radicados
					

					$valorRespuestaWS = "OK: Se realizó el proceso de forma correcta.";
				
				} else {
					
					//$respuesta = "ERROR: El usuario no tiene permisos de anulacion.";
					$valorRespuestaWS = "ERROR: El usuario '$loginUsuarioAnulador' no tiene permisos para solicitar la anulación de los radicados.";
				}
			
			} else {
				
				//$respuesta = "ERROR: Datos de Usuario Invalidos.";
				$valorRespuestaWS = "ERROR: No se encontró información del usuario asociado a los datos ($dependencia, $codusuario, $usua_doc).";
			}
		
		} else {
			
			//$respuesta = "ERROR: Faltan datos para realizar la operacion de anulacion.";
			$valorRespuestaWS = "ERROR: Faltan datos para realizar la operación de solicitud de anulación.";
		
		}
	
	} catch ( Exception $e ) {
		
		$valorRespuestaWS = "ERROR: Se presentó un problema al realizar la operación de solicitud de anulación: " . $e->getMessage () . ".";
	
	}
	
	$propertyRespuestaWS = new Property ( "respuestaWS", $valorRespuestaWS, "String" );
	$mensajerespuesta->addProperty ( $propertyRespuestaWS );
	$respuesta = MessageUtil::getStringFromMessage ( $mensajerespuesta );
	
	return $respuesta;

}

/**
 * @author CGPRADA
 * @version 1.0
 *  
 * Operacion que permite actualizar el radi_path de un radicado
 * 
 * @param string $radicado Numero de radicado al cual sele va a actualizar el radipath 
 * @param string $loginUsuario Login del usuario que debe tener actualmente el radicado 
 * @param string $path Nuevo radipath del radicado 
 * @return string 'OK: .....' ó 'ERROR: .....' dependiendo del resultado de la operacion
 */
function updatePathRadicado($radicado, $loginUsuario, $path) {
	
	$resultado = "OK";
	
	try {
		
		//1. Validacion de completitud de datos
		if ($radicado != null && strlen ( $radicado ) == "14" && $loginUsuario != null && strlen ( $loginUsuario ) > 0 && $path != null && strlen ( $path ) > 0) {
			
			global $ruta_raiz;
			
			include_once $ruta_raiz . "/include/db/ConnectionHandler.php";
			$db = new ConnectionHandler ( $ruta_raiz, 'WS' );
			$db->conn->SetFetchMode ( ADODB_FETCH_ASSOC );
			
			//2. Validaciones adicionales
			$consultaRad = "select radi_nume_radi, radi_usua_actu, radi_depe_actu from radicado where radi_nume_radi = " . $radicado;
			$rs = $db->query ( $consultaRad );
			
			//validacion existencia de radicado
			if ($rs != null && !$rs->EOF) {
				
				$radiUsuActu = $rs->fields['RADI_USUA_ACTU'];
				$radiDepeActu = $rs->fields['RADI_DEPE_ACTU'];
				
				$loginUsuario = strtoupper($loginUsuario);
				$consultaUsuario = "SELECT usua_codi, depe_codi FROM USUARIO WHERE usua_login = '$loginUsuario' AND usua_esta = 1";
				$rs2 = $db->query ( $consultaUsuario );
				//validacion existencia de usuario
				if ($rs2 != null && !$rs2->EOF) {

					$codUsu = $rs2->fields['USUA_CODI'];
					$codDepe = $rs2->fields['DEPE_CODI'];
					
					//validacion usuario actual radicado
					if ($radiUsuActu == $codUsu && $radiDepeActu == $codDepe) {
						
						//3.  Actualizacon del radipath
						$updatePathQuery = "UPDATE RADICADO SET radi_path = '$path' WHERE radi_nume_radi = $radicado";
						$rs3 =  $db->query ( $updatePathQuery );
						
						if ($rs3) {
							
							$resultado = "OK: Se actualizó el path del radicado a " . $path . ".";
							
						}else {
							
							$resultado = "ERROR: Falló la actualización del path del radicado.";
						}
						
						
					}else {
						
						$resultado = "ERROR: No se puede realizar la operación debido a que " . $loginUsuario . " no tiene actualmente al radicado " . $radicado . ".";
					}
					
				}else {
					
					$resultado = "ERROR: El usuario " . $loginUsuario . " no existe.";
				}
				
				
			}else {
				
				$resultado = "ERROR: El número de radicado " .  $radicado . " no existe.";
				
			}
			
		}else {
			
			$resultado = "ERROR: Información insuficiente.";
			
			if ($radicado == null) {
				
				$resultado = $resultado . " Se requiere información del radicado a modificar.";
			}else {
				
				if (strlen ( $radicado ) < "14") {
					
					$resultado = $resultado . " Formato de radicado inválido.";
					
				}
			}
			
			if ($loginUsuario == null) {
				
				$resultado = $resultado . " Se requiere información del usuario que realiza la acción.";
			}
			
			if ($path == null) {
				
				$resultado = $resultado . " Se requiere información de el nuevo path asociado al radicado.";
			}
			
		}
	
	} catch ( Exception $e ) {
		
		$resultado = "ERROR: Ocurrio un error al realizar la operación. " . $e->getMessage ();
	
	}
	
	return $resultado;
}

/**
 * Operacion que permite validar la informacion con respecto a un radicado y un expediente
 * @param $expnum
 * @param $radinum
 * @return string con el 
 */
function validaRadicado($expnum, $radinum) {
	$radiEstado = 0;
	if (strlen ( $radinum ) == "14") {
		global $ruta_raiz;
		$db = new ConnectionHandler ( $ruta_raiz, 'WS' );
		//consulta si el expediente existe
		$consultaExp = "select sgd_exp_numero from sgd_sexp_secexpedientes where sgd_exp_numero='$expnum'";
		//consulta si el radicado existe
		$consultaRad = "select radi_nume_radi,sgd_eanu_codigo from radicado where radi_nume_radi=$radinum";
		
		//consulta si tiene relacion
		$consultaExpxRad = "select radi_nume_radi, sgd_exp_numero, sgd_exp_estado from sgd_exp_expediente where radi_nume_radi=$radinum  and sgd_exp_numero='$expnum'";
		
		$rs = $db->query ( $consultaExp );
		if ($rs->EOF) {
			$error .= "No existe el Expediente";
		}
		
		$rs1 = $db->query ( $consultaRad );
		
		//return $rs1->fields['sgd_eanu_codigo'];
		if ($rs1->EOF) {
			$error .= "No existe el radicado";
			$radiEstado = $rs1->fields ['sgd_eanu_codigo'];
			//return $radiEstado;
		} else {
			$radiEstado = $rs1->fields ['SGD_EANU_CODIGO'];
			// return $radiEstado;
			if ($radiEstado == 1)
				$error .= "El radicado Esta en solicitud de Anulacion";
			elseif ($radiEstado == 2) {
				$error .= "El radicado Esta  Anulado";
			} elseif (! $rs1->EOF && ! $rs->EOF) {
				
				$rs2 = $db->query ( $consultaExpxRad );
				if (! $rs2->EOF) {
					$respuesta = "OK";
				} else {
					$error .= "El numero de radicado no esta contenido en el expediente suministrado";
				}
			} else {
				$error .= "No se puede consultar relacion";
			}
		}
	} else {
		$error = " El numero de radicado esta incompleto";
	}
	if ($respuesta != "OK") {
		$respuesta = "ERROR: " . $error;
	}
	return $respuesta;
}

?>