<?php 
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
date_default_timezone_set('America/Bogota'); 

//Llamado a la clase nusoap
$ruta_raiz = "../../";
define('RUTA_RAIZ',"../../");
include RUTA_RAIZ.'/core/config/config-inc.php';
include RUTA_RAIZ."/extra/nusoap/nusoap.php";
/*echo  RUTA_RAIZ.'/core/config/config-inc.php';
echo "<br>";
echo RUTA_RAIZ."/extra/nusoap/nusoap.php";
echo "<br>";*/
include RUTA_RAIZ."/core/clases/ConnectionHandler.php";


//Asignacion del namespace  
$ns="webServices/Orfeo";

//Creacion del objeto soap_server
$server = new nusoap_server();
$server->configureWSDL('Sistema de Gestion Documental Orfeo',$ns);

//*************************************************************
// Se agregan los tipos de datos complejos
//
//***************************************************************

//Tipo complejo destinatario
$server->wsdl->addComplexType(
	'Destinatario',
	'complexType',
	'struct',
	'all',
	'',
	array(
		'documento' => array('name' => 'documento','type' => 'xsd:string'),
		'cc_documento' => array('name' => 'cc_documento','type' => 'xsd:string'),
		'tipo_emp' => array('name' => 'tipo_emp','type' => 'xsd:string'),
		'nombre' => array('name' => 'nombre','type' => 'xsd:string'),
		'prim_apel' => array('name' => 'prim_apell','type' => 'xsd:string'),
		'seg_apel' => array('name' => 'seg_apell','type' => 'xsd:string'),
		'telefono' => array('name' => 'telefono','type'=>'xsd:string'),
		'direccion' => array('name' => 'direccion','type' => 'xsd:string'),
		'mail' => array('name' => 'mail','type'=>'xsd:string'),
		'otro' => array('name' => 'mail','type'=>'xsd:string'),
		'idcont' => array('name' => 'idcont','type'=>'xsd:string'),
		'idpais' => array('name' => 'idpais','type'=>'xsd:string'),
		'codep' => array('name' => 'codep','type'=>'xsd:string'),
		'muni' => array('name' => 'muni','type' => 'xsd:string')
		)
);
//Adicionando un tipo complejo MATRIZ
 
$server->wsdl->addComplexType(
    'Matriz',
    'complexType',
    'array',
    '',
    'SOAP-ENC:Array',
	array(),
    array(
    array('ref'=>'SOAP-ENC:arrayType','wsdl:arrayType'=>'tns:Vector[]')
    ),
    'tns:Vector'
);

//Adicionando un tipo complejo VECTOR

$server->wsdl->addComplexType(
    'Vector',
    'complexType',
    'array',
    '',
    'SOAP-ENC:Array',
	array(),
    array(
    array('ref'=>'SOAP-ENC:arrayType','wsdl:arrayType'=>'xsd:string[]')
    ),
    'xsd:string'
);

//**************************************************************************
// Se registran los servicios que se van a ofrecer, el metodo register tiene los sigientes parametros
//
//**************************************************************************

/**
 * Se registran las siguientes operaciones:
 * 
 * crearAnexo
 * getDocumentoAnexo
 */
include_once $ruta_raiz . "Modulos/webServices/operaciones/reg_anexos.php";
/**
 * Se registran las siguientes operaciones:
 * 
 * radicado
 * 
 */
include_once $ruta_raiz . "Modulos/webServices/operaciones/reg_radicado.php";
/**
 * Se registran las siguientes operaciones:
 * 
 * imagen
 *
 */
include_once $ruta_raiz . "Modulos/webServices/operaciones/reg_imagen.php";
/**
 * Se registran las siguientes operaciones:
 * 
 * imagen
 *
 */
include_once $ruta_raiz . "Modulos/webServices/operaciones/reg_expediente.php";



//*************************************************************************
// Se inlcuyen las funciones que soportan las operaciones registradas
//*************************************************************************
/**
 * Se incluyen las funciones:
 * crearAnexo
 * getDocumentoAnexo
 */
include_once $ruta_raiz . "Modulos/webServices/operaciones/fun_anexos.php";
/**
 * Se incluyen las funciones:
 * radicado
 */
include_once $ruta_raiz . "Modulos/webServices/operaciones/fun_radicado.php";
/**
 * Se incluyen las funciones:
 * imagen
  */
include_once $ruta_raiz . "Modulos/webServices/operaciones/fun_imagen.php";
/**
 * Se incluyen las funciones:
 * imagen
  */
include_once $ruta_raiz . "Modulos/webServices/operaciones/fun_expediente.php";

$POST_DATA = isset($GLOBALS['HTTP_RAW_POST_DATA'])? $GLOBALS['HTTP_RAW_POST_DATA'] : '';
$server->service($POST_DATA);

?>
