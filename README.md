# README OrfeoGob v1.7.5#

OrfeoGob es un fork de Orfeo 3.9.7, que se integra con los módulos de uso libre de Papiro Cloud, y está orientado a solucionar los problemas de seguridad, y facilitar el uso de la herramienta por parte de los usuarios, simplificando procesos y utilizando tecnologías modernas para su implementación.

OrfeoGob is a fork of Orfeo 3.9.7. aimed to improve security problems, and to make easy the integration with Papiro Cloud modules like simplified document creation, improved audit trail, descriptive document tray, file security, etc.)

In this file you will find the steps to install and run OrfeoGob and a short description of the application

### How do I get set up? ###
#### Requirements ####
* PHP 5.5.9+
* Apache 2.4
* PDO driver to database (postgresql or oracle)
* Git
* Composer
* Working Orfeo version 3.8.4 or 3.9.7 database

#### Clone the repository ####
Clone the orfeoGob repository.

	$ git clone https://bitbucket.org/ximil/orfeogob.git

Enter to orfeogob directory created during clonation

	$ cd orfeogob	

Clone the Papiro Cloud repository.

	$ git clone https://bitbucket.org/ximil/orfeopowertools.git orfeox

Clone the Papiro Cloud PQRD Front End Module
	
	$ git clone https://bitbucket.org/ximil/orfeopowertools-pqr.git pqrd

Create the packages directory to install the modules

	$ cd orfeox
	$ mkdir packages
	$ cd packages
	
Clone the modules repositories
	
	$ git clone https://bitbucket.org/ximil/orfeopowertools-common-modules.git ximil
	$ cd ximil
	$ git submodule init
	$ git submodule update
	
Install the application

	$ cd ../..
	$ composer install && composer update

#### Configuration files ####
##### orfeox/config/app.php #####
First we need to enable the service providers commented in the file:

This should be done uncommenting the corresponding lines in the file orfeox/config/app.php (including services and aliases)

##### orfeox/.env #####
Rename the file .env.example to .env and fill it with correct database connection data values.

After save this file is important to generate a new random key to improve encryption process in the application.

	$ php artisan key:generate

##### orfeox/config/database.php #####
set the value for the key 'default' to the correct database driver (pgsql or oracle)

##### orfeox/config/orfeox.php #####
Rename the file orfeox.php.default to orfeox.php and fill the values with the current orfeo information.

##### orfeox/config/orfeox.json #####
Edit the file to set the actual paths

##### orfeo/old/config.php #####
Rename the file orfeo/old/config.php.default to orfeo/old/config.php and fill the values for each key

Please be sure to fix the absolute path to the file orfeo_config_include.php included in the last line of this file.

##### orfeo/core/config/dbconfig.php #####
Fill this file with current database connection values

##### orfeo/core/config/config-inc.php #####
Rename the file orfeo/core/config/config-inc.php.default to orfeo/core/config/config-inc.php and fill the values for each key

Please be sure to fix the absolute path to the file orfeo_config_include.php included in the last line of this file.

##### orfeox/orfeo_client_configuration/orfeo_config_include.php #####
Edit this file and set the actual paths to orfeo and the api key defined in the file orfeox.php

#### Set the user files directory ####
Create the directory to store user files, and use it to configure the parameters RUTA_BODEGA in teh config.inc.php file and parameter 'files_path' in orfeox.php file

Set permissions to let webserver write into following orfeo directories and user files directory

* orfeo
* orfeox/storage
* owncloud/data
* orfeox/config/orfeox.json
* orfeox/orfeo_client_configuration/original_files
* orfeox/orfeo_client_configuration/replace_files

#### Publish Papiro Cloud modules data ####
To finish the Papiro Cloud modules installation is needed run the publish command in the orfeox directory

	$ php artisan vendor:publish

#### Database upgrade ####
If you are upgrading from Orfeo version 3.8.4. you should set this value in the key 'version' in the configuration file 'orfeox.php' and run the migrations to upgrade the database, assign unique identifiers to users, create roles and assign permissions.

WARNING: THIS COMMAND WILL CHANGE YOUR DATABASE AND CANNOT BE ROLLEDBACK, PLEASE BE SURE YOU DO IT FIRST IN A TEST ENVIRONMENT

	$ php artisan migrate

#### Customize company images ####
The systems use default images for company image located in orfeo/imagenes/default.
To override this configuration you should put your custom images into the directory orfeo/imagenes/custom using the same image names that are used in the default folder.